﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AI.NavMeshPath::.ctor()
extern void NavMeshPath__ctor_m0B7FA10A8709A07D67DC34E2B1263218058A892E (void);
// 0x00000002 System.Void UnityEngine.AI.NavMeshPath::Finalize()
extern void NavMeshPath_Finalize_m5EFE68A7EC561B422FF53F3D23A3F36A6774C02D (void);
// 0x00000003 System.IntPtr UnityEngine.AI.NavMeshPath::InitializeNavMeshPath()
extern void NavMeshPath_InitializeNavMeshPath_m32DFF3F0374B71A31D91E74620E2FC57F891D884 (void);
// 0x00000004 System.Void UnityEngine.AI.NavMeshPath::DestroyNavMeshPath(System.IntPtr)
extern void NavMeshPath_DestroyNavMeshPath_m0FDC5217D082B8887F70888B9BD142615FB0AE87 (void);
// 0x00000005 System.Void UnityEngine.AI.NavMeshPath::ClearCornersInternal()
extern void NavMeshPath_ClearCornersInternal_mF43A0D073E144E471C44D74F6EAB11DBF4B5F7C3 (void);
// 0x00000006 System.Void UnityEngine.AI.NavMeshPath::ClearCorners()
extern void NavMeshPath_ClearCorners_m32CA3284F144B5C24A355453D3DE4509088C6226 (void);
// 0x00000007 System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern void NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747 (void);
// 0x00000008 UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_velocity()
extern void NavMeshAgent_get_velocity_mA6F25F6B38D5092BBE6DECD77F8FDB93D5C515C9 (void);
// 0x00000009 UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_desiredVelocity()
extern void NavMeshAgent_get_desiredVelocity_m5499366125464B6F2ADE807378738496F3D385B8 (void);
// 0x0000000A System.Single UnityEngine.AI.NavMeshAgent::get_remainingDistance()
extern void NavMeshAgent_get_remainingDistance_mB55D92B0CBEA48367C5FC6879FDEE2915FB6CD36 (void);
// 0x0000000B System.Boolean UnityEngine.AI.NavMeshAgent::get_isOnOffMeshLink()
extern void NavMeshAgent_get_isOnOffMeshLink_mFDAA9304C61F1EB4D7F2B4D47B8DE3485C967CD3 (void);
// 0x0000000C System.Boolean UnityEngine.AI.NavMeshAgent::get_hasPath()
extern void NavMeshAgent_get_hasPath_m0FD535ACA98272F1C5082C7EE600ECCB66F044CB (void);
// 0x0000000D System.Boolean UnityEngine.AI.NavMeshAgent::get_pathPending()
extern void NavMeshAgent_get_pathPending_mA015EA650D0BE842B15BDF2C8F344F3F2DE0828D (void);
// 0x0000000E UnityEngine.AI.NavMeshPathStatus UnityEngine.AI.NavMeshAgent::get_pathStatus()
extern void NavMeshAgent_get_pathStatus_m088F9138896B01CE3E280D63B4B445BC171E6705 (void);
// 0x0000000F UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_pathEndPosition()
extern void NavMeshAgent_get_pathEndPosition_mB4A48D2E3A97AC31050D8D00DB9826F2408CF222 (void);
// 0x00000010 System.Boolean UnityEngine.AI.NavMeshAgent::Warp(UnityEngine.Vector3)
extern void NavMeshAgent_Warp_mE6417B4AA745066309AD7B833D2BB698F244541E (void);
// 0x00000011 System.Void UnityEngine.AI.NavMeshAgent::Move(UnityEngine.Vector3)
extern void NavMeshAgent_Move_m46DF1365A2852DE02080E9B444070A08AAE67FE0 (void);
// 0x00000012 System.Void UnityEngine.AI.NavMeshAgent::set_isStopped(System.Boolean)
extern void NavMeshAgent_set_isStopped_m3258581121A85B9F8BC02FCC2111B15506A26896 (void);
// 0x00000013 System.Boolean UnityEngine.AI.NavMeshAgent::SetPath(UnityEngine.AI.NavMeshPath)
extern void NavMeshAgent_SetPath_mBE7339D857BD57D7D8064B0C2DDD1C2FD73FB999 (void);
// 0x00000014 System.Single UnityEngine.AI.NavMeshAgent::get_speed()
extern void NavMeshAgent_get_speed_m5AA9A1B23412A8F5CE24A5312F6E6D4BA282B173 (void);
// 0x00000015 System.Void UnityEngine.AI.NavMeshAgent::set_speed(System.Single)
extern void NavMeshAgent_set_speed_mE71CB504B0CC1E977293722F9BA81B7060A99E14 (void);
// 0x00000016 System.Void UnityEngine.AI.NavMeshAgent::set_angularSpeed(System.Single)
extern void NavMeshAgent_set_angularSpeed_m5729B56BEEF8F863E5FA522135EC9B316A307F8D (void);
// 0x00000017 System.Void UnityEngine.AI.NavMeshAgent::set_acceleration(System.Single)
extern void NavMeshAgent_set_acceleration_mB39FD7A03E6A30CADB1D21AA8CF8078DB3D4F16D (void);
// 0x00000018 System.Void UnityEngine.AI.NavMeshAgent::set_updatePosition(System.Boolean)
extern void NavMeshAgent_set_updatePosition_m098D6C65D34D3AB00EFF1096DB2B6A5373D828BC (void);
// 0x00000019 System.Void UnityEngine.AI.NavMeshAgent::set_updateRotation(System.Boolean)
extern void NavMeshAgent_set_updateRotation_m63A01B06311F89C4D8B201209F83401F0407FCF3 (void);
// 0x0000001A System.Void UnityEngine.AI.NavMeshAgent::set_updateUpAxis(System.Boolean)
extern void NavMeshAgent_set_updateUpAxis_m7AEC5E7C7BDB65A0103BB9D4652EF3B6C35147D3 (void);
// 0x0000001B System.Void UnityEngine.AI.NavMeshAgent::set_radius(System.Single)
extern void NavMeshAgent_set_radius_mAE10A91811FCF27714377F200A4A9C4C7C5E24D2 (void);
// 0x0000001C System.Void UnityEngine.AI.NavMeshAgent::set_height(System.Single)
extern void NavMeshAgent_set_height_mCF84A46255BD102841059890F8C4752F282D5A18 (void);
// 0x0000001D System.Boolean UnityEngine.AI.NavMeshAgent::get_isOnNavMesh()
extern void NavMeshAgent_get_isOnNavMesh_mA14A7BDC5D7669AF5DB9EC1FE9B3C580714EAAB0 (void);
// 0x0000001E System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_SetDestination_Injected_m41607AA111EE126BBBDCDDF76B7523B0BC369D9A (void);
// 0x0000001F System.Void UnityEngine.AI.NavMeshAgent::get_velocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_velocity_Injected_m64CD1C3DAE418314D44A1194F014CEC159CDDAA8 (void);
// 0x00000020 System.Void UnityEngine.AI.NavMeshAgent::get_desiredVelocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_desiredVelocity_Injected_m53FAA060331C4864F6429255E3F78B2B01FA74A1 (void);
// 0x00000021 System.Void UnityEngine.AI.NavMeshAgent::get_pathEndPosition_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_pathEndPosition_Injected_m58ED89B52B96BE917D3371681A288899942D2E75 (void);
// 0x00000022 System.Boolean UnityEngine.AI.NavMeshAgent::Warp_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_Warp_Injected_m861204DF82547182B4A36A41C36BEE8AFD3CE01B (void);
// 0x00000023 System.Void UnityEngine.AI.NavMeshAgent::Move_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_Move_Injected_m4E5EBA840207837268BFE2267E64C8EE93448E95 (void);
// 0x00000024 UnityEngine.Vector3 UnityEngine.AI.NavMeshHit::get_position()
extern void NavMeshHit_get_position_m66845935ED76B2480F72EE6628EFD9D6BF35B39A (void);
// 0x00000025 System.Boolean UnityEngine.AI.NavMeshHit::get_hit()
extern void NavMeshHit_get_hit_m31860A09F6C906FCB1ED3CD9F5AD80A74E809CB2 (void);
// 0x00000026 System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_m5C0CEF0AEF92B6BE0368AA5ABC02B4CCDEFA9AD5 (void);
// 0x00000027 System.Boolean UnityEngine.AI.NavMesh::CalculatePath(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePath_mB19A024583AACFEAFCCEB05797B22799A9DAB850 (void);
// 0x00000028 System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_mC3DCAF515B2B080361987E9DFBB6A36627955C97 (void);
// 0x00000029 System.Boolean UnityEngine.AI.NavMesh::SamplePosition(UnityEngine.Vector3,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_m9675E148D95E1D92ED75DC608CAA33E75ABCA05E (void);
// 0x0000002A System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_Injected_m98321EAC66E7CD56CB97E4F33015287D527159AF (void);
// 0x0000002B System.Boolean UnityEngine.AI.NavMesh::SamplePosition_Injected(UnityEngine.Vector3&,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_Injected_m080B714E6E15722B909226ADC8176F66A51E418A (void);
// 0x0000002C System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_mDBB85480C3EA968112EB3B356486B9C9FF387BD4 (void);
// 0x0000002D System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_m8950FEDFD3E07B272ED469FD1911AA98C60FC28D (void);
// 0x0000002E System.IAsyncResult UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnNavMeshPreUpdate_BeginInvoke_m8B7FF1B745E38190A2B744775602508E77B291FA (void);
// 0x0000002F System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::EndInvoke(System.IAsyncResult)
extern void OnNavMeshPreUpdate_EndInvoke_mA263F64ADF01540E24327DDB24BD334539B1B4D2 (void);
static Il2CppMethodPointer s_methodPointers[47] = 
{
	NavMeshPath__ctor_m0B7FA10A8709A07D67DC34E2B1263218058A892E,
	NavMeshPath_Finalize_m5EFE68A7EC561B422FF53F3D23A3F36A6774C02D,
	NavMeshPath_InitializeNavMeshPath_m32DFF3F0374B71A31D91E74620E2FC57F891D884,
	NavMeshPath_DestroyNavMeshPath_m0FDC5217D082B8887F70888B9BD142615FB0AE87,
	NavMeshPath_ClearCornersInternal_mF43A0D073E144E471C44D74F6EAB11DBF4B5F7C3,
	NavMeshPath_ClearCorners_m32CA3284F144B5C24A355453D3DE4509088C6226,
	NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747,
	NavMeshAgent_get_velocity_mA6F25F6B38D5092BBE6DECD77F8FDB93D5C515C9,
	NavMeshAgent_get_desiredVelocity_m5499366125464B6F2ADE807378738496F3D385B8,
	NavMeshAgent_get_remainingDistance_mB55D92B0CBEA48367C5FC6879FDEE2915FB6CD36,
	NavMeshAgent_get_isOnOffMeshLink_mFDAA9304C61F1EB4D7F2B4D47B8DE3485C967CD3,
	NavMeshAgent_get_hasPath_m0FD535ACA98272F1C5082C7EE600ECCB66F044CB,
	NavMeshAgent_get_pathPending_mA015EA650D0BE842B15BDF2C8F344F3F2DE0828D,
	NavMeshAgent_get_pathStatus_m088F9138896B01CE3E280D63B4B445BC171E6705,
	NavMeshAgent_get_pathEndPosition_mB4A48D2E3A97AC31050D8D00DB9826F2408CF222,
	NavMeshAgent_Warp_mE6417B4AA745066309AD7B833D2BB698F244541E,
	NavMeshAgent_Move_m46DF1365A2852DE02080E9B444070A08AAE67FE0,
	NavMeshAgent_set_isStopped_m3258581121A85B9F8BC02FCC2111B15506A26896,
	NavMeshAgent_SetPath_mBE7339D857BD57D7D8064B0C2DDD1C2FD73FB999,
	NavMeshAgent_get_speed_m5AA9A1B23412A8F5CE24A5312F6E6D4BA282B173,
	NavMeshAgent_set_speed_mE71CB504B0CC1E977293722F9BA81B7060A99E14,
	NavMeshAgent_set_angularSpeed_m5729B56BEEF8F863E5FA522135EC9B316A307F8D,
	NavMeshAgent_set_acceleration_mB39FD7A03E6A30CADB1D21AA8CF8078DB3D4F16D,
	NavMeshAgent_set_updatePosition_m098D6C65D34D3AB00EFF1096DB2B6A5373D828BC,
	NavMeshAgent_set_updateRotation_m63A01B06311F89C4D8B201209F83401F0407FCF3,
	NavMeshAgent_set_updateUpAxis_m7AEC5E7C7BDB65A0103BB9D4652EF3B6C35147D3,
	NavMeshAgent_set_radius_mAE10A91811FCF27714377F200A4A9C4C7C5E24D2,
	NavMeshAgent_set_height_mCF84A46255BD102841059890F8C4752F282D5A18,
	NavMeshAgent_get_isOnNavMesh_mA14A7BDC5D7669AF5DB9EC1FE9B3C580714EAAB0,
	NavMeshAgent_SetDestination_Injected_m41607AA111EE126BBBDCDDF76B7523B0BC369D9A,
	NavMeshAgent_get_velocity_Injected_m64CD1C3DAE418314D44A1194F014CEC159CDDAA8,
	NavMeshAgent_get_desiredVelocity_Injected_m53FAA060331C4864F6429255E3F78B2B01FA74A1,
	NavMeshAgent_get_pathEndPosition_Injected_m58ED89B52B96BE917D3371681A288899942D2E75,
	NavMeshAgent_Warp_Injected_m861204DF82547182B4A36A41C36BEE8AFD3CE01B,
	NavMeshAgent_Move_Injected_m4E5EBA840207837268BFE2267E64C8EE93448E95,
	NavMeshHit_get_position_m66845935ED76B2480F72EE6628EFD9D6BF35B39A,
	NavMeshHit_get_hit_m31860A09F6C906FCB1ED3CD9F5AD80A74E809CB2,
	NavMesh_Internal_CallOnNavMeshPreUpdate_m5C0CEF0AEF92B6BE0368AA5ABC02B4CCDEFA9AD5,
	NavMesh_CalculatePath_mB19A024583AACFEAFCCEB05797B22799A9DAB850,
	NavMesh_CalculatePathInternal_mC3DCAF515B2B080361987E9DFBB6A36627955C97,
	NavMesh_SamplePosition_m9675E148D95E1D92ED75DC608CAA33E75ABCA05E,
	NavMesh_CalculatePathInternal_Injected_m98321EAC66E7CD56CB97E4F33015287D527159AF,
	NavMesh_SamplePosition_Injected_m080B714E6E15722B909226ADC8176F66A51E418A,
	OnNavMeshPreUpdate__ctor_mDBB85480C3EA968112EB3B356486B9C9FF387BD4,
	OnNavMeshPreUpdate_Invoke_m8950FEDFD3E07B272ED469FD1911AA98C60FC28D,
	OnNavMeshPreUpdate_BeginInvoke_m8B7FF1B745E38190A2B744775602508E77B291FA,
	OnNavMeshPreUpdate_EndInvoke_mA263F64ADF01540E24327DDB24BD334539B1B4D2,
};
extern void NavMeshHit_get_position_m66845935ED76B2480F72EE6628EFD9D6BF35B39A_AdjustorThunk (void);
extern void NavMeshHit_get_hit_m31860A09F6C906FCB1ED3CD9F5AD80A74E809CB2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000024, NavMeshHit_get_position_m66845935ED76B2480F72EE6628EFD9D6BF35B39A_AdjustorThunk },
	{ 0x06000025, NavMeshHit_get_hit_m31860A09F6C906FCB1ED3CD9F5AD80A74E809CB2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[47] = 
{
	1551,
	1551,
	2615,
	2588,
	1551,
	1551,
	1192,
	1549,
	1549,
	1539,
	1536,
	1536,
	1536,
	1500,
	1549,
	1192,
	1334,
	1319,
	1159,
	1539,
	1321,
	1321,
	1321,
	1319,
	1319,
	1319,
	1321,
	1321,
	1536,
	1113,
	1264,
	1264,
	1264,
	1113,
	1264,
	1549,
	1536,
	2635,
	1938,
	1938,
	1936,
	1906,
	1907,
	849,
	1551,
	643,
	1301,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_AIModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	47,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_AIModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
