﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean GameCreator.Variables.ActionListVariableAdd::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableAdd_InstantExecute_mE2C7482D4783BDD864E13EFCB6A38EDE2825A489 (void);
// 0x00000002 System.Void GameCreator.Variables.ActionListVariableAdd::.ctor()
extern void ActionListVariableAdd__ctor_mBFDB45503BCC2FEC4D03B73C09738A66B86C2F73 (void);
// 0x00000003 System.Boolean GameCreator.Variables.ActionListVariableClear::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableClear_InstantExecute_m53E62B4075E9002631CF80D2C37EBE43B8206473 (void);
// 0x00000004 System.Void GameCreator.Variables.ActionListVariableClear::.ctor()
extern void ActionListVariableClear__ctor_m0BAC288575F953D1C4BB82B482D07F09B4DC0F22 (void);
// 0x00000005 System.Boolean GameCreator.Variables.ActionListVariableIterator::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableIterator_InstantExecute_m787E110B3BA0CEFA3231C617DC02210182EE54B7 (void);
// 0x00000006 System.Void GameCreator.Variables.ActionListVariableIterator::.ctor()
extern void ActionListVariableIterator__ctor_m0A47C44DFA38ED917B017251444AE38143AD8BD4 (void);
// 0x00000007 System.Collections.IEnumerator GameCreator.Variables.ActionListVariableLoop::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableLoop_Execute_m484A83A08BD40ACF0AC56B7F65DD61F11E15465E (void);
// 0x00000008 System.Void GameCreator.Variables.ActionListVariableLoop::OnCompleteActions()
extern void ActionListVariableLoop_OnCompleteActions_m3DA67F4FA0001E682F01958E5CA7931946638AD5 (void);
// 0x00000009 System.Void GameCreator.Variables.ActionListVariableLoop::Stop()
extern void ActionListVariableLoop_Stop_m37D23A85714A4EDC2EC3BFA774CF070E770EA20E (void);
// 0x0000000A System.Void GameCreator.Variables.ActionListVariableLoop::.ctor()
extern void ActionListVariableLoop__ctor_m2770F2E8CCC639A33A3A2124A9B0CE3CB5BD4C54 (void);
// 0x0000000B System.Void GameCreator.Variables.ActionListVariableLoop/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mEC0C8A8A57314E4BEF46BF12C67104887B6E9E13 (void);
// 0x0000000C System.Boolean GameCreator.Variables.ActionListVariableLoop/<>c__DisplayClass8_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CExecuteU3Eb__0_mD3BC06E8BE1CC8C7C59FB32A843073EDC199D94C (void);
// 0x0000000D System.Void GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::.ctor(System.Int32)
extern void U3CExecuteU3Ed__8__ctor_m658ECA02DE483EE45BD23E8B74735576A87A991B (void);
// 0x0000000E System.Void GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__8_System_IDisposable_Dispose_mA4C5A1494AACB2E9E96F1C9BD450A199CE8B687D (void);
// 0x0000000F System.Boolean GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::MoveNext()
extern void U3CExecuteU3Ed__8_MoveNext_m362E562699C5EE8E86F41DBDCA618A8E42F40602 (void);
// 0x00000010 System.Object GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFD2215568C11F45456ADE5DF3E6A8D8F5004560 (void);
// 0x00000011 System.Void GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEEBD76692D994EC6D6539E5B6392BE1A12366222 (void);
// 0x00000012 System.Object GameCreator.Variables.ActionListVariableLoop/<Execute>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_mD77DDDFDD37CE5E81FC21CF5E61415CDFC2FDC99 (void);
// 0x00000013 System.Boolean GameCreator.Variables.ActionListVariableRemove::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableRemove_InstantExecute_m567FA75F31C039F4735805D16AF9203B232D8D38 (void);
// 0x00000014 System.Void GameCreator.Variables.ActionListVariableRemove::.ctor()
extern void ActionListVariableRemove__ctor_mDA41DC23F364308633369787399BA240CB5A7625 (void);
// 0x00000015 System.Boolean GameCreator.Variables.ActionListVariableSelect::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionListVariableSelect_InstantExecute_m153650C0129732939E55242E11109BEB0BC8A678 (void);
// 0x00000016 System.Void GameCreator.Variables.ActionListVariableSelect::.ctor()
extern void ActionListVariableSelect__ctor_m26C5B0FA38CD4B081C73F3E1A397D6C998CABEF1 (void);
// 0x00000017 System.Boolean GameCreator.Variables.ActionVariableDebug::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableDebug_InstantExecute_m35FF2F62FEB907B794393E26D77604342C2158EC (void);
// 0x00000018 System.Void GameCreator.Variables.ActionVariableDebug::.ctor()
extern void ActionVariableDebug__ctor_m041C5096929B1B620FA1741CC5227C9917F5D3F6 (void);
// 0x00000019 System.Boolean GameCreator.Variables.ActionVariableRandom::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableRandom_InstantExecute_m04C4C74EA9502D8DF62B11736FF1F5D242F8504D (void);
// 0x0000001A System.Void GameCreator.Variables.ActionVariableRandom::.ctor()
extern void ActionVariableRandom__ctor_mDB5336055F15FB606BCEF40EE7205B75CFB07787 (void);
// 0x0000001B System.Boolean GameCreator.Variables.ActionVariablesReset::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariablesReset_InstantExecute_m2A27B917EBEBDDBFA875B69BB28089B92F150889 (void);
// 0x0000001C System.Void GameCreator.Variables.ActionVariablesReset::.ctor()
extern void ActionVariablesReset__ctor_mB9840097D01D15EC6082E777DA48EEAC87E2F365 (void);
// 0x0000001D System.Boolean GameCreator.Variables.ActionVariableAdd::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableAdd_InstantExecute_mAEB97B431BA9B1C0D94943CA0FAC42582CC16D04 (void);
// 0x0000001E System.Void GameCreator.Variables.ActionVariableAdd::.ctor()
extern void ActionVariableAdd__ctor_mCD4DEBBE1F5339604DDFA19BD0B03749C7BEF212 (void);
// 0x0000001F System.Boolean GameCreator.Variables.ActionVariableDivide::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableDivide_InstantExecute_m7D5ABC4B1A515B4EDB64BEAA88B4AF3410A65CCC (void);
// 0x00000020 System.Void GameCreator.Variables.ActionVariableDivide::.ctor()
extern void ActionVariableDivide__ctor_m352A7B3BF1303840F425FAC245914B733F446437 (void);
// 0x00000021 System.Boolean GameCreator.Variables.ActionVariableMath::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableMath_InstantExecute_m0F961A12FBB6616247EC5F19EAC4BDA85B5C899E (void);
// 0x00000022 System.Void GameCreator.Variables.ActionVariableMath::.ctor()
extern void ActionVariableMath__ctor_m1A0AEF910F1C40AF57D50058F6DAE8BF116C6A71 (void);
// 0x00000023 System.Boolean GameCreator.Variables.ActionVariableMultiply::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableMultiply_InstantExecute_m62C1DCA655EFC52799F4794AB586DAF616202C31 (void);
// 0x00000024 System.Void GameCreator.Variables.ActionVariableMultiply::.ctor()
extern void ActionVariableMultiply__ctor_m1868B94E5A2285C1B1C0C4DBCC4A3A75FB9845E3 (void);
// 0x00000025 System.Void GameCreator.Variables.ActionVariableOperationBase::.ctor()
extern void ActionVariableOperationBase__ctor_m59051C577B76627AA6926C51FC5E5C8E3F34D1E5 (void);
// 0x00000026 System.Boolean GameCreator.Variables.ActionVariableSubtract::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariableSubtract_InstantExecute_m7B71E5AE31AD790D5B4DCF69201AF546DA35EC6E (void);
// 0x00000027 System.Void GameCreator.Variables.ActionVariableSubtract::.ctor()
extern void ActionVariableSubtract__ctor_m710FFA32C444CF50A86C7253649F8554C4383684 (void);
// 0x00000028 System.Void GameCreator.Variables.VariableFilterAttribute::.ctor(GameCreator.Variables.Variable/DataType[])
extern void VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639 (void);
// 0x00000029 System.Object GameCreator.Variables.BaseHelperVariable::Get(UnityEngine.GameObject)
// 0x0000002A System.Void GameCreator.Variables.BaseHelperVariable::Set(System.Object,UnityEngine.GameObject)
// 0x0000002B System.String GameCreator.Variables.BaseHelperVariable::ToStringValue(UnityEngine.GameObject)
// 0x0000002C GameCreator.Variables.Variable/DataType GameCreator.Variables.BaseHelperVariable::GetDataType(UnityEngine.GameObject)
// 0x0000002D System.Void GameCreator.Variables.BaseHelperVariable::.ctor()
extern void BaseHelperVariable__ctor_mC05AF16A3BF8D2C30658398BD61FA555C960DB5B (void);
// 0x0000002E System.Void GameCreator.Variables.BaseProperty`1::.ctor()
// 0x0000002F System.Void GameCreator.Variables.BaseProperty`1::.ctor(T)
// 0x00000030 System.Void GameCreator.Variables.BaseProperty`1::SetupVariables()
// 0x00000031 T GameCreator.Variables.BaseProperty`1::GetValue()
// 0x00000032 T GameCreator.Variables.BaseProperty`1::GetValue(UnityEngine.GameObject)
// 0x00000033 System.String GameCreator.Variables.BaseProperty`1::ToString()
// 0x00000034 System.String GameCreator.Variables.BaseProperty`1::GetValueName()
// 0x00000035 System.Void GameCreator.Variables.BoolProperty::.ctor()
extern void BoolProperty__ctor_mDD02036E09F03E5BDB2B7787D3D830BBEB41B0A2 (void);
// 0x00000036 System.Void GameCreator.Variables.BoolProperty::.ctor(System.Boolean)
extern void BoolProperty__ctor_mF8DD9BE677D6ED39278DFEE77F880241E07FC162 (void);
// 0x00000037 System.Void GameCreator.Variables.ColorProperty::.ctor()
extern void ColorProperty__ctor_m48210CF165ABC115E44B994AB31E95ECFFB161A0 (void);
// 0x00000038 System.Void GameCreator.Variables.ColorProperty::.ctor(UnityEngine.Color)
extern void ColorProperty__ctor_m1E1901CDB0631C9B41070AC3C0E4D4A4ACFBA230 (void);
// 0x00000039 System.Void GameCreator.Variables.GameObjectProperty::.ctor()
extern void GameObjectProperty__ctor_m441BDB9AE7A0BE871EA35A739473BC12D754E5BB (void);
// 0x0000003A System.Void GameCreator.Variables.GameObjectProperty::.ctor(UnityEngine.GameObject)
extern void GameObjectProperty__ctor_m8C2E81F3F1AB5CCA22C730DB9E185142881D014B (void);
// 0x0000003B System.String GameCreator.Variables.GameObjectProperty::GetValueName()
extern void GameObjectProperty_GetValueName_mD0B2EE91E913D001E85174F66082C8CEFDDBFDC1 (void);
// 0x0000003C System.Object GameCreator.Variables.HelperGlobalVariable::Get(UnityEngine.GameObject)
extern void HelperGlobalVariable_Get_m09932ED7508E89B31F87B39BA7A8D203A6115729 (void);
// 0x0000003D System.Void GameCreator.Variables.HelperGlobalVariable::Set(System.Object,UnityEngine.GameObject)
extern void HelperGlobalVariable_Set_mC9297547BFD49326D1C69CA98AEC6236614C5548 (void);
// 0x0000003E System.String GameCreator.Variables.HelperGlobalVariable::ToString()
extern void HelperGlobalVariable_ToString_m0F6A621C5E4B18AE1C6FFA3773EB39D6AE2022F3 (void);
// 0x0000003F System.String GameCreator.Variables.HelperGlobalVariable::ToStringValue(UnityEngine.GameObject)
extern void HelperGlobalVariable_ToStringValue_m365C800DCDA1E958E092D96DBC6CF831380D621B (void);
// 0x00000040 GameCreator.Variables.Variable/DataType GameCreator.Variables.HelperGlobalVariable::GetDataType(UnityEngine.GameObject)
extern void HelperGlobalVariable_GetDataType_m8D5F57780ADA430B90028C20301D1EEF53520A17 (void);
// 0x00000041 System.Void GameCreator.Variables.HelperGlobalVariable::.ctor()
extern void HelperGlobalVariable__ctor_m677699EFE96DBF7A14A4790A51155CC09592DE2B (void);
// 0x00000042 System.Object GameCreator.Variables.HelperLocalVariable::Get(UnityEngine.GameObject)
extern void HelperLocalVariable_Get_m1C67AB1D2F517B291B70C5A594BE222CCBD69F7C (void);
// 0x00000043 System.Void GameCreator.Variables.HelperLocalVariable::Set(System.Object,UnityEngine.GameObject)
extern void HelperLocalVariable_Set_mFFAF517342EFC9CD90913A4C890988DD8191C316 (void);
// 0x00000044 UnityEngine.GameObject GameCreator.Variables.HelperLocalVariable::GetGameObject(UnityEngine.GameObject)
extern void HelperLocalVariable_GetGameObject_m8EF3CF852A8C9C5FE8BB41B04D6EA3FA9F8CF6D4 (void);
// 0x00000045 System.String GameCreator.Variables.HelperLocalVariable::ToString()
extern void HelperLocalVariable_ToString_m09E3A4752569C99C9D2EB5D09E3A3AE2558A9F41 (void);
// 0x00000046 System.String GameCreator.Variables.HelperLocalVariable::ToStringValue(UnityEngine.GameObject)
extern void HelperLocalVariable_ToStringValue_m0E19E255B70381F58243A4BDB85B90A26EC169A6 (void);
// 0x00000047 GameCreator.Variables.Variable/DataType GameCreator.Variables.HelperLocalVariable::GetDataType(UnityEngine.GameObject)
extern void HelperLocalVariable_GetDataType_m0CEC98FAB106DC2DB2C41890AFAE01042D2BF9B8 (void);
// 0x00000048 System.Void GameCreator.Variables.HelperLocalVariable::.ctor()
extern void HelperLocalVariable__ctor_mEED4EB403BBDA152A6723707E83103BA5E7AB44C (void);
// 0x00000049 System.Object GameCreator.Variables.HelperGetListVariable::Get(UnityEngine.GameObject)
extern void HelperGetListVariable_Get_m23119D24E6DF8890EDC12C82962DB9057D8FB06B (void);
// 0x0000004A System.Void GameCreator.Variables.HelperGetListVariable::Set(System.Object,UnityEngine.GameObject)
extern void HelperGetListVariable_Set_m146EEBE758BD1C3C2B4186EA75CA48CE7BBFEF9A (void);
// 0x0000004B UnityEngine.GameObject GameCreator.Variables.HelperGetListVariable::GetGameObject(UnityEngine.GameObject)
extern void HelperGetListVariable_GetGameObject_m92ECF2EA41A24A91AF957B3C6CA742675B98D3BB (void);
// 0x0000004C System.String GameCreator.Variables.HelperGetListVariable::ToString()
extern void HelperGetListVariable_ToString_m8F934406057E6057AF9A2A0D7B3A0744B978AE3E (void);
// 0x0000004D System.String GameCreator.Variables.HelperGetListVariable::ToStringValue(UnityEngine.GameObject)
extern void HelperGetListVariable_ToStringValue_mB1BE97493DD66093124FFAD15F41BBB89E90E1BE (void);
// 0x0000004E System.Void GameCreator.Variables.HelperGetListVariable::.ctor()
extern void HelperGetListVariable__ctor_mEDC502309303BB06450512CA31C1AD5EFD8B7FF1 (void);
// 0x0000004F GameCreator.Variables.ListVariables GameCreator.Variables.HelperListVariable::GetListVariables(UnityEngine.GameObject)
extern void HelperListVariable_GetListVariables_mD5A5C3B3F08C3C2EE90ACF48F9194741E6B04492 (void);
// 0x00000050 GameCreator.Variables.Variable/DataType GameCreator.Variables.HelperListVariable::GetDataType(UnityEngine.GameObject)
extern void HelperListVariable_GetDataType_mBF354A8AAA04424D530D9B7E5FB5DC396A25B33B (void);
// 0x00000051 System.String GameCreator.Variables.HelperListVariable::ToString()
extern void HelperListVariable_ToString_m665B505FAEBBCFF46029AE9288506DA7F81D8D32 (void);
// 0x00000052 System.Void GameCreator.Variables.HelperListVariable::.ctor()
extern void HelperListVariable__ctor_m88E797146ADAC261DC62C9FB87A100F2113F138B (void);
// 0x00000053 System.Void GameCreator.Variables.NumberProperty::.ctor()
extern void NumberProperty__ctor_mF2DA5C8020A19D44ABBA0835E7EA6814059A385B (void);
// 0x00000054 System.Void GameCreator.Variables.NumberProperty::.ctor(System.Single)
extern void NumberProperty__ctor_m4D9D33F2E42D3CC6D54B3895BA8AB58785F921E0 (void);
// 0x00000055 System.Int32 GameCreator.Variables.NumberProperty::GetInt(UnityEngine.GameObject)
extern void NumberProperty_GetInt_mCAD822FE8FADC604D44C3095B4BD2DAB0F2EF24C (void);
// 0x00000056 System.Void GameCreator.Variables.SpriteProperty::.ctor()
extern void SpriteProperty__ctor_m71EAE090E587CE6240224B262822680B351EBD89 (void);
// 0x00000057 System.Void GameCreator.Variables.SpriteProperty::.ctor(UnityEngine.Sprite)
extern void SpriteProperty__ctor_m92A3B6B6D47F2B0F36B75C91F8FFB6A2D5A1BFE3 (void);
// 0x00000058 System.Void GameCreator.Variables.StringProperty::.ctor()
extern void StringProperty__ctor_m81E59CBF6DA8CB77591CC9685A7C491DE4AF8E32 (void);
// 0x00000059 System.Void GameCreator.Variables.StringProperty::.ctor(System.String)
extern void StringProperty__ctor_m01EDACDB45370F73B9DD1A3C606A477C39C074B2 (void);
// 0x0000005A System.Void GameCreator.Variables.Texture2DProperty::.ctor()
extern void Texture2DProperty__ctor_m2B3F19141270905B8EB9F0EE752FB50BD3773F3D (void);
// 0x0000005B System.Void GameCreator.Variables.Texture2DProperty::.ctor(UnityEngine.Texture2D)
extern void Texture2DProperty__ctor_m3EEE53F595F373E105FEF1A4FFB4C46160E687BB (void);
// 0x0000005C System.Void GameCreator.Variables.VariableGlobalProperty::.ctor()
extern void VariableGlobalProperty__ctor_m24AA98E5A0156A3764CF12F4CC913A2A3466E234 (void);
// 0x0000005D System.Object GameCreator.Variables.VariableGlobalProperty::Get()
extern void VariableGlobalProperty_Get_mE71CA9062C1DD8E7AD4D42887960DC9FF2F2E9B2 (void);
// 0x0000005E System.Void GameCreator.Variables.VariableGlobalProperty::Set(System.Object)
extern void VariableGlobalProperty_Set_m43CCE4F0F8CA865AE2E06A827089EC3FDC3B5CE5 (void);
// 0x0000005F System.String GameCreator.Variables.VariableGlobalProperty::GetVariableID()
extern void VariableGlobalProperty_GetVariableID_mBB7DEEBEFC22E09734BB49DBAC8319A98E690BEC (void);
// 0x00000060 System.String GameCreator.Variables.VariableGlobalProperty::ToString()
extern void VariableGlobalProperty_ToString_m5E978A59AD3A437E5A732F2E22A1BA217FA132B2 (void);
// 0x00000061 System.String GameCreator.Variables.VariableGlobalProperty::ToStringValue()
extern void VariableGlobalProperty_ToStringValue_m08EFFBDE56E4A72B7D690A4DF8564B52785605D7 (void);
// 0x00000062 System.Void GameCreator.Variables.VariableProperty::.ctor()
extern void VariableProperty__ctor_mFDAF04955A1159213AE4F03503A3BB3AD9168A3B (void);
// 0x00000063 System.Void GameCreator.Variables.VariableProperty::.ctor(GameCreator.Variables.Variable/VarType)
extern void VariableProperty__ctor_m78325DD5CF084FD38CE44EE59EB80BDBE24C944F (void);
// 0x00000064 System.Void GameCreator.Variables.VariableProperty::.ctor(GameCreator.Variables.VariableProperty/GetVarType)
extern void VariableProperty__ctor_m2C83FF6B15BED75507FFE30E8C90EA0C2A52D340 (void);
// 0x00000065 System.Void GameCreator.Variables.VariableProperty::SetupVariables()
extern void VariableProperty_SetupVariables_m670C2D5B7042900C7795F0A515BBCE3CE961890B (void);
// 0x00000066 System.Object GameCreator.Variables.VariableProperty::Get(UnityEngine.GameObject)
extern void VariableProperty_Get_m57DF25DA48EEF019FEBB223C0BE503E686828D1B (void);
// 0x00000067 System.Void GameCreator.Variables.VariableProperty::Set(System.Object,UnityEngine.GameObject)
extern void VariableProperty_Set_m9CF11AB910494CCCFC50C5BC5D9D942D4A1DB896 (void);
// 0x00000068 System.String GameCreator.Variables.VariableProperty::GetVariableID()
extern void VariableProperty_GetVariableID_m30092B35E1DD0EECB4E67F7B53A80DD526F06089 (void);
// 0x00000069 GameCreator.Variables.Variable/VarType GameCreator.Variables.VariableProperty::GetVariableType()
extern void VariableProperty_GetVariableType_m0C09A17246022F05775F3B061A7A7DC418042EB6 (void);
// 0x0000006A UnityEngine.GameObject GameCreator.Variables.VariableProperty::GetLocalVariableGameObject()
extern void VariableProperty_GetLocalVariableGameObject_m6504A712133D2C0BABAD881CA4C52458D30D2093 (void);
// 0x0000006B UnityEngine.GameObject GameCreator.Variables.VariableProperty::GetListVariableGameObject()
extern void VariableProperty_GetListVariableGameObject_mE5B588D7C12BD2ACB67AFE4AB8E63F25845E3551 (void);
// 0x0000006C UnityEngine.GameObject GameCreator.Variables.VariableProperty::GetLocalVariableGameObject(UnityEngine.GameObject)
extern void VariableProperty_GetLocalVariableGameObject_mB54ABFA79491D60E0AB987CF51451B1BCA2A25AE (void);
// 0x0000006D UnityEngine.GameObject GameCreator.Variables.VariableProperty::GetListVariableGameObject(UnityEngine.GameObject)
extern void VariableProperty_GetListVariableGameObject_m23A6FDB73101A2F0F6FF1D4244FCF85BE1ACCC01 (void);
// 0x0000006E GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableProperty::GetVariableDataType(UnityEngine.GameObject)
extern void VariableProperty_GetVariableDataType_mB748F26EAFD5CF417F95C7A324156634008530F1 (void);
// 0x0000006F System.String GameCreator.Variables.VariableProperty::ToString()
extern void VariableProperty_ToString_m39957D30CAA647AEDBA0F72D011A03922F15BA8F (void);
// 0x00000070 System.String GameCreator.Variables.VariableProperty::ToStringValue(UnityEngine.GameObject)
extern void VariableProperty_ToStringValue_m25DE9AEBB563740777C7FE7C5CEBCD8F786189B9 (void);
// 0x00000071 System.Void GameCreator.Variables.Vector2Property::.ctor()
extern void Vector2Property__ctor_m80EC4749B26B1B876AA6B34235801A1CF320AE94 (void);
// 0x00000072 System.Void GameCreator.Variables.Vector2Property::.ctor(UnityEngine.Vector2)
extern void Vector2Property__ctor_m21BAD476D71BD0C1DC508656C351C74E308552E8 (void);
// 0x00000073 System.Void GameCreator.Variables.Vector3Property::.ctor()
extern void Vector3Property__ctor_m2B4DB21791503120E38667E22B000128306CA842 (void);
// 0x00000074 System.Void GameCreator.Variables.Vector3Property::.ctor(UnityEngine.Vector3)
extern void Vector3Property__ctor_mA5958966050D255F20DD61B47043B55F22092BEA (void);
// 0x00000075 System.Void GameCreator.Variables.GlobalVariables::.ctor()
extern void GlobalVariables__ctor_mD9B570C621BF1C71792B86A72A185DB6D5755DB8 (void);
// 0x00000076 System.Int32 GameCreator.Variables.ListVariables::get_iterator()
extern void ListVariables_get_iterator_mFCBB7595D4EA279F6B1F9C15FA22579B60672FA4 (void);
// 0x00000077 System.Void GameCreator.Variables.ListVariables::set_iterator(System.Int32)
extern void ListVariables_set_iterator_m74F41B75E46D982BD624A4E8D6EDD63FCE9235BB (void);
// 0x00000078 System.Void GameCreator.Variables.ListVariables::Start()
extern void ListVariables_Start_m7293DEC914CDF4263C509648EF8126C2664D988A (void);
// 0x00000079 GameCreator.Variables.Variable GameCreator.Variables.ListVariables::Get(System.Int32)
extern void ListVariables_Get_m559E86FD2BA087D3B899656AE7422EF9140DB578 (void);
// 0x0000007A GameCreator.Variables.Variable GameCreator.Variables.ListVariables::Get(GameCreator.Variables.ListVariables/Position,System.Int32)
extern void ListVariables_Get_m5BC953765178DC4752C2C1F850FAF94E6362C9C5 (void);
// 0x0000007B System.Void GameCreator.Variables.ListVariables::Push(System.Object,System.Int32)
extern void ListVariables_Push_m6EB7799F5CD5C2F9CD206DEABB8BCBF15810553E (void);
// 0x0000007C System.Void GameCreator.Variables.ListVariables::Push(System.Object,GameCreator.Variables.ListVariables/Position,System.Int32)
extern void ListVariables_Push_m0338EC7966E21DE313B8118B738887023032E128 (void);
// 0x0000007D System.Void GameCreator.Variables.ListVariables::Remove(System.Int32)
extern void ListVariables_Remove_mCB3A653187FBCAFC0D1063845A93024E56CA4C72 (void);
// 0x0000007E System.Void GameCreator.Variables.ListVariables::Remove(GameCreator.Variables.ListVariables/Position,System.Int32)
extern void ListVariables_Remove_m237E9B88D8A01A833A1C9E8E7244130007DE1B1D (void);
// 0x0000007F System.Void GameCreator.Variables.ListVariables::SetInterator(System.Int32)
extern void ListVariables_SetInterator_m27219638DAA711D47240C67B75316FD09620B582 (void);
// 0x00000080 System.Void GameCreator.Variables.ListVariables::NextIterator()
extern void ListVariables_NextIterator_m863D13A64AAA6536E7C5D9A58ED6561C76A62AFC (void);
// 0x00000081 System.Void GameCreator.Variables.ListVariables::PrevIterator()
extern void ListVariables_PrevIterator_m4E608D27743368534A59FC63BDE2623C7AB772C8 (void);
// 0x00000082 System.Void GameCreator.Variables.ListVariables::RequireInit(System.Boolean)
extern void ListVariables_RequireInit_mB23EB4548A5473EBC88ED2C594AED4D881988FC5 (void);
// 0x00000083 System.String GameCreator.Variables.ListVariables::GetUniqueName()
extern void ListVariables_GetUniqueName_m275227B73AE11BD0952DA8D5A92041C41569C6FF (void);
// 0x00000084 System.Object GameCreator.Variables.ListVariables::GetSaveData()
extern void ListVariables_GetSaveData_mC8E89C9E0E38529FCB838A461497ECA0E9C35204 (void);
// 0x00000085 System.Void GameCreator.Variables.ListVariables::ResetData()
extern void ListVariables_ResetData_m9BF9A95A3C6766B0B7759765D8CFCC5E0F052EB9 (void);
// 0x00000086 System.Void GameCreator.Variables.ListVariables::OnLoad(System.Object)
extern void ListVariables_OnLoad_m3F83AF182DF83CF5F7A982D9F6F157C45708D9DC (void);
// 0x00000087 System.Void GameCreator.Variables.ListVariables::.ctor()
extern void ListVariables__ctor_m76BF3F959915316C6B67FFAB63BD5A1A4E6EB19B (void);
// 0x00000088 GameCreator.Variables.Variable GameCreator.Variables.LocalVariables::Get(System.String)
extern void LocalVariables_Get_m79A91CAC485E5E280D3B59E2F6C7CA7185820130 (void);
// 0x00000089 System.Void GameCreator.Variables.LocalVariables::Start()
extern void LocalVariables_Start_m15EBF8F550EADAD705B26BE1E6EA2466475E09BE (void);
// 0x0000008A System.Void GameCreator.Variables.LocalVariables::Initialize(System.Boolean)
extern void LocalVariables_Initialize_m6E845686584BEFD153D4FC13A7AC2A5457A87759 (void);
// 0x0000008B System.Void GameCreator.Variables.LocalVariables::OnDestroy()
extern void LocalVariables_OnDestroy_mD21BD0AF00ACB5BF31103996D085EB69B09FB2FB (void);
// 0x0000008C System.Void GameCreator.Variables.LocalVariables::RequireInit(System.Boolean)
extern void LocalVariables_RequireInit_m902194544F7CB798E1307655766C4106D42E3DF7 (void);
// 0x0000008D System.String GameCreator.Variables.LocalVariables::GetUniqueName()
extern void LocalVariables_GetUniqueName_mE74EB27002380DA06B6A267A908C7CCFD4AE1EF5 (void);
// 0x0000008E System.Type GameCreator.Variables.LocalVariables::GetSaveDataType()
extern void LocalVariables_GetSaveDataType_m23622054DE35D3ECF261FBDAF766F929A91E864D (void);
// 0x0000008F System.Object GameCreator.Variables.LocalVariables::GetSaveData()
extern void LocalVariables_GetSaveData_m5CBDCC8EEBEC857BE4923FE4D79099D4AE294E5E (void);
// 0x00000090 System.Void GameCreator.Variables.LocalVariables::ResetData()
extern void LocalVariables_ResetData_mEA858173E3CD522F785DD14C2565BA90D9D6F158 (void);
// 0x00000091 System.Void GameCreator.Variables.LocalVariables::OnLoad(System.Object)
extern void LocalVariables_OnLoad_m6D9641956A069EEC6E1C9E5289F374F71CF06E63 (void);
// 0x00000092 System.Void GameCreator.Variables.LocalVariables::.ctor()
extern void LocalVariables__ctor_m2F28D428E723D3C019344AE9B93871E311262242 (void);
// 0x00000093 System.Void GameCreator.Variables.LocalVariables::.cctor()
extern void LocalVariables__cctor_m754F3A7DF3DF2050A8FC6BBA0E659871E607912B (void);
// 0x00000094 System.Void GameCreator.Variables.TextVariable::Awake()
extern void TextVariable_Awake_m18990994AA54705B5C6128C95EA0820388728FF2 (void);
// 0x00000095 System.Void GameCreator.Variables.TextVariable::OnEnable()
extern void TextVariable_OnEnable_mEE2AAC94BDA99CD43BBEC806BD3ECF4B0EB462CF (void);
// 0x00000096 System.Void GameCreator.Variables.TextVariable::OnDisable()
extern void TextVariable_OnDisable_m27C93BA14CDDF48558E4177D0A685A69DF7402A6 (void);
// 0x00000097 System.Void GameCreator.Variables.TextVariable::OnDestroy()
extern void TextVariable_OnDestroy_m535255D7234ED8625720FB2BFB4932F2303D8639 (void);
// 0x00000098 System.Void GameCreator.Variables.TextVariable::OnApplicationQuit()
extern void TextVariable_OnApplicationQuit_m5AF72C397D98CC153FBEEBF179F035C33C84AEB7 (void);
// 0x00000099 System.Void GameCreator.Variables.TextVariable::OnUpdateVariable(System.String)
extern void TextVariable_OnUpdateVariable_m8B62A491F12BFAAB44A7A4AA2B54BCD0DF97129B (void);
// 0x0000009A System.Void GameCreator.Variables.TextVariable::OnUpdateList(System.Int32,System.Object,System.Object)
extern void TextVariable_OnUpdateList_m30346CD773390BEB688D41FC19AEAE14A990A2FF (void);
// 0x0000009B System.Void GameCreator.Variables.TextVariable::UpdateText()
extern void TextVariable_UpdateText_m4DD81C187B3D0E6168C6DBCE9574FB16A20B8DF0 (void);
// 0x0000009C System.Void GameCreator.Variables.TextVariable::SubscribeOnChange()
extern void TextVariable_SubscribeOnChange_mFA8CFFB06C127CDC9C237EEC002188C128E558B6 (void);
// 0x0000009D System.Void GameCreator.Variables.TextVariable::UnsubscribeOnChange()
extern void TextVariable_UnsubscribeOnChange_mC5A8F4E5B305FB439C147AE43E1F828C31C97760 (void);
// 0x0000009E System.Void GameCreator.Variables.TextVariable::.ctor()
extern void TextVariable__ctor_mFDBE5D6EFE7CBCE6D6B224FB43870911C8684A39 (void);
// 0x0000009F System.Boolean GameCreator.Variables.ConditionGameObjectInList::Check(UnityEngine.GameObject)
extern void ConditionGameObjectInList_Check_m66B0E5D6C9202DABCCD2110855086BC13225F9E5 (void);
// 0x000000A0 System.Void GameCreator.Variables.ConditionGameObjectInList::.ctor()
extern void ConditionGameObjectInList__ctor_mD9FC8A0B1F1CE89BE0683026F9D6D56D74048212 (void);
// 0x000000A1 System.Boolean GameCreator.Variables.ConditionListVariableCount::Check(UnityEngine.GameObject)
extern void ConditionListVariableCount_Check_mA7A076F6E247B3D4BA57881647EE5198F0667E99 (void);
// 0x000000A2 System.Void GameCreator.Variables.ConditionListVariableCount::.ctor()
extern void ConditionListVariableCount__ctor_mAA8B24AC2FF3482C25FCD8B819B7416A08479857 (void);
// 0x000000A3 GameCreator.Variables.GlobalVariables GameCreator.Variables.DatabaseVariables::GetGlobalVariables()
extern void DatabaseVariables_GetGlobalVariables_m98365648B2EDEC62AB66B21DC0B6D612B3046C45 (void);
// 0x000000A4 GameCreator.Variables.DatabaseVariables GameCreator.Variables.DatabaseVariables::Load()
extern void DatabaseVariables_Load_m960CB53FAC00CCE45FABDDC7F143A5E1C7ED6060 (void);
// 0x000000A5 System.Void GameCreator.Variables.DatabaseVariables::.ctor()
extern void DatabaseVariables__ctor_m824A52530C6D789D7EF749889EA4EF8E34FE4D8D (void);
// 0x000000A6 System.Void GameCreator.Variables.DatabaseVariables/Container::.ctor()
extern void Container__ctor_m6AFEE4B02BA2B87B3C94C41237E56E56ED937E45 (void);
// 0x000000A7 System.Void GameCreator.Variables.MBVariable::.ctor()
extern void MBVariable__ctor_m7D2CC7BBB0DF3A5D50E01D40448B13B5E42230BC (void);
// 0x000000A8 System.Void GameCreator.Variables.SOVariable::.ctor()
extern void SOVariable__ctor_mB6E1D836299CC14249E98157E7E8E040E1FE807A (void);
// 0x000000A9 System.Void GameCreator.Variables.GlobalVariablesManager::InitializeOnLoad()
extern void GlobalVariablesManager_InitializeOnLoad_mA313C49AF61E1FD4278C32C60581918067C63663 (void);
// 0x000000AA System.Void GameCreator.Variables.GlobalVariablesManager::OnCreate()
extern void GlobalVariablesManager_OnCreate_mAA730EB526E0F24283821CA077A2CA3B065D6CF4 (void);
// 0x000000AB GameCreator.Variables.Variable GameCreator.Variables.GlobalVariablesManager::Get(System.String)
extern void GlobalVariablesManager_Get_m526D9624ADC1CA8923484924C1844ABCBAE7276A (void);
// 0x000000AC System.String[] GameCreator.Variables.GlobalVariablesManager::GetNames()
extern void GlobalVariablesManager_GetNames_m579B393A8C54B2DBB22845C7E19D608D17AB1A9E (void);
// 0x000000AD System.Void GameCreator.Variables.GlobalVariablesManager::RequireInit(System.Boolean)
extern void GlobalVariablesManager_RequireInit_mB7C4D1035738D10D755B3845CD01610878817992 (void);
// 0x000000AE System.String GameCreator.Variables.GlobalVariablesManager::GetUniqueName()
extern void GlobalVariablesManager_GetUniqueName_m1282AF44B2959018F22207FDDAC91BD155759865 (void);
// 0x000000AF System.Type GameCreator.Variables.GlobalVariablesManager::GetSaveDataType()
extern void GlobalVariablesManager_GetSaveDataType_m0700935F0830BB528384DEF5BF6A3EE3885BDA1C (void);
// 0x000000B0 System.Object GameCreator.Variables.GlobalVariablesManager::GetSaveData()
extern void GlobalVariablesManager_GetSaveData_m550C93A75DD69C570E3FCDADFF711C493D5E0864 (void);
// 0x000000B1 System.Void GameCreator.Variables.GlobalVariablesManager::ResetData()
extern void GlobalVariablesManager_ResetData_mC1CE993D8DDA689A05B2BCFEC4EF45E057999653 (void);
// 0x000000B2 System.Void GameCreator.Variables.GlobalVariablesManager::OnLoad(System.Object)
extern void GlobalVariablesManager_OnLoad_m255D02BED6D080363C9B13822D132E47D797DC2A (void);
// 0x000000B3 System.Void GameCreator.Variables.GlobalVariablesManager::.ctor()
extern void GlobalVariablesManager__ctor_mF8E100FDC1AE9838850F7BF3C8DE469120143BF0 (void);
// 0x000000B4 System.Void GameCreator.Variables.VariablesManager::Reset()
extern void VariablesManager_Reset_m0FCF892148B5144A0261ADC0033C40438100EB20 (void);
// 0x000000B5 System.Object GameCreator.Variables.VariablesManager::GetGlobal(System.String)
extern void VariablesManager_GetGlobal_m348BC7578CA4AB0CB6191DCB42E2A308C089B7E0 (void);
// 0x000000B6 System.Object GameCreator.Variables.VariablesManager::GetLocal(UnityEngine.GameObject,System.String,System.Boolean)
extern void VariablesManager_GetLocal_mE9DFD0DA22A3FECC7A2EC5B80D7835FD7DA2A823 (void);
// 0x000000B7 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariablesManager::GetGlobalType(System.String,System.Boolean)
extern void VariablesManager_GetGlobalType_m2EE9E879DE7D0DFCA3E690E0383594FC01B3CDB8 (void);
// 0x000000B8 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariablesManager::GetLocalType(UnityEngine.GameObject,System.String,System.Boolean)
extern void VariablesManager_GetLocalType_mDE2BB1B779A51C2F498496222FDDC6E227675F70 (void);
// 0x000000B9 GameCreator.Variables.Variable GameCreator.Variables.VariablesManager::GetListItem(UnityEngine.GameObject,GameCreator.Variables.ListVariables/Position,System.Int32)
extern void VariablesManager_GetListItem_m0B4E7AE903DAEFD19B736DECD80DFB1DCD3610A6 (void);
// 0x000000BA GameCreator.Variables.Variable GameCreator.Variables.VariablesManager::GetListItem(GameCreator.Variables.ListVariables,GameCreator.Variables.ListVariables/Position,System.Int32)
extern void VariablesManager_GetListItem_m531AA9CE909A9533F7BF47869268BBD30FD76C72 (void);
// 0x000000BB System.Int32 GameCreator.Variables.VariablesManager::GetListCount(UnityEngine.GameObject)
extern void VariablesManager_GetListCount_m0432310633C87037D7A5598C94E3DE3814B7EFFE (void);
// 0x000000BC System.Int32 GameCreator.Variables.VariablesManager::GetListCount(GameCreator.Variables.ListVariables)
extern void VariablesManager_GetListCount_mA08E77E836E7071650705281A0B30514E8DFD211 (void);
// 0x000000BD System.Void GameCreator.Variables.VariablesManager::SetGlobal(System.String,System.Object)
extern void VariablesManager_SetGlobal_mFDB69F586A4295F3098F8F4D556457F9D666D4A6 (void);
// 0x000000BE System.Void GameCreator.Variables.VariablesManager::SetLocal(UnityEngine.GameObject,System.String,System.Object,System.Boolean)
extern void VariablesManager_SetLocal_mACFFD07C40436B8016966EA392CB1197E7E8B77C (void);
// 0x000000BF System.Void GameCreator.Variables.VariablesManager::ListPush(UnityEngine.GameObject,System.Int32,System.Object)
extern void VariablesManager_ListPush_mDA6C63F351F7859BE4E3C5DDD3D80E41E5222DB1 (void);
// 0x000000C0 System.Void GameCreator.Variables.VariablesManager::ListPush(GameCreator.Variables.ListVariables,System.Int32,System.Object)
extern void VariablesManager_ListPush_m068822A2F5DC31C4812A7F16C6C8AEF5628D8D7E (void);
// 0x000000C1 System.Void GameCreator.Variables.VariablesManager::ListPush(UnityEngine.GameObject,GameCreator.Variables.ListVariables/Position,System.Object)
extern void VariablesManager_ListPush_m37130CD8F7E639BF5E08DD225CB2E7EDE66257AC (void);
// 0x000000C2 System.Void GameCreator.Variables.VariablesManager::ListPush(GameCreator.Variables.ListVariables,GameCreator.Variables.ListVariables/Position,System.Object)
extern void VariablesManager_ListPush_mD9D5B3C603A1E55AAB930F2137C2EED75120B11A (void);
// 0x000000C3 System.Void GameCreator.Variables.VariablesManager::ListRemove(UnityEngine.GameObject,System.Int32)
extern void VariablesManager_ListRemove_mFB1007A1F40A93A03F3049B4AAA53259DF5BAAF0 (void);
// 0x000000C4 System.Void GameCreator.Variables.VariablesManager::ListRemove(GameCreator.Variables.ListVariables,System.Int32)
extern void VariablesManager_ListRemove_m75518EAF8D13D873B208CF5FB431F54CB24861BB (void);
// 0x000000C5 System.Void GameCreator.Variables.VariablesManager::ListRemove(UnityEngine.GameObject,GameCreator.Variables.ListVariables/Position)
extern void VariablesManager_ListRemove_mBBDFED8C965C2F2CB866328C39B23491E9347C8B (void);
// 0x000000C6 System.Void GameCreator.Variables.VariablesManager::ListRemove(GameCreator.Variables.ListVariables,GameCreator.Variables.ListVariables/Position)
extern void VariablesManager_ListRemove_mF800A390D08F3BE97385930D4EB01066ADB7095F (void);
// 0x000000C7 System.Void GameCreator.Variables.VariablesManager::ListClear(UnityEngine.GameObject)
extern void VariablesManager_ListClear_mACE19808E051CEF677FED65DDC6D287678C1E4F6 (void);
// 0x000000C8 System.Void GameCreator.Variables.VariablesManager::ListClear(GameCreator.Variables.ListVariables)
extern void VariablesManager_ListClear_m87A063B43C39DCC97075AF024FCCA95169F71239 (void);
// 0x000000C9 System.Boolean GameCreator.Variables.VariablesManager::ExistsGlobal(System.String)
extern void VariablesManager_ExistsGlobal_mE67CEAA71A6D7BCFCA16E43CD258A5217523C26E (void);
// 0x000000CA System.Boolean GameCreator.Variables.VariablesManager::ExistsLocal(UnityEngine.GameObject,System.String,System.Boolean)
extern void VariablesManager_ExistsLocal_mEADE537B9C398C2AE13FB4BC23436D269A03655C (void);
// 0x000000CB System.Void GameCreator.Variables.VariablesManager::.cctor()
extern void VariablesManager__cctor_m535862B81D0BEA0CCD4C77A0E2EAD076D4C2ACD4 (void);
// 0x000000CC System.Void GameCreator.Variables.HexColor::.ctor()
extern void HexColor__ctor_m4B2A07A8A2407596B23FF262832968F9E25DA9A2 (void);
// 0x000000CD System.Void GameCreator.Variables.HexColor::.ctor(System.String)
extern void HexColor__ctor_m824844D36955C2C8A7EE6D00B1D5DCDB6F58A93E (void);
// 0x000000CE UnityEngine.Color GameCreator.Variables.HexColor::GetColor()
extern void HexColor_GetColor_m883288BA0EFCA4647652B3084166ADD0D7F75B10 (void);
// 0x000000CF System.Void GameCreator.Variables.GlobalTags::.ctor()
extern void GlobalTags__ctor_mF8F9A5C1B66787F4C960A3D0EDEEF68DB547A658 (void);
// 0x000000D0 System.Void GameCreator.Variables.Tag::.ctor()
extern void Tag__ctor_mAF76928BBE05D9258FB940356ACD82285A15994B (void);
// 0x000000D1 System.Void GameCreator.Variables.Tag::.ctor(System.String,System.Int32)
extern void Tag__ctor_m20CC7478EA6D0BA45B024D3DCC2F5854BA90E26C (void);
// 0x000000D2 UnityEngine.Color GameCreator.Variables.Tag::GetColor()
extern void Tag_GetColor_m8CFBD2A88548D691DC2D03CDC847217B1044538B (void);
// 0x000000D3 System.Void GameCreator.Variables.Tag::.cctor()
extern void Tag__cctor_m60B14CB5DD395F76FE50526492E6FF17AE72DC36 (void);
// 0x000000D4 System.Void GameCreator.Variables.Variable::.ctor()
extern void Variable__ctor_m817707EB54CA671257646F968DFBC6311BA1CB50 (void);
// 0x000000D5 System.Void GameCreator.Variables.Variable::.ctor(GameCreator.Variables.Variable)
extern void Variable__ctor_mCDEAA96C0A3B4AE5C56EC53AE40D5AAFA8A8A92A (void);
// 0x000000D6 System.Void GameCreator.Variables.Variable::.ctor(System.String,GameCreator.Variables.Variable/DataType,System.Object,System.Boolean)
extern void Variable__ctor_m3AF3D709DAD361AAD55866002671FCA6AB7138A4 (void);
// 0x000000D7 System.Object GameCreator.Variables.Variable::Get()
extern void Variable_Get_m33D7E38A01DBBB67C6FFE3EC778FB0E2E9909EF5 (void);
// 0x000000D8 T GameCreator.Variables.Variable::Get()
// 0x000000D9 System.Boolean GameCreator.Variables.Variable::CanSave()
extern void Variable_CanSave_m69D5A9AE48E079EF91855720DF131BF4D13CF2F9 (void);
// 0x000000DA System.Boolean GameCreator.Variables.Variable::CanSave(GameCreator.Variables.Variable/DataType)
extern void Variable_CanSave_m70EBE2B163489AAEB375BE8C180F8C855928C6A0 (void);
// 0x000000DB System.Void GameCreator.Variables.Variable::Set(GameCreator.Variables.Variable/DataType,System.Object)
extern void Variable_Set_m5284CBAFDE5E8B84714FB056194A38C8258682E5 (void);
// 0x000000DC System.Void GameCreator.Variables.Variable::Update(System.Object)
extern void Variable_Update_m92DC0E304AF020229E3CDDE5A981B5E926CDEC7E (void);
// 0x000000DD System.Boolean GameCreator.Variables.VariableBase::CanSave()
extern void VariableBase_CanSave_mE790667FC1D5CEAC939E74AECE4B4E4D8EC2FAD4 (void);
// 0x000000DE System.Boolean GameCreator.Variables.VariableBase::CanSaveType(GameCreator.Variables.Variable/DataType)
extern void VariableBase_CanSaveType_mFD9709A8A029B318A965FB77355F44B341075E74 (void);
// 0x000000DF GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableBase::GetDataType()
extern void VariableBase_GetDataType_m0D1D9E35E0E9E0A4659B2E6C0188E5F5ED7A41BB (void);
// 0x000000E0 System.Void GameCreator.Variables.VariableBase::.ctor()
extern void VariableBase__ctor_mD74869CAC9ACAFC053BE41A9BDCD9610807D559D (void);
// 0x000000E1 System.Void GameCreator.Variables.VariableBol::.ctor()
extern void VariableBol__ctor_m88C0D20785A31263BF75FD6BDC66EE12AC245A08 (void);
// 0x000000E2 System.Void GameCreator.Variables.VariableBol::.ctor(System.Boolean)
extern void VariableBol__ctor_m97A8D03588FB356482C37D783492A787A8144EDB (void);
// 0x000000E3 System.Boolean GameCreator.Variables.VariableBol::CanSave()
extern void VariableBol_CanSave_m51164FB1A2B47EB2C976556FDE394AB71CFE88C0 (void);
// 0x000000E4 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableBol::GetDataType()
extern void VariableBol_GetDataType_m686144EE00FDB38454D0D63F507E5460DB6A9DE9 (void);
// 0x000000E5 System.Void GameCreator.Variables.VariableCol::.ctor()
extern void VariableCol__ctor_mDB8D18CBB79A4571E9DDF496973D0747475E60B2 (void);
// 0x000000E6 System.Void GameCreator.Variables.VariableCol::.ctor(UnityEngine.Color)
extern void VariableCol__ctor_mDFF47CAD8264BE947A3D6D56E9F607B5D9F7534A (void);
// 0x000000E7 System.Boolean GameCreator.Variables.VariableCol::CanSave()
extern void VariableCol_CanSave_m96C81C94708E64AA4371D14B41DEFC77770FA5F6 (void);
// 0x000000E8 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableCol::GetDataType()
extern void VariableCol_GetDataType_mA2CF7EF6F4A1CBC62840977F07AFE87C70ED65E6 (void);
// 0x000000E9 System.Void GameCreator.Variables.VariableGeneric`1::.ctor(T)
// 0x000000EA T GameCreator.Variables.VariableGeneric`1::Get()
// 0x000000EB System.Void GameCreator.Variables.VariableGeneric`1::Set(T)
// 0x000000EC GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableGeneric`1::GetDataType()
// 0x000000ED System.Void GameCreator.Variables.VariableNum::.ctor()
extern void VariableNum__ctor_m11B275ECAEB20E83BD0D628655CB0C8D25C6E116 (void);
// 0x000000EE System.Void GameCreator.Variables.VariableNum::.ctor(System.Single)
extern void VariableNum__ctor_mA917E28D18A53DA1814E6F346B5A402D7EA2D9BB (void);
// 0x000000EF System.Boolean GameCreator.Variables.VariableNum::CanSave()
extern void VariableNum_CanSave_m2BD77DABB784DCC5C70765CC947B2B062B578D54 (void);
// 0x000000F0 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableNum::GetDataType()
extern void VariableNum_GetDataType_mFC6056B0E7F32EB2C786D2242D1280967CA12C01 (void);
// 0x000000F1 System.Void GameCreator.Variables.VariableObj::.ctor()
extern void VariableObj__ctor_mA3F3125C0D19BF62A6E46E2556B00C5ED0BBF30B (void);
// 0x000000F2 System.Void GameCreator.Variables.VariableObj::.ctor(UnityEngine.GameObject)
extern void VariableObj__ctor_mDD01DD0970C8FE41C5D2AABD164C025ABD00100B (void);
// 0x000000F3 System.Boolean GameCreator.Variables.VariableObj::CanSave()
extern void VariableObj_CanSave_m5DD3F37A5A7834EEE3D2417EB4DF8DF60F522BEB (void);
// 0x000000F4 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableObj::GetDataType()
extern void VariableObj_GetDataType_mD861F6513A055C6E15B126A8D066026AEF38041D (void);
// 0x000000F5 System.Void GameCreator.Variables.VariableSpr::.ctor()
extern void VariableSpr__ctor_mBC01DBBBA8010E6277C17066DFCFBE2AAB2AB6BB (void);
// 0x000000F6 System.Void GameCreator.Variables.VariableSpr::.ctor(UnityEngine.Sprite)
extern void VariableSpr__ctor_m6348CE3FEF08A60F048CEB44487B71C890A8F954 (void);
// 0x000000F7 System.Boolean GameCreator.Variables.VariableSpr::CanSave()
extern void VariableSpr_CanSave_mF20ACD13BBDE9DD09E07D0174B1E6B4ECF49AA37 (void);
// 0x000000F8 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableSpr::GetDataType()
extern void VariableSpr_GetDataType_m0B8DF3DE28900437121861774E9D4589C92C2586 (void);
// 0x000000F9 System.Void GameCreator.Variables.VariableStr::.ctor()
extern void VariableStr__ctor_mD10D69670D205789AFD1DC01A0B4A960E9F9B24E (void);
// 0x000000FA System.Void GameCreator.Variables.VariableStr::.ctor(System.String)
extern void VariableStr__ctor_mA1A979C0417ABCEE3F27F995E800B4AABC57063C (void);
// 0x000000FB System.Boolean GameCreator.Variables.VariableStr::CanSave()
extern void VariableStr_CanSave_m2EF95118B3A20089D6C84F90936D5D62A1064052 (void);
// 0x000000FC GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableStr::GetDataType()
extern void VariableStr_GetDataType_mCA1B669BA93CD25B8633ADAE70E71D429854B31F (void);
// 0x000000FD System.Void GameCreator.Variables.VariableTxt::.ctor()
extern void VariableTxt__ctor_m1F13B89995D702CC5EF7945265042D8654018364 (void);
// 0x000000FE System.Void GameCreator.Variables.VariableTxt::.ctor(UnityEngine.Texture2D)
extern void VariableTxt__ctor_mF36548D1B226FEA766DE092432F46352C11E6BEB (void);
// 0x000000FF System.Boolean GameCreator.Variables.VariableTxt::CanSave()
extern void VariableTxt_CanSave_mF194F50389C2F319D02D40AB369F77D321F8EAA9 (void);
// 0x00000100 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableTxt::GetDataType()
extern void VariableTxt_GetDataType_mEC8E495DAB13BE3BF9FD76501280B4FA8BB5D520 (void);
// 0x00000101 System.Void GameCreator.Variables.VariableVc2::.ctor()
extern void VariableVc2__ctor_m66E57DC33A2253378AA667BF8F20C058BCCE16AF (void);
// 0x00000102 System.Void GameCreator.Variables.VariableVc2::.ctor(UnityEngine.Vector2)
extern void VariableVc2__ctor_mC84CC79F255AD77392ACC2C16F704CF87F0C5A5F (void);
// 0x00000103 System.Boolean GameCreator.Variables.VariableVc2::CanSave()
extern void VariableVc2_CanSave_m7DB531418591C0C9F4E75C81D287D3130071A203 (void);
// 0x00000104 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableVc2::GetDataType()
extern void VariableVc2_GetDataType_m651023A2E794E459138EDE48AA90D7EC3B27B7E1 (void);
// 0x00000105 System.Void GameCreator.Variables.VariableVc3::.ctor()
extern void VariableVc3__ctor_mCE6657CCD45DDF19A10599F71E3D487D89AE027C (void);
// 0x00000106 System.Void GameCreator.Variables.VariableVc3::.ctor(UnityEngine.Vector3)
extern void VariableVc3__ctor_mB27B168CB513854E80D390AC8A94E3B2EB0E37F8 (void);
// 0x00000107 System.Boolean GameCreator.Variables.VariableVc3::CanSave()
extern void VariableVc3_CanSave_m76FACBD873474548E93EDDD8E86206E06CA6CB62 (void);
// 0x00000108 GameCreator.Variables.Variable/DataType GameCreator.Variables.VariableVc3::GetDataType()
extern void VariableVc3_GetDataType_m23DE8E822DFA6CF344CA0001E4D89C306710EC84 (void);
// 0x00000109 GameCreator.Variables.Variable GameCreator.Variables.GlobalVariablesUtilities::Get(System.String)
extern void GlobalVariablesUtilities_Get_m3FD248BBABD3D52B50C3CB414AEA4EC735DEFF25 (void);
// 0x0000010A GameCreator.Variables.Variable GameCreator.Variables.LocalVariablesUtilities::Get(UnityEngine.GameObject,System.String,System.Boolean)
extern void LocalVariablesUtilities_Get_m68458BF76450E25497630F89AE4489483546B17E (void);
// 0x0000010B GameCreator.Variables.Variable GameCreator.Variables.LocalVariablesUtilities::GetSingle(UnityEngine.GameObject,System.String,System.Boolean)
extern void LocalVariablesUtilities_GetSingle_mE71E316A5E101C34CA707FC4ADE15592100B2605 (void);
// 0x0000010C GameCreator.Variables.LocalVariables[] GameCreator.Variables.LocalVariablesUtilities::GatherLocals(UnityEngine.GameObject,System.Boolean)
extern void LocalVariablesUtilities_GatherLocals_m78BDC7157D713265B4A94BB8ED467AD623F177A8 (void);
// 0x0000010D System.Void GameCreator.Variables.LocalVariablesUtilities::.cctor()
extern void LocalVariablesUtilities__cctor_mEA8F98AF672C2406DB96DFC614A75BADA2120ED8 (void);
// 0x0000010E System.Void GameCreator.Variables.VariablesEvents::.ctor()
extern void VariablesEvents__ctor_mFCC31E1E935AFA9D37300DF5A046947D223ACC85 (void);
// 0x0000010F System.Void GameCreator.Variables.VariablesEvents::OnChangeGlobal(System.String)
extern void VariablesEvents_OnChangeGlobal_m957DDAD89B0BB71942C50615D38A178AFA25C9B3 (void);
// 0x00000110 System.Void GameCreator.Variables.VariablesEvents::OnChangeLocal(UnityEngine.GameObject,System.String)
extern void VariablesEvents_OnChangeLocal_m4A4607D6655100CFD624CC5E8B9FE3C022A5F722 (void);
// 0x00000111 System.Void GameCreator.Variables.VariablesEvents::OnListChange(UnityEngine.GameObject,System.Int32,System.Object,System.Object)
extern void VariablesEvents_OnListChange_m856A41DBF9BAD81480C14F1F390B6FC49848D1DA (void);
// 0x00000112 System.Void GameCreator.Variables.VariablesEvents::OnListAdd(UnityEngine.GameObject,System.Int32,System.Object)
extern void VariablesEvents_OnListAdd_m617D01F7DFC6163622A4AA354B0B22ED68AB3557 (void);
// 0x00000113 System.Void GameCreator.Variables.VariablesEvents::OnListRemove(UnityEngine.GameObject,System.Int32,System.Object)
extern void VariablesEvents_OnListRemove_m0E489D41716BBAA0149530347950FE940CF0A6E0 (void);
// 0x00000114 System.Void GameCreator.Variables.VariablesEvents::SetOnChangeGlobal(UnityEngine.Events.UnityAction`1<System.String>,System.String)
extern void VariablesEvents_SetOnChangeGlobal_m1C1C4D14993A2F7F40B63D767810E61E63EC3394 (void);
// 0x00000115 System.Void GameCreator.Variables.VariablesEvents::SetOnChangeLocal(UnityEngine.Events.UnityAction`1<System.String>,UnityEngine.GameObject,System.String)
extern void VariablesEvents_SetOnChangeLocal_mD93F9D3C765CFDFD724367771F90F06D7F7CAE77 (void);
// 0x00000116 System.Void GameCreator.Variables.VariablesEvents::StartListenListAny(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StartListenListAny_mE6F20D47D8B1E48E6B9AEB39F2B90BD220536941 (void);
// 0x00000117 System.Void GameCreator.Variables.VariablesEvents::StartListenListChg(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StartListenListChg_m55B6C4F1523806096A727C34D8768459EE293340 (void);
// 0x00000118 System.Void GameCreator.Variables.VariablesEvents::StartListenListAdd(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StartListenListAdd_m41837AADE9B9A0BB707EB2545F51DD0993AD2219 (void);
// 0x00000119 System.Void GameCreator.Variables.VariablesEvents::StartListenListRmv(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StartListenListRmv_m4291D723AC8E9DBB0FA1D8BC43D656783997E6CC (void);
// 0x0000011A System.Void GameCreator.Variables.VariablesEvents::RemoveChangeGlobal(UnityEngine.Events.UnityAction`1<System.String>,System.String)
extern void VariablesEvents_RemoveChangeGlobal_m1BAB1DFDBE128CA5CD9DB2D9C501F281CDD85A34 (void);
// 0x0000011B System.Void GameCreator.Variables.VariablesEvents::RemoveChangeLocal(UnityEngine.Events.UnityAction`1<System.String>,UnityEngine.GameObject,System.String)
extern void VariablesEvents_RemoveChangeLocal_mF37775B101909670805DA226EB614922ADB2A5EC (void);
// 0x0000011C System.Void GameCreator.Variables.VariablesEvents::StopListenListAny(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StopListenListAny_mAB890A9E9C9064D306E7F7534F46789E6223ACB0 (void);
// 0x0000011D System.Void GameCreator.Variables.VariablesEvents::StopListenListChg(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StopListenListChg_m8F9D18F05B2D3466A82CDE1E340122E855C464CC (void);
// 0x0000011E System.Void GameCreator.Variables.VariablesEvents::StopListenListAdd(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StopListenListAdd_m6F1A8054B5EC6EA7DDBBD2CC9D02D49B870B4AC9 (void);
// 0x0000011F System.Void GameCreator.Variables.VariablesEvents::StopListenListRmv(UnityEngine.Events.UnityAction`3<System.Int32,System.Object,System.Object>,UnityEngine.GameObject)
extern void VariablesEvents_StopListenListRmv_mE09281BC22EAC3C23A0CB96BBAC1D868C694BC21 (void);
// 0x00000120 System.String GameCreator.Variables.VariablesEvents::GetLocalID(UnityEngine.GameObject,System.String)
extern void VariablesEvents_GetLocalID_m1F2A3819BEE241EE46E65BA00CEAB53110208533 (void);
// 0x00000121 System.String GameCreator.Variables.VariablesEvents::GetListID(UnityEngine.GameObject)
extern void VariablesEvents_GetListID_m86995987EAD00B588811B0ED7AB2637E5823EDAD (void);
// 0x00000122 System.Void GameCreator.Variables.VariablesEvents/VarEvent::.ctor()
extern void VarEvent__ctor_mFACCCD91990D9181703ED69D2831210F3ED63A4C (void);
// 0x00000123 System.Void GameCreator.Variables.VariablesEvents/ListEvent::.ctor()
extern void ListEvent__ctor_mACD2CDF01C6E809E6CD98FB14EE3BF839AD5C909 (void);
// 0x00000124 System.Collections.IEnumerator GameCreator.Messages.ActionFloatingMessage::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionFloatingMessage_Execute_m5DB2F53398A5D023EA3DAB96EACBBBE242D448F0 (void);
// 0x00000125 System.Void GameCreator.Messages.ActionFloatingMessage::Stop()
extern void ActionFloatingMessage_Stop_m88D275FB87FFE0C6229AA50C8CDD3E49FB9B06E8 (void);
// 0x00000126 System.Void GameCreator.Messages.ActionFloatingMessage::.ctor()
extern void ActionFloatingMessage__ctor_m0620647B6661F6A4C4F6204FD01E762E693E803C (void);
// 0x00000127 System.Void GameCreator.Messages.ActionFloatingMessage/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mF5061944FC0A460DED1EA0C8588B936DACEE9EDD (void);
// 0x00000128 System.Boolean GameCreator.Messages.ActionFloatingMessage/<>c__DisplayClass7_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CExecuteU3Eb__0_mF9468594434EBB4FF71F43F24932628D5FF728BF (void);
// 0x00000129 System.Void GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::.ctor(System.Int32)
extern void U3CExecuteU3Ed__7__ctor_m645B88BD53A458E607304C79B5A5D69EC4FBE72B (void);
// 0x0000012A System.Void GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__7_System_IDisposable_Dispose_m074630D9FC92E1AE2788E076CA71820143E7E731 (void);
// 0x0000012B System.Boolean GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::MoveNext()
extern void U3CExecuteU3Ed__7_MoveNext_mB88B7396A7EEC180CFD7E316C2642290FCE2536D (void);
// 0x0000012C System.Object GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE43ABB7C01C2C671FEDD0BBFB7C5497148DC7B47 (void);
// 0x0000012D System.Void GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_mFE2F97328C5088F606C650B4DF62ACA28328F7C3 (void);
// 0x0000012E System.Object GameCreator.Messages.ActionFloatingMessage/<Execute>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m09CF5B3C1EC26C1A1F053626B8B2A6E8A7658AEE (void);
// 0x0000012F System.Void GameCreator.Messages.FloatingMessageManager::RequireInit()
extern void FloatingMessageManager_RequireInit_mCF51128970129CEBE0763D14B87EA1D8B9722E21 (void);
// 0x00000130 System.Void GameCreator.Messages.FloatingMessageManager::Show(System.String,UnityEngine.Color,UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void FloatingMessageManager_Show_m4B9E9DCFF3C83CDCD69B20DAD8437D477A504B34 (void);
// 0x00000131 System.Collections.IEnumerator GameCreator.Messages.FloatingMessageManager::CoroutineShow(System.String,UnityEngine.Color,UnityEngine.GameObject,System.Single)
extern void FloatingMessageManager_CoroutineShow_m0220E1AE93E6EC41522C334BA5C5F1287A269900 (void);
// 0x00000132 System.Void GameCreator.Messages.FloatingMessageManager::.cctor()
extern void FloatingMessageManager__cctor_m5357BBECC985BC2CF855B96FE84A398672C0B86A (void);
// 0x00000133 System.Void GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::.ctor(System.Int32)
extern void U3CCoroutineShowU3Ed__7__ctor_mCAE5A30C39DEC18976BFB9583C017FCCCA30A36E (void);
// 0x00000134 System.Void GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::System.IDisposable.Dispose()
extern void U3CCoroutineShowU3Ed__7_System_IDisposable_Dispose_m86C925F77A798544C527999C561D3874064C373B (void);
// 0x00000135 System.Boolean GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::MoveNext()
extern void U3CCoroutineShowU3Ed__7_MoveNext_m693C40E5D45D1E5C3C90716FD12DB1A246EA6EDA (void);
// 0x00000136 System.Object GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineShowU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m714EDE17A8EBE56F2D7BA04B146A1F94D5C2A1D9 (void);
// 0x00000137 System.Void GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_Reset_m9BEB6319F95DA2D06C30F20C4A7D55000B14232B (void);
// 0x00000138 System.Object GameCreator.Messages.FloatingMessageManager/<CoroutineShow>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_get_Current_mEB27EBC56988E61DF2FF2BEB9968EBC2ED2CA3A0 (void);
// 0x00000139 System.Collections.IEnumerator GameCreator.Messages.ActionSimpleMessageShow::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionSimpleMessageShow_Execute_mC5832C6D843A71A1A4C29DA4676AFF28998EE806 (void);
// 0x0000013A System.Void GameCreator.Messages.ActionSimpleMessageShow::Stop()
extern void ActionSimpleMessageShow_Stop_m409FCD8D60C2F5E1374CCF960FFE17E0C082D2F7 (void);
// 0x0000013B System.Void GameCreator.Messages.ActionSimpleMessageShow::.ctor()
extern void ActionSimpleMessageShow__ctor_mCD27905EEC1BB4EE9D1C1ECBB77ABF4AD8B081F6 (void);
// 0x0000013C System.Void GameCreator.Messages.ActionSimpleMessageShow/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m021A16FEBAC869F1BA1503628EA07637905AA2A4 (void);
// 0x0000013D System.Boolean GameCreator.Messages.ActionSimpleMessageShow/<>c__DisplayClass5_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CExecuteU3Eb__0_mBB5A1E7586484A3A536C0CA616D754A7499CC4BF (void);
// 0x0000013E System.Void GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::.ctor(System.Int32)
extern void U3CExecuteU3Ed__5__ctor_m3F5262E9C558F8015181D212E910AF955EED9179 (void);
// 0x0000013F System.Void GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__5_System_IDisposable_Dispose_m388E6650772D9F9F1B1C94AA393E1C65D0E5743E (void);
// 0x00000140 System.Boolean GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::MoveNext()
extern void U3CExecuteU3Ed__5_MoveNext_m7E1D4587B5001C868150BFD887727A30C4B0D9A2 (void);
// 0x00000141 System.Object GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28E82C9CEF55D8CF9A58366AD7F3FC1071937101 (void);
// 0x00000142 System.Void GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m551180D660E24ED9170D68C6C7FC8E25934190CD (void);
// 0x00000143 System.Object GameCreator.Messages.ActionSimpleMessageShow/<Execute>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mB797C426D079B14DA6B07F17558837335739D50A (void);
// 0x00000144 System.Void GameCreator.Messages.SimpleMessageManager::OnCreate()
extern void SimpleMessageManager_OnCreate_m17F697AC203A81CB424AD33ED661B1673C6BA36A (void);
// 0x00000145 System.Boolean GameCreator.Messages.SimpleMessageManager::ShouldNotDestroyOnLoad()
extern void SimpleMessageManager_ShouldNotDestroyOnLoad_m97B004741FAD77D2BAC0B708D0424E0509F32623 (void);
// 0x00000146 System.Void GameCreator.Messages.SimpleMessageManager::ShowText(System.String,UnityEngine.Color)
extern void SimpleMessageManager_ShowText_mB7F3F7C8A6F4C9AE2B6AD847E8AE10F1FE8D5FFA (void);
// 0x00000147 System.Void GameCreator.Messages.SimpleMessageManager::HideText()
extern void SimpleMessageManager_HideText_m12114E862760EC6291F19B373868F7D30D51697B (void);
// 0x00000148 System.Collections.IEnumerator GameCreator.Messages.SimpleMessageManager::HideTextDelayed()
extern void SimpleMessageManager_HideTextDelayed_m1CB192B2F48BECC263728253FA9840D360B3CE64 (void);
// 0x00000149 System.Void GameCreator.Messages.SimpleMessageManager::.ctor()
extern void SimpleMessageManager__ctor_mAE01CFB53C76F96ADAB30DBD4519C3DFAA6FEB87 (void);
// 0x0000014A System.Void GameCreator.Messages.SimpleMessageManager::.cctor()
extern void SimpleMessageManager__cctor_m375E8FACFBF94EB72EB32FCA351F405D18DD3F86 (void);
// 0x0000014B System.Void GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::.ctor(System.Int32)
extern void U3CHideTextDelayedU3Ed__11__ctor_m7E5DCB81FCD6E15E8D6CC97EAD891CA0BD8F22A1 (void);
// 0x0000014C System.Void GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::System.IDisposable.Dispose()
extern void U3CHideTextDelayedU3Ed__11_System_IDisposable_Dispose_m50F21BC68D8A41B1065AFDDFBB4BC323ED1024C4 (void);
// 0x0000014D System.Boolean GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::MoveNext()
extern void U3CHideTextDelayedU3Ed__11_MoveNext_m268FEE54D4811A9CDF53676B448B76C6B1F39EE5 (void);
// 0x0000014E System.Object GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHideTextDelayedU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67242BD4F4EE571C0EFA15B38109ED78B3D974E5 (void);
// 0x0000014F System.Void GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::System.Collections.IEnumerator.Reset()
extern void U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_Reset_m5869D5BC10C92C2F71A32895DDDFA44A0A862BB0 (void);
// 0x00000150 System.Object GameCreator.Messages.SimpleMessageManager/<HideTextDelayed>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_get_Current_m1C941C49AC049605C872428FD343A5D9990088AB (void);
// 0x00000151 System.Boolean GameCreator.Melee.ActionMeleeAddDefense::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeAddDefense_InstantExecute_m16D5E914AA189A4ED65BED7767F1C393F8C11633 (void);
// 0x00000152 System.Void GameCreator.Melee.ActionMeleeAddDefense::.ctor()
extern void ActionMeleeAddDefense__ctor_mE2166250B3620BCB97CEDB6B6801CF4CEA69B0E5 (void);
// 0x00000153 System.Boolean GameCreator.Melee.ActionMeleeAddPoise::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeAddPoise_InstantExecute_mA8EE621C624FF0E0656AB6BA81EC5EC7716A0092 (void);
// 0x00000154 System.Void GameCreator.Melee.ActionMeleeAddPoise::.ctor()
extern void ActionMeleeAddPoise__ctor_mFC5CC9C570615BC93EC2509EB7AD7600728D05A1 (void);
// 0x00000155 System.Boolean GameCreator.Melee.ActionMeleeAttack::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeAttack_InstantExecute_m02288B94E2EC300F338F261E6E6E3769E73F4733 (void);
// 0x00000156 System.Void GameCreator.Melee.ActionMeleeAttack::.ctor()
extern void ActionMeleeAttack__ctor_m637FB043AE1C8A51D7EEEE8181D4DD8C93338514 (void);
// 0x00000157 System.Boolean GameCreator.Melee.ActionMeleeBlock::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeBlock_InstantExecute_m1FD78495D17F23AA130BB7BAA81F2D8B0CDA1BAC (void);
// 0x00000158 System.Void GameCreator.Melee.ActionMeleeBlock::.ctor()
extern void ActionMeleeBlock__ctor_mC58B65405EB57E97C004C1A7197DB3511BD77FE5 (void);
// 0x00000159 System.Boolean GameCreator.Melee.ActionMeleeChangeShield::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeChangeShield_InstantExecute_m950EE64C9C203764F90BD94787CC83F7637693C3 (void);
// 0x0000015A System.Void GameCreator.Melee.ActionMeleeChangeShield::.ctor()
extern void ActionMeleeChangeShield__ctor_mACCD70ED62536553F41A8FA4870BC504B9CE13AA (void);
// 0x0000015B System.Boolean GameCreator.Melee.ActionMeleeDraw::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeDraw_InstantExecute_m60EE8D4094AE766593B28D5B33D5EB932BBFA808 (void);
// 0x0000015C System.Void GameCreator.Melee.ActionMeleeDraw::.ctor()
extern void ActionMeleeDraw__ctor_m8AF7D07C9FCA9A6C251D2C1FD66A7F0E917F869C (void);
// 0x0000015D System.Boolean GameCreator.Melee.ActionMeleeFocusTarget::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeFocusTarget_InstantExecute_m3B20405B5252FC796FA35EE3C83C292B16FDB4EA (void);
// 0x0000015E System.Void GameCreator.Melee.ActionMeleeFocusTarget::.ctor()
extern void ActionMeleeFocusTarget__ctor_m5042A4FDF9293D7E734661AB2EE39D11CBB9254B (void);
// 0x0000015F System.Boolean GameCreator.Melee.ActionMeleeSetInvincible::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeSetInvincible_InstantExecute_mEAB4FC652C5155CF2A76FAE16FFF9738634DD0AC (void);
// 0x00000160 System.Void GameCreator.Melee.ActionMeleeSetInvincible::.ctor()
extern void ActionMeleeSetInvincible__ctor_m53F557E3A20620CEAAF93BFD38C6CC86EDF65FF8 (void);
// 0x00000161 System.Boolean GameCreator.Melee.ActionMeleeSheathe::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMeleeSheathe_InstantExecute_mAD1BDF3870340EEEDC7DEE13CC469BD68B7CB21E (void);
// 0x00000162 System.Void GameCreator.Melee.ActionMeleeSheathe::.ctor()
extern void ActionMeleeSheathe__ctor_m96B548D0C8B0352DC3AABEABECD248F317203B50 (void);
// 0x00000163 System.Single GameCreator.Melee.MeleeClip::get_Length()
extern void MeleeClip_get_Length_m057B66F2EC51F50FAC999A1FD5F7D2FAF743E633 (void);
// 0x00000164 System.Void GameCreator.Melee.MeleeClip::Play(GameCreator.Melee.CharacterMelee)
extern void MeleeClip_Play_m145D2D7DCA193673CD7780D8EC3D219DF2F23699 (void);
// 0x00000165 System.Void GameCreator.Melee.MeleeClip::ExecuteHitPause()
extern void MeleeClip_ExecuteHitPause_m81C58CF9862BCAEF8C3A39F7E464E66541AB3316 (void);
// 0x00000166 System.Void GameCreator.Melee.MeleeClip::ExecuteActionsOnStart(UnityEngine.Vector3,UnityEngine.GameObject)
extern void MeleeClip_ExecuteActionsOnStart_m3091BC5BA369C8E91D55BCD24D7367F19A9B9580 (void);
// 0x00000167 System.Void GameCreator.Melee.MeleeClip::ExecuteActionsOnHit(UnityEngine.Vector3,UnityEngine.GameObject)
extern void MeleeClip_ExecuteActionsOnHit_m372829234FEDBBF6A65AEAD48B521C879D8A0D04 (void);
// 0x00000168 System.Collections.IEnumerator GameCreator.Melee.MeleeClip::ExecuteHitPause(System.Single)
extern void MeleeClip_ExecuteHitPause_m443EF74676E99E02E6B0A751C098887E02AA66F5 (void);
// 0x00000169 System.Void GameCreator.Melee.MeleeClip::.ctor()
extern void MeleeClip__ctor_m01AB68AC75673CE964ECC4D3EF01FEFFDF00B7BF (void);
// 0x0000016A System.Void GameCreator.Melee.MeleeClip::.cctor()
extern void MeleeClip__cctor_m7935E438C993FEA23738841B9D2C3493C6045EFE (void);
// 0x0000016B System.Void GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::.ctor(System.Int32)
extern void U3CExecuteHitPauseU3Ed__37__ctor_mA10EB23AFBB6F257F7D54CB541E96CDB237B2A47 (void);
// 0x0000016C System.Void GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::System.IDisposable.Dispose()
extern void U3CExecuteHitPauseU3Ed__37_System_IDisposable_Dispose_m1FF79EE075B38D2D8362870A57BF06D9A6802B80 (void);
// 0x0000016D System.Boolean GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::MoveNext()
extern void U3CExecuteHitPauseU3Ed__37_MoveNext_m1A74DE14F8A0DE9EA1949C47ABF8949B17FCEEF0 (void);
// 0x0000016E System.Object GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteHitPauseU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9ECB2917A67CF28F30304842FE0AA6968E907F (void);
// 0x0000016F System.Void GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::System.Collections.IEnumerator.Reset()
extern void U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_Reset_m4A85E677F743397D8F2C984245BADCD93DE11221 (void);
// 0x00000170 System.Object GameCreator.Melee.MeleeClip/<ExecuteHitPause>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_get_Current_m7E7F2C0D3E1DE7198126B25DB63FF8E140EB3A79 (void);
// 0x00000171 UnityEngine.GameObject GameCreator.Melee.MeleeShield::EquipShield(GameCreator.Characters.CharacterAnimator)
extern void MeleeShield_EquipShield_mE1C529D9FFC7DA28B8AF181BD2F24E782B011D64 (void);
// 0x00000172 GameCreator.Melee.MeleeClip GameCreator.Melee.MeleeShield::GetBlockReaction()
extern void MeleeShield_GetBlockReaction_m1D2E5CAE1A101CE8CD638C4BB552574C8CEB4145 (void);
// 0x00000173 System.Void GameCreator.Melee.MeleeShield::.ctor()
extern void MeleeShield__ctor_m9C1852F600272FEFC19FF30328B4DBD63C63BA24 (void);
// 0x00000174 System.Collections.Generic.List`1<UnityEngine.GameObject> GameCreator.Melee.MeleeWeapon::EquipWeapon(GameCreator.Characters.CharacterAnimator)
extern void MeleeWeapon_EquipWeapon_m87705EF81FBC464BED0F60A7B63BC2A74A24866A (void);
// 0x00000175 GameCreator.Melee.MeleeClip GameCreator.Melee.MeleeWeapon::GetHitReaction(System.Boolean,GameCreator.Melee.MeleeWeapon/HitLocation,System.Boolean)
extern void MeleeWeapon_GetHitReaction_mD218623FCA5577CB51AC9717B84D8D102D8F2118 (void);
// 0x00000176 System.Void GameCreator.Melee.MeleeWeapon::.ctor()
extern void MeleeWeapon__ctor_m5B0DF3E098ABC7F7229519D135C1F57A4A29323B (void);
// 0x00000177 System.Void GameCreator.Melee.Combo::.ctor()
extern void Combo__ctor_m7B487AD37711BBCE1299AE970BD7512F89C8034C (void);
// 0x00000178 System.Void GameCreator.Melee.ComboSystem::.ctor(GameCreator.Melee.CharacterMelee,System.Collections.Generic.List`1<GameCreator.Melee.Combo>)
extern void ComboSystem__ctor_m323A2E7E095EDB3FC3B2331A8466CD600155BFC7 (void);
// 0x00000179 System.Int32 GameCreator.Melee.ComboSystem::GetCurrentPhase()
extern void ComboSystem_GetCurrentPhase_m003D96E08A9A6AE3AACF3F39155EF19EAA1161AF (void);
// 0x0000017A GameCreator.Melee.MeleeClip GameCreator.Melee.ComboSystem::GetCurrentClip()
extern void ComboSystem_GetCurrentClip_mAAEF47230735C2C31B42D2A9F571E9B23398617C (void);
// 0x0000017B GameCreator.Melee.MeleeClip GameCreator.Melee.ComboSystem::Select(GameCreator.Melee.CharacterMelee/ActionKey)
extern void ComboSystem_Select_m60E1AF6624EECF2369FCDD60C46C738FB2C6685C (void);
// 0x0000017C System.Void GameCreator.Melee.ComboSystem::Stop()
extern void ComboSystem_Stop_m1580D65C4E26F2DCBF856A89276B7CC70C330A90 (void);
// 0x0000017D System.Void GameCreator.Melee.ComboSystem::Update()
extern void ComboSystem_Update_m0AE10E098CD95D038F4C4BE75422E1A45614DDA8 (void);
// 0x0000017E System.Void GameCreator.Melee.ComboSystem::OnPerfectBlock()
extern void ComboSystem_OnPerfectBlock_m288EC6739D6F3DE91998DDDC8F85FFADE2BE18F8 (void);
// 0x0000017F System.Void GameCreator.Melee.ComboSystem::OnBlock()
extern void ComboSystem_OnBlock_mAC05C140F27475AC456B879339BDAE2A3632A19D (void);
// 0x00000180 System.Single GameCreator.Melee.ComboSystem::GetRunningAngle()
extern void ComboSystem_GetRunningAngle_m7F0EF7A270E6EB9DBDEBCC9AE14C4D342306BCDE (void);
// 0x00000181 System.Single GameCreator.Melee.ComboSystem::GetInputAngle()
extern void ComboSystem_GetInputAngle_m18534E71F2227E253DEC317996E8E6187632573E (void);
// 0x00000182 GameCreator.Melee.Combo GameCreator.Melee.ComboSystem::SelectMeleeClip()
extern void ComboSystem_SelectMeleeClip_m27B4EBBD6CCDA0E16DD68EA7185D74342DFE79B8 (void);
// 0x00000183 System.Void GameCreator.Melee.ComboSystem::.cctor()
extern void ComboSystem__cctor_mA74F51B0A36E08F9AC7A639A3FFDB34CDB78720A (void);
// 0x00000184 System.Void GameCreator.Melee.InputBuffer::.ctor(System.Single)
extern void InputBuffer__ctor_mDF6659A814949AFF076C3594F3D3A24AB50C8B5E (void);
// 0x00000185 System.Void GameCreator.Melee.InputBuffer::AddInput(GameCreator.Melee.CharacterMelee/ActionKey)
extern void InputBuffer_AddInput_mDF11397E9660D5B24DF49091E134E5338B0E96BF (void);
// 0x00000186 System.Boolean GameCreator.Melee.InputBuffer::HasInput()
extern void InputBuffer_HasInput_mB8635C4C5E789965A36CEB44C3DDA38C970562C7 (void);
// 0x00000187 GameCreator.Melee.CharacterMelee/ActionKey GameCreator.Melee.InputBuffer::GetInput()
extern void InputBuffer_GetInput_mF2FAB436E16AA4AD861BA3584DB4E0E0C868D06B (void);
// 0x00000188 System.Void GameCreator.Melee.InputBuffer::ConsumeInput()
extern void InputBuffer_ConsumeInput_m28C588D8A2728BFFFAC65A9ABFE8DC21C6B3A69D (void);
// 0x00000189 System.Void GameCreator.Melee.TreeCombo`2::.ctor(TKey)
// 0x0000018A System.Void GameCreator.Melee.TreeCombo`2::.ctor(TKey,TValue)
// 0x0000018B TKey GameCreator.Melee.TreeCombo`2::GetID()
// 0x0000018C TValue[] GameCreator.Melee.TreeCombo`2::GetData()
// 0x0000018D System.Void GameCreator.Melee.TreeCombo`2::SetData(TValue)
// 0x0000018E GameCreator.Melee.TreeCombo`2<TKey,TValue> GameCreator.Melee.TreeCombo`2::GetChild(TKey)
// 0x0000018F System.Boolean GameCreator.Melee.TreeCombo`2::HasChild(TKey)
// 0x00000190 GameCreator.Melee.TreeCombo`2<TKey,TValue> GameCreator.Melee.TreeCombo`2::AddChild(GameCreator.Melee.TreeCombo`2<TKey,TValue>)
// 0x00000191 System.String GameCreator.Melee.TreeCombo`2::ToString()
// 0x00000192 System.String GameCreator.Melee.TreeCombo`2::BuildString(GameCreator.Melee.TreeCombo`2<TKey,TValue>)
// 0x00000193 System.Void GameCreator.Melee.TreeCombo`2::BuildString(System.Text.StringBuilder,GameCreator.Melee.TreeCombo`2<TKey,TValue>,System.Int32)
// 0x00000194 System.Collections.Generic.IEnumerator`1<GameCreator.Melee.TreeCombo`2<TKey,TValue>> GameCreator.Melee.TreeCombo`2::GetEnumerator()
// 0x00000195 System.Collections.IEnumerator GameCreator.Melee.TreeCombo`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000196 System.Void GameCreator.Melee.WeaponTrail::Initialize(UnityEngine.Material,System.Int32,System.Single)
extern void WeaponTrail_Initialize_m48A7B378C9B8D3C085625972B50666F3B0583EFD (void);
// 0x00000197 System.Void GameCreator.Melee.WeaponTrail::Activate()
extern void WeaponTrail_Activate_m218806A2E99FD98589D4318DED263577CD5111A3 (void);
// 0x00000198 System.Void GameCreator.Melee.WeaponTrail::Deactivate()
extern void WeaponTrail_Deactivate_m949F7A3E4FBF65F1B378BA158179E9645E4579F0 (void);
// 0x00000199 System.Void GameCreator.Melee.WeaponTrail::Deactivate(System.Single)
extern void WeaponTrail_Deactivate_m76524862F457E698139470878AAD756ECA2B2AD0 (void);
// 0x0000019A System.Void GameCreator.Melee.WeaponTrail::Tick(UnityEngine.Vector3,UnityEngine.Vector3)
extern void WeaponTrail_Tick_m6D7C19B807B1996CF39EF822B89388EF446107E8 (void);
// 0x0000019B System.Void GameCreator.Melee.WeaponTrail::GatherData()
extern void WeaponTrail_GatherData_mDE4AE9A3754BD64F6750A7853302CC092BF48BA1 (void);
// 0x0000019C System.Void GameCreator.Melee.WeaponTrail::TrimData()
extern void WeaponTrail_TrimData_m9091947EC575F44FD201D450312D00C3FAA86308 (void);
// 0x0000019D System.Boolean GameCreator.Melee.WeaponTrail::CheckLifetime()
extern void WeaponTrail_CheckLifetime_m81BD6B257B188E67EB605AA7C552F7D34859CE10 (void);
// 0x0000019E System.Boolean GameCreator.Melee.WeaponTrail::CheckFading()
extern void WeaponTrail_CheckFading_m52E490A1D63746DA93010AACB207D4D76F820444 (void);
// 0x0000019F System.Void GameCreator.Melee.WeaponTrail::UpdatePoints()
extern void WeaponTrail_UpdatePoints_mB8B117896115FED9A1AB6F80BE6823104572239E (void);
// 0x000001A0 System.Void GameCreator.Melee.WeaponTrail::UpdateMesh()
extern void WeaponTrail_UpdateMesh_m9BD03EC22994F6A36CC61C423AEDA57874EE6E46 (void);
// 0x000001A1 System.Void GameCreator.Melee.WeaponTrail::GenerateSpline(System.Int32,System.Single,System.Collections.Generic.List`1<UnityEngine.Vector3>&)
extern void WeaponTrail_GenerateSpline_mB28D195AAFC4DB2816DC16CC66B464B23CD93B3E (void);
// 0x000001A2 UnityEngine.Vector3 GameCreator.Melee.WeaponTrail::CatmullRomPosition(System.Single,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void WeaponTrail_CatmullRomPosition_mDB848058A980C0E69D21F4AFF9E61C575EAE1FB6 (void);
// 0x000001A3 System.Void GameCreator.Melee.WeaponTrail::RegenerateMesh()
extern void WeaponTrail_RegenerateMesh_m161649E00249D90E694B7364C90002B45499C17C (void);
// 0x000001A4 System.Void GameCreator.Melee.WeaponTrail::DrawGizmos()
extern void WeaponTrail_DrawGizmos_mA8D177D6AE63970436CA01ED094F9E6B14642897 (void);
// 0x000001A5 System.Void GameCreator.Melee.WeaponTrail::.ctor()
extern void WeaponTrail__ctor_mD092110EF39728A6C1BD1EA824612520845881FC (void);
// 0x000001A6 System.Void GameCreator.Melee.WeaponTrail/Data::.ctor()
extern void Data__ctor_mDA1CA317D8B44510B38FB82B1A3CCE618BCBB2DA (void);
// 0x000001A7 System.Void GameCreator.Melee.WeaponTrail/Data::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void Data__ctor_mAD012A8800D04C34E5BF058F8207B904043670A6 (void);
// 0x000001A8 GameCreator.Melee.CharacterMelee GameCreator.Melee.BladeComponent::get_Melee()
extern void BladeComponent_get_Melee_mB9E245D961F3BB1A87373D8E939D278F7C8C28F2 (void);
// 0x000001A9 System.Void GameCreator.Melee.BladeComponent::set_Melee(GameCreator.Melee.CharacterMelee)
extern void BladeComponent_set_Melee_m7071E3DCEB7520779AA743DA210BB810041366A8 (void);
// 0x000001AA System.Void GameCreator.Melee.BladeComponent::Setup(GameCreator.Melee.CharacterMelee)
extern void BladeComponent_Setup_mC5269FD5B78DDD7C353634B357CF8453A6A75F09 (void);
// 0x000001AB System.Void GameCreator.Melee.BladeComponent::Awake()
extern void BladeComponent_Awake_m20B193B1FF75EB6771764D19B241A1593A006383 (void);
// 0x000001AC System.Void GameCreator.Melee.BladeComponent::Update()
extern void BladeComponent_Update_m0F529040D33347C62566B8F98082B81D114C33AC (void);
// 0x000001AD System.Void GameCreator.Melee.BladeComponent::LateUpdate()
extern void BladeComponent_LateUpdate_m3097A8A1876516322CF5B251C4BE859C30D06E65 (void);
// 0x000001AE UnityEngine.GameObject[] GameCreator.Melee.BladeComponent::CaptureHits()
extern void BladeComponent_CaptureHits_m271AE4B474FF2D56B53631CD01D0139943207606 (void);
// 0x000001AF UnityEngine.GameObject[] GameCreator.Melee.BladeComponent::CaptureHitsSegment()
extern void BladeComponent_CaptureHitsSegment_m176CD92D77F9002F398FA5DB3378DEECDCC9A0D3 (void);
// 0x000001B0 UnityEngine.GameObject[] GameCreator.Melee.BladeComponent::CaptureHitsSphere()
extern void BladeComponent_CaptureHitsSphere_m6BB1306C4E46CBE47CD3BEF8D3196D9B0D8DAE18 (void);
// 0x000001B1 UnityEngine.GameObject[] GameCreator.Melee.BladeComponent::CaptureHitsBox()
extern void BladeComponent_CaptureHitsBox_m62A7E2880BE8ACF72D9415772B0DD628B3319ACF (void);
// 0x000001B2 UnityEngine.Vector3 GameCreator.Melee.BladeComponent::GetImpactPosition()
extern void BladeComponent_GetImpactPosition_mAEF1AE7C8F6D1F5418AEF91891D240E536650AB0 (void);
// 0x000001B3 System.Void GameCreator.Melee.BladeComponent::OnDrawGizmos()
extern void BladeComponent_OnDrawGizmos_m6F8C152C197D417C1E221E9D1AA2133612939BCC (void);
// 0x000001B4 System.Void GameCreator.Melee.BladeComponent::.ctor()
extern void BladeComponent__ctor_m759B7EB40E54838BCC29294149124E303E16B2C1 (void);
// 0x000001B5 System.Void GameCreator.Melee.BladeComponent::.cctor()
extern void BladeComponent__cctor_m30292EA825571E54F4F6F17394BA453D95A06D6C (void);
// 0x000001B6 System.Void GameCreator.Melee.BladeComponent/BoxData::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BoxData__ctor_m4379031EB9F1E1D541B8DDB4C0B835D72497F739 (void);
// 0x000001B7 System.Void GameCreator.Melee.BladeComponent/BladeEvent::.ctor()
extern void BladeEvent__ctor_m74EE44CAA1915FF0D4080552CE218D5DD762EAC7 (void);
// 0x000001B8 System.Single GameCreator.Melee.CharacterMelee::get_Poise()
extern void CharacterMelee_get_Poise_mAE7DA4E54BA255648BD8839974782B5FB9759A83 (void);
// 0x000001B9 System.Void GameCreator.Melee.CharacterMelee::set_Poise(System.Single)
extern void CharacterMelee_set_Poise_m4A8BF6725DF27A0938FCF2C698694491FA1CE201 (void);
// 0x000001BA System.Single GameCreator.Melee.CharacterMelee::get_Defense()
extern void CharacterMelee_get_Defense_m507A720A94BCAB37B13ED5B740535DA9208302D3 (void);
// 0x000001BB System.Void GameCreator.Melee.CharacterMelee::set_Defense(System.Single)
extern void CharacterMelee_set_Defense_mB3F50AE24CE4E58C8D6763D7A1CCCB2FA8BAD61A (void);
// 0x000001BC System.Boolean GameCreator.Melee.CharacterMelee::get_IsDrawing()
extern void CharacterMelee_get_IsDrawing_m896E27571073303B79D91CC2886EA9C0636D92CD (void);
// 0x000001BD System.Void GameCreator.Melee.CharacterMelee::set_IsDrawing(System.Boolean)
extern void CharacterMelee_set_IsDrawing_m6DE5FA9449BA3AFE940FFCF9CBFE4C9C7EED6310 (void);
// 0x000001BE System.Boolean GameCreator.Melee.CharacterMelee::get_IsSheathing()
extern void CharacterMelee_get_IsSheathing_mBD515D68F0139E9E1913CA37B3F8B9B10A735FE1 (void);
// 0x000001BF System.Void GameCreator.Melee.CharacterMelee::set_IsSheathing(System.Boolean)
extern void CharacterMelee_set_IsSheathing_mD90AAC761D35BF4D894733EC2AD265B6859CCAA6 (void);
// 0x000001C0 System.Boolean GameCreator.Melee.CharacterMelee::get_IsAttacking()
extern void CharacterMelee_get_IsAttacking_m4E8EE2CD01688E319C7166420F9342088E9B7BFE (void);
// 0x000001C1 System.Void GameCreator.Melee.CharacterMelee::set_IsAttacking(System.Boolean)
extern void CharacterMelee_set_IsAttacking_mA3B512832B1EAE92A05E64DE3791659CB78F8A7E (void);
// 0x000001C2 System.Boolean GameCreator.Melee.CharacterMelee::get_IsBlocking()
extern void CharacterMelee_get_IsBlocking_mFF11D0FD9798CCCF39E1545039E591236C5C896A (void);
// 0x000001C3 System.Void GameCreator.Melee.CharacterMelee::set_IsBlocking(System.Boolean)
extern void CharacterMelee_set_IsBlocking_mCB612F17CA908BA0BD4188DE62ACAA49A7367D95 (void);
// 0x000001C4 System.Boolean GameCreator.Melee.CharacterMelee::get_HasFocusTarget()
extern void CharacterMelee_get_HasFocusTarget_m7BD8A39A0F492B250E44E3208F79B8B5C4DEBF20 (void);
// 0x000001C5 System.Void GameCreator.Melee.CharacterMelee::set_HasFocusTarget(System.Boolean)
extern void CharacterMelee_set_HasFocusTarget_m590FCFD6C2DC40DF79BB59FEDAC4A1AAEA08D2AE (void);
// 0x000001C6 System.Boolean GameCreator.Melee.CharacterMelee::get_IsStaggered()
extern void CharacterMelee_get_IsStaggered_mED35EB62FB65B67C7C358B584ABD64BE0B1D455D (void);
// 0x000001C7 System.Boolean GameCreator.Melee.CharacterMelee::get_IsInvincible()
extern void CharacterMelee_get_IsInvincible_mB69CC21AD035E5B2B4093D9CABCAD13FFB681ED0 (void);
// 0x000001C8 System.Boolean GameCreator.Melee.CharacterMelee::get_IsUninterruptable()
extern void CharacterMelee_get_IsUninterruptable_m986E3B7AC009D01EE26B2B235AE672F24EE14AED (void);
// 0x000001C9 System.Void GameCreator.Melee.CharacterMelee::add_EventDrawWeapon(System.Action`1<GameCreator.Melee.MeleeWeapon>)
extern void CharacterMelee_add_EventDrawWeapon_m729A717864566C772CF71410BDE1924DBC3AE947 (void);
// 0x000001CA System.Void GameCreator.Melee.CharacterMelee::remove_EventDrawWeapon(System.Action`1<GameCreator.Melee.MeleeWeapon>)
extern void CharacterMelee_remove_EventDrawWeapon_m3A5FA842827A19CBB1895C99397FA608F52E9D60 (void);
// 0x000001CB System.Void GameCreator.Melee.CharacterMelee::add_EventSheatheWeapon(System.Action`1<GameCreator.Melee.MeleeWeapon>)
extern void CharacterMelee_add_EventSheatheWeapon_mFDA0966CF05CA40A002E10DCE4C99DB694C5AC77 (void);
// 0x000001CC System.Void GameCreator.Melee.CharacterMelee::remove_EventSheatheWeapon(System.Action`1<GameCreator.Melee.MeleeWeapon>)
extern void CharacterMelee_remove_EventSheatheWeapon_mFA2B6F3F7DE02A7CFA9A36FE638A2B044AD289C5 (void);
// 0x000001CD System.Void GameCreator.Melee.CharacterMelee::add_EventAttack(System.Action`1<GameCreator.Melee.MeleeClip>)
extern void CharacterMelee_add_EventAttack_m2DBB7EEA0FB85D42038CE65C50B355CB02C782BA (void);
// 0x000001CE System.Void GameCreator.Melee.CharacterMelee::remove_EventAttack(System.Action`1<GameCreator.Melee.MeleeClip>)
extern void CharacterMelee_remove_EventAttack_m8EEAE732C1B79F7BD62F5C72513543C2D361183E (void);
// 0x000001CF System.Void GameCreator.Melee.CharacterMelee::add_EventStagger(System.Action)
extern void CharacterMelee_add_EventStagger_m4109CC64CD32AA9507C988F3934BCC867CF25381 (void);
// 0x000001D0 System.Void GameCreator.Melee.CharacterMelee::remove_EventStagger(System.Action)
extern void CharacterMelee_remove_EventStagger_m3E4FB53BF4351A020EDC55C7332F901CF14A99BF (void);
// 0x000001D1 System.Void GameCreator.Melee.CharacterMelee::add_EventBreakDefense(System.Action)
extern void CharacterMelee_add_EventBreakDefense_mB6BB87DFB00DB2B5C574CC8968782E6E6BDA702E (void);
// 0x000001D2 System.Void GameCreator.Melee.CharacterMelee::remove_EventBreakDefense(System.Action)
extern void CharacterMelee_remove_EventBreakDefense_m6FCF86C0FEB98F29565FDA8088184CE910D40BBF (void);
// 0x000001D3 System.Void GameCreator.Melee.CharacterMelee::add_EventBlock(System.Action`1<System.Boolean>)
extern void CharacterMelee_add_EventBlock_mF88A06EB7F4A0F2C1010BC793B5E4E29416468AB (void);
// 0x000001D4 System.Void GameCreator.Melee.CharacterMelee::remove_EventBlock(System.Action`1<System.Boolean>)
extern void CharacterMelee_remove_EventBlock_m8A34212AA2E12435FF6D7EB286882E490296ADC0 (void);
// 0x000001D5 System.Void GameCreator.Melee.CharacterMelee::add_EventFocus(System.Action`1<System.Boolean>)
extern void CharacterMelee_add_EventFocus_m634B32366980C642D31F60AC90C71CB7152BD014 (void);
// 0x000001D6 System.Void GameCreator.Melee.CharacterMelee::remove_EventFocus(System.Action`1<System.Boolean>)
extern void CharacterMelee_remove_EventFocus_m2EF8A205D03A9353EC32768ECFE79B2142FC9674 (void);
// 0x000001D7 GameCreator.Characters.Character GameCreator.Melee.CharacterMelee::get_Character()
extern void CharacterMelee_get_Character_mC8213FA44F089EE7A6B5B06B0D7390E32E4CB5CA (void);
// 0x000001D8 System.Void GameCreator.Melee.CharacterMelee::set_Character(GameCreator.Characters.Character)
extern void CharacterMelee_set_Character_m78B44B240657959C2834FB7887BA2D07FE375DE4 (void);
// 0x000001D9 GameCreator.Characters.CharacterAnimator GameCreator.Melee.CharacterMelee::get_CharacterAnimator()
extern void CharacterMelee_get_CharacterAnimator_mA4B0B02AB3070AEE773BF16764D3D3796A1A9FB6 (void);
// 0x000001DA System.Void GameCreator.Melee.CharacterMelee::set_CharacterAnimator(GameCreator.Characters.CharacterAnimator)
extern void CharacterMelee_set_CharacterAnimator_m2AD046145B4043A4D45654BB2C01E0F7BE6428EA (void);
// 0x000001DB System.Collections.Generic.List`1<GameCreator.Melee.BladeComponent> GameCreator.Melee.CharacterMelee::get_Blades()
extern void CharacterMelee_get_Blades_m4E84AE0B4C496A680D941D9981142B0A81CBD87F (void);
// 0x000001DC System.Void GameCreator.Melee.CharacterMelee::set_Blades(System.Collections.Generic.List`1<GameCreator.Melee.BladeComponent>)
extern void CharacterMelee_set_Blades_mC80F11AE570B6BD8E1339C79799EA35F12D6E0F7 (void);
// 0x000001DD System.Void GameCreator.Melee.CharacterMelee::Awake()
extern void CharacterMelee_Awake_m8941B52A3C884E1B937C0965022AB44F4AFF8133 (void);
// 0x000001DE System.Void GameCreator.Melee.CharacterMelee::Update()
extern void CharacterMelee_Update_mF5BB207BBBF1490BBED738303F5001BEDBD37D95 (void);
// 0x000001DF System.Void GameCreator.Melee.CharacterMelee::LateUpdate()
extern void CharacterMelee_LateUpdate_mBF9B44E2FEDADAE3BFFA16C88F39D16C678AD56A (void);
// 0x000001E0 System.Void GameCreator.Melee.CharacterMelee::UpdatePoise()
extern void CharacterMelee_UpdatePoise_mE8A2B55A212C6FE911FF3952780F76ACCC4AFB25 (void);
// 0x000001E1 System.Void GameCreator.Melee.CharacterMelee::UpdateDefense()
extern void CharacterMelee_UpdateDefense_m3F4412825A479BA5000341B5B84FA08B7CD95E38 (void);
// 0x000001E2 System.Collections.IEnumerator GameCreator.Melee.CharacterMelee::Sheathe()
extern void CharacterMelee_Sheathe_mFB1AED1FFEB02DFD57F1E5FAA86CB41C8E18C3E7 (void);
// 0x000001E3 System.Collections.IEnumerator GameCreator.Melee.CharacterMelee::Draw(GameCreator.Melee.MeleeWeapon,GameCreator.Melee.MeleeShield)
extern void CharacterMelee_Draw_m00C457FC85DEA4F4DFC918137617E27E6357A39B (void);
// 0x000001E4 System.Void GameCreator.Melee.CharacterMelee::EquipShield(GameCreator.Melee.MeleeShield)
extern void CharacterMelee_EquipShield_mEA6FB9A21F21372EB26FDE824290171087351E06 (void);
// 0x000001E5 System.Void GameCreator.Melee.CharacterMelee::StartBlocking()
extern void CharacterMelee_StartBlocking_m4A206BA912284E9EFF5B66E4A46DE114D0CB4EDF (void);
// 0x000001E6 System.Void GameCreator.Melee.CharacterMelee::StopBlocking()
extern void CharacterMelee_StopBlocking_mC63943524C106212F7873D4A836AF31E25319DC9 (void);
// 0x000001E7 System.Void GameCreator.Melee.CharacterMelee::Execute(GameCreator.Melee.CharacterMelee/ActionKey)
extern void CharacterMelee_Execute_mB6253E6A77A5D380E040F96CB08AE9FD241C0E8B (void);
// 0x000001E8 System.Void GameCreator.Melee.CharacterMelee::StopAttack()
extern void CharacterMelee_StopAttack_mDBD5ACC7F4FAC6CCC74029BD7B5B74149AE11BAD (void);
// 0x000001E9 System.Int32 GameCreator.Melee.CharacterMelee::GetCurrentPhase()
extern void CharacterMelee_GetCurrentPhase_m76B691033676DCBC262722BCBFB163AEE83C8E37 (void);
// 0x000001EA System.Void GameCreator.Melee.CharacterMelee::PlayAudio(UnityEngine.AudioClip)
extern void CharacterMelee_PlayAudio_m66ADDBC2CEAC0F73A0C08585D8C23D9AEEF93D4E (void);
// 0x000001EB System.Void GameCreator.Melee.CharacterMelee::SetPosture(GameCreator.Melee.MeleeClip/Posture,System.Single)
extern void CharacterMelee_SetPosture_m3CC6A16B0ADD742C49FDF7BBB724C6BC99038B6B (void);
// 0x000001EC System.Void GameCreator.Melee.CharacterMelee::SetInvincibility(System.Single)
extern void CharacterMelee_SetInvincibility_mF4DB8C8F23C044980FC6CE364CAD460DCF93D46A (void);
// 0x000001ED System.Void GameCreator.Melee.CharacterMelee::SetUninterruptable(System.Single)
extern void CharacterMelee_SetUninterruptable_m9B72D916ED6563F44807B18F6DDA889E645F6487 (void);
// 0x000001EE System.Void GameCreator.Melee.CharacterMelee::SetPoise(System.Single)
extern void CharacterMelee_SetPoise_m306F507038AEEF64F9671AC4A58B619B86E000DF (void);
// 0x000001EF System.Void GameCreator.Melee.CharacterMelee::AddPoise(System.Single)
extern void CharacterMelee_AddPoise_mA561D732BB23B08B40AF74AB562732200E712B0C (void);
// 0x000001F0 System.Void GameCreator.Melee.CharacterMelee::SetDefense(System.Single)
extern void CharacterMelee_SetDefense_m252B2B406050164C8C2FC7BB1AE519684C980020 (void);
// 0x000001F1 System.Void GameCreator.Melee.CharacterMelee::AddDefense(System.Single)
extern void CharacterMelee_AddDefense_m8A0477CCE6A3F3A07684FBF6E343D000F68CFC88 (void);
// 0x000001F2 System.Void GameCreator.Melee.CharacterMelee::SetTargetFocus(GameCreator.Melee.TargetMelee)
extern void CharacterMelee_SetTargetFocus_m83CAA8DD28979D0F6E932204D7582282E962D8C6 (void);
// 0x000001F3 System.Void GameCreator.Melee.CharacterMelee::ReleaseTargetFocus()
extern void CharacterMelee_ReleaseTargetFocus_m5CF26E648497D7F12D64F24D1D5FDD49948D6477 (void);
// 0x000001F4 System.Void GameCreator.Melee.CharacterMelee::OnSheatheWeapon()
extern void CharacterMelee_OnSheatheWeapon_mF3B06846DB7B8D3BF8ED1DC54E70E86D83D29DEC (void);
// 0x000001F5 System.Void GameCreator.Melee.CharacterMelee::OnDrawWeapon()
extern void CharacterMelee_OnDrawWeapon_m2579D0BDE8B651B3CEDF8264C3EFFD866FB198DD (void);
// 0x000001F6 GameCreator.Melee.CharacterMelee/HitResult GameCreator.Melee.CharacterMelee::OnReceiveAttack(GameCreator.Melee.CharacterMelee,GameCreator.Melee.MeleeClip,GameCreator.Melee.BladeComponent)
extern void CharacterMelee_OnReceiveAttack_m2FA475982B7864D74478547C2672F9C50005ACB4 (void);
// 0x000001F7 System.Void GameCreator.Melee.CharacterMelee::ExecuteEffects(UnityEngine.Vector3,UnityEngine.AudioClip,UnityEngine.GameObject)
extern void CharacterMelee_ExecuteEffects_mB8A680FF3AD3F3D0AEF9A7B8A89F5363DBA7795C (void);
// 0x000001F8 System.Boolean GameCreator.Melee.CharacterMelee::CanAttack()
extern void CharacterMelee_CanAttack_m92732DB6E3FC556B2DB6DF8A353BE47CF399212A (void);
// 0x000001F9 System.Single GameCreator.Melee.CharacterMelee::ResetState(GameCreator.Characters.CharacterState,GameCreator.Characters.CharacterAnimation/Layer)
extern void CharacterMelee_ResetState_m19A26A0A528F8EDCE65C505EE64233327E3AA73F (void);
// 0x000001FA System.Single GameCreator.Melee.CharacterMelee::ChangeState(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,GameCreator.Characters.CharacterAnimation/Layer)
extern void CharacterMelee_ChangeState_m8DF5A2669EB59E52C2562C2DB52E364132E067D7 (void);
// 0x000001FB System.Void GameCreator.Melee.CharacterMelee::.ctor()
extern void CharacterMelee__ctor_mE2B52659BBE9732B63F2122C0B5CB649CF12D900 (void);
// 0x000001FC System.Void GameCreator.Melee.CharacterMelee/<Sheathe>d__102::.ctor(System.Int32)
extern void U3CSheatheU3Ed__102__ctor_m786A68C4F750B9E38A58F10D081860846F94A69D (void);
// 0x000001FD System.Void GameCreator.Melee.CharacterMelee/<Sheathe>d__102::System.IDisposable.Dispose()
extern void U3CSheatheU3Ed__102_System_IDisposable_Dispose_m689042F48C397182F117B6C74794F679471DEB34 (void);
// 0x000001FE System.Boolean GameCreator.Melee.CharacterMelee/<Sheathe>d__102::MoveNext()
extern void U3CSheatheU3Ed__102_MoveNext_m84C793A0BB74EA843CB9773216A311066982CEC8 (void);
// 0x000001FF System.Object GameCreator.Melee.CharacterMelee/<Sheathe>d__102::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSheatheU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1FEA64D0AD51EBDA277E8E71DA96251B74C7CA9 (void);
// 0x00000200 System.Void GameCreator.Melee.CharacterMelee/<Sheathe>d__102::System.Collections.IEnumerator.Reset()
extern void U3CSheatheU3Ed__102_System_Collections_IEnumerator_Reset_m8D077FC808E8BC30A3CEE7FE46311E214B99C354 (void);
// 0x00000201 System.Object GameCreator.Melee.CharacterMelee/<Sheathe>d__102::System.Collections.IEnumerator.get_Current()
extern void U3CSheatheU3Ed__102_System_Collections_IEnumerator_get_Current_mD4C206E05163045BB434D18C816174F17F2B2741 (void);
// 0x00000202 System.Void GameCreator.Melee.CharacterMelee/<Draw>d__103::.ctor(System.Int32)
extern void U3CDrawU3Ed__103__ctor_m3CDEEE7F8DB99DC8D219743C706D8827F2422D56 (void);
// 0x00000203 System.Void GameCreator.Melee.CharacterMelee/<Draw>d__103::System.IDisposable.Dispose()
extern void U3CDrawU3Ed__103_System_IDisposable_Dispose_m24DBACDBA5ED681EB17250F583B4BA16E82115C5 (void);
// 0x00000204 System.Boolean GameCreator.Melee.CharacterMelee/<Draw>d__103::MoveNext()
extern void U3CDrawU3Ed__103_MoveNext_m527C5CA66B9BDC70D387971339CE7C8843A128FE (void);
// 0x00000205 System.Object GameCreator.Melee.CharacterMelee/<Draw>d__103::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrawU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3BCBB31D4FD10EECEE363E70EB8F06ACBB66B39 (void);
// 0x00000206 System.Void GameCreator.Melee.CharacterMelee/<Draw>d__103::System.Collections.IEnumerator.Reset()
extern void U3CDrawU3Ed__103_System_Collections_IEnumerator_Reset_m6768D2D348120C62AE0E8148B25D55D1CA19CBE7 (void);
// 0x00000207 System.Object GameCreator.Melee.CharacterMelee/<Draw>d__103::System.Collections.IEnumerator.get_Current()
extern void U3CDrawU3Ed__103_System_Collections_IEnumerator_get_Current_m6E02C65508D6A1B59269C5B13E0D3A071A40EAAE (void);
// 0x00000208 System.Void GameCreator.Melee.TargetMelee::SetTracker(GameCreator.Melee.CharacterMelee)
extern void TargetMelee_SetTracker_m1FF9C444D4292CB8BDC45C5A5B23FC8688609784 (void);
// 0x00000209 System.Void GameCreator.Melee.TargetMelee::ReleaseTracker(GameCreator.Melee.CharacterMelee)
extern void TargetMelee_ReleaseTracker_mD8E962BDF6932AF0A80DF85B75E9C281F704ED88 (void);
// 0x0000020A System.Void GameCreator.Melee.TargetMelee::OnDisable()
extern void TargetMelee_OnDisable_m269D814FF98E0B8DEA2FDA2D23A5283B94A87FA6 (void);
// 0x0000020B System.Void GameCreator.Melee.TargetMelee::.ctor()
extern void TargetMelee__ctor_m00B54E11CCC94EFE4911331EDBC306033DDBCAA5 (void);
// 0x0000020C System.Void GameCreator.Melee.CharacterMeleeUI::Start()
extern void CharacterMeleeUI_Start_m642ABDF11C7614F3F7854CCE8A3731782E03087B (void);
// 0x0000020D System.Void GameCreator.Melee.CharacterMeleeUI::Update()
extern void CharacterMeleeUI_Update_m8F25519B4EDCAE910FE4AB321584393AA2D819F4 (void);
// 0x0000020E System.Void GameCreator.Melee.CharacterMeleeUI::UpdateUI()
extern void CharacterMeleeUI_UpdateUI_m43FB849870298E79B14501FD72AE7B0FA2130718 (void);
// 0x0000020F System.Void GameCreator.Melee.CharacterMeleeUI::.ctor()
extern void CharacterMeleeUI__ctor_m39A6937AF1BF695D648DD9111C0007D5257C1939 (void);
// 0x00000210 System.Boolean GameCreator.Melee.ConditionMeleeArmed::Check(UnityEngine.GameObject)
extern void ConditionMeleeArmed_Check_m8E04A02E2A90A70342B4C1B13AE95512A8675598 (void);
// 0x00000211 System.Void GameCreator.Melee.ConditionMeleeArmed::.ctor()
extern void ConditionMeleeArmed__ctor_m5DD65423D2822D909635A4D77171577CE5DF6479 (void);
// 0x00000212 System.Boolean GameCreator.Melee.ConditionMeleeCompareDefense::Check(UnityEngine.GameObject)
extern void ConditionMeleeCompareDefense_Check_m7D7AC8463148AA6CC7304DCAE9CBDA8735C5247D (void);
// 0x00000213 System.Void GameCreator.Melee.ConditionMeleeCompareDefense::.ctor()
extern void ConditionMeleeCompareDefense__ctor_m0736EC8B44B9F64193FEA0528D26E64AAC386CBC (void);
// 0x00000214 System.Boolean GameCreator.Melee.ConditionMeleeComparePoise::Check(UnityEngine.GameObject)
extern void ConditionMeleeComparePoise_Check_m54F2082ABBD77A6F43F21BF93CDBDD9A7804E5FF (void);
// 0x00000215 System.Void GameCreator.Melee.ConditionMeleeComparePoise::.ctor()
extern void ConditionMeleeComparePoise__ctor_mB536701990A48EBF9D2FC9D6F51C7FBB9EB5AF2D (void);
// 0x00000216 System.Boolean GameCreator.Melee.ConditionMeleeFocusing::Check(UnityEngine.GameObject)
extern void ConditionMeleeFocusing_Check_mB2B46C9D015CE6429ED2255E50FE4A30A8345BCF (void);
// 0x00000217 System.Void GameCreator.Melee.ConditionMeleeFocusing::.ctor()
extern void ConditionMeleeFocusing__ctor_m0C09B7D27228BC73DE8FB68A521E4E1217F1256C (void);
// 0x00000218 System.Boolean GameCreator.Melee.ConditionMeleeIsAttacking::Check(UnityEngine.GameObject)
extern void ConditionMeleeIsAttacking_Check_mAAE8332F1A59DE0FA82FC67899B28A04E5414D5D (void);
// 0x00000219 System.Void GameCreator.Melee.ConditionMeleeIsAttacking::.ctor()
extern void ConditionMeleeIsAttacking__ctor_m4B99A3D4B7296B1507866CB2A4E3B912D63E97B0 (void);
// 0x0000021A System.Boolean GameCreator.Melee.ConditionMeleeIsBlocking::Check(UnityEngine.GameObject)
extern void ConditionMeleeIsBlocking_Check_m8245FABE40909C80BA0837EF24437ADD1AA1236C (void);
// 0x0000021B System.Void GameCreator.Melee.ConditionMeleeIsBlocking::.ctor()
extern void ConditionMeleeIsBlocking__ctor_mEA2C71CF9DE2DA33864AC0185016B4F02836957F (void);
// 0x0000021C System.Void GameCreator.Melee.IgniterMeleeOnAttack::Start()
extern void IgniterMeleeOnAttack_Start_m01450F525A7588FA986B12930820B368227272CF (void);
// 0x0000021D System.Void GameCreator.Melee.IgniterMeleeOnAttack::OnAttack(GameCreator.Melee.MeleeClip)
extern void IgniterMeleeOnAttack_OnAttack_m0CDAB8D0101A5D17118F86564884041D84C804D9 (void);
// 0x0000021E System.Void GameCreator.Melee.IgniterMeleeOnAttack::.ctor()
extern void IgniterMeleeOnAttack__ctor_mB6A9A1BAFC86166D933580FB50FA98727A5C4CFF (void);
// 0x0000021F System.Void GameCreator.Melee.IgniterMeleeOnReceiveAttack::OnReceiveAttack(GameCreator.Melee.CharacterMelee,GameCreator.Melee.MeleeClip,GameCreator.Melee.CharacterMelee/HitResult)
extern void IgniterMeleeOnReceiveAttack_OnReceiveAttack_m5BC3422DA2AF9AEEF1CB3E0D8D5DF053C6B7D499 (void);
// 0x00000220 System.Void GameCreator.Melee.IgniterMeleeOnReceiveAttack::.ctor()
extern void IgniterMeleeOnReceiveAttack__ctor_m3D706E8B9553F6DED688B2DF718E513D905C0D8E (void);
// 0x00000221 GameCreator.Feedback.DatabaseFeedback GameCreator.Feedback.DatabaseFeedback::Load()
extern void DatabaseFeedback_Load_m9FEBAFA74F5C9F3F6DDEBE4CF03895336FBA346D (void);
// 0x00000222 System.Void GameCreator.Feedback.DatabaseFeedback::.ctor()
extern void DatabaseFeedback__ctor_m12BBE445B8CEF0CED24DAEEECB7542AE84DE48B1 (void);
// 0x00000223 System.Void GameCreator.DataStructures.TreeNode`1::.ctor(System.String,T)
// 0x00000224 System.String GameCreator.DataStructures.TreeNode`1::GetID()
// 0x00000225 T GameCreator.DataStructures.TreeNode`1::GetData()
// 0x00000226 System.Void GameCreator.DataStructures.TreeNode`1::SetData(T)
// 0x00000227 GameCreator.DataStructures.TreeNode`1<T> GameCreator.DataStructures.TreeNode`1::GetChild(System.String)
// 0x00000228 System.Boolean GameCreator.DataStructures.TreeNode`1::HasChild(System.String)
// 0x00000229 GameCreator.DataStructures.TreeNode`1<T> GameCreator.DataStructures.TreeNode`1::AddChild(GameCreator.DataStructures.TreeNode`1<T>)
// 0x0000022A System.String GameCreator.DataStructures.TreeNode`1::ToString()
// 0x0000022B System.String GameCreator.DataStructures.TreeNode`1::BuildString(GameCreator.DataStructures.TreeNode`1<T>)
// 0x0000022C System.Void GameCreator.DataStructures.TreeNode`1::BuildString(System.Text.StringBuilder,GameCreator.DataStructures.TreeNode`1<T>,System.Int32)
// 0x0000022D System.Collections.Generic.IEnumerator`1<GameCreator.DataStructures.TreeNode`1<T>> GameCreator.DataStructures.TreeNode`1::GetEnumerator()
// 0x0000022E System.Collections.IEnumerator GameCreator.DataStructures.TreeNode`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000022F System.Void GameCreator.Pool.PoolManager::OnCreate()
extern void PoolManager_OnCreate_m1740B5569145C3429EE6F7C22920AF83DD6C1C3B (void);
// 0x00000230 UnityEngine.GameObject GameCreator.Pool.PoolManager::Pick(UnityEngine.GameObject)
extern void PoolManager_Pick_mBC9C72B9F2CD73E6618D3411AE119206285D144F (void);
// 0x00000231 UnityEngine.GameObject GameCreator.Pool.PoolManager::Pick(GameCreator.Pool.PoolObject)
extern void PoolManager_Pick_mE9F93D3CF2FB602C98BDABE3E1BD9D29846AD753 (void);
// 0x00000232 System.Void GameCreator.Pool.PoolManager::BuildPool(GameCreator.Pool.PoolObject)
extern void PoolManager_BuildPool_mC7DC3538EED00977F47EFB8A4AC317E107D7DD19 (void);
// 0x00000233 System.Void GameCreator.Pool.PoolManager::.ctor()
extern void PoolManager__ctor_mF148809988EBC891C00666A0131E8C9A6781B048 (void);
// 0x00000234 System.Void GameCreator.Pool.PoolManager/PoolData::.ctor(GameCreator.Pool.PoolObject)
extern void PoolData__ctor_m7039BCECA48F973B57743AC22192A50BAE0530DF (void);
// 0x00000235 System.Void GameCreator.Pool.PoolManager/PoolData::Rebuild()
extern void PoolData_Rebuild_m0692CE662DDD0FDFF2F93471A5915DF514A2E888 (void);
// 0x00000236 UnityEngine.GameObject GameCreator.Pool.PoolManager/PoolData::Get()
extern void PoolData_Get_mB39181870B756FD479EA6A48322B67316402B426 (void);
// 0x00000237 System.Void GameCreator.Pool.PoolObject::OnEnable()
extern void PoolObject_OnEnable_m805B8BF963518A0C109795CF13C0414F566CFECF (void);
// 0x00000238 System.Void GameCreator.Pool.PoolObject::OnDisable()
extern void PoolObject_OnDisable_m8B108CEE013E7095CD47C72FFE7DB8A3A9F08810 (void);
// 0x00000239 System.Collections.IEnumerator GameCreator.Pool.PoolObject::SetDisable()
extern void PoolObject_SetDisable_mAA2DF8FF1FE4B2A52BDA18F7D4D9711A81591C51 (void);
// 0x0000023A System.Void GameCreator.Pool.PoolObject::.ctor()
extern void PoolObject__ctor_m1131B4A605ED58F331E22FC901F54F39405F5299 (void);
// 0x0000023B System.Void GameCreator.Pool.PoolObject/<SetDisable>d__7::.ctor(System.Int32)
extern void U3CSetDisableU3Ed__7__ctor_m82A6D08FFA1F31480B620ECA12FC75F1CAD29BCC (void);
// 0x0000023C System.Void GameCreator.Pool.PoolObject/<SetDisable>d__7::System.IDisposable.Dispose()
extern void U3CSetDisableU3Ed__7_System_IDisposable_Dispose_m245E04359C4A3FC3EDA5E332275E22D78084C139 (void);
// 0x0000023D System.Boolean GameCreator.Pool.PoolObject/<SetDisable>d__7::MoveNext()
extern void U3CSetDisableU3Ed__7_MoveNext_m956B34E92F14BF324F2FDE8E264457D7D5D78BA8 (void);
// 0x0000023E System.Object GameCreator.Pool.PoolObject/<SetDisable>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetDisableU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB212C3BF8612759E13002DB305FDCD7C5C2FD19B (void);
// 0x0000023F System.Void GameCreator.Pool.PoolObject/<SetDisable>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSetDisableU3Ed__7_System_Collections_IEnumerator_Reset_mF5D91E3D2FF98620E0869B07A72911E72F8F3B46 (void);
// 0x00000240 System.Object GameCreator.Pool.PoolObject/<SetDisable>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSetDisableU3Ed__7_System_Collections_IEnumerator_get_Current_mB428927DA65710A66D860A096A09C621EB5F7189 (void);
// 0x00000241 UnityEngine.Playables.Playable GameCreator.Playables.ActionsAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
extern void ActionsAsset_CreatePlayable_m704653C2BADFB6282F5A4510C492BF9C2A7706E2 (void);
// 0x00000242 System.Void GameCreator.Playables.ActionsAsset::.ctor()
extern void ActionsAsset__ctor_mD53F52339EAC828AF6BC73F880821093CA6FB90C (void);
// 0x00000243 System.Void GameCreator.Playables.ActionsBehavior::Execute()
extern void ActionsBehavior_Execute_mB21DF5847201534A0D4528B504D89E92DEDF0F94 (void);
// 0x00000244 System.Void GameCreator.Playables.ActionsBehavior::.ctor()
extern void ActionsBehavior__ctor_mAB09895D35B1EBC6C3B4CD2DA7E8121445B0130E (void);
// 0x00000245 System.Void GameCreator.Playables.ActionsTrack::OnCreateClip(UnityEngine.Timeline.TimelineClip)
extern void ActionsTrack_OnCreateClip_m39DEEB7D4557F30D0B464F31DA8AFEEEBDAD1E4F (void);
// 0x00000246 System.Void GameCreator.Playables.ActionsTrack::.ctor()
extern void ActionsTrack__ctor_m3C49574A6D505E9898B700B3E2723A755C48CFEA (void);
// 0x00000247 UnityEngine.Playables.Playable GameCreator.Playables.ConditionsAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
extern void ConditionsAsset_CreatePlayable_mED74F36313F18A355D301AF17EC7089FA8F80841 (void);
// 0x00000248 System.Void GameCreator.Playables.ConditionsAsset::.ctor()
extern void ConditionsAsset__ctor_mA9A2E524F85797DF109B141DD4DF6ABB56D9D7F2 (void);
// 0x00000249 System.Void GameCreator.Playables.ConditionsBehavior::Execute()
extern void ConditionsBehavior_Execute_m4900D0A1AB2EF62874604BCBFEC93F003973894A (void);
// 0x0000024A System.Void GameCreator.Playables.ConditionsBehavior::.ctor()
extern void ConditionsBehavior__ctor_mA15A77D6ED2B68D374A82D0A1CF44898D5694FD8 (void);
// 0x0000024B System.Void GameCreator.Playables.ConditionsTrack::OnCreateClip(UnityEngine.Timeline.TimelineClip)
extern void ConditionsTrack_OnCreateClip_mE51236CE9FB2F1547D9746D264C9950FEF2D994A (void);
// 0x0000024C System.Void GameCreator.Playables.ConditionsTrack::.ctor()
extern void ConditionsTrack__ctor_m9EB19F56B614B8535A003EB9F7B610CA47564D3C (void);
// 0x0000024D System.Void GameCreator.Playables.IGenericBehavior`1::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x0000024E System.Void GameCreator.Playables.IGenericBehavior`1::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
// 0x0000024F System.Void GameCreator.Playables.IGenericBehavior`1::Execute()
// 0x00000250 System.Void GameCreator.Playables.IGenericBehavior`1::.ctor()
// 0x00000251 UnityEngine.Playables.Playable GameCreator.Playables.TriggerAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
extern void TriggerAsset_CreatePlayable_m6A4FBE5BBF6488EFABE1BED49CC854034BE3ED0D (void);
// 0x00000252 System.Void GameCreator.Playables.TriggerAsset::.ctor()
extern void TriggerAsset__ctor_m7D555C061C757B78464A5E9A2D2443EAB8590E27 (void);
// 0x00000253 System.Void GameCreator.Playables.TriggerBehavior::Execute()
extern void TriggerBehavior_Execute_m4E285ACC9B93C4BC34E065F2F570158EE97CDDE1 (void);
// 0x00000254 System.Void GameCreator.Playables.TriggerBehavior::.ctor()
extern void TriggerBehavior__ctor_mC6BEAB9AB0A66F0B2C698607961DA5C594B66C85 (void);
// 0x00000255 System.Void GameCreator.Playables.TriggerTrack::OnCreateClip(UnityEngine.Timeline.TimelineClip)
extern void TriggerTrack_OnCreateClip_mDF9BA9CC2D13B44286DF9DE835F84C35E158F193 (void);
// 0x00000256 System.Void GameCreator.Playables.TriggerTrack::.ctor()
extern void TriggerTrack__ctor_m321A723283AA9AA2770BBFB491E3A57A51C4D8D8 (void);
// 0x00000257 System.Void GameCreator.Localization.LocString::.ctor()
extern void LocString__ctor_m986B98F23C0524D832B4A4804C31F97E70D3920E (void);
// 0x00000258 System.Void GameCreator.Localization.LocString::.ctor(System.String)
extern void LocString__ctor_m097ABEFEB1D6F84F29B29C793E4713851DBBAF52 (void);
// 0x00000259 System.String GameCreator.Localization.LocString::GetText()
extern void LocString_GetText_mAFE912D54FEADAE5EA82F2B876FB661A6D2CDC5B (void);
// 0x0000025A System.String GameCreator.Localization.LocString::PostProcessContent(System.String)
extern void LocString_PostProcessContent_m1C825919507CB5F543B58373C2F1439976C5C66E (void);
// 0x0000025B System.Int32 GameCreator.Localization.LocString::GetFirstCharacterIndex(System.String)
extern void LocString_GetFirstCharacterIndex_m5C3894E88E370D88CAD6A4A54389B8A1408B317E (void);
// 0x0000025C System.Void GameCreator.Localization.LocalizationManager::OnCreate()
extern void LocalizationManager_OnCreate_mC788076CCD62B8710CD52FC807540EF8793236F7 (void);
// 0x0000025D UnityEngine.SystemLanguage GameCreator.Localization.LocalizationManager::GetCurrentLanguage()
extern void LocalizationManager_GetCurrentLanguage_m58F85828AF75FD1555C5BAD5D01798948B3D2631 (void);
// 0x0000025E System.Void GameCreator.Localization.LocalizationManager::ChangeLanguage(UnityEngine.SystemLanguage)
extern void LocalizationManager_ChangeLanguage_mD7918AD89B2B1D636EF0203795C36EE3F57C4082 (void);
// 0x0000025F System.String GameCreator.Localization.LocalizationManager::GetUniqueName()
extern void LocalizationManager_GetUniqueName_m41AD2A1D6670BA7B43980B6EE704003ED1DA714B (void);
// 0x00000260 System.Type GameCreator.Localization.LocalizationManager::GetSaveDataType()
extern void LocalizationManager_GetSaveDataType_m4C69BBD64081E826DEA76D5FB3E839A15076C858 (void);
// 0x00000261 System.Object GameCreator.Localization.LocalizationManager::GetSaveData()
extern void LocalizationManager_GetSaveData_m09AC38A527A2689977EA3D7CA028ACDB53373019 (void);
// 0x00000262 System.Void GameCreator.Localization.LocalizationManager::ResetData()
extern void LocalizationManager_ResetData_m40C7B4E5D129ADA95AE22DEFE7D474CB3BD281BD (void);
// 0x00000263 System.Void GameCreator.Localization.LocalizationManager::OnLoad(System.Object)
extern void LocalizationManager_OnLoad_m6DB62E849DE4B1CC6994F1B8987AAA891E41523E (void);
// 0x00000264 System.Void GameCreator.Localization.LocalizationManager::.ctor()
extern void LocalizationManager__ctor_m4A685549C117DA9419A5EE08DDD8DDB5A19B9D91 (void);
// 0x00000265 System.Void GameCreator.Localization.TextLocalized::Awake()
extern void TextLocalized_Awake_mCDD92F0751D7B409EE3AC9C5F1345A05BCC6BD5A (void);
// 0x00000266 System.Void GameCreator.Localization.TextLocalized::OnEnable()
extern void TextLocalized_OnEnable_m2D375AEB4F363D6182FE54072AEC937EB08489B9 (void);
// 0x00000267 System.Void GameCreator.Localization.TextLocalized::OnDisable()
extern void TextLocalized_OnDisable_m46511998C77900FAFCA6E9AFF223B768824D2965 (void);
// 0x00000268 System.Void GameCreator.Localization.TextLocalized::OnDestroy()
extern void TextLocalized_OnDestroy_mED057D8C1E897B388626367BA2D59FD259A67E76 (void);
// 0x00000269 System.Void GameCreator.Localization.TextLocalized::OnApplicationQuit()
extern void TextLocalized_OnApplicationQuit_m0F72C9DF0B7E4EBDEE78F5A9F49E0BC9420CE87D (void);
// 0x0000026A System.Void GameCreator.Localization.TextLocalized::ChangeKey(System.String)
extern void TextLocalized_ChangeKey_m0CE6D3CB09E3A6813CD623528F88E2CBF920FA4E (void);
// 0x0000026B System.Void GameCreator.Localization.TextLocalized::UpdateText()
extern void TextLocalized_UpdateText_m792EC099FE230F4ECA5C5C3BBB577894F75A3A67 (void);
// 0x0000026C System.Void GameCreator.Localization.TextLocalized::.ctor()
extern void TextLocalized__ctor_m9E6126C2CB76D8C51278BADDB21A27B5C3FB941C (void);
// 0x0000026D System.Void GameCreator.Localization.DatabaseLocalization::BuildContentDictionary()
extern void DatabaseLocalization_BuildContentDictionary_mC3252B3E16D1B299ABD4310D1B935ABC50D0CB14 (void);
// 0x0000026E System.String GameCreator.Localization.DatabaseLocalization::GetText(GameCreator.Localization.LocString,UnityEngine.SystemLanguage)
extern void DatabaseLocalization_GetText_mBB31A15CA732B8D1CD4C4A7B02ECC9B253DB9B03 (void);
// 0x0000026F UnityEngine.SystemLanguage GameCreator.Localization.DatabaseLocalization::GetMainLanguage()
extern void DatabaseLocalization_GetMainLanguage_mA3B7AA282E2EE7FBB66853FD9BE499B07DAB01FC (void);
// 0x00000270 GameCreator.Localization.DatabaseLocalization GameCreator.Localization.DatabaseLocalization::Load()
extern void DatabaseLocalization_Load_mEFE5BD0B62E3F136A089F215FC943253D1692CCF (void);
// 0x00000271 System.Void GameCreator.Localization.DatabaseLocalization::.ctor()
extern void DatabaseLocalization__ctor_m2C765FA3FFF37B5A514183B2D6D6715F9D9E1A98 (void);
// 0x00000272 System.Void GameCreator.Localization.DatabaseLocalization/TranslationContent::.ctor()
extern void TranslationContent__ctor_mCFC977FCE2D564D5907C7DE3A10D9535CD2F609B (void);
// 0x00000273 System.Void GameCreator.Localization.DatabaseLocalization/TranslationStrings::.ctor(System.Int32)
extern void TranslationStrings__ctor_mCF12F9FFC08822DD44C7F74E912D6D18209ABC8C (void);
// 0x00000274 System.Void GameCreator.Localization.DatabaseLocalization/TranslationLanguage::.ctor()
extern void TranslationLanguage__ctor_mC56C7D588DE32303FE5E34F76FBCDD70F5EF9A11 (void);
// 0x00000275 System.Boolean GameCreator.Characters.ActionCharacterAttachment::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterAttachment_InstantExecute_m8C4D96CE49D527A73D9AC9E0D9567FA980B4CFC9 (void);
// 0x00000276 System.Void GameCreator.Characters.ActionCharacterAttachment::.ctor()
extern void ActionCharacterAttachment__ctor_mF157FC2204E9CD3B84442B9BA3E4E372942896C8 (void);
// 0x00000277 System.Boolean GameCreator.Characters.ActionCharacterDash::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterDash_InstantExecute_m016D9C2ECAAB41C15C4927F962B9D8497B329D5A (void);
// 0x00000278 System.Void GameCreator.Characters.ActionCharacterDash::.ctor()
extern void ActionCharacterDash__ctor_m9DA50E983A77B2C4F97C2BF5DD1242412B907476 (void);
// 0x00000279 System.Void GameCreator.Characters.ActionCharacterDash::.cctor()
extern void ActionCharacterDash__cctor_m02566C445BDA87220F46DA29EED8713F96FD14F5 (void);
// 0x0000027A System.Boolean GameCreator.Characters.ActionCharacterDefaultState::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterDefaultState_InstantExecute_m8F1FABE74E9BCBD0E74F13B6B61414F5E2231153 (void);
// 0x0000027B System.Void GameCreator.Characters.ActionCharacterDefaultState::.ctor()
extern void ActionCharacterDefaultState__ctor_m0C4843702BD4B87B37012E3C1578E6566D06C3AE (void);
// 0x0000027C System.Boolean GameCreator.Characters.ActionCharacterDirection::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterDirection_InstantExecute_m0C775ACF997208305E667AF28AF67AE4D305C8B1 (void);
// 0x0000027D System.Void GameCreator.Characters.ActionCharacterDirection::.ctor()
extern void ActionCharacterDirection__ctor_m69A6C0CB18202C5F3F9565D51185F244F75A8CC6 (void);
// 0x0000027E System.Boolean GameCreator.Characters.ActionCharacterFollow::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterFollow_InstantExecute_m61A078127F6F88DE8ED21853D219797A18666BA8 (void);
// 0x0000027F System.Void GameCreator.Characters.ActionCharacterFollow::.ctor()
extern void ActionCharacterFollow__ctor_m7A780C1AE3E9DCFC0807460A8BDBA0635982A962 (void);
// 0x00000280 System.Boolean GameCreator.Characters.ActionCharacterGesture::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterGesture_InstantExecute_m3DD3007D00A1FCD02115AB27E4183EFD156640A1 (void);
// 0x00000281 System.Collections.IEnumerator GameCreator.Characters.ActionCharacterGesture::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterGesture_Execute_mF079124C1F71B2CBA071614DF5D961DA403013ED (void);
// 0x00000282 System.Void GameCreator.Characters.ActionCharacterGesture::Stop()
extern void ActionCharacterGesture_Stop_mB3FA4B81606471F6479DC3945072F3DBC8FC6C63 (void);
// 0x00000283 System.Void GameCreator.Characters.ActionCharacterGesture::.ctor()
extern void ActionCharacterGesture__ctor_mEAECA3E1F17C2179CE9B7D9D05A672137867F6EA (void);
// 0x00000284 System.Void GameCreator.Characters.ActionCharacterGesture/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB7A2A28D631824113D412161EDC4A21417C5D11A (void);
// 0x00000285 System.Boolean GameCreator.Characters.ActionCharacterGesture/<>c__DisplayClass10_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CExecuteU3Eb__0_mC78DA78906F82A06AB7527D2ED0966D8C16CD0AD (void);
// 0x00000286 System.Void GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::.ctor(System.Int32)
extern void U3CExecuteU3Ed__10__ctor_m3C6F29778E65D111F6E51FFF293FF6AB4E1D8B54 (void);
// 0x00000287 System.Void GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__10_System_IDisposable_Dispose_m4E1B6B7427CEFE2C3661378F63CEA456C34B7F18 (void);
// 0x00000288 System.Boolean GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::MoveNext()
extern void U3CExecuteU3Ed__10_MoveNext_m1F096BB3032B73E9F3A60AFA4EB8957264232136 (void);
// 0x00000289 System.Object GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80DEEACBD0C640866FB5D092E8FFE518CFA782A3 (void);
// 0x0000028A System.Void GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__10_System_Collections_IEnumerator_Reset_m5DF0124AAAD326C0E6C5520768B34B7C811DBEB6 (void);
// 0x0000028B System.Object GameCreator.Characters.ActionCharacterGesture/<Execute>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__10_System_Collections_IEnumerator_get_Current_mBA7CD44299C5921344680D9462F7C272A5782DE7 (void);
// 0x0000028C System.Boolean GameCreator.Characters.ActionCharacterHand::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterHand_InstantExecute_m36A1B04105617C5AC2A97A3EE37532AF53D3CBAD (void);
// 0x0000028D System.Void GameCreator.Characters.ActionCharacterHand::.ctor()
extern void ActionCharacterHand__ctor_m50BCB72299D9CE3BA80F62C510360D5195FA5FD3 (void);
// 0x0000028E System.Boolean GameCreator.Characters.ActionCharacterIK::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterIK_InstantExecute_mC7030131D0830FDE9B6B5674D4366B9ED03DAF0A (void);
// 0x0000028F System.Void GameCreator.Characters.ActionCharacterIK::.ctor()
extern void ActionCharacterIK__ctor_m440A41B7AAB7AAF31BF3ADA13578BAA5D076EB3F (void);
// 0x00000290 System.Boolean GameCreator.Characters.ActionCharacterModel::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterModel_InstantExecute_m9A8A984A84F7EE47A3040850C855373EAEFCF5FE (void);
// 0x00000291 System.Void GameCreator.Characters.ActionCharacterModel::.ctor()
extern void ActionCharacterModel__ctor_m326DB4721490BEF9A5486B031ADCF56555D86B3D (void);
// 0x00000292 System.Boolean GameCreator.Characters.ActionCharacterMount::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterMount_InstantExecute_m8EB7506F1DD5F5B533F504B8C5DD7E1497BEC0FC (void);
// 0x00000293 System.Void GameCreator.Characters.ActionCharacterMount::.ctor()
extern void ActionCharacterMount__ctor_mE4E07F40FF9B4D5D5879D359B6E63CC38081F9AA (void);
// 0x00000294 System.Boolean GameCreator.Characters.ActionCharacterMoveTo::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterMoveTo_InstantExecute_mF374F876B0EAE7298F9A5EAC1CBB5F994055506E (void);
// 0x00000295 System.Collections.IEnumerator GameCreator.Characters.ActionCharacterMoveTo::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterMoveTo_Execute_mFDD8702F438971F618BC2CC5070F79F5D2D755B1 (void);
// 0x00000296 System.Void GameCreator.Characters.ActionCharacterMoveTo::Stop()
extern void ActionCharacterMoveTo_Stop_mE1CAC5FDC5431AEDBC7EAB28A036318168199FC3 (void);
// 0x00000297 System.Void GameCreator.Characters.ActionCharacterMoveTo::CharacterArrivedCallback()
extern void ActionCharacterMoveTo_CharacterArrivedCallback_m41E769537DB11BCB08019FEEC8CCA2C798D640B2 (void);
// 0x00000298 System.Void GameCreator.Characters.ActionCharacterMoveTo::GetTarget(GameCreator.Characters.Character,UnityEngine.GameObject,UnityEngine.Vector3&,GameCreator.Characters.ILocomotionSystem/TargetRotation&,System.Single&)
extern void ActionCharacterMoveTo_GetTarget_mDE4F8E96945D8E55CBAF320112A7773083E777BC (void);
// 0x00000299 System.Void GameCreator.Characters.ActionCharacterMoveTo::.ctor()
extern void ActionCharacterMoveTo__ctor_m197BD02D857EB24E2F751EC399EA5FBD94C3C17A (void);
// 0x0000029A System.Void GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::.ctor(System.Int32)
extern void U3CExecuteU3Ed__16__ctor_m11BFD9B002E8E393AA4F150E4D26ED9408BA65B1 (void);
// 0x0000029B System.Void GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__16_System_IDisposable_Dispose_m76698B7FC09D4CEB478E55D7D28CCDDA5FD7A03B (void);
// 0x0000029C System.Boolean GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::MoveNext()
extern void U3CExecuteU3Ed__16_MoveNext_mBCCB0DBBFB7AACF7DD74A7EC48CC56C0B52B41FB (void);
// 0x0000029D System.Object GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14CCFEB1097ADA90AF91AB66664339DC1FA086D1 (void);
// 0x0000029E System.Void GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__16_System_Collections_IEnumerator_Reset_m954AD4B93FCE4A094FE5D1EA875B8516F283DE95 (void);
// 0x0000029F System.Object GameCreator.Characters.ActionCharacterMoveTo/<Execute>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__16_System_Collections_IEnumerator_get_Current_m1819E8E5ECDC100A7ACF93D0787045CFBA63EBCD (void);
// 0x000002A0 System.Boolean GameCreator.Characters.ActionCharacterProperies::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterProperies_InstantExecute_mD4D6C34C631F5ECAF24CFD52895414F2E2B67833 (void);
// 0x000002A1 System.Void GameCreator.Characters.ActionCharacterProperies::.ctor()
extern void ActionCharacterProperies__ctor_mE77245BADA459257C41FC16CB8F4DBF94D94C641 (void);
// 0x000002A2 System.Boolean GameCreator.Characters.ActionCharacterRagdoll::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterRagdoll_InstantExecute_mF39B6F752543AA04D5E77C900F3ED8431D0BC857 (void);
// 0x000002A3 System.Void GameCreator.Characters.ActionCharacterRagdoll::.ctor()
extern void ActionCharacterRagdoll__ctor_mECA7EB049291FB52619D6FF638684A67D7835D1D (void);
// 0x000002A4 System.Boolean GameCreator.Characters.ActionCharacterRotateTowards::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterRotateTowards_InstantExecute_m86CC99C0A1FAE6F61777FBEA6AD4A50C6FBCC7F8 (void);
// 0x000002A5 System.Collections.IEnumerator GameCreator.Characters.ActionCharacterRotateTowards::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32,System.Object[])
extern void ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B (void);
// 0x000002A6 System.Void GameCreator.Characters.ActionCharacterRotateTowards::.ctor()
extern void ActionCharacterRotateTowards__ctor_m5A5EE2F838912B9496647F75EC9078A15983EB1F (void);
// 0x000002A7 System.Void GameCreator.Characters.ActionCharacterRotateTowards::.cctor()
extern void ActionCharacterRotateTowards__cctor_m633E8E24E32AFA8546526363078EC65716991C02 (void);
// 0x000002A8 System.Void GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::.ctor(System.Int32)
extern void U3CExecuteU3Ed__7__ctor_m93A9ED4176E3050FF375EC8C119F4EF8DF71FE74 (void);
// 0x000002A9 System.Void GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__7_System_IDisposable_Dispose_mC69556267AA0ECA60558F102F170DE67D89E5E2C (void);
// 0x000002AA System.Boolean GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::MoveNext()
extern void U3CExecuteU3Ed__7_MoveNext_m4F67D87C429C01EC78CB5C61F26966CB6AD2D1E6 (void);
// 0x000002AB System.Object GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF92FCFBC97E98DD5FA109F65537870A658014802 (void);
// 0x000002AC System.Void GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m28C693D9D027244014063A86657F20629EB0D302 (void);
// 0x000002AD System.Object GameCreator.Characters.ActionCharacterRotateTowards/<Execute>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m89023EE397F53C4FA098252012B715C3A57DF914 (void);
// 0x000002AE System.Boolean GameCreator.Characters.ActionCharacterState::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterState_InstantExecute_m3E9C996496A61AE2E7EFE8619041C64215E016A6 (void);
// 0x000002AF System.Void GameCreator.Characters.ActionCharacterState::.ctor()
extern void ActionCharacterState__ctor_m0DB3055CB95207A10FEEC919955E446B52DD9EE1 (void);
// 0x000002B0 System.Boolean GameCreator.Characters.ActionCharacterStateWeight::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterStateWeight_InstantExecute_m921C1DBA8FC0723E21934DD4CE712CD7056B3D78 (void);
// 0x000002B1 System.Void GameCreator.Characters.ActionCharacterStateWeight::.ctor()
extern void ActionCharacterStateWeight__ctor_mFDABAFE6C33BDCB9D9BD8435008EF02A43D84C80 (void);
// 0x000002B2 System.Boolean GameCreator.Characters.ActionCharacterStopGesture::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterStopGesture_InstantExecute_mC7A8F3652E5FAAA234E72F3A54B5D6B0BE0F1254 (void);
// 0x000002B3 System.Void GameCreator.Characters.ActionCharacterStopGesture::.ctor()
extern void ActionCharacterStopGesture__ctor_m8ADFEDC5474333898AB5F17DD9B9E18F6B4908EF (void);
// 0x000002B4 System.Boolean GameCreator.Characters.ActionCharacterStopMoving::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterStopMoving_InstantExecute_m0FF72E2A8A537B5640AAE6718D00BC2C0C883A4D (void);
// 0x000002B5 System.Void GameCreator.Characters.ActionCharacterStopMoving::.ctor()
extern void ActionCharacterStopMoving__ctor_mA2D5567662CD48F48D499E7E16EA87C328138D0B (void);
// 0x000002B6 System.Collections.IEnumerator GameCreator.Characters.ActionCharacterTeleport::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterTeleport_Execute_m9542BA250FECE50E3E269827B77E3E44633BA65E (void);
// 0x000002B7 System.Void GameCreator.Characters.ActionCharacterTeleport::.ctor()
extern void ActionCharacterTeleport__ctor_mB1E70BED18B4D4BA054C88AF179CE9C2361E2DCE (void);
// 0x000002B8 System.Void GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_m0229D5A895D47865D75B9CA9E6E669E1C2F14465 (void);
// 0x000002B9 System.Void GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_m895F9261CA1402B728553CFF4F16F2F936BF7F9C (void);
// 0x000002BA System.Boolean GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_m254B83199E4B0C4C0B2F41C65347478D8AE65A8E (void);
// 0x000002BB System.Object GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m105C970E77214FD89055FF4D88816CB5BA8E1938 (void);
// 0x000002BC System.Void GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m1A8C9CD34172B9F3BD01A4CBF933EEEE5DBC06EF (void);
// 0x000002BD System.Object GameCreator.Characters.ActionCharacterTeleport/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mE70B6EA23875886C4CFBEC39F12808AC10AEB884 (void);
// 0x000002BE System.Boolean GameCreator.Characters.ActionCharacterVisibility::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterVisibility_InstantExecute_m859CE6E81F893FD528FFEC47141215AFB9E58E92 (void);
// 0x000002BF System.Void GameCreator.Characters.ActionCharacterVisibility::.ctor()
extern void ActionCharacterVisibility__ctor_m299DB100418CCF7DBABF9AD1D23092B2570F501B (void);
// 0x000002C0 System.Boolean GameCreator.Characters.ActionHeadTrack::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionHeadTrack_InstantExecute_m5AA5607B3A75929917A34952DEFB56E4385BECB2 (void);
// 0x000002C1 System.Void GameCreator.Characters.ActionHeadTrack::.ctor()
extern void ActionHeadTrack__ctor_m53AF509981C178C41AA71241E847AA8CC8F56E62 (void);
// 0x000002C2 System.Boolean GameCreator.Characters.ActionPlayerMovementInput::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionPlayerMovementInput_InstantExecute_m3D59A8951BCF014F763E687E533E23C57D13042C (void);
// 0x000002C3 System.Void GameCreator.Characters.ActionPlayerMovementInput::.ctor()
extern void ActionPlayerMovementInput__ctor_m1115C9009DC55F16D320B9846D82CDDD03ED0080 (void);
// 0x000002C4 System.Boolean GameCreator.Characters.ConditionCharacterBusy::Check(UnityEngine.GameObject)
extern void ConditionCharacterBusy_Check_m38374431622CC8DBA8F003A13C746E2DA3E59639 (void);
// 0x000002C5 System.Void GameCreator.Characters.ConditionCharacterBusy::.ctor()
extern void ConditionCharacterBusy__ctor_mDE29B4DF1EBF05FFCCDDA6702870348D14B85402 (void);
// 0x000002C6 System.Boolean GameCreator.Characters.ConditionCharacterCapsule::Check(UnityEngine.GameObject)
extern void ConditionCharacterCapsule_Check_m360848896C6B11C0DD02AFCF39DEF4BB2C90DDDA (void);
// 0x000002C7 System.Void GameCreator.Characters.ConditionCharacterCapsule::.ctor()
extern void ConditionCharacterCapsule__ctor_mF4290842373CD5AB95C16FC6E733536179718293 (void);
// 0x000002C8 System.Boolean GameCreator.Characters.ConditionCharacterProperty::Check(UnityEngine.GameObject)
extern void ConditionCharacterProperty_Check_m97FE43C913730292488D61124FD6194FBA6EEFA9 (void);
// 0x000002C9 System.Void GameCreator.Characters.ConditionCharacterProperty::.ctor()
extern void ConditionCharacterProperty__ctor_m00E4DFF472BECD86B761311BB09385954A3BB4EF (void);
// 0x000002CA System.Void GameCreator.Characters.IgniterCharacterStep::Start()
extern void IgniterCharacterStep_Start_mE495B86E1AE65C603D5684F1F7DCA90DA44D1707 (void);
// 0x000002CB System.Void GameCreator.Characters.IgniterCharacterStep::OnDestroy()
extern void IgniterCharacterStep_OnDestroy_m5E684025468519EC328E9955EEE4A711A6D0F803 (void);
// 0x000002CC System.Void GameCreator.Characters.IgniterCharacterStep::OnStep(GameCreator.Characters.CharacterLocomotion/STEP)
extern void IgniterCharacterStep_OnStep_mB182CD26BA5145D1482B283B6F3DF32B7B7CEB06 (void);
// 0x000002CD System.Void GameCreator.Characters.IgniterCharacterStep::.ctor()
extern void IgniterCharacterStep__ctor_mF7A7300AA835F4367AAB0970F43B2A0B168709EF (void);
// 0x000002CE System.Void GameCreator.Characters.CharacterAnimation::.ctor(GameCreator.Characters.CharacterAnimator,GameCreator.Characters.CharacterState)
extern void CharacterAnimation__ctor_mECE0194E90AC33560A08A40C7A80D9B0A767D008 (void);
// 0x000002CF System.Void GameCreator.Characters.CharacterAnimation::OnDestroy()
extern void CharacterAnimation_OnDestroy_mE0A0D66618ABBAB6204FF6C52B192051F1EB6F55 (void);
// 0x000002D0 System.Void GameCreator.Characters.CharacterAnimation::ChangeRuntimeController(UnityEngine.RuntimeAnimatorController)
extern void CharacterAnimation_ChangeRuntimeController_mC6C3AE9FAC858E74E356B64CC275CDDD55E28985 (void);
// 0x000002D1 System.Void GameCreator.Characters.CharacterAnimation::Update()
extern void CharacterAnimation_Update_m075A061338D6ED160DDAEC0A80B4EB0905BBA6AD (void);
// 0x000002D2 System.Void GameCreator.Characters.CharacterAnimation::PlayGesture(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single)
extern void CharacterAnimation_PlayGesture_m5F14C1053A3B3249AF1DC6C7584DD9219B0F369E (void);
// 0x000002D3 System.Void GameCreator.Characters.CharacterAnimation::CrossFadeGesture(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single)
extern void CharacterAnimation_CrossFadeGesture_m0BD134E9120E99D9005B2FB69350CDA683CCCFB8 (void);
// 0x000002D4 System.Void GameCreator.Characters.CharacterAnimation::CrossFadeGesture(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimation_CrossFadeGesture_m1F6197A3A14AB8E9EE47EAB409CE37AC6A7BB230 (void);
// 0x000002D5 System.Void GameCreator.Characters.CharacterAnimation::StopGesture(System.Single)
extern void CharacterAnimation_StopGesture_m6B605B6064CAEA3005E49E42F6030F81FD764E1B (void);
// 0x000002D6 System.Void GameCreator.Characters.CharacterAnimation::SetState(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,System.Int32)
extern void CharacterAnimation_SetState_m5E29EBF5A63F165C4ACC4018BB45E07768406826 (void);
// 0x000002D7 System.Void GameCreator.Characters.CharacterAnimation::SetState(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,System.Int32,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimation_SetState_mF674B49D07C0F6E9E1A28331F0189379391B5CDB (void);
// 0x000002D8 System.Void GameCreator.Characters.CharacterAnimation::SetState(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,System.Int32,System.Boolean,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimation_SetState_m8C3CE69D37ECC30B3B9F9C352B2A3D365942A4D2 (void);
// 0x000002D9 System.Void GameCreator.Characters.CharacterAnimation::ResetState(System.Single,System.Int32)
extern void CharacterAnimation_ResetState_mFECB6FA55444023312D4DA06810D1BFB4FF96544 (void);
// 0x000002DA System.Void GameCreator.Characters.CharacterAnimation::ChangeStateWeight(System.Int32,System.Single)
extern void CharacterAnimation_ChangeStateWeight_m461E96C71DE77D892542D6B33D514AC35F01A0E7 (void);
// 0x000002DB GameCreator.Characters.CharacterState GameCreator.Characters.CharacterAnimation::GetState(System.Int32)
extern void CharacterAnimation_GetState_m47541F74096D58EFA514A6332366FDEB8A2E4290 (void);
// 0x000002DC System.Void GameCreator.Characters.CharacterAnimation::Setup()
extern void CharacterAnimation_Setup_m95D7FDF0D642CD8FAD9768CF04578641D673C247 (void);
// 0x000002DD System.Void GameCreator.Characters.CharacterAnimation::SetupSectionDefaultStates()
extern void CharacterAnimation_SetupSectionDefaultStates_m4FFF84A9ED4A3ACBC1BECE4E05BE5A19F755D724 (void);
// 0x000002DE System.Void GameCreator.Characters.CharacterAnimation::SetupSectionStates()
extern void CharacterAnimation_SetupSectionStates_mDD012ED29D5D2AC5569DBEAE3F49A24846B7BF4B (void);
// 0x000002DF System.Void GameCreator.Characters.CharacterAnimation::SetupSectionGestures()
extern void CharacterAnimation_SetupSectionGestures_mE15766A62A3611E6599816E523CB54B9CAC5581C (void);
// 0x000002E0 System.Int32 GameCreator.Characters.CharacterAnimation::GetSurroundingStates(System.Int32,GameCreator.Characters.PlayableState&,GameCreator.Characters.PlayableState&)
extern void CharacterAnimation_GetSurroundingStates_m79EF98DD4ADF79B342880EFF41AFC35A1DA9EC8C (void);
// 0x000002E1 UnityEngine.Playables.Playable GameCreator.Characters.PlayableBase::get_Mixer()
extern void PlayableBase_get_Mixer_m28AB0F73632EF768973B8B3B05B9C9C7CE0AA272 (void);
// 0x000002E2 UnityEngine.Playables.Playable GameCreator.Characters.PlayableBase::get_Input0()
extern void PlayableBase_get_Input0_mB9976B380FCEBC97F3D6E2984B0C94E273835240 (void);
// 0x000002E3 UnityEngine.Playables.Playable GameCreator.Characters.PlayableBase::get_Input1()
extern void PlayableBase_get_Input1_m74BFEF7D4346A2CF70DF8A9CBBA6043F4908D6E1 (void);
// 0x000002E4 UnityEngine.Playables.Playable GameCreator.Characters.PlayableBase::get_Output()
extern void PlayableBase_get_Output_m534F11974C308079426866408B7967446C3449E5 (void);
// 0x000002E5 System.Void GameCreator.Characters.PlayableBase::.ctor(UnityEngine.AvatarMask,System.Single,System.Single,System.Single,System.Single)
extern void PlayableBase__ctor_m650AB8F71D7961C0A1A635D031B2381045D46048 (void);
// 0x000002E6 System.Void GameCreator.Characters.PlayableBase::Destroy()
extern void PlayableBase_Destroy_m7ABBA0E75A397E8D32822725CD532831D57588A5 (void);
// 0x000002E7 System.Collections.IEnumerator GameCreator.Characters.PlayableBase::DestroyNextFrame()
extern void PlayableBase_DestroyNextFrame_mD1CA8E6618014C5F0FF63CFB4CC580E8481DB93C (void);
// 0x000002E8 System.Void GameCreator.Characters.PlayableBase::Stop(System.Single)
extern void PlayableBase_Stop_m656CAD3433BDDCBE1CF6B72A88B68C167B1F4DB4 (void);
// 0x000002E9 System.Boolean GameCreator.Characters.PlayableBase::Update()
// 0x000002EA System.Void GameCreator.Characters.PlayableBase::Setup(UnityEngine.Playables.PlayableGraph&,TInput0&,TInput1&,TOutput&)
// 0x000002EB System.Void GameCreator.Characters.PlayableBase::Setup(UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,TInput1&)
// 0x000002EC System.Void GameCreator.Characters.PlayableBase::Setup(UnityEngine.Playables.PlayableGraph&,TInput1&,GameCreator.Characters.PlayableBase)
// 0x000002ED System.Void GameCreator.Characters.PlayableBase::UpdateMixerWeights(System.Single)
extern void PlayableBase_UpdateMixerWeights_mEE1D0D70A595A742EC7D8C1AA4E002683BE3DF9D (void);
// 0x000002EE System.Void GameCreator.Characters.PlayableBase::SetupMixer(UnityEngine.Playables.PlayableGraph&,TInput0&,TInput1&,TOutput&)
// 0x000002EF System.Void GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::.ctor(System.Int32)
extern void U3CDestroyNextFrameU3Ed__16__ctor_mC2D8B87272A5FF7978C249A18186598873F2B420 (void);
// 0x000002F0 System.Void GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::System.IDisposable.Dispose()
extern void U3CDestroyNextFrameU3Ed__16_System_IDisposable_Dispose_mD1D6D3F8B8A5047BE75FBBFD0DCD02F05CF12971 (void);
// 0x000002F1 System.Boolean GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::MoveNext()
extern void U3CDestroyNextFrameU3Ed__16_MoveNext_m5C7E1C93D78826B5A8D6EC6CB889F7904555BE43 (void);
// 0x000002F2 System.Object GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyNextFrameU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70A9E325F1FF7756263DDB8DF254F4C1ABB5E9CD (void);
// 0x000002F3 System.Void GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_Reset_m3FB96BB157E9D36F17571507B075F3A3B60EEB6D (void);
// 0x000002F4 System.Object GameCreator.Characters.PlayableBase/<DestroyNextFrame>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_get_Current_mE41FA7C32E7D6344416ECD3407387C2AD2F5365E (void);
// 0x000002F5 System.Void GameCreator.Characters.PlayableGesture::.ctor(UnityEngine.AvatarMask,System.Single,System.Single,System.Single)
extern void PlayableGesture__ctor_mC0821B6642BA9F558D8DA138B812A84ACF119989 (void);
// 0x000002F6 System.Boolean GameCreator.Characters.PlayableGesture::Update()
extern void PlayableGesture_Update_m241AE0C17DC9C9F90AC23522F1FC358460F6821C (void);
// 0x000002F7 System.Void GameCreator.Characters.PlayableGesture::StretchDuration(System.Single)
extern void PlayableGesture_StretchDuration_m154E0D9684BD3E88E13433A9A253F79391493729 (void);
// 0x000002F8 System.Void GameCreator.Characters.PlayableGesture::Stop(System.Single)
extern void PlayableGesture_Stop_m44CB6ECC264926D3662AF8E0634D11552DB0FB28 (void);
// 0x000002F9 System.Void GameCreator.Characters.PlayableGesture/Parameter::.ctor(System.Int32,System.Single)
extern void Parameter__ctor_mF1BAEB0C446EF259DA381E3C86A2246909C7BE05 (void);
// 0x000002FA System.Void GameCreator.Characters.PlayableGesture/Parameter::.ctor(System.String,System.Single)
extern void Parameter__ctor_mF986F68A136BDB460D180D5CF86785FB563A38CF (void);
// 0x000002FB System.Void GameCreator.Characters.PlayableGestureClip::.ctor(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single)
extern void PlayableGestureClip__ctor_m145CCCB60EAC2FD5773741D32C1343F59D471370 (void);
// 0x000002FC GameCreator.Characters.PlayableGestureClip GameCreator.Characters.PlayableGestureClip::Create(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,TInput0&,TOutput&)
// 0x000002FD GameCreator.Characters.PlayableGestureClip GameCreator.Characters.PlayableGestureClip::CreateAfter(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase)
extern void PlayableGestureClip_CreateAfter_mE658AC514265D18636A9D616D41E5F4BDDC2A267 (void);
// 0x000002FE System.Void GameCreator.Characters.PlayableGestureRTC::.ctor(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single)
extern void PlayableGestureRTC__ctor_m81D3C49E928781B5677A25BB041E0BD5CD68E589 (void);
// 0x000002FF GameCreator.Characters.PlayableGestureRTC GameCreator.Characters.PlayableGestureRTC::Create(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,TInput0&,TOutput&,GameCreator.Characters.PlayableGesture/Parameter[])
// 0x00000300 GameCreator.Characters.PlayableGestureRTC GameCreator.Characters.PlayableGestureRTC::CreateAfter(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,GameCreator.Characters.PlayableGesture/Parameter[])
extern void PlayableGestureRTC_CreateAfter_m171D1CC7E4B96BD20FA8DA9906BC797FC6B791A6 (void);
// 0x00000301 System.Int32 GameCreator.Characters.PlayableState::get_Layer()
extern void PlayableState_get_Layer_mA0A0FEF3E34BB52210BEB65292E152C37F4E5305 (void);
// 0x00000302 System.Void GameCreator.Characters.PlayableState::set_Layer(System.Int32)
extern void PlayableState_set_Layer_mDBC6CF2A5E1072D454BCD7F6633D52C18E7C04E2 (void);
// 0x00000303 UnityEngine.AnimationClip GameCreator.Characters.PlayableState::get_AnimationClip()
extern void PlayableState_get_AnimationClip_m86C11355A72EB8137CA039DAB5123DD11D9AB080 (void);
// 0x00000304 System.Void GameCreator.Characters.PlayableState::set_AnimationClip(UnityEngine.AnimationClip)
extern void PlayableState_set_AnimationClip_mAC5DDE460E808A580B977179B28D9EA95CDF44A8 (void);
// 0x00000305 GameCreator.Characters.CharacterState GameCreator.Characters.PlayableState::get_CharacterState()
extern void PlayableState_get_CharacterState_m42DDC145AE77AB341C5AB3B0C78C34E0B6815A60 (void);
// 0x00000306 System.Void GameCreator.Characters.PlayableState::set_CharacterState(GameCreator.Characters.CharacterState)
extern void PlayableState_set_CharacterState_m79263DC2A30FB8420BB2EBF7197CA9B823ADA473 (void);
// 0x00000307 System.Void GameCreator.Characters.PlayableState::.ctor(UnityEngine.AvatarMask,System.Int32,System.Single,System.Single,System.Single)
extern void PlayableState__ctor_m03254C46A94C917CC537877C639A3F50281F3951 (void);
// 0x00000308 System.Boolean GameCreator.Characters.PlayableState::Update()
extern void PlayableState_Update_m5A05BE2C69C7AF3E45EA0318C337C00D130A8A04 (void);
// 0x00000309 System.Void GameCreator.Characters.PlayableState::Stop(System.Single)
extern void PlayableState_Stop_m5F62B094147F46E2EB83BBC5BE107F7951E4DDE2 (void);
// 0x0000030A System.Void GameCreator.Characters.PlayableState::StretchDuration(System.Single)
extern void PlayableState_StretchDuration_mB01FD587E576FDFBE3E8BB47BE6A99F31F48EF32 (void);
// 0x0000030B System.Void GameCreator.Characters.PlayableState::SetWeight(System.Single)
extern void PlayableState_SetWeight_mF5B2ABBE5E328232A82947BE35C67301BB5FC039 (void);
// 0x0000030C System.Void GameCreator.Characters.PlayableState::OnExitState()
extern void PlayableState_OnExitState_m8C7088B95C61444B365ECFCF357247A99E11378C (void);
// 0x0000030D System.Void GameCreator.Characters.PlayableStateCharacter::.ctor(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,GameCreator.Characters.CharacterAnimation,System.Int32,System.Single,System.Single,System.Single)
extern void PlayableStateCharacter__ctor_mD647A3BFD771CACA7FE548625FAB3CEE0F2AFD2F (void);
// 0x0000030E GameCreator.Characters.PlayableStateCharacter GameCreator.Characters.PlayableStateCharacter::Create(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,GameCreator.Characters.CharacterAnimation,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,TInput0&,TOutput&,GameCreator.Characters.PlayableGesture/Parameter[])
// 0x0000030F GameCreator.Characters.PlayableStateCharacter GameCreator.Characters.PlayableStateCharacter::CreateAfter(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,GameCreator.Characters.CharacterAnimation,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,GameCreator.Characters.PlayableGesture/Parameter[])
extern void PlayableStateCharacter_CreateAfter_mD3174E0DE0FB1886551378074FC02AE8BC6FFDF9 (void);
// 0x00000310 GameCreator.Characters.PlayableStateCharacter GameCreator.Characters.PlayableStateCharacter::CreateBefore(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,GameCreator.Characters.CharacterAnimation,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,GameCreator.Characters.PlayableGesture/Parameter[])
extern void PlayableStateCharacter_CreateBefore_m541F003F91662EAF4A5C74B16127F0A19D9816D0 (void);
// 0x00000311 System.Void GameCreator.Characters.PlayableStateCharacter::OnExitState()
extern void PlayableStateCharacter_OnExitState_m2C8B41F85579FA8551AA153FDB7CB5D8B31EF611 (void);
// 0x00000312 System.Void GameCreator.Characters.PlayableStateClip::.ctor(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Int32,System.Single,System.Single,System.Single)
extern void PlayableStateClip__ctor_mC98302A89889583FD869E9B956EDB75EABEB5877 (void);
// 0x00000313 GameCreator.Characters.PlayableStateClip GameCreator.Characters.PlayableStateClip::Create(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,TInput0&,TOutput&)
// 0x00000314 GameCreator.Characters.PlayableState GameCreator.Characters.PlayableStateClip::CreateAfter(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase)
extern void PlayableStateClip_CreateAfter_mF118D102D71C55D7FE69D4BA09F8E1CB97C85853 (void);
// 0x00000315 GameCreator.Characters.PlayableState GameCreator.Characters.PlayableStateClip::CreateBefore(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase)
extern void PlayableStateClip_CreateBefore_mE3EF4040D5DC5C75F251247016DF41D30822D95D (void);
// 0x00000316 System.Void GameCreator.Characters.PlayableStateRTC::.ctor(UnityEngine.AvatarMask,System.Int32,System.Single,System.Single,System.Single)
extern void PlayableStateRTC__ctor_m72212DA8439BD43B0DBE45A0CE92C4D632304BE0 (void);
// 0x00000317 GameCreator.Characters.PlayableStateRTC GameCreator.Characters.PlayableStateRTC::Create(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,TInput0&,TOutput&,GameCreator.Characters.PlayableGesture/Parameter[])
// 0x00000318 GameCreator.Characters.PlayableStateRTC GameCreator.Characters.PlayableStateRTC::CreateAfter(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,GameCreator.Characters.PlayableGesture/Parameter[])
extern void PlayableStateRTC_CreateAfter_m04896D425083377B366B74F0B217FA12F4EC00CE (void);
// 0x00000319 GameCreator.Characters.PlayableStateRTC GameCreator.Characters.PlayableStateRTC::CreateBefore(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Int32,System.Double,System.Single,System.Single,System.Single,UnityEngine.Playables.PlayableGraph&,GameCreator.Characters.PlayableBase,GameCreator.Characters.PlayableGesture/Parameter[])
extern void PlayableStateRTC_CreateBefore_mDB2045A10400A6AF3BFA8F6DC46B31723666487D (void);
// 0x0000031A System.Void GameCreator.Characters.PlayableStateRTC::Stop(System.Single)
extern void PlayableStateRTC_Stop_mD2406F0E620E7696D7E8A0A8C7521DF8415A9FDC (void);
// 0x0000031B System.Void GameCreator.Characters.Character::Awake()
extern void Character_Awake_mED9A7131CB9A629E5F46DBAD873311E6980741B2 (void);
// 0x0000031C System.Void GameCreator.Characters.Character::CharacterAwake()
extern void Character_CharacterAwake_m54D3EC70E92083086903DA36B04C8486C530A1B8 (void);
// 0x0000031D System.Void GameCreator.Characters.Character::OnDestroy()
extern void Character_OnDestroy_m70374F80918FB9C8EECED4A860EAD76A48B86FBC (void);
// 0x0000031E System.Void GameCreator.Characters.Character::Update()
extern void Character_Update_mCE933E81829FEA4EE700DC6651DAF1B9A567F286 (void);
// 0x0000031F System.Void GameCreator.Characters.Character::CharacterUpdate()
extern void Character_CharacterUpdate_m77B076A3B3355234FB8E6A3DB12125DF29207BA0 (void);
// 0x00000320 System.Void GameCreator.Characters.Character::LateUpdate()
extern void Character_LateUpdate_mF4E74D67160996B8084010604C3B4D8AA43639CC (void);
// 0x00000321 GameCreator.Characters.Character/State GameCreator.Characters.Character::GetCharacterState()
extern void Character_GetCharacterState_m68567F9897365B45D5964C025D9680906D630EBE (void);
// 0x00000322 System.Void GameCreator.Characters.Character::SetRagdoll(System.Boolean,System.Boolean)
extern void Character_SetRagdoll_m75C7E23741ABC5089AB8A99D0E05FCF0D6499E51 (void);
// 0x00000323 System.Void GameCreator.Characters.Character::InitializeRagdoll()
extern void Character_InitializeRagdoll_m200151443177A1F8AEEC651B7079400C9B518C6E (void);
// 0x00000324 System.Boolean GameCreator.Characters.Character::IsControllable()
extern void Character_IsControllable_m95A5C59804E3634FA827FF28A6A3DDDD5C1960A8 (void);
// 0x00000325 System.Boolean GameCreator.Characters.Character::IsRagdoll()
extern void Character_IsRagdoll_mCD8015DF95BEE0E090096A9CBE58E8630D88ABE1 (void);
// 0x00000326 System.Int32 GameCreator.Characters.Character::GetCharacterMotion()
extern void Character_GetCharacterMotion_m7DB7D931421ECDAC903DAA11285E1D007465DDC3 (void);
// 0x00000327 System.Boolean GameCreator.Characters.Character::IsGrounded()
extern void Character_IsGrounded_m3E5D9076238AFCE0E41C58783024BC44592C7B16 (void);
// 0x00000328 GameCreator.Characters.CharacterAnimator GameCreator.Characters.Character::GetCharacterAnimator()
extern void Character_GetCharacterAnimator_m4D0A42BE455D4213B9DAE6E1D9F6CD786C038F85 (void);
// 0x00000329 System.Boolean GameCreator.Characters.Character::Dash(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void Character_Dash_m28458E6255E78E1EDC7733649798FE8FF8D0CBE9 (void);
// 0x0000032A System.Void GameCreator.Characters.Character::RootMovement(System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern void Character_RootMovement_m1521C3C93C6F55AB4E475DD2088E29A11360EEBF (void);
// 0x0000032B System.Void GameCreator.Characters.Character::Jump(System.Single)
extern void Character_Jump_mB3EC45C3D5C12468AA8017649F34F457857B3138 (void);
// 0x0000032C System.Void GameCreator.Characters.Character::Jump()
extern void Character_Jump_mE9FBB53697D3BEE4ADED7760E55A0F8270705999 (void);
// 0x0000032D System.Collections.IEnumerator GameCreator.Characters.Character::DelayJump(System.Single,System.Single)
extern void Character_DelayJump_mA4414CFD1241AEB812807E713BC32E7980413D8F (void);
// 0x0000032E GameCreator.Characters.CharacterHeadTrack GameCreator.Characters.Character::GetHeadTracker()
extern void Character_GetHeadTracker_m75B5A41C47A852D742BA9379BE2263AAB8317422 (void);
// 0x0000032F System.Void GameCreator.Characters.Character::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void Character_OnControllerColliderHit_mA49F17FB37338C35270DE89D669684482FB3CD78 (void);
// 0x00000330 System.Void GameCreator.Characters.Character::OnDrawGizmos()
extern void Character_OnDrawGizmos_mE478E2139FE2EA470C0D68026783EEB823051B58 (void);
// 0x00000331 System.String GameCreator.Characters.Character::GetUniqueName()
extern void Character_GetUniqueName_m030834FAA246A837464308CFB2BBA931B0DB8D68 (void);
// 0x00000332 System.String GameCreator.Characters.Character::GetUniqueCharacterID()
extern void Character_GetUniqueCharacterID_m25619D4E11FA2460C943450BAD85AC33A445BEFA (void);
// 0x00000333 System.Type GameCreator.Characters.Character::GetSaveDataType()
extern void Character_GetSaveDataType_m342DE4C439F4724437C6CD1AA6C7FE74B0D1F27E (void);
// 0x00000334 System.Object GameCreator.Characters.Character::GetSaveData()
extern void Character_GetSaveData_m76CB0996BEFD6CD0443AAA11B20F01F0F21B88D1 (void);
// 0x00000335 System.Void GameCreator.Characters.Character::ResetData()
extern void Character_ResetData_mE84D45B5C160F57F7ADF62699414558611E9E8EF (void);
// 0x00000336 System.Void GameCreator.Characters.Character::OnLoad(System.Object)
extern void Character_OnLoad_mD307A35F4E81C6F1130675D79D460648C61AA56A (void);
// 0x00000337 System.Void GameCreator.Characters.Character::.ctor()
extern void Character__ctor_m3AE630DAD4574CF8EE5FFD22569FE1549B257A6D (void);
// 0x00000338 System.Void GameCreator.Characters.Character/State::.ctor()
extern void State__ctor_m2F9B40CA1AD0962A7A35D317633C70079D769EE3 (void);
// 0x00000339 System.Void GameCreator.Characters.Character/SaveData::.ctor()
extern void SaveData__ctor_m2D3D6C748A10DE71997658380BB2C2E10F515615 (void);
// 0x0000033A System.Boolean GameCreator.Characters.Character/OnLoadSceneData::get_active()
extern void OnLoadSceneData_get_active_m34FF82B6AF6DD2D3E928D916780B138E9719C367 (void);
// 0x0000033B System.Void GameCreator.Characters.Character/OnLoadSceneData::set_active(System.Boolean)
extern void OnLoadSceneData_set_active_m0BA91835FFFE3C402173307019ACB40D307A391C (void);
// 0x0000033C UnityEngine.Vector3 GameCreator.Characters.Character/OnLoadSceneData::get_position()
extern void OnLoadSceneData_get_position_m5B68052A77DBC44449874754803BAE3644D63890 (void);
// 0x0000033D System.Void GameCreator.Characters.Character/OnLoadSceneData::set_position(UnityEngine.Vector3)
extern void OnLoadSceneData_set_position_m093315344BB72869F08A67A90158EE06ADA7CEC2 (void);
// 0x0000033E UnityEngine.Quaternion GameCreator.Characters.Character/OnLoadSceneData::get_rotation()
extern void OnLoadSceneData_get_rotation_mBD41328E6A7445448AE5836E0334AF7F452599CD (void);
// 0x0000033F System.Void GameCreator.Characters.Character/OnLoadSceneData::set_rotation(UnityEngine.Quaternion)
extern void OnLoadSceneData_set_rotation_m6D4D33972A156DECD91F919E2CE0BE33EC8E560C (void);
// 0x00000340 System.Void GameCreator.Characters.Character/OnLoadSceneData::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void OnLoadSceneData__ctor_mBDEDC1F95221E0AB08F8239614823D67A0B6610D (void);
// 0x00000341 System.Void GameCreator.Characters.Character/OnLoadSceneData::Consume()
extern void OnLoadSceneData_Consume_mBBB092EFDB254DF931610771381FF95F6C2C749B (void);
// 0x00000342 System.Void GameCreator.Characters.Character/LandEvent::.ctor()
extern void LandEvent__ctor_mB51AD3C6A4A30D43D5D67B4F3175AB5D75742D93 (void);
// 0x00000343 System.Void GameCreator.Characters.Character/JumpEvent::.ctor()
extern void JumpEvent__ctor_m63709847FA7E6A68E6BBE033083033769ED273D5 (void);
// 0x00000344 System.Void GameCreator.Characters.Character/DashEvent::.ctor()
extern void DashEvent__ctor_mCD418C2506DDF6BA05D3DA88AF79EECA9D183BD4 (void);
// 0x00000345 System.Void GameCreator.Characters.Character/StepEvent::.ctor()
extern void StepEvent__ctor_m6F5BA53C8A4943C241FF6D34052CB45E85D19FD8 (void);
// 0x00000346 System.Void GameCreator.Characters.Character/IsControllableEvent::.ctor()
extern void IsControllableEvent__ctor_m886CE2A1C162B6AD611FAD83E2B072580AE5036F (void);
// 0x00000347 System.Void GameCreator.Characters.Character/<DelayJump>d__38::.ctor(System.Int32)
extern void U3CDelayJumpU3Ed__38__ctor_m45DB7082FB55923B86B2ED0DEF78A2C72F7DD8DF (void);
// 0x00000348 System.Void GameCreator.Characters.Character/<DelayJump>d__38::System.IDisposable.Dispose()
extern void U3CDelayJumpU3Ed__38_System_IDisposable_Dispose_mB0D6B4E55D63E183B283D9DDD648BDF1F006DB49 (void);
// 0x00000349 System.Boolean GameCreator.Characters.Character/<DelayJump>d__38::MoveNext()
extern void U3CDelayJumpU3Ed__38_MoveNext_mE1E0C27879EA6DF628DC4EA51E24C5447F02E089 (void);
// 0x0000034A System.Object GameCreator.Characters.Character/<DelayJump>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayJumpU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDA92A55DECFFF61C95B266DE83FCC68616C1187 (void);
// 0x0000034B System.Void GameCreator.Characters.Character/<DelayJump>d__38::System.Collections.IEnumerator.Reset()
extern void U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_Reset_m3EAD32CF2878187EF54DD5E5BBBCEDEF3188B42C (void);
// 0x0000034C System.Object GameCreator.Characters.Character/<DelayJump>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_get_Current_m80E2FDA7B04AEE391BD2B84A8D27DD146AA6FBBB (void);
// 0x0000034D System.Void GameCreator.Characters.CharacterAnimator::Awake()
extern void CharacterAnimator_Awake_m7D3A74B396FEF30FE3DCFF06DB35E645622C50CF (void);
// 0x0000034E System.Void GameCreator.Characters.CharacterAnimator::Start()
extern void CharacterAnimator_Start_mAD07E0EE35BC294EB1D388CD3CD870ADE2F723FB (void);
// 0x0000034F System.Void GameCreator.Characters.CharacterAnimator::OnDestroy()
extern void CharacterAnimator_OnDestroy_mF980963DC38FBBB033C2E085A5B152B435A18715 (void);
// 0x00000350 System.Void GameCreator.Characters.CharacterAnimator::Update()
extern void CharacterAnimator_Update_m69D95BFF307AD4B355E136C2BC0EE890A58C1825 (void);
// 0x00000351 System.Void GameCreator.Characters.CharacterAnimator::Normals(GameCreator.Characters.Character/State)
extern void CharacterAnimator_Normals_m160F117803B1C7E93E23EB5EBE611EC617DF7492 (void);
// 0x00000352 System.Void GameCreator.Characters.CharacterAnimator::LateUpdate()
extern void CharacterAnimator_LateUpdate_m781C3FC6008DB69D08A47DAF49F024422987A360 (void);
// 0x00000353 GameCreator.Characters.CharacterHeadTrack GameCreator.Characters.CharacterAnimator::GetHeadTracker()
extern void CharacterAnimator_GetHeadTracker_m11E3517D300D9B6E54E46B743F9B0A5428B94284 (void);
// 0x00000354 System.Void GameCreator.Characters.CharacterAnimator::Jump(System.Int32)
extern void CharacterAnimator_Jump_m70EA5614660EB7C0CF6586517A817A88D82E8971 (void);
// 0x00000355 System.Void GameCreator.Characters.CharacterAnimator::Dash()
extern void CharacterAnimator_Dash_m50E88FFB56490482B3B738A516DD2E480A086F6D (void);
// 0x00000356 UnityEngine.Transform GameCreator.Characters.CharacterAnimator::GetHeadTransform()
extern void CharacterAnimator_GetHeadTransform_m4F4FDA6EB1B0BFE8F517FD5256151AF7EA1974A1 (void);
// 0x00000357 System.Void GameCreator.Characters.CharacterAnimator::PlayGesture(UnityEngine.AnimationClip,System.Single,UnityEngine.AvatarMask,System.Single,System.Single)
extern void CharacterAnimator_PlayGesture_m6A99841BF6485031541946941D06E50AD5BBE174 (void);
// 0x00000358 System.Void GameCreator.Characters.CharacterAnimator::CrossFadeGesture(UnityEngine.AnimationClip,System.Single,UnityEngine.AvatarMask,System.Single,System.Single)
extern void CharacterAnimator_CrossFadeGesture_mB07C15E6C9BCDE96FA3CECAC0949BEF94976BB30 (void);
// 0x00000359 System.Void GameCreator.Characters.CharacterAnimator::CrossFadeGesture(UnityEngine.RuntimeAnimatorController,System.Single,UnityEngine.AvatarMask,System.Single,System.Single,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimator_CrossFadeGesture_mE39F18534A58C1963F900C2D6FE97E8CADA6AF66 (void);
// 0x0000035A System.Void GameCreator.Characters.CharacterAnimator::StopGesture(System.Single)
extern void CharacterAnimator_StopGesture_m2E55CBC9F54879B0A8F4813E3AF71464BD476817 (void);
// 0x0000035B System.Void GameCreator.Characters.CharacterAnimator::SetState(GameCreator.Characters.CharacterState,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,GameCreator.Characters.CharacterAnimation/Layer,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimator_SetState_mB85A38CEDE7CC182D218720577DC5E982BF28ADA (void);
// 0x0000035C System.Void GameCreator.Characters.CharacterAnimator::SetState(UnityEngine.RuntimeAnimatorController,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,GameCreator.Characters.CharacterAnimation/Layer,System.Boolean,GameCreator.Characters.PlayableGesture/Parameter[])
extern void CharacterAnimator_SetState_m2A2E1EC36A2F8F29C58943482954A613185F21CC (void);
// 0x0000035D System.Void GameCreator.Characters.CharacterAnimator::SetState(UnityEngine.AnimationClip,UnityEngine.AvatarMask,System.Single,System.Single,System.Single,GameCreator.Characters.CharacterAnimation/Layer)
extern void CharacterAnimator_SetState_m98021864BF27BE41A4D2C8174FA463AE0FD09723 (void);
// 0x0000035E System.Void GameCreator.Characters.CharacterAnimator::ResetState(System.Single,GameCreator.Characters.CharacterAnimation/Layer)
extern void CharacterAnimator_ResetState_m9D3A5C40E61D6DDA76115EFBB84845D4F222320B (void);
// 0x0000035F System.Void GameCreator.Characters.CharacterAnimator::ChangeStateWeight(GameCreator.Characters.CharacterAnimation/Layer,System.Single)
extern void CharacterAnimator_ChangeStateWeight_m5CFCB376A823C4386AD42CBDC0B5643931DBC38B (void);
// 0x00000360 System.Void GameCreator.Characters.CharacterAnimator::ResetControllerTopology(UnityEngine.RuntimeAnimatorController)
extern void CharacterAnimator_ResetControllerTopology_m62DB64A99A9BF35A60B419E9EBA8D14EF39AC8C4 (void);
// 0x00000361 GameCreator.Characters.CharacterAttachments GameCreator.Characters.CharacterAnimator::GetCharacterAttachments()
extern void CharacterAnimator_GetCharacterAttachments_mEE41166651BF33C8D5724DB56AD5CE5A85E2F77C (void);
// 0x00000362 System.Void GameCreator.Characters.CharacterAnimator::SetCharacterAttachments(GameCreator.Characters.CharacterAttachments)
extern void CharacterAnimator_SetCharacterAttachments_mB01B85DBC98B935032CBECED8255590DB275F17D (void);
// 0x00000363 GameCreator.Characters.CharacterHandIK GameCreator.Characters.CharacterAnimator::GetCharacterHandIK()
extern void CharacterAnimator_GetCharacterHandIK_m0474121F388BFB2063ED666F2051EFB1832DBDB5 (void);
// 0x00000364 GameCreator.Characters.CharacterState GameCreator.Characters.CharacterAnimator::GetState(GameCreator.Characters.CharacterAnimation/Layer)
extern void CharacterAnimator_GetState_mFC74DD09DB02891953DB06266DF4B63E1F21F1CF (void);
// 0x00000365 System.Void GameCreator.Characters.CharacterAnimator::ChangeModel(UnityEngine.GameObject)
extern void CharacterAnimator_ChangeModel_m608C46BF40C24F434857EDB52B5974C33CD030FC (void);
// 0x00000366 System.Void GameCreator.Characters.CharacterAnimator::SetRotation(UnityEngine.Quaternion)
extern void CharacterAnimator_SetRotation_m771A41F6FFC0FFF9782FB917D91474BE321BDFFA (void);
// 0x00000367 System.Void GameCreator.Characters.CharacterAnimator::SetRotationPitch(System.Single)
extern void CharacterAnimator_SetRotationPitch_m4F97E58DEE05260E474DE1429E3603454FA17C5F (void);
// 0x00000368 System.Void GameCreator.Characters.CharacterAnimator::SetRotationYaw(System.Single)
extern void CharacterAnimator_SetRotationYaw_mEEFC7EA3ACA97469ED90021AD37B45199A3ED149 (void);
// 0x00000369 System.Void GameCreator.Characters.CharacterAnimator::SetRotationRoll(System.Single)
extern void CharacterAnimator_SetRotationRoll_m64ED09C52C74679A9A76D6C8D10444988926F597 (void);
// 0x0000036A UnityEngine.Quaternion GameCreator.Characters.CharacterAnimator::GetCurrentRotation()
extern void CharacterAnimator_GetCurrentRotation_mE4A8FEF1F41A89507279EBF7AA8A912BAFDF22DF (void);
// 0x0000036B UnityEngine.Quaternion GameCreator.Characters.CharacterAnimator::GetTargetRotation()
extern void CharacterAnimator_GetTargetRotation_mB50FF0A4C8A8C9E547EF7B7D296FDE1ECCF2049D (void);
// 0x0000036C System.Void GameCreator.Characters.CharacterAnimator::SetVisibility(System.Boolean)
extern void CharacterAnimator_SetVisibility_mE77B48344F2EF82EB35C8D265D4D43CA04124EB0 (void);
// 0x0000036D System.Void GameCreator.Characters.CharacterAnimator::SetStiffBody(System.Boolean)
extern void CharacterAnimator_SetStiffBody_m1BC56E6D7D715403A8441CBCCC64CBF80803D79F (void);
// 0x0000036E System.Void GameCreator.Characters.CharacterAnimator::SetActiveWeightFeetIK(System.Boolean)
extern void CharacterAnimator_SetActiveWeightFeetIK_mF7E18201D33488D52DBABC3E0639BC2EE00C2E5A (void);
// 0x0000036F System.Void GameCreator.Characters.CharacterAnimator::SetActiveWeightHandsIK(System.Boolean)
extern void CharacterAnimator_SetActiveWeightHandsIK_m3334D710A16059C03F7DBD24DDA49B472BF5BD9C (void);
// 0x00000370 System.Void GameCreator.Characters.CharacterAnimator::SetActiveWeightHeadIK(System.Boolean)
extern void CharacterAnimator_SetActiveWeightHeadIK_mA415BE63377F2EBD293BED1C7F625D7310B68362 (void);
// 0x00000371 System.Void GameCreator.Characters.CharacterAnimator::OnLand(System.Single)
extern void CharacterAnimator_OnLand_m31AC65D6D9554F643492077F04EAE75FC0EF3000 (void);
// 0x00000372 System.Void GameCreator.Characters.CharacterAnimator::GenerateAnimatorEvents()
extern void CharacterAnimator_GenerateAnimatorEvents_m81D08BA8AB16ED24413A83D582DF446DDDBD1BED (void);
// 0x00000373 System.Void GameCreator.Characters.CharacterAnimator::GenerateCharacterAttachments()
extern void CharacterAnimator_GenerateCharacterAttachments_mEBA23B103C1A5F7CF28C52665B021BB49BE05B99 (void);
// 0x00000374 System.Void GameCreator.Characters.CharacterAnimator::GenerateFootIK()
extern void CharacterAnimator_GenerateFootIK_mDD1766FD31E62AC935F5AFD17843655596362200 (void);
// 0x00000375 System.Void GameCreator.Characters.CharacterAnimator::GenerateHandIK()
extern void CharacterAnimator_GenerateHandIK_m64886E345AF929D0A3AAA61BBA12C3B218E0AD3D (void);
// 0x00000376 System.Void GameCreator.Characters.CharacterAnimator::.ctor()
extern void CharacterAnimator__ctor_m71188D89AF9F503C8D74F30DECB693E0532F3827 (void);
// 0x00000377 System.Void GameCreator.Characters.CharacterAnimator::.cctor()
extern void CharacterAnimator__cctor_m52EDAA71E163254FFA22D4F645C83806C087711C (void);
// 0x00000378 System.Single GameCreator.Characters.CharacterAnimator/AnimFloat::Get(System.Single,System.Single)
extern void AnimFloat_Get_mE94BCFBFF3B3C281DCD4E1FFE09A1F8F6618988A (void);
// 0x00000379 System.Void GameCreator.Characters.CharacterAnimator/AnimFloat::Set(System.Single)
extern void AnimFloat_Set_mD2A43B03C2E1AF1A00C1979B69E204E6CE611327 (void);
// 0x0000037A System.Void GameCreator.Characters.CharacterAnimator/AnimFloat::.ctor()
extern void AnimFloat__ctor_mFB5CD7333836A619A167C717AC19F0EB98BAEEE6 (void);
// 0x0000037B System.Void GameCreator.Characters.CharacterAnimator/EventIK::.ctor()
extern void EventIK__ctor_mBA724C20B14A42A58B2BE4B018B7B2A8E8769EA7 (void);
// 0x0000037C System.Void GameCreator.Characters.CharacterAnimatorEvents::Setup(GameCreator.Characters.Character)
extern void CharacterAnimatorEvents_Setup_m2153714397E54B1BCACBF7186C08A89E0557E05F (void);
// 0x0000037D System.Void GameCreator.Characters.CharacterAnimatorEvents::Event_KeepPosition(System.Single)
extern void CharacterAnimatorEvents_Event_KeepPosition_m887AE976BAF1C4C2F165B1CED799C4AB5D744DB8 (void);
// 0x0000037E System.Void GameCreator.Characters.CharacterAnimatorEvents::Event_KeepMovement(System.Single)
extern void CharacterAnimatorEvents_Event_KeepMovement_mD015EA5A181760D0C67DFE7EDA4B5CAFD4ABA2F6 (void);
// 0x0000037F System.Void GameCreator.Characters.CharacterAnimatorEvents::Event_BothSteps(UnityEngine.AnimationEvent)
extern void CharacterAnimatorEvents_Event_BothSteps_mF3605B212A829D43323886EC0400E81C7CCDF13C (void);
// 0x00000380 System.Void GameCreator.Characters.CharacterAnimatorEvents::Event_LeftStep(UnityEngine.AnimationEvent)
extern void CharacterAnimatorEvents_Event_LeftStep_m79727B9CB7ABC5930C55E8B3C02B43966E91C471 (void);
// 0x00000381 System.Void GameCreator.Characters.CharacterAnimatorEvents::Event_RightStep(UnityEngine.AnimationEvent)
extern void CharacterAnimatorEvents_Event_RightStep_mD66E5F4EFBF847959A27CACB751690E255F92F82 (void);
// 0x00000382 System.Collections.IEnumerator GameCreator.Characters.CharacterAnimatorEvents::CoroutineTrigger(GameCreator.Characters.CharacterLocomotion/ANIM_CONSTRAINT,System.Single)
extern void CharacterAnimatorEvents_CoroutineTrigger_m9088D17F2E9CAA8C76F641F7F41BC210DDD32476 (void);
// 0x00000383 System.Void GameCreator.Characters.CharacterAnimatorEvents::.ctor()
extern void CharacterAnimatorEvents__ctor_m17D5916ECE927C47A2EEA8C802B8CC3B7AF9A09C (void);
// 0x00000384 System.Void GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::.ctor(System.Int32)
extern void U3CCoroutineTriggerU3Ed__7__ctor_mB1B3F3D66017ACD041A3DF472B357151D137719B (void);
// 0x00000385 System.Void GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::System.IDisposable.Dispose()
extern void U3CCoroutineTriggerU3Ed__7_System_IDisposable_Dispose_mDF30931154439004D31B2A026851F8AA63ACDDBA (void);
// 0x00000386 System.Boolean GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::MoveNext()
extern void U3CCoroutineTriggerU3Ed__7_MoveNext_m163C09A7EDE63ECFC916361BDB7C24D480CD6CC4 (void);
// 0x00000387 System.Object GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineTriggerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D7C9AC15EAC985E0945CE394972822428370F2D (void);
// 0x00000388 System.Void GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_Reset_m623E4A129BD91E66D1397F77758386EBF64A20F5 (void);
// 0x00000389 System.Object GameCreator.Characters.CharacterAnimatorEvents/<CoroutineTrigger>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_get_Current_mC44FB9A3B262F7673714DD3EDA5E77E5891DBCB5 (void);
// 0x0000038A UnityEngine.Quaternion GameCreator.Characters.CharacterAnimatorRotation::Update()
extern void CharacterAnimatorRotation_Update_mCB7ADFF8421C5F30627142DAFF39E63E29620B76 (void);
// 0x0000038B UnityEngine.Quaternion GameCreator.Characters.CharacterAnimatorRotation::GetCurrentRotation()
extern void CharacterAnimatorRotation_GetCurrentRotation_mB10F2A567B44E6F8633AC001DF219BFC0CF448C2 (void);
// 0x0000038C UnityEngine.Quaternion GameCreator.Characters.CharacterAnimatorRotation::GetTargetRotation()
extern void CharacterAnimatorRotation_GetTargetRotation_mFAD6C43FA587E5B003F36FA3604289F08600C2DB (void);
// 0x0000038D System.Void GameCreator.Characters.CharacterAnimatorRotation::SetPitch(System.Single)
extern void CharacterAnimatorRotation_SetPitch_m7082BCD17CCA5DA9CECED257F88470FEA579A80C (void);
// 0x0000038E System.Void GameCreator.Characters.CharacterAnimatorRotation::SetYaw(System.Single)
extern void CharacterAnimatorRotation_SetYaw_mBAC82FC120FF961C4C506EED5A1667FBEC4E83A0 (void);
// 0x0000038F System.Void GameCreator.Characters.CharacterAnimatorRotation::SetRoll(System.Single)
extern void CharacterAnimatorRotation_SetRoll_m0BD0FF62E1D43742B0AC88D6A34166E9F0B82B81 (void);
// 0x00000390 System.Void GameCreator.Characters.CharacterAnimatorRotation::SetQuaternion(UnityEngine.Quaternion)
extern void CharacterAnimatorRotation_SetQuaternion_mCA5ABEBC4A6A949ADBDE586608AE60F5C0D776EA (void);
// 0x00000391 System.Void GameCreator.Characters.CharacterAnimatorRotation::.ctor()
extern void CharacterAnimatorRotation__ctor_m99F256F0ED9D8D321CBCEB02F0D6ECD4E6BF32FB (void);
// 0x00000392 System.Single GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::get_target()
extern void AnimFloat_get_target_m0B4324C67138CD8F6AAC5BC84888EFF7A978F1DF (void);
// 0x00000393 System.Void GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::set_target(System.Single)
extern void AnimFloat_set_target_m189D5417D456FCEBB0DD9F0DDBB4322E5AAB4A1D (void);
// 0x00000394 System.Single GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::get_value()
extern void AnimFloat_get_value_m2CDBD910B33AA9C38C3E807BACAC38C1978F9D63 (void);
// 0x00000395 System.Void GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::set_value(System.Single)
extern void AnimFloat_set_value_mC1E3E842276BA71CA344134C5593569748D6584E (void);
// 0x00000396 System.Void GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::.ctor(System.Single)
extern void AnimFloat__ctor_m7B67EA6E0CFF0A719873BC6574A47A2B7A3C1FE0 (void);
// 0x00000397 System.Single GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::Update()
extern void AnimFloat_Update_m28E55E472FDA689A31711FBA94C3D8589E5D1192 (void);
// 0x00000398 System.Void GameCreator.Characters.CharacterAnimatorRotation/AnimFloat::SetTarget(System.Single)
extern void AnimFloat_SetTarget_mA44BC6F1D0580F1351C563CAEABA602C6D0B8357 (void);
// 0x00000399 System.Collections.Generic.Dictionary`2<UnityEngine.HumanBodyBones,System.Collections.Generic.List`1<GameCreator.Characters.CharacterAttachments/Attachment>> GameCreator.Characters.CharacterAttachments::get_attachments()
extern void CharacterAttachments_get_attachments_m0CB2DB2A159F8F311D19A4707283B2F5C9213D50 (void);
// 0x0000039A System.Void GameCreator.Characters.CharacterAttachments::set_attachments(System.Collections.Generic.Dictionary`2<UnityEngine.HumanBodyBones,System.Collections.Generic.List`1<GameCreator.Characters.CharacterAttachments/Attachment>>)
extern void CharacterAttachments_set_attachments_m8FDDE1DF5B13AEA89E97636F72BBF4222ECDCD08 (void);
// 0x0000039B System.Void GameCreator.Characters.CharacterAttachments::Setup(UnityEngine.Animator)
extern void CharacterAttachments_Setup_mC77BF9B88E84F10B2E9F2C60FE0E0871F0BE0643 (void);
// 0x0000039C System.Void GameCreator.Characters.CharacterAttachments::Attach(UnityEngine.HumanBodyBones,UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Space)
extern void CharacterAttachments_Attach_mCF2F16665D894126502C11D9D90EE4BDB328C9AA (void);
// 0x0000039D System.Collections.Generic.List`1<UnityEngine.GameObject> GameCreator.Characters.CharacterAttachments::Detach(UnityEngine.HumanBodyBones)
extern void CharacterAttachments_Detach_mB7A9665966839D74FECC05D1BD738CDEE0AC5A78 (void);
// 0x0000039E System.Boolean GameCreator.Characters.CharacterAttachments::Detach(UnityEngine.GameObject)
extern void CharacterAttachments_Detach_m363607A34FE5E8C3FF127F1AA7DD93699693133C (void);
// 0x0000039F System.Void GameCreator.Characters.CharacterAttachments::Remove(UnityEngine.HumanBodyBones)
extern void CharacterAttachments_Remove_m1D51BAA230285FF1E0CDD78EDBD1E9DD02416DAB (void);
// 0x000003A0 System.Void GameCreator.Characters.CharacterAttachments::Remove(UnityEngine.GameObject)
extern void CharacterAttachments_Remove_mCF1C1F06E000D3D7270957E61E5E5C108B19A95D (void);
// 0x000003A1 System.Collections.Generic.List`1<UnityEngine.GameObject> GameCreator.Characters.CharacterAttachments::DetachOrDestroy(UnityEngine.HumanBodyBones,System.Boolean)
extern void CharacterAttachments_DetachOrDestroy_m359CC679167CBACE7DA22901BA0FE4C40A5F6C17 (void);
// 0x000003A2 System.Boolean GameCreator.Characters.CharacterAttachments::DetachOrDestroy(UnityEngine.GameObject,System.Boolean)
extern void CharacterAttachments_DetachOrDestroy_m8FD9ACCA2539BB5ECE922B12E797A9539B167748 (void);
// 0x000003A3 System.Void GameCreator.Characters.CharacterAttachments::.ctor()
extern void CharacterAttachments__ctor_m0CCC82BB6515D20D5CEA6487C7938148B8A39967 (void);
// 0x000003A4 System.Void GameCreator.Characters.CharacterAttachments/EventData::.ctor()
extern void EventData__ctor_m7BE228DE04DD384C57C7762F0D44E4F3771A067A (void);
// 0x000003A5 System.Void GameCreator.Characters.CharacterAttachments/AttachmentEvent::.ctor()
extern void AttachmentEvent__ctor_m54DEA0243711F1110702718B4AA29699BBD50614 (void);
// 0x000003A6 System.Void GameCreator.Characters.CharacterAttachments/Attachment::.ctor(UnityEngine.GameObject,UnityEngine.GameObject)
extern void Attachment__ctor_mBEAFD25BCAAAC5DFE9C7ED51D210C2004231868A (void);
// 0x000003A7 System.Boolean GameCreator.Characters.CharacterFootIK::get_Active()
extern void CharacterFootIK_get_Active_m026E791D09313D95E6552031863D8A3C177CE13F (void);
// 0x000003A8 System.Void GameCreator.Characters.CharacterFootIK::set_Active(System.Boolean)
extern void CharacterFootIK_set_Active_m6ED2E272FEB78F4FD67F9E8D9A6A4E1A7CE6303C (void);
// 0x000003A9 System.Void GameCreator.Characters.CharacterFootIK::Setup(GameCreator.Characters.Character)
extern void CharacterFootIK_Setup_mA82BE7B9C6E52D891BE7DC527723720754ED38B6 (void);
// 0x000003AA System.Void GameCreator.Characters.CharacterFootIK::Update()
extern void CharacterFootIK_Update_m4A073DB20AF2E326944F2DFE15203E2DFD250010 (void);
// 0x000003AB System.Void GameCreator.Characters.CharacterFootIK::LateUpdate()
extern void CharacterFootIK_LateUpdate_m519C67139FD3218D68401A596D49F626CF1F6A5E (void);
// 0x000003AC System.Void GameCreator.Characters.CharacterFootIK::OnAnimatorIK(System.Int32)
extern void CharacterFootIK_OnAnimatorIK_m7FD8DE11205F055E0AB3EBB1082C5483985832E7 (void);
// 0x000003AD System.Void GameCreator.Characters.CharacterFootIK::UpdateFoot(GameCreator.Characters.CharacterFootIK/Foot)
extern void CharacterFootIK_UpdateFoot_m72728B9463E0F94E9699A7F6D9E871223F5D4120 (void);
// 0x000003AE System.Void GameCreator.Characters.CharacterFootIK::SetFoot(GameCreator.Characters.CharacterFootIK/Foot)
extern void CharacterFootIK_SetFoot_m6191440AE707E2F255A0A7B973993865B8CE3176 (void);
// 0x000003AF System.Void GameCreator.Characters.CharacterFootIK::WeightCompensationPosition()
extern void CharacterFootIK_WeightCompensationPosition_m8A98315FA2B8C4F676BECA96CDFB241B72F5DBD9 (void);
// 0x000003B0 UnityEngine.Vector3 GameCreator.Characters.CharacterFootIK::GetControllerBase()
extern void CharacterFootIK_GetControllerBase_mB65AE07F060CCE694C9A9967309EED703DB7A904 (void);
// 0x000003B1 System.Void GameCreator.Characters.CharacterFootIK::.ctor()
extern void CharacterFootIK__ctor_m5FF22DC643C27F213DCF483FDFA7AA2CECD8A6A6 (void);
// 0x000003B2 System.Void GameCreator.Characters.CharacterFootIK::.cctor()
extern void CharacterFootIK__cctor_mB73863D0FF302CA3AC97F79B647E6A1FE87000A9 (void);
// 0x000003B3 System.Void GameCreator.Characters.CharacterFootIK/Foot::.ctor(UnityEngine.Transform,UnityEngine.AvatarIKGoal,System.Int32)
extern void Foot__ctor_mD66DD1A662395E4FECAA4878FD773ED8E036F26E (void);
// 0x000003B4 System.Single GameCreator.Characters.CharacterFootIK/Foot::GetWeight(UnityEngine.Animator)
extern void Foot_GetWeight_m15E837CFA8AC8645CC5677C6D446CC84F40FA9C7 (void);
// 0x000003B5 System.Boolean GameCreator.Characters.CharacterHandIK::get_Active()
extern void CharacterHandIK_get_Active_m9DE731503AA6488A0F459F86B49C35C7DCBB38B9 (void);
// 0x000003B6 System.Void GameCreator.Characters.CharacterHandIK::set_Active(System.Boolean)
extern void CharacterHandIK_set_Active_mB3995094DD4DA840C8B4791913796CFA57475C64 (void);
// 0x000003B7 System.Void GameCreator.Characters.CharacterHandIK::Setup(GameCreator.Characters.Character)
extern void CharacterHandIK_Setup_m44CF45FEF4C3419638A861E36EA9B988686691D3 (void);
// 0x000003B8 System.Void GameCreator.Characters.CharacterHandIK::OnAnimatorIK(System.Int32)
extern void CharacterHandIK_OnAnimatorIK_mEC29938044DF1E9706964B605C580F86C7FDCCE4 (void);
// 0x000003B9 System.Void GameCreator.Characters.CharacterHandIK::UpdateHand(GameCreator.Characters.CharacterHandIK/Hand)
extern void CharacterHandIK_UpdateHand_m71EBCD17DE47A64CB1224E7CE9835447F357AAF1 (void);
// 0x000003BA System.Void GameCreator.Characters.CharacterHandIK::Update()
extern void CharacterHandIK_Update_m01BAD7A6113AD6C70968B487A0A5CB31B7865A7D (void);
// 0x000003BB System.Void GameCreator.Characters.CharacterHandIK::Reach(GameCreator.Characters.CharacterHandIK/Limb,UnityEngine.Transform,System.Single)
extern void CharacterHandIK_Reach_mFAD2A3CF29931F216581FDF0501F54045FE41445 (void);
// 0x000003BC System.Void GameCreator.Characters.CharacterHandIK::LetGo(GameCreator.Characters.CharacterHandIK/Limb,System.Single)
extern void CharacterHandIK_LetGo_mB270F4770DC12FF34457E9E9261DC03A3085FD64 (void);
// 0x000003BD GameCreator.Characters.CharacterHandIK/Hand GameCreator.Characters.CharacterHandIK::NearestHand(UnityEngine.Transform)
extern void CharacterHandIK_NearestHand_m3CD7FEE1FDB3D5E9C9C79A51D7E52C523404C1FE (void);
// 0x000003BE System.Void GameCreator.Characters.CharacterHandIK::.ctor()
extern void CharacterHandIK__ctor_mD06DCDA1DA1C59281B9B59971AB2957E4F09FAD1 (void);
// 0x000003BF System.Void GameCreator.Characters.CharacterHandIK/Hand::.ctor(UnityEngine.Transform,UnityEngine.AvatarIKGoal)
extern void Hand__ctor_m2FE951C0D079B39453BB1B3AF680664E5347542B (void);
// 0x000003C0 System.Void GameCreator.Characters.CharacterHandIK/Hand::Update(UnityEngine.Animator,System.Single)
extern void Hand_Update_mEDCD95DA66ECDAE641576934ECAB6719685F7DA4 (void);
// 0x000003C1 System.Void GameCreator.Characters.CharacterHandIK/Hand::Reach(UnityEngine.Animator,UnityEngine.Transform,System.Single)
extern void Hand_Reach_m2610AE9B1CD3C85487204B44998B24D391980296 (void);
// 0x000003C2 System.Void GameCreator.Characters.CharacterHandIK/Hand::Unreach(System.Single)
extern void Hand_Unreach_mCAA10EA9B2D3E84F320954B322384CBA76DA61EB (void);
// 0x000003C3 System.Boolean GameCreator.Characters.CharacterHeadTrack::get_Active()
extern void CharacterHeadTrack_get_Active_m3816DE3591D91706830CB25BEA2C2FE7DC5BACFD (void);
// 0x000003C4 System.Void GameCreator.Characters.CharacterHeadTrack::set_Active(System.Boolean)
extern void CharacterHeadTrack_set_Active_mFC5D304F334A636912FC185B19AB2D4C7EF4FD95 (void);
// 0x000003C5 System.Void GameCreator.Characters.CharacterHeadTrack::Awake()
extern void CharacterHeadTrack_Awake_m87787E563BAF0D0BB1089F75DBE25919699E8E8E (void);
// 0x000003C6 System.Void GameCreator.Characters.CharacterHeadTrack::OnAnimatorIK(System.Int32)
extern void CharacterHeadTrack_OnAnimatorIK_m87430FCAD998BED198A7C6EE02BF010586423621 (void);
// 0x000003C7 System.Void GameCreator.Characters.CharacterHeadTrack::Update()
extern void CharacterHeadTrack_Update_m7BA0BCF6F4C0D01AD1CD3913171A5595A1212780 (void);
// 0x000003C8 System.Void GameCreator.Characters.CharacterHeadTrack::Track(UnityEngine.Vector3,System.Single)
extern void CharacterHeadTrack_Track_m304F417D9D14B150A54859980C1C07413DC37AE2 (void);
// 0x000003C9 System.Void GameCreator.Characters.CharacterHeadTrack::Track(UnityEngine.Transform,System.Single)
extern void CharacterHeadTrack_Track_mDC79F1C66129B773F27BEE4C741606B71FEDFBC0 (void);
// 0x000003CA System.Void GameCreator.Characters.CharacterHeadTrack::TrackPlayer(System.Single)
extern void CharacterHeadTrack_TrackPlayer_m47BBD85231D20102D7065D25307CC46E8BFC494A (void);
// 0x000003CB System.Void GameCreator.Characters.CharacterHeadTrack::TrackCamera(System.Single)
extern void CharacterHeadTrack_TrackCamera_m40AAC71DC4452EC8B40628025D87CBD871E11AD0 (void);
// 0x000003CC System.Void GameCreator.Characters.CharacterHeadTrack::Untrack(System.Single)
extern void CharacterHeadTrack_Untrack_m841550F4B44FF1CCDD0F81809EBA97F97CFE3257 (void);
// 0x000003CD System.Void GameCreator.Characters.CharacterHeadTrack::OnDrawGizmosSelected()
extern void CharacterHeadTrack_OnDrawGizmosSelected_mEF9FEB2CC485462BF38F3AC471C41E2D9EA731A3 (void);
// 0x000003CE System.Void GameCreator.Characters.CharacterHeadTrack::.ctor()
extern void CharacterHeadTrack__ctor_m70B7E76D49C0F84F2891B8B37F02E3160B99CBB3 (void);
// 0x000003CF System.Void GameCreator.Characters.CharacterHeadTrack/TrackInfo::.ctor(GameCreator.Characters.Character)
extern void TrackInfo__ctor_m1E708327CBD781D8C236453691E0964F4B87CCCA (void);
// 0x000003D0 System.Void GameCreator.Characters.CharacterHeadTrack/TrackInfo::UpdateInfo(GameCreator.Characters.CharacterHeadTrack/TrackInfo/TrackState,UnityEngine.Vector3)
extern void TrackInfo_UpdateInfo_m13A18370C1D2F2006E064E3B3A9C07C19AA48D4E (void);
// 0x000003D1 System.Void GameCreator.Characters.CharacterHeadTrack/TrackInfo::ChangeTrackTarget(System.Single)
extern void TrackInfo_ChangeTrackTarget_mB5E7F0CCA1C85FEC0B9344672891F7133E827DC6 (void);
// 0x000003D2 UnityEngine.Vector3 GameCreator.Characters.CharacterHeadTrack/Target::GetPosition()
extern void Target_GetPosition_m69DF73C1A51CB103ABDC9A1DE3CE1B72A67853AD (void);
// 0x000003D3 System.Boolean GameCreator.Characters.CharacterHeadTrack/Target::HasTarget()
extern void Target_HasTarget_m59EB9B459975DCBA51F4086BF331C036B82EBA6F (void);
// 0x000003D4 System.Void GameCreator.Characters.CharacterHeadTrack/Target::.ctor()
extern void Target__ctor_m03B55122FF0ACB7EC2683F3090D6555E23280F67 (void);
// 0x000003D5 System.Void GameCreator.Characters.CharacterHeadTrack/TargetPosition::.ctor(UnityEngine.Vector3)
extern void TargetPosition__ctor_m5BA7690083B706004DBEC810BC5E91A0FDC37B25 (void);
// 0x000003D6 System.Boolean GameCreator.Characters.CharacterHeadTrack/TargetPosition::HasTarget()
extern void TargetPosition_HasTarget_m92B3D66946F65474899F7256C023A055AD040EAF (void);
// 0x000003D7 UnityEngine.Vector3 GameCreator.Characters.CharacterHeadTrack/TargetPosition::GetPosition()
extern void TargetPosition_GetPosition_mD662A6DAAD52607726D2A6129639162A55CA33CD (void);
// 0x000003D8 System.Void GameCreator.Characters.CharacterHeadTrack/TargetTransform::.ctor(UnityEngine.Transform)
extern void TargetTransform__ctor_m4C1C55A1B9C7BD02C21503883E4D798E70D904D0 (void);
// 0x000003D9 System.Boolean GameCreator.Characters.CharacterHeadTrack/TargetTransform::HasTarget()
extern void TargetTransform_HasTarget_mB38A96A39AA886EE609FB825E4D5C4F3000380A6 (void);
// 0x000003DA UnityEngine.Vector3 GameCreator.Characters.CharacterHeadTrack/TargetTransform::GetPosition()
extern void TargetTransform_GetPosition_m19AC976B6A2EDD5C5D1AEC5DAD0744B11F11FA8C (void);
// 0x000003DB System.Void GameCreator.Characters.CharacterHeadTrack/TargetPlayer::.ctor()
extern void TargetPlayer__ctor_mB76A5F3194E1B6C73437C82BD2DB8105417796B4 (void);
// 0x000003DC System.Void GameCreator.Characters.CharacterHeadTrack/TargetCamera::.ctor()
extern void TargetCamera__ctor_mAE89FB908A6D2C01BFE85679F69E3C5A4B7EC112 (void);
// 0x000003DD System.Boolean GameCreator.Characters.IToggleIK::get_Active()
// 0x000003DE System.Void GameCreator.Characters.IToggleIK::set_Active(System.Boolean)
// 0x000003DF GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.CharacterLocomotion::get_currentLocomotionType()
extern void CharacterLocomotion_get_currentLocomotionType_mA4FD56B3638F170115848BC7803482297B3E29D9 (void);
// 0x000003E0 System.Void GameCreator.Characters.CharacterLocomotion::set_currentLocomotionType(GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM)
extern void CharacterLocomotion_set_currentLocomotionType_m56CF03F923B8B8A110975B02D4CB3BF2A402B3FD (void);
// 0x000003E1 GameCreator.Characters.ILocomotionSystem GameCreator.Characters.CharacterLocomotion::get_currentLocomotionSystem()
extern void CharacterLocomotion_get_currentLocomotionSystem_mD7F3DE1436DF555B140C54593D6095D9AB7EAA52 (void);
// 0x000003E2 System.Void GameCreator.Characters.CharacterLocomotion::set_currentLocomotionSystem(GameCreator.Characters.ILocomotionSystem)
extern void CharacterLocomotion_set_currentLocomotionSystem_mED30E6DD7EDAB456C270565D8C8BA330C517ABBE (void);
// 0x000003E3 System.Void GameCreator.Characters.CharacterLocomotion::Setup(GameCreator.Characters.Character)
extern void CharacterLocomotion_Setup_m4EA3558ACAFA191FA47D5EF4310B1D304AF959F4 (void);
// 0x000003E4 System.Void GameCreator.Characters.CharacterLocomotion::Update()
extern void CharacterLocomotion_Update_mD15E823D65248B100E8E9C673F39D6399A9DF82E (void);
// 0x000003E5 System.Void GameCreator.Characters.CharacterLocomotion::Dash(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void CharacterLocomotion_Dash_m36D15DC423760CDECFE76D9943D8CFEEE241D0B3 (void);
// 0x000003E6 System.Void GameCreator.Characters.CharacterLocomotion::RootMovement(System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern void CharacterLocomotion_RootMovement_mC8937A5D9C71FBDFC34107563E27730AC135117B (void);
// 0x000003E7 System.Int32 GameCreator.Characters.CharacterLocomotion::Jump()
extern void CharacterLocomotion_Jump_m35848C357D878187E36D971B7700AD2AFB36610F (void);
// 0x000003E8 System.Int32 GameCreator.Characters.CharacterLocomotion::Jump(System.Single)
extern void CharacterLocomotion_Jump_m93C37A3BF498C8F1F912C732DD12BC16A8C86012 (void);
// 0x000003E9 System.Void GameCreator.Characters.CharacterLocomotion::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void CharacterLocomotion_Teleport_m7E0C0F8FA0E31CA6A9BCB31AF0E88212810F1326 (void);
// 0x000003EA System.Void GameCreator.Characters.CharacterLocomotion::Teleport(UnityEngine.Vector3)
extern void CharacterLocomotion_Teleport_mE26957E6893D7BB3B8471AEFDF2A9F2904F7D8D1 (void);
// 0x000003EB System.Void GameCreator.Characters.CharacterLocomotion::SetAnimatorConstraint(GameCreator.Characters.CharacterLocomotion/ANIM_CONSTRAINT)
extern void CharacterLocomotion_SetAnimatorConstraint_mDF62BB2C551735C18B02BDD5599D921BA27AC8F5 (void);
// 0x000003EC System.Void GameCreator.Characters.CharacterLocomotion::ChangeHeight(System.Single)
extern void CharacterLocomotion_ChangeHeight_m647C36A16867B6EE298CF970008F2F31490BFFEB (void);
// 0x000003ED System.Void GameCreator.Characters.CharacterLocomotion::SetIsControllable(System.Boolean)
extern void CharacterLocomotion_SetIsControllable_m849C687AF27484FB64E04A2541141EA3C3581AF4 (void);
// 0x000003EE UnityEngine.Vector3 GameCreator.Characters.CharacterLocomotion::GetAimDirection()
extern void CharacterLocomotion_GetAimDirection_m9A443A47670DC8C14F48EA5F1B257C9463091C4D (void);
// 0x000003EF UnityEngine.Vector3 GameCreator.Characters.CharacterLocomotion::GetMovementDirection()
extern void CharacterLocomotion_GetMovementDirection_m92E11D87E44E0C7EF1CCEB798750C44526DAAEB0 (void);
// 0x000003F0 System.Void GameCreator.Characters.CharacterLocomotion::SetDirectionalDirection(UnityEngine.Vector3,GameCreator.Characters.ILocomotionSystem/TargetRotation)
extern void CharacterLocomotion_SetDirectionalDirection_m5FF9FD079797718B799191A4C5DC144707E37477 (void);
// 0x000003F1 System.Void GameCreator.Characters.CharacterLocomotion::SetTankDirection(UnityEngine.Vector3,System.Single)
extern void CharacterLocomotion_SetTankDirection_m9DC0A25913E163BCDF9FDFD2176FCF8DE1E90796 (void);
// 0x000003F2 System.Void GameCreator.Characters.CharacterLocomotion::SetTarget(UnityEngine.Ray,UnityEngine.LayerMask,GameCreator.Characters.ILocomotionSystem/TargetRotation,System.Single,UnityEngine.Events.UnityAction)
extern void CharacterLocomotion_SetTarget_mB368E45B577F5A374CF293A69E68552EFFC9CF5A (void);
// 0x000003F3 System.Void GameCreator.Characters.CharacterLocomotion::SetTarget(UnityEngine.Vector3,GameCreator.Characters.ILocomotionSystem/TargetRotation,System.Single,UnityEngine.Events.UnityAction)
extern void CharacterLocomotion_SetTarget_m6BCC9EB41ACB501DD1059BCE36B2DAAB56CA2A3B (void);
// 0x000003F4 System.Void GameCreator.Characters.CharacterLocomotion::FollowTarget(UnityEngine.Transform,System.Single,System.Single)
extern void CharacterLocomotion_FollowTarget_mE4E3D80B1A9D530DB672095AA7163F88A39A5FBA (void);
// 0x000003F5 System.Void GameCreator.Characters.CharacterLocomotion::Stop(GameCreator.Characters.ILocomotionSystem/TargetRotation,UnityEngine.Events.UnityAction)
extern void CharacterLocomotion_Stop_m742307B2D5B38C856F082005E377A5D2BCBE3940 (void);
// 0x000003F6 System.Void GameCreator.Characters.CharacterLocomotion::SetRotation(UnityEngine.Vector3)
extern void CharacterLocomotion_SetRotation_m8E28A434293EF1F6E409BD22E4ED613C68D59CDF (void);
// 0x000003F7 System.Void GameCreator.Characters.CharacterLocomotion::UseGravity(System.Boolean)
extern void CharacterLocomotion_UseGravity_m165D187B22912DB09C8DF83F94370BD28A2E0371 (void);
// 0x000003F8 System.Void GameCreator.Characters.CharacterLocomotion::SetVerticalSpeed(System.Single)
extern void CharacterLocomotion_SetVerticalSpeed_m4C3D6EA0B5483E8D1447268FDF29840725ACC7A2 (void);
// 0x000003F9 System.Void GameCreator.Characters.CharacterLocomotion::GenerateNavmeshAgent()
extern void CharacterLocomotion_GenerateNavmeshAgent_mEA79F854DDF609B3A2D80E36E0A5E0E017041DA6 (void);
// 0x000003FA System.Void GameCreator.Characters.CharacterLocomotion::ChangeLocomotionSystem()
// 0x000003FB System.Void GameCreator.Characters.CharacterLocomotion::UpdateVerticalSpeed(System.Boolean)
extern void CharacterLocomotion_UpdateVerticalSpeed_mAFD960C4294BE61F6BE1697A386D832FFD1E4785 (void);
// 0x000003FC System.Void GameCreator.Characters.CharacterLocomotion::UpdateCharacterState(GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM)
extern void CharacterLocomotion_UpdateCharacterState_mFB3387780D4032353BF4B8B43B0451E7D8AF452A (void);
// 0x000003FD System.Void GameCreator.Characters.CharacterLocomotion::.ctor()
extern void CharacterLocomotion__ctor_m392D2187B5E13A888FDB851717BB297C448E313D (void);
// 0x000003FE System.Void GameCreator.Characters.ILocomotionSystem::set_isDashing(System.Boolean)
extern void ILocomotionSystem_set_isDashing_mC079E6C302938750FA3B860353FD0657B09C4A53 (void);
// 0x000003FF System.Boolean GameCreator.Characters.ILocomotionSystem::get_isDashing()
extern void ILocomotionSystem_get_isDashing_m1C6E364F5B8D885EEB5EF34053F5C9156B12A439 (void);
// 0x00000400 System.Void GameCreator.Characters.ILocomotionSystem::set_isRootMoving(System.Boolean)
extern void ILocomotionSystem_set_isRootMoving_m360142C3A457C12C703CD94B9674C1A64EB3F21C (void);
// 0x00000401 System.Boolean GameCreator.Characters.ILocomotionSystem::get_isRootMoving()
extern void ILocomotionSystem_get_isRootMoving_mD154CF22484E4ADD2130D6A2BC32A3FF9A44AACD (void);
// 0x00000402 System.Void GameCreator.Characters.ILocomotionSystem::Setup(GameCreator.Characters.CharacterLocomotion)
extern void ILocomotionSystem_Setup_m85D4ADC29050DA9FDB7C88A6E7D0ACCBA85CBB1F (void);
// 0x00000403 System.Void GameCreator.Characters.ILocomotionSystem::Dash(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void ILocomotionSystem_Dash_mF3FB36C24B2653848EC9E20F89B0FE7C93AC0CA5 (void);
// 0x00000404 System.Void GameCreator.Characters.ILocomotionSystem::RootMovement(System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern void ILocomotionSystem_RootMovement_m55040C62019E0D98C1FC8E73DEB3D67DE00BE198 (void);
// 0x00000405 System.Void GameCreator.Characters.ILocomotionSystem::StopRootMovement()
extern void ILocomotionSystem_StopRootMovement_m9A7A160B43184CD3C5005E7324EDF91703C8AA9E (void);
// 0x00000406 GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.ILocomotionSystem::Update()
extern void ILocomotionSystem_Update_m14272E6794D9606A43A960639AF184712BBD3197 (void);
// 0x00000407 System.Void GameCreator.Characters.ILocomotionSystem::OnDestroy()
// 0x00000408 UnityEngine.Quaternion GameCreator.Characters.ILocomotionSystem::UpdateRotation(UnityEngine.Vector3)
extern void ILocomotionSystem_UpdateRotation_mAE1E9768D5D6BE86BC74FEC662A30387564EBC0C (void);
// 0x00000409 System.Single GameCreator.Characters.ILocomotionSystem::CalculateSpeed(UnityEngine.Vector3,System.Boolean)
extern void ILocomotionSystem_CalculateSpeed_m4AA97D553556B9D10CAA965EE45957B6A4F26D33 (void);
// 0x0000040A System.Void GameCreator.Characters.ILocomotionSystem::UpdateAnimationConstraints(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void ILocomotionSystem_UpdateAnimationConstraints_m18F19A08DEE8231E5B8EBDFB72C047E44B1CDAD8 (void);
// 0x0000040B System.Void GameCreator.Characters.ILocomotionSystem::UpdateSliding()
extern void ILocomotionSystem_UpdateSliding_m4A440698BDA8ACC4B21C4A08A08C0B664844FA46 (void);
// 0x0000040C System.Void GameCreator.Characters.ILocomotionSystem::UpdateRootMovement(UnityEngine.Vector3)
extern void ILocomotionSystem_UpdateRootMovement_mA412472A457C3669DA5E23122DC8253A2E3CEE98 (void);
// 0x0000040D GameCreator.Characters.ILocomotionSystem/DirectionData GameCreator.Characters.ILocomotionSystem::GetFaceDirection()
extern void ILocomotionSystem_GetFaceDirection_mD98283E733C6DD693635942A82C917C1EA2AE9AF (void);
// 0x0000040E System.Void GameCreator.Characters.ILocomotionSystem::.ctor()
extern void ILocomotionSystem__ctor_m718D873429BE1D12E7954D500490DBF173628E27 (void);
// 0x0000040F System.Void GameCreator.Characters.ILocomotionSystem::.cctor()
extern void ILocomotionSystem__cctor_m3002E1C212BFC6F8DFD08779EC33441E75E1F309 (void);
// 0x00000410 System.Void GameCreator.Characters.ILocomotionSystem/TargetRotation::.ctor(System.Boolean,UnityEngine.Vector3)
extern void TargetRotation__ctor_m5434766884A6F24044E0CBF81867E803F02A3CAC (void);
// 0x00000411 System.Void GameCreator.Characters.ILocomotionSystem/DirectionData::.ctor(GameCreator.Characters.CharacterLocomotion/FACE_DIRECTION,GameCreator.Core.TargetPosition)
extern void DirectionData__ctor_m27020C2DD23A950CD9B706D3FB006C2B116F5D3D (void);
// 0x00000412 GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.LocomotionSystemDirectional::Update()
extern void LocomotionSystemDirectional_Update_mAB157B3C3E9A7DCF6DC8AEDB79CC802A460A9752 (void);
// 0x00000413 System.Void GameCreator.Characters.LocomotionSystemDirectional::OnDestroy()
extern void LocomotionSystemDirectional_OnDestroy_mFD1CF6D94B1D4A06915A2F91E7AE3622FBC69DBC (void);
// 0x00000414 System.Void GameCreator.Characters.LocomotionSystemDirectional::SetDirection(UnityEngine.Vector3,GameCreator.Characters.ILocomotionSystem/TargetRotation)
extern void LocomotionSystemDirectional_SetDirection_m56B264E25F7F9B1353378BBEEE5F875D2B8D5A2D (void);
// 0x00000415 System.Void GameCreator.Characters.LocomotionSystemDirectional::.ctor()
extern void LocomotionSystemDirectional__ctor_mD43454B4E471D06B903A870BEC39DFFF43109F13 (void);
// 0x00000416 GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.LocomotionSystemFollow::Update()
extern void LocomotionSystemFollow_Update_m2FFF48BC4C4A95AF150468F3D5BD30BC566BFF57 (void);
// 0x00000417 System.Void GameCreator.Characters.LocomotionSystemFollow::OnDestroy()
extern void LocomotionSystemFollow_OnDestroy_m64EF03C39A0FB6B31E5FB4BF59CF2D274EB58575 (void);
// 0x00000418 System.Void GameCreator.Characters.LocomotionSystemFollow::UpdateNavmeshAnimationConstraints()
extern void LocomotionSystemFollow_UpdateNavmeshAnimationConstraints_mBC69B3A51F3C36045F72A3B2E043686336CDA17C (void);
// 0x00000419 System.Void GameCreator.Characters.LocomotionSystemFollow::SetFollow(UnityEngine.Transform,System.Single,System.Single)
extern void LocomotionSystemFollow_SetFollow_m6CF8B1B9D5509BAF9A538B53101E938511B8870E (void);
// 0x0000041A System.Void GameCreator.Characters.LocomotionSystemFollow::.ctor()
extern void LocomotionSystemFollow__ctor_mC7A2BA9AFB8E39141C9796E7AFD78D8C033543A2 (void);
// 0x0000041B GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.LocomotionSystemRotation::Update()
extern void LocomotionSystemRotation_Update_mFEFC7D539FF76753C0CC9BA5EEB688D977CF24F0 (void);
// 0x0000041C System.Void GameCreator.Characters.LocomotionSystemRotation::OnDestroy()
extern void LocomotionSystemRotation_OnDestroy_m8C6CEDD2660A6AAB259BF7A3B095E220E920153F (void);
// 0x0000041D System.Void GameCreator.Characters.LocomotionSystemRotation::SetDirection(UnityEngine.Vector3,GameCreator.Characters.ILocomotionSystem/TargetRotation)
extern void LocomotionSystemRotation_SetDirection_m366770BD8A47C75CC7DF07A26A54C651012DFD23 (void);
// 0x0000041E System.Void GameCreator.Characters.LocomotionSystemRotation::.ctor()
extern void LocomotionSystemRotation__ctor_m9723F2F15C8F194A13ECD1301FBDB2F320A64237 (void);
// 0x0000041F GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.LocomotionSystemTank::Update()
extern void LocomotionSystemTank_Update_mB20E4FAEC16E21DF49C3FE673539C28AEF1D297D (void);
// 0x00000420 System.Void GameCreator.Characters.LocomotionSystemTank::OnDestroy()
extern void LocomotionSystemTank_OnDestroy_m0C6E7ADD33CBED20F6DFA9092E06C70D734B5C24 (void);
// 0x00000421 System.Void GameCreator.Characters.LocomotionSystemTank::SetDirection(UnityEngine.Vector3,System.Single)
extern void LocomotionSystemTank_SetDirection_m1D97B11ECC841B23E1668FC2B5DC4706CDCDC6C0 (void);
// 0x00000422 System.Void GameCreator.Characters.LocomotionSystemTank::.ctor()
extern void LocomotionSystemTank__ctor_mDC544EE788E973749349AADAE979D380A2B4A66C (void);
// 0x00000423 GameCreator.Characters.CharacterLocomotion/LOCOMOTION_SYSTEM GameCreator.Characters.LocomotionSystemTarget::Update()
extern void LocomotionSystemTarget_Update_m9C8A7BD119F6250FD0F762ACA17096D026E2A7FE (void);
// 0x00000424 System.Void GameCreator.Characters.LocomotionSystemTarget::OnDestroy()
extern void LocomotionSystemTarget_OnDestroy_mBD9A01DBFD00BEB57900148BB247AA9C6381CC41 (void);
// 0x00000425 System.Void GameCreator.Characters.LocomotionSystemTarget::Stopping()
extern void LocomotionSystemTarget_Stopping_mDDBFDF8BD138D3858BFE108B6552BBA6D4ABE3D1 (void);
// 0x00000426 System.Void GameCreator.Characters.LocomotionSystemTarget::Slowing(System.Single)
extern void LocomotionSystemTarget_Slowing_m5812DAF1040CFA26DE93AA2DF1FE6D67905931A8 (void);
// 0x00000427 System.Void GameCreator.Characters.LocomotionSystemTarget::Moving()
extern void LocomotionSystemTarget_Moving_m9D1A4B6687B8CC4E464218152B33F227C9B1D7C1 (void);
// 0x00000428 System.Void GameCreator.Characters.LocomotionSystemTarget::UpdateNavmeshAnimationConstraints()
extern void LocomotionSystemTarget_UpdateNavmeshAnimationConstraints_m7A0070BBC3DB9F3C6A3E444AF5DC5E5C76F70D3C (void);
// 0x00000429 System.Void GameCreator.Characters.LocomotionSystemTarget::FinishMovement()
extern void LocomotionSystemTarget_FinishMovement_m372B8D4830B34017E520B97187E20BA5232710B0 (void);
// 0x0000042A System.Void GameCreator.Characters.LocomotionSystemTarget::SetTarget(UnityEngine.Ray,UnityEngine.LayerMask,GameCreator.Characters.ILocomotionSystem/TargetRotation,System.Single,UnityEngine.Events.UnityAction)
extern void LocomotionSystemTarget_SetTarget_mF8B47367FE45FAAD0648BC4A81D958F39D6ABC6D (void);
// 0x0000042B System.Void GameCreator.Characters.LocomotionSystemTarget::SetTarget(UnityEngine.Vector3,GameCreator.Characters.ILocomotionSystem/TargetRotation,System.Single,UnityEngine.Events.UnityAction)
extern void LocomotionSystemTarget_SetTarget_m219C22C501ED91BFF61595732B18FABA0F89FC5B (void);
// 0x0000042C System.Void GameCreator.Characters.LocomotionSystemTarget::Stop(GameCreator.Characters.ILocomotionSystem/TargetRotation,UnityEngine.Events.UnityAction)
extern void LocomotionSystemTarget_Stop_m3E1E47BC681B0ACE3DA94479B57A0ADB6CEC4C7D (void);
// 0x0000042D System.Void GameCreator.Characters.LocomotionSystemTarget::.ctor()
extern void LocomotionSystemTarget__ctor_m9C46630538B0934131B8F5251F873ED98F60F0C4 (void);
// 0x0000042E System.Void GameCreator.Characters.PlayerCharacter::Awake()
extern void PlayerCharacter_Awake_m53D5D700F69BDEE5AB4F714ACF182FE598F0EEF3 (void);
// 0x0000042F System.Void GameCreator.Characters.PlayerCharacter::Update()
extern void PlayerCharacter_Update_m13257097B99AC70AB01F659E9DF726A43D359734 (void);
// 0x00000430 System.Void GameCreator.Characters.PlayerCharacter::UpdateInputDirectional()
extern void PlayerCharacter_UpdateInputDirectional_m0E619C9BC6DFDBB3CE4300116BDA49642D0A6D49 (void);
// 0x00000431 System.Void GameCreator.Characters.PlayerCharacter::UpdateInputTank()
extern void PlayerCharacter_UpdateInputTank_m70200A320FD4FAC887220AD6A7C479D0558A4435 (void);
// 0x00000432 System.Void GameCreator.Characters.PlayerCharacter::UpdateInputPointClick()
extern void PlayerCharacter_UpdateInputPointClick_mEEE73C6D74082F51F1C1FF2DB8185F6757B4F3B5 (void);
// 0x00000433 System.Void GameCreator.Characters.PlayerCharacter::UpdateInputFollowPointer()
extern void PlayerCharacter_UpdateInputFollowPointer_m85DE2E3070FACC19570B7DF517E22F8C2A0303B2 (void);
// 0x00000434 System.Void GameCreator.Characters.PlayerCharacter::UpdateInputSideScroll(UnityEngine.Vector3)
extern void PlayerCharacter_UpdateInputSideScroll_m7E84F2D7105836D9C30D0ADC74C2DB055FACC2AD (void);
// 0x00000435 UnityEngine.Camera GameCreator.Characters.PlayerCharacter::GetMainCamera()
extern void PlayerCharacter_GetMainCamera_m8B6979A44B32645C383564FB543F1A9CE607530C (void);
// 0x00000436 System.Void GameCreator.Characters.PlayerCharacter::UpdateUIConstraints()
extern void PlayerCharacter_UpdateUIConstraints_m45F683E3C1EF6E086EAD7A9DE49C560FAA595A44 (void);
// 0x00000437 System.Void GameCreator.Characters.PlayerCharacter::ComputeMovement(UnityEngine.Vector3)
extern void PlayerCharacter_ComputeMovement_m6D0A0881459C2ED63A577F9F457D6473F48E1932 (void);
// 0x00000438 System.Void GameCreator.Characters.PlayerCharacter::ForceDirection(UnityEngine.Vector3)
extern void PlayerCharacter_ForceDirection_m388CC2A70CAA8B679E745AE632F2B9E77F872685 (void);
// 0x00000439 System.String GameCreator.Characters.PlayerCharacter::GetUniqueCharacterID()
extern void PlayerCharacter_GetUniqueCharacterID_m47AA4385D9751CE3EFA970D6DEEFD43D43A1DD4A (void);
// 0x0000043A System.Void GameCreator.Characters.PlayerCharacter::.ctor()
extern void PlayerCharacter__ctor_m6022B24D034F3904510B6E0972F97869EC0D6873 (void);
// 0x0000043B System.Void GameCreator.Characters.PlayerCharacter::.cctor()
extern void PlayerCharacter__cctor_m91B76767E7D22A0319ABDA37FFBE5171681C4EFC (void);
// 0x0000043C System.Void GameCreator.Characters.NavigationMarker::OnDrawGizmos()
extern void NavigationMarker_OnDrawGizmos_mE7242C301340CE85AA544B9D9AEB070FD5700E14 (void);
// 0x0000043D UnityEngine.Mesh GameCreator.Characters.NavigationMarker::GetMarkerMesh()
extern void NavigationMarker_GetMarkerMesh_m650D052A4C20CA0BB176D18E613B3BB69751242D (void);
// 0x0000043E UnityEngine.Mesh GameCreator.Characters.NavigationMarker::GenerateMarkerMesh()
extern void NavigationMarker_GenerateMarkerMesh_m72D84D44144F22F1FDE5307DEC953E8B1AA40546 (void);
// 0x0000043F System.Void GameCreator.Characters.NavigationMarker::.ctor()
extern void NavigationMarker__ctor_mD2BA18BBA7C0E193D10551C8948B547EDAA78CCF (void);
// 0x00000440 System.Void GameCreator.Characters.NavigationMarker::.cctor()
extern void NavigationMarker__cctor_mC56E52F03C86EB5FFFE1C7997408DD0AF5680A31 (void);
// 0x00000441 System.Void GameCreator.Characters.CharacterRagdoll::.ctor(GameCreator.Characters.Character)
extern void CharacterRagdoll__ctor_m0768B7775B2D0DA1133F76F91247B75C7FF5D0E7 (void);
// 0x00000442 System.Void GameCreator.Characters.CharacterRagdoll::Initialize(System.Boolean)
extern void CharacterRagdoll_Initialize_m70F15030A8069EF45D4545D35B852B0A01067CBF (void);
// 0x00000443 System.Void GameCreator.Characters.CharacterRagdoll::Ragdoll(System.Boolean,System.Boolean)
extern void CharacterRagdoll_Ragdoll_m836CCB504A1AE79678CA0F2AAB16EFE4F2097CB1 (void);
// 0x00000444 GameCreator.Characters.CharacterRagdoll/State GameCreator.Characters.CharacterRagdoll::GetState()
extern void CharacterRagdoll_GetState_m3080A4F2C5D055DF92BF4A59D8555AB8B3364D97 (void);
// 0x00000445 System.Void GameCreator.Characters.CharacterRagdoll::Update()
extern void CharacterRagdoll_Update_m096EECD2D70CDE09AF55B929A82F980BEA86F70C (void);
// 0x00000446 System.Void GameCreator.Characters.CharacterRagdoll::UpdateRagdoll()
extern void CharacterRagdoll_UpdateRagdoll_mB916D1AD0B8923B921AC3C01161019694BBBBC5B (void);
// 0x00000447 System.Void GameCreator.Characters.CharacterRagdoll::UpdateRecover()
extern void CharacterRagdoll_UpdateRecover_m35A386589CE689F73AC190DF2E968696FDBF4FD1 (void);
// 0x00000448 System.Void GameCreator.Characters.CharacterRagdoll::ToRagdoll()
extern void CharacterRagdoll_ToRagdoll_m37B7822DBF68F1E977E4439947FBE832C0CFA8F9 (void);
// 0x00000449 System.Void GameCreator.Characters.CharacterRagdoll::ToRecover()
extern void CharacterRagdoll_ToRecover_m680C4016FA3588C4FA27462CC2C0CF6CBF1A9FC6 (void);
// 0x0000044A UnityEngine.Vector3 GameCreator.Characters.CharacterRagdoll::GetRagdollDirection()
extern void CharacterRagdoll_GetRagdollDirection_mD9DDD7C8CA3D101B2BA4FF4502CB55019A5527D0 (void);
// 0x0000044B System.Boolean GameCreator.Characters.CharacterRagdoll::BuildBonesData()
extern void CharacterRagdoll_BuildBonesData_mA123BECA25CB529A7B6C96F2A8CFF264D18F6784 (void);
// 0x0000044C System.Void GameCreator.Characters.CharacterRagdoll::SetupJoint(GameCreator.Characters.CharacterRagdoll/Bone,GameCreator.Characters.CharacterRagdoll/Bone,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Type,System.Single,System.Single)
extern void CharacterRagdoll_SetupJoint_m950EB8BEE7023EF957CC55354C3F645AD152D3F5 (void);
// 0x0000044D System.Boolean GameCreator.Characters.CharacterRagdoll::BuildColliders()
extern void CharacterRagdoll_BuildColliders_mCA00F6D60ABD45D13083B131E6B53452E00B28B5 (void);
// 0x0000044E System.Void GameCreator.Characters.CharacterRagdoll::BuildColliderCapsule(GameCreator.Characters.CharacterRagdoll/BoneData)
extern void CharacterRagdoll_BuildColliderCapsule_mA52586CB1F0F475040CF162722116AB97E3504FD (void);
// 0x0000044F System.Void GameCreator.Characters.CharacterRagdoll::BuildColliderSphere(GameCreator.Characters.CharacterRagdoll/BoneData)
extern void CharacterRagdoll_BuildColliderSphere_m90056C02CD66AC6345BDC9A9A16EC18379B48A89 (void);
// 0x00000450 System.Void GameCreator.Characters.CharacterRagdoll::BuildColliderBox(GameCreator.Characters.CharacterRagdoll/BoneData,System.Boolean)
extern void CharacterRagdoll_BuildColliderBox_m2C0E782A2085F6B15ABED0EDCCAD98C1A5E282B9 (void);
// 0x00000451 System.Boolean GameCreator.Characters.CharacterRagdoll::BuildBodies()
extern void CharacterRagdoll_BuildBodies_m99C8266BB2140EB8B9262CE5B8251102A3FAD436 (void);
// 0x00000452 System.Boolean GameCreator.Characters.CharacterRagdoll::BuildJoints()
extern void CharacterRagdoll_BuildJoints_mF46C5B2451861F579E3DAEADA64660284335AC0A (void);
// 0x00000453 System.Boolean GameCreator.Characters.CharacterRagdoll::BuildMasses()
extern void CharacterRagdoll_BuildMasses_m891438176EEB3BDA27F04CE52548270C0F17F87F (void);
// 0x00000454 System.Boolean GameCreator.Characters.CharacterRagdoll::BuildLayers()
extern void CharacterRagdoll_BuildLayers_m05D844300ED0950555F93B8310F44BFA42C7E17A (void);
// 0x00000455 System.Boolean GameCreator.Characters.CharacterRagdoll::BuildChunks()
extern void CharacterRagdoll_BuildChunks_m1DCFE1E3DE881B7102EAAC0EF290A2104A609296 (void);
// 0x00000456 System.Void GameCreator.Characters.CharacterRagdoll::OnDrawGizmos()
extern void CharacterRagdoll_OnDrawGizmos_mFB373C4F7E04EFAD219BE0CF507FA1042E8A67E6 (void);
// 0x00000457 System.Void GameCreator.Characters.CharacterRagdoll::GizmoBone(GameCreator.Characters.CharacterRagdoll/Bone,GameCreator.Characters.CharacterRagdoll/Bone)
extern void CharacterRagdoll_GizmoBone_m895D5D237DF0D3CC8DB0700853FB283C5B8FBF3D (void);
// 0x00000458 System.Void GameCreator.Characters.CharacterRagdoll::.cctor()
extern void CharacterRagdoll__cctor_m827FF4F040D7FF9E3AEAF94E6AB911EFD515CB8E (void);
// 0x00000459 System.Void GameCreator.Characters.CharacterRagdoll/BoneData::.ctor()
extern void BoneData__ctor_mCF41F48FCE2C403A76C860E1963F21BE6BC46674 (void);
// 0x0000045A System.Void GameCreator.Characters.CharacterRagdoll/BoneData::.ctor(UnityEngine.Transform)
extern void BoneData__ctor_m9F43DA88153F565AECD7F3964A391B21123AC23E (void);
// 0x0000045B System.Void GameCreator.Characters.CharacterRagdoll/HumanChunk::.ctor(UnityEngine.Transform)
extern void HumanChunk__ctor_mF152FD85E8AD808AC27AB1466AD5D52EDAC16381 (void);
// 0x0000045C System.Void GameCreator.Characters.CharacterRagdoll/HumanChunk::Snapshot()
extern void HumanChunk_Snapshot_mCA65B9999E27FAEE3E994B3660C7A9E83612A3DF (void);
// 0x0000045D UnityEngine.Bounds GameCreator.Characters.RagdollUtilities::GetBounds(UnityEngine.Transform,UnityEngine.Transform[])
extern void RagdollUtilities_GetBounds_m663E840916CD6689EE671CBC57ECA6729C7757CD (void);
// 0x0000045E System.Void GameCreator.Characters.RagdollUtilities::GetDirection(UnityEngine.Vector3,System.Int32&,System.Single&)
extern void RagdollUtilities_GetDirection_mE621518E33E9D980C3BB6B69D87A472C45973007 (void);
// 0x0000045F UnityEngine.Vector3 GameCreator.Characters.RagdollUtilities::GetDirectionAxis(UnityEngine.Vector3)
extern void RagdollUtilities_GetDirectionAxis_mE96946788564D3A8AFAB8F6D2147196158D4B79F (void);
// 0x00000460 System.Int32 GameCreator.Characters.RagdollUtilities::SmallestComponent(UnityEngine.Vector3)
extern void RagdollUtilities_SmallestComponent_mD6CC9635D2A23998DD65F638198CEA19AEA2ED51 (void);
// 0x00000461 System.Int32 GameCreator.Characters.RagdollUtilities::LargestComponent(UnityEngine.Vector3)
extern void RagdollUtilities_LargestComponent_m90AFEDCD066541F2EA5BE171A4CDE0D8A490840A (void);
// 0x00000462 UnityEngine.Bounds GameCreator.Characters.RagdollUtilities::ProportionalBounds(UnityEngine.Bounds)
extern void RagdollUtilities_ProportionalBounds_m9AA484162ED2A0419E833AC2A1A0FD91D1A0651B (void);
// 0x00000463 UnityEngine.Bounds GameCreator.Characters.RagdollUtilities::Clip(UnityEngine.Bounds,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,System.Boolean)
extern void RagdollUtilities_Clip_m000709885E660539799085DDFADFCA54EF3FDD60 (void);
// 0x00000464 System.Void GameCreator.Characters.AnimationClipGroup::.ctor()
extern void AnimationClipGroup__ctor_m39D2E32859B4F56F09A99B52C45613AEA17D64B3 (void);
// 0x00000465 UnityEngine.RuntimeAnimatorController GameCreator.Characters.CharacterState::GetRuntimeAnimatorController()
extern void CharacterState_GetRuntimeAnimatorController_m8F15A83E82712FA39085866CA47DA7E5A44E320C (void);
// 0x00000466 System.Void GameCreator.Characters.CharacterState::StartState(GameCreator.Characters.CharacterAnimation)
extern void CharacterState_StartState_m78F0DDDEF2FBD5B411D612A66B40527F5FB325AD (void);
// 0x00000467 System.Void GameCreator.Characters.CharacterState::ExitState(GameCreator.Characters.CharacterAnimation)
extern void CharacterState_ExitState_m1FAC3B00029E6429B9B579A20E49148C68655C68 (void);
// 0x00000468 System.Void GameCreator.Characters.CharacterState::.ctor()
extern void CharacterState__ctor_m8166B6AE73B675CA2030F9D84E674C94B738531A (void);
// 0x00000469 System.Boolean GameCreator.Core.ActionCameraFOV::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraFOV_InstantExecute_mE7644EBD6F76F422572D28DEDAE01A8DD2F0DD48 (void);
// 0x0000046A System.Collections.IEnumerator GameCreator.Core.ActionCameraFOV::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraFOV_Execute_m2961F82EDD4ED139F459031B2114495B90DDBEB1 (void);
// 0x0000046B System.Void GameCreator.Core.ActionCameraFOV::.ctor()
extern void ActionCameraFOV__ctor_m46A7CDEFC90178BFB8451AE909B2C62159E2A7DA (void);
// 0x0000046C System.Void GameCreator.Core.ActionCameraFOV/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_m568C46FD84354F03E321B4A7D9B6A406155A6442 (void);
// 0x0000046D System.Void GameCreator.Core.ActionCameraFOV/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_m119B7D8FFAD38119F367543F53D104EB2CB9AC46 (void);
// 0x0000046E System.Boolean GameCreator.Core.ActionCameraFOV/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_m4995AEA698858151266D9E05A91465C9BF335EDE (void);
// 0x0000046F System.Object GameCreator.Core.ActionCameraFOV/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D53463E3447749CF12F7FEA5553D2AD9FE91169 (void);
// 0x00000470 System.Void GameCreator.Core.ActionCameraFOV/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mA5FDD65159048AF6BDEE02D3389DF5B1870EEAFB (void);
// 0x00000471 System.Object GameCreator.Core.ActionCameraFOV/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m1AF795A4F8BA74E09BB67237231F64629E174B23 (void);
// 0x00000472 System.Collections.IEnumerator GameCreator.Core.ActionCharacterJump::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCharacterJump_Execute_m08FA51AE3EF80F74E8C7437DF69FE88F2B3074B5 (void);
// 0x00000473 System.Void GameCreator.Core.ActionCharacterJump::.ctor()
extern void ActionCharacterJump__ctor_mF8308DF8F033033C842C870023831128048DE6A3 (void);
// 0x00000474 System.Void GameCreator.Core.ActionCharacterJump/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_mD014279CDD1DA2CB66827900A7FEF6B36E4034A6 (void);
// 0x00000475 System.Void GameCreator.Core.ActionCharacterJump/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_mC869E87A2210092C97E4DBD7E06FCBE358652E49 (void);
// 0x00000476 System.Boolean GameCreator.Core.ActionCharacterJump/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_m52C7500D1240EF3E83BE33E619EBF688D4DAE5B4 (void);
// 0x00000477 System.Object GameCreator.Core.ActionCharacterJump/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA829096573BA1FF01D24904DA418638E07E295D1 (void);
// 0x00000478 System.Void GameCreator.Core.ActionCharacterJump/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m251ED7B0A59C1F6511E8A0752E09990150A66F22 (void);
// 0x00000479 System.Object GameCreator.Core.ActionCharacterJump/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mEA566C81C92D1772C5B0DA08E2015A4B14C12E25 (void);
// 0x0000047A System.Boolean GameCreator.Core.ActionCursor::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCursor_InstantExecute_mC4990B3E8BAC57EAF15EF7A5758B363577C502B9 (void);
// 0x0000047B System.Void GameCreator.Core.ActionCursor::.ctor()
extern void ActionCursor__ctor_mD6E25CA36E1F1F798AB886C31AB524BCEE3EE65B (void);
// 0x0000047C System.Boolean GameCreator.Core.ActionOpenURL::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionOpenURL_InstantExecute_mE158A9BE18BFF2DAF25C20D7A1A7E56FBD6C7CBD (void);
// 0x0000047D System.Void GameCreator.Core.ActionOpenURL::.ctor()
extern void ActionOpenURL__ctor_m37469D89871D5088B0F28BAE5F8373E00F77DD65 (void);
// 0x0000047E System.Boolean GameCreator.Core.ActionQualitySettings::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionQualitySettings_InstantExecute_m688B78057BCE5A173A4EB85C09E29AE461F71D73 (void);
// 0x0000047F System.Void GameCreator.Core.ActionQualitySettings::.ctor()
extern void ActionQualitySettings__ctor_mD7828EE728F60C0E8CE940C6DD43A626937F9FA0 (void);
// 0x00000480 System.Boolean GameCreator.Core.ActionQuit::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionQuit_InstantExecute_mD7156E86BF560FD18388771A6AF951BE2A18FB94 (void);
// 0x00000481 System.Void GameCreator.Core.ActionQuit::.ctor()
extern void ActionQuit__ctor_m284C476899B42797B01FC4C796E40BE95AB71829 (void);
// 0x00000482 System.Boolean GameCreator.Core.ActionVideoPlay::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVideoPlay_InstantExecute_m4C1836CC342D621818883B41498C7038A71BF819 (void);
// 0x00000483 System.Void GameCreator.Core.ActionVideoPlay::.ctor()
extern void ActionVideoPlay__ctor_m41DBF5979E5EFCCC9D2D5628E76019A3BAA38CE0 (void);
// 0x00000484 System.Boolean GameCreator.Core.ActionAudioMixerParameter::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAudioMixerParameter_InstantExecute_mB7B7A9B3EB9315B9A3CB9FE80EAFA22D33431A0F (void);
// 0x00000485 System.Void GameCreator.Core.ActionAudioMixerParameter::.ctor()
extern void ActionAudioMixerParameter__ctor_m7513A634CC954E5786BB067C7A5D5EE1395FD5B8 (void);
// 0x00000486 System.Boolean GameCreator.Core.ActionAudioPause::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAudioPause_InstantExecute_m5A1F5BCE06180CD8514488B1A866B660346ABDBD (void);
// 0x00000487 System.Void GameCreator.Core.ActionAudioPause::.ctor()
extern void ActionAudioPause__ctor_m9BE09F83B40F024CCDE701FA2768C99EF52CB13A (void);
// 0x00000488 System.Boolean GameCreator.Core.ActionAudioSnapshot::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAudioSnapshot_InstantExecute_m87C15ECAD2E26934875998C56E8EF4A5D794A9AD (void);
// 0x00000489 System.Void GameCreator.Core.ActionAudioSnapshot::.ctor()
extern void ActionAudioSnapshot__ctor_m890806D219A9073C45A1D8A7022D6399083C06C1 (void);
// 0x0000048A System.Boolean GameCreator.Core.ActionPlayMusic::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionPlayMusic_InstantExecute_mECDACC252E10B3D53EFE73C6D34C2D2AB4959611 (void);
// 0x0000048B System.Void GameCreator.Core.ActionPlayMusic::.ctor()
extern void ActionPlayMusic__ctor_m8476718D283EEFC6015B7F082D9C982C03F54089 (void);
// 0x0000048C System.Boolean GameCreator.Core.ActionPlaySound::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionPlaySound_InstantExecute_m9AB8F54F7991F4E3A371421B1BFF9E69F60973B8 (void);
// 0x0000048D System.Void GameCreator.Core.ActionPlaySound::.ctor()
extern void ActionPlaySound__ctor_m741C8033EA48ADEAEA0BE0566872ECC975E862A7 (void);
// 0x0000048E System.Boolean GameCreator.Core.ActionPlaySound3D::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionPlaySound3D_InstantExecute_m6DF8CD49FA96692D96D174A138D8B6B568277610 (void);
// 0x0000048F System.Void GameCreator.Core.ActionPlaySound3D::.ctor()
extern void ActionPlaySound3D__ctor_mB08E8C7535665060322F400B6BE5E60B958D85C8 (void);
// 0x00000490 System.Boolean GameCreator.Core.ActionStopAllSound::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionStopAllSound_InstantExecute_m1C9AFAF392E0ED68217922C36B9F690E0E214FDB (void);
// 0x00000491 System.Void GameCreator.Core.ActionStopAllSound::.ctor()
extern void ActionStopAllSound__ctor_m3E725077B7E61D90DAE707E083AF2636991E9B75 (void);
// 0x00000492 System.Boolean GameCreator.Core.ActionStopMusic::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionStopMusic_InstantExecute_m6FB6F069204E6D5B222FF9F0992062AA035DE12A (void);
// 0x00000493 System.Void GameCreator.Core.ActionStopMusic::.ctor()
extern void ActionStopMusic__ctor_m94A68C8A540A6576CDAD250BD0BAEAC71B065141 (void);
// 0x00000494 System.Boolean GameCreator.Core.ActionStopSound::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionStopSound_InstantExecute_m92C5FFD01FABC278B4214A34BCD4B66F11DC5112 (void);
// 0x00000495 System.Void GameCreator.Core.ActionStopSound::.ctor()
extern void ActionStopSound__ctor_m49B5D139B4BF0FD5A01D03C7B0F8602BE80E458B (void);
// 0x00000496 System.Boolean GameCreator.Core.ActionVolume::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVolume_InstantExecute_m8B433252A8F0275F168772358693B4016A49A4E9 (void);
// 0x00000497 System.Void GameCreator.Core.ActionVolume::.ctor()
extern void ActionVolume__ctor_m9D2126EA2C65C67A4359AB36C5DC26FDA22EA3E1 (void);
// 0x00000498 System.Boolean GameCreator.Core.ActionDebugBeep::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDebugBeep_InstantExecute_m0959A9B35FA7B16B7526630456511E5094E1A165 (void);
// 0x00000499 System.Void GameCreator.Core.ActionDebugBeep::.ctor()
extern void ActionDebugBeep__ctor_mE2D494C491E92235BFD3A68BFD09D7D5A6208E4F (void);
// 0x0000049A System.Boolean GameCreator.Core.ActionDebugBreak::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDebugBreak_InstantExecute_m873A6129A511773BD20EA426CCDDDE0F0BAEEC43 (void);
// 0x0000049B System.Void GameCreator.Core.ActionDebugBreak::.ctor()
extern void ActionDebugBreak__ctor_m03E7FBD708C85F62A48D8DC2BDFCE68C5E1FBBB1 (void);
// 0x0000049C System.Boolean GameCreator.Core.ActionDebugMessage::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDebugMessage_InstantExecute_mB1C886F34153FAC958F1594C804C11463587291C (void);
// 0x0000049D System.Void GameCreator.Core.ActionDebugMessage::.ctor()
extern void ActionDebugMessage__ctor_mCA86AF2033FE92E2DA93931C7400C0D143C0C443 (void);
// 0x0000049E System.Boolean GameCreator.Core.ActionDebugName::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDebugName_InstantExecute_m1CE00EE329405A09530029BBC6EB5488AA8E1E5B (void);
// 0x0000049F System.Void GameCreator.Core.ActionDebugName::.ctor()
extern void ActionDebugName__ctor_mA9D5C2E1CB17A85B000FC7251942393395CD3B8A (void);
// 0x000004A0 System.Boolean GameCreator.Core.ActionDebugPause::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDebugPause_InstantExecute_m494ABAC1945AD21E15F289616EBECB35D664E135 (void);
// 0x000004A1 System.Void GameCreator.Core.ActionDebugPause::.ctor()
extern void ActionDebugPause__ctor_m4DEB65DA50197DCDBDF6594B8B3E16C14BA88EF5 (void);
// 0x000004A2 System.Collections.IEnumerator GameCreator.Core.ActionActions::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionActions_Execute_mF60338A03B212040424031E749D555D34F89EC45 (void);
// 0x000004A3 System.Void GameCreator.Core.ActionActions::OnCompleteActions()
extern void ActionActions_OnCompleteActions_m545AA77F055E7ABBE0BB78384637204A429D9B65 (void);
// 0x000004A4 System.Void GameCreator.Core.ActionActions::Stop()
extern void ActionActions_Stop_mC5B1E26F6A4CE71F9191F3C13600657C6F9F75B0 (void);
// 0x000004A5 System.Void GameCreator.Core.ActionActions::.ctor()
extern void ActionActions__ctor_mD8BA0CCD7C145588FA69585E6F59C5870EE2CE3D (void);
// 0x000004A6 System.Void GameCreator.Core.ActionActions/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m762552681A1828B8FA649A3F54F341D736F501B3 (void);
// 0x000004A7 System.Boolean GameCreator.Core.ActionActions/<>c__DisplayClass7_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CExecuteU3Eb__0_m484878E698FEB2254A02BC992CC4E1A78E190901 (void);
// 0x000004A8 System.Void GameCreator.Core.ActionActions/<Execute>d__7::.ctor(System.Int32)
extern void U3CExecuteU3Ed__7__ctor_m8F225E767DD6DB758FEA0B87C3078AA77FBFA5FA (void);
// 0x000004A9 System.Void GameCreator.Core.ActionActions/<Execute>d__7::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__7_System_IDisposable_Dispose_m4376D2481CBD9E7ADDC3DAF2152CFC9092F5F7E5 (void);
// 0x000004AA System.Boolean GameCreator.Core.ActionActions/<Execute>d__7::MoveNext()
extern void U3CExecuteU3Ed__7_MoveNext_m6312B7FB682A4C9645217291396A35A5DD41FD90 (void);
// 0x000004AB System.Object GameCreator.Core.ActionActions/<Execute>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1684DA24B04EC7538FBC1999207915EF16FD474D (void);
// 0x000004AC System.Void GameCreator.Core.ActionActions/<Execute>d__7::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m7251172E50F11399CF598F7C05629B9825C0F233 (void);
// 0x000004AD System.Object GameCreator.Core.ActionActions/<Execute>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m0DA803CF52C2AF51D6ECF171BF22580ED5539F5B (void);
// 0x000004AE System.Boolean GameCreator.Core.ActionCancelActions::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCancelActions_InstantExecute_m3A20B7634B52F157BFDA50208E87744986BC79B1 (void);
// 0x000004AF System.Void GameCreator.Core.ActionCancelActions::.ctor()
extern void ActionCancelActions__ctor_mBACDD73E2355093DF3C4673300A71C46CBCBBD71 (void);
// 0x000004B0 System.Boolean GameCreator.Core.ActionChangeLanguage::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeLanguage_InstantExecute_mB29B2C922C3818C897C89052ACCD6F95F0F4BB39 (void);
// 0x000004B1 System.Void GameCreator.Core.ActionChangeLanguage::.ctor()
extern void ActionChangeLanguage__ctor_mEFF15550C942621BB8FE92E96AC559089AAC88F9 (void);
// 0x000004B2 System.Boolean GameCreator.Core.ActionComment::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionComment_InstantExecute_mFB667DA16F8FE67ABC8022F8A795957E66E4FDE1 (void);
// 0x000004B3 System.Void GameCreator.Core.ActionComment::.ctor()
extern void ActionComment__ctor_m52FF57E2F29C02E6B6D93F7C3278E55DE8168EFE (void);
// 0x000004B4 System.Boolean GameCreator.Core.ActionConditions::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionConditions_InstantExecute_mC97A34C22BE6BB1F55BD1C50E854AC20A86C27F3 (void);
// 0x000004B5 System.Collections.IEnumerator GameCreator.Core.ActionConditions::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionConditions_Execute_mE0522B42F62D3F243F1AFAE772B9CD175E83C357 (void);
// 0x000004B6 System.Void GameCreator.Core.ActionConditions::.ctor()
extern void ActionConditions__ctor_m8F43A65CFD4882EB2EBC7F668C6989338A2E4C03 (void);
// 0x000004B7 System.Void GameCreator.Core.ActionConditions/<Execute>d__6::.ctor(System.Int32)
extern void U3CExecuteU3Ed__6__ctor_m9EAFACDF88412BADB6CC5CAEEA99FE03D10FDAE1 (void);
// 0x000004B8 System.Void GameCreator.Core.ActionConditions/<Execute>d__6::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__6_System_IDisposable_Dispose_m659F01A84B88179C9CAECF6702B4C86FF9A43B89 (void);
// 0x000004B9 System.Boolean GameCreator.Core.ActionConditions/<Execute>d__6::MoveNext()
extern void U3CExecuteU3Ed__6_MoveNext_m04124FAD32B9595D466981B048248ACBE08A626B (void);
// 0x000004BA System.Object GameCreator.Core.ActionConditions/<Execute>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC017996B99A9A285EE0A0982EF05AD814B15FC3E (void);
// 0x000004BB System.Void GameCreator.Core.ActionConditions/<Execute>d__6::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_mF76B5998B28F241A1530AF47AF804C28A03FEFC2 (void);
// 0x000004BC System.Object GameCreator.Core.ActionConditions/<Execute>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m555CFC88911972DC5B58D4D04A9F0C3E3821B1ED (void);
// 0x000004BD System.Boolean GameCreator.Core.ActionDispatchEvent::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDispatchEvent_InstantExecute_mC1B8C8CAEA5FC37CD909D91F9EEAFF030FE09D71 (void);
// 0x000004BE System.Void GameCreator.Core.ActionDispatchEvent::.ctor()
extern void ActionDispatchEvent__ctor_m63166C105F4082F5124D5D23846A2AE0EBB13A08 (void);
// 0x000004BF System.Boolean GameCreator.Core.ActionGravity::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionGravity_InstantExecute_m206E7E15FF8A302660597BFD95DA7FAD2B6CEE89 (void);
// 0x000004C0 System.Void GameCreator.Core.ActionGravity::.ctor()
extern void ActionGravity__ctor_mCA4824CD9C8CA1BD4B5519867373ACF7F2394489 (void);
// 0x000004C1 System.Boolean GameCreator.Core.ActionMethods::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMethods_InstantExecute_m3CA9958DDB9FEE56A90011ADBA60056E4B232C6E (void);
// 0x000004C2 System.Void GameCreator.Core.ActionMethods::.ctor()
extern void ActionMethods__ctor_m156EC7B148FC9C7DE08EDCD5316021AEBD4258BA (void);
// 0x000004C3 System.Boolean GameCreator.Core.ActionReflectionProbes::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionReflectionProbes_InstantExecute_m9E5F655977D6FD5F09EF8CC2D5D03A28F660551A (void);
// 0x000004C4 System.Collections.IEnumerator GameCreator.Core.ActionReflectionProbes::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionReflectionProbes_Execute_m5A918D81E680B42CF41A202C722F9B6CA4DA1475 (void);
// 0x000004C5 System.Void GameCreator.Core.ActionReflectionProbes::.ctor()
extern void ActionReflectionProbes__ctor_m09D7F2D65DBDBAE81CF8F772BD0BAE3D112AE5A0 (void);
// 0x000004C6 System.Void GameCreator.Core.ActionReflectionProbes/<Execute>d__4::.ctor(System.Int32)
extern void U3CExecuteU3Ed__4__ctor_mE3705328BA02A5C6804D656A84D2F01521659B17 (void);
// 0x000004C7 System.Void GameCreator.Core.ActionReflectionProbes/<Execute>d__4::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__4_System_IDisposable_Dispose_mBF5E03402ECCA456AD7654A8C19ECD5785307A89 (void);
// 0x000004C8 System.Boolean GameCreator.Core.ActionReflectionProbes/<Execute>d__4::MoveNext()
extern void U3CExecuteU3Ed__4_MoveNext_m843C3CC7620B1BCE167E2B3D455DEAFD1C1CE55F (void);
// 0x000004C9 System.Object GameCreator.Core.ActionReflectionProbes/<Execute>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD002A5B232EDAB262AD2577DA21E86AE289242CA (void);
// 0x000004CA System.Void GameCreator.Core.ActionReflectionProbes/<Execute>d__4::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mAF5226AA4EF8C651DDF193AA52AF3F58E3720CE6 (void);
// 0x000004CB System.Object GameCreator.Core.ActionReflectionProbes/<Execute>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mE081B69A7BC0703684ACD7D36C0899EBDD746C3B (void);
// 0x000004CC System.Collections.IEnumerator GameCreator.Core.ActionRestartActions::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionRestartActions_Execute_m141874EBFFD0D71B3EEBC622D98CB24E7FB22955 (void);
// 0x000004CD System.Void GameCreator.Core.ActionRestartActions::.ctor()
extern void ActionRestartActions__ctor_mDCB68BABA7D7851A666AFC269B0DB22B07774A2C (void);
// 0x000004CE System.Void GameCreator.Core.ActionRestartActions/<Execute>d__0::.ctor(System.Int32)
extern void U3CExecuteU3Ed__0__ctor_m52231F20C05F8C80A3ED4721BF367C1DDFB06CE2 (void);
// 0x000004CF System.Void GameCreator.Core.ActionRestartActions/<Execute>d__0::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__0_System_IDisposable_Dispose_mF52E298FC3DC9C9456316D73F74C0005B769B4E0 (void);
// 0x000004D0 System.Boolean GameCreator.Core.ActionRestartActions/<Execute>d__0::MoveNext()
extern void U3CExecuteU3Ed__0_MoveNext_mDD9693A504CAF31C0405F9E72BC6DA6F4838A845 (void);
// 0x000004D1 System.Object GameCreator.Core.ActionRestartActions/<Execute>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37052B91459D04361E63F25F8DCA3E4C52A95CEA (void);
// 0x000004D2 System.Void GameCreator.Core.ActionRestartActions/<Execute>d__0::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__0_System_Collections_IEnumerator_Reset_mB1712CAF9C824B2468F658B4D5A50B88CE06D799 (void);
// 0x000004D3 System.Object GameCreator.Core.ActionRestartActions/<Execute>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__0_System_Collections_IEnumerator_get_Current_m3C22C51AFB94DA83D80DB69FBF40AAAAD12D9356 (void);
// 0x000004D4 System.Boolean GameCreator.Core.ActionStoreRaycastMouse::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionStoreRaycastMouse_InstantExecute_m4E307965AD8CF419D53CAD86C403EAAE89B6B6AA (void);
// 0x000004D5 System.Void GameCreator.Core.ActionStoreRaycastMouse::.ctor()
extern void ActionStoreRaycastMouse__ctor_m72C7F9C84F1209C415794D6A31E1F149D32EBFDB (void);
// 0x000004D6 System.Boolean GameCreator.Core.ActionTimescale::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTimescale_InstantExecute_m3A69C005C907A4A54BB9A3068C7384372EBD9D0E (void);
// 0x000004D7 System.Void GameCreator.Core.ActionTimescale::.ctor()
extern void ActionTimescale__ctor_mFA188EF2F76835C714F251FC1B02E4EE89447E87 (void);
// 0x000004D8 System.Collections.IEnumerator GameCreator.Core.ActionWait::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionWait_Execute_m04586A3FDF625719D21C4722DF245176A7190D66 (void);
// 0x000004D9 System.Void GameCreator.Core.ActionWait::Stop()
extern void ActionWait_Stop_m0454C181F028BB73E8E7422F5049A07D1D2458C6 (void);
// 0x000004DA System.Void GameCreator.Core.ActionWait::.ctor()
extern void ActionWait__ctor_m95D6E42172C9B182EAC6BFC4F8B54AE5B70CF7AA (void);
// 0x000004DB System.Void GameCreator.Core.ActionWait/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mFCA1EDB4D982DD2330E98FED3942A2AFAF2F4CB5 (void);
// 0x000004DC System.Boolean GameCreator.Core.ActionWait/<>c__DisplayClass2_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CExecuteU3Eb__0_m3AC3D9D6CFF98C3F1D9E5B52F7A41DC0F7EFEF58 (void);
// 0x000004DD System.Void GameCreator.Core.ActionWait/<Execute>d__2::.ctor(System.Int32)
extern void U3CExecuteU3Ed__2__ctor_m1C9147D9500D8992417D62EDB2969850D0BCA95F (void);
// 0x000004DE System.Void GameCreator.Core.ActionWait/<Execute>d__2::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__2_System_IDisposable_Dispose_m46DB6B72EF206B2AA46FB515BB10A3E6CE5B8FB8 (void);
// 0x000004DF System.Boolean GameCreator.Core.ActionWait/<Execute>d__2::MoveNext()
extern void U3CExecuteU3Ed__2_MoveNext_mAD649B1285179EBEF4960C810D3EBC8EBF3C0818 (void);
// 0x000004E0 System.Object GameCreator.Core.ActionWait/<Execute>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E1BA8ED4D7167A7EC8B04C9E249F6C95F7C911 (void);
// 0x000004E1 System.Void GameCreator.Core.ActionWait/<Execute>d__2::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mEDA5AA1846C72B143E01D49133FD304FA342C9E4 (void);
// 0x000004E2 System.Object GameCreator.Core.ActionWait/<Execute>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_mD1D01BD5AE2C54941C4051CFEF0DE1FE6E368F8D (void);
// 0x000004E3 System.Boolean GameCreator.Core.ActionAddComponent::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAddComponent_InstantExecute_m9D1B18FBA54D22E221F1857DFECCAA8FC86CF0C3 (void);
// 0x000004E4 System.Void GameCreator.Core.ActionAddComponent::.ctor()
extern void ActionAddComponent__ctor_mFB16A30B5F80CCF6E292B30E5121ECA13C2309F1 (void);
// 0x000004E5 System.Boolean GameCreator.Core.ActionAnimate::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAnimate_InstantExecute_mE96B8B0287291CD5F2529121E40DDD5FF3F0506E (void);
// 0x000004E6 System.Void GameCreator.Core.ActionAnimate::.ctor()
extern void ActionAnimate__ctor_m123DCDE1CAE994A7B1B66995AE5009FF4E518DD6 (void);
// 0x000004E7 System.Boolean GameCreator.Core.ActionAnimatorLayer::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAnimatorLayer_InstantExecute_m7384E1C6524275E4663D34F52A1610EA68547109 (void);
// 0x000004E8 System.Void GameCreator.Core.ActionAnimatorLayer::.ctor()
extern void ActionAnimatorLayer__ctor_mEF8B588CFABB3AC894CB5168430AAD54EE8C030A (void);
// 0x000004E9 System.Collections.IEnumerator GameCreator.Core.ActionBlendShape::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionBlendShape_Execute_mA4756B727376FBA06CDDC8C25893BB0FE8EEE787 (void);
// 0x000004EA System.Void GameCreator.Core.ActionBlendShape::SetWeights(System.Collections.Generic.List`1<GameCreator.Core.ActionBlendShape/SMRData>,System.Single)
extern void ActionBlendShape_SetWeights_m59BD8EAE316DB6032F28C5F6F349681FA85BF058 (void);
// 0x000004EB System.Void GameCreator.Core.ActionBlendShape::.ctor()
extern void ActionBlendShape__ctor_mA2A6DE6F31397E3765DBDC0B97F11B598A3E6F13 (void);
// 0x000004EC System.Void GameCreator.Core.ActionBlendShape/SMRData::.ctor(UnityEngine.SkinnedMeshRenderer,System.Int32,System.Single)
extern void SMRData__ctor_m9C24890B024A2CB176EB4DC143E445339BCEEEF7 (void);
// 0x000004ED System.Void GameCreator.Core.ActionBlendShape/<Execute>d__5::.ctor(System.Int32)
extern void U3CExecuteU3Ed__5__ctor_mF19135E1262F01401F62B65C6AAE21E195800841 (void);
// 0x000004EE System.Void GameCreator.Core.ActionBlendShape/<Execute>d__5::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__5_System_IDisposable_Dispose_mB01B52B59D5766C1B8DCB01058767AAE92EE08A3 (void);
// 0x000004EF System.Boolean GameCreator.Core.ActionBlendShape/<Execute>d__5::MoveNext()
extern void U3CExecuteU3Ed__5_MoveNext_mE0E30A5D3B6AA02B1071BA303F62D231321C4485 (void);
// 0x000004F0 System.Object GameCreator.Core.ActionBlendShape/<Execute>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB6BDC403B3CC4A1A6CC2BAC53F492A49EC68B33 (void);
// 0x000004F1 System.Void GameCreator.Core.ActionBlendShape/<Execute>d__5::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m7E4256ABD8A2DB3A8A5DD1F625898904BD4749E5 (void);
// 0x000004F2 System.Object GameCreator.Core.ActionBlendShape/<Execute>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mD6A3F2C62C6A20DBC26A9983D538106441A5CE1D (void);
// 0x000004F3 System.Boolean GameCreator.Core.ActionChangeColor::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeColor_InstantExecute_mA50C0E69898880B5AACBEE76DBC6579D2634D11F (void);
// 0x000004F4 System.Void GameCreator.Core.ActionChangeColor::.ctor()
extern void ActionChangeColor__ctor_m83ACC636AA8E376D7B770871D0359347A2A7E133 (void);
// 0x000004F5 System.Boolean GameCreator.Core.ActionChangeMaterial::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeMaterial_InstantExecute_m5CDC7FDBBDF93E6BE031F56E765ECD02A27C5957 (void);
// 0x000004F6 System.Void GameCreator.Core.ActionChangeMaterial::.ctor()
extern void ActionChangeMaterial__ctor_mB1FFD9ACD3AEAF80F38B379414716E9A071EA26C (void);
// 0x000004F7 System.Boolean GameCreator.Core.ActionChangeTag::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeTag_InstantExecute_m699FF0439862332679B22012A7028AE0C06842A1 (void);
// 0x000004F8 System.Void GameCreator.Core.ActionChangeTag::.ctor()
extern void ActionChangeTag__ctor_m802B4FF8CEC2BFFA944DA736DEC56835B496C5E4 (void);
// 0x000004F9 System.Boolean GameCreator.Core.ActionChangeTexture::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeTexture_InstantExecute_mC5BE545615EF574B8515D829D077AAD7554A35E0 (void);
// 0x000004FA System.Void GameCreator.Core.ActionChangeTexture::.ctor()
extern void ActionChangeTexture__ctor_m9311B2AC5B500BA6C005C511D227670A602B7356 (void);
// 0x000004FB System.Boolean GameCreator.Core.ActionDestroy::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDestroy_InstantExecute_mD17B89FD1CB4939E25AFD5DD0B03E09311603DB7 (void);
// 0x000004FC System.Void GameCreator.Core.ActionDestroy::.ctor()
extern void ActionDestroy__ctor_m4B75F43F01E25677B5795F27D63502F7864291F7 (void);
// 0x000004FD System.Boolean GameCreator.Core.ActionEnableComponent::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionEnableComponent_InstantExecute_m51547AAF5ACC530DBB11288F985566700B03ABEE (void);
// 0x000004FE System.Void GameCreator.Core.ActionEnableComponent::.ctor()
extern void ActionEnableComponent__ctor_m694AE3192C53034852D45D0696295BF3962FAA59 (void);
// 0x000004FF System.Boolean GameCreator.Core.ActionExplosion::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionExplosion_InstantExecute_mA900922CC39142D1D944096B03CBD0362321B336 (void);
// 0x00000500 System.Void GameCreator.Core.ActionExplosion::.ctor()
extern void ActionExplosion__ctor_m98F32A978F5535009B7CF233AB75DAF4666053BD (void);
// 0x00000501 System.Boolean GameCreator.Core.ActionInstantiate::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionInstantiate_InstantExecute_mF1C6C108E2F9C86AA2BA1DAB278FB7E9FFEE2CCD (void);
// 0x00000502 System.Void GameCreator.Core.ActionInstantiate::.ctor()
extern void ActionInstantiate__ctor_m29CE0FEA8508669E97DC1D211C5EE342F7F0CCAC (void);
// 0x00000503 System.Boolean GameCreator.Core.ActionInstantiatePool::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionInstantiatePool_InstantExecute_m3521ED1EEED965B9D53A0419BFC331A801144DA2 (void);
// 0x00000504 System.Void GameCreator.Core.ActionInstantiatePool::.ctor()
extern void ActionInstantiatePool__ctor_m31C52412692869459F210405B0593FD9E8921832 (void);
// 0x00000505 System.Boolean GameCreator.Core.ActionLight::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLight_InstantExecute_m250124C567D86437B39DC4F49287C870A94F42BE (void);
// 0x00000506 System.Void GameCreator.Core.ActionLight::.ctor()
extern void ActionLight__ctor_m56896D4416C85A77962E1F2DBE8BF917F6456838 (void);
// 0x00000507 System.Boolean GameCreator.Core.ActionLookAt::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLookAt_InstantExecute_m5D8135C2E073D36EB5DC73EF2232EFD585A37E16 (void);
// 0x00000508 System.Void GameCreator.Core.ActionLookAt::.ctor()
extern void ActionLookAt__ctor_m31872EDB0F1C8C10EB460CD121EE2C32F44EA9C6 (void);
// 0x00000509 System.Collections.IEnumerator GameCreator.Core.ActionMaterialTransitionValue::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionMaterialTransitionValue_Execute_m5532403A5A5539119EFFC92D16650C435CE447DF (void);
// 0x0000050A System.Void GameCreator.Core.ActionMaterialTransitionValue::.ctor()
extern void ActionMaterialTransitionValue__ctor_m5BFBB0C1353452348FDA15D828F3283B1D263E84 (void);
// 0x0000050B System.Void GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::.ctor(System.Int32)
extern void U3CExecuteU3Ed__8__ctor_m63455629CDFC718B52F66A855AA973034CA1EC15 (void);
// 0x0000050C System.Void GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__8_System_IDisposable_Dispose_m33F4E3AABA145A6F5929A9646A66A74F9B8EB3F6 (void);
// 0x0000050D System.Boolean GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::MoveNext()
extern void U3CExecuteU3Ed__8_MoveNext_mB2C54BADEEF83C1C1E2657A39D187D5BF2094BD5 (void);
// 0x0000050E System.Object GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m921C4AE7D1431654297FBFF722D990452EAB020D (void);
// 0x0000050F System.Void GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEBCDDDC6151242BB567F141352561DD344537C13 (void);
// 0x00000510 System.Object GameCreator.Core.ActionMaterialTransitionValue/<Execute>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m4D68358EA683E8DFAA8FB743BAD43BCC240A86D1 (void);
// 0x00000511 System.Boolean GameCreator.Core.ActionNearestComponent::FilterCondition(UnityEngine.GameObject)
extern void ActionNearestComponent_FilterCondition_m1E6F4586541015561204AE2EAF7CA502BAAF0090 (void);
// 0x00000512 System.Void GameCreator.Core.ActionNearestComponent::.ctor()
extern void ActionNearestComponent__ctor_m15250BAF56E780913829C7C0DA5697B8ACAC5052 (void);
// 0x00000513 System.Int32 GameCreator.Core.ActionNearestInLayer::FilterLayerMask()
extern void ActionNearestInLayer_FilterLayerMask_m0C97B76234D25844476013A88D9E0F92A7D5C6B1 (void);
// 0x00000514 System.Void GameCreator.Core.ActionNearestInLayer::.ctor()
extern void ActionNearestInLayer__ctor_m28E6C7D94ECDD7D98FDBF48E0709184134626E33 (void);
// 0x00000515 System.Boolean GameCreator.Core.ActionNearestTag::FilterCondition(UnityEngine.GameObject)
extern void ActionNearestTag_FilterCondition_m749FD438C1C88D027826B8FAAC4A23841ED3CF76 (void);
// 0x00000516 System.Void GameCreator.Core.ActionNearestTag::.ctor()
extern void ActionNearestTag__ctor_m4D9CB6D60FE40718DF59289E44AF254AD17EBD26 (void);
// 0x00000517 System.Boolean GameCreator.Core.ActionNearestVariable::FilterCondition(UnityEngine.GameObject)
extern void ActionNearestVariable_FilterCondition_mC680A0696E4D7C59BF72F5E37776ADA7E21ACC54 (void);
// 0x00000518 System.Void GameCreator.Core.ActionNearestVariable::.ctor()
extern void ActionNearestVariable__ctor_m746DFBD74A6E2227DED3CC9CD6D06D31ED05A52D (void);
// 0x00000519 System.Boolean GameCreator.Core.IActionNearest::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void IActionNearest_InstantExecute_m1CBC737222CE8B396C9A57D64466B625E4C09788 (void);
// 0x0000051A System.Boolean GameCreator.Core.IActionNearest::FilterCondition(UnityEngine.GameObject)
extern void IActionNearest_FilterCondition_m4DF489DB4954ED56B8D20F86F2F54C4F06A4E608 (void);
// 0x0000051B System.Int32 GameCreator.Core.IActionNearest::FilterLayerMask()
extern void IActionNearest_FilterLayerMask_m2E2708DB03C8C5D170527BCB71B191AD0F7A476E (void);
// 0x0000051C UnityEngine.Collider[] GameCreator.Core.IActionNearest::GatherColliders(UnityEngine.GameObject)
extern void IActionNearest_GatherColliders_m4D9D2AC8FEB557650035F440E66FD8D0751DBE01 (void);
// 0x0000051D System.Void GameCreator.Core.IActionNearest::.ctor()
extern void IActionNearest__ctor_mBA4C046CDC51E7642A296F47BE99FEB98E9D6266 (void);
// 0x0000051E System.Boolean GameCreator.Core.ActionPhysics::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionPhysics_InstantExecute_m75EB3FC9E8756A1E4A55CE658721E7EC5F708470 (void);
// 0x0000051F System.Void GameCreator.Core.ActionPhysics::.ctor()
extern void ActionPhysics__ctor_m4BC89263118AA154361D85F225B223A428D864BD (void);
// 0x00000520 System.Boolean GameCreator.Core.ActionRigidbody::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionRigidbody_InstantExecute_mBF6D50FC2886324F9CDBC9267F949446F653694C (void);
// 0x00000521 System.Void GameCreator.Core.ActionRigidbody::.ctor()
extern void ActionRigidbody__ctor_m126570E9009E175916DFD2E2E224ABA78C25274F (void);
// 0x00000522 System.Boolean GameCreator.Core.ActionSendMessage::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionSendMessage_InstantExecute_m4C84B6C0E7AA563748ED176D8243AB6B8278E04D (void);
// 0x00000523 System.Void GameCreator.Core.ActionSendMessage::.ctor()
extern void ActionSendMessage__ctor_m34C95C4AB959567A8437BB4D35C1844F5D896831 (void);
// 0x00000524 System.Boolean GameCreator.Core.ActionSetActive::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionSetActive_InstantExecute_m739FDC4A4158715FD2D39734239AEF98F070A78B (void);
// 0x00000525 System.Void GameCreator.Core.ActionSetActive::.ctor()
extern void ActionSetActive__ctor_m349AC9AA6161521239D1335449B01334DCCC4CA3 (void);
// 0x00000526 System.Boolean GameCreator.Core.ActionTimeline::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTimeline_InstantExecute_m2ABA190F897CF55D01108D301F02E6725236CDF7 (void);
// 0x00000527 System.Void GameCreator.Core.ActionTimeline::.ctor()
extern void ActionTimeline__ctor_mCD43DA95E2AC2B102667EA78CB527D2ABE548F36 (void);
// 0x00000528 System.Boolean GameCreator.Core.ActionToggleActive::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionToggleActive_InstantExecute_m59E4EB2403262DEB9CA4791FDC824C11E21C683D (void);
// 0x00000529 System.Void GameCreator.Core.ActionToggleActive::.ctor()
extern void ActionToggleActive__ctor_m3627497EECD7251BFF209445E9355D70D017E208 (void);
// 0x0000052A System.Boolean GameCreator.Core.ActionTransform::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTransform_InstantExecute_m10203CAD81F56B78D7367E1D728CBD3D77273A06 (void);
// 0x0000052B System.Void GameCreator.Core.ActionTransform::.ctor()
extern void ActionTransform__ctor_m89BBDC5F692A75C4B8EEE4A4F75667AEEAF014DB (void);
// 0x0000052C System.Collections.IEnumerator GameCreator.Core.ActionTransformMove::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTransformMove_Execute_m83A314D67419B65DB5ADD6C174738DFE15984D53 (void);
// 0x0000052D System.Void GameCreator.Core.ActionTransformMove::Stop()
extern void ActionTransformMove_Stop_m59B133326F77C1AFDC7BF17CE76855BB87D550BB (void);
// 0x0000052E System.Void GameCreator.Core.ActionTransformMove::.ctor()
extern void ActionTransformMove__ctor_m8B04E1BD88E0D726447024B6D36183128FFA1866 (void);
// 0x0000052F System.Void GameCreator.Core.ActionTransformMove/<Execute>d__8::.ctor(System.Int32)
extern void U3CExecuteU3Ed__8__ctor_mBBC55362DD6F73D1A9D3FD8C3F815D57E473521A (void);
// 0x00000530 System.Void GameCreator.Core.ActionTransformMove/<Execute>d__8::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__8_System_IDisposable_Dispose_m3BE76413C35E97EE41BF887F5C94338A08F59274 (void);
// 0x00000531 System.Boolean GameCreator.Core.ActionTransformMove/<Execute>d__8::MoveNext()
extern void U3CExecuteU3Ed__8_MoveNext_mB0D3F93FADD140BDA6520C5E2CB363D4FB310DC3 (void);
// 0x00000532 System.Object GameCreator.Core.ActionTransformMove/<Execute>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B2FDCD99FC7D00F3F2E45033E4D4E43C434EF29 (void);
// 0x00000533 System.Void GameCreator.Core.ActionTransformMove/<Execute>d__8::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_m9BFD11409852AC030EAD6175AF52C6C04FF2797B (void);
// 0x00000534 System.Object GameCreator.Core.ActionTransformMove/<Execute>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m826A920F08D5B9712D8CAD38A583A9014C784C76 (void);
// 0x00000535 System.Collections.IEnumerator GameCreator.Core.ActionTransformRotate::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTransformRotate_Execute_m52E7014DC3C1A4BC3CDDBA1EF21E37AF4EE154C0 (void);
// 0x00000536 System.Void GameCreator.Core.ActionTransformRotate::Stop()
extern void ActionTransformRotate_Stop_m531AE2935EC17C19D1A215EAEE3A432732FAC674 (void);
// 0x00000537 System.Void GameCreator.Core.ActionTransformRotate::.ctor()
extern void ActionTransformRotate__ctor_m897781FCCF31C107FA4188757DB61C26611D1CE7 (void);
// 0x00000538 System.Void GameCreator.Core.ActionTransformRotate/<Execute>d__6::.ctor(System.Int32)
extern void U3CExecuteU3Ed__6__ctor_m3EA267B4AAEB1FB64D9098EE4ACCA34AEA590FF8 (void);
// 0x00000539 System.Void GameCreator.Core.ActionTransformRotate/<Execute>d__6::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__6_System_IDisposable_Dispose_m213250BD24E8EE8853B913051A787012D435EB5F (void);
// 0x0000053A System.Boolean GameCreator.Core.ActionTransformRotate/<Execute>d__6::MoveNext()
extern void U3CExecuteU3Ed__6_MoveNext_mF24A97E2BD236D37AFFF5C83AF02AF6A519425EC (void);
// 0x0000053B System.Object GameCreator.Core.ActionTransformRotate/<Execute>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0305DABCAE84612AD3F76206412BD342D52129E (void);
// 0x0000053C System.Void GameCreator.Core.ActionTransformRotate/<Execute>d__6::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m204D54BF27D7CFB6F425C2D20648D125078A38A6 (void);
// 0x0000053D System.Object GameCreator.Core.ActionTransformRotate/<Execute>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_mE4DFC63A85A78AD6793EA847AC75148919B971AA (void);
// 0x0000053E System.Collections.IEnumerator GameCreator.Core.ActionTransformRotateTowards::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTransformRotateTowards_Execute_m8C328A448FA212122A818FAEC0C3D3AC2404A146 (void);
// 0x0000053F System.Void GameCreator.Core.ActionTransformRotateTowards::Stop()
extern void ActionTransformRotateTowards_Stop_m060D5EB12C9C4C4137DED1F0C904446E58EA179E (void);
// 0x00000540 System.Void GameCreator.Core.ActionTransformRotateTowards::.ctor()
extern void ActionTransformRotateTowards__ctor_mF3D34881EA18D59FD7E395DB215328EBBE1D7802 (void);
// 0x00000541 System.Void GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::.ctor(System.Int32)
extern void U3CExecuteU3Ed__5__ctor_m0706F423CF92DB9367533BF6A9123319F8CA8278 (void);
// 0x00000542 System.Void GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__5_System_IDisposable_Dispose_mCD2F8FA04E201EF31D00ACA8DD462E0A1E22BB9D (void);
// 0x00000543 System.Boolean GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::MoveNext()
extern void U3CExecuteU3Ed__5_MoveNext_m9A64B969940A044F56EDD54925F61AF5436F7766 (void);
// 0x00000544 System.Object GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF72942B4B178CA78A6749390E9BC859525A6EA48 (void);
// 0x00000545 System.Void GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m398E925CAA656EED3C0632C1A3ACBD8F10B14364 (void);
// 0x00000546 System.Object GameCreator.Core.ActionTransformRotateTowards/<Execute>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mBA9BBCAB06CC9DF538CC5EC1E17E21165FD96FA9 (void);
// 0x00000547 System.Boolean GameCreator.Core.ActionTrigger::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTrigger_InstantExecute_m1AD43F047FF26D259044A8D34A2AF4F7B34AD5BD (void);
// 0x00000548 System.Void GameCreator.Core.ActionTrigger::.ctor()
extern void ActionTrigger__ctor_mE2FD2B01CCE53A61B2756DF7AFEC3309691538FA (void);
// 0x00000549 System.Boolean GameCreator.Core.ActionCurrentProfile::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCurrentProfile_InstantExecute_mF8D88BF26892A966CAA0AA907CDF10B874892638 (void);
// 0x0000054A System.Void GameCreator.Core.ActionCurrentProfile::.ctor()
extern void ActionCurrentProfile__ctor_m1FA23827EF7CAFF7ED178EA33D9224BDA5DCC589 (void);
// 0x0000054B System.Boolean GameCreator.Core.ActionDeleteProfile::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionDeleteProfile_InstantExecute_m015A41742C8B50D2A42ECAB5C002D590E193F15B (void);
// 0x0000054C System.Void GameCreator.Core.ActionDeleteProfile::.ctor()
extern void ActionDeleteProfile__ctor_mE1D62B073244A7A514585647B3F6A3DDDE3B7339 (void);
// 0x0000054D System.Collections.IEnumerator GameCreator.Core.ActionLoadGame::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLoadGame_Execute_mCA4BCD88110DF8FFB57B75DA02CA1D13A1B9E82A (void);
// 0x0000054E System.Void GameCreator.Core.ActionLoadGame::OnLoad()
extern void ActionLoadGame_OnLoad_m1EDD3548779082FE47F15961E646EA5F74FE72AD (void);
// 0x0000054F System.Void GameCreator.Core.ActionLoadGame::.ctor()
extern void ActionLoadGame__ctor_mE583DCDC0289E2D0BCE694BA557542269D4FF2CE (void);
// 0x00000550 System.Boolean GameCreator.Core.ActionLoadGame::<Execute>b__3_0()
extern void ActionLoadGame_U3CExecuteU3Eb__3_0_m5D65A8280B7151F6FBAD8401F772A0B860F290E0 (void);
// 0x00000551 System.Void GameCreator.Core.ActionLoadGame/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_mEF7EE534F5AB0E4EC07E539E821DA3AC95D970D1 (void);
// 0x00000552 System.Void GameCreator.Core.ActionLoadGame/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_m2DDDC6B615D89AC51CF63297B2EB8A205C1AAE4B (void);
// 0x00000553 System.Boolean GameCreator.Core.ActionLoadGame/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_mD975623873DCC084758FD155D4FAD34E98981763 (void);
// 0x00000554 System.Object GameCreator.Core.ActionLoadGame/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C3C7C7718E40C63F6851C0502FBCBAE5D51D10 (void);
// 0x00000555 System.Void GameCreator.Core.ActionLoadGame/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m6110CA08FADF00FD94D352C8091DE1191313BC5C (void);
// 0x00000556 System.Object GameCreator.Core.ActionLoadGame/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mC29C82265D7A10E38E4D4595D194B45C4A17ABE8 (void);
// 0x00000557 System.Collections.IEnumerator GameCreator.Core.ActionLoadLastGame::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLoadLastGame_Execute_m0661A69EA5997349140E8D5EB8F76E601B056394 (void);
// 0x00000558 System.Void GameCreator.Core.ActionLoadLastGame::OnLoad()
extern void ActionLoadLastGame_OnLoad_mD80F24B17F011BAE5B0DC01708BE8B4F0429C627 (void);
// 0x00000559 System.Void GameCreator.Core.ActionLoadLastGame::.ctor()
extern void ActionLoadLastGame__ctor_m9850637435D51E049F808E6CB6DFE6B18FBB988F (void);
// 0x0000055A System.Boolean GameCreator.Core.ActionLoadLastGame::<Execute>b__1_0()
extern void ActionLoadLastGame_U3CExecuteU3Eb__1_0_m03276802B4B26570E825CDA0F2E78DFE703FD2EE (void);
// 0x0000055B System.Void GameCreator.Core.ActionLoadLastGame/<Execute>d__1::.ctor(System.Int32)
extern void U3CExecuteU3Ed__1__ctor_mDED1314A51DDE61865770C493D2D9CCDE42C4957 (void);
// 0x0000055C System.Void GameCreator.Core.ActionLoadLastGame/<Execute>d__1::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__1_System_IDisposable_Dispose_mB03A5268D828F14A443D72BCD62F9977A7434C2E (void);
// 0x0000055D System.Boolean GameCreator.Core.ActionLoadLastGame/<Execute>d__1::MoveNext()
extern void U3CExecuteU3Ed__1_MoveNext_m1474DAE3F9DE27390296341603CE9A80343FFBDC (void);
// 0x0000055E System.Object GameCreator.Core.ActionLoadLastGame/<Execute>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE706719587A7A7BAE947DAB178A404636438E62A (void);
// 0x0000055F System.Void GameCreator.Core.ActionLoadLastGame/<Execute>d__1::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m0A98AD78FF4D9FDFAFB4E2B4ADC7E918ECE084B7 (void);
// 0x00000560 System.Object GameCreator.Core.ActionLoadLastGame/<Execute>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m14B357E056A4AADB78A9CADBC779A46325788ACD (void);
// 0x00000561 System.Collections.IEnumerator GameCreator.Core.ActionSaveGame::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionSaveGame_Execute_m5C49646C313E617CF557E9FEE2D5EBC3AAA9FA10 (void);
// 0x00000562 System.Void GameCreator.Core.ActionSaveGame::.ctor()
extern void ActionSaveGame__ctor_mDA66363E5C00A9CDEAD4E50CDA430E162FE4CFB4 (void);
// 0x00000563 System.Void GameCreator.Core.ActionSaveGame/<Execute>d__2::.ctor(System.Int32)
extern void U3CExecuteU3Ed__2__ctor_m1A294E66E9D46043E2E23F4544014588A8D604B5 (void);
// 0x00000564 System.Void GameCreator.Core.ActionSaveGame/<Execute>d__2::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__2_System_IDisposable_Dispose_m14FDC26EDECC814A10CAA9F29C23A76370143B6D (void);
// 0x00000565 System.Boolean GameCreator.Core.ActionSaveGame/<Execute>d__2::MoveNext()
extern void U3CExecuteU3Ed__2_MoveNext_mF7ED2256C924F29E662472C549BC1DFF1F036A68 (void);
// 0x00000566 System.Object GameCreator.Core.ActionSaveGame/<Execute>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF519EBD37B180B513D8F652C309B25B1F9E64536 (void);
// 0x00000567 System.Void GameCreator.Core.ActionSaveGame/<Execute>d__2::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mF841C3F84566662D6B63615136CA3AD0C469B029 (void);
// 0x00000568 System.Object GameCreator.Core.ActionSaveGame/<Execute>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m488D15C23530AC60B5EA48F0D808A137B7F2ECDC (void);
// 0x00000569 System.Collections.IEnumerator GameCreator.Core.ActionLoadScene::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLoadScene_Execute_mE4830BE17B75DC6B0D7C331BA8B1D2D73A4821A3 (void);
// 0x0000056A System.Void GameCreator.Core.ActionLoadScene::.ctor()
extern void ActionLoadScene__ctor_m93D2900CD98A09DEBE182ADF1CFD0F5C09FD85FE (void);
// 0x0000056B System.Void GameCreator.Core.ActionLoadScene/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_mB38F8F46EAA249E49BF209D521CA8E4DAD872375 (void);
// 0x0000056C System.Void GameCreator.Core.ActionLoadScene/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_m9A55C15113480F1DFED0E5D92EFCA663FAD44C22 (void);
// 0x0000056D System.Boolean GameCreator.Core.ActionLoadScene/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_m7FAA086CC4758C929AA9429144C6B28462C5D3BC (void);
// 0x0000056E System.Object GameCreator.Core.ActionLoadScene/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0C73195701400504EFD358451A634E203F71E2 (void);
// 0x0000056F System.Void GameCreator.Core.ActionLoadScene/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mC95F035CC2C00DD21F7CEB6C10C00AC264472D05 (void);
// 0x00000570 System.Object GameCreator.Core.ActionLoadScene/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m61B9FE124AD3E7B14B38A197435F1766A601DAC8 (void);
// 0x00000571 System.Boolean GameCreator.Core.ActionLoadScenePlayer::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionLoadScenePlayer_InstantExecute_m64439B108A4AC8C5747CEA015432D1DE6B19C4C4 (void);
// 0x00000572 System.Void GameCreator.Core.ActionLoadScenePlayer::.ctor()
extern void ActionLoadScenePlayer__ctor_m994D988E05BCC1308A28EE448608769D7D574BF7 (void);
// 0x00000573 System.Collections.IEnumerator GameCreator.Core.ActionUnloadSceneAsync::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionUnloadSceneAsync_Execute_m221F4EA5967752F1CE262C88154BBE883F5C5D7C (void);
// 0x00000574 System.Void GameCreator.Core.ActionUnloadSceneAsync::.ctor()
extern void ActionUnloadSceneAsync__ctor_m50F8810D8C184399847A4FB80955688F18BCFF96 (void);
// 0x00000575 System.Void GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::.ctor(System.Int32)
extern void U3CExecuteU3Ed__1__ctor_m50099F295DA27DCA901FA8869D15AABEFB2A89F8 (void);
// 0x00000576 System.Void GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__1_System_IDisposable_Dispose_m73C667B2CD0942FC8C1D621030F62DE6EBCF43EE (void);
// 0x00000577 System.Boolean GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::MoveNext()
extern void U3CExecuteU3Ed__1_MoveNext_mA436F81DAD07F2FE9ED0B42D650ADB7A4119BA4A (void);
// 0x00000578 System.Object GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD24FFC8179AF6C49CF69C41720A1CA67CCCC3FF5 (void);
// 0x00000579 System.Void GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m7A06787F5F87A17EDF4B9F7E89E5F121D17FAA6B (void);
// 0x0000057A System.Object GameCreator.Core.ActionUnloadSceneAsync/<Execute>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m302C6CB72E7DC52A8CE4D1CF360309C511F79770 (void);
// 0x0000057B System.Boolean GameCreator.Core.ActionCanvasGroup::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCanvasGroup_InstantExecute_mFC8DCE1FB7CDDE9BC98C4E2A5C4DB9F970A92A8B (void);
// 0x0000057C System.Collections.IEnumerator GameCreator.Core.ActionCanvasGroup::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCanvasGroup_Execute_m99266FDCA2C58E3B774F0F5D01CE04A0755E06DE (void);
// 0x0000057D System.Void GameCreator.Core.ActionCanvasGroup::.ctor()
extern void ActionCanvasGroup__ctor_mC06D186F04852AACAE1C628725BF7B4474750CB6 (void);
// 0x0000057E System.Void GameCreator.Core.ActionCanvasGroup/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mB76E5D3A9023480D59247E3012AA072155CE1550 (void);
// 0x0000057F System.Void GameCreator.Core.ActionCanvasGroup/<>c__DisplayClass6_1::.ctor()
extern void U3CU3Ec__DisplayClass6_1__ctor_m21D6D310D996DD859BAC20B62F54E85D60D0BF3D (void);
// 0x00000580 System.Boolean GameCreator.Core.ActionCanvasGroup/<>c__DisplayClass6_1::<Execute>b__0()
extern void U3CU3Ec__DisplayClass6_1_U3CExecuteU3Eb__0_mD9C59D9AAACFDE86A1DDAD4C655EA101B04C6A20 (void);
// 0x00000581 System.Void GameCreator.Core.ActionCanvasGroup/<Execute>d__6::.ctor(System.Int32)
extern void U3CExecuteU3Ed__6__ctor_m27E44C3A9DF57ADC577B19802B5193AE67FF720F (void);
// 0x00000582 System.Void GameCreator.Core.ActionCanvasGroup/<Execute>d__6::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__6_System_IDisposable_Dispose_m6EDEDC583396BC219AC77E7CC4519AFF08AA95E0 (void);
// 0x00000583 System.Boolean GameCreator.Core.ActionCanvasGroup/<Execute>d__6::MoveNext()
extern void U3CExecuteU3Ed__6_MoveNext_m718B85A7994C030999F57C1287E1EC1B4CDBD311 (void);
// 0x00000584 System.Object GameCreator.Core.ActionCanvasGroup/<Execute>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DC9E2350D3BAFA324B15BADB658E25EC071426 (void);
// 0x00000585 System.Void GameCreator.Core.ActionCanvasGroup/<Execute>d__6::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m9A9BC40719774A6E6D1F34F82FE21CF69C9A2B55 (void);
// 0x00000586 System.Object GameCreator.Core.ActionCanvasGroup/<Execute>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m2AF14DBD96EB0B156A6600D4C0B33F3BB952F55D (void);
// 0x00000587 System.Boolean GameCreator.Core.ActionChangeFontSize::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeFontSize_InstantExecute_m8188DF05DFDD64362707948AEFA550DA440C594A (void);
// 0x00000588 System.Void GameCreator.Core.ActionChangeFontSize::.ctor()
extern void ActionChangeFontSize__ctor_mC503BD4E7445C65A4BD44F5B7DC74380ABCD6134 (void);
// 0x00000589 System.Boolean GameCreator.Core.ActionChangeText::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionChangeText_InstantExecute_m8B15CCE23A4E6B94B502CC8493BC6BAE4AE06634 (void);
// 0x0000058A System.Void GameCreator.Core.ActionChangeText::.ctor()
extern void ActionChangeText__ctor_m3C82336B5A63C53209FDAED7FA15CBD8895115DF (void);
// 0x0000058B System.Boolean GameCreator.Core.ActionFocusOnElement::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionFocusOnElement_InstantExecute_mC4C7FE790D5BD2E97A762666377639502C9D2A62 (void);
// 0x0000058C System.Void GameCreator.Core.ActionFocusOnElement::.ctor()
extern void ActionFocusOnElement__ctor_mEF2D1413C5256485BAE70F8E08D525471FB81982 (void);
// 0x0000058D System.Boolean GameCreator.Core.ActionGraphicColor::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionGraphicColor_InstantExecute_mD5BDC42CC18C8BC00A154A7C1A4E070399B48AF8 (void);
// 0x0000058E System.Collections.IEnumerator GameCreator.Core.ActionGraphicColor::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionGraphicColor_Execute_mB50BCF17FFC8C783B2CAC5AA6999EC69282DBC60 (void);
// 0x0000058F System.Void GameCreator.Core.ActionGraphicColor::.ctor()
extern void ActionGraphicColor__ctor_m065DD87AF9CEC29B423B2ECE75565C500FF1FBD8 (void);
// 0x00000590 System.Void GameCreator.Core.ActionGraphicColor/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mA482B568524BECC5B28BA36C17A6B643A97704EF (void);
// 0x00000591 System.Boolean GameCreator.Core.ActionGraphicColor/<>c__DisplayClass4_0::<Execute>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CExecuteU3Eb__0_mF6B1183E51030785F4B0CD2D8B0B596485C51B0F (void);
// 0x00000592 System.Void GameCreator.Core.ActionGraphicColor/<Execute>d__4::.ctor(System.Int32)
extern void U3CExecuteU3Ed__4__ctor_m719692616249D8B1B9A18A4639A2BEBCF839B63E (void);
// 0x00000593 System.Void GameCreator.Core.ActionGraphicColor/<Execute>d__4::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__4_System_IDisposable_Dispose_m322211ED0B980FF930875657D598C03697632275 (void);
// 0x00000594 System.Boolean GameCreator.Core.ActionGraphicColor/<Execute>d__4::MoveNext()
extern void U3CExecuteU3Ed__4_MoveNext_m4601CAE8C93BF900AFEB74BB9FB1E8E315364D85 (void);
// 0x00000595 System.Object GameCreator.Core.ActionGraphicColor/<Execute>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F499A2C7AEE9F7CAFC35900478C44882351C41B (void);
// 0x00000596 System.Void GameCreator.Core.ActionGraphicColor/<Execute>d__4::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mC64DC9F5F707FDAA833235FBCA5AB402F4B49918 (void);
// 0x00000597 System.Object GameCreator.Core.ActionGraphicColor/<Execute>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mAC358A6CCA560BCC9EFA484A7E9CE5F4D47CB979 (void);
// 0x00000598 System.Boolean GameCreator.Core.ActionImageSprite::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionImageSprite_InstantExecute_m3403E950E5C7317CE95A79496EA9E7B44B57F1E5 (void);
// 0x00000599 System.Void GameCreator.Core.ActionImageSprite::.ctor()
extern void ActionImageSprite__ctor_m807DB0C862D7CA8308D9F0BBE3489D616E66910C (void);
// 0x0000059A System.Void GameCreator.Core.AudioBuffer::.ctor(UnityEngine.AudioSource,System.Int32)
extern void AudioBuffer__ctor_mFE5C7A4B3EDC91C7A4D8619C6981819951771553 (void);
// 0x0000059B System.Void GameCreator.Core.AudioBuffer::Update()
extern void AudioBuffer_Update_mFE68D8A5F570AA8BC4CA8AE15EA603646DAF57B3 (void);
// 0x0000059C System.Void GameCreator.Core.AudioBuffer::Play(UnityEngine.AudioClip,System.Single,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void AudioBuffer_Play_mA872195793B3D3E09FA2F4344120EBD33D7E6939 (void);
// 0x0000059D System.Void GameCreator.Core.AudioBuffer::Stop(System.Single)
extern void AudioBuffer_Stop_m861C15494929753C966FCD4BC31556ACBA21840B (void);
// 0x0000059E UnityEngine.AudioClip GameCreator.Core.AudioBuffer::GetAudioClip()
extern void AudioBuffer_GetAudioClip_m40490AD5C5840397B82E01C4A97C90635754FACF (void);
// 0x0000059F System.Void GameCreator.Core.AudioBuffer::SetPosition(UnityEngine.Vector3)
extern void AudioBuffer_SetPosition_mA63FDF7BBD0C5C3EE077CE8DACE5A107E6A69248 (void);
// 0x000005A0 System.Void GameCreator.Core.AudioBuffer::SetPitch(System.Single)
extern void AudioBuffer_SetPitch_mE7CD2426B005A30893E62F70DB017AF91C9EF79E (void);
// 0x000005A1 System.Void GameCreator.Core.AudioBuffer::SetSpatialBlend(System.Single)
extern void AudioBuffer_SetSpatialBlend_m7AF01DA4A9275D5CF14AECE20698FC22C58F5110 (void);
// 0x000005A2 System.Void GameCreator.Core.AudioManager::OnCreate()
extern void AudioManager_OnCreate_mBC2F5EC1831F2F6A79F6B9E47BF9EC989D1E2A2C (void);
// 0x000005A3 GameCreator.Core.AudioBuffer GameCreator.Core.AudioManager::CreateMusicSource(System.Int32)
extern void AudioManager_CreateMusicSource_m1BD781E8B6833F8C2E55DE83FD69980C7F4C73D8 (void);
// 0x000005A4 GameCreator.Core.AudioBuffer GameCreator.Core.AudioManager::CreateSoundSource(System.Int32,System.String)
extern void AudioManager_CreateSoundSource_m994C718E7383142ECB86C3F1DE18BD2B7A793B32 (void);
// 0x000005A5 GameCreator.Core.AudioBuffer GameCreator.Core.AudioManager::CreateVoiceSource(System.Int32)
extern void AudioManager_CreateVoiceSource_m0C1A596D0F8EFBB1EF012EFBFE4418134FD10B2A (void);
// 0x000005A6 UnityEngine.AudioSource GameCreator.Core.AudioManager::CreateAudioAsset(System.String,System.Int32,System.Boolean)
extern void AudioManager_CreateAudioAsset_mD6A70A2854A97D47630FBFCB5307EBAA8B9828D9 (void);
// 0x000005A7 System.Void GameCreator.Core.AudioManager::Update()
extern void AudioManager_Update_m777F5CE15410D58878CE7D462E60CABADB43D488 (void);
// 0x000005A8 System.Void GameCreator.Core.AudioManager::PlayMusic(UnityEngine.AudioClip,System.Single,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void AudioManager_PlayMusic_m5BB30A7648E5055B6F6DED7D691CFFC21D60C769 (void);
// 0x000005A9 System.Void GameCreator.Core.AudioManager::StopMusic(UnityEngine.AudioClip,System.Single)
extern void AudioManager_StopMusic_m96E45AE6A9A199D98DB47B6A6022BB4BFB7D95B5 (void);
// 0x000005AA System.Void GameCreator.Core.AudioManager::StopAllMusic(System.Single)
extern void AudioManager_StopAllMusic_m77ABB7542AD63C8FBBC8D258EFD00ECD7DA975CC (void);
// 0x000005AB System.Void GameCreator.Core.AudioManager::PlaySound2D(UnityEngine.AudioClip,System.Single,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void AudioManager_PlaySound2D_mCC4F71BD7E1843AC7374A8CDF9B30F4F5EDAD7A6 (void);
// 0x000005AC System.Void GameCreator.Core.AudioManager::StopSound2D(UnityEngine.AudioClip,System.Single)
extern void AudioManager_StopSound2D_mC2861318A7B4C8F8FC55E896520682CBD675CF8F (void);
// 0x000005AD System.Void GameCreator.Core.AudioManager::PlaySound3D(UnityEngine.AudioClip,System.Single,UnityEngine.Vector3,System.Single,System.Single,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void AudioManager_PlaySound3D_mD816B797E25FFEDC78DFA775185164AA70435E3B (void);
// 0x000005AE System.Void GameCreator.Core.AudioManager::StopSound3D(UnityEngine.AudioClip,System.Single)
extern void AudioManager_StopSound3D_mE0AF6C87871BFC2CEA92F54431DF1A9BF742E3D3 (void);
// 0x000005AF System.Void GameCreator.Core.AudioManager::StopSound(UnityEngine.AudioClip,System.Single)
extern void AudioManager_StopSound_mFC4EAA7E9693C15ED4F8E4604D83031E1226655C (void);
// 0x000005B0 System.Void GameCreator.Core.AudioManager::StopAllSounds(System.Single)
extern void AudioManager_StopAllSounds_m651E1A529F4EF8D77A49BE960016D3A277B4F8FC (void);
// 0x000005B1 System.Void GameCreator.Core.AudioManager::PlayVoice(UnityEngine.AudioClip,System.Single,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void AudioManager_PlayVoice_m589094F8CFDEB8C0AA1308E48DEF40C3FA1CC195 (void);
// 0x000005B2 System.Void GameCreator.Core.AudioManager::StopVoice(UnityEngine.AudioClip,System.Single)
extern void AudioManager_StopVoice_m0B6EA9A1D011EC6A59217E5922646E6877C08D6C (void);
// 0x000005B3 System.Void GameCreator.Core.AudioManager::StopAllVoices(System.Single)
extern void AudioManager_StopAllVoices_mA5BEAF8A8E2A7E1DBE1C54E71ADB09870AD4025E (void);
// 0x000005B4 System.Void GameCreator.Core.AudioManager::StopAllAudios(System.Single)
extern void AudioManager_StopAllAudios_mDE6935DE28E855F55E6067D3899ABA930013F7D5 (void);
// 0x000005B5 System.Void GameCreator.Core.AudioManager::SetGlobalVolume(System.Int32,System.Single)
extern void AudioManager_SetGlobalVolume_mC78A4C92E8DFCA6B94D9CC2BC57DCABEFFB3D69E (void);
// 0x000005B6 System.Void GameCreator.Core.AudioManager::SetGlobalMastrVolume(System.Single)
extern void AudioManager_SetGlobalMastrVolume_mB9BD6C11628BC171FFD857025448BDAB4F76AFE8 (void);
// 0x000005B7 System.Void GameCreator.Core.AudioManager::SetGlobalMusicVolume(System.Single)
extern void AudioManager_SetGlobalMusicVolume_m2E57E4936EDD01C7B9C441E994189096D56E8555 (void);
// 0x000005B8 System.Void GameCreator.Core.AudioManager::SetGlobalSoundVolume(System.Single)
extern void AudioManager_SetGlobalSoundVolume_mE88A627BC73D89119A4DE5A5ECE2EB1881334EAC (void);
// 0x000005B9 System.Void GameCreator.Core.AudioManager::SetGlobalVoiceVolume(System.Single)
extern void AudioManager_SetGlobalVoiceVolume_m327FF33E0BD5464E36133743AB811559922AAEAB (void);
// 0x000005BA System.Single GameCreator.Core.AudioManager::GetGlobalVolume(System.Int32)
extern void AudioManager_GetGlobalVolume_m224B9705EA865E428FFBFF34B853086644DD2008 (void);
// 0x000005BB System.Single GameCreator.Core.AudioManager::GetGlobalMastrVolume()
extern void AudioManager_GetGlobalMastrVolume_m7C5D77F1BFA82E0D0323F0D19D12836D8C6319C1 (void);
// 0x000005BC System.Single GameCreator.Core.AudioManager::GetGlobalMusicVolume()
extern void AudioManager_GetGlobalMusicVolume_mC9BA5499B0F8230A7E5FCC3634A4C0ECA7FAAA15 (void);
// 0x000005BD System.Single GameCreator.Core.AudioManager::GetGlobalSoundVolume()
extern void AudioManager_GetGlobalSoundVolume_m9A358B18F9F314B8265BF166B1E364AE4CC40147 (void);
// 0x000005BE System.Single GameCreator.Core.AudioManager::GetGlobalVoiceVolume()
extern void AudioManager_GetGlobalVoiceVolume_m9516BC8AA7E6893845631AAFAAD5B29D62AAFAA5 (void);
// 0x000005BF System.String GameCreator.Core.AudioManager::GetUniqueName()
extern void AudioManager_GetUniqueName_mCB536A5A75B03F788291C4E0E8ABEF845195EA62 (void);
// 0x000005C0 System.Type GameCreator.Core.AudioManager::GetSaveDataType()
extern void AudioManager_GetSaveDataType_mC0151C70ECC42821D722458F97EE6547E486ADF0 (void);
// 0x000005C1 System.Object GameCreator.Core.AudioManager::GetSaveData()
extern void AudioManager_GetSaveData_m728DD74D5F501F2BA61955C65B2287ED7F164191 (void);
// 0x000005C2 System.Void GameCreator.Core.AudioManager::ResetData()
extern void AudioManager_ResetData_m74A07C301C442C8FF0ADE5519F2D64AECE60DB2A (void);
// 0x000005C3 System.Void GameCreator.Core.AudioManager::OnLoad(System.Object)
extern void AudioManager_OnLoad_m3F8D2AEF6D665D9192C0A362EDF1A5C825B19EAB (void);
// 0x000005C4 System.Void GameCreator.Core.AudioManager::.ctor()
extern void AudioManager__ctor_mE0302690294F0D3A91645B6BA76C9BA2D070FB03 (void);
// 0x000005C5 System.Void GameCreator.Core.AudioManager::.cctor()
extern void AudioManager__cctor_m2461AEB95DDA7001E05525AB4E4494E7469031D5 (void);
// 0x000005C6 System.Void GameCreator.Core.AudioManager/Volume::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Volume__ctor_mBE3C51739CEC1A217A985EC47CC69B77822EE43D (void);
// 0x000005C7 System.Boolean GameCreator.Core.ConditionPattern::Check()
extern void ConditionPattern_Check_m608C578D0AA69C72465418CB40AE1AF1DECB4678 (void);
// 0x000005C8 System.Void GameCreator.Core.ConditionPattern::.ctor()
extern void ConditionPattern__ctor_m9D290D22F50580F340CCA9FB802A134376A5C0C6 (void);
// 0x000005C9 System.Boolean GameCreator.Core.ConditionSceneLoaded::Check(UnityEngine.GameObject)
extern void ConditionSceneLoaded_Check_m10F81907783356BCEBFD797AA44BB4B8810C1C2D (void);
// 0x000005CA System.Void GameCreator.Core.ConditionSceneLoaded::.ctor()
extern void ConditionSceneLoaded__ctor_mCCD95428508DA4F3F9AB6545022C8268EBBAF433 (void);
// 0x000005CB System.Boolean GameCreator.Core.ConditionSimple::Check()
extern void ConditionSimple_Check_mCB15C2C01D3DC368ABFEB935689C08EA76F2B59C (void);
// 0x000005CC System.Void GameCreator.Core.ConditionSimple::.ctor()
extern void ConditionSimple__ctor_mB488E2AEC8F0873FB0CFC5D8BE6D6526E48B5502 (void);
// 0x000005CD System.Boolean GameCreator.Core.ConditionInputKey::Check()
extern void ConditionInputKey_Check_m44DD27E5E9DCADE96E47B5E6A87F97C1EF25BE7B (void);
// 0x000005CE System.Void GameCreator.Core.ConditionInputKey::.ctor()
extern void ConditionInputKey__ctor_m7F9CD2FE8D4EE0204C17B1AFAB61A0DFAA36441D (void);
// 0x000005CF System.Boolean GameCreator.Core.ConditionInputMouse::Check()
extern void ConditionInputMouse_Check_mAA2DCC245FEFA7DD42941F615BF7BC6D146A4C5C (void);
// 0x000005D0 System.Void GameCreator.Core.ConditionInputMouse::.ctor()
extern void ConditionInputMouse__ctor_m8CF986A1321A4EAAF9674195FB7A4FB53BC59237 (void);
// 0x000005D1 System.Boolean GameCreator.Core.ConditionActiveObject::Check(UnityEngine.GameObject)
extern void ConditionActiveObject_Check_m9FBF9A1E7586E35EE864FF7B848EADEA89BE7A90 (void);
// 0x000005D2 System.Void GameCreator.Core.ConditionActiveObject::.ctor()
extern void ConditionActiveObject__ctor_m1EEE9CC012389EBA46904865FF91C5D448EDA223 (void);
// 0x000005D3 System.Boolean GameCreator.Core.ConditionEnabledComponent::Check(UnityEngine.GameObject)
extern void ConditionEnabledComponent_Check_m719B977A081EE6A74DC1EAE1B40E01F468576321 (void);
// 0x000005D4 System.Void GameCreator.Core.ConditionEnabledComponent::.ctor()
extern void ConditionEnabledComponent__ctor_mE8031DF3002CF100C8AB83D795236F3231D80FE5 (void);
// 0x000005D5 System.Boolean GameCreator.Core.ConditionExistsObject::Check(UnityEngine.GameObject)
extern void ConditionExistsObject_Check_m45EA262A531B1121C58E82C00A8F194A2035A307 (void);
// 0x000005D6 System.Void GameCreator.Core.ConditionExistsObject::.ctor()
extern void ConditionExistsObject__ctor_mF04F641775641728E0753FDEEFC2FD77BDB8D1A9 (void);
// 0x000005D7 System.Boolean GameCreator.Core.ConditionRigidbody::Check(UnityEngine.GameObject)
extern void ConditionRigidbody_Check_mC2DB9AD58677271135A45D814EDD9C24C62479E9 (void);
// 0x000005D8 System.Void GameCreator.Core.ConditionRigidbody::.ctor()
extern void ConditionRigidbody__ctor_m6279AB268A044952DADAFD20CA62BBC2A8BE63F7 (void);
// 0x000005D9 System.Boolean GameCreator.Core.ConditionTag::Check(UnityEngine.GameObject)
extern void ConditionTag_Check_mDA0C9D7648F48598BECF87A2C17D19DD5A082096 (void);
// 0x000005DA System.Void GameCreator.Core.ConditionTag::.ctor()
extern void ConditionTag__ctor_mBED95CA09F7538AF738F55C4081ADD588C33A596 (void);
// 0x000005DB System.Boolean GameCreator.Core.ConditionHasSaveGame::Check(UnityEngine.GameObject)
extern void ConditionHasSaveGame_Check_m3E70ACDA594C99EFD110AA9F1315E9437AE11C6F (void);
// 0x000005DC System.Void GameCreator.Core.ConditionHasSaveGame::.ctor()
extern void ConditionHasSaveGame__ctor_mF8CFDBAC667C2728F41E5AF90115550902CEFF70 (void);
// 0x000005DD System.Void GameCreator.Core.HPCursor::Initialize()
extern void HPCursor_Initialize_m9FF4BF6D3EA65EFE5E375B3B1610DDBBCCD922D8 (void);
// 0x000005DE System.Void GameCreator.Core.HPCursor::HotspotMouseEnter()
extern void HPCursor_HotspotMouseEnter_m09ACDA80AECD5E629712F39C4129BBD37006DA15 (void);
// 0x000005DF System.Void GameCreator.Core.HPCursor::HotspotMouseExit()
extern void HPCursor_HotspotMouseExit_m19AF919867C2716BEBB5A0314978D92137CBD584 (void);
// 0x000005E0 System.Void GameCreator.Core.HPCursor::HotspotMouseOver()
extern void HPCursor_HotspotMouseOver_m32888765C5259D1FA8B96823E20F5B2498B1293E (void);
// 0x000005E1 System.Void GameCreator.Core.HPCursor::.ctor()
extern void HPCursor__ctor_m84B4122116F89FEA83ED3E9482DFB0B23E3BA0BE (void);
// 0x000005E2 System.Void GameCreator.Core.HPCursor/Data::.ctor()
extern void Data__ctor_m1C4204CA27CAEF61BD7F08F3146DAFCE0E269B1A (void);
// 0x000005E3 System.Void GameCreator.Core.HPHeadTrack::Initialize()
extern void HPHeadTrack_Initialize_m37D877268CB7E93DF03CFFEEC3D1D2FCE82E766C (void);
// 0x000005E4 System.Void GameCreator.Core.HPHeadTrack::OnTriggerEnter(UnityEngine.Collider)
extern void HPHeadTrack_OnTriggerEnter_m9B467E5B2D42B03EB7960D603297313C403677E5 (void);
// 0x000005E5 System.Void GameCreator.Core.HPHeadTrack::OnTriggerExit(UnityEngine.Collider)
extern void HPHeadTrack_OnTriggerExit_m112099B8DF57D10460D435D2FEE642A208056A62 (void);
// 0x000005E6 GameCreator.Characters.CharacterHeadTrack GameCreator.Core.HPHeadTrack::HotspotIndicatorIsTarget(UnityEngine.GameObject,System.Int32)
extern void HPHeadTrack_HotspotIndicatorIsTarget_mA97C4FB1EF719A8667473132F8285D1118FE269D (void);
// 0x000005E7 System.Void GameCreator.Core.HPHeadTrack::.ctor()
extern void HPHeadTrack__ctor_m2A78894EDA961758ABF6D91A0DF78EEE4074B30C (void);
// 0x000005E8 System.Void GameCreator.Core.HPHeadTrack/Data::.ctor()
extern void Data__ctor_mFF2BAFC8635B8B3B4861D730ECB428E960AE9E5B (void);
// 0x000005E9 System.Void GameCreator.Core.HPProximity::Initialize()
extern void HPProximity_Initialize_m12F05889065A1F6919F2124ED18D62B03FC12603 (void);
// 0x000005EA System.Void GameCreator.Core.HPProximity::OnTriggerEnter(UnityEngine.Collider)
extern void HPProximity_OnTriggerEnter_m4133EBA18EB99206C25ECD7281A5F3095BAB8737 (void);
// 0x000005EB System.Void GameCreator.Core.HPProximity::OnTriggerExit(UnityEngine.Collider)
extern void HPProximity_OnTriggerExit_m5DB20065610006E266F5AF36A0792B4B4048E6DB (void);
// 0x000005EC System.Boolean GameCreator.Core.HPProximity::HotspotIndicatorIsTarget(UnityEngine.Collider)
extern void HPProximity_HotspotIndicatorIsTarget_m81B99169E9512362548B320EB6DAF1FF2A5803E7 (void);
// 0x000005ED System.Void GameCreator.Core.HPProximity::.ctor()
extern void HPProximity__ctor_m9F5BC9754BFE47F72E4FDBAA45FD12091D028891 (void);
// 0x000005EE System.Void GameCreator.Core.HPProximity/Data::ChangeState(UnityEngine.Transform,System.Boolean)
extern void Data_ChangeState_m8A028B6848BEB767C963BEBE35368F1DDE92B6A5 (void);
// 0x000005EF System.Void GameCreator.Core.HPProximity/Data::.ctor()
extern void Data__ctor_m2849F741D7670F6890717C41088E2320B2DF7499 (void);
// 0x000005F0 TData GameCreator.Core.IHPMonoBehaviour`1::Create(GameCreator.Core.Hotspot,TData)
// 0x000005F1 System.Void GameCreator.Core.IHPMonoBehaviour`1::ConfigureData(GameCreator.Core.Hotspot,UnityEngine.GameObject)
// 0x000005F2 System.Void GameCreator.Core.IHPMonoBehaviour`1::Initialize()
// 0x000005F3 System.Void GameCreator.Core.IHPMonoBehaviour`1::HotspotMouseEnter()
// 0x000005F4 System.Void GameCreator.Core.IHPMonoBehaviour`1::HotspotMouseExit()
// 0x000005F5 System.Void GameCreator.Core.IHPMonoBehaviour`1::HotspotMouseOver()
// 0x000005F6 System.Boolean GameCreator.Core.IHPMonoBehaviour`1::IsWithinConstrainedRadius()
// 0x000005F7 System.Void GameCreator.Core.IHPMonoBehaviour`1::.ctor()
// 0x000005F8 System.Void GameCreator.Core.IHPMonoBehaviour`1/IData::Setup(GameCreator.Core.Hotspot,UnityEngine.GameObject)
// 0x000005F9 System.Void GameCreator.Core.IHPMonoBehaviour`1/IData::.ctor()
// 0x000005FA System.Void GameCreator.Core.Igniter::Setup(GameCreator.Core.Trigger)
extern void Igniter_Setup_mC3B1EC1B396B999EEF93672A08A80DF29A3CFCA4 (void);
// 0x000005FB System.Void GameCreator.Core.Igniter::Awake()
extern void Igniter_Awake_mC2E9C9BCA939A44843E47FA55C4ABF34B9797437 (void);
// 0x000005FC System.Void GameCreator.Core.Igniter::OnEnable()
extern void Igniter_OnEnable_mAD4D4787890AFF225025BA0BFB34E315618D866A (void);
// 0x000005FD System.Void GameCreator.Core.Igniter::OnValidate()
extern void Igniter_OnValidate_m60E97AA7AF7FEC63563FACA80C02E3912861C69A (void);
// 0x000005FE System.Void GameCreator.Core.Igniter::ExecuteTrigger()
extern void Igniter_ExecuteTrigger_m0177E0F91EAAEC6E3060E3999D94BC632F6DE15F (void);
// 0x000005FF System.Void GameCreator.Core.Igniter::ExecuteTrigger(UnityEngine.GameObject)
extern void Igniter_ExecuteTrigger_m62CFC9086E2F87B0172D6D91A4D9B5ACF946AEAD (void);
// 0x00000600 System.Void GameCreator.Core.Igniter::ExecuteTrigger(UnityEngine.GameObject,System.Object[])
extern void Igniter_ExecuteTrigger_m5913E13D844E3833A79E888FA2F887EAA1BDB204 (void);
// 0x00000601 System.Void GameCreator.Core.Igniter::OnApplicationQuit()
extern void Igniter_OnApplicationQuit_m93AE6D0FB709202A7EFA4503F5BA9491A2FD3430 (void);
// 0x00000602 System.Boolean GameCreator.Core.Igniter::IsColliderPlayer(UnityEngine.Collider)
extern void Igniter_IsColliderPlayer_m1526B8C714F1C6E51ED804CE60491D5EAB6A659B (void);
// 0x00000603 System.Void GameCreator.Core.Igniter::.ctor()
extern void Igniter__ctor_mD6241C66A7C942BC21390F2F0D1FC482E6D01F8D (void);
// 0x00000604 System.Void GameCreator.Core.Igniter::.cctor()
extern void Igniter__cctor_m5767AAC294453D8ABAFFF6893EF4CDEC6DE87B67 (void);
// 0x00000605 System.Void GameCreator.Core.IgniterCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern void IgniterCollisionEnter_OnCollisionEnter_m43E2DEA3D592E567BDE72C6DA9516E4772408CCB (void);
// 0x00000606 System.Void GameCreator.Core.IgniterCollisionEnter::.ctor()
extern void IgniterCollisionEnter__ctor_m8F33BDC1A02D6712CA30E292D7500C7D17E2B10C (void);
// 0x00000607 System.Void GameCreator.Core.IgniterCollisionEnterTag::OnCollisionEnter(UnityEngine.Collision)
extern void IgniterCollisionEnterTag_OnCollisionEnter_mC7C9F695D5AEB69FBFC49BAAE203DECE0E4460A5 (void);
// 0x00000608 System.Void GameCreator.Core.IgniterCollisionEnterTag::.ctor()
extern void IgniterCollisionEnterTag__ctor_m23D7E2FBC718265D11727972218F255D1CF01722 (void);
// 0x00000609 System.Void GameCreator.Core.IgniterCollisionExit::OnCollisionEnter(UnityEngine.Collision)
extern void IgniterCollisionExit_OnCollisionEnter_m8067EB459E10F194830F8AD4F0FE653C6498236D (void);
// 0x0000060A System.Void GameCreator.Core.IgniterCollisionExit::.ctor()
extern void IgniterCollisionExit__ctor_m57636BF8416B44146CD4C710AA084ED280A2DCBD (void);
// 0x0000060B System.Void GameCreator.Core.IgniterCollisionExitTag::OnCollisionEnter(UnityEngine.Collision)
extern void IgniterCollisionExitTag_OnCollisionEnter_m1FD121A2A9165849CF70E1E290BC206E1596B8A3 (void);
// 0x0000060C System.Void GameCreator.Core.IgniterCollisionExitTag::.ctor()
extern void IgniterCollisionExitTag__ctor_m73C711F1F32DA59B6F4F4DC5777133D83302D414 (void);
// 0x0000060D System.Void GameCreator.Core.IgniterCursorRaycast::Update()
extern void IgniterCursorRaycast_Update_m003B8B037767BAF3CBA3DEB85E9BA5AC712B0614 (void);
// 0x0000060E System.Void GameCreator.Core.IgniterCursorRaycast::.ctor()
extern void IgniterCursorRaycast__ctor_m04313D6F2785D8FD3409F85D824D4ACFCB294A2D (void);
// 0x0000060F System.Void GameCreator.Core.IgniterEventReceive::Start()
extern void IgniterEventReceive_Start_mA1E02B824315BA352AA071C41F978E160605FDCB (void);
// 0x00000610 System.Void GameCreator.Core.IgniterEventReceive::OnDestroy()
extern void IgniterEventReceive_OnDestroy_mF5E427A22B07EEDA77C857374921BF8A3BAE543A (void);
// 0x00000611 System.Void GameCreator.Core.IgniterEventReceive::OnReceiveEvent(UnityEngine.GameObject)
extern void IgniterEventReceive_OnReceiveEvent_m7A99BF2750B8244EC812476BADD07F69C2EB3649 (void);
// 0x00000612 System.Void GameCreator.Core.IgniterEventReceive::.ctor()
extern void IgniterEventReceive__ctor_m87F1CB2680AE98B8ED813B0D6E1FF9D942D7DAFC (void);
// 0x00000613 System.Void GameCreator.Core.IgniterHoverHoldKey::Update()
extern void IgniterHoverHoldKey_Update_m39876EEFAF8217116EEDBD7E9572606E3F358E62 (void);
// 0x00000614 System.Void GameCreator.Core.IgniterHoverHoldKey::OnMouseExit()
extern void IgniterHoverHoldKey_OnMouseExit_mC3F052632122A399E3C79AA168CB20A4E128FA72 (void);
// 0x00000615 System.Void GameCreator.Core.IgniterHoverHoldKey::OnMouseEnter()
extern void IgniterHoverHoldKey_OnMouseEnter_m2AB538C7D877A8636B747F29ABF5D3F9BB3347B0 (void);
// 0x00000616 System.Void GameCreator.Core.IgniterHoverHoldKey::.ctor()
extern void IgniterHoverHoldKey__ctor_mD8E9E6DFF1103F72993AB267FFB17DA2D19B4AC0 (void);
// 0x00000617 System.Void GameCreator.Core.IgniterHoverPressKey::Update()
extern void IgniterHoverPressKey_Update_m935EE3287AACFF0D082EAB9A4D45F6A5F3730B9C (void);
// 0x00000618 System.Void GameCreator.Core.IgniterHoverPressKey::OnMouseExit()
extern void IgniterHoverPressKey_OnMouseExit_mACCC6899985612E032394D9AEFB4B15354D26365 (void);
// 0x00000619 System.Void GameCreator.Core.IgniterHoverPressKey::OnMouseEnter()
extern void IgniterHoverPressKey_OnMouseEnter_m5286D46ACE681FCB3B9D90FA0A96EBD5652B4571 (void);
// 0x0000061A System.Void GameCreator.Core.IgniterHoverPressKey::.ctor()
extern void IgniterHoverPressKey__ctor_mCF524860198F2B11BC22F4CDEC3DEDEEF2349692 (void);
// 0x0000061B System.Void GameCreator.Core.IgniterInvisible::OnBecameInvisible()
extern void IgniterInvisible_OnBecameInvisible_m0CE4AEFFBAB022D5330A20C8DF93791C2D5A8CF0 (void);
// 0x0000061C System.Void GameCreator.Core.IgniterInvisible::.ctor()
extern void IgniterInvisible__ctor_mB98ADA26B7CEF8CDC66A9F04A05B156973EA288A (void);
// 0x0000061D System.Void GameCreator.Core.IgniterInvoke::ExecuteTrigger(UnityEngine.GameObject)
extern void IgniterInvoke_ExecuteTrigger_m0C367F3EFBC14B922F84A5F846447D744B72AAD6 (void);
// 0x0000061E System.Void GameCreator.Core.IgniterInvoke::.ctor()
extern void IgniterInvoke__ctor_mC8ABAA17396482ED209A6205B6E2D0DB9FD9963D (void);
// 0x0000061F System.Void GameCreator.Core.IgniterKeyDown::Update()
extern void IgniterKeyDown_Update_m4E498DA0893C0D537ED8382D3AC3B8D26BCD8DCE (void);
// 0x00000620 System.Void GameCreator.Core.IgniterKeyDown::.ctor()
extern void IgniterKeyDown__ctor_mD89B8FA6D98461C14028E90262A347DEC65D5C34 (void);
// 0x00000621 System.Void GameCreator.Core.IgniterKeyHold::Update()
extern void IgniterKeyHold_Update_mF39731922B01B0895459875D6C5241E75CCC908F (void);
// 0x00000622 System.Void GameCreator.Core.IgniterKeyHold::.ctor()
extern void IgniterKeyHold__ctor_m87444F60F8A5D7FC04ACB1FC5606C7BFE2CBE8D0 (void);
// 0x00000623 System.Void GameCreator.Core.IgniterKeyUp::Update()
extern void IgniterKeyUp_Update_m84BAEFF3373AD4F82477289840DF2A3B8F77B5AA (void);
// 0x00000624 System.Void GameCreator.Core.IgniterKeyUp::.ctor()
extern void IgniterKeyUp__ctor_mF0B4C3B7BF4F78AFCD0AE116D4E3D3FF278985DB (void);
// 0x00000625 System.Void GameCreator.Core.IgniterMouseDown::Update()
extern void IgniterMouseDown_Update_mC23FCF294F412106159CE7F15C48FF1C13DB87A6 (void);
// 0x00000626 System.Void GameCreator.Core.IgniterMouseDown::.ctor()
extern void IgniterMouseDown__ctor_m9745AF43A914820D9464BF6F0AA10D9620D95555 (void);
// 0x00000627 System.Void GameCreator.Core.IgniterMouseEnter::OnMouseEnter()
extern void IgniterMouseEnter_OnMouseEnter_m2D19BF8512E83A5BDA215215E8F98485E5DA7815 (void);
// 0x00000628 System.Void GameCreator.Core.IgniterMouseEnter::.ctor()
extern void IgniterMouseEnter__ctor_mA68F31EDE461A216747D5622792954442B9786ED (void);
// 0x00000629 System.Void GameCreator.Core.IgniterMouseExit::OnMouseExit()
extern void IgniterMouseExit_OnMouseExit_m9170A8A34C8145B0E69CF8EDE1E7EDB6FF6780CD (void);
// 0x0000062A System.Void GameCreator.Core.IgniterMouseExit::.ctor()
extern void IgniterMouseExit__ctor_m75974BC20C0B436EAF929DC494D2135B8CE81618 (void);
// 0x0000062B System.Void GameCreator.Core.IgniterMouseHoldLeft::Start()
extern void IgniterMouseHoldLeft_Start_m23D379FE1616B517668B56A1C6E28C295FD58236 (void);
// 0x0000062C System.Void GameCreator.Core.IgniterMouseHoldLeft::Update()
extern void IgniterMouseHoldLeft_Update_m35EA351618374DCA5E45C540AE733C38C5676741 (void);
// 0x0000062D System.Void GameCreator.Core.IgniterMouseHoldLeft::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldLeft_OnPointerDown_m332CD1FAE25A8FDF6C0D4EE069B9BB8D812476B8 (void);
// 0x0000062E System.Void GameCreator.Core.IgniterMouseHoldLeft::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldLeft_OnPointerExit_mEBC45FDD1E3B0D86EB08C8B2C003A76E3CD849C4 (void);
// 0x0000062F System.Void GameCreator.Core.IgniterMouseHoldLeft::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldLeft_OnPointerUp_mFBF9A4B1554E09F209034041935CBCC3434E5311 (void);
// 0x00000630 System.Void GameCreator.Core.IgniterMouseHoldLeft::.ctor()
extern void IgniterMouseHoldLeft__ctor_m4EC0211F6D688B57E064DA57A27E59C6BA8EBC69 (void);
// 0x00000631 System.Void GameCreator.Core.IgniterMouseHoldMiddle::Start()
extern void IgniterMouseHoldMiddle_Start_mE64034455FD1093230CC49E9BCFF50EFAF6572A4 (void);
// 0x00000632 System.Void GameCreator.Core.IgniterMouseHoldMiddle::Update()
extern void IgniterMouseHoldMiddle_Update_mCE9E4EDB3343E1BFF11FA80FBA8ACD28A955DC36 (void);
// 0x00000633 System.Void GameCreator.Core.IgniterMouseHoldMiddle::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldMiddle_OnPointerDown_m81991E0B2DC5A0C9DAA2BEB7ED72665DBB911EC9 (void);
// 0x00000634 System.Void GameCreator.Core.IgniterMouseHoldMiddle::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldMiddle_OnPointerExit_m2B9CA16EBE33549C33D5F0430CD82D27592484FF (void);
// 0x00000635 System.Void GameCreator.Core.IgniterMouseHoldMiddle::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldMiddle_OnPointerUp_m396FEDD4A20955CA646B8B20B3D57CD83EACFC8F (void);
// 0x00000636 System.Void GameCreator.Core.IgniterMouseHoldMiddle::.ctor()
extern void IgniterMouseHoldMiddle__ctor_m37ECBC942E72B154B5AE3C8305B790E886A4C83E (void);
// 0x00000637 System.Void GameCreator.Core.IgniterMouseHoldRight::Start()
extern void IgniterMouseHoldRight_Start_m9BB86E33D54A0BD37677A7093800E609A6083AD2 (void);
// 0x00000638 System.Void GameCreator.Core.IgniterMouseHoldRight::Update()
extern void IgniterMouseHoldRight_Update_m79B60F54F95127AD0DBE319792CB590DF3FCDE94 (void);
// 0x00000639 System.Void GameCreator.Core.IgniterMouseHoldRight::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldRight_OnPointerDown_mAA0574BCC3AD0896A27904E1D33C38AB30036862 (void);
// 0x0000063A System.Void GameCreator.Core.IgniterMouseHoldRight::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldRight_OnPointerExit_m833BE2730D52B3D962DA956FC5E293B2EE08F53A (void);
// 0x0000063B System.Void GameCreator.Core.IgniterMouseHoldRight::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseHoldRight_OnPointerUp_mA18C72C53DE4978C910DE8F8A96136139504A134 (void);
// 0x0000063C System.Void GameCreator.Core.IgniterMouseHoldRight::.ctor()
extern void IgniterMouseHoldRight__ctor_m6934C27E9E7AF35CA030403CBB885981B41CB63E (void);
// 0x0000063D System.Void GameCreator.Core.IgniterMouseLeftClick::Start()
extern void IgniterMouseLeftClick_Start_mEDDCBE52A5D1FD4042C37F26FE905000128025B7 (void);
// 0x0000063E System.Void GameCreator.Core.IgniterMouseLeftClick::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseLeftClick_OnPointerClick_mA08FE7BFA176D81FA8742FBB3217CF3EFEB9A783 (void);
// 0x0000063F System.Void GameCreator.Core.IgniterMouseLeftClick::.ctor()
extern void IgniterMouseLeftClick__ctor_mAA817A372E584AC3CA838A7C4AA55D1682EE15B4 (void);
// 0x00000640 System.Void GameCreator.Core.IgniterMouseMiddleClick::Start()
extern void IgniterMouseMiddleClick_Start_mCED056ADCCC0928DD2EDFEADBAA2FEE69A9A7BA4 (void);
// 0x00000641 System.Void GameCreator.Core.IgniterMouseMiddleClick::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseMiddleClick_OnPointerClick_mA493E6C5A1485C41CC4C9795E3D40A2450AAC6CE (void);
// 0x00000642 System.Void GameCreator.Core.IgniterMouseMiddleClick::.ctor()
extern void IgniterMouseMiddleClick__ctor_mD74CF90EA0CCE5D6B3D1165677DB034E15018009 (void);
// 0x00000643 System.Void GameCreator.Core.IgniterMouseRightClick::Start()
extern void IgniterMouseRightClick_Start_m2ABF3380E45BB361A52576447B327E1D5CF08334 (void);
// 0x00000644 System.Void GameCreator.Core.IgniterMouseRightClick::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void IgniterMouseRightClick_OnPointerClick_m8B2E71BA03E806957BB0EBA36DB404F91F1E5111 (void);
// 0x00000645 System.Void GameCreator.Core.IgniterMouseRightClick::.ctor()
extern void IgniterMouseRightClick__ctor_m4FD08197319D91CAFE8B2F5C7FC12D75BAC889F1 (void);
// 0x00000646 System.Void GameCreator.Core.IgniterMouseUp::Update()
extern void IgniterMouseUp_Update_mA46325FFEE9AAF48A635F870BB589D92A05EE334 (void);
// 0x00000647 System.Void GameCreator.Core.IgniterMouseUp::.ctor()
extern void IgniterMouseUp__ctor_m850F31E4C844F0815CB998149255F66215C342F1 (void);
// 0x00000648 System.Void GameCreator.Core.IgniterOnDisable::OnDisable()
extern void IgniterOnDisable_OnDisable_mE617871EF339BBDED4510FE2519F2C78DC697603 (void);
// 0x00000649 System.Void GameCreator.Core.IgniterOnDisable::.ctor()
extern void IgniterOnDisable__ctor_mCD7F7B6D616B5363B45448FF175E970A34B63C6B (void);
// 0x0000064A System.Void GameCreator.Core.IgniterOnEnable::OnEnable()
extern void IgniterOnEnable_OnEnable_mE65BF907BAA48B0753D072AABFF46090D4AC02CD (void);
// 0x0000064B System.Void GameCreator.Core.IgniterOnEnable::.ctor()
extern void IgniterOnEnable__ctor_m7A66AC2D172BDEDABA28DC5C58583343EE23BAE0 (void);
// 0x0000064C System.Void GameCreator.Core.IgniterOnJump::Start()
extern void IgniterOnJump_Start_m7077150F799EEA0019DFC2339B903709AF5F70E8 (void);
// 0x0000064D System.Void GameCreator.Core.IgniterOnJump::OnJump(System.Int32)
extern void IgniterOnJump_OnJump_m558F3E2905B07A19B5E3ACE9FD6ADF29FE3DB0A6 (void);
// 0x0000064E System.Void GameCreator.Core.IgniterOnJump::.ctor()
extern void IgniterOnJump__ctor_m061B801777E2819B4D4DD7E29CDFD07A58E54DFA (void);
// 0x0000064F System.Void GameCreator.Core.IgniterOnLand::Start()
extern void IgniterOnLand_Start_mB732C362CD623CB6A75C957D9D7FF67A9EB4B585 (void);
// 0x00000650 System.Void GameCreator.Core.IgniterOnLand::OnLand(System.Single)
extern void IgniterOnLand_OnLand_m4B198E34515C910DF9E4084015C84CC1F46BDBCE (void);
// 0x00000651 System.Void GameCreator.Core.IgniterOnLand::.ctor()
extern void IgniterOnLand__ctor_m8FD0C03F3AAF748BA3576C55B86DFDB061C8E1BB (void);
// 0x00000652 System.Void GameCreator.Core.IgniterOnLoad::Start()
extern void IgniterOnLoad_Start_mDFB87CD6403683BEE1E65486DEEB0694C550BC43 (void);
// 0x00000653 System.Void GameCreator.Core.IgniterOnLoad::OnDestroy()
extern void IgniterOnLoad_OnDestroy_m2926D6B344F3D37EA6085DBA1E6808C08FA119ED (void);
// 0x00000654 System.Void GameCreator.Core.IgniterOnLoad::OnLoad(System.Int32)
extern void IgniterOnLoad_OnLoad_mC8B735E12A61D43982B3610DFC63C2F01DC7D0BF (void);
// 0x00000655 System.Collections.IEnumerator GameCreator.Core.IgniterOnLoad::DeferredOnLoad()
extern void IgniterOnLoad_DeferredOnLoad_m64C11C5F16BD9E5EAEC2D50D48E978F52A238DC6 (void);
// 0x00000656 System.Void GameCreator.Core.IgniterOnLoad::.ctor()
extern void IgniterOnLoad__ctor_m8977EB10549A3466E83BA551CDE3DA48A28664C5 (void);
// 0x00000657 System.Void GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::.ctor(System.Int32)
extern void U3CDeferredOnLoadU3Ed__3__ctor_m5C8E1E3AA4B98F20105756C6130E53147A68BD9F (void);
// 0x00000658 System.Void GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::System.IDisposable.Dispose()
extern void U3CDeferredOnLoadU3Ed__3_System_IDisposable_Dispose_mB475A3D3A9F97EDC5FD87B397BC25ECBDE3760FF (void);
// 0x00000659 System.Boolean GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::MoveNext()
extern void U3CDeferredOnLoadU3Ed__3_MoveNext_m49D64844F953FA9FBD9AF1594B86C0941A02031D (void);
// 0x0000065A System.Object GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeferredOnLoadU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BF406ACFB64F6A8D3582DD5C4686BD52C2EC93E (void);
// 0x0000065B System.Void GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_Reset_m5E740051AF8A54914754A26FF03008A1B83E4C1E (void);
// 0x0000065C System.Object GameCreator.Core.IgniterOnLoad/<DeferredOnLoad>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_get_Current_mD8F7F0E8B01AF6EC3C85ACB8B40A7FF6242DEF73 (void);
// 0x0000065D System.Void GameCreator.Core.IgniterOnMouseEnterUI::Start()
extern void IgniterOnMouseEnterUI_Start_m534063FF048182CF900D973595F7CC11364BC851 (void);
// 0x0000065E System.Void GameCreator.Core.IgniterOnMouseEnterUI::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void IgniterOnMouseEnterUI_OnPointerEnter_m98259B679FB72031FD2F190BE58C6D5A8054FF81 (void);
// 0x0000065F System.Void GameCreator.Core.IgniterOnMouseEnterUI::.ctor()
extern void IgniterOnMouseEnterUI__ctor_mDA8CED0CA8614E50FEE8B6238D9DDEEEF4B96A19 (void);
// 0x00000660 System.Void GameCreator.Core.IgniterOnMouseExitUI::Start()
extern void IgniterOnMouseExitUI_Start_mE0ADF7F2B822F2A7BA5F5A1ECDE6C0829F9F5644 (void);
// 0x00000661 System.Void GameCreator.Core.IgniterOnMouseExitUI::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void IgniterOnMouseExitUI_OnPointerExit_m24A3F0C88D598A7FE25F75080B0398816A6BD802 (void);
// 0x00000662 System.Void GameCreator.Core.IgniterOnMouseExitUI::.ctor()
extern void IgniterOnMouseExitUI__ctor_m607D2FB41D62C2363FBE2D4661C82946E61A141C (void);
// 0x00000663 System.Void GameCreator.Core.IgniterOnSave::Start()
extern void IgniterOnSave_Start_mB26DA45FB9B289AC96514148AE82C00390CE3BB1 (void);
// 0x00000664 System.Void GameCreator.Core.IgniterOnSave::OnDestroy()
extern void IgniterOnSave_OnDestroy_mAB75B09ECEE75CB377BBAE4F6C8946BE7FEFA9B6 (void);
// 0x00000665 System.Void GameCreator.Core.IgniterOnSave::OnSave(System.Int32)
extern void IgniterOnSave_OnSave_m396483DB72D828C76ACCE4F0BF645DC936937F8C (void);
// 0x00000666 System.Void GameCreator.Core.IgniterOnSave::.ctor()
extern void IgniterOnSave__ctor_m0241F31E80B607B4319A7F374EEF095730A6BAFC (void);
// 0x00000667 System.Void GameCreator.Core.IgniterPlayerEnter::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterPlayerEnter_OnTriggerEnter_m90F4F2AF15EB9725EE7BBB7B48D8E9CC4C00F942 (void);
// 0x00000668 System.Void GameCreator.Core.IgniterPlayerEnter::.ctor()
extern void IgniterPlayerEnter__ctor_m30D2DB2B398D24D8F279706B4DB7B585F800F964 (void);
// 0x00000669 System.Void GameCreator.Core.IgniterPlayerEnterPressKey::Update()
extern void IgniterPlayerEnterPressKey_Update_m804205357C1AF258A429140FFD06D768F1D7C457 (void);
// 0x0000066A System.Void GameCreator.Core.IgniterPlayerEnterPressKey::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterPlayerEnterPressKey_OnTriggerEnter_m641490B8C67D9A0167DBE684C04FC5EA593C4562 (void);
// 0x0000066B System.Void GameCreator.Core.IgniterPlayerEnterPressKey::OnTriggerExit(UnityEngine.Collider)
extern void IgniterPlayerEnterPressKey_OnTriggerExit_m618BB5D7A61718C06A2BBD4E17B4BFD2886830FB (void);
// 0x0000066C System.Void GameCreator.Core.IgniterPlayerEnterPressKey::.ctor()
extern void IgniterPlayerEnterPressKey__ctor_m13DBC7ABC110C332931F46368B1821F2F1EF783F (void);
// 0x0000066D System.Void GameCreator.Core.IgniterPlayerExit::OnTriggerExit(UnityEngine.Collider)
extern void IgniterPlayerExit_OnTriggerExit_mBDFD28A8D35D8F019BD27970E5F1BD24E712692A (void);
// 0x0000066E System.Void GameCreator.Core.IgniterPlayerExit::.ctor()
extern void IgniterPlayerExit__ctor_mD0FCC1DB89966126F9190D021525E568B4FDD734 (void);
// 0x0000066F System.Void GameCreator.Core.IgniterPlayerStayTimeout::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterPlayerStayTimeout_OnTriggerEnter_mE186F0AB204AE6F3CA9FF332D6A12EBA306C4F5C (void);
// 0x00000670 System.Void GameCreator.Core.IgniterPlayerStayTimeout::OnTriggerExit(UnityEngine.Collider)
extern void IgniterPlayerStayTimeout_OnTriggerExit_m26CD6D3F7EB60BDF2EA3DA46DA56F6EB5C29DE81 (void);
// 0x00000671 System.Void GameCreator.Core.IgniterPlayerStayTimeout::OnTriggerStay(UnityEngine.Collider)
extern void IgniterPlayerStayTimeout_OnTriggerStay_mBB4622AAD7518335E786E4BF36BF3BB509985D66 (void);
// 0x00000672 System.Void GameCreator.Core.IgniterPlayerStayTimeout::.ctor()
extern void IgniterPlayerStayTimeout__ctor_mACAF71B87BAAE2B4749AA4F9B41C5D91AF22645E (void);
// 0x00000673 System.Void GameCreator.Core.IgniterRaycastMousePosition::Start()
extern void IgniterRaycastMousePosition_Start_m3C269DBD2B569B5FA5FD012ECAA10473EE8AA40A (void);
// 0x00000674 System.Void GameCreator.Core.IgniterRaycastMousePosition::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void IgniterRaycastMousePosition_OnPointerClick_m610DB7B260641814FD9C0622E6F20DB7092A7BB2 (void);
// 0x00000675 System.Void GameCreator.Core.IgniterRaycastMousePosition::.ctor()
extern void IgniterRaycastMousePosition__ctor_m80EB79946A736E0D1B73C7E9A4F6184360DF7C32 (void);
// 0x00000676 System.Void GameCreator.Core.IgniterStart::Start()
extern void IgniterStart_Start_mDA4327C504F7E8B5614C7EBEFE854FFB2C2EA8B4 (void);
// 0x00000677 System.Void GameCreator.Core.IgniterStart::.ctor()
extern void IgniterStart__ctor_m61D6F7FE96CE90813B4B1866306D81132F3C4CB7 (void);
// 0x00000678 System.Void GameCreator.Core.IgniterTriggerEnter::Start()
extern void IgniterTriggerEnter_Start_mCA90CBDADD1381FA363CDE253D679914D89D051B (void);
// 0x00000679 System.Void GameCreator.Core.IgniterTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterTriggerEnter_OnTriggerEnter_m1C6DB71DE38DC27D6FDC3CC30FEEE2D0C5FB2F5E (void);
// 0x0000067A System.Void GameCreator.Core.IgniterTriggerEnter::.ctor()
extern void IgniterTriggerEnter__ctor_mD96D93BD86B32D9C41819D3970687D0454A0A63B (void);
// 0x0000067B System.Void GameCreator.Core.IgniterTriggerEnterTag::Start()
extern void IgniterTriggerEnterTag_Start_m8C96DD5B2CB9BDB7D1F14E4A762BE7C89EEE8340 (void);
// 0x0000067C System.Void GameCreator.Core.IgniterTriggerEnterTag::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterTriggerEnterTag_OnTriggerEnter_mBBF19E8401FD7705477B361F2F5F44CE6C551DE7 (void);
// 0x0000067D System.Void GameCreator.Core.IgniterTriggerEnterTag::.ctor()
extern void IgniterTriggerEnterTag__ctor_mB039C897D088F286423E40955DB376E186D55BD1 (void);
// 0x0000067E System.Void GameCreator.Core.IgniterTriggerExit::Start()
extern void IgniterTriggerExit_Start_m83E75098EA38C3DAA6BFBDF6BD9524666CC5731E (void);
// 0x0000067F System.Void GameCreator.Core.IgniterTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern void IgniterTriggerExit_OnTriggerExit_m328A23B3018786FA5DF435A6891B0677CF594570 (void);
// 0x00000680 System.Void GameCreator.Core.IgniterTriggerExit::.ctor()
extern void IgniterTriggerExit__ctor_mBFBAEDC1B58AD54E49D8472E877CB69E834FFCC9 (void);
// 0x00000681 System.Void GameCreator.Core.IgniterTriggerExitTag::Start()
extern void IgniterTriggerExitTag_Start_mBA63438E8A9E24BC3A628238D383AA188C018A23 (void);
// 0x00000682 System.Void GameCreator.Core.IgniterTriggerExitTag::OnTriggerExit(UnityEngine.Collider)
extern void IgniterTriggerExitTag_OnTriggerExit_m392CAB08043B7A21D4F37F127D6BE75F5F286EA3 (void);
// 0x00000683 System.Void GameCreator.Core.IgniterTriggerExitTag::.ctor()
extern void IgniterTriggerExitTag__ctor_mA8601E05C7BD77C3F0CF3A17274D8FAAAF80AB1D (void);
// 0x00000684 System.Void GameCreator.Core.IgniterTriggerStayTimeout::OnTriggerEnter(UnityEngine.Collider)
extern void IgniterTriggerStayTimeout_OnTriggerEnter_m39C9798CD74A70D067493DBCC99D5EC4B733392E (void);
// 0x00000685 System.Void GameCreator.Core.IgniterTriggerStayTimeout::OnTriggerExit(UnityEngine.Collider)
extern void IgniterTriggerStayTimeout_OnTriggerExit_m53AD57D524CAAA14B7EDF243E0410B2F535EFE6E (void);
// 0x00000686 System.Void GameCreator.Core.IgniterTriggerStayTimeout::OnTriggerStay(UnityEngine.Collider)
extern void IgniterTriggerStayTimeout_OnTriggerStay_m6B90A8781451B606777234E4D04E1BEAB5FD9789 (void);
// 0x00000687 System.Void GameCreator.Core.IgniterTriggerStayTimeout::.ctor()
extern void IgniterTriggerStayTimeout__ctor_m300C2D59CFDBEA855C8D3945F160D8FCD03EA22F (void);
// 0x00000688 System.Void GameCreator.Core.IgniterVariable::Start()
extern void IgniterVariable_Start_mFAA7D85379354FC6C398E81599604DD5DF377C28 (void);
// 0x00000689 System.Void GameCreator.Core.IgniterVariable::OnVariableChange(System.String)
extern void IgniterVariable_OnVariableChange_m2E59EAA694B904B9AEBE78FF81D226A769F0B817 (void);
// 0x0000068A System.Void GameCreator.Core.IgniterVariable::OnListChange(System.Int32,System.Object,System.Object)
extern void IgniterVariable_OnListChange_m8351BF9F0F95D1397C014F4027CFB74745FA6246 (void);
// 0x0000068B System.Void GameCreator.Core.IgniterVariable::OnDestroy()
extern void IgniterVariable_OnDestroy_m4C710FD4F50AB342FA764F72647A896B6DB06ACD (void);
// 0x0000068C System.Void GameCreator.Core.IgniterVariable::.ctor()
extern void IgniterVariable__ctor_m55D7A2194387663B343522738365C7F740420444 (void);
// 0x0000068D System.Void GameCreator.Core.IgniterVisible::OnBecameVisible()
extern void IgniterVisible_OnBecameVisible_m2265116EDAB4BBF43CB4E3DEBC997B669F843E7D (void);
// 0x0000068E System.Void GameCreator.Core.IgniterVisible::.ctor()
extern void IgniterVisible__ctor_mDB27D4D40EE7B28CFE4524A29928B6CDD2304D96 (void);
// 0x0000068F System.Void GameCreator.Core.IgniterWhileKeyDown::Update()
extern void IgniterWhileKeyDown_Update_mB37CDD80D5E385075FE2E5858920349137C5D8C2 (void);
// 0x00000690 System.Void GameCreator.Core.IgniterWhileKeyDown::.ctor()
extern void IgniterWhileKeyDown__ctor_m6D6C8224021FCA5C2D64DE0A71F8E094541936FE (void);
// 0x00000691 System.Void GameCreator.Core.IgniterWhileMouseDown::Update()
extern void IgniterWhileMouseDown_Update_mF295631074D1CE8DB46A2B271FF3236B662238F0 (void);
// 0x00000692 System.Void GameCreator.Core.IgniterWhileMouseDown::.ctor()
extern void IgniterWhileMouseDown__ctor_m0823DD930CBBAB17E68795143810B9A9E463456D (void);
// 0x00000693 System.Void GameCreator.Core.TouchStick::Start()
extern void TouchStick_Start_m39C9D732A7BF16DDA32FF426B8BE2A1741F27361 (void);
// 0x00000694 System.Void GameCreator.Core.TouchStick::OnEnable()
extern void TouchStick_OnEnable_m2A913D859AAD75999747256CF37CA15C31DD5BB6 (void);
// 0x00000695 System.Void GameCreator.Core.TouchStick::OnDisable()
extern void TouchStick_OnDisable_m62C7BD83B429494E8730E06667108267B851A32B (void);
// 0x00000696 System.Void GameCreator.Core.TouchStick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void TouchStick_OnDrag_mABBC10FF6BC78F16B9E353965EB16781A2377C06 (void);
// 0x00000697 System.Void GameCreator.Core.TouchStick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TouchStick_OnPointerDown_m7A97779E350C736428B612CE5124895ABE77D08C (void);
// 0x00000698 System.Void GameCreator.Core.TouchStick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TouchStick_OnPointerUp_m788060CD9FD1836EFB16874C38E3F292DFBEB27B (void);
// 0x00000699 UnityEngine.Vector2 GameCreator.Core.TouchStick::GetDirection()
extern void TouchStick_GetDirection_mDF78799859024D1AFFB049CC81E4BEB7670E5364 (void);
// 0x0000069A System.Void GameCreator.Core.TouchStick::SetDirection(UnityEngine.Vector2)
extern void TouchStick_SetDirection_m9C27AB10B629507512A882C058ADED708FFD48C3 (void);
// 0x0000069B System.Void GameCreator.Core.TouchStick::.ctor()
extern void TouchStick__ctor_mE27AB7F0C45EC3D9A3A3E9CBA86BE58C56DE84A9 (void);
// 0x0000069C System.Void GameCreator.Core.TouchStickManager::OnCreate()
extern void TouchStickManager_OnCreate_m636797065101B4C4FB0ECD895F91E5CDF5F594F1 (void);
// 0x0000069D System.Void GameCreator.Core.TouchStickManager::OnSceneLoad(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void TouchStickManager_OnSceneLoad_m27BD1E8FEB03CED638EDEE7735BB510E210079F1 (void);
// 0x0000069E System.Void GameCreator.Core.TouchStickManager::UpdatePlayerEvents()
extern void TouchStickManager_UpdatePlayerEvents_m7144AC7BF194EC0BE881BDD2ECA1410E1F03B970 (void);
// 0x0000069F System.Void GameCreator.Core.TouchStickManager::OnChangeIsControllable(System.Boolean)
extern void TouchStickManager_OnChangeIsControllable_mAE63B19719B9F48DA2DF6128FC35CDCC3D73CC0F (void);
// 0x000006A0 UnityEngine.Vector2 GameCreator.Core.TouchStickManager::GetDirection(GameCreator.Characters.PlayerCharacter)
extern void TouchStickManager_GetDirection_m30B183E1F9A3D990D82A4DFB930083F59BC88219 (void);
// 0x000006A1 System.Void GameCreator.Core.TouchStickManager::SetVisibility(System.Boolean)
extern void TouchStickManager_SetVisibility_m6200595A101FBD41404994EBC2A0B2E262833A18 (void);
// 0x000006A2 System.Void GameCreator.Core.TouchStickManager::.ctor()
extern void TouchStickManager__ctor_m3CAD680A041D808F6566579A306E0DAF704175E3 (void);
// 0x000006A3 System.Void GameCreator.Core.TouchStickManager::.cctor()
extern void TouchStickManager__cctor_m3B2F295CCB7A562B3FC0D53D5D7C45B2984B5A9F (void);
// 0x000006A4 System.Void GameCreator.Core.Actions::OnDestroy()
extern void Actions_OnDestroy_mFC2B35EE8300A684436681AD7A67C8C1F78C9841 (void);
// 0x000006A5 System.Void GameCreator.Core.Actions::Awake()
extern void Actions_Awake_mB0E337F429D50524C635671CED552C2A60326BB9 (void);
// 0x000006A6 System.Void GameCreator.Core.Actions::OnEnable()
extern void Actions_OnEnable_m28D6A232DC3BA403959A9A0F84B08A1BEDE03E3D (void);
// 0x000006A7 System.Void GameCreator.Core.Actions::Execute(UnityEngine.GameObject,System.Object[])
extern void Actions_Execute_m028F25C515D5D301413DB5E45F9C970A69C3387A (void);
// 0x000006A8 System.Void GameCreator.Core.Actions::Execute(System.Object[])
extern void Actions_Execute_m753E2E2998E02C5F2E806D59A36F5FB78C9A753D (void);
// 0x000006A9 System.Void GameCreator.Core.Actions::Execute()
extern void Actions_Execute_m62B8E7EC0880236989338403898EC9742FC6F2EC (void);
// 0x000006AA System.Void GameCreator.Core.Actions::ExecuteWithTarget(UnityEngine.GameObject)
extern void Actions_ExecuteWithTarget_m024C3C5B550AF7511201D956015401BE572268C7 (void);
// 0x000006AB System.Void GameCreator.Core.Actions::OnFinish()
extern void Actions_OnFinish_m150299A1E4E025945B1F6DCB56D1D40D3802DB8A (void);
// 0x000006AC System.Void GameCreator.Core.Actions::Stop()
extern void Actions_Stop_m70B5D297E8E2FA2871B9FEDE74752FC3DCE7235A (void);
// 0x000006AD System.Void GameCreator.Core.Actions::OnDrawGizmos()
extern void Actions_OnDrawGizmos_m7CB70BFE85415070FDD4641EE9B3E35121A86B10 (void);
// 0x000006AE System.Void GameCreator.Core.Actions::.ctor()
extern void Actions__ctor_m4E5B373C8BDF42D2F6356DFAF862B4C92D32E7D3 (void);
// 0x000006AF System.Void GameCreator.Core.Actions::.cctor()
extern void Actions__cctor_m3A199AB5FA778C5F4E6B765EE73CF8A47040F596 (void);
// 0x000006B0 System.Void GameCreator.Core.Actions/ExecuteEvent::.ctor()
extern void ExecuteEvent__ctor_m7320C292EA87B508C0A4E6E836A20CA86772B6C9 (void);
// 0x000006B1 System.Void GameCreator.Core.Conditions::Interact()
extern void Conditions_Interact_m7DF3743DBE4C025DF5AD5DA69B17B9D8AB73BC64 (void);
// 0x000006B2 System.Void GameCreator.Core.Conditions::Interact(UnityEngine.GameObject,System.Object[])
extern void Conditions_Interact_m730603257F8CBFDE70A458D95E42F1374675F25A (void);
// 0x000006B3 System.Collections.IEnumerator GameCreator.Core.Conditions::InteractCoroutine(UnityEngine.GameObject)
extern void Conditions_InteractCoroutine_m28D3C22682852BF779FC1C09E3A86331BB9EEB64 (void);
// 0x000006B4 System.Void GameCreator.Core.Conditions::OnDrawGizmos()
extern void Conditions_OnDrawGizmos_m4229741782C602BFA0D538BBECC3B707DE073AC9 (void);
// 0x000006B5 System.Void GameCreator.Core.Conditions::.ctor()
extern void Conditions__ctor_m57AA7F41BA24E1D40D0F8D3D28B6EE0759BDE65E (void);
// 0x000006B6 System.Void GameCreator.Core.Conditions/<InteractCoroutine>d__5::.ctor(System.Int32)
extern void U3CInteractCoroutineU3Ed__5__ctor_mD95C463FEDE49E3E955F7F9A1BB8C951B75C3122 (void);
// 0x000006B7 System.Void GameCreator.Core.Conditions/<InteractCoroutine>d__5::System.IDisposable.Dispose()
extern void U3CInteractCoroutineU3Ed__5_System_IDisposable_Dispose_mF1F4A66B05E74B40A8654C03EC212E38000C6FCF (void);
// 0x000006B8 System.Boolean GameCreator.Core.Conditions/<InteractCoroutine>d__5::MoveNext()
extern void U3CInteractCoroutineU3Ed__5_MoveNext_m24726723493F4A4F0A0D974C22D1794D2CFF7CE8 (void);
// 0x000006B9 System.Object GameCreator.Core.Conditions/<InteractCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInteractCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BCBBC8CAA0E427B467E5E183B7D934C8E4D42B (void);
// 0x000006BA System.Void GameCreator.Core.Conditions/<InteractCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCFDA796A50A6135EA71F3FE4E5BC9E44C478C3E1 (void);
// 0x000006BB System.Object GameCreator.Core.Conditions/<InteractCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mE8C1D7E5CA1FBBB04F671E6939B8BE5D7209572C (void);
// 0x000006BC System.Void GameCreator.Core.Hotspot::Awake()
extern void Hotspot_Awake_mC998E7EA8E59A852077E9DA2B64C2A18C5A261D5 (void);
// 0x000006BD System.Void GameCreator.Core.Hotspot::OnMouseEnter()
extern void Hotspot_OnMouseEnter_mB9804C7CD78656A395B0BD441EB03DF84D51B11C (void);
// 0x000006BE System.Void GameCreator.Core.Hotspot::OnMouseExit()
extern void Hotspot_OnMouseExit_mEE60C2E87C8D1D6E3D5F2A0939F8A27416D8E254 (void);
// 0x000006BF System.Void GameCreator.Core.Hotspot::OnMouseOver()
extern void Hotspot_OnMouseOver_mA0B8ED0837B05F28CCA858EC4CA4898A31984FE2 (void);
// 0x000006C0 System.Void GameCreator.Core.Hotspot::OnDestroy()
extern void Hotspot_OnDestroy_mF5BCD8E234E58A8B6526DCB3E82A5D3A6DC93DD0 (void);
// 0x000006C1 System.Void GameCreator.Core.Hotspot::OnDrawGizmos()
extern void Hotspot_OnDrawGizmos_mFC69E159824D7CF02B12092D5FE963FFDC736748 (void);
// 0x000006C2 System.Void GameCreator.Core.Hotspot::.ctor()
extern void Hotspot__ctor_mEE591D14A1571FE1ED64987BBE4084F09571BB44 (void);
// 0x000006C3 System.Void GameCreator.Core.Trigger::Awake()
extern void Trigger_Awake_m041B79D685F982DDD0B897DB09F44F0E2A946954 (void);
// 0x000006C4 System.Void GameCreator.Core.Trigger::OnDisable()
extern void Trigger_OnDisable_m927F60743CF15C4578AFACFB055F56D4EE4DDF30 (void);
// 0x000006C5 System.Void GameCreator.Core.Trigger::SetupPlatformIgniter()
extern void Trigger_SetupPlatformIgniter_m039E268B916D2873413085FA6EA13DA7287DABD0 (void);
// 0x000006C6 System.Boolean GameCreator.Core.Trigger::CheckPlatformIgniter(GameCreator.Core.Trigger/Platforms)
extern void Trigger_CheckPlatformIgniter_mE73972AC98417134753CD2F8268F3ED252FA3792 (void);
// 0x000006C7 System.Void GameCreator.Core.Trigger::Execute(UnityEngine.GameObject,System.Object[])
extern void Trigger_Execute_m1A6FC0B07CE3CC98139220B8963BE093CF3F0EFE (void);
// 0x000006C8 System.Void GameCreator.Core.Trigger::Execute()
extern void Trigger_Execute_m228F7389F4F940D018F6375A56C285803FB2891B (void);
// 0x000006C9 System.Void GameCreator.Core.Trigger::OnDrawGizmos()
extern void Trigger_OnDrawGizmos_m83A4B9B54C4409CA092A92C40BBDF33574E35780 (void);
// 0x000006CA System.Void GameCreator.Core.Trigger::.ctor()
extern void Trigger__ctor_mE3CF6103551311CEBDAAE7896AAC698B4E8ABB0B (void);
// 0x000006CB System.Void GameCreator.Core.Trigger::.cctor()
extern void Trigger__cctor_m860B33710C9B1D86F6D5F17D4040335F7DC96743 (void);
// 0x000006CC System.Void GameCreator.Core.Trigger/Item::.ctor(GameCreator.Core.Actions)
extern void Item__ctor_m8A79F7CDD986E8044B2C5466C25145F001CFBA4E (void);
// 0x000006CD System.Void GameCreator.Core.Trigger/Item::.ctor(GameCreator.Core.Conditions)
extern void Item__ctor_mD9369BB440151931E5316AA9DEF0814CED60B6CA (void);
// 0x000006CE System.Void GameCreator.Core.Trigger/PlatformIgniters::.ctor()
extern void PlatformIgniters__ctor_mC5BF3A8D71283CDBF1615FC95F3A0118D0C5475A (void);
// 0x000006CF System.Void GameCreator.Core.Trigger/TriggerEvent::.ctor()
extern void TriggerEvent__ctor_m649935D4523605FD40B8E11927028E1A7138AE7A (void);
// 0x000006D0 System.Void GameCreator.Core.KeysData::Update(System.Int32,System.Collections.Generic.List`1<System.String>)
extern void KeysData_Update_m6314F5F348C65173F16FA1B3F1FFDF49AB6656D4 (void);
// 0x000006D1 System.Void GameCreator.Core.KeysData::Delete(System.Int32)
extern void KeysData_Delete_m5729C2D5379AC3F686E74132922BA7D35BDFF616 (void);
// 0x000006D2 GameCreator.Core.KeysData/Data GameCreator.Core.KeysData::GetCurrentKeys(System.Int32)
extern void KeysData_GetCurrentKeys_m01577460B384E5B150069F67216EE2CABDDD5C07 (void);
// 0x000006D3 System.String GameCreator.Core.KeysData::GetKey(System.Int32)
extern void KeysData_GetKey_mD8ECB9A3F544FA92E1A47A9EA52F084A6A11D57A (void);
// 0x000006D4 System.Void GameCreator.Core.KeysData::.ctor()
extern void KeysData__ctor_mDEE11C28D1C704EBA1B1EBDFC1B8E7A4EA2E7972 (void);
// 0x000006D5 System.Void GameCreator.Core.KeysData/Data::.ctor()
extern void Data__ctor_mDF786960BE82C20F06F7AB04170A701ACB407E5F (void);
// 0x000006D6 System.Void GameCreator.Core.KeysData/Data::.ctor(System.Collections.Generic.HashSet`1<System.String>)
extern void Data__ctor_m902FDCFD1F7BDE2B25A051550FD71871EDF4F8BD (void);
// 0x000006D7 System.Void GameCreator.Core.SavesData::.ctor(GameCreator.Core.SaveLoadManager)
extern void SavesData__ctor_m9FB5191B94BE4CCA78A3F5FEC120835C751ABC46 (void);
// 0x000006D8 System.Void GameCreator.Core.SavesData::OnSave(System.Int32)
extern void SavesData_OnSave_m0215A8526801A255CD7353190FF5F1308908BAB0 (void);
// 0x000006D9 System.Int32 GameCreator.Core.SavesData::GetLastSave()
extern void SavesData_GetLastSave_m21354B84158DC597D99FE5D32C564DB86EAE7686 (void);
// 0x000006DA System.Int32 GameCreator.Core.SavesData::GetSavesCount()
extern void SavesData_GetSavesCount_mA8CD1A6AC76918B852A640997117B6ABDC0C6948 (void);
// 0x000006DB GameCreator.Core.SavesData/Profile GameCreator.Core.SavesData::GetProfileInfo(System.Int32)
extern void SavesData_GetProfileInfo_m096AEE837F504DD9F29B325AD90D56870FC5B1EC (void);
// 0x000006DC System.Void GameCreator.Core.SavesData/Profile::.ctor()
extern void Profile__ctor_m0651C18B7EFB79060DEE727A9DC0F7FAB9FEB712 (void);
// 0x000006DD System.Void GameCreator.Core.SavesData/Profiles::.ctor()
extern void Profiles__ctor_mF90C1A67E2BBA11BE4ABE7DCA281DC433043DD30 (void);
// 0x000006DE System.Void GameCreator.Core.ScenesData::.ctor(System.String)
extern void ScenesData__ctor_m4D0E2A75B27613D5474C1A165DEC47AB559F207E (void);
// 0x000006DF System.Void GameCreator.Core.ScenesData::Add(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern void ScenesData_Add_mF7AA174217EDF483EED605361D614EF58EA587EB (void);
// 0x000006E0 System.Void GameCreator.Core.ScenesData::Remove(System.String)
extern void ScenesData_Remove_m0E4C81DC9CC4D1BC019E9FF4799E7BACEC715EC0 (void);
// 0x000006E1 System.Object GameCreator.Core.ScenesData::GetSaveData()
extern void ScenesData_GetSaveData_m6D46AD38EA6F422D6C9F7BFCB10E0E629AA03EEB (void);
// 0x000006E2 System.Type GameCreator.Core.ScenesData::GetSaveDataType()
extern void ScenesData_GetSaveDataType_m0FA65655ACAAAE01CCC4D6A918173368EE37B20F (void);
// 0x000006E3 System.String GameCreator.Core.ScenesData::GetUniqueName()
extern void ScenesData_GetUniqueName_m64D4C160CE95624B5C1A6B0E1681E031E330BFDD (void);
// 0x000006E4 System.Collections.IEnumerator GameCreator.Core.ScenesData::OnLoad(System.Object)
extern void ScenesData_OnLoad_m9DD2B0255D05F2CC56291A43DA68F1D794D9B8BA (void);
// 0x000006E5 System.Void GameCreator.Core.ScenesData::ResetData()
extern void ScenesData_ResetData_m04448DBFD21D55A8D09D53C7EEB5127F4DA51913 (void);
// 0x000006E6 System.Void GameCreator.Core.ScenesData/<OnLoad>d__8::.ctor(System.Int32)
extern void U3COnLoadU3Ed__8__ctor_m2623EB815DC1D76469AE888CCFC58B3EC04319B0 (void);
// 0x000006E7 System.Void GameCreator.Core.ScenesData/<OnLoad>d__8::System.IDisposable.Dispose()
extern void U3COnLoadU3Ed__8_System_IDisposable_Dispose_mDF1AEC6C64F34D03F84D44928EF2E34986C0CC9A (void);
// 0x000006E8 System.Boolean GameCreator.Core.ScenesData/<OnLoad>d__8::MoveNext()
extern void U3COnLoadU3Ed__8_MoveNext_m420980EF293A2B4DCE636DA9A82C94700F6024DD (void);
// 0x000006E9 System.Object GameCreator.Core.ScenesData/<OnLoad>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnLoadU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC76155CEECA7B46580F2B1A8028F9F301BCDDAB (void);
// 0x000006EA System.Void GameCreator.Core.ScenesData/<OnLoad>d__8::System.Collections.IEnumerator.Reset()
extern void U3COnLoadU3Ed__8_System_Collections_IEnumerator_Reset_m56DC36C13893D1FB4DF0C015C520BFE5A539A182 (void);
// 0x000006EB System.Object GameCreator.Core.ScenesData/<OnLoad>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COnLoadU3Ed__8_System_Collections_IEnumerator_get_Current_mA8ABABF1674E47A2E404A53461DBD8AF95075ED6 (void);
// 0x000006EC System.Void GameCreator.Core.GameProfileUI::Start()
extern void GameProfileUI_Start_m77E83860D1A06E55851D4B73B99C796E3D5D29D0 (void);
// 0x000006ED System.Void GameCreator.Core.GameProfileUI::OnDestroy()
extern void GameProfileUI_OnDestroy_mCCD9EE02D38672CDD957327B2C4BB7B219902B1E (void);
// 0x000006EE System.Void GameCreator.Core.GameProfileUI::UpdateUI(System.Int32)
extern void GameProfileUI_UpdateUI_m99D0D709A17B6421E5ABE187A7089B6C2F633B74 (void);
// 0x000006EF System.Void GameCreator.Core.GameProfileUI::.ctor()
extern void GameProfileUI__ctor_m2CFEE491140B90B8C5ED4CFB1596E915E13B978E (void);
// 0x000006F0 System.Single GameCreator.Core.DataProviderDefault::GetFloat(System.String,System.Single)
extern void DataProviderDefault_GetFloat_m36FC003E8E00DBAB967F37BD53BA0F80152E6F42 (void);
// 0x000006F1 System.Int32 GameCreator.Core.DataProviderDefault::GetInt(System.String,System.Int32)
extern void DataProviderDefault_GetInt_mFB0852533DFE956A1052CC04A47F7DC75B0CAE4D (void);
// 0x000006F2 System.String GameCreator.Core.DataProviderDefault::GetString(System.String,System.String)
extern void DataProviderDefault_GetString_m3F82DFAB342988E0A18841CABCCD697332F49850 (void);
// 0x000006F3 System.Void GameCreator.Core.DataProviderDefault::SetFloat(System.String,System.Single)
extern void DataProviderDefault_SetFloat_m164715EE6E2CD77A889F6E031C10AEB869EB2A11 (void);
// 0x000006F4 System.Void GameCreator.Core.DataProviderDefault::SetInt(System.String,System.Int32)
extern void DataProviderDefault_SetInt_m765E7224F8485591CEA9A66B70DCB74CB13EDF8C (void);
// 0x000006F5 System.Void GameCreator.Core.DataProviderDefault::SetString(System.String,System.String)
extern void DataProviderDefault_SetString_mC4AD3CCC7297ADD1ACFB8C9E63CAB4B6DCD81FC1 (void);
// 0x000006F6 System.Void GameCreator.Core.DataProviderDefault::DeleteAll()
extern void DataProviderDefault_DeleteAll_mB48302D72051CE0D32B48E7EF15F1B913358DB58 (void);
// 0x000006F7 System.Void GameCreator.Core.DataProviderDefault::DeleteKey(System.String)
extern void DataProviderDefault_DeleteKey_mFACD63C8FF0658357F55EA5BA5EACFE516068959 (void);
// 0x000006F8 System.Boolean GameCreator.Core.DataProviderDefault::HasKey(System.String)
extern void DataProviderDefault_HasKey_m6F7D0B2A9D3BA3FA41A2BAB4A969C9B767A42A94 (void);
// 0x000006F9 System.Void GameCreator.Core.DataProviderDefault::.ctor()
extern void DataProviderDefault__ctor_m002E0BB99E475D604996A1E90866DEEC9F5019D8 (void);
// 0x000006FA System.String GameCreator.Core.IDataProvider::GetString(System.String,System.String)
// 0x000006FB System.Void GameCreator.Core.IDataProvider::SetString(System.String,System.String)
// 0x000006FC System.Void GameCreator.Core.IDataProvider::DeleteAll()
// 0x000006FD System.Void GameCreator.Core.IDataProvider::DeleteKey(System.String)
// 0x000006FE System.Boolean GameCreator.Core.IDataProvider::HasKey(System.String)
// 0x000006FF System.Single GameCreator.Core.IDataProvider::GetFloat(System.String,System.Single)
extern void IDataProvider_GetFloat_m64C478F7D563C50D253221AE7F0CDF8837931D2A (void);
// 0x00000700 System.Int32 GameCreator.Core.IDataProvider::GetInt(System.String,System.Int32)
extern void IDataProvider_GetInt_m1013A847488C16AE1509B68A1D840339A61A1EC7 (void);
// 0x00000701 System.Void GameCreator.Core.IDataProvider::SetFloat(System.String,System.Single)
extern void IDataProvider_SetFloat_mD920C29AAC301C6DCDC91697A8EB28A8555E5806 (void);
// 0x00000702 System.Void GameCreator.Core.IDataProvider::SetInt(System.String,System.Int32)
extern void IDataProvider_SetInt_m3A062C8A86B183C799DDABDC658D79AB54733232 (void);
// 0x00000703 System.Void GameCreator.Core.IDataProvider::.ctor()
extern void IDataProvider__ctor_m15C932F3A99997B660614D6BB8D22EC679C5AAD9 (void);
// 0x00000704 System.Void GameCreator.Core.RememberActive::Awake()
extern void RememberActive_Awake_m6A0C7290441B48B7295E8D725AF1CFB1A6C94151 (void);
// 0x00000705 System.Void GameCreator.Core.RememberActive::OnEnable()
extern void RememberActive_OnEnable_m59895BFDAC72B15AD6E32676EE5D2A47EF7F8431 (void);
// 0x00000706 System.Void GameCreator.Core.RememberActive::OnDisable()
extern void RememberActive_OnDisable_m284785B0B430A3016A0688A14C95B2EF08CA7AA6 (void);
// 0x00000707 System.Void GameCreator.Core.RememberActive::UpdateState()
extern void RememberActive_UpdateState_m68A57A63F72B7E8E46A4F3467181C9CDB1D2238B (void);
// 0x00000708 System.Void GameCreator.Core.RememberActive::OnDestroy()
extern void RememberActive_OnDestroy_m280A1B7700B672A6B4BB31A926442E9CF005E28E (void);
// 0x00000709 System.Object GameCreator.Core.RememberActive::GetSaveData()
extern void RememberActive_GetSaveData_m629F2E95EDA0775514A508EC98A11AF85DD98986 (void);
// 0x0000070A System.Type GameCreator.Core.RememberActive::GetSaveDataType()
extern void RememberActive_GetSaveDataType_m0A009C072897F55370599A25F4898C93A1A1BD35 (void);
// 0x0000070B System.String GameCreator.Core.RememberActive::GetUniqueName()
extern void RememberActive_GetUniqueName_m82FA76CC5A03BF6779C924921451C8E1708CA03B (void);
// 0x0000070C System.Void GameCreator.Core.RememberActive::OnLoad(System.Object)
extern void RememberActive_OnLoad_mDEE857E252E85C0348B093FD3AF741F1EB158E7D (void);
// 0x0000070D System.Void GameCreator.Core.RememberActive::ResetData()
extern void RememberActive_ResetData_m072659E48606ADB37358FFFE064E1AADF3922F2C (void);
// 0x0000070E System.Void GameCreator.Core.RememberActive::.ctor()
extern void RememberActive__ctor_m45821ED2A8C4B0456F7DE339271CFCFEA8104F41 (void);
// 0x0000070F System.Void GameCreator.Core.RememberActive/Memory::.ctor()
extern void Memory__ctor_m45B98EE1C3540BA23E7F9DF5DECDF2745A9EF0EC (void);
// 0x00000710 System.Object GameCreator.Core.RememberBase::GetSaveData()
// 0x00000711 System.Type GameCreator.Core.RememberBase::GetSaveDataType()
// 0x00000712 System.String GameCreator.Core.RememberBase::GetUniqueName()
// 0x00000713 System.Void GameCreator.Core.RememberBase::OnLoad(System.Object)
// 0x00000714 System.Void GameCreator.Core.RememberBase::ResetData()
// 0x00000715 System.Void GameCreator.Core.RememberBase::Start()
extern void RememberBase_Start_mE9ACC15835ADFA57140712D43533F97FF1C988CF (void);
// 0x00000716 System.Void GameCreator.Core.RememberBase::OnDestroy()
extern void RememberBase_OnDestroy_mBF795437D6B9DCAF024BF7837218738DFC6CDB3A (void);
// 0x00000717 System.Void GameCreator.Core.RememberBase::.ctor()
extern void RememberBase__ctor_m0162687B0B84A50712C5FE1F019E35DF6BBB3DA3 (void);
// 0x00000718 System.Object GameCreator.Core.RememberTransform::GetSaveData()
extern void RememberTransform_GetSaveData_m7BC204A5636276D3B7CCD881D1F9195FF6CDF384 (void);
// 0x00000719 System.Type GameCreator.Core.RememberTransform::GetSaveDataType()
extern void RememberTransform_GetSaveDataType_m5B7722FB22BE7AC0FE25FDA1BBAE91E53C515F31 (void);
// 0x0000071A System.String GameCreator.Core.RememberTransform::GetUniqueName()
extern void RememberTransform_GetUniqueName_mD1849ADAD7505028D12CD2612442321DBEEA5019 (void);
// 0x0000071B System.Void GameCreator.Core.RememberTransform::OnLoad(System.Object)
extern void RememberTransform_OnLoad_m5A495A443004AC8B88340AE49E2EFFD83FDC1E63 (void);
// 0x0000071C System.Void GameCreator.Core.RememberTransform::ResetData()
extern void RememberTransform_ResetData_mF78A0341D00F2835EE84D37486E04B183D342555 (void);
// 0x0000071D System.Void GameCreator.Core.RememberTransform::.ctor()
extern void RememberTransform__ctor_mE63BBF05F5EEEBFC724B0CD572193E477E100C10 (void);
// 0x0000071E System.Void GameCreator.Core.RememberTransform/Memory::.ctor()
extern void Memory__ctor_m54D380158DA6738E0931614C265BFDC7A27D063C (void);
// 0x0000071F System.Boolean GameCreator.Core.SaveLoadManager::get_IsLoading()
extern void SaveLoadManager_get_IsLoading_m48BA1B1C94B4C49EDD9AE123CB172588AB0F677B (void);
// 0x00000720 System.Void GameCreator.Core.SaveLoadManager::set_IsLoading(System.Boolean)
extern void SaveLoadManager_set_IsLoading_m72592D60732CD135C5A77D6F0983FB422F577C07 (void);
// 0x00000721 System.Boolean GameCreator.Core.SaveLoadManager::get_IsProfileLoaded()
extern void SaveLoadManager_get_IsProfileLoaded_m5284D5C2C5C09F8DD70C4F0F655C382F849A95FC (void);
// 0x00000722 System.Void GameCreator.Core.SaveLoadManager::set_IsProfileLoaded(System.Boolean)
extern void SaveLoadManager_set_IsProfileLoaded_mEF54E5023DF9D2A82D2B1B725486EACABE8A93FA (void);
// 0x00000723 System.Int32 GameCreator.Core.SaveLoadManager::get_ActiveProfile()
extern void SaveLoadManager_get_ActiveProfile_m9738798B39E0A1677771A2D85620B66F7D9E4D6E (void);
// 0x00000724 System.Void GameCreator.Core.SaveLoadManager::set_ActiveProfile(System.Int32)
extern void SaveLoadManager_set_ActiveProfile_mE1B1C4AE9C7C893FF128D3693F0AECB79E4AA4C2 (void);
// 0x00000725 System.Int32 GameCreator.Core.SaveLoadManager::get_LoadedProfile()
extern void SaveLoadManager_get_LoadedProfile_mB06CD36F75FFBC8CD6CF6EAA8A9D49B52AAC186A (void);
// 0x00000726 System.Void GameCreator.Core.SaveLoadManager::set_LoadedProfile(System.Int32)
extern void SaveLoadManager_set_LoadedProfile_mA4FCE167840C7250ED217EB4D9B03F89C7D4EFC2 (void);
// 0x00000727 GameCreator.Core.SavesData GameCreator.Core.SaveLoadManager::get_savesData()
extern void SaveLoadManager_get_savesData_mF5D63C886C2D47F216FA1BBBF71EA45CB5E825AD (void);
// 0x00000728 System.Void GameCreator.Core.SaveLoadManager::set_savesData(GameCreator.Core.SavesData)
extern void SaveLoadManager_set_savesData_m25C89E92AB5C3540BA52D7A12A63801808C510E6 (void);
// 0x00000729 System.Void GameCreator.Core.SaveLoadManager::InitializeOnLoad()
extern void SaveLoadManager_InitializeOnLoad_m10EFC6CEB1BF1717756D029431963EDF4A3AB8E0 (void);
// 0x0000072A System.Void GameCreator.Core.SaveLoadManager::OnCreate()
extern void SaveLoadManager_OnCreate_m81DE2F6B61CE9CDDD170E397663686BC6CE95B7C (void);
// 0x0000072B System.Void GameCreator.Core.SaveLoadManager::OnLoadScene(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SaveLoadManager_OnLoadScene_mA376B37FD4489D9DF1BE29F5B29B378CB50C2BF3 (void);
// 0x0000072C System.Void GameCreator.Core.SaveLoadManager::OnUnloadScene(UnityEngine.SceneManagement.Scene)
extern void SaveLoadManager_OnUnloadScene_m7B04C7FAAC17FD040DC5D67BFC5289C63D68E5C9 (void);
// 0x0000072D System.Void GameCreator.Core.SaveLoadManager::SetCurrentProfile(System.Int32)
extern void SaveLoadManager_SetCurrentProfile_mF27EB4254B93FA933135012DBD3B8475D0437030 (void);
// 0x0000072E System.Int32 GameCreator.Core.SaveLoadManager::GetCurrentProfile()
extern void SaveLoadManager_GetCurrentProfile_mF07D3349F2CB7F8451BADB9A81FE1121C2C61C9E (void);
// 0x0000072F System.Void GameCreator.Core.SaveLoadManager::DeleteProfile(System.Int32)
extern void SaveLoadManager_DeleteProfile_mC93CBF58D60E4A1C27B6A7DD7549753BBE019B1A (void);
// 0x00000730 System.Void GameCreator.Core.SaveLoadManager::Initialize(GameCreator.Core.IGameSave,System.Int32,System.Boolean)
extern void SaveLoadManager_Initialize_m55BBF273B0AC96A6AF463E338E7418A043E8C117 (void);
// 0x00000731 System.Void GameCreator.Core.SaveLoadManager::Save(System.Int32)
extern void SaveLoadManager_Save_mCBE83510A454B865AD8289F4CB42DC7477209A48 (void);
// 0x00000732 System.Void GameCreator.Core.SaveLoadManager::Load(System.Int32,System.Action)
extern void SaveLoadManager_Load_mC4E72352680186FF525832DCD993B7617A3E3F4F (void);
// 0x00000733 System.Collections.IEnumerator GameCreator.Core.SaveLoadManager::CoroutineLoad(System.Int32,System.Action)
extern void SaveLoadManager_CoroutineLoad_mA7DDBA766A8C4F7A96D4F726AA978178F4FE9CF0 (void);
// 0x00000734 System.Void GameCreator.Core.SaveLoadManager::LoadLast(System.Action)
extern void SaveLoadManager_LoadLast_m5DB788144BFF7809AF2D3001A8DBFE0AEB888280 (void);
// 0x00000735 System.Int32 GameCreator.Core.SaveLoadManager::GetSavesCount()
extern void SaveLoadManager_GetSavesCount_m5DACFB70C34FCFF42D3252F118B92B09FF33811E (void);
// 0x00000736 GameCreator.Core.SavesData/Profile GameCreator.Core.SaveLoadManager::GetProfileInfo(System.Int32)
extern void SaveLoadManager_GetProfileInfo_m545680876276621234A0838B7D573DBF1DB3DAB4 (void);
// 0x00000737 System.Void GameCreator.Core.SaveLoadManager::OnDestroyIGameSave(GameCreator.Core.IGameSave)
extern void SaveLoadManager_OnDestroyIGameSave_m3CC47941A3C361FB1996D49CDCADE5EC3AEA3FBB (void);
// 0x00000738 System.Void GameCreator.Core.SaveLoadManager::LoadItem(GameCreator.Core.IGameSave,System.Int32)
extern void SaveLoadManager_LoadItem_m87A01D70724F65541BC9A759C9060AE15C4C4503 (void);
// 0x00000739 System.String GameCreator.Core.SaveLoadManager::GetKeyName(System.Int32,System.String)
extern void SaveLoadManager_GetKeyName_m270659FE12BAF7D89C3F8BD9E04BD9C79107C6DB (void);
// 0x0000073A System.Void GameCreator.Core.SaveLoadManager::.ctor()
extern void SaveLoadManager__ctor_m1989B50AADE49ADB1ED5F24BC384A967254ACBCE (void);
// 0x0000073B System.Void GameCreator.Core.SaveLoadManager/Storage::.ctor(GameCreator.Core.IGameSave,System.Int32)
extern void Storage__ctor_m878B0AB68F0E4B7346B80B40EB10BA428C5AD5E5 (void);
// 0x0000073C System.Void GameCreator.Core.SaveLoadManager/ProfileEvent::.ctor()
extern void ProfileEvent__ctor_m29739ACE435BC396B6E03DE7999A7948AF6A1913 (void);
// 0x0000073D System.Void GameCreator.Core.SaveLoadManager/NonSerializableException::.ctor(System.String)
extern void NonSerializableException__ctor_m65AD626A2F44C22A0186EB8AAEFAAA46D9F54116 (void);
// 0x0000073E System.Void GameCreator.Core.SaveLoadManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mCFB839B220BE8478B18998CBE86279DC4D87339B (void);
// 0x0000073F System.Void GameCreator.Core.SaveLoadManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mAC75468494725563BFE6E5F1B65D40E1787E6D3E (void);
// 0x00000740 System.Int32 GameCreator.Core.SaveLoadManager/<>c::<Load>b__40_0(GameCreator.Core.SaveLoadManager/Storage,GameCreator.Core.SaveLoadManager/Storage)
extern void U3CU3Ec_U3CLoadU3Eb__40_0_m6DC9A3DDCEF98B03270D731D4678F8AD7580E8FA (void);
// 0x00000741 System.Void GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::.ctor(System.Int32)
extern void U3CCoroutineLoadU3Ed__41__ctor_m3223DF3BDBA925B9C01B1075D3CD3347CD43AEC0 (void);
// 0x00000742 System.Void GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::System.IDisposable.Dispose()
extern void U3CCoroutineLoadU3Ed__41_System_IDisposable_Dispose_m9988DEBB87C0389FF609CC218B64C147B325A8FA (void);
// 0x00000743 System.Boolean GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::MoveNext()
extern void U3CCoroutineLoadU3Ed__41_MoveNext_mC6F624465D7DE4C959C13B6A8DAD35F77C244B44 (void);
// 0x00000744 System.Object GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineLoadU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m801AB1F500A3A2998AD56254068EE4CFEF1AFC57 (void);
// 0x00000745 System.Void GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_Reset_m1C07BD4300758CD834338BDDECA0875E978F353E (void);
// 0x00000746 System.Object GameCreator.Core.SaveLoadManager/<CoroutineLoad>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_get_Current_m5193714FB64E1B8E2DD8DACEBC03DD5545EF39D2 (void);
// 0x00000747 System.String GameCreator.Core.IGameSave::GetUniqueName()
// 0x00000748 System.Type GameCreator.Core.IGameSave::GetSaveDataType()
// 0x00000749 System.Object GameCreator.Core.IGameSave::GetSaveData()
// 0x0000074A System.Void GameCreator.Core.IGameSave::ResetData()
// 0x0000074B System.Void GameCreator.Core.IGameSave::OnLoad(System.Object)
// 0x0000074C System.Void GameCreator.Core.ButtonActions::Awake()
extern void ButtonActions_Awake_m28B9AFCE9EF0B40805DE4C8B41449F555EC4E879 (void);
// 0x0000074D System.Void GameCreator.Core.ButtonActions::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ButtonActions_OnPointerClick_mACF4831B872F627240D5F797AB548021DBE902B4 (void);
// 0x0000074E System.Void GameCreator.Core.ButtonActions::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void ButtonActions_OnSubmit_m5CDB8500688B75363E87546F349A19CA438EAE84 (void);
// 0x0000074F System.Collections.IEnumerator GameCreator.Core.ButtonActions::OnFinishSubmit()
extern void ButtonActions_OnFinishSubmit_m913CD3FD4E3EDFF039CFE2655DE2FDA322EE6570 (void);
// 0x00000750 System.Void GameCreator.Core.ButtonActions::Press()
extern void ButtonActions_Press_m4E183732730A317BB189E11F861FE3771974FF1E (void);
// 0x00000751 System.Void GameCreator.Core.ButtonActions::.ctor()
extern void ButtonActions__ctor_m275325C2EF9197719BF51956948AAE2EDB84A846 (void);
// 0x00000752 System.Void GameCreator.Core.ButtonActions/ButtonActionsEvent::.ctor()
extern void ButtonActionsEvent__ctor_mD3DE58B33E07BB186ACC5BA699DB79434EF3BEB1 (void);
// 0x00000753 System.Void GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::.ctor(System.Int32)
extern void U3COnFinishSubmitU3Ed__6__ctor_m893E2EF3FB1B81650DC4D1CCFC1FCB37398F45EF (void);
// 0x00000754 System.Void GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::System.IDisposable.Dispose()
extern void U3COnFinishSubmitU3Ed__6_System_IDisposable_Dispose_m2B5E0BBD91BB5335BF4D0D72AD2B8E0D12ADEC7A (void);
// 0x00000755 System.Boolean GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::MoveNext()
extern void U3COnFinishSubmitU3Ed__6_MoveNext_m7306FA775148F6605FB68089AAE0E2BC9E65DB81 (void);
// 0x00000756 System.Object GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnFinishSubmitU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE497123EB4BC9F432B4072E2D058559767136CAB (void);
// 0x00000757 System.Void GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::System.Collections.IEnumerator.Reset()
extern void U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_Reset_mFB604330DB8D7CD8B5C768C549F2946B73F59377 (void);
// 0x00000758 System.Object GameCreator.Core.ButtonActions/<OnFinishSubmit>d__6::System.Collections.IEnumerator.get_Current()
extern void U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_get_Current_m43798DF5AB19460ECD104AF7F4F2FC05ADB14768 (void);
// 0x00000759 System.Void GameCreator.Core.InputFieldVariable::Awake()
extern void InputFieldVariable_Awake_m2E1C01DB29513E483A5E77EFF77E933CDC916B38 (void);
// 0x0000075A System.Void GameCreator.Core.InputFieldVariable::Start()
extern void InputFieldVariable_Start_mD58D164CD3590465F9BEF7FFBF25F5C8D54EBEE6 (void);
// 0x0000075B System.Void GameCreator.Core.InputFieldVariable::SyncVariable(System.String)
extern void InputFieldVariable_SyncVariable_m8EA9F48DE23DB5E1B6766B936A84960CFC24F2E4 (void);
// 0x0000075C System.Void GameCreator.Core.InputFieldVariable::.ctor()
extern void InputFieldVariable__ctor_mEE7899474D66E61CC848F506EEE3C2A75F24078D (void);
// 0x0000075D System.Void GameCreator.Core.SliderVariable::Awake()
extern void SliderVariable_Awake_m8927113CD1CB82F79F5086C809161CDCE6548114 (void);
// 0x0000075E System.Void GameCreator.Core.SliderVariable::Start()
extern void SliderVariable_Start_m6C826149AE41B2E6F6FEE369E81D68F2C64A9D11 (void);
// 0x0000075F System.Void GameCreator.Core.SliderVariable::OnDestroy()
extern void SliderVariable_OnDestroy_m4B2752A7157B6713BFB63B1183223F30F16A30EE (void);
// 0x00000760 System.Void GameCreator.Core.SliderVariable::SyncValueWithList(System.Int32,System.Object,System.Object)
extern void SliderVariable_SyncValueWithList_m9AAFECDDF26E307DE29D00C21E5532F09959CAA9 (void);
// 0x00000761 System.Void GameCreator.Core.SliderVariable::SyncValueWithVariable(System.String)
extern void SliderVariable_SyncValueWithVariable_mB0D4D529490FE0CBF2C2718FE4162C4FD1E0D8C5 (void);
// 0x00000762 System.Void GameCreator.Core.SliderVariable::SyncVariableWithValue(System.Single)
extern void SliderVariable_SyncVariableWithValue_mB5E43D8678F7D50CCF27A45D839C05298882405B (void);
// 0x00000763 System.Void GameCreator.Core.SliderVariable::.ctor()
extern void SliderVariable__ctor_mE966D89B209542F022D58537377DA0CC618CF864 (void);
// 0x00000764 System.Void GameCreator.Core.SliderVectorVariable::Awake()
extern void SliderVectorVariable_Awake_mDBBB2EA1AF2FFD99DD4DCA113DA3C0E6BDBAF181 (void);
// 0x00000765 System.Void GameCreator.Core.SliderVectorVariable::Start()
extern void SliderVectorVariable_Start_mBA2A7C3031B68683D0F51BBE7656FF0C2CF334ED (void);
// 0x00000766 System.Void GameCreator.Core.SliderVectorVariable::OnDestroy()
extern void SliderVectorVariable_OnDestroy_m2171F85359CB80079F8E070F7682EA6B40A89A8E (void);
// 0x00000767 System.Void GameCreator.Core.SliderVectorVariable::SyncValueWithList(System.Int32,System.Object,System.Object)
extern void SliderVectorVariable_SyncValueWithList_m4B6E1C129A2CF2AC245994DA01B41957D248BED1 (void);
// 0x00000768 System.Void GameCreator.Core.SliderVectorVariable::SyncValueWithVariable(System.String)
extern void SliderVectorVariable_SyncValueWithVariable_m3A1436991EDD2E26FEF5471671674DA7C0300F1E (void);
// 0x00000769 System.Void GameCreator.Core.SliderVectorVariable::SyncVariableWithValue(System.Single)
extern void SliderVectorVariable_SyncVariableWithValue_m788F0CC85D7450413ECAB7028AFCE6C666962798 (void);
// 0x0000076A System.Void GameCreator.Core.SliderVectorVariable::.ctor()
extern void SliderVectorVariable__ctor_m9BCCD8C22C781081AB82F2CB64F2BDAD5240569C (void);
// 0x0000076B System.Void GameCreator.Core.ToggleVariable::Awake()
extern void ToggleVariable_Awake_m05F078AB46B324163D72F71EFD35C23EBA70495E (void);
// 0x0000076C System.Void GameCreator.Core.ToggleVariable::Start()
extern void ToggleVariable_Start_m171C38256E49AE00CB5DC6AFC86812AC8CCD66DA (void);
// 0x0000076D System.Void GameCreator.Core.ToggleVariable::OnDestroy()
extern void ToggleVariable_OnDestroy_m11ECAB30572523226CD73C35C814B6D8A25166C5 (void);
// 0x0000076E System.Void GameCreator.Core.ToggleVariable::SyncValueWithVariable(System.String)
extern void ToggleVariable_SyncValueWithVariable_mA62EF9B208F1C10E6C5FC6E83683E90F413BC4FA (void);
// 0x0000076F System.Void GameCreator.Core.ToggleVariable::SyncValueWithList(System.Int32,System.Object,System.Object)
extern void ToggleVariable_SyncValueWithList_m6C919C3B5607DEF4427B9B8DD24360D042F1D472 (void);
// 0x00000770 System.Void GameCreator.Core.ToggleVariable::SyncVariableWithValue(System.Boolean)
extern void ToggleVariable_SyncVariableWithValue_mE176F524337B8612E7789E44100A8CEF5C7624AE (void);
// 0x00000771 System.Void GameCreator.Core.ToggleVariable::.ctor()
extern void ToggleVariable__ctor_mE6C568A08CF8AE4373A094FA1FFD8264B5533671 (void);
// 0x00000772 System.Void GameCreator.Core.VariableAttribute::.ctor()
extern void VariableAttribute__ctor_m023A5E63AF4BB4D95294769E98F5521EB0D3C42C (void);
// 0x00000773 System.String GameCreator.Core.VariableAttribute::GenerateRandomString()
extern void VariableAttribute_GenerateRandomString_m986D8E86AC82A8B060F45C49419DEEB6F6BCB93D (void);
// 0x00000774 System.Void GameCreator.Core.LocStringNoTextAttribute::.ctor()
extern void LocStringNoTextAttribute__ctor_m2F3042F3260B6BEB7DF68552F97EA5E96D8143FF (void);
// 0x00000775 System.Void GameCreator.Core.LocStringNoPostProcessAttribute::.ctor()
extern void LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D (void);
// 0x00000776 System.Void GameCreator.Core.LocStringBigTextAttribute::.ctor()
extern void LocStringBigTextAttribute__ctor_m476D7DD7EDB472B0B3CA92371824A8CD0CBC1937 (void);
// 0x00000777 System.Void GameCreator.Core.LocStringBigTextNoPostProcessAttribute::.ctor()
extern void LocStringBigTextNoPostProcessAttribute__ctor_m42D47E5B93D057A32E64D25E20EC56459C626D22 (void);
// 0x00000778 System.Void GameCreator.Core.RotationConstraintAttribute::.ctor()
extern void RotationConstraintAttribute__ctor_m67FAF4934092044140A0E4EDF2B2571EC3C1495C (void);
// 0x00000779 System.Void GameCreator.Core.TagSelectorAttribute::.ctor()
extern void TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE (void);
// 0x0000077A System.Void GameCreator.Core.LayerSelectorAttribute::.ctor()
extern void LayerSelectorAttribute__ctor_m3F5FD467B8F48AD2B1F6670B88E06E4ACDF5FF3C (void);
// 0x0000077B System.Void GameCreator.Core.IndentAttribute::.ctor()
extern void IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B (void);
// 0x0000077C System.Void GameCreator.Core.EventNameAttribute::.ctor()
extern void EventNameAttribute__ctor_m41852A61BDA5AB2D287649265057870A8DC92B15 (void);
// 0x0000077D System.Void GameCreator.Core.CanvasRenderMode::Awake()
extern void CanvasRenderMode_Awake_mE5B1DEEE1AF74F60AFC8469B055C7E81052A4DF0 (void);
// 0x0000077E System.Void GameCreator.Core.CanvasRenderMode::.ctor()
extern void CanvasRenderMode__ctor_mE5C8BD458C8EC4BDC1D5D58C1D31A9DAA679AF9C (void);
// 0x0000077F System.String GameCreator.Core.GlobalID::GetID()
extern void GlobalID_GetID_m7C7826203A2620A253CF8EC2D61A1CF3129BE815 (void);
// 0x00000780 System.Void GameCreator.Core.GlobalID::OnDestroyGID()
extern void GlobalID_OnDestroyGID_m023FFC94237D2E7E1778E07574D5DE440D65F48D (void);
// 0x00000781 System.Void GameCreator.Core.GlobalID::OnApplicationQuit()
extern void GlobalID_OnApplicationQuit_mBBA480D327619D7A4CF5FE624140A5CD0802D34B (void);
// 0x00000782 System.Void GameCreator.Core.GlobalID::CreateGuid()
extern void GlobalID_CreateGuid_m2E80738FC023E12802DEEF4C0C1E25F594D38587 (void);
// 0x00000783 System.Void GameCreator.Core.GlobalID::Awake()
extern void GlobalID_Awake_mCB8148C1106E870C9B9768AB810433A493020DBA (void);
// 0x00000784 System.Void GameCreator.Core.GlobalID::OnValidate()
extern void GlobalID_OnValidate_m1BEE450E20AF0D589887CC1EED0E8ADD1DA39D8A (void);
// 0x00000785 System.Void GameCreator.Core.GlobalID::.ctor()
extern void GlobalID__ctor_m019E03CDBDC66F3E3B7B9A265EE3E0B799FF3286 (void);
// 0x00000786 System.Void GameCreator.Core.TargetCharacter::.ctor()
extern void TargetCharacter__ctor_mC995B7FF2BB8BD2522E7ABEA7DF75B804F37499C (void);
// 0x00000787 System.Void GameCreator.Core.TargetCharacter::.ctor(GameCreator.Core.TargetCharacter/Target)
extern void TargetCharacter__ctor_m8B7B6983941F7F82A6EC3BEDB71CF6D90D00B506 (void);
// 0x00000788 GameCreator.Characters.Character GameCreator.Core.TargetCharacter::GetCharacter(UnityEngine.GameObject)
extern void TargetCharacter_GetCharacter_mC2843CBC87D87D3DBE9390C5552919A738A0C114 (void);
// 0x00000789 System.Void GameCreator.Core.TargetCharacter::StartListeningVariableChanges(UnityEngine.GameObject)
extern void TargetCharacter_StartListeningVariableChanges_mDFCACA2ECE5ECEBADD7B1873734397AFE311C53A (void);
// 0x0000078A System.Void GameCreator.Core.TargetCharacter::StopListeningVariableChanges(UnityEngine.GameObject)
extern void TargetCharacter_StopListeningVariableChanges_mDCC6629802D2480702188648B3E7E38C0F0D67EC (void);
// 0x0000078B System.Void GameCreator.Core.TargetCharacter::OnChangeVariable(System.String)
extern void TargetCharacter_OnChangeVariable_m9B7356AF4A4A584ED45F9FF1E4DEBECAA10F2197 (void);
// 0x0000078C System.Void GameCreator.Core.TargetCharacter::OnChangeVariable(System.Int32,System.Object,System.Object)
extern void TargetCharacter_OnChangeVariable_mF5B8C3E8F57CA63F595CC2CDB9F31C70CBA98F7B (void);
// 0x0000078D System.String GameCreator.Core.TargetCharacter::ToString()
extern void TargetCharacter_ToString_mC05B1D08D37A34FACC8B142A9009BD445F700052 (void);
// 0x0000078E System.Void GameCreator.Core.TargetCharacter/ChangeEvent::.ctor()
extern void ChangeEvent__ctor_m48B36645B6B130B5B815295E439741090190B263 (void);
// 0x0000078F System.Void GameCreator.Core.TargetDirection::.ctor()
extern void TargetDirection__ctor_m4B0CD8400860168AA6D11373048351BAD4DD75B6 (void);
// 0x00000790 System.Void GameCreator.Core.TargetDirection::.ctor(GameCreator.Core.TargetDirection/Target)
extern void TargetDirection__ctor_mDD8A3A7392B1C0374A60AE80809A7593CCDCF467 (void);
// 0x00000791 UnityEngine.Vector3 GameCreator.Core.TargetDirection::GetDirection(UnityEngine.GameObject,UnityEngine.Space)
extern void TargetDirection_GetDirection_m7081CE5BE371125AC0347D34F7089DD9D0076325 (void);
// 0x00000792 System.String GameCreator.Core.TargetDirection::ToString()
extern void TargetDirection_ToString_mFA790A5C284101B13BB1AAD92792DCA228013830 (void);
// 0x00000793 System.Void GameCreator.Core.TargetGameObject::.ctor()
extern void TargetGameObject__ctor_mC7D13DF596BBEF1F5FFFFB75E47AC4C614F2C241 (void);
// 0x00000794 System.Void GameCreator.Core.TargetGameObject::.ctor(GameCreator.Core.TargetGameObject/Target)
extern void TargetGameObject__ctor_m742A24BC70C3F8FFD18A8BC0D3BF76B5E54B3CEE (void);
// 0x00000795 UnityEngine.GameObject GameCreator.Core.TargetGameObject::GetGameObject(UnityEngine.GameObject)
extern void TargetGameObject_GetGameObject_m24C7B5DCE02C07EAAB5552F4102F05C91347D3E6 (void);
// 0x00000796 UnityEngine.Transform GameCreator.Core.TargetGameObject::GetTransform(UnityEngine.GameObject)
extern void TargetGameObject_GetTransform_m24E7036C334853515E050F49CA513C9F2D7F4C04 (void);
// 0x00000797 T GameCreator.Core.TargetGameObject::GetComponent(UnityEngine.GameObject)
// 0x00000798 System.Object GameCreator.Core.TargetGameObject::GetComponent(UnityEngine.GameObject,System.String)
extern void TargetGameObject_GetComponent_mFB6C4F7CA82D2369F40A04B690BC19ECE17E8ACD (void);
// 0x00000799 T GameCreator.Core.TargetGameObject::GetComponentInChildren(UnityEngine.GameObject)
// 0x0000079A T[] GameCreator.Core.TargetGameObject::GetComponentsInChildren(UnityEngine.GameObject)
// 0x0000079B System.Void GameCreator.Core.TargetGameObject::StartListeningVariableChanges(UnityEngine.GameObject)
extern void TargetGameObject_StartListeningVariableChanges_mF3770C6C822874C2E4D836386889A36E1B0E6689 (void);
// 0x0000079C System.Void GameCreator.Core.TargetGameObject::StopListeningVariableChanges(UnityEngine.GameObject)
extern void TargetGameObject_StopListeningVariableChanges_m0FE7CCCF1ABE2B8B12BA9B5C98E050ED5A93A409 (void);
// 0x0000079D System.Void GameCreator.Core.TargetGameObject::OnChangeVariable(System.String)
extern void TargetGameObject_OnChangeVariable_mFD31393A94437738EC1F3A610830A1A6C98E9A45 (void);
// 0x0000079E System.Void GameCreator.Core.TargetGameObject::OnChangeVariable(System.Int32,System.Object,System.Object)
extern void TargetGameObject_OnChangeVariable_mC4CA25EEA8FEE0057EA2F2A140F24BE4CF3324D1 (void);
// 0x0000079F System.String GameCreator.Core.TargetGameObject::ToString()
extern void TargetGameObject_ToString_m82AE2007064B46C1FD411B805BD276DA9E592FA5 (void);
// 0x000007A0 System.Void GameCreator.Core.TargetGameObject/ChangeEvent::.ctor()
extern void ChangeEvent__ctor_mA4F2D27E8473D5C1D3B323B247121FAED5FAB708 (void);
// 0x000007A1 System.Void GameCreator.Core.TargetPosition::.ctor()
extern void TargetPosition__ctor_m2160D8C67055C072334E65F8A4393E657C597C1D (void);
// 0x000007A2 System.Void GameCreator.Core.TargetPosition::.ctor(GameCreator.Core.TargetPosition/Target)
extern void TargetPosition__ctor_mD1D16AD569EFC8A8F6BFBFC27DE4DD4C475F6361 (void);
// 0x000007A3 UnityEngine.Vector3 GameCreator.Core.TargetPosition::GetPosition(UnityEngine.GameObject,UnityEngine.Space)
extern void TargetPosition_GetPosition_m637CE2ED4CD257B9D37F271D70CEF28975A51C6F (void);
// 0x000007A4 UnityEngine.Quaternion GameCreator.Core.TargetPosition::GetRotation(UnityEngine.GameObject)
extern void TargetPosition_GetRotation_m41F057CE3BA9C7E580E2FBB747EE5CE7B7C5B280 (void);
// 0x000007A5 System.String GameCreator.Core.TargetPosition::ToString()
extern void TargetPosition_ToString_m9674087D39FDF0ACAD16CD8D711EE426A47BD93E (void);
// 0x000007A6 System.Void GameCreator.Core.UniqueInstanceID::.ctor()
extern void UniqueInstanceID__ctor_mDF4F68AFEFDAA7B80BD3030AB1FAD5EFA2BCAE85 (void);
// 0x000007A7 System.Void GameCreator.Core.CoroutinesManager::.ctor()
extern void CoroutinesManager__ctor_m166D269EA0893785D76034A5D3A01932306992B4 (void);
// 0x000007A8 System.Single GameCreator.Core.Easing::GetEase(GameCreator.Core.Easing/EaseType,System.Single,System.Single,System.Single)
extern void Easing_GetEase_m57FBBD4072E8C52FC3D037FC376BD0456695CF26 (void);
// 0x000007A9 System.Single GameCreator.Core.Easing::Lerp(System.Single,System.Single,System.Single)
extern void Easing_Lerp_m7A0A1B3073CFCBFCBA5FBC5985BBA889CFABD04C (void);
// 0x000007AA System.Single GameCreator.Core.Easing::QuadIn(System.Single,System.Single,System.Single)
extern void Easing_QuadIn_m5F1EED3339FA9129ED4CFB5B47155FC7479906B0 (void);
// 0x000007AB System.Single GameCreator.Core.Easing::QuadOut(System.Single,System.Single,System.Single)
extern void Easing_QuadOut_mFDB2772A277E6DFC396B748FF0635DC9C2E318E7 (void);
// 0x000007AC System.Single GameCreator.Core.Easing::QuadInOut(System.Single,System.Single,System.Single)
extern void Easing_QuadInOut_m8A38B5641950362CD870898A32D71629DFB8CC84 (void);
// 0x000007AD System.Single GameCreator.Core.Easing::QuadOutIn(System.Single,System.Single,System.Single)
extern void Easing_QuadOutIn_mE47F0ED7B4E78EFD55F8B49E0BA211F5940C38DD (void);
// 0x000007AE System.Single GameCreator.Core.Easing::CubicIn(System.Single,System.Single,System.Single)
extern void Easing_CubicIn_mED2F74C4DCBFB56B24B6031292CF7DA9DA69B9C4 (void);
// 0x000007AF System.Single GameCreator.Core.Easing::CubicOut(System.Single,System.Single,System.Single)
extern void Easing_CubicOut_mF47F96B153CDC83960AE53CBF4E84DE1307366F4 (void);
// 0x000007B0 System.Single GameCreator.Core.Easing::CubicInOut(System.Single,System.Single,System.Single)
extern void Easing_CubicInOut_mFACFEC02C9C847C10E481F9BD2997C066E9BF44A (void);
// 0x000007B1 System.Single GameCreator.Core.Easing::CubicOutIn(System.Single,System.Single,System.Single)
extern void Easing_CubicOutIn_m82829045838CD5D881B8B5A5D7B3E64552D1355A (void);
// 0x000007B2 System.Single GameCreator.Core.Easing::QuartIn(System.Single,System.Single,System.Single)
extern void Easing_QuartIn_mFEB9BDD01CB20773D8290DFE8B759F8ADB505A72 (void);
// 0x000007B3 System.Single GameCreator.Core.Easing::QuartOut(System.Single,System.Single,System.Single)
extern void Easing_QuartOut_m8FA852389F119071937BB280AAE5D3E8D2F004B5 (void);
// 0x000007B4 System.Single GameCreator.Core.Easing::QuartInOut(System.Single,System.Single,System.Single)
extern void Easing_QuartInOut_mAC2EA9B95E19E45C135A79555D59683A32B98390 (void);
// 0x000007B5 System.Single GameCreator.Core.Easing::QuartOutIn(System.Single,System.Single,System.Single)
extern void Easing_QuartOutIn_mC61F2EBE301DDE65E1C875A29E7AA3FE3002DB80 (void);
// 0x000007B6 System.Single GameCreator.Core.Easing::QuintIn(System.Single,System.Single,System.Single)
extern void Easing_QuintIn_mDA7CA0638892A334208D4C28BB371DCE3CA8C877 (void);
// 0x000007B7 System.Single GameCreator.Core.Easing::QuintOut(System.Single,System.Single,System.Single)
extern void Easing_QuintOut_m0EA804634345B183AA3BF433D86C7653E3418561 (void);
// 0x000007B8 System.Single GameCreator.Core.Easing::QuintInOut(System.Single,System.Single,System.Single)
extern void Easing_QuintInOut_m8AC49CBF4F1264EE27CA383737DDBD733FD1A27E (void);
// 0x000007B9 System.Single GameCreator.Core.Easing::QuintOutIn(System.Single,System.Single,System.Single)
extern void Easing_QuintOutIn_m8F5A3CC1D0A327F88EC63E768E2E15F12C57FA99 (void);
// 0x000007BA System.Single GameCreator.Core.Easing::SineIn(System.Single,System.Single,System.Single)
extern void Easing_SineIn_mCC4AEE5DD769983001F1E5DED5ED956B114FC749 (void);
// 0x000007BB System.Single GameCreator.Core.Easing::SineOut(System.Single,System.Single,System.Single)
extern void Easing_SineOut_mB6997547FD3F109B0ACD2A111241F6240FD34762 (void);
// 0x000007BC System.Single GameCreator.Core.Easing::SineInOut(System.Single,System.Single,System.Single)
extern void Easing_SineInOut_mF6608B1869E96FEBE7833314838D238A31E2314D (void);
// 0x000007BD System.Single GameCreator.Core.Easing::SineOutIn(System.Single,System.Single,System.Single)
extern void Easing_SineOutIn_mE8DD82F83361BB08381F2B7B53C30C739C58395B (void);
// 0x000007BE System.Single GameCreator.Core.Easing::ExpoIn(System.Single,System.Single,System.Single)
extern void Easing_ExpoIn_m271C943BA165B846A92E6F7D0B061480E41318E6 (void);
// 0x000007BF System.Single GameCreator.Core.Easing::ExpoOut(System.Single,System.Single,System.Single)
extern void Easing_ExpoOut_m7D8EEB401FF7E69F79C2148E73FC2607DC0D218E (void);
// 0x000007C0 System.Single GameCreator.Core.Easing::ExpoInOut(System.Single,System.Single,System.Single)
extern void Easing_ExpoInOut_m4F6D554F14C073FEA759988FCBB9E6E33643AF43 (void);
// 0x000007C1 System.Single GameCreator.Core.Easing::ExpoOutIn(System.Single,System.Single,System.Single)
extern void Easing_ExpoOutIn_m97B1A24B86AE76FAF2FA072D28B5B7C08B659CD3 (void);
// 0x000007C2 System.Single GameCreator.Core.Easing::CircIn(System.Single,System.Single,System.Single)
extern void Easing_CircIn_mED7F2377FCBBAF8E707B7467AE12987A1EBA6E77 (void);
// 0x000007C3 System.Single GameCreator.Core.Easing::CircOut(System.Single,System.Single,System.Single)
extern void Easing_CircOut_m543BEE25BE82D74B746A1E72E4A1A3C81201DEC5 (void);
// 0x000007C4 System.Single GameCreator.Core.Easing::CircInOut(System.Single,System.Single,System.Single)
extern void Easing_CircInOut_mB958C983AB03FB7B47D40C2E26E69F77ADCFED7F (void);
// 0x000007C5 System.Single GameCreator.Core.Easing::CircOutIn(System.Single,System.Single,System.Single)
extern void Easing_CircOutIn_m58CA477CB966F7C3A5E38F1FFD5DB8DFFE1E5785 (void);
// 0x000007C6 System.Single GameCreator.Core.Easing::ElasticIn(System.Single,System.Single,System.Single)
extern void Easing_ElasticIn_mCD43F6E8BB260E553C6C81DAF10DEFA08DEB1D04 (void);
// 0x000007C7 System.Single GameCreator.Core.Easing::ElasticOut(System.Single,System.Single,System.Single)
extern void Easing_ElasticOut_m02C3B4C6F3A937AE7C7932F0D191ADA0E6D9FE83 (void);
// 0x000007C8 System.Single GameCreator.Core.Easing::ElasticInOut(System.Single,System.Single,System.Single)
extern void Easing_ElasticInOut_mAE99A3948EE8FADCE38F264E50BCB7194935EDF5 (void);
// 0x000007C9 System.Single GameCreator.Core.Easing::ElasticOutIn(System.Single,System.Single,System.Single)
extern void Easing_ElasticOutIn_mB51988F99C50FC82181F9A665AB19A361F51780B (void);
// 0x000007CA System.Single GameCreator.Core.Easing::BackIn(System.Single,System.Single,System.Single)
extern void Easing_BackIn_mE040BDCAF71E215003D5206C26A8C72E0AE3D9BC (void);
// 0x000007CB System.Single GameCreator.Core.Easing::BackOut(System.Single,System.Single,System.Single)
extern void Easing_BackOut_m857C043D5ED508D5E7AD5C7E522059BD46B602E0 (void);
// 0x000007CC System.Single GameCreator.Core.Easing::BackInOut(System.Single,System.Single,System.Single)
extern void Easing_BackInOut_m2FF2C72C4A18B32870524EBAAD3A3851BD615281 (void);
// 0x000007CD System.Single GameCreator.Core.Easing::BackOutIn(System.Single,System.Single,System.Single)
extern void Easing_BackOutIn_m191FF37A08502698110536C5E3EB44D015B910D5 (void);
// 0x000007CE System.Single GameCreator.Core.Easing::BounceIn(System.Single,System.Single,System.Single)
extern void Easing_BounceIn_m36761C816842613BFE145024C8116EFDE95D62C4 (void);
// 0x000007CF System.Single GameCreator.Core.Easing::BounceOut(System.Single,System.Single,System.Single)
extern void Easing_BounceOut_m623FFA5DE68F1109E18118FCDF499CEFF8313BFA (void);
// 0x000007D0 System.Single GameCreator.Core.Easing::BounceInOut(System.Single,System.Single,System.Single)
extern void Easing_BounceInOut_mA72EC95BF02707FB1D879B2FFEE3394A93F1E507 (void);
// 0x000007D1 System.Single GameCreator.Core.Easing::BounceOutIn(System.Single,System.Single,System.Single)
extern void Easing_BounceOutIn_mA7CA86DDA0203ACF1D0DF14188C51E6EB917D5AC (void);
// 0x000007D2 System.Single GameCreator.Core.Easing::In(System.Func`3<System.Single,System.Single,System.Single>,System.Single,System.Single,System.Single,System.Single)
extern void Easing_In_m335607C44528050C930CBF3B2FF92A24589836BA (void);
// 0x000007D3 System.Single GameCreator.Core.Easing::Out(System.Func`3<System.Single,System.Single,System.Single>,System.Single,System.Single,System.Single,System.Single)
extern void Easing_Out_m43E5F01F0DEF94F1283AAE098868EE32FB140D75 (void);
// 0x000007D4 System.Single GameCreator.Core.Easing::InOut(System.Func`3<System.Single,System.Single,System.Single>,System.Single,System.Single,System.Single,System.Single)
extern void Easing_InOut_m0D440931FC5A173A02ADFFD3D44A8C5F16DA0189 (void);
// 0x000007D5 System.Single GameCreator.Core.Easing::OutIn(System.Func`3<System.Single,System.Single,System.Single>,System.Single,System.Single,System.Single,System.Single)
extern void Easing_OutIn_m5879B12AF2797D65A292287D4345E1523C256859 (void);
// 0x000007D6 System.Single GameCreator.Core.Easing::Linear(System.Single,System.Single)
extern void Easing_Linear_m73409BE556E33EA0E947BE22959239B6D596E8F1 (void);
// 0x000007D7 System.Single GameCreator.Core.Easing::Quad(System.Single,System.Single)
extern void Easing_Quad_m8FCED52E357129D5E005DC04E507139E2EA51514 (void);
// 0x000007D8 System.Single GameCreator.Core.Easing::Cubic(System.Single,System.Single)
extern void Easing_Cubic_mB31C34DBD48DD330CCE0E335A4016E368333E189 (void);
// 0x000007D9 System.Single GameCreator.Core.Easing::Quart(System.Single,System.Single)
extern void Easing_Quart_m038CFF8B9CC5121D61F9C70422C75ED78873F4D5 (void);
// 0x000007DA System.Single GameCreator.Core.Easing::Quint(System.Single,System.Single)
extern void Easing_Quint_m4D317BC3CB4A6CAB38C3B1538C45A02923F61483 (void);
// 0x000007DB System.Single GameCreator.Core.Easing::Sine(System.Single,System.Single)
extern void Easing_Sine_m1EF03D30BA9EFCA71331C763E8C2FFFFA0F410A8 (void);
// 0x000007DC System.Single GameCreator.Core.Easing::Expo(System.Single,System.Single)
extern void Easing_Expo_m8858D435AFF2F216E5D5389A51029BF25BC204E6 (void);
// 0x000007DD System.Single GameCreator.Core.Easing::Circ(System.Single,System.Single)
extern void Easing_Circ_m0A62AD4F5E066752C62B0DF9A6E2C71144EAB846 (void);
// 0x000007DE System.Single GameCreator.Core.Easing::Elastic(System.Single,System.Single)
extern void Easing_Elastic_mCDE7BDF98CF51464CE5887E14F7E8494D094257E (void);
// 0x000007DF System.Single GameCreator.Core.Easing::Back(System.Single,System.Single)
extern void Easing_Back_m5C065BC75E150B8600754D7F01E6E4262440448F (void);
// 0x000007E0 System.Single GameCreator.Core.Easing::Bounce(System.Single,System.Single)
extern void Easing_Bounce_m148CBA7C3AF3E3930E8AD71501421EEE8BB5CA96 (void);
// 0x000007E1 System.Void GameCreator.Core.EventDispatchManager::OnCreate()
extern void EventDispatchManager_OnCreate_m74D61544F98EF87FDEE03A270AB48478A7130679 (void);
// 0x000007E2 System.Void GameCreator.Core.EventDispatchManager::Dispatch(System.String,UnityEngine.GameObject)
extern void EventDispatchManager_Dispatch_m4C9CCE465CDAEB22253976275238522434380305 (void);
// 0x000007E3 System.Void GameCreator.Core.EventDispatchManager::Subscribe(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>)
extern void EventDispatchManager_Subscribe_m2013731FFA13D6D5FD36776720E381566B6FE12F (void);
// 0x000007E4 System.Void GameCreator.Core.EventDispatchManager::Unsubscribe(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>)
extern void EventDispatchManager_Unsubscribe_mA81C0C8F371B9D8A25951475F104C23F0AA6AA78 (void);
// 0x000007E5 System.String[] GameCreator.Core.EventDispatchManager::GetSubscribedKeys()
extern void EventDispatchManager_GetSubscribedKeys_m9523AE4C57D9E3622D4EC647877FEA8E8FC3CC85 (void);
// 0x000007E6 System.Void GameCreator.Core.EventDispatchManager::RequireInit(System.String&)
extern void EventDispatchManager_RequireInit_m5BE08EAF970F3F144910EE8FFCEC8E8673D2DACE (void);
// 0x000007E7 System.Void GameCreator.Core.EventDispatchManager::.ctor()
extern void EventDispatchManager__ctor_m7B01F8905DDE249D1DE62003986D56C3F8E927EB (void);
// 0x000007E8 System.Void GameCreator.Core.EventDispatchManager/Dispatcher::.ctor()
extern void Dispatcher__ctor_m0058A590B1E8D012DCEDA064E1608B70E0F98E79 (void);
// 0x000007E9 System.Void GameCreator.Core.EventSystemManager::OnCreate()
extern void EventSystemManager_OnCreate_mC9C237BF2EC97536ED4651CF002AFF4834321275 (void);
// 0x000007EA System.Void GameCreator.Core.EventSystemManager::Wakeup()
extern void EventSystemManager_Wakeup_m3A671F4AFF31179CC18B96F4ACF68F792EAB9148 (void);
// 0x000007EB UnityEngine.GameObject GameCreator.Core.EventSystemManager::GetPointerGameObject(System.Int32)
extern void EventSystemManager_GetPointerGameObject_m7CE7D3717B16D1C5B067AB60152393E05FF9DEAA (void);
// 0x000007EC System.Boolean GameCreator.Core.EventSystemManager::IsPointerOverUI(System.Int32)
extern void EventSystemManager_IsPointerOverUI_mE8B61CECA53D9D6E79F02013EC33A744ACC810A9 (void);
// 0x000007ED System.Void GameCreator.Core.EventSystemManager::OnSceneLoad(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void EventSystemManager_OnSceneLoad_mB8B4DB6D5A75066BC5A8445EEB4180E36854FA4E (void);
// 0x000007EE System.Void GameCreator.Core.EventSystemManager::RequireInit()
extern void EventSystemManager_RequireInit_mA707A6B0575AF2EBA7C47FB7243332266CE6C32D (void);
// 0x000007EF System.Void GameCreator.Core.EventSystemManager::RequireCamera()
extern void EventSystemManager_RequireCamera_m8E97F5AC18FD54EF80210BAEB204153746869C6A (void);
// 0x000007F0 System.Void GameCreator.Core.EventSystemManager::.ctor()
extern void EventSystemManager__ctor_m4DAE8F7A7A02CFED3794542A95FB1CB2827BBDE5 (void);
// 0x000007F1 UnityEngine.GameObject GameCreator.Core.GameCreatorStandaloneInputModule::GameObjectUnderPointer(System.Int32)
extern void GameCreatorStandaloneInputModule_GameObjectUnderPointer_m06517611C25F7B816A9379CD5F096FF59C1BEE84 (void);
// 0x000007F2 UnityEngine.GameObject GameCreator.Core.GameCreatorStandaloneInputModule::GameObjectUnderPointer()
extern void GameCreatorStandaloneInputModule_GameObjectUnderPointer_m14836EC0230548BE8FC4F0806EF629EED74722ED (void);
// 0x000007F3 System.Void GameCreator.Core.GameCreatorStandaloneInputModule::.ctor()
extern void GameCreatorStandaloneInputModule__ctor_m32B6723949D77EAC60747DEEF530897EFF4A34FE (void);
// 0x000007F4 System.Single GameCreator.Core.ExpressionEvaluator::Evaluate(System.String)
extern void ExpressionEvaluator_Evaluate_mF21D454D7897339A46F658B59428D486CC35ADBC (void);
// 0x000007F5 System.Int32 GameCreator.Core.SerializableDictionaryBase`2::get_Count()
// 0x000007F6 System.Void GameCreator.Core.SerializableDictionaryBase`2::Add(TKey,TValue)
// 0x000007F7 System.Boolean GameCreator.Core.SerializableDictionaryBase`2::ContainsKey(TKey)
// 0x000007F8 System.Collections.Generic.ICollection`1<TKey> GameCreator.Core.SerializableDictionaryBase`2::get_Keys()
// 0x000007F9 System.Boolean GameCreator.Core.SerializableDictionaryBase`2::Remove(TKey)
// 0x000007FA System.Boolean GameCreator.Core.SerializableDictionaryBase`2::TryGetValue(TKey,TValue&)
// 0x000007FB System.Collections.Generic.ICollection`1<TValue> GameCreator.Core.SerializableDictionaryBase`2::get_Values()
// 0x000007FC TValue GameCreator.Core.SerializableDictionaryBase`2::get_Item(TKey)
// 0x000007FD System.Void GameCreator.Core.SerializableDictionaryBase`2::set_Item(TKey,TValue)
// 0x000007FE System.Void GameCreator.Core.SerializableDictionaryBase`2::Clear()
// 0x000007FF System.Void GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000800 System.Boolean GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000801 System.Void GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000802 System.Boolean GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000803 System.Boolean GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
// 0x00000804 System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> GameCreator.Core.SerializableDictionaryBase`2::GetEnumerator()
// 0x00000805 System.Collections.IEnumerator GameCreator.Core.SerializableDictionaryBase`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000806 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> GameCreator.Core.SerializableDictionaryBase`2::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
// 0x00000807 System.Void GameCreator.Core.SerializableDictionaryBase`2::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
// 0x00000808 System.Void GameCreator.Core.SerializableDictionaryBase`2::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
// 0x00000809 System.Void GameCreator.Core.SerializableDictionaryBase`2::.ctor()
// 0x0000080A System.Boolean GameCreator.Core.Singleton`1::get_isExiting()
// 0x0000080B T GameCreator.Core.Singleton`1::get_Instance()
// 0x0000080C System.Void GameCreator.Core.Singleton`1::OnCreate()
// 0x0000080D System.Void GameCreator.Core.Singleton`1::WakeUp()
// 0x0000080E System.Boolean GameCreator.Core.Singleton`1::ShouldNotDestroyOnLoad()
// 0x0000080F System.Void GameCreator.Core.Singleton`1::OnApplicationQuit()
// 0x00000810 System.Void GameCreator.Core.Singleton`1::OnDestroy()
// 0x00000811 System.Void GameCreator.Core.Singleton`1::DebugLogFormat(System.String,System.Object[])
// 0x00000812 System.Void GameCreator.Core.Singleton`1::.ctor()
// 0x00000813 System.Void GameCreator.Core.Singleton`1::.cctor()
// 0x00000814 System.Void GameCreator.Core.TimeManager::SetTimeScale(System.Single,System.Int32)
extern void TimeManager_SetTimeScale_mEEDE46B57F1F17E3F4D8B72B3938FBA50716DCBD (void);
// 0x00000815 System.Void GameCreator.Core.TimeManager::SetSmoothTimeScale(System.Single,System.Single,System.Int32)
extern void TimeManager_SetSmoothTimeScale_m38FFB09FC7D5574F007740A251FD0C45908AECA9 (void);
// 0x00000816 System.Void GameCreator.Core.TimeManager::Update()
extern void TimeManager_Update_m1D7F0ECB60DA6FB28915EDB89BB6ECCC56922987 (void);
// 0x00000817 System.Void GameCreator.Core.TimeManager::RecalculateTimeScale()
extern void TimeManager_RecalculateTimeScale_m906E7EF6D3DD70AB1A88314E2069BFFA81FAEFAA (void);
// 0x00000818 System.Void GameCreator.Core.TimeManager::.ctor()
extern void TimeManager__ctor_mC51F648957C62375EB611515037E69D4D4901188 (void);
// 0x00000819 System.Void GameCreator.Core.TimeManager/TimeData::.ctor(System.Single,System.Single,System.Single)
extern void TimeData__ctor_m04B54313A8A18140DE2425198C911D28E64A6CC2 (void);
// 0x0000081A System.Single GameCreator.Core.TimeManager/TimeData::Get()
extern void TimeData_Get_m8A8C30CCE05B6580B5C0107D564053A91FE6738D (void);
// 0x0000081B GameCreator.Core.DatabaseGeneral GameCreator.Core.DatabaseGeneral::Load()
extern void DatabaseGeneral_Load_mBEF0664FB6E5B775A92B5E1855FE8585140CC832 (void);
// 0x0000081C GameCreator.Core.IDataProvider GameCreator.Core.DatabaseGeneral::GetDataProvider()
extern void DatabaseGeneral_GetDataProvider_mA2DB709EFB601C0992D3D51EF1F3E3418E1A55EA (void);
// 0x0000081D System.Void GameCreator.Core.DatabaseGeneral::ChangeDataProvider(GameCreator.Core.IDataProvider)
extern void DatabaseGeneral_ChangeDataProvider_m57A8EC10AE4959E9FF6C8740E931FBBDD069496A (void);
// 0x0000081E System.Void GameCreator.Core.DatabaseGeneral::.ctor()
extern void DatabaseGeneral__ctor_m4AD2C32C56B039D2820E63F1446B04229F10DFB6 (void);
// 0x0000081F T GameCreator.Core.IDatabase::LoadDatabaseCopy()
// 0x00000820 T GameCreator.Core.IDatabase::LoadDatabase(System.Boolean)
// 0x00000821 System.String GameCreator.Core.IDatabase::GetAssetFilename(System.Type,System.Boolean)
extern void IDatabase_GetAssetFilename_mCA6BBC257EDD36B35C934B9F487283E4A95534B9 (void);
// 0x00000822 System.Void GameCreator.Core.IDatabase::.ctor()
extern void IDatabase__ctor_mEF4097D1D76A55127E0899C684E7F2CB32B56422 (void);
// 0x00000823 GameCreator.Core.DatabaseQuickstart GameCreator.Core.DatabaseQuickstart::Load()
extern void DatabaseQuickstart_Load_m73A9F0104B54F6B45E70D9BECB996795BF8C9757 (void);
// 0x00000824 System.Void GameCreator.Core.DatabaseQuickstart::.ctor()
extern void DatabaseQuickstart__ctor_mAF60125F5AD3D8E4696EB651A2A4B12146165415 (void);
// 0x00000825 System.Boolean GameCreator.Core.Clause::CheckConditions(UnityEngine.GameObject,System.Object[])
extern void Clause_CheckConditions_mC801467291AC7571A50985555DB38737062BC8F7 (void);
// 0x00000826 System.Void GameCreator.Core.Clause::ExecuteActions(UnityEngine.GameObject,System.Object[])
extern void Clause_ExecuteActions_m0CFB6797D8E897ACD15B1E4097B5CCFF6C06468B (void);
// 0x00000827 System.Void GameCreator.Core.Clause::.ctor()
extern void Clause__ctor_m76021F8997EE55C562A9F516A40F0B8A801F2010 (void);
// 0x00000828 System.Boolean GameCreator.Core.IAction::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void IAction_InstantExecute_m1F07FB6A41C0EC6596C4428A770F4E6FB35884AC (void);
// 0x00000829 System.Boolean GameCreator.Core.IAction::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32,System.Object[])
extern void IAction_InstantExecute_m512502AA6B9A94C8B5563F89D270D82275BD61C6 (void);
// 0x0000082A System.Collections.IEnumerator GameCreator.Core.IAction::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void IAction_Execute_mC7BC452CD9CEB42F6B5FF76A1653C0318E4E7F9F (void);
// 0x0000082B System.Collections.IEnumerator GameCreator.Core.IAction::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32,System.Object[])
extern void IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE (void);
// 0x0000082C System.Void GameCreator.Core.IAction::Stop()
extern void IAction_Stop_m732479EECFF9D88E69F63A107C677E2562680EBF (void);
// 0x0000082D System.Void GameCreator.Core.IAction::.ctor()
extern void IAction__ctor_m02069D61821B3EB24BE1C7331C3CA538A539CFDB (void);
// 0x0000082E System.Void GameCreator.Core.IAction/<Execute>d__2::.ctor(System.Int32)
extern void U3CExecuteU3Ed__2__ctor_m7DDC9C9F4F6271CBC115EB695C09CD7020514C91 (void);
// 0x0000082F System.Void GameCreator.Core.IAction/<Execute>d__2::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__2_System_IDisposable_Dispose_mCE34EB7AF956AF35D260652DAA9487875F5E5E18 (void);
// 0x00000830 System.Boolean GameCreator.Core.IAction/<Execute>d__2::MoveNext()
extern void U3CExecuteU3Ed__2_MoveNext_m81F513C5865780038792ACB85B6CC9B71655A682 (void);
// 0x00000831 System.Object GameCreator.Core.IAction/<Execute>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD451C8F90AB42372F393CA15C9D4C30110852768 (void);
// 0x00000832 System.Void GameCreator.Core.IAction/<Execute>d__2::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_m15914DD547004FBF1D2595DB46564433345D5090 (void);
// 0x00000833 System.Object GameCreator.Core.IAction/<Execute>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m9F64445ABE6D9783AD5D9537B7C8AAA82798ACEF (void);
// 0x00000834 System.Void GameCreator.Core.IAction/<Execute>d__3::.ctor(System.Int32)
extern void U3CExecuteU3Ed__3__ctor_mC5BAFE1C6CE5BF49D9685E6382F0D67665BA1271 (void);
// 0x00000835 System.Void GameCreator.Core.IAction/<Execute>d__3::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__3_System_IDisposable_Dispose_mD044F79A0BF7913EA3A7F7D88B63DC0F6676553E (void);
// 0x00000836 System.Boolean GameCreator.Core.IAction/<Execute>d__3::MoveNext()
extern void U3CExecuteU3Ed__3_MoveNext_m07CF32E1EB7087D03829F8556B04DE85299105AD (void);
// 0x00000837 System.Object GameCreator.Core.IAction/<Execute>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD2403DC5621D9B93CD571F03569AE0A49546A16 (void);
// 0x00000838 System.Void GameCreator.Core.IAction/<Execute>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mB8CD42CE43D0BE19240A03A82805AAE7BDB19368 (void);
// 0x00000839 System.Object GameCreator.Core.IAction/<Execute>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m0810B2FE029F9C067DCCFD795ADD587BC8E9B744 (void);
// 0x0000083A System.Void GameCreator.Core.IActionsList::Execute(UnityEngine.GameObject,System.Action,System.Object[])
extern void IActionsList_Execute_m58A609A93DAF9BB26DDB4996AC23A068450D6D6B (void);
// 0x0000083B System.Collections.IEnumerator GameCreator.Core.IActionsList::ExecuteCoroutine(UnityEngine.GameObject,System.Action,System.Object[])
extern void IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE (void);
// 0x0000083C System.Void GameCreator.Core.IActionsList::Cancel()
extern void IActionsList_Cancel_m28D90B6CBA72AA42140181F99C9789D5654D9D87 (void);
// 0x0000083D System.Void GameCreator.Core.IActionsList::Stop()
extern void IActionsList_Stop_m999FB5131D73E7A06DB651D44E28A1F0A5CD14CE (void);
// 0x0000083E System.Void GameCreator.Core.IActionsList::.ctor()
extern void IActionsList__ctor_mCD8BCE27505BAEF7F3D9529268F68304ADAA6318 (void);
// 0x0000083F UnityEngine.Coroutine GameCreator.Core.IActionsList/ActionCoroutine::get_coroutine()
extern void ActionCoroutine_get_coroutine_m43BF4DBCBD32FA5208B4AC6690372328638225F4 (void);
// 0x00000840 System.Void GameCreator.Core.IActionsList/ActionCoroutine::set_coroutine(UnityEngine.Coroutine)
extern void ActionCoroutine_set_coroutine_m84480EA36BEBF51030B497DC4EC7E7CDC3C93F64 (void);
// 0x00000841 System.Object GameCreator.Core.IActionsList/ActionCoroutine::get_result()
extern void ActionCoroutine_get_result_mC8B5641A49CCF3B6CDE09E713E41E47D51B78BD5 (void);
// 0x00000842 System.Void GameCreator.Core.IActionsList/ActionCoroutine::set_result(System.Object)
extern void ActionCoroutine_set_result_m5C7494A81C65703C4CA6CA479A3196B72508332B (void);
// 0x00000843 System.Void GameCreator.Core.IActionsList/ActionCoroutine::.ctor(System.Collections.IEnumerator)
extern void ActionCoroutine__ctor_m1F13249399556C4C875AA6340F2F248F9BDF71FB (void);
// 0x00000844 System.Collections.IEnumerator GameCreator.Core.IActionsList/ActionCoroutine::Run()
extern void ActionCoroutine_Run_m5E2B10DAD3C70CE29D7C2785F8A0C216EFB90C40 (void);
// 0x00000845 System.Void GameCreator.Core.IActionsList/ActionCoroutine::Stop()
extern void ActionCoroutine_Stop_m8E14BC660326DF33FF5EA3E996777F502C63F46C (void);
// 0x00000846 System.Void GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::.ctor(System.Int32)
extern void U3CRunU3Ed__10__ctor_m161D5DDA96EC9824164EAA25CFBA7959B31C271C (void);
// 0x00000847 System.Void GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::System.IDisposable.Dispose()
extern void U3CRunU3Ed__10_System_IDisposable_Dispose_m4CB135F3D4224B9BAED517629FE906CE77A206F1 (void);
// 0x00000848 System.Boolean GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::MoveNext()
extern void U3CRunU3Ed__10_MoveNext_m186F0FA0414EFECC6B9714342DFAC6487DCB80E2 (void);
// 0x00000849 System.Object GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96EC009DA64F1B02F4571CF2C15B947E77829245 (void);
// 0x0000084A System.Void GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::System.Collections.IEnumerator.Reset()
extern void U3CRunU3Ed__10_System_Collections_IEnumerator_Reset_m1061AF69C60892787419DC2081CFB2D051A34488 (void);
// 0x0000084B System.Object GameCreator.Core.IActionsList/ActionCoroutine/<Run>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CRunU3Ed__10_System_Collections_IEnumerator_get_Current_m7AF120E5C89DA314DC5E5AB7794A4F40258E1F0D (void);
// 0x0000084C System.Void GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::.ctor(System.Int32)
extern void U3CExecuteCoroutineU3Ed__7__ctor_m83AD0BC314DA34572FA3D490CE3253BFB7F5FA68 (void);
// 0x0000084D System.Void GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CExecuteCoroutineU3Ed__7_System_IDisposable_Dispose_mA37486D7327AA40D8CC84D0492AB0A0121403CB0 (void);
// 0x0000084E System.Boolean GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::MoveNext()
extern void U3CExecuteCoroutineU3Ed__7_MoveNext_mBC55C2B6383CB0B9E6A9029132B8F3ABE22402D0 (void);
// 0x0000084F System.Object GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA7ABC9F3A7C54472C63995C362F345FDCC8862B (void);
// 0x00000850 System.Void GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m81F9FA9DDF2680024453C428C9D7EEF0CA4F7ED5 (void);
// 0x00000851 System.Object GameCreator.Core.IActionsList/<ExecuteCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m4A0F6B20C3E45CE35088B56C0E871F65FB617DEE (void);
// 0x00000852 System.Boolean GameCreator.Core.ICondition::Check()
extern void ICondition_Check_m81530D798167254D63ACFF4ABD22B5C8DE6A72F2 (void);
// 0x00000853 System.Boolean GameCreator.Core.ICondition::Check(UnityEngine.GameObject)
extern void ICondition_Check_m84512F42B00660DF7FE8FAAA1DBBBB7C889B254C (void);
// 0x00000854 System.Boolean GameCreator.Core.ICondition::Check(UnityEngine.GameObject,System.Object[])
extern void ICondition_Check_mEC8DAE0A9AEEAC0944D71A55F212091BC90562C0 (void);
// 0x00000855 System.Void GameCreator.Core.ICondition::.ctor()
extern void ICondition__ctor_m54B0A608BC52CDE896560E64BBCF46F9F0ECE592 (void);
// 0x00000856 System.Boolean GameCreator.Core.IConditionsList::Check(UnityEngine.GameObject,System.Object[])
extern void IConditionsList_Check_mE39A03DEA4F39E41F50EF48BEFA52F265AF7CE48 (void);
// 0x00000857 System.Void GameCreator.Core.IConditionsList::.ctor()
extern void IConditionsList__ctor_m1A71FB2E27D04FBA7CAC962B131064288ED8535C (void);
// 0x00000858 System.Boolean GameCreator.Core.ActionGatherComponentsByDistance::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionGatherComponentsByDistance_InstantExecute_m14B300FCAF41FCA5475A1CCFC5C448DEEA2FFA3D (void);
// 0x00000859 System.Int32 GameCreator.Core.ActionGatherComponentsByDistance::FilterLayerMask()
extern void ActionGatherComponentsByDistance_FilterLayerMask_m138AAAC0BB674566AA4E8704384CDDF0C3A173A8 (void);
// 0x0000085A UnityEngine.Collider[] GameCreator.Core.ActionGatherComponentsByDistance::GatherColliders(UnityEngine.GameObject)
extern void ActionGatherComponentsByDistance_GatherColliders_m943FB1722FC15C0822D6A3191A5B06358E42988F (void);
// 0x0000085B System.Void GameCreator.Core.ActionGatherComponentsByDistance::.ctor()
extern void ActionGatherComponentsByDistance__ctor_m7CE7A3B5249BD8A4D1650E1B8DF45715A7E6770E (void);
// 0x0000085C System.Void GameCreator.Core.ActionGatherComponentsByDistance/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m411391E28B4793C960820320502C6A52421F3053 (void);
// 0x0000085D System.Int32 GameCreator.Core.ActionGatherComponentsByDistance/<>c__DisplayClass4_0::<InstantExecute>b__0(UnityEngine.GameObject,UnityEngine.GameObject)
extern void U3CU3Ec__DisplayClass4_0_U3CInstantExecuteU3Eb__0_mB3DAFA107748DAA0625DB5045184E6443121552F (void);
// 0x0000085E System.Boolean GameCreator.Core.ActionGatherTagsByDistance::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionGatherTagsByDistance_InstantExecute_m1C2542F14681DCA3DB04116D7389F986AB13ED8D (void);
// 0x0000085F System.Int32 GameCreator.Core.ActionGatherTagsByDistance::FilterLayerMask()
extern void ActionGatherTagsByDistance_FilterLayerMask_mCE2BDFBE7824AFDB6C2100A57CE62E2D165E7075 (void);
// 0x00000860 UnityEngine.Collider[] GameCreator.Core.ActionGatherTagsByDistance::GatherColliders(UnityEngine.GameObject)
extern void ActionGatherTagsByDistance_GatherColliders_mCD08191A9BBF8395CE0C824EBE2909281970D554 (void);
// 0x00000861 System.Void GameCreator.Core.ActionGatherTagsByDistance::.ctor()
extern void ActionGatherTagsByDistance__ctor_m53A91F4F5F93C3D62EB16AD3012BCB9C8B0C3A25 (void);
// 0x00000862 System.Void GameCreator.Core.ActionGatherTagsByDistance/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF1FE5E6FB7D1A786A4F7885ACEA5E7A1C869E006 (void);
// 0x00000863 System.Int32 GameCreator.Core.ActionGatherTagsByDistance/<>c__DisplayClass4_0::<InstantExecute>b__0(UnityEngine.GameObject,UnityEngine.GameObject)
extern void U3CU3Ec__DisplayClass4_0_U3CInstantExecuteU3Eb__0_m36D00A6AAD4ED6B9F63A89559123A0984D136F18 (void);
// 0x00000864 System.Void GameCreator.Core.ActionVariablesAssignBool::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignBool_ExecuteAssignement_mEFEF3721A44EE9A2FC0EC0A4EBECC1A0EA74B51C (void);
// 0x00000865 System.Void GameCreator.Core.ActionVariablesAssignBool::.ctor()
extern void ActionVariablesAssignBool__ctor_mCDA429F7084EF49EA9B23355EA5DE6F812AC20FF (void);
// 0x00000866 System.Void GameCreator.Core.ActionVariablesAssignColor::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignColor_ExecuteAssignement_m03849532AB2BB34E0FE2D2F1F413D4B199639B53 (void);
// 0x00000867 System.Void GameCreator.Core.ActionVariablesAssignColor::.ctor()
extern void ActionVariablesAssignColor__ctor_m067DBB1A7F22A49D8C89D208E832982BEBF1CCF8 (void);
// 0x00000868 System.Void GameCreator.Core.ActionVariablesAssignGameObject::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignGameObject_ExecuteAssignement_mF4BA404DF0BCC6FCB932A7F0165A7BE65648D780 (void);
// 0x00000869 System.Void GameCreator.Core.ActionVariablesAssignGameObject::.ctor()
extern void ActionVariablesAssignGameObject__ctor_mC708D6D1ABD36EE174095B66FAA2E8F06B1CF650 (void);
// 0x0000086A System.Void GameCreator.Core.ActionVariablesAssignNumber::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignNumber_ExecuteAssignement_mE12F4AE0018D363A8159A67E91B5D67A8563739A (void);
// 0x0000086B System.Void GameCreator.Core.ActionVariablesAssignNumber::.ctor()
extern void ActionVariablesAssignNumber__ctor_m1EE74D940E28D458075CE66095C43C68EAFF8909 (void);
// 0x0000086C System.Void GameCreator.Core.ActionVariablesAssignSprite::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignSprite_ExecuteAssignement_mC412941A3C3E7BBAE9489CD386614B994B7B6229 (void);
// 0x0000086D System.Void GameCreator.Core.ActionVariablesAssignSprite::.ctor()
extern void ActionVariablesAssignSprite__ctor_m48D27ED66E34A34EBAA2FE613790512B28D82C74 (void);
// 0x0000086E System.Void GameCreator.Core.ActionVariablesAssignString::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignString_ExecuteAssignement_m03B46F08F0D3BC4A450425CBE0A4D0846D23CDF3 (void);
// 0x0000086F System.Void GameCreator.Core.ActionVariablesAssignString::.ctor()
extern void ActionVariablesAssignString__ctor_mE1DC7FD46C4ACA7BCB431385AAB3AC58F14DDB52 (void);
// 0x00000870 System.Void GameCreator.Core.ActionVariablesAssignTexture2D::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignTexture2D_ExecuteAssignement_m5E396BBD33A5AB0622224C0EC506B6779445EDF8 (void);
// 0x00000871 System.Void GameCreator.Core.ActionVariablesAssignTexture2D::.ctor()
extern void ActionVariablesAssignTexture2D__ctor_mABBF5AE95DC11B5365B4A8FDD11C3FE01D9ACA93 (void);
// 0x00000872 System.Void GameCreator.Core.ActionVariablesAssignVector2::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignVector2_ExecuteAssignement_mD6D7D7EC7107A54FF233638D48E55746B7E8BE54 (void);
// 0x00000873 UnityEngine.Vector2 GameCreator.Core.ActionVariablesAssignVector2::GetVector2(UnityEngine.Vector3)
extern void ActionVariablesAssignVector2_GetVector2_m524C5FBBC7F780A6A171987A1E9E40FB3B11384B (void);
// 0x00000874 System.Void GameCreator.Core.ActionVariablesAssignVector2::.ctor()
extern void ActionVariablesAssignVector2__ctor_mE64A955BD607F469148E1BE6A093796745A33CFA (void);
// 0x00000875 System.Void GameCreator.Core.ActionVariablesAssignVector3::ExecuteAssignement(UnityEngine.GameObject)
extern void ActionVariablesAssignVector3_ExecuteAssignement_m1A046D882B8C5430F80ADB64F7100A88FADB9172 (void);
// 0x00000876 System.Void GameCreator.Core.ActionVariablesAssignVector3::.ctor()
extern void ActionVariablesAssignVector3__ctor_mE1596456FB260789BB710D20EEEECF507E6DBB6B (void);
// 0x00000877 System.Boolean GameCreator.Core.ActionVariablesToggleBool::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionVariablesToggleBool_InstantExecute_m03499F149F478E438F0719EBC3F08F4A145AD994 (void);
// 0x00000878 System.Void GameCreator.Core.ActionVariablesToggleBool::.ctor()
extern void ActionVariablesToggleBool__ctor_mB33C471C5273D236FE0089FA7860AE581F11F220 (void);
// 0x00000879 System.Boolean GameCreator.Core.IActionVariablesAssign::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void IActionVariablesAssign_InstantExecute_m1C8F2263386CA352903B7C61BE71832A7235E0C5 (void);
// 0x0000087A System.Void GameCreator.Core.IActionVariablesAssign::ExecuteAssignement(UnityEngine.GameObject)
// 0x0000087B System.Void GameCreator.Core.IActionVariablesAssign::.ctor()
extern void IActionVariablesAssign__ctor_m07CFFBB2F45FD0DD62788DF9393FF9FF31882922 (void);
// 0x0000087C System.Boolean GameCreator.Core.ConditionVariable::Check(UnityEngine.GameObject)
extern void ConditionVariable_Check_mE1E3D474715CF5D198D081CE50ACADAE0CFCB116 (void);
// 0x0000087D System.Boolean GameCreator.Core.ConditionVariable::Compare(UnityEngine.GameObject)
// 0x0000087E System.Void GameCreator.Core.ConditionVariable::.ctor()
extern void ConditionVariable__ctor_m597A51459E986E1604484A6734623E4092A5C62E (void);
// 0x0000087F System.Boolean GameCreator.Core.ConditionVariableBool::Compare(UnityEngine.GameObject)
extern void ConditionVariableBool_Compare_m827168145037490EEF9DABEDE9FF0A7C865B3E59 (void);
// 0x00000880 System.Void GameCreator.Core.ConditionVariableBool::.ctor()
extern void ConditionVariableBool__ctor_m9386F6E7EB1F5BB6FFAFF8A34F0D9B5BE101394F (void);
// 0x00000881 System.Boolean GameCreator.Core.ConditionVariableColor::Compare(UnityEngine.GameObject)
extern void ConditionVariableColor_Compare_m55003087C863924A82FF868D0DCF2F193B328CDA (void);
// 0x00000882 System.Void GameCreator.Core.ConditionVariableColor::.ctor()
extern void ConditionVariableColor__ctor_mEF167027FB33EBE8F69454A6A83F6BEF61A529EA (void);
// 0x00000883 System.Boolean GameCreator.Core.ConditionVariableGameObject::Compare(UnityEngine.GameObject)
extern void ConditionVariableGameObject_Compare_mE5803F22F6DBF0B0A1D59841F76C08CF3A464360 (void);
// 0x00000884 System.Void GameCreator.Core.ConditionVariableGameObject::.ctor()
extern void ConditionVariableGameObject__ctor_mE2BB5DF92315C4EE6DD21FDEE391CFB4AE529D2D (void);
// 0x00000885 System.Boolean GameCreator.Core.ConditionVariableNumber::Compare(UnityEngine.GameObject)
extern void ConditionVariableNumber_Compare_m05D29243685005AF6E1CF0DBEA3F95A7BB820BD5 (void);
// 0x00000886 System.Void GameCreator.Core.ConditionVariableNumber::.ctor()
extern void ConditionVariableNumber__ctor_m00A21C7982CDCF44420050BC26273C923D87253D (void);
// 0x00000887 System.Boolean GameCreator.Core.ConditionVariableSprite::Compare(UnityEngine.GameObject)
extern void ConditionVariableSprite_Compare_m4A81D080DD0CE65DA2ADEB31D056BB54B76028D6 (void);
// 0x00000888 System.Void GameCreator.Core.ConditionVariableSprite::.ctor()
extern void ConditionVariableSprite__ctor_m3DDC5935970F8F549B97046C0009718B277F772B (void);
// 0x00000889 System.Boolean GameCreator.Core.ConditionVariableString::Compare(UnityEngine.GameObject)
extern void ConditionVariableString_Compare_mBBBE38B901BB6E90D1F16922C9EA8FB4CE1CD203 (void);
// 0x0000088A System.Void GameCreator.Core.ConditionVariableString::.ctor()
extern void ConditionVariableString__ctor_m2F23E73D95B4CDF063B564023E5706FB800C78F4 (void);
// 0x0000088B System.Boolean GameCreator.Core.ConditionVariableTexture2D::Compare(UnityEngine.GameObject)
extern void ConditionVariableTexture2D_Compare_m8F5A25F0A92B71AF7FFABCDFC9E39DA8FA628FCE (void);
// 0x0000088C System.Void GameCreator.Core.ConditionVariableTexture2D::.ctor()
extern void ConditionVariableTexture2D__ctor_mB70B180F2D96D31F82B7FA185400B608798D5586 (void);
// 0x0000088D System.Boolean GameCreator.Core.ConditionVariableVector2::Compare(UnityEngine.GameObject)
extern void ConditionVariableVector2_Compare_m5ADD1BD2B9FD9AA35333BF24657643195AD37BD5 (void);
// 0x0000088E System.Void GameCreator.Core.ConditionVariableVector2::.ctor()
extern void ConditionVariableVector2__ctor_mA9EF45C2C6757895F765011D22787E05E3FABF9C (void);
// 0x0000088F System.Boolean GameCreator.Core.ConditionVariableVector3::Compare(UnityEngine.GameObject)
extern void ConditionVariableVector3_Compare_mB1432ADBF3874A833C616F80615FAEF4A7703885 (void);
// 0x00000890 System.Void GameCreator.Core.ConditionVariableVector3::.ctor()
extern void ConditionVariableVector3__ctor_mAD3A8E380D5A04EFFA302820E6B3A3456FB91D98 (void);
// 0x00000891 System.Void GameCreator.Core.Math.Parser::.ctor(System.String)
extern void Parser__ctor_mF54C2866550D736AA4B5C0C1ABB89980801E66D2 (void);
// 0x00000892 System.Single GameCreator.Core.Math.Parser::Evaluate()
extern void Parser_Evaluate_m74B4F613F51904E756F6278D27E380F06B9C67F9 (void);
// 0x00000893 GameCreator.Core.Math.Node GameCreator.Core.Math.Parser::ParseExpression()
extern void Parser_ParseExpression_m4521039C01A1E53EEECA6F35AA3B2F0B477EC732 (void);
// 0x00000894 GameCreator.Core.Math.Node GameCreator.Core.Math.Parser::ParseAddSubtract()
extern void Parser_ParseAddSubtract_m386ABBF12533B05051D6C23259F942D63D165995 (void);
// 0x00000895 GameCreator.Core.Math.Node GameCreator.Core.Math.Parser::ParseMultiplyDivide()
extern void Parser_ParseMultiplyDivide_mBB8C230DC202BD552902C1458E27EB4A1CBFBA37 (void);
// 0x00000896 GameCreator.Core.Math.Node GameCreator.Core.Math.Parser::ParseUnary()
extern void Parser_ParseUnary_mC9B1E0F78ACAEF9332C0C11D93DAE0000FB9F849 (void);
// 0x00000897 GameCreator.Core.Math.Node GameCreator.Core.Math.Parser::ParseLeaf()
extern void Parser_ParseLeaf_m4D14FCD70E5E84409F35A49585C0DBD1F80EA517 (void);
// 0x00000898 System.Void GameCreator.Core.Math.Parser/<>c::.cctor()
extern void U3CU3Ec__cctor_m4B45CDDCFACD8DE455E30365173F8969DF9A6253 (void);
// 0x00000899 System.Void GameCreator.Core.Math.Parser/<>c::.ctor()
extern void U3CU3Ec__ctor_m2A3560CCB93085BC6E780D9EE67E77FDCEF1B120 (void);
// 0x0000089A System.Single GameCreator.Core.Math.Parser/<>c::<ParseAddSubtract>b__4_0(System.Single,System.Single)
extern void U3CU3Ec_U3CParseAddSubtractU3Eb__4_0_m4324E573756743B448EA5476D8913022E35C5B64 (void);
// 0x0000089B System.Single GameCreator.Core.Math.Parser/<>c::<ParseAddSubtract>b__4_1(System.Single,System.Single)
extern void U3CU3Ec_U3CParseAddSubtractU3Eb__4_1_m6A372F9E02371AD5411F2C13C6CFB24332A09491 (void);
// 0x0000089C System.Single GameCreator.Core.Math.Parser/<>c::<ParseMultiplyDivide>b__5_0(System.Single,System.Single)
extern void U3CU3Ec_U3CParseMultiplyDivideU3Eb__5_0_m989DAA85F48ACB69FEEFF6F868BCFCA753D316AC (void);
// 0x0000089D System.Single GameCreator.Core.Math.Parser/<>c::<ParseMultiplyDivide>b__5_1(System.Single,System.Single)
extern void U3CU3Ec_U3CParseMultiplyDivideU3Eb__5_1_m420FD9C9BAC467CD3ED9228B259258D8394E9FD2 (void);
// 0x0000089E System.Single GameCreator.Core.Math.Parser/<>c::<ParseUnary>b__6_0(System.Single)
extern void U3CU3Ec_U3CParseUnaryU3Eb__6_0_mCAB4B46063F9809AA2AC694BA9514575635C44A3 (void);
// 0x0000089F System.Char GameCreator.Core.Math.Tokenizer::get_currentCharacter()
extern void Tokenizer_get_currentCharacter_m0F54B6D1EDB6099B9702A01A5B9834B74EC300A7 (void);
// 0x000008A0 System.Void GameCreator.Core.Math.Tokenizer::set_currentCharacter(System.Char)
extern void Tokenizer_set_currentCharacter_m827A7EB986EA3DC7D3A2226918C13B18E732255E (void);
// 0x000008A1 GameCreator.Core.Math.Tokenizer/Token GameCreator.Core.Math.Tokenizer::get_currentToken()
extern void Tokenizer_get_currentToken_mCD8FA828E6CC412CF1DFE31196E63C2FEB6B5F00 (void);
// 0x000008A2 System.Void GameCreator.Core.Math.Tokenizer::set_currentToken(GameCreator.Core.Math.Tokenizer/Token)
extern void Tokenizer_set_currentToken_m734975EE88A7476FFC961BE9C066FEF54FC7A574 (void);
// 0x000008A3 System.Single GameCreator.Core.Math.Tokenizer::get_number()
extern void Tokenizer_get_number_mE49477FA7BB77D7AA94B3659689F5FB6AF0FD4B1 (void);
// 0x000008A4 System.Void GameCreator.Core.Math.Tokenizer::set_number(System.Single)
extern void Tokenizer_set_number_mEAE023F2196EB4BA001B1120783D84C0E7F29C47 (void);
// 0x000008A5 System.String GameCreator.Core.Math.Tokenizer::get_identifier()
extern void Tokenizer_get_identifier_mCE413649912A31A44093A6BC8B0CC673405ADF79 (void);
// 0x000008A6 System.Void GameCreator.Core.Math.Tokenizer::set_identifier(System.String)
extern void Tokenizer_set_identifier_mB59DC360B4A15FA7675821A2D0F05F6DD3591A0F (void);
// 0x000008A7 System.Void GameCreator.Core.Math.Tokenizer::.ctor(System.String)
extern void Tokenizer__ctor_m1C20A051A4A2880827E92CFB843FB76EFE753AA8 (void);
// 0x000008A8 System.Void GameCreator.Core.Math.Tokenizer::NextChar()
extern void Tokenizer_NextChar_m3C42276A768974D7D87F071C2B534C123D0BFCE8 (void);
// 0x000008A9 System.Void GameCreator.Core.Math.Tokenizer::NextToken()
extern void Tokenizer_NextToken_mE31761CC66BF94455A6E315AD7BEE2B7045F66FB (void);
// 0x000008AA System.Void GameCreator.Core.Math.Tokenizer::.cctor()
extern void Tokenizer__cctor_m6D4BF70DF9D970B61AF73FFA1FA4BD990D86D380 (void);
// 0x000008AB System.Single GameCreator.Core.Math.Expression::Evaluate(System.String)
extern void Expression_Evaluate_mD9077AA0B1DB7140C31598F986FF1B6468A091F6 (void);
// 0x000008AC System.Single GameCreator.Core.Math.Node::Evaluate()
// 0x000008AD System.Void GameCreator.Core.Math.Node::.ctor()
extern void Node__ctor_mB5E22B29FE7B6B2F4C5E5112015F34806A234938 (void);
// 0x000008AE System.Void GameCreator.Core.Math.NodeBinary::.ctor(GameCreator.Core.Math.Node,GameCreator.Core.Math.Node,System.Func`3<System.Single,System.Single,System.Single>)
extern void NodeBinary__ctor_m511F3E2C4CB4033BC461EFE17FDD3E8EE9488B24 (void);
// 0x000008AF System.Single GameCreator.Core.Math.NodeBinary::Evaluate()
extern void NodeBinary_Evaluate_m864B908E9B7A0B930592084D29684A99EF3B282B (void);
// 0x000008B0 System.Void GameCreator.Core.Math.NodeNumber::.ctor(System.Single)
extern void NodeNumber__ctor_m4B09BEC80A8C748A3B3FB0058290F28D367B07C2 (void);
// 0x000008B1 System.Single GameCreator.Core.Math.NodeNumber::Evaluate()
extern void NodeNumber_Evaluate_mB695978905E919C3ED7AE8558CF16F1B647CA611 (void);
// 0x000008B2 System.Void GameCreator.Core.Math.NodeUnary::.ctor(GameCreator.Core.Math.Node,System.Func`2<System.Single,System.Single>)
extern void NodeUnary__ctor_m6ACB3B495FCA2F3D8680D620C01EEE3714ED13B9 (void);
// 0x000008B3 System.Single GameCreator.Core.Math.NodeUnary::Evaluate()
extern void NodeUnary_Evaluate_m0A90B18E240ACE51E18DAF8384082424BECA0EF9 (void);
// 0x000008B4 System.Void GameCreator.Core.Hooks.HookCamera::.ctor()
extern void HookCamera__ctor_m6DA0E9DA4DDB68FBCA66292C293062C163246CDA (void);
// 0x000008B5 System.Void GameCreator.Core.Hooks.HookPlayer::.ctor()
extern void HookPlayer__ctor_mCD010BE22559A4C60A285288F2E495758F6F68CB (void);
// 0x000008B6 System.Void GameCreator.Core.Hooks.IHook`1::Awake()
// 0x000008B7 TComponent GameCreator.Core.Hooks.IHook`1::Get()
// 0x000008B8 System.Void GameCreator.Core.Hooks.IHook`1::.ctor()
// 0x000008B9 System.Boolean GameCreator.Camera.ActionAdventureCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAdventureCamera_InstantExecute_mF1A187E527FDFC989B102BCDBA217C07996CE512 (void);
// 0x000008BA System.Collections.IEnumerator GameCreator.Camera.ActionAdventureCamera::Execute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionAdventureCamera_Execute_m4B5B9A7099FE47828DCE6EB5948F385DA040162D (void);
// 0x000008BB System.Void GameCreator.Camera.ActionAdventureCamera::.ctor()
extern void ActionAdventureCamera__ctor_mF38C7453DC435023D61AD746F91A3A10482033D7 (void);
// 0x000008BC System.Void GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::.ctor(System.Int32)
extern void U3CExecuteU3Ed__11__ctor_m0200F3F5D3D966175D9581F749E6B5A835BBA109 (void);
// 0x000008BD System.Void GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::System.IDisposable.Dispose()
extern void U3CExecuteU3Ed__11_System_IDisposable_Dispose_m00942BBE5812FCCE5A1F9497036D58AE8C9E927E (void);
// 0x000008BE System.Boolean GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::MoveNext()
extern void U3CExecuteU3Ed__11_MoveNext_m9CCF1888383DFB261BCB767E9697B27EAF8CE2B2 (void);
// 0x000008BF System.Object GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68C82A5E23D0F81B0514376497701F2EE9F32A67 (void);
// 0x000008C0 System.Void GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::System.Collections.IEnumerator.Reset()
extern void U3CExecuteU3Ed__11_System_Collections_IEnumerator_Reset_mDE04DF8E62B2F75FA258B59AB333597FBC2E57A7 (void);
// 0x000008C1 System.Object GameCreator.Camera.ActionAdventureCamera/<Execute>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteU3Ed__11_System_Collections_IEnumerator_get_Current_m421DCFCEB82C325052928E10715A6D19482F238F (void);
// 0x000008C2 System.Boolean GameCreator.Camera.ActionCameraChange::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraChange_InstantExecute_mC4705F16B7B58BA5D85A1DE7C02CCA0CBB70C24A (void);
// 0x000008C3 System.Void GameCreator.Camera.ActionCameraChange::.ctor()
extern void ActionCameraChange__ctor_mAF10CC8D5DD950EF4D067A2D52A87F002662ADAD (void);
// 0x000008C4 System.Boolean GameCreator.Camera.ActionCameraChangeVariable::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraChangeVariable_InstantExecute_mFF2C25D66028DDCBE1DC819E2E412FC2FF359192 (void);
// 0x000008C5 System.Void GameCreator.Camera.ActionCameraChangeVariable::.ctor()
extern void ActionCameraChangeVariable__ctor_mFCE4C3A5DA844C9DBA1F51351728873F64FC617F (void);
// 0x000008C6 System.Boolean GameCreator.Camera.ActionCameraCullingMask::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraCullingMask_InstantExecute_mB014E2DC116E9C9B6D0F56DDC1F98DE34E610EE3 (void);
// 0x000008C7 System.Void GameCreator.Camera.ActionCameraCullingMask::.ctor()
extern void ActionCameraCullingMask__ctor_m65CA1618E1800B65CACF48D339EF38F024410A57 (void);
// 0x000008C8 System.Boolean GameCreator.Camera.ActionCameraDamping::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraDamping_InstantExecute_m5BD2C3091137F0113D496DAD7A792864D436B850 (void);
// 0x000008C9 System.Void GameCreator.Camera.ActionCameraDamping::.ctor()
extern void ActionCameraDamping__ctor_m232385ECA4F4C32E34B64460702D1A2C4000F216 (void);
// 0x000008CA System.Boolean GameCreator.Camera.ActionCameraRotate::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraRotate_InstantExecute_mB003C76ACD91D2047A77256B1C9FAA59C5B320A2 (void);
// 0x000008CB System.Void GameCreator.Camera.ActionCameraRotate::.ctor()
extern void ActionCameraRotate__ctor_mE867F7917BC1F103E7E88C18B4FBB01F6A80D653 (void);
// 0x000008CC System.Boolean GameCreator.Camera.ActionCameraShakeBurst::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraShakeBurst_InstantExecute_mAEB95E25D61E5213C35A38526998D6A5B68AFEC7 (void);
// 0x000008CD System.Void GameCreator.Camera.ActionCameraShakeBurst::.ctor()
extern void ActionCameraShakeBurst__ctor_mC39FB9D77C94D024399249D6F7505B7A4940B4AF (void);
// 0x000008CE System.Boolean GameCreator.Camera.ActionCameraShakeSustain::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionCameraShakeSustain_InstantExecute_mB3CC3C0E17EE2F1D4500348F5CDC240FD2A9CD4A (void);
// 0x000008CF System.Void GameCreator.Camera.ActionCameraShakeSustain::.ctor()
extern void ActionCameraShakeSustain__ctor_mF8C37E3829092D83B26BC7927F50B3D6B7E6267D (void);
// 0x000008D0 System.Boolean GameCreator.Camera.ActionFPSCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionFPSCamera_InstantExecute_m28164C2D02D135DEBA3458A2C9BDFE6C20C75A25 (void);
// 0x000008D1 System.Void GameCreator.Camera.ActionFPSCamera::.ctor()
extern void ActionFPSCamera__ctor_m4DACC211B31B1ACBDD3DF028B3AD9CF9AE411473 (void);
// 0x000008D2 System.Boolean GameCreator.Camera.ActionFixedCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionFixedCamera_InstantExecute_mCFDC544CEFFE032B016C8CABAA36C106436691E4 (void);
// 0x000008D3 System.Void GameCreator.Camera.ActionFixedCamera::.ctor()
extern void ActionFixedCamera__ctor_mAF96DBC23A093B3A7DB92284D4AF2B11971F50A4 (void);
// 0x000008D4 System.Boolean GameCreator.Camera.ActionFollowCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionFollowCamera_InstantExecute_mB9D6FB513DF26DF30B5BD6B4A435B996853CDC9E (void);
// 0x000008D5 System.Void GameCreator.Camera.ActionFollowCamera::.ctor()
extern void ActionFollowCamera__ctor_m087FB6B90A7C9E00E1EF1A9A4FDEC78607BBD15D (void);
// 0x000008D6 System.Boolean GameCreator.Camera.ActionRailwayCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionRailwayCamera_InstantExecute_mA0A2553E882C22F6F400C7A5C8C5E322CC60C6DC (void);
// 0x000008D7 System.Void GameCreator.Camera.ActionRailwayCamera::.ctor()
extern void ActionRailwayCamera__ctor_mFF24F0E08EB98F37E8D31DC078615764B0A39700 (void);
// 0x000008D8 System.Boolean GameCreator.Camera.ActionTargetCamera::InstantExecute(UnityEngine.GameObject,GameCreator.Core.IAction[],System.Int32)
extern void ActionTargetCamera_InstantExecute_m6F880C3A19DA4A2F8A2EE874BB23549CF1FFB086 (void);
// 0x000008D9 System.Void GameCreator.Camera.ActionTargetCamera::.ctor()
extern void ActionTargetCamera__ctor_mC80798E15934BEC991C1369F07D9DAEAF355021D (void);
// 0x000008DA System.Void GameCreator.Camera.CameraController::Start()
extern void CameraController_Start_m0A38F6D7B9DFC80289295FC8928A1668C2BF17A5 (void);
// 0x000008DB System.Void GameCreator.Camera.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mAB1D81FE39884E5803540B7FD8B020615527303D (void);
// 0x000008DC System.Void GameCreator.Camera.CameraController::UpdatePosition()
extern void CameraController_UpdatePosition_mBF7767C6551C7E93DA7A52D3F41411C9C4A78ED1 (void);
// 0x000008DD System.Void GameCreator.Camera.CameraController::UpdateRotation()
extern void CameraController_UpdateRotation_m63C54FA753CE9E97200417543CD3A8E9EAE98EC8 (void);
// 0x000008DE System.Void GameCreator.Camera.CameraController::UpdateProperties()
extern void CameraController_UpdateProperties_mC5FDA854B7B306DEBBFD7820BA7C6235E66EE58A (void);
// 0x000008DF System.Void GameCreator.Camera.CameraController::UpdateShake()
extern void CameraController_UpdateShake_m47A07FD5901E9A7061486C24F1B6EA53E5E55CD3 (void);
// 0x000008E0 System.Void GameCreator.Camera.CameraController::ChangeCameraMotor(GameCreator.Camera.CameraMotor,System.Single)
extern void CameraController_ChangeCameraMotor_m95276DEAF702F096A3A1D92A63816F5671070C37 (void);
// 0x000008E1 System.Void GameCreator.Camera.CameraController::AddShake(GameCreator.Camera.CameraShake)
extern void CameraController_AddShake_m85CE3FA00900CEB694D458676434461B31194DBE (void);
// 0x000008E2 System.Void GameCreator.Camera.CameraController::SetSustainShake(GameCreator.Camera.CameraShake)
extern void CameraController_SetSustainShake_m07CF5F5A74E12C81B2E1D5395EAB86B088A70CF4 (void);
// 0x000008E3 System.Void GameCreator.Camera.CameraController::StopSustainShake(System.Single)
extern void CameraController_StopSustainShake_m1CCCA5CE2FF926EB76D8B83718D44F3CB8C306E8 (void);
// 0x000008E4 System.Void GameCreator.Camera.CameraController::.ctor()
extern void CameraController__ctor_m9AB4CFB9C9A5EF4C4FA6B3DEA4E91FD3533E1D16 (void);
// 0x000008E5 System.Void GameCreator.Camera.CameraController/CameraSmoothTime::.ctor()
extern void CameraSmoothTime__ctor_m562A5EC7FC27D368617AA6065AEF0D4A6F84FDCD (void);
// 0x000008E6 UnityEngine.Vector3 GameCreator.Camera.CameraController/CameraSmoothTime::GetNextPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CameraSmoothTime_GetNextPosition_m61A77096EB509E4D1EDCD34EC48F215B94AA18E5 (void);
// 0x000008E7 UnityEngine.Vector3 GameCreator.Camera.CameraController/CameraSmoothTime::GetNextDirection(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CameraSmoothTime_GetNextDirection_m20783D2D5615B1A7D9E30CF1E6CCEE53D44ECCB3 (void);
// 0x000008E8 System.Void GameCreator.Camera.CameraMotor::Awake()
extern void CameraMotor_Awake_m773F69BB952FB72BD2F40B85FD723CC4B15E00B4 (void);
// 0x000008E9 System.Void GameCreator.Camera.CameraMotor::OnDrawGizmos()
extern void CameraMotor_OnDrawGizmos_m9FC51CE7EA69EE24229379A3445F262B044AE0AA (void);
// 0x000008EA System.Void GameCreator.Camera.CameraMotor::.ctor()
extern void CameraMotor__ctor_m341C15C9C92B9BB2A99978DC45C75713C50D2B74 (void);
// 0x000008EB System.Void GameCreator.Camera.CameraShake::.ctor(System.Single,System.Single,System.Single,System.Boolean,System.Boolean,UnityEngine.Transform,System.Single)
extern void CameraShake__ctor_mE77C9CE50699CB72CD32511507BC47FDAB503673 (void);
// 0x000008EC System.Void GameCreator.Camera.CameraShake::.ctor(System.Single,GameCreator.Camera.CameraShake)
extern void CameraShake__ctor_mEE666C60F97DCF3D20A1692A5B14B42063C633FA (void);
// 0x000008ED System.Void GameCreator.Camera.CameraShake::Initialize()
extern void CameraShake_Initialize_m1690CA6AB9195E46FF41F83030D199D5E0F553D1 (void);
// 0x000008EE UnityEngine.Vector3 GameCreator.Camera.CameraShake::Update(GameCreator.Camera.CameraController)
extern void CameraShake_Update_mB3946F931C04E2B475429DF37B9A6B5426D7B14B (void);
// 0x000008EF System.Single GameCreator.Camera.CameraShake::GetEasing()
extern void CameraShake_GetEasing_m6B6A5FBC9BF96BF8297DBA78320C83722377786C (void);
// 0x000008F0 System.Boolean GameCreator.Camera.CameraShake::IsComplete()
extern void CameraShake_IsComplete_m66DC8917BB0C9AB79800D781803C313203802419 (void);
// 0x000008F1 UnityEngine.Vector3 GameCreator.Camera.CameraShake::GetInfluencePosition()
extern void CameraShake_GetInfluencePosition_m81620CA44CE9C2E8A91C2C3FB804670866C40D54 (void);
// 0x000008F2 UnityEngine.Vector3 GameCreator.Camera.CameraShake::GetInfluenceRotation()
extern void CameraShake_GetInfluenceRotation_m86583AB84FA703C49EF73A56AB3243AE48B213FA (void);
// 0x000008F3 System.Single GameCreator.Camera.CameraShake::GetNormalizedProgress()
extern void CameraShake_GetNormalizedProgress_mA914D4BE1A36A5C174717FC407AAEC476264EFAA (void);
// 0x000008F4 System.Void GameCreator.Camera.CameraShake::.cctor()
extern void CameraShake__cctor_mAD3C5C8BD70AC587F952E4933575AC31D517FAD6 (void);
// 0x000008F5 System.Void GameCreator.Camera.CameraMotorTypeAdventure::Awake()
extern void CameraMotorTypeAdventure_Awake_mA66DE8181AE89DBDC233A52A80E4946275AB528F (void);
// 0x000008F6 System.Void GameCreator.Camera.CameraMotorTypeAdventure::EnableMotor()
extern void CameraMotorTypeAdventure_EnableMotor_m809B1F469E5006BD0825649E58109166B73F35F7 (void);
// 0x000008F7 System.Void GameCreator.Camera.CameraMotorTypeAdventure::DisableMotor()
extern void CameraMotorTypeAdventure_DisableMotor_m685DB327BC7E7CFC5BA2945EBEEDC8862E8F3842 (void);
// 0x000008F8 System.Void GameCreator.Camera.CameraMotorTypeAdventure::UpdateMotor()
extern void CameraMotorTypeAdventure_UpdateMotor_mE6CFB125392462C91205FCF731EAA41C6BC405F9 (void);
// 0x000008F9 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeAdventure::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeAdventure_GetPosition_mFA7F72CC08406E15E54940DC26D632CCCCD1D36F (void);
// 0x000008FA UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeAdventure::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeAdventure_GetDirection_mE7B8777981062795F72E4A014C0C2BFC2E7DF758 (void);
// 0x000008FB System.Boolean GameCreator.Camera.CameraMotorTypeAdventure::UseSmoothPosition()
extern void CameraMotorTypeAdventure_UseSmoothPosition_m2CB223E5A08CDF87FA4C34E45DE2FD92CE1E1374 (void);
// 0x000008FC System.Boolean GameCreator.Camera.CameraMotorTypeAdventure::UseSmoothRotation()
extern void CameraMotorTypeAdventure_UseSmoothRotation_m9132F9E2F55FCFE67A09985A306FD76A97A4F876 (void);
// 0x000008FD System.Void GameCreator.Camera.CameraMotorTypeAdventure::AddRotation(System.Single,System.Single,System.Single)
extern void CameraMotorTypeAdventure_AddRotation_m5BC7E64E1592CBC5F9C67CAA6B6FDC6154E8E91A (void);
// 0x000008FE System.Void GameCreator.Camera.CameraMotorTypeAdventure::FixedUpdate()
extern void CameraMotorTypeAdventure_FixedUpdate_mD86CD67911AD18F0FDBFC36E42F59EA822A7A049 (void);
// 0x000008FF System.Void GameCreator.Camera.CameraMotorTypeAdventure::RotationInput(System.Single&,System.Single&)
extern void CameraMotorTypeAdventure_RotationInput_m21E9F252AD43F8057A58317ACE3BC5E0AF2832BB (void);
// 0x00000900 System.Void GameCreator.Camera.CameraMotorTypeAdventure::RotationRecover()
extern void CameraMotorTypeAdventure_RotationRecover_m8DBFDF3A448806F90D336986D7AF4C525DA8D818 (void);
// 0x00000901 System.Void GameCreator.Camera.CameraMotorTypeAdventure::.ctor()
extern void CameraMotorTypeAdventure__ctor_m7A76CCBD66630B322BC508D0520A8E4D160FA884 (void);
// 0x00000902 System.Void GameCreator.Camera.CameraMotorTypeAdventure::.cctor()
extern void CameraMotorTypeAdventure__cctor_mD8C1B4B595EA99398F9C896DDD7E690D15581582 (void);
// 0x00000903 System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::EnableMotor()
extern void CameraMotorTypeFirstPerson_EnableMotor_mBAF0B3217A76E748B15793FD79DEC4A3F7B54345 (void);
// 0x00000904 System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::DisableMotor()
extern void CameraMotorTypeFirstPerson_DisableMotor_m2C55A8AEE63CA20E3C8275CC9FEEB3D6A8C06373 (void);
// 0x00000905 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFirstPerson::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeFirstPerson_GetPosition_m9744F54BB30CD25D89AEF7A72980B8385244D65D (void);
// 0x00000906 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFirstPerson::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeFirstPerson_GetDirection_m0051B547AF9DD5D96199219F3FA447D18CC7C0C5 (void);
// 0x00000907 System.Boolean GameCreator.Camera.CameraMotorTypeFirstPerson::UseSmoothRotation()
extern void CameraMotorTypeFirstPerson_UseSmoothRotation_m7A36578B50E56D2DCB1A5BBABF2C20ECD5EDD940 (void);
// 0x00000908 System.Boolean GameCreator.Camera.CameraMotorTypeFirstPerson::UseSmoothPosition()
extern void CameraMotorTypeFirstPerson_UseSmoothPosition_m4B1EA93F9A6B8F551066E1380034DB711A9C3335 (void);
// 0x00000909 System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::AddRotation(System.Single,System.Single,System.Single)
extern void CameraMotorTypeFirstPerson_AddRotation_mC9D1107180F62249FF696C5789F79F3D1435358F (void);
// 0x0000090A UnityEngine.Transform GameCreator.Camera.CameraMotorTypeFirstPerson::GetTarget()
extern void CameraMotorTypeFirstPerson_GetTarget_m2C836AEDAB84E73B78F14614FA151683D011B5EA (void);
// 0x0000090B System.Single GameCreator.Camera.CameraMotorTypeFirstPerson::ClampAngle(System.Single,System.Single,System.Single)
extern void CameraMotorTypeFirstPerson_ClampAngle_m4FD718505DAEEC6FBA6DF555CB88A140032087A9 (void);
// 0x0000090C UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFirstPerson::Headbob()
extern void CameraMotorTypeFirstPerson_Headbob_m5839B25479F234D6D78D5F792650C8953181083A (void);
// 0x0000090D System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::RotationInput()
extern void CameraMotorTypeFirstPerson_RotationInput_m353F56C911AC6B059ECEC68897F4B01DAA012FCF (void);
// 0x0000090E System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::.ctor()
extern void CameraMotorTypeFirstPerson__ctor_mF970958EFC869EC8DF14A972DA5C5290F02D7C5B (void);
// 0x0000090F System.Void GameCreator.Camera.CameraMotorTypeFirstPerson::.cctor()
extern void CameraMotorTypeFirstPerson__cctor_m9EDE12F0C0200C3939DA862B2F5226E1315F290C (void);
// 0x00000910 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFixed::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeFixed_GetDirection_mEC2629174EA9858602727F1E0643DFEEB2A4465C (void);
// 0x00000911 System.Void GameCreator.Camera.CameraMotorTypeFixed::.ctor()
extern void CameraMotorTypeFixed__ctor_mC23AE80806123CAF151A45D2D9E6717287875732 (void);
// 0x00000912 System.Void GameCreator.Camera.CameraMotorTypeFixed::.cctor()
extern void CameraMotorTypeFixed__cctor_mA220BC9EA2F3A18BDABC5E989A58BF45611EFF55 (void);
// 0x00000913 System.Void GameCreator.Camera.CameraMotorTypeFollow::UpdateMotor()
extern void CameraMotorTypeFollow_UpdateMotor_m1DFCDC71832CD8A30F600A787A4FF390CED5B6E6 (void);
// 0x00000914 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFollow::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeFollow_GetPosition_m07529476A93039982AD9822DC1B6E945957A31DE (void);
// 0x00000915 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeFollow::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeFollow_GetDirection_m833F650CDC16901B3E697E243ADEDA04E88B2E90 (void);
// 0x00000916 System.Void GameCreator.Camera.CameraMotorTypeFollow::.ctor()
extern void CameraMotorTypeFollow__ctor_mAC662C7C8E6A8AD3E73C48DFDB6811F1D1380174 (void);
// 0x00000917 System.Void GameCreator.Camera.CameraMotorTypeFollow::.cctor()
extern void CameraMotorTypeFollow__cctor_mF1175A60D34BC705C65E22275088B3949D9ECA60 (void);
// 0x00000918 System.Void GameCreator.Camera.CameraMotorTypeRailway::Awake()
extern void CameraMotorTypeRailway_Awake_m418E098D2ECB65777E2DB2E3296EBF784706EAC9 (void);
// 0x00000919 System.Void GameCreator.Camera.CameraMotorTypeRailway::UpdateMotor()
extern void CameraMotorTypeRailway_UpdateMotor_m92932F5E01CED9FEC1AD1528DA254D06E1D54A39 (void);
// 0x0000091A UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeRailway::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeRailway_GetPosition_m4A59F6EC151260B73A3B10229E5760024D6F7F83 (void);
// 0x0000091B UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeRailway::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeRailway_GetDirection_m4854DB06E6FF9E9189F57EB1363CC8392BA5898A (void);
// 0x0000091C System.Void GameCreator.Camera.CameraMotorTypeRailway::.ctor()
extern void CameraMotorTypeRailway__ctor_m09F630D2333F40ED4735EDF491DD7E1B0E877E9C (void);
// 0x0000091D System.Void GameCreator.Camera.CameraMotorTypeRailway::.cctor()
extern void CameraMotorTypeRailway__cctor_m5DE3EA3A18C85114293BFD91461B4CBD8E20C52E (void);
// 0x0000091E System.Void GameCreator.Camera.CameraMotorTypeTarget::UpdateMotor()
extern void CameraMotorTypeTarget_UpdateMotor_m46496547F98179BAF6F3430824B2345172B31DA2 (void);
// 0x0000091F UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeTarget::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeTarget_GetPosition_m14E5E777F9291524296E3C17612420546AFE5312 (void);
// 0x00000920 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeTarget::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeTarget_GetDirection_m8A63FA7D45805E3A2FDB4DB9DC89B0E1876C837A (void);
// 0x00000921 System.Void GameCreator.Camera.CameraMotorTypeTarget::.ctor()
extern void CameraMotorTypeTarget__ctor_m765132F98BC20F21EF988E77DD0A4DFE12414FE4 (void);
// 0x00000922 System.Void GameCreator.Camera.CameraMotorTypeTarget::.cctor()
extern void CameraMotorTypeTarget__cctor_m316C2A562718E30F9C41D0153FDF413F820CE05B (void);
// 0x00000923 System.Void GameCreator.Camera.CameraMotorTypeTween::Awake()
extern void CameraMotorTypeTween_Awake_mEBFA729B93FADCAFE1E7663B3F49F72F900FF866 (void);
// 0x00000924 System.Void GameCreator.Camera.CameraMotorTypeTween::EnableMotor()
extern void CameraMotorTypeTween_EnableMotor_m195390B029589D57FA431FD12E8708F41E06D1B2 (void);
// 0x00000925 System.Void GameCreator.Camera.CameraMotorTypeTween::UpdateMotor()
extern void CameraMotorTypeTween_UpdateMotor_m1500B97DCCD33595C9D25140B17593A7CF1F2D68 (void);
// 0x00000926 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeTween::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeTween_GetPosition_mF7699FC7B8FEB017845771A92D8F996B6526221B (void);
// 0x00000927 UnityEngine.Vector3 GameCreator.Camera.CameraMotorTypeTween::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void CameraMotorTypeTween_GetDirection_m4BD18C534F25D00DB0AD02DCE614DC312C62E89B (void);
// 0x00000928 System.Void GameCreator.Camera.CameraMotorTypeTween::.ctor()
extern void CameraMotorTypeTween__ctor_m08751A9B5E789A2ED878C5AF8649FCFADD8E9F1E (void);
// 0x00000929 System.Void GameCreator.Camera.CameraMotorTypeTween::.cctor()
extern void CameraMotorTypeTween__cctor_m88CC3CEC454270E89771046F7E3504523B391E58 (void);
// 0x0000092A System.Void GameCreator.Camera.ICameraMotorType::Initialize(GameCreator.Camera.CameraMotor)
extern void ICameraMotorType_Initialize_m64FA374ECB42165D57957C34F38CBF3F6A52EACC (void);
// 0x0000092B System.Void GameCreator.Camera.ICameraMotorType::Update()
extern void ICameraMotorType_Update_m13B9BD65B115493682EC77A42CCC072F962F2DBB (void);
// 0x0000092C System.Void GameCreator.Camera.ICameraMotorType::EnableMotor()
extern void ICameraMotorType_EnableMotor_mEA7863B477E3AA5B4573C527741D86062787C5C2 (void);
// 0x0000092D System.Void GameCreator.Camera.ICameraMotorType::DisableMotor()
extern void ICameraMotorType_DisableMotor_m3CB2A76C959467D5945831DDF7A7DE35C9561EB6 (void);
// 0x0000092E System.Void GameCreator.Camera.ICameraMotorType::UpdateMotor()
extern void ICameraMotorType_UpdateMotor_mB95865F50C0A3F900C9047361AD3133EBAFD1661 (void);
// 0x0000092F UnityEngine.Vector3 GameCreator.Camera.ICameraMotorType::GetPosition(GameCreator.Camera.CameraController,System.Boolean)
extern void ICameraMotorType_GetPosition_m490516BAC627DF422BA16D8E41D688D4A3DFB5B3 (void);
// 0x00000930 UnityEngine.Vector3 GameCreator.Camera.ICameraMotorType::GetDirection(GameCreator.Camera.CameraController,System.Boolean)
extern void ICameraMotorType_GetDirection_mC5D9E0835B9FDB2F858CB021E2B532916421E280 (void);
// 0x00000931 System.Boolean GameCreator.Camera.ICameraMotorType::UseSmoothPosition()
extern void ICameraMotorType_UseSmoothPosition_m89982E1CF36D801FBC65769854B911C4E8639BCE (void);
// 0x00000932 System.Boolean GameCreator.Camera.ICameraMotorType::UseSmoothRotation()
extern void ICameraMotorType_UseSmoothRotation_m0838436342552B19469206176B2AE9178859F01C (void);
// 0x00000933 System.Void GameCreator.Camera.ICameraMotorType::.ctor()
extern void ICameraMotorType__ctor_mFA88B9EF227DBA8CB9284A09A60921303143537F (void);
// 0x00000934 System.Void GameCreator.Camera.ICameraMotorType::.cctor()
extern void ICameraMotorType__cctor_mDC3BA5A1A3A323A2F4A8737A83B92C9BB9C5FD38 (void);
static Il2CppMethodPointer s_methodPointers[2356] = 
{
	ActionListVariableAdd_InstantExecute_mE2C7482D4783BDD864E13EFCB6A38EDE2825A489,
	ActionListVariableAdd__ctor_mBFDB45503BCC2FEC4D03B73C09738A66B86C2F73,
	ActionListVariableClear_InstantExecute_m53E62B4075E9002631CF80D2C37EBE43B8206473,
	ActionListVariableClear__ctor_m0BAC288575F953D1C4BB82B482D07F09B4DC0F22,
	ActionListVariableIterator_InstantExecute_m787E110B3BA0CEFA3231C617DC02210182EE54B7,
	ActionListVariableIterator__ctor_m0A47C44DFA38ED917B017251444AE38143AD8BD4,
	ActionListVariableLoop_Execute_m484A83A08BD40ACF0AC56B7F65DD61F11E15465E,
	ActionListVariableLoop_OnCompleteActions_m3DA67F4FA0001E682F01958E5CA7931946638AD5,
	ActionListVariableLoop_Stop_m37D23A85714A4EDC2EC3BFA774CF070E770EA20E,
	ActionListVariableLoop__ctor_m2770F2E8CCC639A33A3A2124A9B0CE3CB5BD4C54,
	U3CU3Ec__DisplayClass8_0__ctor_mEC0C8A8A57314E4BEF46BF12C67104887B6E9E13,
	U3CU3Ec__DisplayClass8_0_U3CExecuteU3Eb__0_mD3BC06E8BE1CC8C7C59FB32A843073EDC199D94C,
	U3CExecuteU3Ed__8__ctor_m658ECA02DE483EE45BD23E8B74735576A87A991B,
	U3CExecuteU3Ed__8_System_IDisposable_Dispose_mA4C5A1494AACB2E9E96F1C9BD450A199CE8B687D,
	U3CExecuteU3Ed__8_MoveNext_m362E562699C5EE8E86F41DBDCA618A8E42F40602,
	U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFD2215568C11F45456ADE5DF3E6A8D8F5004560,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEEBD76692D994EC6D6539E5B6392BE1A12366222,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_mD77DDDFDD37CE5E81FC21CF5E61415CDFC2FDC99,
	ActionListVariableRemove_InstantExecute_m567FA75F31C039F4735805D16AF9203B232D8D38,
	ActionListVariableRemove__ctor_mDA41DC23F364308633369787399BA240CB5A7625,
	ActionListVariableSelect_InstantExecute_m153650C0129732939E55242E11109BEB0BC8A678,
	ActionListVariableSelect__ctor_m26C5B0FA38CD4B081C73F3E1A397D6C998CABEF1,
	ActionVariableDebug_InstantExecute_m35FF2F62FEB907B794393E26D77604342C2158EC,
	ActionVariableDebug__ctor_m041C5096929B1B620FA1741CC5227C9917F5D3F6,
	ActionVariableRandom_InstantExecute_m04C4C74EA9502D8DF62B11736FF1F5D242F8504D,
	ActionVariableRandom__ctor_mDB5336055F15FB606BCEF40EE7205B75CFB07787,
	ActionVariablesReset_InstantExecute_m2A27B917EBEBDDBFA875B69BB28089B92F150889,
	ActionVariablesReset__ctor_mB9840097D01D15EC6082E777DA48EEAC87E2F365,
	ActionVariableAdd_InstantExecute_mAEB97B431BA9B1C0D94943CA0FAC42582CC16D04,
	ActionVariableAdd__ctor_mCD4DEBBE1F5339604DDFA19BD0B03749C7BEF212,
	ActionVariableDivide_InstantExecute_m7D5ABC4B1A515B4EDB64BEAA88B4AF3410A65CCC,
	ActionVariableDivide__ctor_m352A7B3BF1303840F425FAC245914B733F446437,
	ActionVariableMath_InstantExecute_m0F961A12FBB6616247EC5F19EAC4BDA85B5C899E,
	ActionVariableMath__ctor_m1A0AEF910F1C40AF57D50058F6DAE8BF116C6A71,
	ActionVariableMultiply_InstantExecute_m62C1DCA655EFC52799F4794AB586DAF616202C31,
	ActionVariableMultiply__ctor_m1868B94E5A2285C1B1C0C4DBCC4A3A75FB9845E3,
	ActionVariableOperationBase__ctor_m59051C577B76627AA6926C51FC5E5C8E3F34D1E5,
	ActionVariableSubtract_InstantExecute_m7B71E5AE31AD790D5B4DCF69201AF546DA35EC6E,
	ActionVariableSubtract__ctor_m710FFA32C444CF50A86C7253649F8554C4383684,
	VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseHelperVariable__ctor_mC05AF16A3BF8D2C30658398BD61FA555C960DB5B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BoolProperty__ctor_mDD02036E09F03E5BDB2B7787D3D830BBEB41B0A2,
	BoolProperty__ctor_mF8DD9BE677D6ED39278DFEE77F880241E07FC162,
	ColorProperty__ctor_m48210CF165ABC115E44B994AB31E95ECFFB161A0,
	ColorProperty__ctor_m1E1901CDB0631C9B41070AC3C0E4D4A4ACFBA230,
	GameObjectProperty__ctor_m441BDB9AE7A0BE871EA35A739473BC12D754E5BB,
	GameObjectProperty__ctor_m8C2E81F3F1AB5CCA22C730DB9E185142881D014B,
	GameObjectProperty_GetValueName_mD0B2EE91E913D001E85174F66082C8CEFDDBFDC1,
	HelperGlobalVariable_Get_m09932ED7508E89B31F87B39BA7A8D203A6115729,
	HelperGlobalVariable_Set_mC9297547BFD49326D1C69CA98AEC6236614C5548,
	HelperGlobalVariable_ToString_m0F6A621C5E4B18AE1C6FFA3773EB39D6AE2022F3,
	HelperGlobalVariable_ToStringValue_m365C800DCDA1E958E092D96DBC6CF831380D621B,
	HelperGlobalVariable_GetDataType_m8D5F57780ADA430B90028C20301D1EEF53520A17,
	HelperGlobalVariable__ctor_m677699EFE96DBF7A14A4790A51155CC09592DE2B,
	HelperLocalVariable_Get_m1C67AB1D2F517B291B70C5A594BE222CCBD69F7C,
	HelperLocalVariable_Set_mFFAF517342EFC9CD90913A4C890988DD8191C316,
	HelperLocalVariable_GetGameObject_m8EF3CF852A8C9C5FE8BB41B04D6EA3FA9F8CF6D4,
	HelperLocalVariable_ToString_m09E3A4752569C99C9D2EB5D09E3A3AE2558A9F41,
	HelperLocalVariable_ToStringValue_m0E19E255B70381F58243A4BDB85B90A26EC169A6,
	HelperLocalVariable_GetDataType_m0CEC98FAB106DC2DB2C41890AFAE01042D2BF9B8,
	HelperLocalVariable__ctor_mEED4EB403BBDA152A6723707E83103BA5E7AB44C,
	HelperGetListVariable_Get_m23119D24E6DF8890EDC12C82962DB9057D8FB06B,
	HelperGetListVariable_Set_m146EEBE758BD1C3C2B4186EA75CA48CE7BBFEF9A,
	HelperGetListVariable_GetGameObject_m92ECF2EA41A24A91AF957B3C6CA742675B98D3BB,
	HelperGetListVariable_ToString_m8F934406057E6057AF9A2A0D7B3A0744B978AE3E,
	HelperGetListVariable_ToStringValue_mB1BE97493DD66093124FFAD15F41BBB89E90E1BE,
	HelperGetListVariable__ctor_mEDC502309303BB06450512CA31C1AD5EFD8B7FF1,
	HelperListVariable_GetListVariables_mD5A5C3B3F08C3C2EE90ACF48F9194741E6B04492,
	HelperListVariable_GetDataType_mBF354A8AAA04424D530D9B7E5FB5DC396A25B33B,
	HelperListVariable_ToString_m665B505FAEBBCFF46029AE9288506DA7F81D8D32,
	HelperListVariable__ctor_m88E797146ADAC261DC62C9FB87A100F2113F138B,
	NumberProperty__ctor_mF2DA5C8020A19D44ABBA0835E7EA6814059A385B,
	NumberProperty__ctor_m4D9D33F2E42D3CC6D54B3895BA8AB58785F921E0,
	NumberProperty_GetInt_mCAD822FE8FADC604D44C3095B4BD2DAB0F2EF24C,
	SpriteProperty__ctor_m71EAE090E587CE6240224B262822680B351EBD89,
	SpriteProperty__ctor_m92A3B6B6D47F2B0F36B75C91F8FFB6A2D5A1BFE3,
	StringProperty__ctor_m81E59CBF6DA8CB77591CC9685A7C491DE4AF8E32,
	StringProperty__ctor_m01EDACDB45370F73B9DD1A3C606A477C39C074B2,
	Texture2DProperty__ctor_m2B3F19141270905B8EB9F0EE752FB50BD3773F3D,
	Texture2DProperty__ctor_m3EEE53F595F373E105FEF1A4FFB4C46160E687BB,
	VariableGlobalProperty__ctor_m24AA98E5A0156A3764CF12F4CC913A2A3466E234,
	VariableGlobalProperty_Get_mE71CA9062C1DD8E7AD4D42887960DC9FF2F2E9B2,
	VariableGlobalProperty_Set_m43CCE4F0F8CA865AE2E06A827089EC3FDC3B5CE5,
	VariableGlobalProperty_GetVariableID_mBB7DEEBEFC22E09734BB49DBAC8319A98E690BEC,
	VariableGlobalProperty_ToString_m5E978A59AD3A437E5A732F2E22A1BA217FA132B2,
	VariableGlobalProperty_ToStringValue_m08EFFBDE56E4A72B7D690A4DF8564B52785605D7,
	VariableProperty__ctor_mFDAF04955A1159213AE4F03503A3BB3AD9168A3B,
	VariableProperty__ctor_m78325DD5CF084FD38CE44EE59EB80BDBE24C944F,
	VariableProperty__ctor_m2C83FF6B15BED75507FFE30E8C90EA0C2A52D340,
	VariableProperty_SetupVariables_m670C2D5B7042900C7795F0A515BBCE3CE961890B,
	VariableProperty_Get_m57DF25DA48EEF019FEBB223C0BE503E686828D1B,
	VariableProperty_Set_m9CF11AB910494CCCFC50C5BC5D9D942D4A1DB896,
	VariableProperty_GetVariableID_m30092B35E1DD0EECB4E67F7B53A80DD526F06089,
	VariableProperty_GetVariableType_m0C09A17246022F05775F3B061A7A7DC418042EB6,
	VariableProperty_GetLocalVariableGameObject_m6504A712133D2C0BABAD881CA4C52458D30D2093,
	VariableProperty_GetListVariableGameObject_mE5B588D7C12BD2ACB67AFE4AB8E63F25845E3551,
	VariableProperty_GetLocalVariableGameObject_mB54ABFA79491D60E0AB987CF51451B1BCA2A25AE,
	VariableProperty_GetListVariableGameObject_m23A6FDB73101A2F0F6FF1D4244FCF85BE1ACCC01,
	VariableProperty_GetVariableDataType_mB748F26EAFD5CF417F95C7A324156634008530F1,
	VariableProperty_ToString_m39957D30CAA647AEDBA0F72D011A03922F15BA8F,
	VariableProperty_ToStringValue_m25DE9AEBB563740777C7FE7C5CEBCD8F786189B9,
	Vector2Property__ctor_m80EC4749B26B1B876AA6B34235801A1CF320AE94,
	Vector2Property__ctor_m21BAD476D71BD0C1DC508656C351C74E308552E8,
	Vector3Property__ctor_m2B4DB21791503120E38667E22B000128306CA842,
	Vector3Property__ctor_mA5958966050D255F20DD61B47043B55F22092BEA,
	GlobalVariables__ctor_mD9B570C621BF1C71792B86A72A185DB6D5755DB8,
	ListVariables_get_iterator_mFCBB7595D4EA279F6B1F9C15FA22579B60672FA4,
	ListVariables_set_iterator_m74F41B75E46D982BD624A4E8D6EDD63FCE9235BB,
	ListVariables_Start_m7293DEC914CDF4263C509648EF8126C2664D988A,
	ListVariables_Get_m559E86FD2BA087D3B899656AE7422EF9140DB578,
	ListVariables_Get_m5BC953765178DC4752C2C1F850FAF94E6362C9C5,
	ListVariables_Push_m6EB7799F5CD5C2F9CD206DEABB8BCBF15810553E,
	ListVariables_Push_m0338EC7966E21DE313B8118B738887023032E128,
	ListVariables_Remove_mCB3A653187FBCAFC0D1063845A93024E56CA4C72,
	ListVariables_Remove_m237E9B88D8A01A833A1C9E8E7244130007DE1B1D,
	ListVariables_SetInterator_m27219638DAA711D47240C67B75316FD09620B582,
	ListVariables_NextIterator_m863D13A64AAA6536E7C5D9A58ED6561C76A62AFC,
	ListVariables_PrevIterator_m4E608D27743368534A59FC63BDE2623C7AB772C8,
	ListVariables_RequireInit_mB23EB4548A5473EBC88ED2C594AED4D881988FC5,
	ListVariables_GetUniqueName_m275227B73AE11BD0952DA8D5A92041C41569C6FF,
	ListVariables_GetSaveData_mC8E89C9E0E38529FCB838A461497ECA0E9C35204,
	ListVariables_ResetData_m9BF9A95A3C6766B0B7759765D8CFCC5E0F052EB9,
	ListVariables_OnLoad_m3F83AF182DF83CF5F7A982D9F6F157C45708D9DC,
	ListVariables__ctor_m76BF3F959915316C6B67FFAB63BD5A1A4E6EB19B,
	LocalVariables_Get_m79A91CAC485E5E280D3B59E2F6C7CA7185820130,
	LocalVariables_Start_m15EBF8F550EADAD705B26BE1E6EA2466475E09BE,
	LocalVariables_Initialize_m6E845686584BEFD153D4FC13A7AC2A5457A87759,
	LocalVariables_OnDestroy_mD21BD0AF00ACB5BF31103996D085EB69B09FB2FB,
	LocalVariables_RequireInit_m902194544F7CB798E1307655766C4106D42E3DF7,
	LocalVariables_GetUniqueName_mE74EB27002380DA06B6A267A908C7CCFD4AE1EF5,
	LocalVariables_GetSaveDataType_m23622054DE35D3ECF261FBDAF766F929A91E864D,
	LocalVariables_GetSaveData_m5CBDCC8EEBEC857BE4923FE4D79099D4AE294E5E,
	LocalVariables_ResetData_mEA858173E3CD522F785DD14C2565BA90D9D6F158,
	LocalVariables_OnLoad_m6D9641956A069EEC6E1C9E5289F374F71CF06E63,
	LocalVariables__ctor_m2F28D428E723D3C019344AE9B93871E311262242,
	LocalVariables__cctor_m754F3A7DF3DF2050A8FC6BBA0E659871E607912B,
	TextVariable_Awake_m18990994AA54705B5C6128C95EA0820388728FF2,
	TextVariable_OnEnable_mEE2AAC94BDA99CD43BBEC806BD3ECF4B0EB462CF,
	TextVariable_OnDisable_m27C93BA14CDDF48558E4177D0A685A69DF7402A6,
	TextVariable_OnDestroy_m535255D7234ED8625720FB2BFB4932F2303D8639,
	TextVariable_OnApplicationQuit_m5AF72C397D98CC153FBEEBF179F035C33C84AEB7,
	TextVariable_OnUpdateVariable_m8B62A491F12BFAAB44A7A4AA2B54BCD0DF97129B,
	TextVariable_OnUpdateList_m30346CD773390BEB688D41FC19AEAE14A990A2FF,
	TextVariable_UpdateText_m4DD81C187B3D0E6168C6DBCE9574FB16A20B8DF0,
	TextVariable_SubscribeOnChange_mFA8CFFB06C127CDC9C237EEC002188C128E558B6,
	TextVariable_UnsubscribeOnChange_mC5A8F4E5B305FB439C147AE43E1F828C31C97760,
	TextVariable__ctor_mFDBE5D6EFE7CBCE6D6B224FB43870911C8684A39,
	ConditionGameObjectInList_Check_m66B0E5D6C9202DABCCD2110855086BC13225F9E5,
	ConditionGameObjectInList__ctor_mD9FC8A0B1F1CE89BE0683026F9D6D56D74048212,
	ConditionListVariableCount_Check_mA7A076F6E247B3D4BA57881647EE5198F0667E99,
	ConditionListVariableCount__ctor_mAA8B24AC2FF3482C25FCD8B819B7416A08479857,
	DatabaseVariables_GetGlobalVariables_m98365648B2EDEC62AB66B21DC0B6D612B3046C45,
	DatabaseVariables_Load_m960CB53FAC00CCE45FABDDC7F143A5E1C7ED6060,
	DatabaseVariables__ctor_m824A52530C6D789D7EF749889EA4EF8E34FE4D8D,
	Container__ctor_m6AFEE4B02BA2B87B3C94C41237E56E56ED937E45,
	MBVariable__ctor_m7D2CC7BBB0DF3A5D50E01D40448B13B5E42230BC,
	SOVariable__ctor_mB6E1D836299CC14249E98157E7E8E040E1FE807A,
	GlobalVariablesManager_InitializeOnLoad_mA313C49AF61E1FD4278C32C60581918067C63663,
	GlobalVariablesManager_OnCreate_mAA730EB526E0F24283821CA077A2CA3B065D6CF4,
	GlobalVariablesManager_Get_m526D9624ADC1CA8923484924C1844ABCBAE7276A,
	GlobalVariablesManager_GetNames_m579B393A8C54B2DBB22845C7E19D608D17AB1A9E,
	GlobalVariablesManager_RequireInit_mB7C4D1035738D10D755B3845CD01610878817992,
	GlobalVariablesManager_GetUniqueName_m1282AF44B2959018F22207FDDAC91BD155759865,
	GlobalVariablesManager_GetSaveDataType_m0700935F0830BB528384DEF5BF6A3EE3885BDA1C,
	GlobalVariablesManager_GetSaveData_m550C93A75DD69C570E3FCDADFF711C493D5E0864,
	GlobalVariablesManager_ResetData_mC1CE993D8DDA689A05B2BCFEC4EF45E057999653,
	GlobalVariablesManager_OnLoad_m255D02BED6D080363C9B13822D132E47D797DC2A,
	GlobalVariablesManager__ctor_mF8E100FDC1AE9838850F7BF3C8DE469120143BF0,
	VariablesManager_Reset_m0FCF892148B5144A0261ADC0033C40438100EB20,
	VariablesManager_GetGlobal_m348BC7578CA4AB0CB6191DCB42E2A308C089B7E0,
	VariablesManager_GetLocal_mE9DFD0DA22A3FECC7A2EC5B80D7835FD7DA2A823,
	VariablesManager_GetGlobalType_m2EE9E879DE7D0DFCA3E690E0383594FC01B3CDB8,
	VariablesManager_GetLocalType_mDE2BB1B779A51C2F498496222FDDC6E227675F70,
	VariablesManager_GetListItem_m0B4E7AE903DAEFD19B736DECD80DFB1DCD3610A6,
	VariablesManager_GetListItem_m531AA9CE909A9533F7BF47869268BBD30FD76C72,
	VariablesManager_GetListCount_m0432310633C87037D7A5598C94E3DE3814B7EFFE,
	VariablesManager_GetListCount_mA08E77E836E7071650705281A0B30514E8DFD211,
	VariablesManager_SetGlobal_mFDB69F586A4295F3098F8F4D556457F9D666D4A6,
	VariablesManager_SetLocal_mACFFD07C40436B8016966EA392CB1197E7E8B77C,
	VariablesManager_ListPush_mDA6C63F351F7859BE4E3C5DDD3D80E41E5222DB1,
	VariablesManager_ListPush_m068822A2F5DC31C4812A7F16C6C8AEF5628D8D7E,
	VariablesManager_ListPush_m37130CD8F7E639BF5E08DD225CB2E7EDE66257AC,
	VariablesManager_ListPush_mD9D5B3C603A1E55AAB930F2137C2EED75120B11A,
	VariablesManager_ListRemove_mFB1007A1F40A93A03F3049B4AAA53259DF5BAAF0,
	VariablesManager_ListRemove_m75518EAF8D13D873B208CF5FB431F54CB24861BB,
	VariablesManager_ListRemove_mBBDFED8C965C2F2CB866328C39B23491E9347C8B,
	VariablesManager_ListRemove_mF800A390D08F3BE97385930D4EB01066ADB7095F,
	VariablesManager_ListClear_mACE19808E051CEF677FED65DDC6D287678C1E4F6,
	VariablesManager_ListClear_m87A063B43C39DCC97075AF024FCCA95169F71239,
	VariablesManager_ExistsGlobal_mE67CEAA71A6D7BCFCA16E43CD258A5217523C26E,
	VariablesManager_ExistsLocal_mEADE537B9C398C2AE13FB4BC23436D269A03655C,
	VariablesManager__cctor_m535862B81D0BEA0CCD4C77A0E2EAD076D4C2ACD4,
	HexColor__ctor_m4B2A07A8A2407596B23FF262832968F9E25DA9A2,
	HexColor__ctor_m824844D36955C2C8A7EE6D00B1D5DCDB6F58A93E,
	HexColor_GetColor_m883288BA0EFCA4647652B3084166ADD0D7F75B10,
	GlobalTags__ctor_mF8F9A5C1B66787F4C960A3D0EDEEF68DB547A658,
	Tag__ctor_mAF76928BBE05D9258FB940356ACD82285A15994B,
	Tag__ctor_m20CC7478EA6D0BA45B024D3DCC2F5854BA90E26C,
	Tag_GetColor_m8CFBD2A88548D691DC2D03CDC847217B1044538B,
	Tag__cctor_m60B14CB5DD395F76FE50526492E6FF17AE72DC36,
	Variable__ctor_m817707EB54CA671257646F968DFBC6311BA1CB50,
	Variable__ctor_mCDEAA96C0A3B4AE5C56EC53AE40D5AAFA8A8A92A,
	Variable__ctor_m3AF3D709DAD361AAD55866002671FCA6AB7138A4,
	Variable_Get_m33D7E38A01DBBB67C6FFE3EC778FB0E2E9909EF5,
	NULL,
	Variable_CanSave_m69D5A9AE48E079EF91855720DF131BF4D13CF2F9,
	Variable_CanSave_m70EBE2B163489AAEB375BE8C180F8C855928C6A0,
	Variable_Set_m5284CBAFDE5E8B84714FB056194A38C8258682E5,
	Variable_Update_m92DC0E304AF020229E3CDDE5A981B5E926CDEC7E,
	VariableBase_CanSave_mE790667FC1D5CEAC939E74AECE4B4E4D8EC2FAD4,
	VariableBase_CanSaveType_mFD9709A8A029B318A965FB77355F44B341075E74,
	VariableBase_GetDataType_m0D1D9E35E0E9E0A4659B2E6C0188E5F5ED7A41BB,
	VariableBase__ctor_mD74869CAC9ACAFC053BE41A9BDCD9610807D559D,
	VariableBol__ctor_m88C0D20785A31263BF75FD6BDC66EE12AC245A08,
	VariableBol__ctor_m97A8D03588FB356482C37D783492A787A8144EDB,
	VariableBol_CanSave_m51164FB1A2B47EB2C976556FDE394AB71CFE88C0,
	VariableBol_GetDataType_m686144EE00FDB38454D0D63F507E5460DB6A9DE9,
	VariableCol__ctor_mDB8D18CBB79A4571E9DDF496973D0747475E60B2,
	VariableCol__ctor_mDFF47CAD8264BE947A3D6D56E9F607B5D9F7534A,
	VariableCol_CanSave_m96C81C94708E64AA4371D14B41DEFC77770FA5F6,
	VariableCol_GetDataType_mA2CF7EF6F4A1CBC62840977F07AFE87C70ED65E6,
	NULL,
	NULL,
	NULL,
	NULL,
	VariableNum__ctor_m11B275ECAEB20E83BD0D628655CB0C8D25C6E116,
	VariableNum__ctor_mA917E28D18A53DA1814E6F346B5A402D7EA2D9BB,
	VariableNum_CanSave_m2BD77DABB784DCC5C70765CC947B2B062B578D54,
	VariableNum_GetDataType_mFC6056B0E7F32EB2C786D2242D1280967CA12C01,
	VariableObj__ctor_mA3F3125C0D19BF62A6E46E2556B00C5ED0BBF30B,
	VariableObj__ctor_mDD01DD0970C8FE41C5D2AABD164C025ABD00100B,
	VariableObj_CanSave_m5DD3F37A5A7834EEE3D2417EB4DF8DF60F522BEB,
	VariableObj_GetDataType_mD861F6513A055C6E15B126A8D066026AEF38041D,
	VariableSpr__ctor_mBC01DBBBA8010E6277C17066DFCFBE2AAB2AB6BB,
	VariableSpr__ctor_m6348CE3FEF08A60F048CEB44487B71C890A8F954,
	VariableSpr_CanSave_mF20ACD13BBDE9DD09E07D0174B1E6B4ECF49AA37,
	VariableSpr_GetDataType_m0B8DF3DE28900437121861774E9D4589C92C2586,
	VariableStr__ctor_mD10D69670D205789AFD1DC01A0B4A960E9F9B24E,
	VariableStr__ctor_mA1A979C0417ABCEE3F27F995E800B4AABC57063C,
	VariableStr_CanSave_m2EF95118B3A20089D6C84F90936D5D62A1064052,
	VariableStr_GetDataType_mCA1B669BA93CD25B8633ADAE70E71D429854B31F,
	VariableTxt__ctor_m1F13B89995D702CC5EF7945265042D8654018364,
	VariableTxt__ctor_mF36548D1B226FEA766DE092432F46352C11E6BEB,
	VariableTxt_CanSave_mF194F50389C2F319D02D40AB369F77D321F8EAA9,
	VariableTxt_GetDataType_mEC8E495DAB13BE3BF9FD76501280B4FA8BB5D520,
	VariableVc2__ctor_m66E57DC33A2253378AA667BF8F20C058BCCE16AF,
	VariableVc2__ctor_mC84CC79F255AD77392ACC2C16F704CF87F0C5A5F,
	VariableVc2_CanSave_m7DB531418591C0C9F4E75C81D287D3130071A203,
	VariableVc2_GetDataType_m651023A2E794E459138EDE48AA90D7EC3B27B7E1,
	VariableVc3__ctor_mCE6657CCD45DDF19A10599F71E3D487D89AE027C,
	VariableVc3__ctor_mB27B168CB513854E80D390AC8A94E3B2EB0E37F8,
	VariableVc3_CanSave_m76FACBD873474548E93EDDD8E86206E06CA6CB62,
	VariableVc3_GetDataType_m23DE8E822DFA6CF344CA0001E4D89C306710EC84,
	GlobalVariablesUtilities_Get_m3FD248BBABD3D52B50C3CB414AEA4EC735DEFF25,
	LocalVariablesUtilities_Get_m68458BF76450E25497630F89AE4489483546B17E,
	LocalVariablesUtilities_GetSingle_mE71E316A5E101C34CA707FC4ADE15592100B2605,
	LocalVariablesUtilities_GatherLocals_m78BDC7157D713265B4A94BB8ED467AD623F177A8,
	LocalVariablesUtilities__cctor_mEA8F98AF672C2406DB96DFC614A75BADA2120ED8,
	VariablesEvents__ctor_mFCC31E1E935AFA9D37300DF5A046947D223ACC85,
	VariablesEvents_OnChangeGlobal_m957DDAD89B0BB71942C50615D38A178AFA25C9B3,
	VariablesEvents_OnChangeLocal_m4A4607D6655100CFD624CC5E8B9FE3C022A5F722,
	VariablesEvents_OnListChange_m856A41DBF9BAD81480C14F1F390B6FC49848D1DA,
	VariablesEvents_OnListAdd_m617D01F7DFC6163622A4AA354B0B22ED68AB3557,
	VariablesEvents_OnListRemove_m0E489D41716BBAA0149530347950FE940CF0A6E0,
	VariablesEvents_SetOnChangeGlobal_m1C1C4D14993A2F7F40B63D767810E61E63EC3394,
	VariablesEvents_SetOnChangeLocal_mD93F9D3C765CFDFD724367771F90F06D7F7CAE77,
	VariablesEvents_StartListenListAny_mE6F20D47D8B1E48E6B9AEB39F2B90BD220536941,
	VariablesEvents_StartListenListChg_m55B6C4F1523806096A727C34D8768459EE293340,
	VariablesEvents_StartListenListAdd_m41837AADE9B9A0BB707EB2545F51DD0993AD2219,
	VariablesEvents_StartListenListRmv_m4291D723AC8E9DBB0FA1D8BC43D656783997E6CC,
	VariablesEvents_RemoveChangeGlobal_m1BAB1DFDBE128CA5CD9DB2D9C501F281CDD85A34,
	VariablesEvents_RemoveChangeLocal_mF37775B101909670805DA226EB614922ADB2A5EC,
	VariablesEvents_StopListenListAny_mAB890A9E9C9064D306E7F7534F46789E6223ACB0,
	VariablesEvents_StopListenListChg_m8F9D18F05B2D3466A82CDE1E340122E855C464CC,
	VariablesEvents_StopListenListAdd_m6F1A8054B5EC6EA7DDBBD2CC9D02D49B870B4AC9,
	VariablesEvents_StopListenListRmv_mE09281BC22EAC3C23A0CB96BBAC1D868C694BC21,
	VariablesEvents_GetLocalID_m1F2A3819BEE241EE46E65BA00CEAB53110208533,
	VariablesEvents_GetListID_m86995987EAD00B588811B0ED7AB2637E5823EDAD,
	VarEvent__ctor_mFACCCD91990D9181703ED69D2831210F3ED63A4C,
	ListEvent__ctor_mACD2CDF01C6E809E6CD98FB14EE3BF839AD5C909,
	ActionFloatingMessage_Execute_m5DB2F53398A5D023EA3DAB96EACBBBE242D448F0,
	ActionFloatingMessage_Stop_m88D275FB87FFE0C6229AA50C8CDD3E49FB9B06E8,
	ActionFloatingMessage__ctor_m0620647B6661F6A4C4F6204FD01E762E693E803C,
	U3CU3Ec__DisplayClass7_0__ctor_mF5061944FC0A460DED1EA0C8588B936DACEE9EDD,
	U3CU3Ec__DisplayClass7_0_U3CExecuteU3Eb__0_mF9468594434EBB4FF71F43F24932628D5FF728BF,
	U3CExecuteU3Ed__7__ctor_m645B88BD53A458E607304C79B5A5D69EC4FBE72B,
	U3CExecuteU3Ed__7_System_IDisposable_Dispose_m074630D9FC92E1AE2788E076CA71820143E7E731,
	U3CExecuteU3Ed__7_MoveNext_mB88B7396A7EEC180CFD7E316C2642290FCE2536D,
	U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE43ABB7C01C2C671FEDD0BBFB7C5497148DC7B47,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_mFE2F97328C5088F606C650B4DF62ACA28328F7C3,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m09CF5B3C1EC26C1A1F053626B8B2A6E8A7658AEE,
	FloatingMessageManager_RequireInit_mCF51128970129CEBE0763D14B87EA1D8B9722E21,
	FloatingMessageManager_Show_m4B9E9DCFF3C83CDCD69B20DAD8437D477A504B34,
	FloatingMessageManager_CoroutineShow_m0220E1AE93E6EC41522C334BA5C5F1287A269900,
	FloatingMessageManager__cctor_m5357BBECC985BC2CF855B96FE84A398672C0B86A,
	U3CCoroutineShowU3Ed__7__ctor_mCAE5A30C39DEC18976BFB9583C017FCCCA30A36E,
	U3CCoroutineShowU3Ed__7_System_IDisposable_Dispose_m86C925F77A798544C527999C561D3874064C373B,
	U3CCoroutineShowU3Ed__7_MoveNext_m693C40E5D45D1E5C3C90716FD12DB1A246EA6EDA,
	U3CCoroutineShowU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m714EDE17A8EBE56F2D7BA04B146A1F94D5C2A1D9,
	U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_Reset_m9BEB6319F95DA2D06C30F20C4A7D55000B14232B,
	U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_get_Current_mEB27EBC56988E61DF2FF2BEB9968EBC2ED2CA3A0,
	ActionSimpleMessageShow_Execute_mC5832C6D843A71A1A4C29DA4676AFF28998EE806,
	ActionSimpleMessageShow_Stop_m409FCD8D60C2F5E1374CCF960FFE17E0C082D2F7,
	ActionSimpleMessageShow__ctor_mCD27905EEC1BB4EE9D1C1ECBB77ABF4AD8B081F6,
	U3CU3Ec__DisplayClass5_0__ctor_m021A16FEBAC869F1BA1503628EA07637905AA2A4,
	U3CU3Ec__DisplayClass5_0_U3CExecuteU3Eb__0_mBB5A1E7586484A3A536C0CA616D754A7499CC4BF,
	U3CExecuteU3Ed__5__ctor_m3F5262E9C558F8015181D212E910AF955EED9179,
	U3CExecuteU3Ed__5_System_IDisposable_Dispose_m388E6650772D9F9F1B1C94AA393E1C65D0E5743E,
	U3CExecuteU3Ed__5_MoveNext_m7E1D4587B5001C868150BFD887727A30C4B0D9A2,
	U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28E82C9CEF55D8CF9A58366AD7F3FC1071937101,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m551180D660E24ED9170D68C6C7FC8E25934190CD,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mB797C426D079B14DA6B07F17558837335739D50A,
	SimpleMessageManager_OnCreate_m17F697AC203A81CB424AD33ED661B1673C6BA36A,
	SimpleMessageManager_ShouldNotDestroyOnLoad_m97B004741FAD77D2BAC0B708D0424E0509F32623,
	SimpleMessageManager_ShowText_mB7F3F7C8A6F4C9AE2B6AD847E8AE10F1FE8D5FFA,
	SimpleMessageManager_HideText_m12114E862760EC6291F19B373868F7D30D51697B,
	SimpleMessageManager_HideTextDelayed_m1CB192B2F48BECC263728253FA9840D360B3CE64,
	SimpleMessageManager__ctor_mAE01CFB53C76F96ADAB30DBD4519C3DFAA6FEB87,
	SimpleMessageManager__cctor_m375E8FACFBF94EB72EB32FCA351F405D18DD3F86,
	U3CHideTextDelayedU3Ed__11__ctor_m7E5DCB81FCD6E15E8D6CC97EAD891CA0BD8F22A1,
	U3CHideTextDelayedU3Ed__11_System_IDisposable_Dispose_m50F21BC68D8A41B1065AFDDFBB4BC323ED1024C4,
	U3CHideTextDelayedU3Ed__11_MoveNext_m268FEE54D4811A9CDF53676B448B76C6B1F39EE5,
	U3CHideTextDelayedU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67242BD4F4EE571C0EFA15B38109ED78B3D974E5,
	U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_Reset_m5869D5BC10C92C2F71A32895DDDFA44A0A862BB0,
	U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_get_Current_m1C941C49AC049605C872428FD343A5D9990088AB,
	ActionMeleeAddDefense_InstantExecute_m16D5E914AA189A4ED65BED7767F1C393F8C11633,
	ActionMeleeAddDefense__ctor_mE2166250B3620BCB97CEDB6B6801CF4CEA69B0E5,
	ActionMeleeAddPoise_InstantExecute_mA8EE621C624FF0E0656AB6BA81EC5EC7716A0092,
	ActionMeleeAddPoise__ctor_mFC5CC9C570615BC93EC2509EB7AD7600728D05A1,
	ActionMeleeAttack_InstantExecute_m02288B94E2EC300F338F261E6E6E3769E73F4733,
	ActionMeleeAttack__ctor_m637FB043AE1C8A51D7EEEE8181D4DD8C93338514,
	ActionMeleeBlock_InstantExecute_m1FD78495D17F23AA130BB7BAA81F2D8B0CDA1BAC,
	ActionMeleeBlock__ctor_mC58B65405EB57E97C004C1A7197DB3511BD77FE5,
	ActionMeleeChangeShield_InstantExecute_m950EE64C9C203764F90BD94787CC83F7637693C3,
	ActionMeleeChangeShield__ctor_mACCD70ED62536553F41A8FA4870BC504B9CE13AA,
	ActionMeleeDraw_InstantExecute_m60EE8D4094AE766593B28D5B33D5EB932BBFA808,
	ActionMeleeDraw__ctor_m8AF7D07C9FCA9A6C251D2C1FD66A7F0E917F869C,
	ActionMeleeFocusTarget_InstantExecute_m3B20405B5252FC796FA35EE3C83C292B16FDB4EA,
	ActionMeleeFocusTarget__ctor_m5042A4FDF9293D7E734661AB2EE39D11CBB9254B,
	ActionMeleeSetInvincible_InstantExecute_mEAB4FC652C5155CF2A76FAE16FFF9738634DD0AC,
	ActionMeleeSetInvincible__ctor_m53F557E3A20620CEAAF93BFD38C6CC86EDF65FF8,
	ActionMeleeSheathe_InstantExecute_mAD1BDF3870340EEEDC7DEE13CC469BD68B7CB21E,
	ActionMeleeSheathe__ctor_m96B548D0C8B0352DC3AABEABECD248F317203B50,
	MeleeClip_get_Length_m057B66F2EC51F50FAC999A1FD5F7D2FAF743E633,
	MeleeClip_Play_m145D2D7DCA193673CD7780D8EC3D219DF2F23699,
	MeleeClip_ExecuteHitPause_m81C58CF9862BCAEF8C3A39F7E464E66541AB3316,
	MeleeClip_ExecuteActionsOnStart_m3091BC5BA369C8E91D55BCD24D7367F19A9B9580,
	MeleeClip_ExecuteActionsOnHit_m372829234FEDBBF6A65AEAD48B521C879D8A0D04,
	MeleeClip_ExecuteHitPause_m443EF74676E99E02E6B0A751C098887E02AA66F5,
	MeleeClip__ctor_m01AB68AC75673CE964ECC4D3EF01FEFFDF00B7BF,
	MeleeClip__cctor_m7935E438C993FEA23738841B9D2C3493C6045EFE,
	U3CExecuteHitPauseU3Ed__37__ctor_mA10EB23AFBB6F257F7D54CB541E96CDB237B2A47,
	U3CExecuteHitPauseU3Ed__37_System_IDisposable_Dispose_m1FF79EE075B38D2D8362870A57BF06D9A6802B80,
	U3CExecuteHitPauseU3Ed__37_MoveNext_m1A74DE14F8A0DE9EA1949C47ABF8949B17FCEEF0,
	U3CExecuteHitPauseU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9ECB2917A67CF28F30304842FE0AA6968E907F,
	U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_Reset_m4A85E677F743397D8F2C984245BADCD93DE11221,
	U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_get_Current_m7E7F2C0D3E1DE7198126B25DB63FF8E140EB3A79,
	MeleeShield_EquipShield_mE1C529D9FFC7DA28B8AF181BD2F24E782B011D64,
	MeleeShield_GetBlockReaction_m1D2E5CAE1A101CE8CD638C4BB552574C8CEB4145,
	MeleeShield__ctor_m9C1852F600272FEFC19FF30328B4DBD63C63BA24,
	MeleeWeapon_EquipWeapon_m87705EF81FBC464BED0F60A7B63BC2A74A24866A,
	MeleeWeapon_GetHitReaction_mD218623FCA5577CB51AC9717B84D8D102D8F2118,
	MeleeWeapon__ctor_m5B0DF3E098ABC7F7229519D135C1F57A4A29323B,
	Combo__ctor_m7B487AD37711BBCE1299AE970BD7512F89C8034C,
	ComboSystem__ctor_m323A2E7E095EDB3FC3B2331A8466CD600155BFC7,
	ComboSystem_GetCurrentPhase_m003D96E08A9A6AE3AACF3F39155EF19EAA1161AF,
	ComboSystem_GetCurrentClip_mAAEF47230735C2C31B42D2A9F571E9B23398617C,
	ComboSystem_Select_m60E1AF6624EECF2369FCDD60C46C738FB2C6685C,
	ComboSystem_Stop_m1580D65C4E26F2DCBF856A89276B7CC70C330A90,
	ComboSystem_Update_m0AE10E098CD95D038F4C4BE75422E1A45614DDA8,
	ComboSystem_OnPerfectBlock_m288EC6739D6F3DE91998DDDC8F85FFADE2BE18F8,
	ComboSystem_OnBlock_mAC05C140F27475AC456B879339BDAE2A3632A19D,
	ComboSystem_GetRunningAngle_m7F0EF7A270E6EB9DBDEBCC9AE14C4D342306BCDE,
	ComboSystem_GetInputAngle_m18534E71F2227E253DEC317996E8E6187632573E,
	ComboSystem_SelectMeleeClip_m27B4EBBD6CCDA0E16DD68EA7185D74342DFE79B8,
	ComboSystem__cctor_mA74F51B0A36E08F9AC7A639A3FFDB34CDB78720A,
	InputBuffer__ctor_mDF6659A814949AFF076C3594F3D3A24AB50C8B5E,
	InputBuffer_AddInput_mDF11397E9660D5B24DF49091E134E5338B0E96BF,
	InputBuffer_HasInput_mB8635C4C5E789965A36CEB44C3DDA38C970562C7,
	InputBuffer_GetInput_mF2FAB436E16AA4AD861BA3584DB4E0E0C868D06B,
	InputBuffer_ConsumeInput_m28C588D8A2728BFFFAC65A9ABFE8DC21C6B3A69D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WeaponTrail_Initialize_m48A7B378C9B8D3C085625972B50666F3B0583EFD,
	WeaponTrail_Activate_m218806A2E99FD98589D4318DED263577CD5111A3,
	WeaponTrail_Deactivate_m949F7A3E4FBF65F1B378BA158179E9645E4579F0,
	WeaponTrail_Deactivate_m76524862F457E698139470878AAD756ECA2B2AD0,
	WeaponTrail_Tick_m6D7C19B807B1996CF39EF822B89388EF446107E8,
	WeaponTrail_GatherData_mDE4AE9A3754BD64F6750A7853302CC092BF48BA1,
	WeaponTrail_TrimData_m9091947EC575F44FD201D450312D00C3FAA86308,
	WeaponTrail_CheckLifetime_m81BD6B257B188E67EB605AA7C552F7D34859CE10,
	WeaponTrail_CheckFading_m52E490A1D63746DA93010AACB207D4D76F820444,
	WeaponTrail_UpdatePoints_mB8B117896115FED9A1AB6F80BE6823104572239E,
	WeaponTrail_UpdateMesh_m9BD03EC22994F6A36CC61C423AEDA57874EE6E46,
	WeaponTrail_GenerateSpline_mB28D195AAFC4DB2816DC16CC66B464B23CD93B3E,
	WeaponTrail_CatmullRomPosition_mDB848058A980C0E69D21F4AFF9E61C575EAE1FB6,
	WeaponTrail_RegenerateMesh_m161649E00249D90E694B7364C90002B45499C17C,
	WeaponTrail_DrawGizmos_mA8D177D6AE63970436CA01ED094F9E6B14642897,
	WeaponTrail__ctor_mD092110EF39728A6C1BD1EA824612520845881FC,
	Data__ctor_mDA1CA317D8B44510B38FB82B1A3CCE618BCBB2DA,
	Data__ctor_mAD012A8800D04C34E5BF058F8207B904043670A6,
	BladeComponent_get_Melee_mB9E245D961F3BB1A87373D8E939D278F7C8C28F2,
	BladeComponent_set_Melee_m7071E3DCEB7520779AA743DA210BB810041366A8,
	BladeComponent_Setup_mC5269FD5B78DDD7C353634B357CF8453A6A75F09,
	BladeComponent_Awake_m20B193B1FF75EB6771764D19B241A1593A006383,
	BladeComponent_Update_m0F529040D33347C62566B8F98082B81D114C33AC,
	BladeComponent_LateUpdate_m3097A8A1876516322CF5B251C4BE859C30D06E65,
	BladeComponent_CaptureHits_m271AE4B474FF2D56B53631CD01D0139943207606,
	BladeComponent_CaptureHitsSegment_m176CD92D77F9002F398FA5DB3378DEECDCC9A0D3,
	BladeComponent_CaptureHitsSphere_m6BB1306C4E46CBE47CD3BEF8D3196D9B0D8DAE18,
	BladeComponent_CaptureHitsBox_m62A7E2880BE8ACF72D9415772B0DD628B3319ACF,
	BladeComponent_GetImpactPosition_mAEF1AE7C8F6D1F5418AEF91891D240E536650AB0,
	BladeComponent_OnDrawGizmos_m6F8C152C197D417C1E221E9D1AA2133612939BCC,
	BladeComponent__ctor_m759B7EB40E54838BCC29294149124E303E16B2C1,
	BladeComponent__cctor_m30292EA825571E54F4F6F17394BA453D95A06D6C,
	BoxData__ctor_m4379031EB9F1E1D541B8DDB4C0B835D72497F739,
	BladeEvent__ctor_m74EE44CAA1915FF0D4080552CE218D5DD762EAC7,
	CharacterMelee_get_Poise_mAE7DA4E54BA255648BD8839974782B5FB9759A83,
	CharacterMelee_set_Poise_m4A8BF6725DF27A0938FCF2C698694491FA1CE201,
	CharacterMelee_get_Defense_m507A720A94BCAB37B13ED5B740535DA9208302D3,
	CharacterMelee_set_Defense_mB3F50AE24CE4E58C8D6763D7A1CCCB2FA8BAD61A,
	CharacterMelee_get_IsDrawing_m896E27571073303B79D91CC2886EA9C0636D92CD,
	CharacterMelee_set_IsDrawing_m6DE5FA9449BA3AFE940FFCF9CBFE4C9C7EED6310,
	CharacterMelee_get_IsSheathing_mBD515D68F0139E9E1913CA37B3F8B9B10A735FE1,
	CharacterMelee_set_IsSheathing_mD90AAC761D35BF4D894733EC2AD265B6859CCAA6,
	CharacterMelee_get_IsAttacking_m4E8EE2CD01688E319C7166420F9342088E9B7BFE,
	CharacterMelee_set_IsAttacking_mA3B512832B1EAE92A05E64DE3791659CB78F8A7E,
	CharacterMelee_get_IsBlocking_mFF11D0FD9798CCCF39E1545039E591236C5C896A,
	CharacterMelee_set_IsBlocking_mCB612F17CA908BA0BD4188DE62ACAA49A7367D95,
	CharacterMelee_get_HasFocusTarget_m7BD8A39A0F492B250E44E3208F79B8B5C4DEBF20,
	CharacterMelee_set_HasFocusTarget_m590FCFD6C2DC40DF79BB59FEDAC4A1AAEA08D2AE,
	CharacterMelee_get_IsStaggered_mED35EB62FB65B67C7C358B584ABD64BE0B1D455D,
	CharacterMelee_get_IsInvincible_mB69CC21AD035E5B2B4093D9CABCAD13FFB681ED0,
	CharacterMelee_get_IsUninterruptable_m986E3B7AC009D01EE26B2B235AE672F24EE14AED,
	CharacterMelee_add_EventDrawWeapon_m729A717864566C772CF71410BDE1924DBC3AE947,
	CharacterMelee_remove_EventDrawWeapon_m3A5FA842827A19CBB1895C99397FA608F52E9D60,
	CharacterMelee_add_EventSheatheWeapon_mFDA0966CF05CA40A002E10DCE4C99DB694C5AC77,
	CharacterMelee_remove_EventSheatheWeapon_mFA2B6F3F7DE02A7CFA9A36FE638A2B044AD289C5,
	CharacterMelee_add_EventAttack_m2DBB7EEA0FB85D42038CE65C50B355CB02C782BA,
	CharacterMelee_remove_EventAttack_m8EEAE732C1B79F7BD62F5C72513543C2D361183E,
	CharacterMelee_add_EventStagger_m4109CC64CD32AA9507C988F3934BCC867CF25381,
	CharacterMelee_remove_EventStagger_m3E4FB53BF4351A020EDC55C7332F901CF14A99BF,
	CharacterMelee_add_EventBreakDefense_mB6BB87DFB00DB2B5C574CC8968782E6E6BDA702E,
	CharacterMelee_remove_EventBreakDefense_m6FCF86C0FEB98F29565FDA8088184CE910D40BBF,
	CharacterMelee_add_EventBlock_mF88A06EB7F4A0F2C1010BC793B5E4E29416468AB,
	CharacterMelee_remove_EventBlock_m8A34212AA2E12435FF6D7EB286882E490296ADC0,
	CharacterMelee_add_EventFocus_m634B32366980C642D31F60AC90C71CB7152BD014,
	CharacterMelee_remove_EventFocus_m2EF8A205D03A9353EC32768ECFE79B2142FC9674,
	CharacterMelee_get_Character_mC8213FA44F089EE7A6B5B06B0D7390E32E4CB5CA,
	CharacterMelee_set_Character_m78B44B240657959C2834FB7887BA2D07FE375DE4,
	CharacterMelee_get_CharacterAnimator_mA4B0B02AB3070AEE773BF16764D3D3796A1A9FB6,
	CharacterMelee_set_CharacterAnimator_m2AD046145B4043A4D45654BB2C01E0F7BE6428EA,
	CharacterMelee_get_Blades_m4E84AE0B4C496A680D941D9981142B0A81CBD87F,
	CharacterMelee_set_Blades_mC80F11AE570B6BD8E1339C79799EA35F12D6E0F7,
	CharacterMelee_Awake_m8941B52A3C884E1B937C0965022AB44F4AFF8133,
	CharacterMelee_Update_mF5BB207BBBF1490BBED738303F5001BEDBD37D95,
	CharacterMelee_LateUpdate_mBF9B44E2FEDADAE3BFFA16C88F39D16C678AD56A,
	CharacterMelee_UpdatePoise_mE8A2B55A212C6FE911FF3952780F76ACCC4AFB25,
	CharacterMelee_UpdateDefense_m3F4412825A479BA5000341B5B84FA08B7CD95E38,
	CharacterMelee_Sheathe_mFB1AED1FFEB02DFD57F1E5FAA86CB41C8E18C3E7,
	CharacterMelee_Draw_m00C457FC85DEA4F4DFC918137617E27E6357A39B,
	CharacterMelee_EquipShield_mEA6FB9A21F21372EB26FDE824290171087351E06,
	CharacterMelee_StartBlocking_m4A206BA912284E9EFF5B66E4A46DE114D0CB4EDF,
	CharacterMelee_StopBlocking_mC63943524C106212F7873D4A836AF31E25319DC9,
	CharacterMelee_Execute_mB6253E6A77A5D380E040F96CB08AE9FD241C0E8B,
	CharacterMelee_StopAttack_mDBD5ACC7F4FAC6CCC74029BD7B5B74149AE11BAD,
	CharacterMelee_GetCurrentPhase_m76B691033676DCBC262722BCBFB163AEE83C8E37,
	CharacterMelee_PlayAudio_m66ADDBC2CEAC0F73A0C08585D8C23D9AEEF93D4E,
	CharacterMelee_SetPosture_m3CC6A16B0ADD742C49FDF7BBB724C6BC99038B6B,
	CharacterMelee_SetInvincibility_mF4DB8C8F23C044980FC6CE364CAD460DCF93D46A,
	CharacterMelee_SetUninterruptable_m9B72D916ED6563F44807B18F6DDA889E645F6487,
	CharacterMelee_SetPoise_m306F507038AEEF64F9671AC4A58B619B86E000DF,
	CharacterMelee_AddPoise_mA561D732BB23B08B40AF74AB562732200E712B0C,
	CharacterMelee_SetDefense_m252B2B406050164C8C2FC7BB1AE519684C980020,
	CharacterMelee_AddDefense_m8A0477CCE6A3F3A07684FBF6E343D000F68CFC88,
	CharacterMelee_SetTargetFocus_m83CAA8DD28979D0F6E932204D7582282E962D8C6,
	CharacterMelee_ReleaseTargetFocus_m5CF26E648497D7F12D64F24D1D5FDD49948D6477,
	CharacterMelee_OnSheatheWeapon_mF3B06846DB7B8D3BF8ED1DC54E70E86D83D29DEC,
	CharacterMelee_OnDrawWeapon_m2579D0BDE8B651B3CEDF8264C3EFFD866FB198DD,
	CharacterMelee_OnReceiveAttack_m2FA475982B7864D74478547C2672F9C50005ACB4,
	CharacterMelee_ExecuteEffects_mB8A680FF3AD3F3D0AEF9A7B8A89F5363DBA7795C,
	CharacterMelee_CanAttack_m92732DB6E3FC556B2DB6DF8A353BE47CF399212A,
	CharacterMelee_ResetState_m19A26A0A528F8EDCE65C505EE64233327E3AA73F,
	CharacterMelee_ChangeState_m8DF5A2669EB59E52C2562C2DB52E364132E067D7,
	CharacterMelee__ctor_mE2B52659BBE9732B63F2122C0B5CB649CF12D900,
	U3CSheatheU3Ed__102__ctor_m786A68C4F750B9E38A58F10D081860846F94A69D,
	U3CSheatheU3Ed__102_System_IDisposable_Dispose_m689042F48C397182F117B6C74794F679471DEB34,
	U3CSheatheU3Ed__102_MoveNext_m84C793A0BB74EA843CB9773216A311066982CEC8,
	U3CSheatheU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1FEA64D0AD51EBDA277E8E71DA96251B74C7CA9,
	U3CSheatheU3Ed__102_System_Collections_IEnumerator_Reset_m8D077FC808E8BC30A3CEE7FE46311E214B99C354,
	U3CSheatheU3Ed__102_System_Collections_IEnumerator_get_Current_mD4C206E05163045BB434D18C816174F17F2B2741,
	U3CDrawU3Ed__103__ctor_m3CDEEE7F8DB99DC8D219743C706D8827F2422D56,
	U3CDrawU3Ed__103_System_IDisposable_Dispose_m24DBACDBA5ED681EB17250F583B4BA16E82115C5,
	U3CDrawU3Ed__103_MoveNext_m527C5CA66B9BDC70D387971339CE7C8843A128FE,
	U3CDrawU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3BCBB31D4FD10EECEE363E70EB8F06ACBB66B39,
	U3CDrawU3Ed__103_System_Collections_IEnumerator_Reset_m6768D2D348120C62AE0E8148B25D55D1CA19CBE7,
	U3CDrawU3Ed__103_System_Collections_IEnumerator_get_Current_m6E02C65508D6A1B59269C5B13E0D3A071A40EAAE,
	TargetMelee_SetTracker_m1FF9C444D4292CB8BDC45C5A5B23FC8688609784,
	TargetMelee_ReleaseTracker_mD8E962BDF6932AF0A80DF85B75E9C281F704ED88,
	TargetMelee_OnDisable_m269D814FF98E0B8DEA2FDA2D23A5283B94A87FA6,
	TargetMelee__ctor_m00B54E11CCC94EFE4911331EDBC306033DDBCAA5,
	CharacterMeleeUI_Start_m642ABDF11C7614F3F7854CCE8A3731782E03087B,
	CharacterMeleeUI_Update_m8F25519B4EDCAE910FE4AB321584393AA2D819F4,
	CharacterMeleeUI_UpdateUI_m43FB849870298E79B14501FD72AE7B0FA2130718,
	CharacterMeleeUI__ctor_m39A6937AF1BF695D648DD9111C0007D5257C1939,
	ConditionMeleeArmed_Check_m8E04A02E2A90A70342B4C1B13AE95512A8675598,
	ConditionMeleeArmed__ctor_m5DD65423D2822D909635A4D77171577CE5DF6479,
	ConditionMeleeCompareDefense_Check_m7D7AC8463148AA6CC7304DCAE9CBDA8735C5247D,
	ConditionMeleeCompareDefense__ctor_m0736EC8B44B9F64193FEA0528D26E64AAC386CBC,
	ConditionMeleeComparePoise_Check_m54F2082ABBD77A6F43F21BF93CDBDD9A7804E5FF,
	ConditionMeleeComparePoise__ctor_mB536701990A48EBF9D2FC9D6F51C7FBB9EB5AF2D,
	ConditionMeleeFocusing_Check_mB2B46C9D015CE6429ED2255E50FE4A30A8345BCF,
	ConditionMeleeFocusing__ctor_m0C09B7D27228BC73DE8FB68A521E4E1217F1256C,
	ConditionMeleeIsAttacking_Check_mAAE8332F1A59DE0FA82FC67899B28A04E5414D5D,
	ConditionMeleeIsAttacking__ctor_m4B99A3D4B7296B1507866CB2A4E3B912D63E97B0,
	ConditionMeleeIsBlocking_Check_m8245FABE40909C80BA0837EF24437ADD1AA1236C,
	ConditionMeleeIsBlocking__ctor_mEA2C71CF9DE2DA33864AC0185016B4F02836957F,
	IgniterMeleeOnAttack_Start_m01450F525A7588FA986B12930820B368227272CF,
	IgniterMeleeOnAttack_OnAttack_m0CDAB8D0101A5D17118F86564884041D84C804D9,
	IgniterMeleeOnAttack__ctor_mB6A9A1BAFC86166D933580FB50FA98727A5C4CFF,
	IgniterMeleeOnReceiveAttack_OnReceiveAttack_m5BC3422DA2AF9AEEF1CB3E0D8D5DF053C6B7D499,
	IgniterMeleeOnReceiveAttack__ctor_m3D706E8B9553F6DED688B2DF718E513D905C0D8E,
	DatabaseFeedback_Load_m9FEBAFA74F5C9F3F6DDEBE4CF03895336FBA346D,
	DatabaseFeedback__ctor_m12BBE445B8CEF0CED24DAEEECB7542AE84DE48B1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PoolManager_OnCreate_m1740B5569145C3429EE6F7C22920AF83DD6C1C3B,
	PoolManager_Pick_mBC9C72B9F2CD73E6618D3411AE119206285D144F,
	PoolManager_Pick_mE9F93D3CF2FB602C98BDABE3E1BD9D29846AD753,
	PoolManager_BuildPool_mC7DC3538EED00977F47EFB8A4AC317E107D7DD19,
	PoolManager__ctor_mF148809988EBC891C00666A0131E8C9A6781B048,
	PoolData__ctor_m7039BCECA48F973B57743AC22192A50BAE0530DF,
	PoolData_Rebuild_m0692CE662DDD0FDFF2F93471A5915DF514A2E888,
	PoolData_Get_mB39181870B756FD479EA6A48322B67316402B426,
	PoolObject_OnEnable_m805B8BF963518A0C109795CF13C0414F566CFECF,
	PoolObject_OnDisable_m8B108CEE013E7095CD47C72FFE7DB8A3A9F08810,
	PoolObject_SetDisable_mAA2DF8FF1FE4B2A52BDA18F7D4D9711A81591C51,
	PoolObject__ctor_m1131B4A605ED58F331E22FC901F54F39405F5299,
	U3CSetDisableU3Ed__7__ctor_m82A6D08FFA1F31480B620ECA12FC75F1CAD29BCC,
	U3CSetDisableU3Ed__7_System_IDisposable_Dispose_m245E04359C4A3FC3EDA5E332275E22D78084C139,
	U3CSetDisableU3Ed__7_MoveNext_m956B34E92F14BF324F2FDE8E264457D7D5D78BA8,
	U3CSetDisableU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB212C3BF8612759E13002DB305FDCD7C5C2FD19B,
	U3CSetDisableU3Ed__7_System_Collections_IEnumerator_Reset_mF5D91E3D2FF98620E0869B07A72911E72F8F3B46,
	U3CSetDisableU3Ed__7_System_Collections_IEnumerator_get_Current_mB428927DA65710A66D860A096A09C621EB5F7189,
	ActionsAsset_CreatePlayable_m704653C2BADFB6282F5A4510C492BF9C2A7706E2,
	ActionsAsset__ctor_mD53F52339EAC828AF6BC73F880821093CA6FB90C,
	ActionsBehavior_Execute_mB21DF5847201534A0D4528B504D89E92DEDF0F94,
	ActionsBehavior__ctor_mAB09895D35B1EBC6C3B4CD2DA7E8121445B0130E,
	ActionsTrack_OnCreateClip_m39DEEB7D4557F30D0B464F31DA8AFEEEBDAD1E4F,
	ActionsTrack__ctor_m3C49574A6D505E9898B700B3E2723A755C48CFEA,
	ConditionsAsset_CreatePlayable_mED74F36313F18A355D301AF17EC7089FA8F80841,
	ConditionsAsset__ctor_mA9A2E524F85797DF109B141DD4DF6ABB56D9D7F2,
	ConditionsBehavior_Execute_m4900D0A1AB2EF62874604BCBFEC93F003973894A,
	ConditionsBehavior__ctor_mA15A77D6ED2B68D374A82D0A1CF44898D5694FD8,
	ConditionsTrack_OnCreateClip_mE51236CE9FB2F1547D9746D264C9950FEF2D994A,
	ConditionsTrack__ctor_m9EB19F56B614B8535A003EB9F7B610CA47564D3C,
	NULL,
	NULL,
	NULL,
	NULL,
	TriggerAsset_CreatePlayable_m6A4FBE5BBF6488EFABE1BED49CC854034BE3ED0D,
	TriggerAsset__ctor_m7D555C061C757B78464A5E9A2D2443EAB8590E27,
	TriggerBehavior_Execute_m4E285ACC9B93C4BC34E065F2F570158EE97CDDE1,
	TriggerBehavior__ctor_mC6BEAB9AB0A66F0B2C698607961DA5C594B66C85,
	TriggerTrack_OnCreateClip_mDF9BA9CC2D13B44286DF9DE835F84C35E158F193,
	TriggerTrack__ctor_m321A723283AA9AA2770BBFB491E3A57A51C4D8D8,
	LocString__ctor_m986B98F23C0524D832B4A4804C31F97E70D3920E,
	LocString__ctor_m097ABEFEB1D6F84F29B29C793E4713851DBBAF52,
	LocString_GetText_mAFE912D54FEADAE5EA82F2B876FB661A6D2CDC5B,
	LocString_PostProcessContent_m1C825919507CB5F543B58373C2F1439976C5C66E,
	LocString_GetFirstCharacterIndex_m5C3894E88E370D88CAD6A4A54389B8A1408B317E,
	LocalizationManager_OnCreate_mC788076CCD62B8710CD52FC807540EF8793236F7,
	LocalizationManager_GetCurrentLanguage_m58F85828AF75FD1555C5BAD5D01798948B3D2631,
	LocalizationManager_ChangeLanguage_mD7918AD89B2B1D636EF0203795C36EE3F57C4082,
	LocalizationManager_GetUniqueName_m41AD2A1D6670BA7B43980B6EE704003ED1DA714B,
	LocalizationManager_GetSaveDataType_m4C69BBD64081E826DEA76D5FB3E839A15076C858,
	LocalizationManager_GetSaveData_m09AC38A527A2689977EA3D7CA028ACDB53373019,
	LocalizationManager_ResetData_m40C7B4E5D129ADA95AE22DEFE7D474CB3BD281BD,
	LocalizationManager_OnLoad_m6DB62E849DE4B1CC6994F1B8987AAA891E41523E,
	LocalizationManager__ctor_m4A685549C117DA9419A5EE08DDD8DDB5A19B9D91,
	TextLocalized_Awake_mCDD92F0751D7B409EE3AC9C5F1345A05BCC6BD5A,
	TextLocalized_OnEnable_m2D375AEB4F363D6182FE54072AEC937EB08489B9,
	TextLocalized_OnDisable_m46511998C77900FAFCA6E9AFF223B768824D2965,
	TextLocalized_OnDestroy_mED057D8C1E897B388626367BA2D59FD259A67E76,
	TextLocalized_OnApplicationQuit_m0F72C9DF0B7E4EBDEE78F5A9F49E0BC9420CE87D,
	TextLocalized_ChangeKey_m0CE6D3CB09E3A6813CD623528F88E2CBF920FA4E,
	TextLocalized_UpdateText_m792EC099FE230F4ECA5C5C3BBB577894F75A3A67,
	TextLocalized__ctor_m9E6126C2CB76D8C51278BADDB21A27B5C3FB941C,
	DatabaseLocalization_BuildContentDictionary_mC3252B3E16D1B299ABD4310D1B935ABC50D0CB14,
	DatabaseLocalization_GetText_mBB31A15CA732B8D1CD4C4A7B02ECC9B253DB9B03,
	DatabaseLocalization_GetMainLanguage_mA3B7AA282E2EE7FBB66853FD9BE499B07DAB01FC,
	DatabaseLocalization_Load_mEFE5BD0B62E3F136A089F215FC943253D1692CCF,
	DatabaseLocalization__ctor_m2C765FA3FFF37B5A514183B2D6D6715F9D9E1A98,
	TranslationContent__ctor_mCFC977FCE2D564D5907C7DE3A10D9535CD2F609B,
	TranslationStrings__ctor_mCF12F9FFC08822DD44C7F74E912D6D18209ABC8C,
	TranslationLanguage__ctor_mC56C7D588DE32303FE5E34F76FBCDD70F5EF9A11,
	ActionCharacterAttachment_InstantExecute_m8C4D96CE49D527A73D9AC9E0D9567FA980B4CFC9,
	ActionCharacterAttachment__ctor_mF157FC2204E9CD3B84442B9BA3E4E372942896C8,
	ActionCharacterDash_InstantExecute_m016D9C2ECAAB41C15C4927F962B9D8497B329D5A,
	ActionCharacterDash__ctor_m9DA50E983A77B2C4F97C2BF5DD1242412B907476,
	ActionCharacterDash__cctor_m02566C445BDA87220F46DA29EED8713F96FD14F5,
	ActionCharacterDefaultState_InstantExecute_m8F1FABE74E9BCBD0E74F13B6B61414F5E2231153,
	ActionCharacterDefaultState__ctor_m0C4843702BD4B87B37012E3C1578E6566D06C3AE,
	ActionCharacterDirection_InstantExecute_m0C775ACF997208305E667AF28AF67AE4D305C8B1,
	ActionCharacterDirection__ctor_m69A6C0CB18202C5F3F9565D51185F244F75A8CC6,
	ActionCharacterFollow_InstantExecute_m61A078127F6F88DE8ED21853D219797A18666BA8,
	ActionCharacterFollow__ctor_m7A780C1AE3E9DCFC0807460A8BDBA0635982A962,
	ActionCharacterGesture_InstantExecute_m3DD3007D00A1FCD02115AB27E4183EFD156640A1,
	ActionCharacterGesture_Execute_mF079124C1F71B2CBA071614DF5D961DA403013ED,
	ActionCharacterGesture_Stop_mB3FA4B81606471F6479DC3945072F3DBC8FC6C63,
	ActionCharacterGesture__ctor_mEAECA3E1F17C2179CE9B7D9D05A672137867F6EA,
	U3CU3Ec__DisplayClass10_0__ctor_mB7A2A28D631824113D412161EDC4A21417C5D11A,
	U3CU3Ec__DisplayClass10_0_U3CExecuteU3Eb__0_mC78DA78906F82A06AB7527D2ED0966D8C16CD0AD,
	U3CExecuteU3Ed__10__ctor_m3C6F29778E65D111F6E51FFF293FF6AB4E1D8B54,
	U3CExecuteU3Ed__10_System_IDisposable_Dispose_m4E1B6B7427CEFE2C3661378F63CEA456C34B7F18,
	U3CExecuteU3Ed__10_MoveNext_m1F096BB3032B73E9F3A60AFA4EB8957264232136,
	U3CExecuteU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80DEEACBD0C640866FB5D092E8FFE518CFA782A3,
	U3CExecuteU3Ed__10_System_Collections_IEnumerator_Reset_m5DF0124AAAD326C0E6C5520768B34B7C811DBEB6,
	U3CExecuteU3Ed__10_System_Collections_IEnumerator_get_Current_mBA7CD44299C5921344680D9462F7C272A5782DE7,
	ActionCharacterHand_InstantExecute_m36A1B04105617C5AC2A97A3EE37532AF53D3CBAD,
	ActionCharacterHand__ctor_m50BCB72299D9CE3BA80F62C510360D5195FA5FD3,
	ActionCharacterIK_InstantExecute_mC7030131D0830FDE9B6B5674D4366B9ED03DAF0A,
	ActionCharacterIK__ctor_m440A41B7AAB7AAF31BF3ADA13578BAA5D076EB3F,
	ActionCharacterModel_InstantExecute_m9A8A984A84F7EE47A3040850C855373EAEFCF5FE,
	ActionCharacterModel__ctor_m326DB4721490BEF9A5486B031ADCF56555D86B3D,
	ActionCharacterMount_InstantExecute_m8EB7506F1DD5F5B533F504B8C5DD7E1497BEC0FC,
	ActionCharacterMount__ctor_mE4E07F40FF9B4D5D5879D359B6E63CC38081F9AA,
	ActionCharacterMoveTo_InstantExecute_mF374F876B0EAE7298F9A5EAC1CBB5F994055506E,
	ActionCharacterMoveTo_Execute_mFDD8702F438971F618BC2CC5070F79F5D2D755B1,
	ActionCharacterMoveTo_Stop_mE1CAC5FDC5431AEDBC7EAB28A036318168199FC3,
	ActionCharacterMoveTo_CharacterArrivedCallback_m41E769537DB11BCB08019FEEC8CCA2C798D640B2,
	ActionCharacterMoveTo_GetTarget_mDE4F8E96945D8E55CBAF320112A7773083E777BC,
	ActionCharacterMoveTo__ctor_m197BD02D857EB24E2F751EC399EA5FBD94C3C17A,
	U3CExecuteU3Ed__16__ctor_m11BFD9B002E8E393AA4F150E4D26ED9408BA65B1,
	U3CExecuteU3Ed__16_System_IDisposable_Dispose_m76698B7FC09D4CEB478E55D7D28CCDDA5FD7A03B,
	U3CExecuteU3Ed__16_MoveNext_mBCCB0DBBFB7AACF7DD74A7EC48CC56C0B52B41FB,
	U3CExecuteU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14CCFEB1097ADA90AF91AB66664339DC1FA086D1,
	U3CExecuteU3Ed__16_System_Collections_IEnumerator_Reset_m954AD4B93FCE4A094FE5D1EA875B8516F283DE95,
	U3CExecuteU3Ed__16_System_Collections_IEnumerator_get_Current_m1819E8E5ECDC100A7ACF93D0787045CFBA63EBCD,
	ActionCharacterProperies_InstantExecute_mD4D6C34C631F5ECAF24CFD52895414F2E2B67833,
	ActionCharacterProperies__ctor_mE77245BADA459257C41FC16CB8F4DBF94D94C641,
	ActionCharacterRagdoll_InstantExecute_mF39B6F752543AA04D5E77C900F3ED8431D0BC857,
	ActionCharacterRagdoll__ctor_mECA7EB049291FB52619D6FF638684A67D7835D1D,
	ActionCharacterRotateTowards_InstantExecute_m86CC99C0A1FAE6F61777FBEA6AD4A50C6FBCC7F8,
	ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B,
	ActionCharacterRotateTowards__ctor_m5A5EE2F838912B9496647F75EC9078A15983EB1F,
	ActionCharacterRotateTowards__cctor_m633E8E24E32AFA8546526363078EC65716991C02,
	U3CExecuteU3Ed__7__ctor_m93A9ED4176E3050FF375EC8C119F4EF8DF71FE74,
	U3CExecuteU3Ed__7_System_IDisposable_Dispose_mC69556267AA0ECA60558F102F170DE67D89E5E2C,
	U3CExecuteU3Ed__7_MoveNext_m4F67D87C429C01EC78CB5C61F26966CB6AD2D1E6,
	U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF92FCFBC97E98DD5FA109F65537870A658014802,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m28C693D9D027244014063A86657F20629EB0D302,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m89023EE397F53C4FA098252012B715C3A57DF914,
	ActionCharacterState_InstantExecute_m3E9C996496A61AE2E7EFE8619041C64215E016A6,
	ActionCharacterState__ctor_m0DB3055CB95207A10FEEC919955E446B52DD9EE1,
	ActionCharacterStateWeight_InstantExecute_m921C1DBA8FC0723E21934DD4CE712CD7056B3D78,
	ActionCharacterStateWeight__ctor_mFDABAFE6C33BDCB9D9BD8435008EF02A43D84C80,
	ActionCharacterStopGesture_InstantExecute_mC7A8F3652E5FAAA234E72F3A54B5D6B0BE0F1254,
	ActionCharacterStopGesture__ctor_m8ADFEDC5474333898AB5F17DD9B9E18F6B4908EF,
	ActionCharacterStopMoving_InstantExecute_m0FF72E2A8A537B5640AAE6718D00BC2C0C883A4D,
	ActionCharacterStopMoving__ctor_mA2D5567662CD48F48D499E7E16EA87C328138D0B,
	ActionCharacterTeleport_Execute_m9542BA250FECE50E3E269827B77E3E44633BA65E,
	ActionCharacterTeleport__ctor_mB1E70BED18B4D4BA054C88AF179CE9C2361E2DCE,
	U3CExecuteU3Ed__3__ctor_m0229D5A895D47865D75B9CA9E6E669E1C2F14465,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_m895F9261CA1402B728553CFF4F16F2F936BF7F9C,
	U3CExecuteU3Ed__3_MoveNext_m254B83199E4B0C4C0B2F41C65347478D8AE65A8E,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m105C970E77214FD89055FF4D88816CB5BA8E1938,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m1A8C9CD34172B9F3BD01A4CBF933EEEE5DBC06EF,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mE70B6EA23875886C4CFBEC39F12808AC10AEB884,
	ActionCharacterVisibility_InstantExecute_m859CE6E81F893FD528FFEC47141215AFB9E58E92,
	ActionCharacterVisibility__ctor_m299DB100418CCF7DBABF9AD1D23092B2570F501B,
	ActionHeadTrack_InstantExecute_m5AA5607B3A75929917A34952DEFB56E4385BECB2,
	ActionHeadTrack__ctor_m53AF509981C178C41AA71241E847AA8CC8F56E62,
	ActionPlayerMovementInput_InstantExecute_m3D59A8951BCF014F763E687E533E23C57D13042C,
	ActionPlayerMovementInput__ctor_m1115C9009DC55F16D320B9846D82CDDD03ED0080,
	ConditionCharacterBusy_Check_m38374431622CC8DBA8F003A13C746E2DA3E59639,
	ConditionCharacterBusy__ctor_mDE29B4DF1EBF05FFCCDDA6702870348D14B85402,
	ConditionCharacterCapsule_Check_m360848896C6B11C0DD02AFCF39DEF4BB2C90DDDA,
	ConditionCharacterCapsule__ctor_mF4290842373CD5AB95C16FC6E733536179718293,
	ConditionCharacterProperty_Check_m97FE43C913730292488D61124FD6194FBA6EEFA9,
	ConditionCharacterProperty__ctor_m00E4DFF472BECD86B761311BB09385954A3BB4EF,
	IgniterCharacterStep_Start_mE495B86E1AE65C603D5684F1F7DCA90DA44D1707,
	IgniterCharacterStep_OnDestroy_m5E684025468519EC328E9955EEE4A711A6D0F803,
	IgniterCharacterStep_OnStep_mB182CD26BA5145D1482B283B6F3DF32B7B7CEB06,
	IgniterCharacterStep__ctor_mF7A7300AA835F4367AAB0970F43B2A0B168709EF,
	CharacterAnimation__ctor_mECE0194E90AC33560A08A40C7A80D9B0A767D008,
	CharacterAnimation_OnDestroy_mE0A0D66618ABBAB6204FF6C52B192051F1EB6F55,
	CharacterAnimation_ChangeRuntimeController_mC6C3AE9FAC858E74E356B64CC275CDDD55E28985,
	CharacterAnimation_Update_m075A061338D6ED160DDAEC0A80B4EB0905BBA6AD,
	CharacterAnimation_PlayGesture_m5F14C1053A3B3249AF1DC6C7584DD9219B0F369E,
	CharacterAnimation_CrossFadeGesture_m0BD134E9120E99D9005B2FB69350CDA683CCCFB8,
	CharacterAnimation_CrossFadeGesture_m1F6197A3A14AB8E9EE47EAB409CE37AC6A7BB230,
	CharacterAnimation_StopGesture_m6B605B6064CAEA3005E49E42F6030F81FD764E1B,
	CharacterAnimation_SetState_m5E29EBF5A63F165C4ACC4018BB45E07768406826,
	CharacterAnimation_SetState_mF674B49D07C0F6E9E1A28331F0189379391B5CDB,
	CharacterAnimation_SetState_m8C3CE69D37ECC30B3B9F9C352B2A3D365942A4D2,
	CharacterAnimation_ResetState_mFECB6FA55444023312D4DA06810D1BFB4FF96544,
	CharacterAnimation_ChangeStateWeight_m461E96C71DE77D892542D6B33D514AC35F01A0E7,
	CharacterAnimation_GetState_m47541F74096D58EFA514A6332366FDEB8A2E4290,
	CharacterAnimation_Setup_m95D7FDF0D642CD8FAD9768CF04578641D673C247,
	CharacterAnimation_SetupSectionDefaultStates_m4FFF84A9ED4A3ACBC1BECE4E05BE5A19F755D724,
	CharacterAnimation_SetupSectionStates_mDD012ED29D5D2AC5569DBEAE3F49A24846B7BF4B,
	CharacterAnimation_SetupSectionGestures_mE15766A62A3611E6599816E523CB54B9CAC5581C,
	CharacterAnimation_GetSurroundingStates_m79EF98DD4ADF79B342880EFF41AFC35A1DA9EC8C,
	PlayableBase_get_Mixer_m28AB0F73632EF768973B8B3B05B9C9C7CE0AA272,
	PlayableBase_get_Input0_mB9976B380FCEBC97F3D6E2984B0C94E273835240,
	PlayableBase_get_Input1_m74BFEF7D4346A2CF70DF8A9CBBA6043F4908D6E1,
	PlayableBase_get_Output_m534F11974C308079426866408B7967446C3449E5,
	PlayableBase__ctor_m650AB8F71D7961C0A1A635D031B2381045D46048,
	PlayableBase_Destroy_m7ABBA0E75A397E8D32822725CD532831D57588A5,
	PlayableBase_DestroyNextFrame_mD1CA8E6618014C5F0FF63CFB4CC580E8481DB93C,
	PlayableBase_Stop_m656CAD3433BDDCBE1CF6B72A88B68C167B1F4DB4,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayableBase_UpdateMixerWeights_mEE1D0D70A595A742EC7D8C1AA4E002683BE3DF9D,
	NULL,
	U3CDestroyNextFrameU3Ed__16__ctor_mC2D8B87272A5FF7978C249A18186598873F2B420,
	U3CDestroyNextFrameU3Ed__16_System_IDisposable_Dispose_mD1D6D3F8B8A5047BE75FBBFD0DCD02F05CF12971,
	U3CDestroyNextFrameU3Ed__16_MoveNext_m5C7E1C93D78826B5A8D6EC6CB889F7904555BE43,
	U3CDestroyNextFrameU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70A9E325F1FF7756263DDB8DF254F4C1ABB5E9CD,
	U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_Reset_m3FB96BB157E9D36F17571507B075F3A3B60EEB6D,
	U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_get_Current_mE41FA7C32E7D6344416ECD3407387C2AD2F5365E,
	PlayableGesture__ctor_mC0821B6642BA9F558D8DA138B812A84ACF119989,
	PlayableGesture_Update_m241AE0C17DC9C9F90AC23522F1FC358460F6821C,
	PlayableGesture_StretchDuration_m154E0D9684BD3E88E13433A9A253F79391493729,
	PlayableGesture_Stop_m44CB6ECC264926D3662AF8E0634D11552DB0FB28,
	Parameter__ctor_mF1BAEB0C446EF259DA381E3C86A2246909C7BE05,
	Parameter__ctor_mF986F68A136BDB460D180D5CF86785FB563A38CF,
	PlayableGestureClip__ctor_m145CCCB60EAC2FD5773741D32C1343F59D471370,
	NULL,
	PlayableGestureClip_CreateAfter_mE658AC514265D18636A9D616D41E5F4BDDC2A267,
	PlayableGestureRTC__ctor_m81D3C49E928781B5677A25BB041E0BD5CD68E589,
	NULL,
	PlayableGestureRTC_CreateAfter_m171D1CC7E4B96BD20FA8DA9906BC797FC6B791A6,
	PlayableState_get_Layer_mA0A0FEF3E34BB52210BEB65292E152C37F4E5305,
	PlayableState_set_Layer_mDBC6CF2A5E1072D454BCD7F6633D52C18E7C04E2,
	PlayableState_get_AnimationClip_m86C11355A72EB8137CA039DAB5123DD11D9AB080,
	PlayableState_set_AnimationClip_mAC5DDE460E808A580B977179B28D9EA95CDF44A8,
	PlayableState_get_CharacterState_m42DDC145AE77AB341C5AB3B0C78C34E0B6815A60,
	PlayableState_set_CharacterState_m79263DC2A30FB8420BB2EBF7197CA9B823ADA473,
	PlayableState__ctor_m03254C46A94C917CC537877C639A3F50281F3951,
	PlayableState_Update_m5A05BE2C69C7AF3E45EA0318C337C00D130A8A04,
	PlayableState_Stop_m5F62B094147F46E2EB83BBC5BE107F7951E4DDE2,
	PlayableState_StretchDuration_mB01FD587E576FDFBE3E8BB47BE6A99F31F48EF32,
	PlayableState_SetWeight_mF5B2ABBE5E328232A82947BE35C67301BB5FC039,
	PlayableState_OnExitState_m8C7088B95C61444B365ECFCF357247A99E11378C,
	PlayableStateCharacter__ctor_mD647A3BFD771CACA7FE548625FAB3CEE0F2AFD2F,
	NULL,
	PlayableStateCharacter_CreateAfter_mD3174E0DE0FB1886551378074FC02AE8BC6FFDF9,
	PlayableStateCharacter_CreateBefore_m541F003F91662EAF4A5C74B16127F0A19D9816D0,
	PlayableStateCharacter_OnExitState_m2C8B41F85579FA8551AA153FDB7CB5D8B31EF611,
	PlayableStateClip__ctor_mC98302A89889583FD869E9B956EDB75EABEB5877,
	NULL,
	PlayableStateClip_CreateAfter_mF118D102D71C55D7FE69D4BA09F8E1CB97C85853,
	PlayableStateClip_CreateBefore_mE3EF4040D5DC5C75F251247016DF41D30822D95D,
	PlayableStateRTC__ctor_m72212DA8439BD43B0DBE45A0CE92C4D632304BE0,
	NULL,
	PlayableStateRTC_CreateAfter_m04896D425083377B366B74F0B217FA12F4EC00CE,
	PlayableStateRTC_CreateBefore_mDB2045A10400A6AF3BFA8F6DC46B31723666487D,
	PlayableStateRTC_Stop_mD2406F0E620E7696D7E8A0A8C7521DF8415A9FDC,
	Character_Awake_mED9A7131CB9A629E5F46DBAD873311E6980741B2,
	Character_CharacterAwake_m54D3EC70E92083086903DA36B04C8486C530A1B8,
	Character_OnDestroy_m70374F80918FB9C8EECED4A860EAD76A48B86FBC,
	Character_Update_mCE933E81829FEA4EE700DC6651DAF1B9A567F286,
	Character_CharacterUpdate_m77B076A3B3355234FB8E6A3DB12125DF29207BA0,
	Character_LateUpdate_mF4E74D67160996B8084010604C3B4D8AA43639CC,
	Character_GetCharacterState_m68567F9897365B45D5964C025D9680906D630EBE,
	Character_SetRagdoll_m75C7E23741ABC5089AB8A99D0E05FCF0D6499E51,
	Character_InitializeRagdoll_m200151443177A1F8AEEC651B7079400C9B518C6E,
	Character_IsControllable_m95A5C59804E3634FA827FF28A6A3DDDD5C1960A8,
	Character_IsRagdoll_mCD8015DF95BEE0E090096A9CBE58E8630D88ABE1,
	Character_GetCharacterMotion_m7DB7D931421ECDAC903DAA11285E1D007465DDC3,
	Character_IsGrounded_m3E5D9076238AFCE0E41C58783024BC44592C7B16,
	Character_GetCharacterAnimator_m4D0A42BE455D4213B9DAE6E1D9F6CD786C038F85,
	Character_Dash_m28458E6255E78E1EDC7733649798FE8FF8D0CBE9,
	Character_RootMovement_m1521C3C93C6F55AB4E475DD2088E29A11360EEBF,
	Character_Jump_mB3EC45C3D5C12468AA8017649F34F457857B3138,
	Character_Jump_mE9FBB53697D3BEE4ADED7760E55A0F8270705999,
	Character_DelayJump_mA4414CFD1241AEB812807E713BC32E7980413D8F,
	Character_GetHeadTracker_m75B5A41C47A852D742BA9379BE2263AAB8317422,
	Character_OnControllerColliderHit_mA49F17FB37338C35270DE89D669684482FB3CD78,
	Character_OnDrawGizmos_mE478E2139FE2EA470C0D68026783EEB823051B58,
	Character_GetUniqueName_m030834FAA246A837464308CFB2BBA931B0DB8D68,
	Character_GetUniqueCharacterID_m25619D4E11FA2460C943450BAD85AC33A445BEFA,
	Character_GetSaveDataType_m342DE4C439F4724437C6CD1AA6C7FE74B0D1F27E,
	Character_GetSaveData_m76CB0996BEFD6CD0443AAA11B20F01F0F21B88D1,
	Character_ResetData_mE84D45B5C160F57F7ADF62699414558611E9E8EF,
	Character_OnLoad_mD307A35F4E81C6F1130675D79D460648C61AA56A,
	Character__ctor_m3AE630DAD4574CF8EE5FFD22569FE1549B257A6D,
	State__ctor_m2F9B40CA1AD0962A7A35D317633C70079D769EE3,
	SaveData__ctor_m2D3D6C748A10DE71997658380BB2C2E10F515615,
	OnLoadSceneData_get_active_m34FF82B6AF6DD2D3E928D916780B138E9719C367,
	OnLoadSceneData_set_active_m0BA91835FFFE3C402173307019ACB40D307A391C,
	OnLoadSceneData_get_position_m5B68052A77DBC44449874754803BAE3644D63890,
	OnLoadSceneData_set_position_m093315344BB72869F08A67A90158EE06ADA7CEC2,
	OnLoadSceneData_get_rotation_mBD41328E6A7445448AE5836E0334AF7F452599CD,
	OnLoadSceneData_set_rotation_m6D4D33972A156DECD91F919E2CE0BE33EC8E560C,
	OnLoadSceneData__ctor_mBDEDC1F95221E0AB08F8239614823D67A0B6610D,
	OnLoadSceneData_Consume_mBBB092EFDB254DF931610771381FF95F6C2C749B,
	LandEvent__ctor_mB51AD3C6A4A30D43D5D67B4F3175AB5D75742D93,
	JumpEvent__ctor_m63709847FA7E6A68E6BBE033083033769ED273D5,
	DashEvent__ctor_mCD418C2506DDF6BA05D3DA88AF79EECA9D183BD4,
	StepEvent__ctor_m6F5BA53C8A4943C241FF6D34052CB45E85D19FD8,
	IsControllableEvent__ctor_m886CE2A1C162B6AD611FAD83E2B072580AE5036F,
	U3CDelayJumpU3Ed__38__ctor_m45DB7082FB55923B86B2ED0DEF78A2C72F7DD8DF,
	U3CDelayJumpU3Ed__38_System_IDisposable_Dispose_mB0D6B4E55D63E183B283D9DDD648BDF1F006DB49,
	U3CDelayJumpU3Ed__38_MoveNext_mE1E0C27879EA6DF628DC4EA51E24C5447F02E089,
	U3CDelayJumpU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDA92A55DECFFF61C95B266DE83FCC68616C1187,
	U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_Reset_m3EAD32CF2878187EF54DD5E5BBBCEDEF3188B42C,
	U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_get_Current_m80E2FDA7B04AEE391BD2B84A8D27DD146AA6FBBB,
	CharacterAnimator_Awake_m7D3A74B396FEF30FE3DCFF06DB35E645622C50CF,
	CharacterAnimator_Start_mAD07E0EE35BC294EB1D388CD3CD870ADE2F723FB,
	CharacterAnimator_OnDestroy_mF980963DC38FBBB033C2E085A5B152B435A18715,
	CharacterAnimator_Update_m69D95BFF307AD4B355E136C2BC0EE890A58C1825,
	CharacterAnimator_Normals_m160F117803B1C7E93E23EB5EBE611EC617DF7492,
	CharacterAnimator_LateUpdate_m781C3FC6008DB69D08A47DAF49F024422987A360,
	CharacterAnimator_GetHeadTracker_m11E3517D300D9B6E54E46B743F9B0A5428B94284,
	CharacterAnimator_Jump_m70EA5614660EB7C0CF6586517A817A88D82E8971,
	CharacterAnimator_Dash_m50E88FFB56490482B3B738A516DD2E480A086F6D,
	CharacterAnimator_GetHeadTransform_m4F4FDA6EB1B0BFE8F517FD5256151AF7EA1974A1,
	CharacterAnimator_PlayGesture_m6A99841BF6485031541946941D06E50AD5BBE174,
	CharacterAnimator_CrossFadeGesture_mB07C15E6C9BCDE96FA3CECAC0949BEF94976BB30,
	CharacterAnimator_CrossFadeGesture_mE39F18534A58C1963F900C2D6FE97E8CADA6AF66,
	CharacterAnimator_StopGesture_m2E55CBC9F54879B0A8F4813E3AF71464BD476817,
	CharacterAnimator_SetState_mB85A38CEDE7CC182D218720577DC5E982BF28ADA,
	CharacterAnimator_SetState_m2A2E1EC36A2F8F29C58943482954A613185F21CC,
	CharacterAnimator_SetState_m98021864BF27BE41A4D2C8174FA463AE0FD09723,
	CharacterAnimator_ResetState_m9D3A5C40E61D6DDA76115EFBB84845D4F222320B,
	CharacterAnimator_ChangeStateWeight_m5CFCB376A823C4386AD42CBDC0B5643931DBC38B,
	CharacterAnimator_ResetControllerTopology_m62DB64A99A9BF35A60B419E9EBA8D14EF39AC8C4,
	CharacterAnimator_GetCharacterAttachments_mEE41166651BF33C8D5724DB56AD5CE5A85E2F77C,
	CharacterAnimator_SetCharacterAttachments_mB01B85DBC98B935032CBECED8255590DB275F17D,
	CharacterAnimator_GetCharacterHandIK_m0474121F388BFB2063ED666F2051EFB1832DBDB5,
	CharacterAnimator_GetState_mFC74DD09DB02891953DB06266DF4B63E1F21F1CF,
	CharacterAnimator_ChangeModel_m608C46BF40C24F434857EDB52B5974C33CD030FC,
	CharacterAnimator_SetRotation_m771A41F6FFC0FFF9782FB917D91474BE321BDFFA,
	CharacterAnimator_SetRotationPitch_m4F97E58DEE05260E474DE1429E3603454FA17C5F,
	CharacterAnimator_SetRotationYaw_mEEFC7EA3ACA97469ED90021AD37B45199A3ED149,
	CharacterAnimator_SetRotationRoll_m64ED09C52C74679A9A76D6C8D10444988926F597,
	CharacterAnimator_GetCurrentRotation_mE4A8FEF1F41A89507279EBF7AA8A912BAFDF22DF,
	CharacterAnimator_GetTargetRotation_mB50FF0A4C8A8C9E547EF7B7D296FDE1ECCF2049D,
	CharacterAnimator_SetVisibility_mE77B48344F2EF82EB35C8D265D4D43CA04124EB0,
	CharacterAnimator_SetStiffBody_m1BC56E6D7D715403A8441CBCCC64CBF80803D79F,
	CharacterAnimator_SetActiveWeightFeetIK_mF7E18201D33488D52DBABC3E0639BC2EE00C2E5A,
	CharacterAnimator_SetActiveWeightHandsIK_m3334D710A16059C03F7DBD24DDA49B472BF5BD9C,
	CharacterAnimator_SetActiveWeightHeadIK_mA415BE63377F2EBD293BED1C7F625D7310B68362,
	CharacterAnimator_OnLand_m31AC65D6D9554F643492077F04EAE75FC0EF3000,
	CharacterAnimator_GenerateAnimatorEvents_m81D08BA8AB16ED24413A83D582DF446DDDBD1BED,
	CharacterAnimator_GenerateCharacterAttachments_mEBA23B103C1A5F7CF28C52665B021BB49BE05B99,
	CharacterAnimator_GenerateFootIK_mDD1766FD31E62AC935F5AFD17843655596362200,
	CharacterAnimator_GenerateHandIK_m64886E345AF929D0A3AAA61BBA12C3B218E0AD3D,
	CharacterAnimator__ctor_m71188D89AF9F503C8D74F30DECB693E0532F3827,
	CharacterAnimator__cctor_m52EDAA71E163254FFA22D4F645C83806C087711C,
	AnimFloat_Get_mE94BCFBFF3B3C281DCD4E1FFE09A1F8F6618988A,
	AnimFloat_Set_mD2A43B03C2E1AF1A00C1979B69E204E6CE611327,
	AnimFloat__ctor_mFB5CD7333836A619A167C717AC19F0EB98BAEEE6,
	EventIK__ctor_mBA724C20B14A42A58B2BE4B018B7B2A8E8769EA7,
	CharacterAnimatorEvents_Setup_m2153714397E54B1BCACBF7186C08A89E0557E05F,
	CharacterAnimatorEvents_Event_KeepPosition_m887AE976BAF1C4C2F165B1CED799C4AB5D744DB8,
	CharacterAnimatorEvents_Event_KeepMovement_mD015EA5A181760D0C67DFE7EDA4B5CAFD4ABA2F6,
	CharacterAnimatorEvents_Event_BothSteps_mF3605B212A829D43323886EC0400E81C7CCDF13C,
	CharacterAnimatorEvents_Event_LeftStep_m79727B9CB7ABC5930C55E8B3C02B43966E91C471,
	CharacterAnimatorEvents_Event_RightStep_mD66E5F4EFBF847959A27CACB751690E255F92F82,
	CharacterAnimatorEvents_CoroutineTrigger_m9088D17F2E9CAA8C76F641F7F41BC210DDD32476,
	CharacterAnimatorEvents__ctor_m17D5916ECE927C47A2EEA8C802B8CC3B7AF9A09C,
	U3CCoroutineTriggerU3Ed__7__ctor_mB1B3F3D66017ACD041A3DF472B357151D137719B,
	U3CCoroutineTriggerU3Ed__7_System_IDisposable_Dispose_mDF30931154439004D31B2A026851F8AA63ACDDBA,
	U3CCoroutineTriggerU3Ed__7_MoveNext_m163C09A7EDE63ECFC916361BDB7C24D480CD6CC4,
	U3CCoroutineTriggerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D7C9AC15EAC985E0945CE394972822428370F2D,
	U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_Reset_m623E4A129BD91E66D1397F77758386EBF64A20F5,
	U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_get_Current_mC44FB9A3B262F7673714DD3EDA5E77E5891DBCB5,
	CharacterAnimatorRotation_Update_mCB7ADFF8421C5F30627142DAFF39E63E29620B76,
	CharacterAnimatorRotation_GetCurrentRotation_mB10F2A567B44E6F8633AC001DF219BFC0CF448C2,
	CharacterAnimatorRotation_GetTargetRotation_mFAD6C43FA587E5B003F36FA3604289F08600C2DB,
	CharacterAnimatorRotation_SetPitch_m7082BCD17CCA5DA9CECED257F88470FEA579A80C,
	CharacterAnimatorRotation_SetYaw_mBAC82FC120FF961C4C506EED5A1667FBEC4E83A0,
	CharacterAnimatorRotation_SetRoll_m0BD0FF62E1D43742B0AC88D6A34166E9F0B82B81,
	CharacterAnimatorRotation_SetQuaternion_mCA5ABEBC4A6A949ADBDE586608AE60F5C0D776EA,
	CharacterAnimatorRotation__ctor_m99F256F0ED9D8D321CBCEB02F0D6ECD4E6BF32FB,
	AnimFloat_get_target_m0B4324C67138CD8F6AAC5BC84888EFF7A978F1DF,
	AnimFloat_set_target_m189D5417D456FCEBB0DD9F0DDBB4322E5AAB4A1D,
	AnimFloat_get_value_m2CDBD910B33AA9C38C3E807BACAC38C1978F9D63,
	AnimFloat_set_value_mC1E3E842276BA71CA344134C5593569748D6584E,
	AnimFloat__ctor_m7B67EA6E0CFF0A719873BC6574A47A2B7A3C1FE0,
	AnimFloat_Update_m28E55E472FDA689A31711FBA94C3D8589E5D1192,
	AnimFloat_SetTarget_mA44BC6F1D0580F1351C563CAEABA602C6D0B8357,
	CharacterAttachments_get_attachments_m0CB2DB2A159F8F311D19A4707283B2F5C9213D50,
	CharacterAttachments_set_attachments_m8FDDE1DF5B13AEA89E97636F72BBF4222ECDCD08,
	CharacterAttachments_Setup_mC77BF9B88E84F10B2E9F2C60FE0E0871F0BE0643,
	CharacterAttachments_Attach_mCF2F16665D894126502C11D9D90EE4BDB328C9AA,
	CharacterAttachments_Detach_mB7A9665966839D74FECC05D1BD738CDEE0AC5A78,
	CharacterAttachments_Detach_m363607A34FE5E8C3FF127F1AA7DD93699693133C,
	CharacterAttachments_Remove_m1D51BAA230285FF1E0CDD78EDBD1E9DD02416DAB,
	CharacterAttachments_Remove_mCF1C1F06E000D3D7270957E61E5E5C108B19A95D,
	CharacterAttachments_DetachOrDestroy_m359CC679167CBACE7DA22901BA0FE4C40A5F6C17,
	CharacterAttachments_DetachOrDestroy_m8FD9ACCA2539BB5ECE922B12E797A9539B167748,
	CharacterAttachments__ctor_m0CCC82BB6515D20D5CEA6487C7938148B8A39967,
	EventData__ctor_m7BE228DE04DD384C57C7762F0D44E4F3771A067A,
	AttachmentEvent__ctor_m54DEA0243711F1110702718B4AA29699BBD50614,
	Attachment__ctor_mBEAFD25BCAAAC5DFE9C7ED51D210C2004231868A,
	CharacterFootIK_get_Active_m026E791D09313D95E6552031863D8A3C177CE13F,
	CharacterFootIK_set_Active_m6ED2E272FEB78F4FD67F9E8D9A6A4E1A7CE6303C,
	CharacterFootIK_Setup_mA82BE7B9C6E52D891BE7DC527723720754ED38B6,
	CharacterFootIK_Update_m4A073DB20AF2E326944F2DFE15203E2DFD250010,
	CharacterFootIK_LateUpdate_m519C67139FD3218D68401A596D49F626CF1F6A5E,
	CharacterFootIK_OnAnimatorIK_m7FD8DE11205F055E0AB3EBB1082C5483985832E7,
	CharacterFootIK_UpdateFoot_m72728B9463E0F94E9699A7F6D9E871223F5D4120,
	CharacterFootIK_SetFoot_m6191440AE707E2F255A0A7B973993865B8CE3176,
	CharacterFootIK_WeightCompensationPosition_m8A98315FA2B8C4F676BECA96CDFB241B72F5DBD9,
	CharacterFootIK_GetControllerBase_mB65AE07F060CCE694C9A9967309EED703DB7A904,
	CharacterFootIK__ctor_m5FF22DC643C27F213DCF483FDFA7AA2CECD8A6A6,
	CharacterFootIK__cctor_mB73863D0FF302CA3AC97F79B647E6A1FE87000A9,
	Foot__ctor_mD66DD1A662395E4FECAA4878FD773ED8E036F26E,
	Foot_GetWeight_m15E837CFA8AC8645CC5677C6D446CC84F40FA9C7,
	CharacterHandIK_get_Active_m9DE731503AA6488A0F459F86B49C35C7DCBB38B9,
	CharacterHandIK_set_Active_mB3995094DD4DA840C8B4791913796CFA57475C64,
	CharacterHandIK_Setup_m44CF45FEF4C3419638A861E36EA9B988686691D3,
	CharacterHandIK_OnAnimatorIK_mEC29938044DF1E9706964B605C580F86C7FDCCE4,
	CharacterHandIK_UpdateHand_m71EBCD17DE47A64CB1224E7CE9835447F357AAF1,
	CharacterHandIK_Update_m01BAD7A6113AD6C70968B487A0A5CB31B7865A7D,
	CharacterHandIK_Reach_mFAD2A3CF29931F216581FDF0501F54045FE41445,
	CharacterHandIK_LetGo_mB270F4770DC12FF34457E9E9261DC03A3085FD64,
	CharacterHandIK_NearestHand_m3CD7FEE1FDB3D5E9C9C79A51D7E52C523404C1FE,
	CharacterHandIK__ctor_mD06DCDA1DA1C59281B9B59971AB2957E4F09FAD1,
	Hand__ctor_m2FE951C0D079B39453BB1B3AF680664E5347542B,
	Hand_Update_mEDCD95DA66ECDAE641576934ECAB6719685F7DA4,
	Hand_Reach_m2610AE9B1CD3C85487204B44998B24D391980296,
	Hand_Unreach_mCAA10EA9B2D3E84F320954B322384CBA76DA61EB,
	CharacterHeadTrack_get_Active_m3816DE3591D91706830CB25BEA2C2FE7DC5BACFD,
	CharacterHeadTrack_set_Active_mFC5D304F334A636912FC185B19AB2D4C7EF4FD95,
	CharacterHeadTrack_Awake_m87787E563BAF0D0BB1089F75DBE25919699E8E8E,
	CharacterHeadTrack_OnAnimatorIK_m87430FCAD998BED198A7C6EE02BF010586423621,
	CharacterHeadTrack_Update_m7BA0BCF6F4C0D01AD1CD3913171A5595A1212780,
	CharacterHeadTrack_Track_m304F417D9D14B150A54859980C1C07413DC37AE2,
	CharacterHeadTrack_Track_mDC79F1C66129B773F27BEE4C741606B71FEDFBC0,
	CharacterHeadTrack_TrackPlayer_m47BBD85231D20102D7065D25307CC46E8BFC494A,
	CharacterHeadTrack_TrackCamera_m40AAC71DC4452EC8B40628025D87CBD871E11AD0,
	CharacterHeadTrack_Untrack_m841550F4B44FF1CCDD0F81809EBA97F97CFE3257,
	CharacterHeadTrack_OnDrawGizmosSelected_mEF9FEB2CC485462BF38F3AC471C41E2D9EA731A3,
	CharacterHeadTrack__ctor_m70B7E76D49C0F84F2891B8B37F02E3160B99CBB3,
	TrackInfo__ctor_m1E708327CBD781D8C236453691E0964F4B87CCCA,
	TrackInfo_UpdateInfo_m13A18370C1D2F2006E064E3B3A9C07C19AA48D4E,
	TrackInfo_ChangeTrackTarget_mB5E7F0CCA1C85FEC0B9344672891F7133E827DC6,
	Target_GetPosition_m69DF73C1A51CB103ABDC9A1DE3CE1B72A67853AD,
	Target_HasTarget_m59EB9B459975DCBA51F4086BF331C036B82EBA6F,
	Target__ctor_m03B55122FF0ACB7EC2683F3090D6555E23280F67,
	TargetPosition__ctor_m5BA7690083B706004DBEC810BC5E91A0FDC37B25,
	TargetPosition_HasTarget_m92B3D66946F65474899F7256C023A055AD040EAF,
	TargetPosition_GetPosition_mD662A6DAAD52607726D2A6129639162A55CA33CD,
	TargetTransform__ctor_m4C1C55A1B9C7BD02C21503883E4D798E70D904D0,
	TargetTransform_HasTarget_mB38A96A39AA886EE609FB825E4D5C4F3000380A6,
	TargetTransform_GetPosition_m19AC976B6A2EDD5C5D1AEC5DAD0744B11F11FA8C,
	TargetPlayer__ctor_mB76A5F3194E1B6C73437C82BD2DB8105417796B4,
	TargetCamera__ctor_mAE89FB908A6D2C01BFE85679F69E3C5A4B7EC112,
	NULL,
	NULL,
	CharacterLocomotion_get_currentLocomotionType_mA4FD56B3638F170115848BC7803482297B3E29D9,
	CharacterLocomotion_set_currentLocomotionType_m56CF03F923B8B8A110975B02D4CB3BF2A402B3FD,
	CharacterLocomotion_get_currentLocomotionSystem_mD7F3DE1436DF555B140C54593D6095D9AB7EAA52,
	CharacterLocomotion_set_currentLocomotionSystem_mED30E6DD7EDAB456C270565D8C8BA330C517ABBE,
	CharacterLocomotion_Setup_m4EA3558ACAFA191FA47D5EF4310B1D304AF959F4,
	CharacterLocomotion_Update_mD15E823D65248B100E8E9C673F39D6399A9DF82E,
	CharacterLocomotion_Dash_m36D15DC423760CDECFE76D9943D8CFEEE241D0B3,
	CharacterLocomotion_RootMovement_mC8937A5D9C71FBDFC34107563E27730AC135117B,
	CharacterLocomotion_Jump_m35848C357D878187E36D971B7700AD2AFB36610F,
	CharacterLocomotion_Jump_m93C37A3BF498C8F1F912C732DD12BC16A8C86012,
	CharacterLocomotion_Teleport_m7E0C0F8FA0E31CA6A9BCB31AF0E88212810F1326,
	CharacterLocomotion_Teleport_mE26957E6893D7BB3B8471AEFDF2A9F2904F7D8D1,
	CharacterLocomotion_SetAnimatorConstraint_mDF62BB2C551735C18B02BDD5599D921BA27AC8F5,
	CharacterLocomotion_ChangeHeight_m647C36A16867B6EE298CF970008F2F31490BFFEB,
	CharacterLocomotion_SetIsControllable_m849C687AF27484FB64E04A2541141EA3C3581AF4,
	CharacterLocomotion_GetAimDirection_m9A443A47670DC8C14F48EA5F1B257C9463091C4D,
	CharacterLocomotion_GetMovementDirection_m92E11D87E44E0C7EF1CCEB798750C44526DAAEB0,
	CharacterLocomotion_SetDirectionalDirection_m5FF9FD079797718B799191A4C5DC144707E37477,
	CharacterLocomotion_SetTankDirection_m9DC0A25913E163BCDF9FDFD2176FCF8DE1E90796,
	CharacterLocomotion_SetTarget_mB368E45B577F5A374CF293A69E68552EFFC9CF5A,
	CharacterLocomotion_SetTarget_m6BCC9EB41ACB501DD1059BCE36B2DAAB56CA2A3B,
	CharacterLocomotion_FollowTarget_mE4E3D80B1A9D530DB672095AA7163F88A39A5FBA,
	CharacterLocomotion_Stop_m742307B2D5B38C856F082005E377A5D2BCBE3940,
	CharacterLocomotion_SetRotation_m8E28A434293EF1F6E409BD22E4ED613C68D59CDF,
	CharacterLocomotion_UseGravity_m165D187B22912DB09C8DF83F94370BD28A2E0371,
	CharacterLocomotion_SetVerticalSpeed_m4C3D6EA0B5483E8D1447268FDF29840725ACC7A2,
	CharacterLocomotion_GenerateNavmeshAgent_mEA79F854DDF609B3A2D80E36E0A5E0E017041DA6,
	NULL,
	CharacterLocomotion_UpdateVerticalSpeed_mAFD960C4294BE61F6BE1697A386D832FFD1E4785,
	CharacterLocomotion_UpdateCharacterState_mFB3387780D4032353BF4B8B43B0451E7D8AF452A,
	CharacterLocomotion__ctor_m392D2187B5E13A888FDB851717BB297C448E313D,
	ILocomotionSystem_set_isDashing_mC079E6C302938750FA3B860353FD0657B09C4A53,
	ILocomotionSystem_get_isDashing_m1C6E364F5B8D885EEB5EF34053F5C9156B12A439,
	ILocomotionSystem_set_isRootMoving_m360142C3A457C12C703CD94B9674C1A64EB3F21C,
	ILocomotionSystem_get_isRootMoving_mD154CF22484E4ADD2130D6A2BC32A3FF9A44AACD,
	ILocomotionSystem_Setup_m85D4ADC29050DA9FDB7C88A6E7D0ACCBA85CBB1F,
	ILocomotionSystem_Dash_mF3FB36C24B2653848EC9E20F89B0FE7C93AC0CA5,
	ILocomotionSystem_RootMovement_m55040C62019E0D98C1FC8E73DEB3D67DE00BE198,
	ILocomotionSystem_StopRootMovement_m9A7A160B43184CD3C5005E7324EDF91703C8AA9E,
	ILocomotionSystem_Update_m14272E6794D9606A43A960639AF184712BBD3197,
	NULL,
	ILocomotionSystem_UpdateRotation_mAE1E9768D5D6BE86BC74FEC662A30387564EBC0C,
	ILocomotionSystem_CalculateSpeed_m4AA97D553556B9D10CAA965EE45957B6A4F26D33,
	ILocomotionSystem_UpdateAnimationConstraints_m18F19A08DEE8231E5B8EBDFB72C047E44B1CDAD8,
	ILocomotionSystem_UpdateSliding_m4A440698BDA8ACC4B21C4A08A08C0B664844FA46,
	ILocomotionSystem_UpdateRootMovement_mA412472A457C3669DA5E23122DC8253A2E3CEE98,
	ILocomotionSystem_GetFaceDirection_mD98283E733C6DD693635942A82C917C1EA2AE9AF,
	ILocomotionSystem__ctor_m718D873429BE1D12E7954D500490DBF173628E27,
	ILocomotionSystem__cctor_m3002E1C212BFC6F8DFD08779EC33441E75E1F309,
	TargetRotation__ctor_m5434766884A6F24044E0CBF81867E803F02A3CAC,
	DirectionData__ctor_m27020C2DD23A950CD9B706D3FB006C2B116F5D3D,
	LocomotionSystemDirectional_Update_mAB157B3C3E9A7DCF6DC8AEDB79CC802A460A9752,
	LocomotionSystemDirectional_OnDestroy_mFD1CF6D94B1D4A06915A2F91E7AE3622FBC69DBC,
	LocomotionSystemDirectional_SetDirection_m56B264E25F7F9B1353378BBEEE5F875D2B8D5A2D,
	LocomotionSystemDirectional__ctor_mD43454B4E471D06B903A870BEC39DFFF43109F13,
	LocomotionSystemFollow_Update_m2FFF48BC4C4A95AF150468F3D5BD30BC566BFF57,
	LocomotionSystemFollow_OnDestroy_m64EF03C39A0FB6B31E5FB4BF59CF2D274EB58575,
	LocomotionSystemFollow_UpdateNavmeshAnimationConstraints_mBC69B3A51F3C36045F72A3B2E043686336CDA17C,
	LocomotionSystemFollow_SetFollow_m6CF8B1B9D5509BAF9A538B53101E938511B8870E,
	LocomotionSystemFollow__ctor_mC7A2BA9AFB8E39141C9796E7AFD78D8C033543A2,
	LocomotionSystemRotation_Update_mFEFC7D539FF76753C0CC9BA5EEB688D977CF24F0,
	LocomotionSystemRotation_OnDestroy_m8C6CEDD2660A6AAB259BF7A3B095E220E920153F,
	LocomotionSystemRotation_SetDirection_m366770BD8A47C75CC7DF07A26A54C651012DFD23,
	LocomotionSystemRotation__ctor_m9723F2F15C8F194A13ECD1301FBDB2F320A64237,
	LocomotionSystemTank_Update_mB20E4FAEC16E21DF49C3FE673539C28AEF1D297D,
	LocomotionSystemTank_OnDestroy_m0C6E7ADD33CBED20F6DFA9092E06C70D734B5C24,
	LocomotionSystemTank_SetDirection_m1D97B11ECC841B23E1668FC2B5DC4706CDCDC6C0,
	LocomotionSystemTank__ctor_mDC544EE788E973749349AADAE979D380A2B4A66C,
	LocomotionSystemTarget_Update_m9C8A7BD119F6250FD0F762ACA17096D026E2A7FE,
	LocomotionSystemTarget_OnDestroy_mBD9A01DBFD00BEB57900148BB247AA9C6381CC41,
	LocomotionSystemTarget_Stopping_mDDBFDF8BD138D3858BFE108B6552BBA6D4ABE3D1,
	LocomotionSystemTarget_Slowing_m5812DAF1040CFA26DE93AA2DF1FE6D67905931A8,
	LocomotionSystemTarget_Moving_m9D1A4B6687B8CC4E464218152B33F227C9B1D7C1,
	LocomotionSystemTarget_UpdateNavmeshAnimationConstraints_m7A0070BBC3DB9F3C6A3E444AF5DC5E5C76F70D3C,
	LocomotionSystemTarget_FinishMovement_m372B8D4830B34017E520B97187E20BA5232710B0,
	LocomotionSystemTarget_SetTarget_mF8B47367FE45FAAD0648BC4A81D958F39D6ABC6D,
	LocomotionSystemTarget_SetTarget_m219C22C501ED91BFF61595732B18FABA0F89FC5B,
	LocomotionSystemTarget_Stop_m3E1E47BC681B0ACE3DA94479B57A0ADB6CEC4C7D,
	LocomotionSystemTarget__ctor_m9C46630538B0934131B8F5251F873ED98F60F0C4,
	PlayerCharacter_Awake_m53D5D700F69BDEE5AB4F714ACF182FE598F0EEF3,
	PlayerCharacter_Update_m13257097B99AC70AB01F659E9DF726A43D359734,
	PlayerCharacter_UpdateInputDirectional_m0E619C9BC6DFDBB3CE4300116BDA49642D0A6D49,
	PlayerCharacter_UpdateInputTank_m70200A320FD4FAC887220AD6A7C479D0558A4435,
	PlayerCharacter_UpdateInputPointClick_mEEE73C6D74082F51F1C1FF2DB8185F6757B4F3B5,
	PlayerCharacter_UpdateInputFollowPointer_m85DE2E3070FACC19570B7DF517E22F8C2A0303B2,
	PlayerCharacter_UpdateInputSideScroll_m7E84F2D7105836D9C30D0ADC74C2DB055FACC2AD,
	PlayerCharacter_GetMainCamera_m8B6979A44B32645C383564FB543F1A9CE607530C,
	PlayerCharacter_UpdateUIConstraints_m45F683E3C1EF6E086EAD7A9DE49C560FAA595A44,
	PlayerCharacter_ComputeMovement_m6D0A0881459C2ED63A577F9F457D6473F48E1932,
	PlayerCharacter_ForceDirection_m388CC2A70CAA8B679E745AE632F2B9E77F872685,
	PlayerCharacter_GetUniqueCharacterID_m47AA4385D9751CE3EFA970D6DEEFD43D43A1DD4A,
	PlayerCharacter__ctor_m6022B24D034F3904510B6E0972F97869EC0D6873,
	PlayerCharacter__cctor_m91B76767E7D22A0319ABDA37FFBE5171681C4EFC,
	NavigationMarker_OnDrawGizmos_mE7242C301340CE85AA544B9D9AEB070FD5700E14,
	NavigationMarker_GetMarkerMesh_m650D052A4C20CA0BB176D18E613B3BB69751242D,
	NavigationMarker_GenerateMarkerMesh_m72D84D44144F22F1FDE5307DEC953E8B1AA40546,
	NavigationMarker__ctor_mD2BA18BBA7C0E193D10551C8948B547EDAA78CCF,
	NavigationMarker__cctor_mC56E52F03C86EB5FFFE1C7997408DD0AF5680A31,
	CharacterRagdoll__ctor_m0768B7775B2D0DA1133F76F91247B75C7FF5D0E7,
	CharacterRagdoll_Initialize_m70F15030A8069EF45D4545D35B852B0A01067CBF,
	CharacterRagdoll_Ragdoll_m836CCB504A1AE79678CA0F2AAB16EFE4F2097CB1,
	CharacterRagdoll_GetState_m3080A4F2C5D055DF92BF4A59D8555AB8B3364D97,
	CharacterRagdoll_Update_m096EECD2D70CDE09AF55B929A82F980BEA86F70C,
	CharacterRagdoll_UpdateRagdoll_mB916D1AD0B8923B921AC3C01161019694BBBBC5B,
	CharacterRagdoll_UpdateRecover_m35A386589CE689F73AC190DF2E968696FDBF4FD1,
	CharacterRagdoll_ToRagdoll_m37B7822DBF68F1E977E4439947FBE832C0CFA8F9,
	CharacterRagdoll_ToRecover_m680C4016FA3588C4FA27462CC2C0CF6CBF1A9FC6,
	CharacterRagdoll_GetRagdollDirection_mD9DDD7C8CA3D101B2BA4FF4502CB55019A5527D0,
	CharacterRagdoll_BuildBonesData_mA123BECA25CB529A7B6C96F2A8CFF264D18F6784,
	CharacterRagdoll_SetupJoint_m950EB8BEE7023EF957CC55354C3F645AD152D3F5,
	CharacterRagdoll_BuildColliders_mCA00F6D60ABD45D13083B131E6B53452E00B28B5,
	CharacterRagdoll_BuildColliderCapsule_mA52586CB1F0F475040CF162722116AB97E3504FD,
	CharacterRagdoll_BuildColliderSphere_m90056C02CD66AC6345BDC9A9A16EC18379B48A89,
	CharacterRagdoll_BuildColliderBox_m2C0E782A2085F6B15ABED0EDCCAD98C1A5E282B9,
	CharacterRagdoll_BuildBodies_m99C8266BB2140EB8B9262CE5B8251102A3FAD436,
	CharacterRagdoll_BuildJoints_mF46C5B2451861F579E3DAEADA64660284335AC0A,
	CharacterRagdoll_BuildMasses_m891438176EEB3BDA27F04CE52548270C0F17F87F,
	CharacterRagdoll_BuildLayers_m05D844300ED0950555F93B8310F44BFA42C7E17A,
	CharacterRagdoll_BuildChunks_m1DCFE1E3DE881B7102EAAC0EF290A2104A609296,
	CharacterRagdoll_OnDrawGizmos_mFB373C4F7E04EFAD219BE0CF507FA1042E8A67E6,
	CharacterRagdoll_GizmoBone_m895D5D237DF0D3CC8DB0700853FB283C5B8FBF3D,
	CharacterRagdoll__cctor_m827FF4F040D7FF9E3AEAF94E6AB911EFD515CB8E,
	BoneData__ctor_mCF41F48FCE2C403A76C860E1963F21BE6BC46674,
	BoneData__ctor_m9F43DA88153F565AECD7F3964A391B21123AC23E,
	HumanChunk__ctor_mF152FD85E8AD808AC27AB1466AD5D52EDAC16381,
	HumanChunk_Snapshot_mCA65B9999E27FAEE3E994B3660C7A9E83612A3DF,
	RagdollUtilities_GetBounds_m663E840916CD6689EE671CBC57ECA6729C7757CD,
	RagdollUtilities_GetDirection_mE621518E33E9D980C3BB6B69D87A472C45973007,
	RagdollUtilities_GetDirectionAxis_mE96946788564D3A8AFAB8F6D2147196158D4B79F,
	RagdollUtilities_SmallestComponent_mD6CC9635D2A23998DD65F638198CEA19AEA2ED51,
	RagdollUtilities_LargestComponent_m90AFEDCD066541F2EA5BE171A4CDE0D8A490840A,
	RagdollUtilities_ProportionalBounds_m9AA484162ED2A0419E833AC2A1A0FD91D1A0651B,
	RagdollUtilities_Clip_m000709885E660539799085DDFADFCA54EF3FDD60,
	AnimationClipGroup__ctor_m39D2E32859B4F56F09A99B52C45613AEA17D64B3,
	CharacterState_GetRuntimeAnimatorController_m8F15A83E82712FA39085866CA47DA7E5A44E320C,
	CharacterState_StartState_m78F0DDDEF2FBD5B411D612A66B40527F5FB325AD,
	CharacterState_ExitState_m1FAC3B00029E6429B9B579A20E49148C68655C68,
	CharacterState__ctor_m8166B6AE73B675CA2030F9D84E674C94B738531A,
	ActionCameraFOV_InstantExecute_mE7644EBD6F76F422572D28DEDAE01A8DD2F0DD48,
	ActionCameraFOV_Execute_m2961F82EDD4ED139F459031B2114495B90DDBEB1,
	ActionCameraFOV__ctor_m46A7CDEFC90178BFB8451AE909B2C62159E2A7DA,
	U3CExecuteU3Ed__3__ctor_m568C46FD84354F03E321B4A7D9B6A406155A6442,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_m119B7D8FFAD38119F367543F53D104EB2CB9AC46,
	U3CExecuteU3Ed__3_MoveNext_m4995AEA698858151266D9E05A91465C9BF335EDE,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D53463E3447749CF12F7FEA5553D2AD9FE91169,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mA5FDD65159048AF6BDEE02D3389DF5B1870EEAFB,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m1AF795A4F8BA74E09BB67237231F64629E174B23,
	ActionCharacterJump_Execute_m08FA51AE3EF80F74E8C7437DF69FE88F2B3074B5,
	ActionCharacterJump__ctor_mF8308DF8F033033C842C870023831128048DE6A3,
	U3CExecuteU3Ed__3__ctor_mD014279CDD1DA2CB66827900A7FEF6B36E4034A6,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_mC869E87A2210092C97E4DBD7E06FCBE358652E49,
	U3CExecuteU3Ed__3_MoveNext_m52C7500D1240EF3E83BE33E619EBF688D4DAE5B4,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA829096573BA1FF01D24904DA418638E07E295D1,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m251ED7B0A59C1F6511E8A0752E09990150A66F22,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mEA566C81C92D1772C5B0DA08E2015A4B14C12E25,
	ActionCursor_InstantExecute_mC4990B3E8BAC57EAF15EF7A5758B363577C502B9,
	ActionCursor__ctor_mD6E25CA36E1F1F798AB886C31AB524BCEE3EE65B,
	ActionOpenURL_InstantExecute_mE158A9BE18BFF2DAF25C20D7A1A7E56FBD6C7CBD,
	ActionOpenURL__ctor_m37469D89871D5088B0F28BAE5F8373E00F77DD65,
	ActionQualitySettings_InstantExecute_m688B78057BCE5A173A4EB85C09E29AE461F71D73,
	ActionQualitySettings__ctor_mD7828EE728F60C0E8CE940C6DD43A626937F9FA0,
	ActionQuit_InstantExecute_mD7156E86BF560FD18388771A6AF951BE2A18FB94,
	ActionQuit__ctor_m284C476899B42797B01FC4C796E40BE95AB71829,
	ActionVideoPlay_InstantExecute_m4C1836CC342D621818883B41498C7038A71BF819,
	ActionVideoPlay__ctor_m41DBF5979E5EFCCC9D2D5628E76019A3BAA38CE0,
	ActionAudioMixerParameter_InstantExecute_mB7B7A9B3EB9315B9A3CB9FE80EAFA22D33431A0F,
	ActionAudioMixerParameter__ctor_m7513A634CC954E5786BB067C7A5D5EE1395FD5B8,
	ActionAudioPause_InstantExecute_m5A1F5BCE06180CD8514488B1A866B660346ABDBD,
	ActionAudioPause__ctor_m9BE09F83B40F024CCDE701FA2768C99EF52CB13A,
	ActionAudioSnapshot_InstantExecute_m87C15ECAD2E26934875998C56E8EF4A5D794A9AD,
	ActionAudioSnapshot__ctor_m890806D219A9073C45A1D8A7022D6399083C06C1,
	ActionPlayMusic_InstantExecute_mECDACC252E10B3D53EFE73C6D34C2D2AB4959611,
	ActionPlayMusic__ctor_m8476718D283EEFC6015B7F082D9C982C03F54089,
	ActionPlaySound_InstantExecute_m9AB8F54F7991F4E3A371421B1BFF9E69F60973B8,
	ActionPlaySound__ctor_m741C8033EA48ADEAEA0BE0566872ECC975E862A7,
	ActionPlaySound3D_InstantExecute_m6DF8CD49FA96692D96D174A138D8B6B568277610,
	ActionPlaySound3D__ctor_mB08E8C7535665060322F400B6BE5E60B958D85C8,
	ActionStopAllSound_InstantExecute_m1C9AFAF392E0ED68217922C36B9F690E0E214FDB,
	ActionStopAllSound__ctor_m3E725077B7E61D90DAE707E083AF2636991E9B75,
	ActionStopMusic_InstantExecute_m6FB6F069204E6D5B222FF9F0992062AA035DE12A,
	ActionStopMusic__ctor_m94A68C8A540A6576CDAD250BD0BAEAC71B065141,
	ActionStopSound_InstantExecute_m92C5FFD01FABC278B4214A34BCD4B66F11DC5112,
	ActionStopSound__ctor_m49B5D139B4BF0FD5A01D03C7B0F8602BE80E458B,
	ActionVolume_InstantExecute_m8B433252A8F0275F168772358693B4016A49A4E9,
	ActionVolume__ctor_m9D2126EA2C65C67A4359AB36C5DC26FDA22EA3E1,
	ActionDebugBeep_InstantExecute_m0959A9B35FA7B16B7526630456511E5094E1A165,
	ActionDebugBeep__ctor_mE2D494C491E92235BFD3A68BFD09D7D5A6208E4F,
	ActionDebugBreak_InstantExecute_m873A6129A511773BD20EA426CCDDDE0F0BAEEC43,
	ActionDebugBreak__ctor_m03E7FBD708C85F62A48D8DC2BDFCE68C5E1FBBB1,
	ActionDebugMessage_InstantExecute_mB1C886F34153FAC958F1594C804C11463587291C,
	ActionDebugMessage__ctor_mCA86AF2033FE92E2DA93931C7400C0D143C0C443,
	ActionDebugName_InstantExecute_m1CE00EE329405A09530029BBC6EB5488AA8E1E5B,
	ActionDebugName__ctor_mA9D5C2E1CB17A85B000FC7251942393395CD3B8A,
	ActionDebugPause_InstantExecute_m494ABAC1945AD21E15F289616EBECB35D664E135,
	ActionDebugPause__ctor_m4DEB65DA50197DCDBDF6594B8B3E16C14BA88EF5,
	ActionActions_Execute_mF60338A03B212040424031E749D555D34F89EC45,
	ActionActions_OnCompleteActions_m545AA77F055E7ABBE0BB78384637204A429D9B65,
	ActionActions_Stop_mC5B1E26F6A4CE71F9191F3C13600657C6F9F75B0,
	ActionActions__ctor_mD8BA0CCD7C145588FA69585E6F59C5870EE2CE3D,
	U3CU3Ec__DisplayClass7_0__ctor_m762552681A1828B8FA649A3F54F341D736F501B3,
	U3CU3Ec__DisplayClass7_0_U3CExecuteU3Eb__0_m484878E698FEB2254A02BC992CC4E1A78E190901,
	U3CExecuteU3Ed__7__ctor_m8F225E767DD6DB758FEA0B87C3078AA77FBFA5FA,
	U3CExecuteU3Ed__7_System_IDisposable_Dispose_m4376D2481CBD9E7ADDC3DAF2152CFC9092F5F7E5,
	U3CExecuteU3Ed__7_MoveNext_m6312B7FB682A4C9645217291396A35A5DD41FD90,
	U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1684DA24B04EC7538FBC1999207915EF16FD474D,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m7251172E50F11399CF598F7C05629B9825C0F233,
	U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m0DA803CF52C2AF51D6ECF171BF22580ED5539F5B,
	ActionCancelActions_InstantExecute_m3A20B7634B52F157BFDA50208E87744986BC79B1,
	ActionCancelActions__ctor_mBACDD73E2355093DF3C4673300A71C46CBCBBD71,
	ActionChangeLanguage_InstantExecute_mB29B2C922C3818C897C89052ACCD6F95F0F4BB39,
	ActionChangeLanguage__ctor_mEFF15550C942621BB8FE92E96AC559089AAC88F9,
	ActionComment_InstantExecute_mFB667DA16F8FE67ABC8022F8A795957E66E4FDE1,
	ActionComment__ctor_m52FF57E2F29C02E6B6D93F7C3278E55DE8168EFE,
	ActionConditions_InstantExecute_mC97A34C22BE6BB1F55BD1C50E854AC20A86C27F3,
	ActionConditions_Execute_mE0522B42F62D3F243F1AFAE772B9CD175E83C357,
	ActionConditions__ctor_m8F43A65CFD4882EB2EBC7F668C6989338A2E4C03,
	U3CExecuteU3Ed__6__ctor_m9EAFACDF88412BADB6CC5CAEEA99FE03D10FDAE1,
	U3CExecuteU3Ed__6_System_IDisposable_Dispose_m659F01A84B88179C9CAECF6702B4C86FF9A43B89,
	U3CExecuteU3Ed__6_MoveNext_m04124FAD32B9595D466981B048248ACBE08A626B,
	U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC017996B99A9A285EE0A0982EF05AD814B15FC3E,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_mF76B5998B28F241A1530AF47AF804C28A03FEFC2,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m555CFC88911972DC5B58D4D04A9F0C3E3821B1ED,
	ActionDispatchEvent_InstantExecute_mC1B8C8CAEA5FC37CD909D91F9EEAFF030FE09D71,
	ActionDispatchEvent__ctor_m63166C105F4082F5124D5D23846A2AE0EBB13A08,
	ActionGravity_InstantExecute_m206E7E15FF8A302660597BFD95DA7FAD2B6CEE89,
	ActionGravity__ctor_mCA4824CD9C8CA1BD4B5519867373ACF7F2394489,
	ActionMethods_InstantExecute_m3CA9958DDB9FEE56A90011ADBA60056E4B232C6E,
	ActionMethods__ctor_m156EC7B148FC9C7DE08EDCD5316021AEBD4258BA,
	ActionReflectionProbes_InstantExecute_m9E5F655977D6FD5F09EF8CC2D5D03A28F660551A,
	ActionReflectionProbes_Execute_m5A918D81E680B42CF41A202C722F9B6CA4DA1475,
	ActionReflectionProbes__ctor_m09D7F2D65DBDBAE81CF8F772BD0BAE3D112AE5A0,
	U3CExecuteU3Ed__4__ctor_mE3705328BA02A5C6804D656A84D2F01521659B17,
	U3CExecuteU3Ed__4_System_IDisposable_Dispose_mBF5E03402ECCA456AD7654A8C19ECD5785307A89,
	U3CExecuteU3Ed__4_MoveNext_m843C3CC7620B1BCE167E2B3D455DEAFD1C1CE55F,
	U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD002A5B232EDAB262AD2577DA21E86AE289242CA,
	U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mAF5226AA4EF8C651DDF193AA52AF3F58E3720CE6,
	U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mE081B69A7BC0703684ACD7D36C0899EBDD746C3B,
	ActionRestartActions_Execute_m141874EBFFD0D71B3EEBC622D98CB24E7FB22955,
	ActionRestartActions__ctor_mDCB68BABA7D7851A666AFC269B0DB22B07774A2C,
	U3CExecuteU3Ed__0__ctor_m52231F20C05F8C80A3ED4721BF367C1DDFB06CE2,
	U3CExecuteU3Ed__0_System_IDisposable_Dispose_mF52E298FC3DC9C9456316D73F74C0005B769B4E0,
	U3CExecuteU3Ed__0_MoveNext_mDD9693A504CAF31C0405F9E72BC6DA6F4838A845,
	U3CExecuteU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37052B91459D04361E63F25F8DCA3E4C52A95CEA,
	U3CExecuteU3Ed__0_System_Collections_IEnumerator_Reset_mB1712CAF9C824B2468F658B4D5A50B88CE06D799,
	U3CExecuteU3Ed__0_System_Collections_IEnumerator_get_Current_m3C22C51AFB94DA83D80DB69FBF40AAAAD12D9356,
	ActionStoreRaycastMouse_InstantExecute_m4E307965AD8CF419D53CAD86C403EAAE89B6B6AA,
	ActionStoreRaycastMouse__ctor_m72C7F9C84F1209C415794D6A31E1F149D32EBFDB,
	ActionTimescale_InstantExecute_m3A69C005C907A4A54BB9A3068C7384372EBD9D0E,
	ActionTimescale__ctor_mFA188EF2F76835C714F251FC1B02E4EE89447E87,
	ActionWait_Execute_m04586A3FDF625719D21C4722DF245176A7190D66,
	ActionWait_Stop_m0454C181F028BB73E8E7422F5049A07D1D2458C6,
	ActionWait__ctor_m95D6E42172C9B182EAC6BFC4F8B54AE5B70CF7AA,
	U3CU3Ec__DisplayClass2_0__ctor_mFCA1EDB4D982DD2330E98FED3942A2AFAF2F4CB5,
	U3CU3Ec__DisplayClass2_0_U3CExecuteU3Eb__0_m3AC3D9D6CFF98C3F1D9E5B52F7A41DC0F7EFEF58,
	U3CExecuteU3Ed__2__ctor_m1C9147D9500D8992417D62EDB2969850D0BCA95F,
	U3CExecuteU3Ed__2_System_IDisposable_Dispose_m46DB6B72EF206B2AA46FB515BB10A3E6CE5B8FB8,
	U3CExecuteU3Ed__2_MoveNext_mAD649B1285179EBEF4960C810D3EBC8EBF3C0818,
	U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E1BA8ED4D7167A7EC8B04C9E249F6C95F7C911,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mEDA5AA1846C72B143E01D49133FD304FA342C9E4,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_mD1D01BD5AE2C54941C4051CFEF0DE1FE6E368F8D,
	ActionAddComponent_InstantExecute_m9D1B18FBA54D22E221F1857DFECCAA8FC86CF0C3,
	ActionAddComponent__ctor_mFB16A30B5F80CCF6E292B30E5121ECA13C2309F1,
	ActionAnimate_InstantExecute_mE96B8B0287291CD5F2529121E40DDD5FF3F0506E,
	ActionAnimate__ctor_m123DCDE1CAE994A7B1B66995AE5009FF4E518DD6,
	ActionAnimatorLayer_InstantExecute_m7384E1C6524275E4663D34F52A1610EA68547109,
	ActionAnimatorLayer__ctor_mEF8B588CFABB3AC894CB5168430AAD54EE8C030A,
	ActionBlendShape_Execute_mA4756B727376FBA06CDDC8C25893BB0FE8EEE787,
	ActionBlendShape_SetWeights_m59BD8EAE316DB6032F28C5F6F349681FA85BF058,
	ActionBlendShape__ctor_mA2A6DE6F31397E3765DBDC0B97F11B598A3E6F13,
	SMRData__ctor_m9C24890B024A2CB176EB4DC143E445339BCEEEF7,
	U3CExecuteU3Ed__5__ctor_mF19135E1262F01401F62B65C6AAE21E195800841,
	U3CExecuteU3Ed__5_System_IDisposable_Dispose_mB01B52B59D5766C1B8DCB01058767AAE92EE08A3,
	U3CExecuteU3Ed__5_MoveNext_mE0E30A5D3B6AA02B1071BA303F62D231321C4485,
	U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB6BDC403B3CC4A1A6CC2BAC53F492A49EC68B33,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m7E4256ABD8A2DB3A8A5DD1F625898904BD4749E5,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mD6A3F2C62C6A20DBC26A9983D538106441A5CE1D,
	ActionChangeColor_InstantExecute_mA50C0E69898880B5AACBEE76DBC6579D2634D11F,
	ActionChangeColor__ctor_m83ACC636AA8E376D7B770871D0359347A2A7E133,
	ActionChangeMaterial_InstantExecute_m5CDC7FDBBDF93E6BE031F56E765ECD02A27C5957,
	ActionChangeMaterial__ctor_mB1FFD9ACD3AEAF80F38B379414716E9A071EA26C,
	ActionChangeTag_InstantExecute_m699FF0439862332679B22012A7028AE0C06842A1,
	ActionChangeTag__ctor_m802B4FF8CEC2BFFA944DA736DEC56835B496C5E4,
	ActionChangeTexture_InstantExecute_mC5BE545615EF574B8515D829D077AAD7554A35E0,
	ActionChangeTexture__ctor_m9311B2AC5B500BA6C005C511D227670A602B7356,
	ActionDestroy_InstantExecute_mD17B89FD1CB4939E25AFD5DD0B03E09311603DB7,
	ActionDestroy__ctor_m4B75F43F01E25677B5795F27D63502F7864291F7,
	ActionEnableComponent_InstantExecute_m51547AAF5ACC530DBB11288F985566700B03ABEE,
	ActionEnableComponent__ctor_m694AE3192C53034852D45D0696295BF3962FAA59,
	ActionExplosion_InstantExecute_mA900922CC39142D1D944096B03CBD0362321B336,
	ActionExplosion__ctor_m98F32A978F5535009B7CF233AB75DAF4666053BD,
	ActionInstantiate_InstantExecute_mF1C6C108E2F9C86AA2BA1DAB278FB7E9FFEE2CCD,
	ActionInstantiate__ctor_m29CE0FEA8508669E97DC1D211C5EE342F7F0CCAC,
	ActionInstantiatePool_InstantExecute_m3521ED1EEED965B9D53A0419BFC331A801144DA2,
	ActionInstantiatePool__ctor_m31C52412692869459F210405B0593FD9E8921832,
	ActionLight_InstantExecute_m250124C567D86437B39DC4F49287C870A94F42BE,
	ActionLight__ctor_m56896D4416C85A77962E1F2DBE8BF917F6456838,
	ActionLookAt_InstantExecute_m5D8135C2E073D36EB5DC73EF2232EFD585A37E16,
	ActionLookAt__ctor_m31872EDB0F1C8C10EB460CD121EE2C32F44EA9C6,
	ActionMaterialTransitionValue_Execute_m5532403A5A5539119EFFC92D16650C435CE447DF,
	ActionMaterialTransitionValue__ctor_m5BFBB0C1353452348FDA15D828F3283B1D263E84,
	U3CExecuteU3Ed__8__ctor_m63455629CDFC718B52F66A855AA973034CA1EC15,
	U3CExecuteU3Ed__8_System_IDisposable_Dispose_m33F4E3AABA145A6F5929A9646A66A74F9B8EB3F6,
	U3CExecuteU3Ed__8_MoveNext_mB2C54BADEEF83C1C1E2657A39D187D5BF2094BD5,
	U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m921C4AE7D1431654297FBFF722D990452EAB020D,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEBCDDDC6151242BB567F141352561DD344537C13,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m4D68358EA683E8DFAA8FB743BAD43BCC240A86D1,
	ActionNearestComponent_FilterCondition_m1E6F4586541015561204AE2EAF7CA502BAAF0090,
	ActionNearestComponent__ctor_m15250BAF56E780913829C7C0DA5697B8ACAC5052,
	ActionNearestInLayer_FilterLayerMask_m0C97B76234D25844476013A88D9E0F92A7D5C6B1,
	ActionNearestInLayer__ctor_m28E6C7D94ECDD7D98FDBF48E0709184134626E33,
	ActionNearestTag_FilterCondition_m749FD438C1C88D027826B8FAAC4A23841ED3CF76,
	ActionNearestTag__ctor_m4D9CB6D60FE40718DF59289E44AF254AD17EBD26,
	ActionNearestVariable_FilterCondition_mC680A0696E4D7C59BF72F5E37776ADA7E21ACC54,
	ActionNearestVariable__ctor_m746DFBD74A6E2227DED3CC9CD6D06D31ED05A52D,
	IActionNearest_InstantExecute_m1CBC737222CE8B396C9A57D64466B625E4C09788,
	IActionNearest_FilterCondition_m4DF489DB4954ED56B8D20F86F2F54C4F06A4E608,
	IActionNearest_FilterLayerMask_m2E2708DB03C8C5D170527BCB71B191AD0F7A476E,
	IActionNearest_GatherColliders_m4D9D2AC8FEB557650035F440E66FD8D0751DBE01,
	IActionNearest__ctor_mBA4C046CDC51E7642A296F47BE99FEB98E9D6266,
	ActionPhysics_InstantExecute_m75EB3FC9E8756A1E4A55CE658721E7EC5F708470,
	ActionPhysics__ctor_m4BC89263118AA154361D85F225B223A428D864BD,
	ActionRigidbody_InstantExecute_mBF6D50FC2886324F9CDBC9267F949446F653694C,
	ActionRigidbody__ctor_m126570E9009E175916DFD2E2E224ABA78C25274F,
	ActionSendMessage_InstantExecute_m4C84B6C0E7AA563748ED176D8243AB6B8278E04D,
	ActionSendMessage__ctor_m34C95C4AB959567A8437BB4D35C1844F5D896831,
	ActionSetActive_InstantExecute_m739FDC4A4158715FD2D39734239AEF98F070A78B,
	ActionSetActive__ctor_m349AC9AA6161521239D1335449B01334DCCC4CA3,
	ActionTimeline_InstantExecute_m2ABA190F897CF55D01108D301F02E6725236CDF7,
	ActionTimeline__ctor_mCD43DA95E2AC2B102667EA78CB527D2ABE548F36,
	ActionToggleActive_InstantExecute_m59E4EB2403262DEB9CA4791FDC824C11E21C683D,
	ActionToggleActive__ctor_m3627497EECD7251BFF209445E9355D70D017E208,
	ActionTransform_InstantExecute_m10203CAD81F56B78D7367E1D728CBD3D77273A06,
	ActionTransform__ctor_m89BBDC5F692A75C4B8EEE4A4F75667AEEAF014DB,
	ActionTransformMove_Execute_m83A314D67419B65DB5ADD6C174738DFE15984D53,
	ActionTransformMove_Stop_m59B133326F77C1AFDC7BF17CE76855BB87D550BB,
	ActionTransformMove__ctor_m8B04E1BD88E0D726447024B6D36183128FFA1866,
	U3CExecuteU3Ed__8__ctor_mBBC55362DD6F73D1A9D3FD8C3F815D57E473521A,
	U3CExecuteU3Ed__8_System_IDisposable_Dispose_m3BE76413C35E97EE41BF887F5C94338A08F59274,
	U3CExecuteU3Ed__8_MoveNext_mB0D3F93FADD140BDA6520C5E2CB363D4FB310DC3,
	U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B2FDCD99FC7D00F3F2E45033E4D4E43C434EF29,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_m9BFD11409852AC030EAD6175AF52C6C04FF2797B,
	U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m826A920F08D5B9712D8CAD38A583A9014C784C76,
	ActionTransformRotate_Execute_m52E7014DC3C1A4BC3CDDBA1EF21E37AF4EE154C0,
	ActionTransformRotate_Stop_m531AE2935EC17C19D1A215EAEE3A432732FAC674,
	ActionTransformRotate__ctor_m897781FCCF31C107FA4188757DB61C26611D1CE7,
	U3CExecuteU3Ed__6__ctor_m3EA267B4AAEB1FB64D9098EE4ACCA34AEA590FF8,
	U3CExecuteU3Ed__6_System_IDisposable_Dispose_m213250BD24E8EE8853B913051A787012D435EB5F,
	U3CExecuteU3Ed__6_MoveNext_mF24A97E2BD236D37AFFF5C83AF02AF6A519425EC,
	U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0305DABCAE84612AD3F76206412BD342D52129E,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m204D54BF27D7CFB6F425C2D20648D125078A38A6,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_mE4DFC63A85A78AD6793EA847AC75148919B971AA,
	ActionTransformRotateTowards_Execute_m8C328A448FA212122A818FAEC0C3D3AC2404A146,
	ActionTransformRotateTowards_Stop_m060D5EB12C9C4C4137DED1F0C904446E58EA179E,
	ActionTransformRotateTowards__ctor_mF3D34881EA18D59FD7E395DB215328EBBE1D7802,
	U3CExecuteU3Ed__5__ctor_m0706F423CF92DB9367533BF6A9123319F8CA8278,
	U3CExecuteU3Ed__5_System_IDisposable_Dispose_mCD2F8FA04E201EF31D00ACA8DD462E0A1E22BB9D,
	U3CExecuteU3Ed__5_MoveNext_m9A64B969940A044F56EDD54925F61AF5436F7766,
	U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF72942B4B178CA78A6749390E9BC859525A6EA48,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m398E925CAA656EED3C0632C1A3ACBD8F10B14364,
	U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mBA9BBCAB06CC9DF538CC5EC1E17E21165FD96FA9,
	ActionTrigger_InstantExecute_m1AD43F047FF26D259044A8D34A2AF4F7B34AD5BD,
	ActionTrigger__ctor_mE2FD2B01CCE53A61B2756DF7AFEC3309691538FA,
	ActionCurrentProfile_InstantExecute_mF8D88BF26892A966CAA0AA907CDF10B874892638,
	ActionCurrentProfile__ctor_m1FA23827EF7CAFF7ED178EA33D9224BDA5DCC589,
	ActionDeleteProfile_InstantExecute_m015A41742C8B50D2A42ECAB5C002D590E193F15B,
	ActionDeleteProfile__ctor_mE1D62B073244A7A514585647B3F6A3DDDE3B7339,
	ActionLoadGame_Execute_mCA4BCD88110DF8FFB57B75DA02CA1D13A1B9E82A,
	ActionLoadGame_OnLoad_m1EDD3548779082FE47F15961E646EA5F74FE72AD,
	ActionLoadGame__ctor_mE583DCDC0289E2D0BCE694BA557542269D4FF2CE,
	ActionLoadGame_U3CExecuteU3Eb__3_0_m5D65A8280B7151F6FBAD8401F772A0B860F290E0,
	U3CExecuteU3Ed__3__ctor_mEF7EE534F5AB0E4EC07E539E821DA3AC95D970D1,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_m2DDDC6B615D89AC51CF63297B2EB8A205C1AAE4B,
	U3CExecuteU3Ed__3_MoveNext_mD975623873DCC084758FD155D4FAD34E98981763,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C3C7C7718E40C63F6851C0502FBCBAE5D51D10,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m6110CA08FADF00FD94D352C8091DE1191313BC5C,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mC29C82265D7A10E38E4D4595D194B45C4A17ABE8,
	ActionLoadLastGame_Execute_m0661A69EA5997349140E8D5EB8F76E601B056394,
	ActionLoadLastGame_OnLoad_mD80F24B17F011BAE5B0DC01708BE8B4F0429C627,
	ActionLoadLastGame__ctor_m9850637435D51E049F808E6CB6DFE6B18FBB988F,
	ActionLoadLastGame_U3CExecuteU3Eb__1_0_m03276802B4B26570E825CDA0F2E78DFE703FD2EE,
	U3CExecuteU3Ed__1__ctor_mDED1314A51DDE61865770C493D2D9CCDE42C4957,
	U3CExecuteU3Ed__1_System_IDisposable_Dispose_mB03A5268D828F14A443D72BCD62F9977A7434C2E,
	U3CExecuteU3Ed__1_MoveNext_m1474DAE3F9DE27390296341603CE9A80343FFBDC,
	U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE706719587A7A7BAE947DAB178A404636438E62A,
	U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m0A98AD78FF4D9FDFAFB4E2B4ADC7E918ECE084B7,
	U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m14B357E056A4AADB78A9CADBC779A46325788ACD,
	ActionSaveGame_Execute_m5C49646C313E617CF557E9FEE2D5EBC3AAA9FA10,
	ActionSaveGame__ctor_mDA66363E5C00A9CDEAD4E50CDA430E162FE4CFB4,
	U3CExecuteU3Ed__2__ctor_m1A294E66E9D46043E2E23F4544014588A8D604B5,
	U3CExecuteU3Ed__2_System_IDisposable_Dispose_m14FDC26EDECC814A10CAA9F29C23A76370143B6D,
	U3CExecuteU3Ed__2_MoveNext_mF7ED2256C924F29E662472C549BC1DFF1F036A68,
	U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF519EBD37B180B513D8F652C309B25B1F9E64536,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mF841C3F84566662D6B63615136CA3AD0C469B029,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m488D15C23530AC60B5EA48F0D808A137B7F2ECDC,
	ActionLoadScene_Execute_mE4830BE17B75DC6B0D7C331BA8B1D2D73A4821A3,
	ActionLoadScene__ctor_m93D2900CD98A09DEBE182ADF1CFD0F5C09FD85FE,
	U3CExecuteU3Ed__3__ctor_mB38F8F46EAA249E49BF209D521CA8E4DAD872375,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_m9A55C15113480F1DFED0E5D92EFCA663FAD44C22,
	U3CExecuteU3Ed__3_MoveNext_m7FAA086CC4758C929AA9429144C6B28462C5D3BC,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0C73195701400504EFD358451A634E203F71E2,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mC95F035CC2C00DD21F7CEB6C10C00AC264472D05,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m61B9FE124AD3E7B14B38A197435F1766A601DAC8,
	ActionLoadScenePlayer_InstantExecute_m64439B108A4AC8C5747CEA015432D1DE6B19C4C4,
	ActionLoadScenePlayer__ctor_m994D988E05BCC1308A28EE448608769D7D574BF7,
	ActionUnloadSceneAsync_Execute_m221F4EA5967752F1CE262C88154BBE883F5C5D7C,
	ActionUnloadSceneAsync__ctor_m50F8810D8C184399847A4FB80955688F18BCFF96,
	U3CExecuteU3Ed__1__ctor_m50099F295DA27DCA901FA8869D15AABEFB2A89F8,
	U3CExecuteU3Ed__1_System_IDisposable_Dispose_m73C667B2CD0942FC8C1D621030F62DE6EBCF43EE,
	U3CExecuteU3Ed__1_MoveNext_mA436F81DAD07F2FE9ED0B42D650ADB7A4119BA4A,
	U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD24FFC8179AF6C49CF69C41720A1CA67CCCC3FF5,
	U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m7A06787F5F87A17EDF4B9F7E89E5F121D17FAA6B,
	U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m302C6CB72E7DC52A8CE4D1CF360309C511F79770,
	ActionCanvasGroup_InstantExecute_mFC8DCE1FB7CDDE9BC98C4E2A5C4DB9F970A92A8B,
	ActionCanvasGroup_Execute_m99266FDCA2C58E3B774F0F5D01CE04A0755E06DE,
	ActionCanvasGroup__ctor_mC06D186F04852AACAE1C628725BF7B4474750CB6,
	U3CU3Ec__DisplayClass6_0__ctor_mB76E5D3A9023480D59247E3012AA072155CE1550,
	U3CU3Ec__DisplayClass6_1__ctor_m21D6D310D996DD859BAC20B62F54E85D60D0BF3D,
	U3CU3Ec__DisplayClass6_1_U3CExecuteU3Eb__0_mD9C59D9AAACFDE86A1DDAD4C655EA101B04C6A20,
	U3CExecuteU3Ed__6__ctor_m27E44C3A9DF57ADC577B19802B5193AE67FF720F,
	U3CExecuteU3Ed__6_System_IDisposable_Dispose_m6EDEDC583396BC219AC77E7CC4519AFF08AA95E0,
	U3CExecuteU3Ed__6_MoveNext_m718B85A7994C030999F57C1287E1EC1B4CDBD311,
	U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DC9E2350D3BAFA324B15BADB658E25EC071426,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m9A9BC40719774A6E6D1F34F82FE21CF69C9A2B55,
	U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m2AF14DBD96EB0B156A6600D4C0B33F3BB952F55D,
	ActionChangeFontSize_InstantExecute_m8188DF05DFDD64362707948AEFA550DA440C594A,
	ActionChangeFontSize__ctor_mC503BD4E7445C65A4BD44F5B7DC74380ABCD6134,
	ActionChangeText_InstantExecute_m8B15CCE23A4E6B94B502CC8493BC6BAE4AE06634,
	ActionChangeText__ctor_m3C82336B5A63C53209FDAED7FA15CBD8895115DF,
	ActionFocusOnElement_InstantExecute_mC4C7FE790D5BD2E97A762666377639502C9D2A62,
	ActionFocusOnElement__ctor_mEF2D1413C5256485BAE70F8E08D525471FB81982,
	ActionGraphicColor_InstantExecute_mD5BDC42CC18C8BC00A154A7C1A4E070399B48AF8,
	ActionGraphicColor_Execute_mB50BCF17FFC8C783B2CAC5AA6999EC69282DBC60,
	ActionGraphicColor__ctor_m065DD87AF9CEC29B423B2ECE75565C500FF1FBD8,
	U3CU3Ec__DisplayClass4_0__ctor_mA482B568524BECC5B28BA36C17A6B643A97704EF,
	U3CU3Ec__DisplayClass4_0_U3CExecuteU3Eb__0_mF6B1183E51030785F4B0CD2D8B0B596485C51B0F,
	U3CExecuteU3Ed__4__ctor_m719692616249D8B1B9A18A4639A2BEBCF839B63E,
	U3CExecuteU3Ed__4_System_IDisposable_Dispose_m322211ED0B980FF930875657D598C03697632275,
	U3CExecuteU3Ed__4_MoveNext_m4601CAE8C93BF900AFEB74BB9FB1E8E315364D85,
	U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F499A2C7AEE9F7CAFC35900478C44882351C41B,
	U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mC64DC9F5F707FDAA833235FBCA5AB402F4B49918,
	U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mAC358A6CCA560BCC9EFA484A7E9CE5F4D47CB979,
	ActionImageSprite_InstantExecute_m3403E950E5C7317CE95A79496EA9E7B44B57F1E5,
	ActionImageSprite__ctor_m807DB0C862D7CA8308D9F0BBE3489D616E66910C,
	AudioBuffer__ctor_mFE5C7A4B3EDC91C7A4D8619C6981819951771553,
	AudioBuffer_Update_mFE68D8A5F570AA8BC4CA8AE15EA603646DAF57B3,
	AudioBuffer_Play_mA872195793B3D3E09FA2F4344120EBD33D7E6939,
	AudioBuffer_Stop_m861C15494929753C966FCD4BC31556ACBA21840B,
	AudioBuffer_GetAudioClip_m40490AD5C5840397B82E01C4A97C90635754FACF,
	AudioBuffer_SetPosition_mA63FDF7BBD0C5C3EE077CE8DACE5A107E6A69248,
	AudioBuffer_SetPitch_mE7CD2426B005A30893E62F70DB017AF91C9EF79E,
	AudioBuffer_SetSpatialBlend_m7AF01DA4A9275D5CF14AECE20698FC22C58F5110,
	AudioManager_OnCreate_mBC2F5EC1831F2F6A79F6B9E47BF9EC989D1E2A2C,
	AudioManager_CreateMusicSource_m1BD781E8B6833F8C2E55DE83FD69980C7F4C73D8,
	AudioManager_CreateSoundSource_m994C718E7383142ECB86C3F1DE18BD2B7A793B32,
	AudioManager_CreateVoiceSource_m0C1A596D0F8EFBB1EF012EFBFE4418134FD10B2A,
	AudioManager_CreateAudioAsset_mD6A70A2854A97D47630FBFCB5307EBAA8B9828D9,
	AudioManager_Update_m777F5CE15410D58878CE7D462E60CABADB43D488,
	AudioManager_PlayMusic_m5BB30A7648E5055B6F6DED7D691CFFC21D60C769,
	AudioManager_StopMusic_m96E45AE6A9A199D98DB47B6A6022BB4BFB7D95B5,
	AudioManager_StopAllMusic_m77ABB7542AD63C8FBBC8D258EFD00ECD7DA975CC,
	AudioManager_PlaySound2D_mCC4F71BD7E1843AC7374A8CDF9B30F4F5EDAD7A6,
	AudioManager_StopSound2D_mC2861318A7B4C8F8FC55E896520682CBD675CF8F,
	AudioManager_PlaySound3D_mD816B797E25FFEDC78DFA775185164AA70435E3B,
	AudioManager_StopSound3D_mE0AF6C87871BFC2CEA92F54431DF1A9BF742E3D3,
	AudioManager_StopSound_mFC4EAA7E9693C15ED4F8E4604D83031E1226655C,
	AudioManager_StopAllSounds_m651E1A529F4EF8D77A49BE960016D3A277B4F8FC,
	AudioManager_PlayVoice_m589094F8CFDEB8C0AA1308E48DEF40C3FA1CC195,
	AudioManager_StopVoice_m0B6EA9A1D011EC6A59217E5922646E6877C08D6C,
	AudioManager_StopAllVoices_mA5BEAF8A8E2A7E1DBE1C54E71ADB09870AD4025E,
	AudioManager_StopAllAudios_mDE6935DE28E855F55E6067D3899ABA930013F7D5,
	AudioManager_SetGlobalVolume_mC78A4C92E8DFCA6B94D9CC2BC57DCABEFFB3D69E,
	AudioManager_SetGlobalMastrVolume_mB9BD6C11628BC171FFD857025448BDAB4F76AFE8,
	AudioManager_SetGlobalMusicVolume_m2E57E4936EDD01C7B9C441E994189096D56E8555,
	AudioManager_SetGlobalSoundVolume_mE88A627BC73D89119A4DE5A5ECE2EB1881334EAC,
	AudioManager_SetGlobalVoiceVolume_m327FF33E0BD5464E36133743AB811559922AAEAB,
	AudioManager_GetGlobalVolume_m224B9705EA865E428FFBFF34B853086644DD2008,
	AudioManager_GetGlobalMastrVolume_m7C5D77F1BFA82E0D0323F0D19D12836D8C6319C1,
	AudioManager_GetGlobalMusicVolume_mC9BA5499B0F8230A7E5FCC3634A4C0ECA7FAAA15,
	AudioManager_GetGlobalSoundVolume_m9A358B18F9F314B8265BF166B1E364AE4CC40147,
	AudioManager_GetGlobalVoiceVolume_m9516BC8AA7E6893845631AAFAAD5B29D62AAFAA5,
	AudioManager_GetUniqueName_mCB536A5A75B03F788291C4E0E8ABEF845195EA62,
	AudioManager_GetSaveDataType_mC0151C70ECC42821D722458F97EE6547E486ADF0,
	AudioManager_GetSaveData_m728DD74D5F501F2BA61955C65B2287ED7F164191,
	AudioManager_ResetData_m74A07C301C442C8FF0ADE5519F2D64AECE60DB2A,
	AudioManager_OnLoad_m3F8D2AEF6D665D9192C0A362EDF1A5C825B19EAB,
	AudioManager__ctor_mE0302690294F0D3A91645B6BA76C9BA2D070FB03,
	AudioManager__cctor_m2461AEB95DDA7001E05525AB4E4494E7469031D5,
	Volume__ctor_mBE3C51739CEC1A217A985EC47CC69B77822EE43D,
	ConditionPattern_Check_m608C578D0AA69C72465418CB40AE1AF1DECB4678,
	ConditionPattern__ctor_m9D290D22F50580F340CCA9FB802A134376A5C0C6,
	ConditionSceneLoaded_Check_m10F81907783356BCEBFD797AA44BB4B8810C1C2D,
	ConditionSceneLoaded__ctor_mCCD95428508DA4F3F9AB6545022C8268EBBAF433,
	ConditionSimple_Check_mCB15C2C01D3DC368ABFEB935689C08EA76F2B59C,
	ConditionSimple__ctor_mB488E2AEC8F0873FB0CFC5D8BE6D6526E48B5502,
	ConditionInputKey_Check_m44DD27E5E9DCADE96E47B5E6A87F97C1EF25BE7B,
	ConditionInputKey__ctor_m7F9CD2FE8D4EE0204C17B1AFAB61A0DFAA36441D,
	ConditionInputMouse_Check_mAA2DCC245FEFA7DD42941F615BF7BC6D146A4C5C,
	ConditionInputMouse__ctor_m8CF986A1321A4EAAF9674195FB7A4FB53BC59237,
	ConditionActiveObject_Check_m9FBF9A1E7586E35EE864FF7B848EADEA89BE7A90,
	ConditionActiveObject__ctor_m1EEE9CC012389EBA46904865FF91C5D448EDA223,
	ConditionEnabledComponent_Check_m719B977A081EE6A74DC1EAE1B40E01F468576321,
	ConditionEnabledComponent__ctor_mE8031DF3002CF100C8AB83D795236F3231D80FE5,
	ConditionExistsObject_Check_m45EA262A531B1121C58E82C00A8F194A2035A307,
	ConditionExistsObject__ctor_mF04F641775641728E0753FDEEFC2FD77BDB8D1A9,
	ConditionRigidbody_Check_mC2DB9AD58677271135A45D814EDD9C24C62479E9,
	ConditionRigidbody__ctor_m6279AB268A044952DADAFD20CA62BBC2A8BE63F7,
	ConditionTag_Check_mDA0C9D7648F48598BECF87A2C17D19DD5A082096,
	ConditionTag__ctor_mBED95CA09F7538AF738F55C4081ADD588C33A596,
	ConditionHasSaveGame_Check_m3E70ACDA594C99EFD110AA9F1315E9437AE11C6F,
	ConditionHasSaveGame__ctor_mF8CFDBAC667C2728F41E5AF90115550902CEFF70,
	HPCursor_Initialize_m9FF4BF6D3EA65EFE5E375B3B1610DDBBCCD922D8,
	HPCursor_HotspotMouseEnter_m09ACDA80AECD5E629712F39C4129BBD37006DA15,
	HPCursor_HotspotMouseExit_m19AF919867C2716BEBB5A0314978D92137CBD584,
	HPCursor_HotspotMouseOver_m32888765C5259D1FA8B96823E20F5B2498B1293E,
	HPCursor__ctor_m84B4122116F89FEA83ED3E9482DFB0B23E3BA0BE,
	Data__ctor_m1C4204CA27CAEF61BD7F08F3146DAFCE0E269B1A,
	HPHeadTrack_Initialize_m37D877268CB7E93DF03CFFEEC3D1D2FCE82E766C,
	HPHeadTrack_OnTriggerEnter_m9B467E5B2D42B03EB7960D603297313C403677E5,
	HPHeadTrack_OnTriggerExit_m112099B8DF57D10460D435D2FEE642A208056A62,
	HPHeadTrack_HotspotIndicatorIsTarget_mA97C4FB1EF719A8667473132F8285D1118FE269D,
	HPHeadTrack__ctor_m2A78894EDA961758ABF6D91A0DF78EEE4074B30C,
	Data__ctor_mFF2BAFC8635B8B3B4861D730ECB428E960AE9E5B,
	HPProximity_Initialize_m12F05889065A1F6919F2124ED18D62B03FC12603,
	HPProximity_OnTriggerEnter_m4133EBA18EB99206C25ECD7281A5F3095BAB8737,
	HPProximity_OnTriggerExit_m5DB20065610006E266F5AF36A0792B4B4048E6DB,
	HPProximity_HotspotIndicatorIsTarget_m81B99169E9512362548B320EB6DAF1FF2A5803E7,
	HPProximity__ctor_m9F5BC9754BFE47F72E4FDBAA45FD12091D028891,
	Data_ChangeState_m8A028B6848BEB767C963BEBE35368F1DDE92B6A5,
	Data__ctor_m2849F741D7670F6890717C41088E2320B2DF7499,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Igniter_Setup_mC3B1EC1B396B999EEF93672A08A80DF29A3CFCA4,
	Igniter_Awake_mC2E9C9BCA939A44843E47FA55C4ABF34B9797437,
	Igniter_OnEnable_mAD4D4787890AFF225025BA0BFB34E315618D866A,
	Igniter_OnValidate_m60E97AA7AF7FEC63563FACA80C02E3912861C69A,
	Igniter_ExecuteTrigger_m0177E0F91EAAEC6E3060E3999D94BC632F6DE15F,
	Igniter_ExecuteTrigger_m62CFC9086E2F87B0172D6D91A4D9B5ACF946AEAD,
	Igniter_ExecuteTrigger_m5913E13D844E3833A79E888FA2F887EAA1BDB204,
	Igniter_OnApplicationQuit_m93AE6D0FB709202A7EFA4503F5BA9491A2FD3430,
	Igniter_IsColliderPlayer_m1526B8C714F1C6E51ED804CE60491D5EAB6A659B,
	Igniter__ctor_mD6241C66A7C942BC21390F2F0D1FC482E6D01F8D,
	Igniter__cctor_m5767AAC294453D8ABAFFF6893EF4CDEC6DE87B67,
	IgniterCollisionEnter_OnCollisionEnter_m43E2DEA3D592E567BDE72C6DA9516E4772408CCB,
	IgniterCollisionEnter__ctor_m8F33BDC1A02D6712CA30E292D7500C7D17E2B10C,
	IgniterCollisionEnterTag_OnCollisionEnter_mC7C9F695D5AEB69FBFC49BAAE203DECE0E4460A5,
	IgniterCollisionEnterTag__ctor_m23D7E2FBC718265D11727972218F255D1CF01722,
	IgniterCollisionExit_OnCollisionEnter_m8067EB459E10F194830F8AD4F0FE653C6498236D,
	IgniterCollisionExit__ctor_m57636BF8416B44146CD4C710AA084ED280A2DCBD,
	IgniterCollisionExitTag_OnCollisionEnter_m1FD121A2A9165849CF70E1E290BC206E1596B8A3,
	IgniterCollisionExitTag__ctor_m73C711F1F32DA59B6F4F4DC5777133D83302D414,
	IgniterCursorRaycast_Update_m003B8B037767BAF3CBA3DEB85E9BA5AC712B0614,
	IgniterCursorRaycast__ctor_m04313D6F2785D8FD3409F85D824D4ACFCB294A2D,
	IgniterEventReceive_Start_mA1E02B824315BA352AA071C41F978E160605FDCB,
	IgniterEventReceive_OnDestroy_mF5E427A22B07EEDA77C857374921BF8A3BAE543A,
	IgniterEventReceive_OnReceiveEvent_m7A99BF2750B8244EC812476BADD07F69C2EB3649,
	IgniterEventReceive__ctor_m87F1CB2680AE98B8ED813B0D6E1FF9D942D7DAFC,
	IgniterHoverHoldKey_Update_m39876EEFAF8217116EEDBD7E9572606E3F358E62,
	IgniterHoverHoldKey_OnMouseExit_mC3F052632122A399E3C79AA168CB20A4E128FA72,
	IgniterHoverHoldKey_OnMouseEnter_m2AB538C7D877A8636B747F29ABF5D3F9BB3347B0,
	IgniterHoverHoldKey__ctor_mD8E9E6DFF1103F72993AB267FFB17DA2D19B4AC0,
	IgniterHoverPressKey_Update_m935EE3287AACFF0D082EAB9A4D45F6A5F3730B9C,
	IgniterHoverPressKey_OnMouseExit_mACCC6899985612E032394D9AEFB4B15354D26365,
	IgniterHoverPressKey_OnMouseEnter_m5286D46ACE681FCB3B9D90FA0A96EBD5652B4571,
	IgniterHoverPressKey__ctor_mCF524860198F2B11BC22F4CDEC3DEDEEF2349692,
	IgniterInvisible_OnBecameInvisible_m0CE4AEFFBAB022D5330A20C8DF93791C2D5A8CF0,
	IgniterInvisible__ctor_mB98ADA26B7CEF8CDC66A9F04A05B156973EA288A,
	IgniterInvoke_ExecuteTrigger_m0C367F3EFBC14B922F84A5F846447D744B72AAD6,
	IgniterInvoke__ctor_mC8ABAA17396482ED209A6205B6E2D0DB9FD9963D,
	IgniterKeyDown_Update_m4E498DA0893C0D537ED8382D3AC3B8D26BCD8DCE,
	IgniterKeyDown__ctor_mD89B8FA6D98461C14028E90262A347DEC65D5C34,
	IgniterKeyHold_Update_mF39731922B01B0895459875D6C5241E75CCC908F,
	IgniterKeyHold__ctor_m87444F60F8A5D7FC04ACB1FC5606C7BFE2CBE8D0,
	IgniterKeyUp_Update_m84BAEFF3373AD4F82477289840DF2A3B8F77B5AA,
	IgniterKeyUp__ctor_mF0B4C3B7BF4F78AFCD0AE116D4E3D3FF278985DB,
	IgniterMouseDown_Update_mC23FCF294F412106159CE7F15C48FF1C13DB87A6,
	IgniterMouseDown__ctor_m9745AF43A914820D9464BF6F0AA10D9620D95555,
	IgniterMouseEnter_OnMouseEnter_m2D19BF8512E83A5BDA215215E8F98485E5DA7815,
	IgniterMouseEnter__ctor_mA68F31EDE461A216747D5622792954442B9786ED,
	IgniterMouseExit_OnMouseExit_m9170A8A34C8145B0E69CF8EDE1E7EDB6FF6780CD,
	IgniterMouseExit__ctor_m75974BC20C0B436EAF929DC494D2135B8CE81618,
	IgniterMouseHoldLeft_Start_m23D379FE1616B517668B56A1C6E28C295FD58236,
	IgniterMouseHoldLeft_Update_m35EA351618374DCA5E45C540AE733C38C5676741,
	IgniterMouseHoldLeft_OnPointerDown_m332CD1FAE25A8FDF6C0D4EE069B9BB8D812476B8,
	IgniterMouseHoldLeft_OnPointerExit_mEBC45FDD1E3B0D86EB08C8B2C003A76E3CD849C4,
	IgniterMouseHoldLeft_OnPointerUp_mFBF9A4B1554E09F209034041935CBCC3434E5311,
	IgniterMouseHoldLeft__ctor_m4EC0211F6D688B57E064DA57A27E59C6BA8EBC69,
	IgniterMouseHoldMiddle_Start_mE64034455FD1093230CC49E9BCFF50EFAF6572A4,
	IgniterMouseHoldMiddle_Update_mCE9E4EDB3343E1BFF11FA80FBA8ACD28A955DC36,
	IgniterMouseHoldMiddle_OnPointerDown_m81991E0B2DC5A0C9DAA2BEB7ED72665DBB911EC9,
	IgniterMouseHoldMiddle_OnPointerExit_m2B9CA16EBE33549C33D5F0430CD82D27592484FF,
	IgniterMouseHoldMiddle_OnPointerUp_m396FEDD4A20955CA646B8B20B3D57CD83EACFC8F,
	IgniterMouseHoldMiddle__ctor_m37ECBC942E72B154B5AE3C8305B790E886A4C83E,
	IgniterMouseHoldRight_Start_m9BB86E33D54A0BD37677A7093800E609A6083AD2,
	IgniterMouseHoldRight_Update_m79B60F54F95127AD0DBE319792CB590DF3FCDE94,
	IgniterMouseHoldRight_OnPointerDown_mAA0574BCC3AD0896A27904E1D33C38AB30036862,
	IgniterMouseHoldRight_OnPointerExit_m833BE2730D52B3D962DA956FC5E293B2EE08F53A,
	IgniterMouseHoldRight_OnPointerUp_mA18C72C53DE4978C910DE8F8A96136139504A134,
	IgniterMouseHoldRight__ctor_m6934C27E9E7AF35CA030403CBB885981B41CB63E,
	IgniterMouseLeftClick_Start_mEDDCBE52A5D1FD4042C37F26FE905000128025B7,
	IgniterMouseLeftClick_OnPointerClick_mA08FE7BFA176D81FA8742FBB3217CF3EFEB9A783,
	IgniterMouseLeftClick__ctor_mAA817A372E584AC3CA838A7C4AA55D1682EE15B4,
	IgniterMouseMiddleClick_Start_mCED056ADCCC0928DD2EDFEADBAA2FEE69A9A7BA4,
	IgniterMouseMiddleClick_OnPointerClick_mA493E6C5A1485C41CC4C9795E3D40A2450AAC6CE,
	IgniterMouseMiddleClick__ctor_mD74CF90EA0CCE5D6B3D1165677DB034E15018009,
	IgniterMouseRightClick_Start_m2ABF3380E45BB361A52576447B327E1D5CF08334,
	IgniterMouseRightClick_OnPointerClick_m8B2E71BA03E806957BB0EBA36DB404F91F1E5111,
	IgniterMouseRightClick__ctor_m4FD08197319D91CAFE8B2F5C7FC12D75BAC889F1,
	IgniterMouseUp_Update_mA46325FFEE9AAF48A635F870BB589D92A05EE334,
	IgniterMouseUp__ctor_m850F31E4C844F0815CB998149255F66215C342F1,
	IgniterOnDisable_OnDisable_mE617871EF339BBDED4510FE2519F2C78DC697603,
	IgniterOnDisable__ctor_mCD7F7B6D616B5363B45448FF175E970A34B63C6B,
	IgniterOnEnable_OnEnable_mE65BF907BAA48B0753D072AABFF46090D4AC02CD,
	IgniterOnEnable__ctor_m7A66AC2D172BDEDABA28DC5C58583343EE23BAE0,
	IgniterOnJump_Start_m7077150F799EEA0019DFC2339B903709AF5F70E8,
	IgniterOnJump_OnJump_m558F3E2905B07A19B5E3ACE9FD6ADF29FE3DB0A6,
	IgniterOnJump__ctor_m061B801777E2819B4D4DD7E29CDFD07A58E54DFA,
	IgniterOnLand_Start_mB732C362CD623CB6A75C957D9D7FF67A9EB4B585,
	IgniterOnLand_OnLand_m4B198E34515C910DF9E4084015C84CC1F46BDBCE,
	IgniterOnLand__ctor_m8FD0C03F3AAF748BA3576C55B86DFDB061C8E1BB,
	IgniterOnLoad_Start_mDFB87CD6403683BEE1E65486DEEB0694C550BC43,
	IgniterOnLoad_OnDestroy_m2926D6B344F3D37EA6085DBA1E6808C08FA119ED,
	IgniterOnLoad_OnLoad_mC8B735E12A61D43982B3610DFC63C2F01DC7D0BF,
	IgniterOnLoad_DeferredOnLoad_m64C11C5F16BD9E5EAEC2D50D48E978F52A238DC6,
	IgniterOnLoad__ctor_m8977EB10549A3466E83BA551CDE3DA48A28664C5,
	U3CDeferredOnLoadU3Ed__3__ctor_m5C8E1E3AA4B98F20105756C6130E53147A68BD9F,
	U3CDeferredOnLoadU3Ed__3_System_IDisposable_Dispose_mB475A3D3A9F97EDC5FD87B397BC25ECBDE3760FF,
	U3CDeferredOnLoadU3Ed__3_MoveNext_m49D64844F953FA9FBD9AF1594B86C0941A02031D,
	U3CDeferredOnLoadU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BF406ACFB64F6A8D3582DD5C4686BD52C2EC93E,
	U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_Reset_m5E740051AF8A54914754A26FF03008A1B83E4C1E,
	U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_get_Current_mD8F7F0E8B01AF6EC3C85ACB8B40A7FF6242DEF73,
	IgniterOnMouseEnterUI_Start_m534063FF048182CF900D973595F7CC11364BC851,
	IgniterOnMouseEnterUI_OnPointerEnter_m98259B679FB72031FD2F190BE58C6D5A8054FF81,
	IgniterOnMouseEnterUI__ctor_mDA8CED0CA8614E50FEE8B6238D9DDEEEF4B96A19,
	IgniterOnMouseExitUI_Start_mE0ADF7F2B822F2A7BA5F5A1ECDE6C0829F9F5644,
	IgniterOnMouseExitUI_OnPointerExit_m24A3F0C88D598A7FE25F75080B0398816A6BD802,
	IgniterOnMouseExitUI__ctor_m607D2FB41D62C2363FBE2D4661C82946E61A141C,
	IgniterOnSave_Start_mB26DA45FB9B289AC96514148AE82C00390CE3BB1,
	IgniterOnSave_OnDestroy_mAB75B09ECEE75CB377BBAE4F6C8946BE7FEFA9B6,
	IgniterOnSave_OnSave_m396483DB72D828C76ACCE4F0BF645DC936937F8C,
	IgniterOnSave__ctor_m0241F31E80B607B4319A7F374EEF095730A6BAFC,
	IgniterPlayerEnter_OnTriggerEnter_m90F4F2AF15EB9725EE7BBB7B48D8E9CC4C00F942,
	IgniterPlayerEnter__ctor_m30D2DB2B398D24D8F279706B4DB7B585F800F964,
	IgniterPlayerEnterPressKey_Update_m804205357C1AF258A429140FFD06D768F1D7C457,
	IgniterPlayerEnterPressKey_OnTriggerEnter_m641490B8C67D9A0167DBE684C04FC5EA593C4562,
	IgniterPlayerEnterPressKey_OnTriggerExit_m618BB5D7A61718C06A2BBD4E17B4BFD2886830FB,
	IgniterPlayerEnterPressKey__ctor_m13DBC7ABC110C332931F46368B1821F2F1EF783F,
	IgniterPlayerExit_OnTriggerExit_mBDFD28A8D35D8F019BD27970E5F1BD24E712692A,
	IgniterPlayerExit__ctor_mD0FCC1DB89966126F9190D021525E568B4FDD734,
	IgniterPlayerStayTimeout_OnTriggerEnter_mE186F0AB204AE6F3CA9FF332D6A12EBA306C4F5C,
	IgniterPlayerStayTimeout_OnTriggerExit_m26CD6D3F7EB60BDF2EA3DA46DA56F6EB5C29DE81,
	IgniterPlayerStayTimeout_OnTriggerStay_mBB4622AAD7518335E786E4BF36BF3BB509985D66,
	IgniterPlayerStayTimeout__ctor_mACAF71B87BAAE2B4749AA4F9B41C5D91AF22645E,
	IgniterRaycastMousePosition_Start_m3C269DBD2B569B5FA5FD012ECAA10473EE8AA40A,
	IgniterRaycastMousePosition_OnPointerClick_m610DB7B260641814FD9C0622E6F20DB7092A7BB2,
	IgniterRaycastMousePosition__ctor_m80EB79946A736E0D1B73C7E9A4F6184360DF7C32,
	IgniterStart_Start_mDA4327C504F7E8B5614C7EBEFE854FFB2C2EA8B4,
	IgniterStart__ctor_m61D6F7FE96CE90813B4B1866306D81132F3C4CB7,
	IgniterTriggerEnter_Start_mCA90CBDADD1381FA363CDE253D679914D89D051B,
	IgniterTriggerEnter_OnTriggerEnter_m1C6DB71DE38DC27D6FDC3CC30FEEE2D0C5FB2F5E,
	IgniterTriggerEnter__ctor_mD96D93BD86B32D9C41819D3970687D0454A0A63B,
	IgniterTriggerEnterTag_Start_m8C96DD5B2CB9BDB7D1F14E4A762BE7C89EEE8340,
	IgniterTriggerEnterTag_OnTriggerEnter_mBBF19E8401FD7705477B361F2F5F44CE6C551DE7,
	IgniterTriggerEnterTag__ctor_mB039C897D088F286423E40955DB376E186D55BD1,
	IgniterTriggerExit_Start_m83E75098EA38C3DAA6BFBDF6BD9524666CC5731E,
	IgniterTriggerExit_OnTriggerExit_m328A23B3018786FA5DF435A6891B0677CF594570,
	IgniterTriggerExit__ctor_mBFBAEDC1B58AD54E49D8472E877CB69E834FFCC9,
	IgniterTriggerExitTag_Start_mBA63438E8A9E24BC3A628238D383AA188C018A23,
	IgniterTriggerExitTag_OnTriggerExit_m392CAB08043B7A21D4F37F127D6BE75F5F286EA3,
	IgniterTriggerExitTag__ctor_mA8601E05C7BD77C3F0CF3A17274D8FAAAF80AB1D,
	IgniterTriggerStayTimeout_OnTriggerEnter_m39C9798CD74A70D067493DBCC99D5EC4B733392E,
	IgniterTriggerStayTimeout_OnTriggerExit_m53AD57D524CAAA14B7EDF243E0410B2F535EFE6E,
	IgniterTriggerStayTimeout_OnTriggerStay_m6B90A8781451B606777234E4D04E1BEAB5FD9789,
	IgniterTriggerStayTimeout__ctor_m300C2D59CFDBEA855C8D3945F160D8FCD03EA22F,
	IgniterVariable_Start_mFAA7D85379354FC6C398E81599604DD5DF377C28,
	IgniterVariable_OnVariableChange_m2E59EAA694B904B9AEBE78FF81D226A769F0B817,
	IgniterVariable_OnListChange_m8351BF9F0F95D1397C014F4027CFB74745FA6246,
	IgniterVariable_OnDestroy_m4C710FD4F50AB342FA764F72647A896B6DB06ACD,
	IgniterVariable__ctor_m55D7A2194387663B343522738365C7F740420444,
	IgniterVisible_OnBecameVisible_m2265116EDAB4BBF43CB4E3DEBC997B669F843E7D,
	IgniterVisible__ctor_mDB27D4D40EE7B28CFE4524A29928B6CDD2304D96,
	IgniterWhileKeyDown_Update_mB37CDD80D5E385075FE2E5858920349137C5D8C2,
	IgniterWhileKeyDown__ctor_m6D6C8224021FCA5C2D64DE0A71F8E094541936FE,
	IgniterWhileMouseDown_Update_mF295631074D1CE8DB46A2B271FF3236B662238F0,
	IgniterWhileMouseDown__ctor_m0823DD930CBBAB17E68795143810B9A9E463456D,
	TouchStick_Start_m39C9D732A7BF16DDA32FF426B8BE2A1741F27361,
	TouchStick_OnEnable_m2A913D859AAD75999747256CF37CA15C31DD5BB6,
	TouchStick_OnDisable_m62C7BD83B429494E8730E06667108267B851A32B,
	TouchStick_OnDrag_mABBC10FF6BC78F16B9E353965EB16781A2377C06,
	TouchStick_OnPointerDown_m7A97779E350C736428B612CE5124895ABE77D08C,
	TouchStick_OnPointerUp_m788060CD9FD1836EFB16874C38E3F292DFBEB27B,
	TouchStick_GetDirection_mDF78799859024D1AFFB049CC81E4BEB7670E5364,
	TouchStick_SetDirection_m9C27AB10B629507512A882C058ADED708FFD48C3,
	TouchStick__ctor_mE27AB7F0C45EC3D9A3A3E9CBA86BE58C56DE84A9,
	TouchStickManager_OnCreate_m636797065101B4C4FB0ECD895F91E5CDF5F594F1,
	TouchStickManager_OnSceneLoad_m27BD1E8FEB03CED638EDEE7735BB510E210079F1,
	TouchStickManager_UpdatePlayerEvents_m7144AC7BF194EC0BE881BDD2ECA1410E1F03B970,
	TouchStickManager_OnChangeIsControllable_mAE63B19719B9F48DA2DF6128FC35CDCC3D73CC0F,
	TouchStickManager_GetDirection_m30B183E1F9A3D990D82A4DFB930083F59BC88219,
	TouchStickManager_SetVisibility_m6200595A101FBD41404994EBC2A0B2E262833A18,
	TouchStickManager__ctor_m3CAD680A041D808F6566579A306E0DAF704175E3,
	TouchStickManager__cctor_m3B2F295CCB7A562B3FC0D53D5D7C45B2984B5A9F,
	Actions_OnDestroy_mFC2B35EE8300A684436681AD7A67C8C1F78C9841,
	Actions_Awake_mB0E337F429D50524C635671CED552C2A60326BB9,
	Actions_OnEnable_m28D6A232DC3BA403959A9A0F84B08A1BEDE03E3D,
	Actions_Execute_m028F25C515D5D301413DB5E45F9C970A69C3387A,
	Actions_Execute_m753E2E2998E02C5F2E806D59A36F5FB78C9A753D,
	Actions_Execute_m62B8E7EC0880236989338403898EC9742FC6F2EC,
	Actions_ExecuteWithTarget_m024C3C5B550AF7511201D956015401BE572268C7,
	Actions_OnFinish_m150299A1E4E025945B1F6DCB56D1D40D3802DB8A,
	Actions_Stop_m70B5D297E8E2FA2871B9FEDE74752FC3DCE7235A,
	Actions_OnDrawGizmos_m7CB70BFE85415070FDD4641EE9B3E35121A86B10,
	Actions__ctor_m4E5B373C8BDF42D2F6356DFAF862B4C92D32E7D3,
	Actions__cctor_m3A199AB5FA778C5F4E6B765EE73CF8A47040F596,
	ExecuteEvent__ctor_m7320C292EA87B508C0A4E6E836A20CA86772B6C9,
	Conditions_Interact_m7DF3743DBE4C025DF5AD5DA69B17B9D8AB73BC64,
	Conditions_Interact_m730603257F8CBFDE70A458D95E42F1374675F25A,
	Conditions_InteractCoroutine_m28D3C22682852BF779FC1C09E3A86331BB9EEB64,
	Conditions_OnDrawGizmos_m4229741782C602BFA0D538BBECC3B707DE073AC9,
	Conditions__ctor_m57AA7F41BA24E1D40D0F8D3D28B6EE0759BDE65E,
	U3CInteractCoroutineU3Ed__5__ctor_mD95C463FEDE49E3E955F7F9A1BB8C951B75C3122,
	U3CInteractCoroutineU3Ed__5_System_IDisposable_Dispose_mF1F4A66B05E74B40A8654C03EC212E38000C6FCF,
	U3CInteractCoroutineU3Ed__5_MoveNext_m24726723493F4A4F0A0D974C22D1794D2CFF7CE8,
	U3CInteractCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BCBBC8CAA0E427B467E5E183B7D934C8E4D42B,
	U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCFDA796A50A6135EA71F3FE4E5BC9E44C478C3E1,
	U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mE8C1D7E5CA1FBBB04F671E6939B8BE5D7209572C,
	Hotspot_Awake_mC998E7EA8E59A852077E9DA2B64C2A18C5A261D5,
	Hotspot_OnMouseEnter_mB9804C7CD78656A395B0BD441EB03DF84D51B11C,
	Hotspot_OnMouseExit_mEE60C2E87C8D1D6E3D5F2A0939F8A27416D8E254,
	Hotspot_OnMouseOver_mA0B8ED0837B05F28CCA858EC4CA4898A31984FE2,
	Hotspot_OnDestroy_mF5BCD8E234E58A8B6526DCB3E82A5D3A6DC93DD0,
	Hotspot_OnDrawGizmos_mFC69E159824D7CF02B12092D5FE963FFDC736748,
	Hotspot__ctor_mEE591D14A1571FE1ED64987BBE4084F09571BB44,
	Trigger_Awake_m041B79D685F982DDD0B897DB09F44F0E2A946954,
	Trigger_OnDisable_m927F60743CF15C4578AFACFB055F56D4EE4DDF30,
	Trigger_SetupPlatformIgniter_m039E268B916D2873413085FA6EA13DA7287DABD0,
	Trigger_CheckPlatformIgniter_mE73972AC98417134753CD2F8268F3ED252FA3792,
	Trigger_Execute_m1A6FC0B07CE3CC98139220B8963BE093CF3F0EFE,
	Trigger_Execute_m228F7389F4F940D018F6375A56C285803FB2891B,
	Trigger_OnDrawGizmos_m83A4B9B54C4409CA092A92C40BBDF33574E35780,
	Trigger__ctor_mE3CF6103551311CEBDAAE7896AAC698B4E8ABB0B,
	Trigger__cctor_m860B33710C9B1D86F6D5F17D4040335F7DC96743,
	Item__ctor_m8A79F7CDD986E8044B2C5466C25145F001CFBA4E,
	Item__ctor_mD9369BB440151931E5316AA9DEF0814CED60B6CA,
	PlatformIgniters__ctor_mC5BF3A8D71283CDBF1615FC95F3A0118D0C5475A,
	TriggerEvent__ctor_m649935D4523605FD40B8E11927028E1A7138AE7A,
	KeysData_Update_m6314F5F348C65173F16FA1B3F1FFDF49AB6656D4,
	KeysData_Delete_m5729C2D5379AC3F686E74132922BA7D35BDFF616,
	KeysData_GetCurrentKeys_m01577460B384E5B150069F67216EE2CABDDD5C07,
	KeysData_GetKey_mD8ECB9A3F544FA92E1A47A9EA52F084A6A11D57A,
	KeysData__ctor_mDEE11C28D1C704EBA1B1EBDFC1B8E7A4EA2E7972,
	Data__ctor_mDF786960BE82C20F06F7AB04170A701ACB407E5F,
	Data__ctor_m902FDCFD1F7BDE2B25A051550FD71871EDF4F8BD,
	SavesData__ctor_m9FB5191B94BE4CCA78A3F5FEC120835C751ABC46,
	SavesData_OnSave_m0215A8526801A255CD7353190FF5F1308908BAB0,
	SavesData_GetLastSave_m21354B84158DC597D99FE5D32C564DB86EAE7686,
	SavesData_GetSavesCount_mA8CD1A6AC76918B852A640997117B6ABDC0C6948,
	SavesData_GetProfileInfo_m096AEE837F504DD9F29B325AD90D56870FC5B1EC,
	Profile__ctor_m0651C18B7EFB79060DEE727A9DC0F7FAB9FEB712,
	Profiles__ctor_mF90C1A67E2BBA11BE4ABE7DCA281DC433043DD30,
	ScenesData__ctor_m4D0E2A75B27613D5474C1A165DEC47AB559F207E,
	ScenesData_Add_mF7AA174217EDF483EED605361D614EF58EA587EB,
	ScenesData_Remove_m0E4C81DC9CC4D1BC019E9FF4799E7BACEC715EC0,
	ScenesData_GetSaveData_m6D46AD38EA6F422D6C9F7BFCB10E0E629AA03EEB,
	ScenesData_GetSaveDataType_m0FA65655ACAAAE01CCC4D6A918173368EE37B20F,
	ScenesData_GetUniqueName_m64D4C160CE95624B5C1A6B0E1681E031E330BFDD,
	ScenesData_OnLoad_m9DD2B0255D05F2CC56291A43DA68F1D794D9B8BA,
	ScenesData_ResetData_m04448DBFD21D55A8D09D53C7EEB5127F4DA51913,
	U3COnLoadU3Ed__8__ctor_m2623EB815DC1D76469AE888CCFC58B3EC04319B0,
	U3COnLoadU3Ed__8_System_IDisposable_Dispose_mDF1AEC6C64F34D03F84D44928EF2E34986C0CC9A,
	U3COnLoadU3Ed__8_MoveNext_m420980EF293A2B4DCE636DA9A82C94700F6024DD,
	U3COnLoadU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC76155CEECA7B46580F2B1A8028F9F301BCDDAB,
	U3COnLoadU3Ed__8_System_Collections_IEnumerator_Reset_m56DC36C13893D1FB4DF0C015C520BFE5A539A182,
	U3COnLoadU3Ed__8_System_Collections_IEnumerator_get_Current_mA8ABABF1674E47A2E404A53461DBD8AF95075ED6,
	GameProfileUI_Start_m77E83860D1A06E55851D4B73B99C796E3D5D29D0,
	GameProfileUI_OnDestroy_mCCD9EE02D38672CDD957327B2C4BB7B219902B1E,
	GameProfileUI_UpdateUI_m99D0D709A17B6421E5ABE187A7089B6C2F633B74,
	GameProfileUI__ctor_m2CFEE491140B90B8C5ED4CFB1596E915E13B978E,
	DataProviderDefault_GetFloat_m36FC003E8E00DBAB967F37BD53BA0F80152E6F42,
	DataProviderDefault_GetInt_mFB0852533DFE956A1052CC04A47F7DC75B0CAE4D,
	DataProviderDefault_GetString_m3F82DFAB342988E0A18841CABCCD697332F49850,
	DataProviderDefault_SetFloat_m164715EE6E2CD77A889F6E031C10AEB869EB2A11,
	DataProviderDefault_SetInt_m765E7224F8485591CEA9A66B70DCB74CB13EDF8C,
	DataProviderDefault_SetString_mC4AD3CCC7297ADD1ACFB8C9E63CAB4B6DCD81FC1,
	DataProviderDefault_DeleteAll_mB48302D72051CE0D32B48E7EF15F1B913358DB58,
	DataProviderDefault_DeleteKey_mFACD63C8FF0658357F55EA5BA5EACFE516068959,
	DataProviderDefault_HasKey_m6F7D0B2A9D3BA3FA41A2BAB4A969C9B767A42A94,
	DataProviderDefault__ctor_m002E0BB99E475D604996A1E90866DEEC9F5019D8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IDataProvider_GetFloat_m64C478F7D563C50D253221AE7F0CDF8837931D2A,
	IDataProvider_GetInt_m1013A847488C16AE1509B68A1D840339A61A1EC7,
	IDataProvider_SetFloat_mD920C29AAC301C6DCDC91697A8EB28A8555E5806,
	IDataProvider_SetInt_m3A062C8A86B183C799DDABDC658D79AB54733232,
	IDataProvider__ctor_m15C932F3A99997B660614D6BB8D22EC679C5AAD9,
	RememberActive_Awake_m6A0C7290441B48B7295E8D725AF1CFB1A6C94151,
	RememberActive_OnEnable_m59895BFDAC72B15AD6E32676EE5D2A47EF7F8431,
	RememberActive_OnDisable_m284785B0B430A3016A0688A14C95B2EF08CA7AA6,
	RememberActive_UpdateState_m68A57A63F72B7E8E46A4F3467181C9CDB1D2238B,
	RememberActive_OnDestroy_m280A1B7700B672A6B4BB31A926442E9CF005E28E,
	RememberActive_GetSaveData_m629F2E95EDA0775514A508EC98A11AF85DD98986,
	RememberActive_GetSaveDataType_m0A009C072897F55370599A25F4898C93A1A1BD35,
	RememberActive_GetUniqueName_m82FA76CC5A03BF6779C924921451C8E1708CA03B,
	RememberActive_OnLoad_mDEE857E252E85C0348B093FD3AF741F1EB158E7D,
	RememberActive_ResetData_m072659E48606ADB37358FFFE064E1AADF3922F2C,
	RememberActive__ctor_m45821ED2A8C4B0456F7DE339271CFCFEA8104F41,
	Memory__ctor_m45B98EE1C3540BA23E7F9DF5DECDF2745A9EF0EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RememberBase_Start_mE9ACC15835ADFA57140712D43533F97FF1C988CF,
	RememberBase_OnDestroy_mBF795437D6B9DCAF024BF7837218738DFC6CDB3A,
	RememberBase__ctor_m0162687B0B84A50712C5FE1F019E35DF6BBB3DA3,
	RememberTransform_GetSaveData_m7BC204A5636276D3B7CCD881D1F9195FF6CDF384,
	RememberTransform_GetSaveDataType_m5B7722FB22BE7AC0FE25FDA1BBAE91E53C515F31,
	RememberTransform_GetUniqueName_mD1849ADAD7505028D12CD2612442321DBEEA5019,
	RememberTransform_OnLoad_m5A495A443004AC8B88340AE49E2EFFD83FDC1E63,
	RememberTransform_ResetData_mF78A0341D00F2835EE84D37486E04B183D342555,
	RememberTransform__ctor_mE63BBF05F5EEEBFC724B0CD572193E477E100C10,
	Memory__ctor_m54D380158DA6738E0931614C265BFDC7A27D063C,
	SaveLoadManager_get_IsLoading_m48BA1B1C94B4C49EDD9AE123CB172588AB0F677B,
	SaveLoadManager_set_IsLoading_m72592D60732CD135C5A77D6F0983FB422F577C07,
	SaveLoadManager_get_IsProfileLoaded_m5284D5C2C5C09F8DD70C4F0F655C382F849A95FC,
	SaveLoadManager_set_IsProfileLoaded_mEF54E5023DF9D2A82D2B1B725486EACABE8A93FA,
	SaveLoadManager_get_ActiveProfile_m9738798B39E0A1677771A2D85620B66F7D9E4D6E,
	SaveLoadManager_set_ActiveProfile_mE1B1C4AE9C7C893FF128D3693F0AECB79E4AA4C2,
	SaveLoadManager_get_LoadedProfile_mB06CD36F75FFBC8CD6CF6EAA8A9D49B52AAC186A,
	SaveLoadManager_set_LoadedProfile_mA4FCE167840C7250ED217EB4D9B03F89C7D4EFC2,
	SaveLoadManager_get_savesData_mF5D63C886C2D47F216FA1BBBF71EA45CB5E825AD,
	SaveLoadManager_set_savesData_m25C89E92AB5C3540BA52D7A12A63801808C510E6,
	SaveLoadManager_InitializeOnLoad_m10EFC6CEB1BF1717756D029431963EDF4A3AB8E0,
	SaveLoadManager_OnCreate_m81DE2F6B61CE9CDDD170E397663686BC6CE95B7C,
	SaveLoadManager_OnLoadScene_mA376B37FD4489D9DF1BE29F5B29B378CB50C2BF3,
	SaveLoadManager_OnUnloadScene_m7B04C7FAAC17FD040DC5D67BFC5289C63D68E5C9,
	SaveLoadManager_SetCurrentProfile_mF27EB4254B93FA933135012DBD3B8475D0437030,
	SaveLoadManager_GetCurrentProfile_mF07D3349F2CB7F8451BADB9A81FE1121C2C61C9E,
	SaveLoadManager_DeleteProfile_mC93CBF58D60E4A1C27B6A7DD7549753BBE019B1A,
	SaveLoadManager_Initialize_m55BBF273B0AC96A6AF463E338E7418A043E8C117,
	SaveLoadManager_Save_mCBE83510A454B865AD8289F4CB42DC7477209A48,
	SaveLoadManager_Load_mC4E72352680186FF525832DCD993B7617A3E3F4F,
	SaveLoadManager_CoroutineLoad_mA7DDBA766A8C4F7A96D4F726AA978178F4FE9CF0,
	SaveLoadManager_LoadLast_m5DB788144BFF7809AF2D3001A8DBFE0AEB888280,
	SaveLoadManager_GetSavesCount_m5DACFB70C34FCFF42D3252F118B92B09FF33811E,
	SaveLoadManager_GetProfileInfo_m545680876276621234A0838B7D573DBF1DB3DAB4,
	SaveLoadManager_OnDestroyIGameSave_m3CC47941A3C361FB1996D49CDCADE5EC3AEA3FBB,
	SaveLoadManager_LoadItem_m87A01D70724F65541BC9A759C9060AE15C4C4503,
	SaveLoadManager_GetKeyName_m270659FE12BAF7D89C3F8BD9E04BD9C79107C6DB,
	SaveLoadManager__ctor_m1989B50AADE49ADB1ED5F24BC384A967254ACBCE,
	Storage__ctor_m878B0AB68F0E4B7346B80B40EB10BA428C5AD5E5,
	ProfileEvent__ctor_m29739ACE435BC396B6E03DE7999A7948AF6A1913,
	NonSerializableException__ctor_m65AD626A2F44C22A0186EB8AAEFAAA46D9F54116,
	U3CU3Ec__cctor_mCFB839B220BE8478B18998CBE86279DC4D87339B,
	U3CU3Ec__ctor_mAC75468494725563BFE6E5F1B65D40E1787E6D3E,
	U3CU3Ec_U3CLoadU3Eb__40_0_m6DC9A3DDCEF98B03270D731D4678F8AD7580E8FA,
	U3CCoroutineLoadU3Ed__41__ctor_m3223DF3BDBA925B9C01B1075D3CD3347CD43AEC0,
	U3CCoroutineLoadU3Ed__41_System_IDisposable_Dispose_m9988DEBB87C0389FF609CC218B64C147B325A8FA,
	U3CCoroutineLoadU3Ed__41_MoveNext_mC6F624465D7DE4C959C13B6A8DAD35F77C244B44,
	U3CCoroutineLoadU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m801AB1F500A3A2998AD56254068EE4CFEF1AFC57,
	U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_Reset_m1C07BD4300758CD834338BDDECA0875E978F353E,
	U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_get_Current_m5193714FB64E1B8E2DD8DACEBC03DD5545EF39D2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ButtonActions_Awake_m28B9AFCE9EF0B40805DE4C8B41449F555EC4E879,
	ButtonActions_OnPointerClick_mACF4831B872F627240D5F797AB548021DBE902B4,
	ButtonActions_OnSubmit_m5CDB8500688B75363E87546F349A19CA438EAE84,
	ButtonActions_OnFinishSubmit_m913CD3FD4E3EDFF039CFE2655DE2FDA322EE6570,
	ButtonActions_Press_m4E183732730A317BB189E11F861FE3771974FF1E,
	ButtonActions__ctor_m275325C2EF9197719BF51956948AAE2EDB84A846,
	ButtonActionsEvent__ctor_mD3DE58B33E07BB186ACC5BA699DB79434EF3BEB1,
	U3COnFinishSubmitU3Ed__6__ctor_m893E2EF3FB1B81650DC4D1CCFC1FCB37398F45EF,
	U3COnFinishSubmitU3Ed__6_System_IDisposable_Dispose_m2B5E0BBD91BB5335BF4D0D72AD2B8E0D12ADEC7A,
	U3COnFinishSubmitU3Ed__6_MoveNext_m7306FA775148F6605FB68089AAE0E2BC9E65DB81,
	U3COnFinishSubmitU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE497123EB4BC9F432B4072E2D058559767136CAB,
	U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_Reset_mFB604330DB8D7CD8B5C768C549F2946B73F59377,
	U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_get_Current_m43798DF5AB19460ECD104AF7F4F2FC05ADB14768,
	InputFieldVariable_Awake_m2E1C01DB29513E483A5E77EFF77E933CDC916B38,
	InputFieldVariable_Start_mD58D164CD3590465F9BEF7FFBF25F5C8D54EBEE6,
	InputFieldVariable_SyncVariable_m8EA9F48DE23DB5E1B6766B936A84960CFC24F2E4,
	InputFieldVariable__ctor_mEE7899474D66E61CC848F506EEE3C2A75F24078D,
	SliderVariable_Awake_m8927113CD1CB82F79F5086C809161CDCE6548114,
	SliderVariable_Start_m6C826149AE41B2E6F6FEE369E81D68F2C64A9D11,
	SliderVariable_OnDestroy_m4B2752A7157B6713BFB63B1183223F30F16A30EE,
	SliderVariable_SyncValueWithList_m9AAFECDDF26E307DE29D00C21E5532F09959CAA9,
	SliderVariable_SyncValueWithVariable_mB0D4D529490FE0CBF2C2718FE4162C4FD1E0D8C5,
	SliderVariable_SyncVariableWithValue_mB5E43D8678F7D50CCF27A45D839C05298882405B,
	SliderVariable__ctor_mE966D89B209542F022D58537377DA0CC618CF864,
	SliderVectorVariable_Awake_mDBBB2EA1AF2FFD99DD4DCA113DA3C0E6BDBAF181,
	SliderVectorVariable_Start_mBA2A7C3031B68683D0F51BBE7656FF0C2CF334ED,
	SliderVectorVariable_OnDestroy_m2171F85359CB80079F8E070F7682EA6B40A89A8E,
	SliderVectorVariable_SyncValueWithList_m4B6E1C129A2CF2AC245994DA01B41957D248BED1,
	SliderVectorVariable_SyncValueWithVariable_m3A1436991EDD2E26FEF5471671674DA7C0300F1E,
	SliderVectorVariable_SyncVariableWithValue_m788F0CC85D7450413ECAB7028AFCE6C666962798,
	SliderVectorVariable__ctor_m9BCCD8C22C781081AB82F2CB64F2BDAD5240569C,
	ToggleVariable_Awake_m05F078AB46B324163D72F71EFD35C23EBA70495E,
	ToggleVariable_Start_m171C38256E49AE00CB5DC6AFC86812AC8CCD66DA,
	ToggleVariable_OnDestroy_m11ECAB30572523226CD73C35C814B6D8A25166C5,
	ToggleVariable_SyncValueWithVariable_mA62EF9B208F1C10E6C5FC6E83683E90F413BC4FA,
	ToggleVariable_SyncValueWithList_m6C919C3B5607DEF4427B9B8DD24360D042F1D472,
	ToggleVariable_SyncVariableWithValue_mE176F524337B8612E7789E44100A8CEF5C7624AE,
	ToggleVariable__ctor_mE6C568A08CF8AE4373A094FA1FFD8264B5533671,
	VariableAttribute__ctor_m023A5E63AF4BB4D95294769E98F5521EB0D3C42C,
	VariableAttribute_GenerateRandomString_m986D8E86AC82A8B060F45C49419DEEB6F6BCB93D,
	LocStringNoTextAttribute__ctor_m2F3042F3260B6BEB7DF68552F97EA5E96D8143FF,
	LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D,
	LocStringBigTextAttribute__ctor_m476D7DD7EDB472B0B3CA92371824A8CD0CBC1937,
	LocStringBigTextNoPostProcessAttribute__ctor_m42D47E5B93D057A32E64D25E20EC56459C626D22,
	RotationConstraintAttribute__ctor_m67FAF4934092044140A0E4EDF2B2571EC3C1495C,
	TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE,
	LayerSelectorAttribute__ctor_m3F5FD467B8F48AD2B1F6670B88E06E4ACDF5FF3C,
	IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B,
	EventNameAttribute__ctor_m41852A61BDA5AB2D287649265057870A8DC92B15,
	CanvasRenderMode_Awake_mE5B1DEEE1AF74F60AFC8469B055C7E81052A4DF0,
	CanvasRenderMode__ctor_mE5C8BD458C8EC4BDC1D5D58C1D31A9DAA679AF9C,
	GlobalID_GetID_m7C7826203A2620A253CF8EC2D61A1CF3129BE815,
	GlobalID_OnDestroyGID_m023FFC94237D2E7E1778E07574D5DE440D65F48D,
	GlobalID_OnApplicationQuit_mBBA480D327619D7A4CF5FE624140A5CD0802D34B,
	GlobalID_CreateGuid_m2E80738FC023E12802DEEF4C0C1E25F594D38587,
	GlobalID_Awake_mCB8148C1106E870C9B9768AB810433A493020DBA,
	GlobalID_OnValidate_m1BEE450E20AF0D589887CC1EED0E8ADD1DA39D8A,
	GlobalID__ctor_m019E03CDBDC66F3E3B7B9A265EE3E0B799FF3286,
	TargetCharacter__ctor_mC995B7FF2BB8BD2522E7ABEA7DF75B804F37499C,
	TargetCharacter__ctor_m8B7B6983941F7F82A6EC3BEDB71CF6D90D00B506,
	TargetCharacter_GetCharacter_mC2843CBC87D87D3DBE9390C5552919A738A0C114,
	TargetCharacter_StartListeningVariableChanges_mDFCACA2ECE5ECEBADD7B1873734397AFE311C53A,
	TargetCharacter_StopListeningVariableChanges_mDCC6629802D2480702188648B3E7E38C0F0D67EC,
	TargetCharacter_OnChangeVariable_m9B7356AF4A4A584ED45F9FF1E4DEBECAA10F2197,
	TargetCharacter_OnChangeVariable_mF5B8C3E8F57CA63F595CC2CDB9F31C70CBA98F7B,
	TargetCharacter_ToString_mC05B1D08D37A34FACC8B142A9009BD445F700052,
	ChangeEvent__ctor_m48B36645B6B130B5B815295E439741090190B263,
	TargetDirection__ctor_m4B0CD8400860168AA6D11373048351BAD4DD75B6,
	TargetDirection__ctor_mDD8A3A7392B1C0374A60AE80809A7593CCDCF467,
	TargetDirection_GetDirection_m7081CE5BE371125AC0347D34F7089DD9D0076325,
	TargetDirection_ToString_mFA790A5C284101B13BB1AAD92792DCA228013830,
	TargetGameObject__ctor_mC7D13DF596BBEF1F5FFFFB75E47AC4C614F2C241,
	TargetGameObject__ctor_m742A24BC70C3F8FFD18A8BC0D3BF76B5E54B3CEE,
	TargetGameObject_GetGameObject_m24C7B5DCE02C07EAAB5552F4102F05C91347D3E6,
	TargetGameObject_GetTransform_m24E7036C334853515E050F49CA513C9F2D7F4C04,
	NULL,
	TargetGameObject_GetComponent_mFB6C4F7CA82D2369F40A04B690BC19ECE17E8ACD,
	NULL,
	NULL,
	TargetGameObject_StartListeningVariableChanges_mF3770C6C822874C2E4D836386889A36E1B0E6689,
	TargetGameObject_StopListeningVariableChanges_m0FE7CCCF1ABE2B8B12BA9B5C98E050ED5A93A409,
	TargetGameObject_OnChangeVariable_mFD31393A94437738EC1F3A610830A1A6C98E9A45,
	TargetGameObject_OnChangeVariable_mC4CA25EEA8FEE0057EA2F2A140F24BE4CF3324D1,
	TargetGameObject_ToString_m82AE2007064B46C1FD411B805BD276DA9E592FA5,
	ChangeEvent__ctor_mA4F2D27E8473D5C1D3B323B247121FAED5FAB708,
	TargetPosition__ctor_m2160D8C67055C072334E65F8A4393E657C597C1D,
	TargetPosition__ctor_mD1D16AD569EFC8A8F6BFBFC27DE4DD4C475F6361,
	TargetPosition_GetPosition_m637CE2ED4CD257B9D37F271D70CEF28975A51C6F,
	TargetPosition_GetRotation_m41F057CE3BA9C7E580E2FBB747EE5CE7B7C5B280,
	TargetPosition_ToString_m9674087D39FDF0ACAD16CD8D711EE426A47BD93E,
	UniqueInstanceID__ctor_mDF4F68AFEFDAA7B80BD3030AB1FAD5EFA2BCAE85,
	CoroutinesManager__ctor_m166D269EA0893785D76034A5D3A01932306992B4,
	Easing_GetEase_m57FBBD4072E8C52FC3D037FC376BD0456695CF26,
	Easing_Lerp_m7A0A1B3073CFCBFCBA5FBC5985BBA889CFABD04C,
	Easing_QuadIn_m5F1EED3339FA9129ED4CFB5B47155FC7479906B0,
	Easing_QuadOut_mFDB2772A277E6DFC396B748FF0635DC9C2E318E7,
	Easing_QuadInOut_m8A38B5641950362CD870898A32D71629DFB8CC84,
	Easing_QuadOutIn_mE47F0ED7B4E78EFD55F8B49E0BA211F5940C38DD,
	Easing_CubicIn_mED2F74C4DCBFB56B24B6031292CF7DA9DA69B9C4,
	Easing_CubicOut_mF47F96B153CDC83960AE53CBF4E84DE1307366F4,
	Easing_CubicInOut_mFACFEC02C9C847C10E481F9BD2997C066E9BF44A,
	Easing_CubicOutIn_m82829045838CD5D881B8B5A5D7B3E64552D1355A,
	Easing_QuartIn_mFEB9BDD01CB20773D8290DFE8B759F8ADB505A72,
	Easing_QuartOut_m8FA852389F119071937BB280AAE5D3E8D2F004B5,
	Easing_QuartInOut_mAC2EA9B95E19E45C135A79555D59683A32B98390,
	Easing_QuartOutIn_mC61F2EBE301DDE65E1C875A29E7AA3FE3002DB80,
	Easing_QuintIn_mDA7CA0638892A334208D4C28BB371DCE3CA8C877,
	Easing_QuintOut_m0EA804634345B183AA3BF433D86C7653E3418561,
	Easing_QuintInOut_m8AC49CBF4F1264EE27CA383737DDBD733FD1A27E,
	Easing_QuintOutIn_m8F5A3CC1D0A327F88EC63E768E2E15F12C57FA99,
	Easing_SineIn_mCC4AEE5DD769983001F1E5DED5ED956B114FC749,
	Easing_SineOut_mB6997547FD3F109B0ACD2A111241F6240FD34762,
	Easing_SineInOut_mF6608B1869E96FEBE7833314838D238A31E2314D,
	Easing_SineOutIn_mE8DD82F83361BB08381F2B7B53C30C739C58395B,
	Easing_ExpoIn_m271C943BA165B846A92E6F7D0B061480E41318E6,
	Easing_ExpoOut_m7D8EEB401FF7E69F79C2148E73FC2607DC0D218E,
	Easing_ExpoInOut_m4F6D554F14C073FEA759988FCBB9E6E33643AF43,
	Easing_ExpoOutIn_m97B1A24B86AE76FAF2FA072D28B5B7C08B659CD3,
	Easing_CircIn_mED7F2377FCBBAF8E707B7467AE12987A1EBA6E77,
	Easing_CircOut_m543BEE25BE82D74B746A1E72E4A1A3C81201DEC5,
	Easing_CircInOut_mB958C983AB03FB7B47D40C2E26E69F77ADCFED7F,
	Easing_CircOutIn_m58CA477CB966F7C3A5E38F1FFD5DB8DFFE1E5785,
	Easing_ElasticIn_mCD43F6E8BB260E553C6C81DAF10DEFA08DEB1D04,
	Easing_ElasticOut_m02C3B4C6F3A937AE7C7932F0D191ADA0E6D9FE83,
	Easing_ElasticInOut_mAE99A3948EE8FADCE38F264E50BCB7194935EDF5,
	Easing_ElasticOutIn_mB51988F99C50FC82181F9A665AB19A361F51780B,
	Easing_BackIn_mE040BDCAF71E215003D5206C26A8C72E0AE3D9BC,
	Easing_BackOut_m857C043D5ED508D5E7AD5C7E522059BD46B602E0,
	Easing_BackInOut_m2FF2C72C4A18B32870524EBAAD3A3851BD615281,
	Easing_BackOutIn_m191FF37A08502698110536C5E3EB44D015B910D5,
	Easing_BounceIn_m36761C816842613BFE145024C8116EFDE95D62C4,
	Easing_BounceOut_m623FFA5DE68F1109E18118FCDF499CEFF8313BFA,
	Easing_BounceInOut_mA72EC95BF02707FB1D879B2FFEE3394A93F1E507,
	Easing_BounceOutIn_mA7CA86DDA0203ACF1D0DF14188C51E6EB917D5AC,
	Easing_In_m335607C44528050C930CBF3B2FF92A24589836BA,
	Easing_Out_m43E5F01F0DEF94F1283AAE098868EE32FB140D75,
	Easing_InOut_m0D440931FC5A173A02ADFFD3D44A8C5F16DA0189,
	Easing_OutIn_m5879B12AF2797D65A292287D4345E1523C256859,
	Easing_Linear_m73409BE556E33EA0E947BE22959239B6D596E8F1,
	Easing_Quad_m8FCED52E357129D5E005DC04E507139E2EA51514,
	Easing_Cubic_mB31C34DBD48DD330CCE0E335A4016E368333E189,
	Easing_Quart_m038CFF8B9CC5121D61F9C70422C75ED78873F4D5,
	Easing_Quint_m4D317BC3CB4A6CAB38C3B1538C45A02923F61483,
	Easing_Sine_m1EF03D30BA9EFCA71331C763E8C2FFFFA0F410A8,
	Easing_Expo_m8858D435AFF2F216E5D5389A51029BF25BC204E6,
	Easing_Circ_m0A62AD4F5E066752C62B0DF9A6E2C71144EAB846,
	Easing_Elastic_mCDE7BDF98CF51464CE5887E14F7E8494D094257E,
	Easing_Back_m5C065BC75E150B8600754D7F01E6E4262440448F,
	Easing_Bounce_m148CBA7C3AF3E3930E8AD71501421EEE8BB5CA96,
	EventDispatchManager_OnCreate_m74D61544F98EF87FDEE03A270AB48478A7130679,
	EventDispatchManager_Dispatch_m4C9CCE465CDAEB22253976275238522434380305,
	EventDispatchManager_Subscribe_m2013731FFA13D6D5FD36776720E381566B6FE12F,
	EventDispatchManager_Unsubscribe_mA81C0C8F371B9D8A25951475F104C23F0AA6AA78,
	EventDispatchManager_GetSubscribedKeys_m9523AE4C57D9E3622D4EC647877FEA8E8FC3CC85,
	EventDispatchManager_RequireInit_m5BE08EAF970F3F144910EE8FFCEC8E8673D2DACE,
	EventDispatchManager__ctor_m7B01F8905DDE249D1DE62003986D56C3F8E927EB,
	Dispatcher__ctor_m0058A590B1E8D012DCEDA064E1608B70E0F98E79,
	EventSystemManager_OnCreate_mC9C237BF2EC97536ED4651CF002AFF4834321275,
	EventSystemManager_Wakeup_m3A671F4AFF31179CC18B96F4ACF68F792EAB9148,
	EventSystemManager_GetPointerGameObject_m7CE7D3717B16D1C5B067AB60152393E05FF9DEAA,
	EventSystemManager_IsPointerOverUI_mE8B61CECA53D9D6E79F02013EC33A744ACC810A9,
	EventSystemManager_OnSceneLoad_mB8B4DB6D5A75066BC5A8445EEB4180E36854FA4E,
	EventSystemManager_RequireInit_mA707A6B0575AF2EBA7C47FB7243332266CE6C32D,
	EventSystemManager_RequireCamera_m8E97F5AC18FD54EF80210BAEB204153746869C6A,
	EventSystemManager__ctor_m4DAE8F7A7A02CFED3794542A95FB1CB2827BBDE5,
	GameCreatorStandaloneInputModule_GameObjectUnderPointer_m06517611C25F7B816A9379CD5F096FF59C1BEE84,
	GameCreatorStandaloneInputModule_GameObjectUnderPointer_m14836EC0230548BE8FC4F0806EF629EED74722ED,
	GameCreatorStandaloneInputModule__ctor_m32B6723949D77EAC60747DEEF530897EFF4A34FE,
	ExpressionEvaluator_Evaluate_mF21D454D7897339A46F658B59428D486CC35ADBC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TimeManager_SetTimeScale_mEEDE46B57F1F17E3F4D8B72B3938FBA50716DCBD,
	TimeManager_SetSmoothTimeScale_m38FFB09FC7D5574F007740A251FD0C45908AECA9,
	TimeManager_Update_m1D7F0ECB60DA6FB28915EDB89BB6ECCC56922987,
	TimeManager_RecalculateTimeScale_m906E7EF6D3DD70AB1A88314E2069BFFA81FAEFAA,
	TimeManager__ctor_mC51F648957C62375EB611515037E69D4D4901188,
	TimeData__ctor_m04B54313A8A18140DE2425198C911D28E64A6CC2,
	TimeData_Get_m8A8C30CCE05B6580B5C0107D564053A91FE6738D,
	DatabaseGeneral_Load_mBEF0664FB6E5B775A92B5E1855FE8585140CC832,
	DatabaseGeneral_GetDataProvider_mA2DB709EFB601C0992D3D51EF1F3E3418E1A55EA,
	DatabaseGeneral_ChangeDataProvider_m57A8EC10AE4959E9FF6C8740E931FBBDD069496A,
	DatabaseGeneral__ctor_m4AD2C32C56B039D2820E63F1446B04229F10DFB6,
	NULL,
	NULL,
	IDatabase_GetAssetFilename_mCA6BBC257EDD36B35C934B9F487283E4A95534B9,
	IDatabase__ctor_mEF4097D1D76A55127E0899C684E7F2CB32B56422,
	DatabaseQuickstart_Load_m73A9F0104B54F6B45E70D9BECB996795BF8C9757,
	DatabaseQuickstart__ctor_mAF60125F5AD3D8E4696EB651A2A4B12146165415,
	Clause_CheckConditions_mC801467291AC7571A50985555DB38737062BC8F7,
	Clause_ExecuteActions_m0CFB6797D8E897ACD15B1E4097B5CCFF6C06468B,
	Clause__ctor_m76021F8997EE55C562A9F516A40F0B8A801F2010,
	IAction_InstantExecute_m1F07FB6A41C0EC6596C4428A770F4E6FB35884AC,
	IAction_InstantExecute_m512502AA6B9A94C8B5563F89D270D82275BD61C6,
	IAction_Execute_mC7BC452CD9CEB42F6B5FF76A1653C0318E4E7F9F,
	IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE,
	IAction_Stop_m732479EECFF9D88E69F63A107C677E2562680EBF,
	IAction__ctor_m02069D61821B3EB24BE1C7331C3CA538A539CFDB,
	U3CExecuteU3Ed__2__ctor_m7DDC9C9F4F6271CBC115EB695C09CD7020514C91,
	U3CExecuteU3Ed__2_System_IDisposable_Dispose_mCE34EB7AF956AF35D260652DAA9487875F5E5E18,
	U3CExecuteU3Ed__2_MoveNext_m81F513C5865780038792ACB85B6CC9B71655A682,
	U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD451C8F90AB42372F393CA15C9D4C30110852768,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_m15914DD547004FBF1D2595DB46564433345D5090,
	U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m9F64445ABE6D9783AD5D9537B7C8AAA82798ACEF,
	U3CExecuteU3Ed__3__ctor_mC5BAFE1C6CE5BF49D9685E6382F0D67665BA1271,
	U3CExecuteU3Ed__3_System_IDisposable_Dispose_mD044F79A0BF7913EA3A7F7D88B63DC0F6676553E,
	U3CExecuteU3Ed__3_MoveNext_m07CF32E1EB7087D03829F8556B04DE85299105AD,
	U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD2403DC5621D9B93CD571F03569AE0A49546A16,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mB8CD42CE43D0BE19240A03A82805AAE7BDB19368,
	U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m0810B2FE029F9C067DCCFD795ADD587BC8E9B744,
	IActionsList_Execute_m58A609A93DAF9BB26DDB4996AC23A068450D6D6B,
	IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE,
	IActionsList_Cancel_m28D90B6CBA72AA42140181F99C9789D5654D9D87,
	IActionsList_Stop_m999FB5131D73E7A06DB651D44E28A1F0A5CD14CE,
	IActionsList__ctor_mCD8BCE27505BAEF7F3D9529268F68304ADAA6318,
	ActionCoroutine_get_coroutine_m43BF4DBCBD32FA5208B4AC6690372328638225F4,
	ActionCoroutine_set_coroutine_m84480EA36BEBF51030B497DC4EC7E7CDC3C93F64,
	ActionCoroutine_get_result_mC8B5641A49CCF3B6CDE09E713E41E47D51B78BD5,
	ActionCoroutine_set_result_m5C7494A81C65703C4CA6CA479A3196B72508332B,
	ActionCoroutine__ctor_m1F13249399556C4C875AA6340F2F248F9BDF71FB,
	ActionCoroutine_Run_m5E2B10DAD3C70CE29D7C2785F8A0C216EFB90C40,
	ActionCoroutine_Stop_m8E14BC660326DF33FF5EA3E996777F502C63F46C,
	U3CRunU3Ed__10__ctor_m161D5DDA96EC9824164EAA25CFBA7959B31C271C,
	U3CRunU3Ed__10_System_IDisposable_Dispose_m4CB135F3D4224B9BAED517629FE906CE77A206F1,
	U3CRunU3Ed__10_MoveNext_m186F0FA0414EFECC6B9714342DFAC6487DCB80E2,
	U3CRunU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96EC009DA64F1B02F4571CF2C15B947E77829245,
	U3CRunU3Ed__10_System_Collections_IEnumerator_Reset_m1061AF69C60892787419DC2081CFB2D051A34488,
	U3CRunU3Ed__10_System_Collections_IEnumerator_get_Current_m7AF120E5C89DA314DC5E5AB7794A4F40258E1F0D,
	U3CExecuteCoroutineU3Ed__7__ctor_m83AD0BC314DA34572FA3D490CE3253BFB7F5FA68,
	U3CExecuteCoroutineU3Ed__7_System_IDisposable_Dispose_mA37486D7327AA40D8CC84D0492AB0A0121403CB0,
	U3CExecuteCoroutineU3Ed__7_MoveNext_mBC55C2B6383CB0B9E6A9029132B8F3ABE22402D0,
	U3CExecuteCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA7ABC9F3A7C54472C63995C362F345FDCC8862B,
	U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m81F9FA9DDF2680024453C428C9D7EEF0CA4F7ED5,
	U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m4A0F6B20C3E45CE35088B56C0E871F65FB617DEE,
	ICondition_Check_m81530D798167254D63ACFF4ABD22B5C8DE6A72F2,
	ICondition_Check_m84512F42B00660DF7FE8FAAA1DBBBB7C889B254C,
	ICondition_Check_mEC8DAE0A9AEEAC0944D71A55F212091BC90562C0,
	ICondition__ctor_m54B0A608BC52CDE896560E64BBCF46F9F0ECE592,
	IConditionsList_Check_mE39A03DEA4F39E41F50EF48BEFA52F265AF7CE48,
	IConditionsList__ctor_m1A71FB2E27D04FBA7CAC962B131064288ED8535C,
	ActionGatherComponentsByDistance_InstantExecute_m14B300FCAF41FCA5475A1CCFC5C448DEEA2FFA3D,
	ActionGatherComponentsByDistance_FilterLayerMask_m138AAAC0BB674566AA4E8704384CDDF0C3A173A8,
	ActionGatherComponentsByDistance_GatherColliders_m943FB1722FC15C0822D6A3191A5B06358E42988F,
	ActionGatherComponentsByDistance__ctor_m7CE7A3B5249BD8A4D1650E1B8DF45715A7E6770E,
	U3CU3Ec__DisplayClass4_0__ctor_m411391E28B4793C960820320502C6A52421F3053,
	U3CU3Ec__DisplayClass4_0_U3CInstantExecuteU3Eb__0_mB3DAFA107748DAA0625DB5045184E6443121552F,
	ActionGatherTagsByDistance_InstantExecute_m1C2542F14681DCA3DB04116D7389F986AB13ED8D,
	ActionGatherTagsByDistance_FilterLayerMask_mCE2BDFBE7824AFDB6C2100A57CE62E2D165E7075,
	ActionGatherTagsByDistance_GatherColliders_mCD08191A9BBF8395CE0C824EBE2909281970D554,
	ActionGatherTagsByDistance__ctor_m53A91F4F5F93C3D62EB16AD3012BCB9C8B0C3A25,
	U3CU3Ec__DisplayClass4_0__ctor_mF1FE5E6FB7D1A786A4F7885ACEA5E7A1C869E006,
	U3CU3Ec__DisplayClass4_0_U3CInstantExecuteU3Eb__0_m36D00A6AAD4ED6B9F63A89559123A0984D136F18,
	ActionVariablesAssignBool_ExecuteAssignement_mEFEF3721A44EE9A2FC0EC0A4EBECC1A0EA74B51C,
	ActionVariablesAssignBool__ctor_mCDA429F7084EF49EA9B23355EA5DE6F812AC20FF,
	ActionVariablesAssignColor_ExecuteAssignement_m03849532AB2BB34E0FE2D2F1F413D4B199639B53,
	ActionVariablesAssignColor__ctor_m067DBB1A7F22A49D8C89D208E832982BEBF1CCF8,
	ActionVariablesAssignGameObject_ExecuteAssignement_mF4BA404DF0BCC6FCB932A7F0165A7BE65648D780,
	ActionVariablesAssignGameObject__ctor_mC708D6D1ABD36EE174095B66FAA2E8F06B1CF650,
	ActionVariablesAssignNumber_ExecuteAssignement_mE12F4AE0018D363A8159A67E91B5D67A8563739A,
	ActionVariablesAssignNumber__ctor_m1EE74D940E28D458075CE66095C43C68EAFF8909,
	ActionVariablesAssignSprite_ExecuteAssignement_mC412941A3C3E7BBAE9489CD386614B994B7B6229,
	ActionVariablesAssignSprite__ctor_m48D27ED66E34A34EBAA2FE613790512B28D82C74,
	ActionVariablesAssignString_ExecuteAssignement_m03B46F08F0D3BC4A450425CBE0A4D0846D23CDF3,
	ActionVariablesAssignString__ctor_mE1DC7FD46C4ACA7BCB431385AAB3AC58F14DDB52,
	ActionVariablesAssignTexture2D_ExecuteAssignement_m5E396BBD33A5AB0622224C0EC506B6779445EDF8,
	ActionVariablesAssignTexture2D__ctor_mABBF5AE95DC11B5365B4A8FDD11C3FE01D9ACA93,
	ActionVariablesAssignVector2_ExecuteAssignement_mD6D7D7EC7107A54FF233638D48E55746B7E8BE54,
	ActionVariablesAssignVector2_GetVector2_m524C5FBBC7F780A6A171987A1E9E40FB3B11384B,
	ActionVariablesAssignVector2__ctor_mE64A955BD607F469148E1BE6A093796745A33CFA,
	ActionVariablesAssignVector3_ExecuteAssignement_m1A046D882B8C5430F80ADB64F7100A88FADB9172,
	ActionVariablesAssignVector3__ctor_mE1596456FB260789BB710D20EEEECF507E6DBB6B,
	ActionVariablesToggleBool_InstantExecute_m03499F149F478E438F0719EBC3F08F4A145AD994,
	ActionVariablesToggleBool__ctor_mB33C471C5273D236FE0089FA7860AE581F11F220,
	IActionVariablesAssign_InstantExecute_m1C8F2263386CA352903B7C61BE71832A7235E0C5,
	NULL,
	IActionVariablesAssign__ctor_m07CFFBB2F45FD0DD62788DF9393FF9FF31882922,
	ConditionVariable_Check_mE1E3D474715CF5D198D081CE50ACADAE0CFCB116,
	NULL,
	ConditionVariable__ctor_m597A51459E986E1604484A6734623E4092A5C62E,
	ConditionVariableBool_Compare_m827168145037490EEF9DABEDE9FF0A7C865B3E59,
	ConditionVariableBool__ctor_m9386F6E7EB1F5BB6FFAFF8A34F0D9B5BE101394F,
	ConditionVariableColor_Compare_m55003087C863924A82FF868D0DCF2F193B328CDA,
	ConditionVariableColor__ctor_mEF167027FB33EBE8F69454A6A83F6BEF61A529EA,
	ConditionVariableGameObject_Compare_mE5803F22F6DBF0B0A1D59841F76C08CF3A464360,
	ConditionVariableGameObject__ctor_mE2BB5DF92315C4EE6DD21FDEE391CFB4AE529D2D,
	ConditionVariableNumber_Compare_m05D29243685005AF6E1CF0DBEA3F95A7BB820BD5,
	ConditionVariableNumber__ctor_m00A21C7982CDCF44420050BC26273C923D87253D,
	ConditionVariableSprite_Compare_m4A81D080DD0CE65DA2ADEB31D056BB54B76028D6,
	ConditionVariableSprite__ctor_m3DDC5935970F8F549B97046C0009718B277F772B,
	ConditionVariableString_Compare_mBBBE38B901BB6E90D1F16922C9EA8FB4CE1CD203,
	ConditionVariableString__ctor_m2F23E73D95B4CDF063B564023E5706FB800C78F4,
	ConditionVariableTexture2D_Compare_m8F5A25F0A92B71AF7FFABCDFC9E39DA8FA628FCE,
	ConditionVariableTexture2D__ctor_mB70B180F2D96D31F82B7FA185400B608798D5586,
	ConditionVariableVector2_Compare_m5ADD1BD2B9FD9AA35333BF24657643195AD37BD5,
	ConditionVariableVector2__ctor_mA9EF45C2C6757895F765011D22787E05E3FABF9C,
	ConditionVariableVector3_Compare_mB1432ADBF3874A833C616F80615FAEF4A7703885,
	ConditionVariableVector3__ctor_mAD3A8E380D5A04EFFA302820E6B3A3456FB91D98,
	Parser__ctor_mF54C2866550D736AA4B5C0C1ABB89980801E66D2,
	Parser_Evaluate_m74B4F613F51904E756F6278D27E380F06B9C67F9,
	Parser_ParseExpression_m4521039C01A1E53EEECA6F35AA3B2F0B477EC732,
	Parser_ParseAddSubtract_m386ABBF12533B05051D6C23259F942D63D165995,
	Parser_ParseMultiplyDivide_mBB8C230DC202BD552902C1458E27EB4A1CBFBA37,
	Parser_ParseUnary_mC9B1E0F78ACAEF9332C0C11D93DAE0000FB9F849,
	Parser_ParseLeaf_m4D14FCD70E5E84409F35A49585C0DBD1F80EA517,
	U3CU3Ec__cctor_m4B45CDDCFACD8DE455E30365173F8969DF9A6253,
	U3CU3Ec__ctor_m2A3560CCB93085BC6E780D9EE67E77FDCEF1B120,
	U3CU3Ec_U3CParseAddSubtractU3Eb__4_0_m4324E573756743B448EA5476D8913022E35C5B64,
	U3CU3Ec_U3CParseAddSubtractU3Eb__4_1_m6A372F9E02371AD5411F2C13C6CFB24332A09491,
	U3CU3Ec_U3CParseMultiplyDivideU3Eb__5_0_m989DAA85F48ACB69FEEFF6F868BCFCA753D316AC,
	U3CU3Ec_U3CParseMultiplyDivideU3Eb__5_1_m420FD9C9BAC467CD3ED9228B259258D8394E9FD2,
	U3CU3Ec_U3CParseUnaryU3Eb__6_0_mCAB4B46063F9809AA2AC694BA9514575635C44A3,
	Tokenizer_get_currentCharacter_m0F54B6D1EDB6099B9702A01A5B9834B74EC300A7,
	Tokenizer_set_currentCharacter_m827A7EB986EA3DC7D3A2226918C13B18E732255E,
	Tokenizer_get_currentToken_mCD8FA828E6CC412CF1DFE31196E63C2FEB6B5F00,
	Tokenizer_set_currentToken_m734975EE88A7476FFC961BE9C066FEF54FC7A574,
	Tokenizer_get_number_mE49477FA7BB77D7AA94B3659689F5FB6AF0FD4B1,
	Tokenizer_set_number_mEAE023F2196EB4BA001B1120783D84C0E7F29C47,
	Tokenizer_get_identifier_mCE413649912A31A44093A6BC8B0CC673405ADF79,
	Tokenizer_set_identifier_mB59DC360B4A15FA7675821A2D0F05F6DD3591A0F,
	Tokenizer__ctor_m1C20A051A4A2880827E92CFB843FB76EFE753AA8,
	Tokenizer_NextChar_m3C42276A768974D7D87F071C2B534C123D0BFCE8,
	Tokenizer_NextToken_mE31761CC66BF94455A6E315AD7BEE2B7045F66FB,
	Tokenizer__cctor_m6D4BF70DF9D970B61AF73FFA1FA4BD990D86D380,
	Expression_Evaluate_mD9077AA0B1DB7140C31598F986FF1B6468A091F6,
	NULL,
	Node__ctor_mB5E22B29FE7B6B2F4C5E5112015F34806A234938,
	NodeBinary__ctor_m511F3E2C4CB4033BC461EFE17FDD3E8EE9488B24,
	NodeBinary_Evaluate_m864B908E9B7A0B930592084D29684A99EF3B282B,
	NodeNumber__ctor_m4B09BEC80A8C748A3B3FB0058290F28D367B07C2,
	NodeNumber_Evaluate_mB695978905E919C3ED7AE8558CF16F1B647CA611,
	NodeUnary__ctor_m6ACB3B495FCA2F3D8680D620C01EEE3714ED13B9,
	NodeUnary_Evaluate_m0A90B18E240ACE51E18DAF8384082424BECA0EF9,
	HookCamera__ctor_m6DA0E9DA4DDB68FBCA66292C293062C163246CDA,
	HookPlayer__ctor_mCD010BE22559A4C60A285288F2E495758F6F68CB,
	NULL,
	NULL,
	NULL,
	ActionAdventureCamera_InstantExecute_mF1A187E527FDFC989B102BCDBA217C07996CE512,
	ActionAdventureCamera_Execute_m4B5B9A7099FE47828DCE6EB5948F385DA040162D,
	ActionAdventureCamera__ctor_mF38C7453DC435023D61AD746F91A3A10482033D7,
	U3CExecuteU3Ed__11__ctor_m0200F3F5D3D966175D9581F749E6B5A835BBA109,
	U3CExecuteU3Ed__11_System_IDisposable_Dispose_m00942BBE5812FCCE5A1F9497036D58AE8C9E927E,
	U3CExecuteU3Ed__11_MoveNext_m9CCF1888383DFB261BCB767E9697B27EAF8CE2B2,
	U3CExecuteU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68C82A5E23D0F81B0514376497701F2EE9F32A67,
	U3CExecuteU3Ed__11_System_Collections_IEnumerator_Reset_mDE04DF8E62B2F75FA258B59AB333597FBC2E57A7,
	U3CExecuteU3Ed__11_System_Collections_IEnumerator_get_Current_m421DCFCEB82C325052928E10715A6D19482F238F,
	ActionCameraChange_InstantExecute_mC4705F16B7B58BA5D85A1DE7C02CCA0CBB70C24A,
	ActionCameraChange__ctor_mAF10CC8D5DD950EF4D067A2D52A87F002662ADAD,
	ActionCameraChangeVariable_InstantExecute_mFF2C25D66028DDCBE1DC819E2E412FC2FF359192,
	ActionCameraChangeVariable__ctor_mFCE4C3A5DA844C9DBA1F51351728873F64FC617F,
	ActionCameraCullingMask_InstantExecute_mB014E2DC116E9C9B6D0F56DDC1F98DE34E610EE3,
	ActionCameraCullingMask__ctor_m65CA1618E1800B65CACF48D339EF38F024410A57,
	ActionCameraDamping_InstantExecute_m5BD2C3091137F0113D496DAD7A792864D436B850,
	ActionCameraDamping__ctor_m232385ECA4F4C32E34B64460702D1A2C4000F216,
	ActionCameraRotate_InstantExecute_mB003C76ACD91D2047A77256B1C9FAA59C5B320A2,
	ActionCameraRotate__ctor_mE867F7917BC1F103E7E88C18B4FBB01F6A80D653,
	ActionCameraShakeBurst_InstantExecute_mAEB95E25D61E5213C35A38526998D6A5B68AFEC7,
	ActionCameraShakeBurst__ctor_mC39FB9D77C94D024399249D6F7505B7A4940B4AF,
	ActionCameraShakeSustain_InstantExecute_mB3CC3C0E17EE2F1D4500348F5CDC240FD2A9CD4A,
	ActionCameraShakeSustain__ctor_mF8C37E3829092D83B26BC7927F50B3D6B7E6267D,
	ActionFPSCamera_InstantExecute_m28164C2D02D135DEBA3458A2C9BDFE6C20C75A25,
	ActionFPSCamera__ctor_m4DACC211B31B1ACBDD3DF028B3AD9CF9AE411473,
	ActionFixedCamera_InstantExecute_mCFDC544CEFFE032B016C8CABAA36C106436691E4,
	ActionFixedCamera__ctor_mAF96DBC23A093B3A7DB92284D4AF2B11971F50A4,
	ActionFollowCamera_InstantExecute_mB9D6FB513DF26DF30B5BD6B4A435B996853CDC9E,
	ActionFollowCamera__ctor_m087FB6B90A7C9E00E1EF1A9A4FDEC78607BBD15D,
	ActionRailwayCamera_InstantExecute_mA0A2553E882C22F6F400C7A5C8C5E322CC60C6DC,
	ActionRailwayCamera__ctor_mFF24F0E08EB98F37E8D31DC078615764B0A39700,
	ActionTargetCamera_InstantExecute_m6F880C3A19DA4A2F8A2EE874BB23549CF1FFB086,
	ActionTargetCamera__ctor_mC80798E15934BEC991C1369F07D9DAEAF355021D,
	CameraController_Start_m0A38F6D7B9DFC80289295FC8928A1668C2BF17A5,
	CameraController_LateUpdate_mAB1D81FE39884E5803540B7FD8B020615527303D,
	CameraController_UpdatePosition_mBF7767C6551C7E93DA7A52D3F41411C9C4A78ED1,
	CameraController_UpdateRotation_m63C54FA753CE9E97200417543CD3A8E9EAE98EC8,
	CameraController_UpdateProperties_mC5FDA854B7B306DEBBFD7820BA7C6235E66EE58A,
	CameraController_UpdateShake_m47A07FD5901E9A7061486C24F1B6EA53E5E55CD3,
	CameraController_ChangeCameraMotor_m95276DEAF702F096A3A1D92A63816F5671070C37,
	CameraController_AddShake_m85CE3FA00900CEB694D458676434461B31194DBE,
	CameraController_SetSustainShake_m07CF5F5A74E12C81B2E1D5395EAB86B088A70CF4,
	CameraController_StopSustainShake_m1CCCA5CE2FF926EB76D8B83718D44F3CB8C306E8,
	CameraController__ctor_m9AB4CFB9C9A5EF4C4FA6B3DEA4E91FD3533E1D16,
	CameraSmoothTime__ctor_m562A5EC7FC27D368617AA6065AEF0D4A6F84FDCD,
	CameraSmoothTime_GetNextPosition_m61A77096EB509E4D1EDCD34EC48F215B94AA18E5,
	CameraSmoothTime_GetNextDirection_m20783D2D5615B1A7D9E30CF1E6CCEE53D44ECCB3,
	CameraMotor_Awake_m773F69BB952FB72BD2F40B85FD723CC4B15E00B4,
	CameraMotor_OnDrawGizmos_m9FC51CE7EA69EE24229379A3445F262B044AE0AA,
	CameraMotor__ctor_m341C15C9C92B9BB2A99978DC45C75713C50D2B74,
	CameraShake__ctor_mE77C9CE50699CB72CD32511507BC47FDAB503673,
	CameraShake__ctor_mEE666C60F97DCF3D20A1692A5B14B42063C633FA,
	CameraShake_Initialize_m1690CA6AB9195E46FF41F83030D199D5E0F553D1,
	CameraShake_Update_mB3946F931C04E2B475429DF37B9A6B5426D7B14B,
	CameraShake_GetEasing_m6B6A5FBC9BF96BF8297DBA78320C83722377786C,
	CameraShake_IsComplete_m66DC8917BB0C9AB79800D781803C313203802419,
	CameraShake_GetInfluencePosition_m81620CA44CE9C2E8A91C2C3FB804670866C40D54,
	CameraShake_GetInfluenceRotation_m86583AB84FA703C49EF73A56AB3243AE48B213FA,
	CameraShake_GetNormalizedProgress_mA914D4BE1A36A5C174717FC407AAEC476264EFAA,
	CameraShake__cctor_mAD3C5C8BD70AC587F952E4933575AC31D517FAD6,
	CameraMotorTypeAdventure_Awake_mA66DE8181AE89DBDC233A52A80E4946275AB528F,
	CameraMotorTypeAdventure_EnableMotor_m809B1F469E5006BD0825649E58109166B73F35F7,
	CameraMotorTypeAdventure_DisableMotor_m685DB327BC7E7CFC5BA2945EBEEDC8862E8F3842,
	CameraMotorTypeAdventure_UpdateMotor_mE6CFB125392462C91205FCF731EAA41C6BC405F9,
	CameraMotorTypeAdventure_GetPosition_mFA7F72CC08406E15E54940DC26D632CCCCD1D36F,
	CameraMotorTypeAdventure_GetDirection_mE7B8777981062795F72E4A014C0C2BFC2E7DF758,
	CameraMotorTypeAdventure_UseSmoothPosition_m2CB223E5A08CDF87FA4C34E45DE2FD92CE1E1374,
	CameraMotorTypeAdventure_UseSmoothRotation_m9132F9E2F55FCFE67A09985A306FD76A97A4F876,
	CameraMotorTypeAdventure_AddRotation_m5BC7E64E1592CBC5F9C67CAA6B6FDC6154E8E91A,
	CameraMotorTypeAdventure_FixedUpdate_mD86CD67911AD18F0FDBFC36E42F59EA822A7A049,
	CameraMotorTypeAdventure_RotationInput_m21E9F252AD43F8057A58317ACE3BC5E0AF2832BB,
	CameraMotorTypeAdventure_RotationRecover_m8DBFDF3A448806F90D336986D7AF4C525DA8D818,
	CameraMotorTypeAdventure__ctor_m7A76CCBD66630B322BC508D0520A8E4D160FA884,
	CameraMotorTypeAdventure__cctor_mD8C1B4B595EA99398F9C896DDD7E690D15581582,
	CameraMotorTypeFirstPerson_EnableMotor_mBAF0B3217A76E748B15793FD79DEC4A3F7B54345,
	CameraMotorTypeFirstPerson_DisableMotor_m2C55A8AEE63CA20E3C8275CC9FEEB3D6A8C06373,
	CameraMotorTypeFirstPerson_GetPosition_m9744F54BB30CD25D89AEF7A72980B8385244D65D,
	CameraMotorTypeFirstPerson_GetDirection_m0051B547AF9DD5D96199219F3FA447D18CC7C0C5,
	CameraMotorTypeFirstPerson_UseSmoothRotation_m7A36578B50E56D2DCB1A5BBABF2C20ECD5EDD940,
	CameraMotorTypeFirstPerson_UseSmoothPosition_m4B1EA93F9A6B8F551066E1380034DB711A9C3335,
	CameraMotorTypeFirstPerson_AddRotation_mC9D1107180F62249FF696C5789F79F3D1435358F,
	CameraMotorTypeFirstPerson_GetTarget_m2C836AEDAB84E73B78F14614FA151683D011B5EA,
	CameraMotorTypeFirstPerson_ClampAngle_m4FD718505DAEEC6FBA6DF555CB88A140032087A9,
	CameraMotorTypeFirstPerson_Headbob_m5839B25479F234D6D78D5F792650C8953181083A,
	CameraMotorTypeFirstPerson_RotationInput_m353F56C911AC6B059ECEC68897F4B01DAA012FCF,
	CameraMotorTypeFirstPerson__ctor_mF970958EFC869EC8DF14A972DA5C5290F02D7C5B,
	CameraMotorTypeFirstPerson__cctor_m9EDE12F0C0200C3939DA862B2F5226E1315F290C,
	CameraMotorTypeFixed_GetDirection_mEC2629174EA9858602727F1E0643DFEEB2A4465C,
	CameraMotorTypeFixed__ctor_mC23AE80806123CAF151A45D2D9E6717287875732,
	CameraMotorTypeFixed__cctor_mA220BC9EA2F3A18BDABC5E989A58BF45611EFF55,
	CameraMotorTypeFollow_UpdateMotor_m1DFCDC71832CD8A30F600A787A4FF390CED5B6E6,
	CameraMotorTypeFollow_GetPosition_m07529476A93039982AD9822DC1B6E945957A31DE,
	CameraMotorTypeFollow_GetDirection_m833F650CDC16901B3E697E243ADEDA04E88B2E90,
	CameraMotorTypeFollow__ctor_mAC662C7C8E6A8AD3E73C48DFDB6811F1D1380174,
	CameraMotorTypeFollow__cctor_mF1175A60D34BC705C65E22275088B3949D9ECA60,
	CameraMotorTypeRailway_Awake_m418E098D2ECB65777E2DB2E3296EBF784706EAC9,
	CameraMotorTypeRailway_UpdateMotor_m92932F5E01CED9FEC1AD1528DA254D06E1D54A39,
	CameraMotorTypeRailway_GetPosition_m4A59F6EC151260B73A3B10229E5760024D6F7F83,
	CameraMotorTypeRailway_GetDirection_m4854DB06E6FF9E9189F57EB1363CC8392BA5898A,
	CameraMotorTypeRailway__ctor_m09F630D2333F40ED4735EDF491DD7E1B0E877E9C,
	CameraMotorTypeRailway__cctor_m5DE3EA3A18C85114293BFD91461B4CBD8E20C52E,
	CameraMotorTypeTarget_UpdateMotor_m46496547F98179BAF6F3430824B2345172B31DA2,
	CameraMotorTypeTarget_GetPosition_m14E5E777F9291524296E3C17612420546AFE5312,
	CameraMotorTypeTarget_GetDirection_m8A63FA7D45805E3A2FDB4DB9DC89B0E1876C837A,
	CameraMotorTypeTarget__ctor_m765132F98BC20F21EF988E77DD0A4DFE12414FE4,
	CameraMotorTypeTarget__cctor_m316C2A562718E30F9C41D0153FDF413F820CE05B,
	CameraMotorTypeTween_Awake_mEBFA729B93FADCAFE1E7663B3F49F72F900FF866,
	CameraMotorTypeTween_EnableMotor_m195390B029589D57FA431FD12E8708F41E06D1B2,
	CameraMotorTypeTween_UpdateMotor_m1500B97DCCD33595C9D25140B17593A7CF1F2D68,
	CameraMotorTypeTween_GetPosition_mF7699FC7B8FEB017845771A92D8F996B6526221B,
	CameraMotorTypeTween_GetDirection_m4BD18C534F25D00DB0AD02DCE614DC312C62E89B,
	CameraMotorTypeTween__ctor_m08751A9B5E789A2ED878C5AF8649FCFADD8E9F1E,
	CameraMotorTypeTween__cctor_m88CC3CEC454270E89771046F7E3504523B391E58,
	ICameraMotorType_Initialize_m64FA374ECB42165D57957C34F38CBF3F6A52EACC,
	ICameraMotorType_Update_m13B9BD65B115493682EC77A42CCC072F962F2DBB,
	ICameraMotorType_EnableMotor_mEA7863B477E3AA5B4573C527741D86062787C5C2,
	ICameraMotorType_DisableMotor_m3CB2A76C959467D5945831DDF7A7DE35C9561EB6,
	ICameraMotorType_UpdateMotor_mB95865F50C0A3F900C9047361AD3133EBAFD1661,
	ICameraMotorType_GetPosition_m490516BAC627DF422BA16D8E41D688D4A3DFB5B3,
	ICameraMotorType_GetDirection_mC5D9E0835B9FDB2F858CB021E2B532916421E280,
	ICameraMotorType_UseSmoothPosition_m89982E1CF36D801FBC65769854B911C4E8639BCE,
	ICameraMotorType_UseSmoothRotation_m0838436342552B19469206176B2AE9178859F01C,
	ICameraMotorType__ctor_mFA88B9EF227DBA8CB9284A09A60921303143537F,
	ICameraMotorType__cctor_mDC3BA5A1A3A323A2F4A8737A83B92C9BB9C5FD38,
};
extern void BoxData__ctor_m4379031EB9F1E1D541B8DDB4C0B835D72497F739_AdjustorThunk (void);
extern void Parameter__ctor_mF1BAEB0C446EF259DA381E3C86A2246909C7BE05_AdjustorThunk (void);
extern void Parameter__ctor_mF986F68A136BDB460D180D5CF86785FB563A38CF_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x060001B6, BoxData__ctor_m4379031EB9F1E1D541B8DDB4C0B835D72497F739_AdjustorThunk },
	{ 0x060002F9, Parameter__ctor_mF1BAEB0C446EF259DA381E3C86A2246909C7BE05_AdjustorThunk },
	{ 0x060002FA, Parameter__ctor_mF986F68A136BDB460D180D5CF86785FB563A38CF_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2356] = 
{
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	1551,
	492,
	1551,
	1301,
	1057,
	850,
	1057,
	994,
	1551,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1551,
	1319,
	1551,
	1270,
	1551,
	1301,
	1512,
	1057,
	850,
	1512,
	1057,
	994,
	1551,
	1057,
	850,
	1057,
	1512,
	1057,
	994,
	1551,
	1057,
	850,
	1057,
	1512,
	1057,
	1551,
	1057,
	994,
	1512,
	1551,
	1551,
	1321,
	994,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1512,
	1301,
	1512,
	1512,
	1512,
	1551,
	1290,
	1290,
	1551,
	1057,
	850,
	1512,
	1500,
	1512,
	1512,
	1057,
	1057,
	994,
	1512,
	1057,
	1551,
	1332,
	1551,
	1334,
	1551,
	1500,
	1290,
	1551,
	1054,
	634,
	847,
	541,
	1290,
	781,
	1290,
	1551,
	1551,
	1319,
	1512,
	1512,
	1551,
	1301,
	1551,
	1057,
	1551,
	1319,
	1551,
	1319,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	2635,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	523,
	1551,
	1551,
	1551,
	1551,
	1159,
	1551,
	1159,
	1551,
	1512,
	2619,
	1551,
	1551,
	1551,
	1551,
	2635,
	1551,
	1057,
	1512,
	1319,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	2635,
	2506,
	2055,
	2216,
	2019,
	2047,
	2047,
	2470,
	2470,
	2387,
	1971,
	2142,
	2142,
	2142,
	2142,
	2384,
	2384,
	2384,
	2384,
	2590,
	2590,
	2547,
	2088,
	2635,
	1551,
	1301,
	1482,
	1551,
	1551,
	847,
	1482,
	2635,
	1551,
	1301,
	356,
	1512,
	-1,
	1536,
	2544,
	791,
	1301,
	1536,
	2544,
	2613,
	1551,
	1551,
	1319,
	1536,
	2613,
	1551,
	1270,
	1536,
	2613,
	-1,
	-1,
	-1,
	-1,
	1551,
	1321,
	1536,
	2613,
	1551,
	1301,
	1536,
	2613,
	1551,
	1301,
	1536,
	2613,
	1551,
	1301,
	1536,
	2613,
	1551,
	1301,
	1536,
	2613,
	1551,
	1332,
	1536,
	2613,
	1551,
	1334,
	1536,
	2613,
	2506,
	2055,
	2055,
	2248,
	2635,
	1551,
	1301,
	850,
	355,
	543,
	543,
	850,
	550,
	850,
	850,
	850,
	850,
	850,
	550,
	850,
	850,
	850,
	850,
	643,
	1057,
	1551,
	1551,
	444,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	2635,
	1795,
	1880,
	2635,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1536,
	842,
	1551,
	1512,
	1551,
	2635,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	1539,
	1301,
	1551,
	881,
	881,
	1059,
	1551,
	2635,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1057,
	1512,
	1551,
	1057,
	454,
	1551,
	1551,
	850,
	1500,
	1512,
	1054,
	1551,
	1551,
	1551,
	1551,
	1539,
	1539,
	1512,
	2635,
	1321,
	1290,
	1536,
	1500,
	1551,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	545,
	1551,
	1551,
	1321,
	884,
	1551,
	1551,
	1536,
	1536,
	1551,
	1551,
	527,
	157,
	1551,
	1551,
	1551,
	1551,
	580,
	1512,
	1301,
	1301,
	1551,
	1551,
	1551,
	1512,
	1512,
	1512,
	1512,
	1549,
	1551,
	1551,
	2635,
	882,
	1551,
	1539,
	1321,
	1539,
	1321,
	1536,
	1319,
	1536,
	1319,
	1536,
	1319,
	1536,
	1319,
	1536,
	1319,
	1536,
	1536,
	1536,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1512,
	1301,
	1512,
	1301,
	1512,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1512,
	643,
	1301,
	1551,
	1551,
	1290,
	1551,
	1500,
	1301,
	805,
	1321,
	1321,
	1321,
	1321,
	1321,
	1321,
	1301,
	1551,
	1551,
	1551,
	415,
	578,
	1536,
	714,
	501,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1301,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1551,
	1301,
	1551,
	549,
	1551,
	2619,
	1551,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1551,
	1057,
	1057,
	1301,
	1551,
	1301,
	1551,
	1512,
	1551,
	1551,
	1512,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	650,
	1551,
	1551,
	1551,
	1301,
	1551,
	650,
	1551,
	1551,
	1551,
	1301,
	1551,
	-1,
	-1,
	-1,
	-1,
	650,
	1551,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1512,
	1057,
	994,
	1551,
	1500,
	1290,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	642,
	1500,
	2619,
	1551,
	1551,
	1290,
	1551,
	492,
	1551,
	492,
	1551,
	2635,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	444,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	444,
	1551,
	1551,
	178,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	265,
	1551,
	2635,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1551,
	1551,
	1290,
	1551,
	850,
	1551,
	1301,
	1551,
	185,
	185,
	124,
	1321,
	123,
	69,
	43,
	872,
	805,
	1054,
	1551,
	1551,
	1551,
	1551,
	406,
	1515,
	1515,
	1515,
	1515,
	192,
	1551,
	1512,
	1321,
	1536,
	-1,
	-1,
	-1,
	1321,
	-1,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	381,
	1536,
	1321,
	1321,
	805,
	855,
	185,
	-1,
	1636,
	185,
	-1,
	1615,
	1500,
	1290,
	1512,
	1301,
	1512,
	1301,
	175,
	1536,
	1321,
	1321,
	1321,
	1551,
	63,
	-1,
	1596,
	1596,
	1551,
	117,
	-1,
	1601,
	1601,
	175,
	-1,
	1598,
	1598,
	1321,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1512,
	867,
	1551,
	1536,
	1536,
	1500,
	1536,
	1512,
	325,
	128,
	1321,
	1551,
	648,
	1512,
	1301,
	1551,
	1512,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	1551,
	1551,
	1536,
	1319,
	1549,
	1334,
	1522,
	1309,
	882,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1551,
	1551,
	1551,
	1301,
	1551,
	1512,
	1290,
	1551,
	1512,
	190,
	190,
	126,
	1321,
	69,
	43,
	123,
	872,
	805,
	1301,
	1512,
	1301,
	1512,
	1054,
	1301,
	1309,
	1321,
	1321,
	1321,
	1522,
	1522,
	1319,
	1319,
	1319,
	1319,
	1319,
	1321,
	1551,
	1551,
	1551,
	1551,
	1551,
	2635,
	718,
	1321,
	1551,
	1551,
	1301,
	1321,
	1321,
	1301,
	1301,
	1301,
	637,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1522,
	1522,
	1522,
	1321,
	1321,
	1321,
	1309,
	1551,
	1539,
	1321,
	1539,
	1321,
	1321,
	1539,
	1321,
	1512,
	1301,
	1301,
	169,
	1054,
	1159,
	1290,
	1301,
	636,
	686,
	1551,
	1551,
	1551,
	850,
	1536,
	1319,
	1301,
	1551,
	1551,
	1290,
	1301,
	1301,
	1551,
	1549,
	1551,
	2635,
	541,
	1215,
	1536,
	1319,
	1301,
	1290,
	1301,
	1551,
	524,
	805,
	1057,
	1551,
	847,
	855,
	553,
	1321,
	1536,
	1319,
	1551,
	1290,
	1551,
	883,
	855,
	1321,
	1321,
	1321,
	1551,
	1551,
	1301,
	814,
	1321,
	1549,
	1536,
	1551,
	1334,
	1536,
	1549,
	1301,
	1536,
	1549,
	1551,
	1551,
	1536,
	1319,
	1500,
	1290,
	1512,
	1301,
	1301,
	1551,
	396,
	128,
	1500,
	1007,
	882,
	1334,
	1290,
	1321,
	1319,
	1549,
	1549,
	881,
	883,
	195,
	395,
	561,
	850,
	1334,
	1319,
	1321,
	1551,
	-1,
	1319,
	1290,
	1551,
	1319,
	1536,
	1319,
	1536,
	1301,
	396,
	128,
	1551,
	1500,
	1551,
	1074,
	719,
	728,
	1551,
	1334,
	1512,
	1551,
	2635,
	868,
	791,
	1500,
	1551,
	881,
	1551,
	1500,
	1551,
	1551,
	561,
	1551,
	1500,
	1551,
	881,
	1551,
	1500,
	1551,
	883,
	1551,
	1500,
	1551,
	1551,
	1321,
	1551,
	1551,
	1551,
	195,
	395,
	850,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1334,
	1512,
	1551,
	1334,
	1334,
	1512,
	1551,
	2635,
	1551,
	2619,
	2619,
	1551,
	2635,
	1301,
	1319,
	867,
	1500,
	1551,
	1551,
	1551,
	1551,
	1551,
	1549,
	1536,
	11,
	1536,
	1301,
	1301,
	854,
	1536,
	1536,
	1536,
	1536,
	1536,
	1551,
	781,
	2635,
	1551,
	1301,
	1301,
	1551,
	2179,
	2164,
	2578,
	2474,
	2474,
	2418,
	1703,
	1551,
	1512,
	1301,
	1301,
	1551,
	492,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	855,
	1551,
	545,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1159,
	1551,
	1500,
	1551,
	1159,
	1551,
	1159,
	1551,
	492,
	1159,
	1500,
	1057,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	444,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	444,
	1551,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	444,
	1551,
	1551,
	1536,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	847,
	1551,
	380,
	1321,
	1512,
	1334,
	1321,
	1321,
	1551,
	1054,
	635,
	1054,
	442,
	1551,
	380,
	855,
	1321,
	380,
	855,
	73,
	855,
	855,
	1321,
	380,
	855,
	1321,
	1321,
	805,
	1321,
	1321,
	1321,
	1321,
	1214,
	1539,
	1539,
	1539,
	1539,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	2635,
	394,
	1536,
	1551,
	1159,
	1551,
	1536,
	1551,
	1536,
	1551,
	1536,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1301,
	642,
	1551,
	1551,
	1551,
	1301,
	1301,
	1159,
	1551,
	854,
	1551,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1301,
	1551,
	1551,
	1551,
	1551,
	1301,
	850,
	1551,
	1159,
	1551,
	2635,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1301,
	1301,
	1551,
	1551,
	1551,
	1301,
	1301,
	1301,
	1551,
	1551,
	1551,
	1301,
	1301,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1290,
	1551,
	1551,
	1321,
	1551,
	1551,
	1551,
	1290,
	1512,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1290,
	1551,
	1301,
	1551,
	1551,
	1301,
	1301,
	1551,
	1301,
	1551,
	1301,
	1301,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1551,
	1301,
	1551,
	1301,
	1301,
	1301,
	1551,
	1551,
	1301,
	523,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1301,
	1301,
	1301,
	1547,
	1332,
	1551,
	1551,
	869,
	1551,
	1319,
	1233,
	1319,
	1551,
	2635,
	1551,
	1551,
	1551,
	850,
	1301,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	2635,
	1551,
	1551,
	850,
	1057,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1147,
	850,
	1551,
	1551,
	1551,
	2635,
	1301,
	1301,
	1551,
	1551,
	791,
	1290,
	1054,
	1054,
	1551,
	1551,
	1301,
	1301,
	1290,
	1500,
	1500,
	1054,
	1551,
	1551,
	1301,
	847,
	1301,
	1512,
	1512,
	1512,
	1057,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1551,
	1290,
	1551,
	715,
	601,
	643,
	855,
	847,
	850,
	1551,
	1301,
	1159,
	1551,
	643,
	850,
	1551,
	1301,
	1159,
	715,
	601,
	855,
	847,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1512,
	1512,
	1512,
	1301,
	1551,
	1551,
	1551,
	1512,
	1512,
	1512,
	1301,
	1551,
	1551,
	1551,
	1551,
	1512,
	1512,
	1512,
	1301,
	1551,
	1551,
	1551,
	2627,
	2593,
	2627,
	2593,
	2613,
	2586,
	2613,
	2586,
	1512,
	1301,
	2635,
	1551,
	869,
	1320,
	1290,
	1500,
	1290,
	544,
	1290,
	791,
	635,
	1301,
	1500,
	1054,
	1301,
	847,
	635,
	1551,
	847,
	1551,
	1301,
	2635,
	1551,
	602,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1512,
	1512,
	1512,
	1551,
	1301,
	1551,
	1301,
	1301,
	1512,
	1551,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1551,
	1551,
	1301,
	1551,
	1551,
	1551,
	1551,
	523,
	1301,
	1321,
	1551,
	1551,
	1551,
	1551,
	523,
	1301,
	1321,
	1551,
	1551,
	1551,
	1551,
	1301,
	523,
	1319,
	1551,
	1551,
	1512,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1512,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1290,
	1057,
	1301,
	1301,
	1301,
	523,
	1512,
	1551,
	1551,
	1290,
	722,
	1512,
	1551,
	1290,
	1057,
	1057,
	-1,
	643,
	-1,
	-1,
	1301,
	1301,
	1301,
	523,
	1512,
	1551,
	1551,
	1290,
	722,
	1073,
	1512,
	1551,
	1551,
	1941,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	2101,
	1774,
	1774,
	1774,
	1774,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	2332,
	1551,
	850,
	850,
	850,
	1512,
	1264,
	1551,
	1551,
	1551,
	1551,
	1054,
	1147,
	869,
	1551,
	1551,
	1551,
	1054,
	1512,
	1551,
	2562,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	872,
	574,
	1551,
	1551,
	1551,
	576,
	1539,
	2619,
	1512,
	1301,
	1551,
	-1,
	-1,
	2248,
	1551,
	2619,
	1551,
	685,
	850,
	1551,
	492,
	314,
	444,
	265,
	1551,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	550,
	445,
	1551,
	1551,
	1551,
	1512,
	1301,
	1512,
	1301,
	1301,
	1512,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	1536,
	1159,
	685,
	1551,
	685,
	1551,
	492,
	1500,
	1057,
	1551,
	1551,
	602,
	492,
	1500,
	1057,
	1551,
	1551,
	602,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1551,
	1301,
	1235,
	1551,
	1301,
	1551,
	492,
	1551,
	492,
	1301,
	1551,
	1159,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1159,
	1551,
	1301,
	1539,
	1512,
	1512,
	1512,
	1512,
	1512,
	2635,
	1551,
	718,
	718,
	718,
	718,
	1216,
	1499,
	1289,
	1500,
	1290,
	1539,
	1321,
	1512,
	1301,
	1301,
	1551,
	1551,
	2635,
	2562,
	1539,
	1551,
	550,
	1539,
	1321,
	1539,
	850,
	1539,
	1551,
	1551,
	-1,
	-1,
	-1,
	492,
	444,
	1551,
	1290,
	1551,
	1536,
	1512,
	1551,
	1512,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	492,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	1551,
	855,
	1301,
	1301,
	1321,
	1551,
	1551,
	726,
	726,
	1551,
	1551,
	1551,
	75,
	873,
	1551,
	1237,
	1539,
	1536,
	1549,
	1549,
	1539,
	2635,
	1551,
	1551,
	1551,
	1551,
	723,
	723,
	1536,
	1536,
	576,
	1551,
	728,
	1551,
	1551,
	2635,
	1551,
	1551,
	723,
	723,
	1536,
	1536,
	576,
	1512,
	2101,
	1549,
	1551,
	1551,
	2635,
	723,
	1551,
	2635,
	1551,
	723,
	723,
	1551,
	2635,
	1551,
	1551,
	723,
	723,
	1551,
	2635,
	1551,
	723,
	723,
	1551,
	2635,
	1551,
	1551,
	1551,
	723,
	723,
	1551,
	2635,
	1301,
	1551,
	1551,
	1551,
	1551,
	723,
	723,
	1536,
	1536,
	1551,
	2635,
};
static const Il2CppTokenRangePair s_rgctxIndices[27] = 
{
	{ 0x0200001A, { 0, 4 } },
	{ 0x02000074, { 5, 17 } },
	{ 0x02000093, { 22, 14 } },
	{ 0x0200009E, { 36, 2 } },
	{ 0x020001B2, { 55, 4 } },
	{ 0x020001B3, { 62, 1 } },
	{ 0x0200023F, { 67, 21 } },
	{ 0x02000240, { 88, 8 } },
	{ 0x02000277, { 102, 1 } },
	{ 0x060000D8, { 4, 1 } },
	{ 0x060002EA, { 38, 3 } },
	{ 0x060002EB, { 41, 1 } },
	{ 0x060002EC, { 42, 1 } },
	{ 0x060002EE, { 43, 4 } },
	{ 0x060002FC, { 47, 1 } },
	{ 0x060002FF, { 48, 1 } },
	{ 0x0600030E, { 49, 1 } },
	{ 0x06000313, { 50, 1 } },
	{ 0x06000317, { 51, 1 } },
	{ 0x060003FA, { 52, 3 } },
	{ 0x060005F0, { 59, 3 } },
	{ 0x06000797, { 63, 1 } },
	{ 0x06000799, { 64, 1 } },
	{ 0x0600079A, { 65, 2 } },
	{ 0x0600081F, { 96, 2 } },
	{ 0x06000820, { 98, 4 } },
	{ 0x060008B7, { 103, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[106] = 
{
	{ (Il2CppRGCTXDataType)3, 442 },
	{ (Il2CppRGCTXDataType)3, 440 },
	{ (Il2CppRGCTXDataType)2, 231 },
	{ (Il2CppRGCTXDataType)3, 441 },
	{ (Il2CppRGCTXDataType)2, 199 },
	{ (Il2CppRGCTXDataType)2, 617 },
	{ (Il2CppRGCTXDataType)3, 894 },
	{ (Il2CppRGCTXDataType)2, 2154 },
	{ (Il2CppRGCTXDataType)3, 6897 },
	{ (Il2CppRGCTXDataType)3, 898 },
	{ (Il2CppRGCTXDataType)3, 896 },
	{ (Il2CppRGCTXDataType)3, 897 },
	{ (Il2CppRGCTXDataType)3, 895 },
	{ (Il2CppRGCTXDataType)3, 6899 },
	{ (Il2CppRGCTXDataType)3, 6898 },
	{ (Il2CppRGCTXDataType)2, 1702 },
	{ (Il2CppRGCTXDataType)2, 329 },
	{ (Il2CppRGCTXDataType)3, 6900 },
	{ (Il2CppRGCTXDataType)2, 1078 },
	{ (Il2CppRGCTXDataType)3, 899 },
	{ (Il2CppRGCTXDataType)3, 7206 },
	{ (Il2CppRGCTXDataType)2, 752 },
	{ (Il2CppRGCTXDataType)2, 664 },
	{ (Il2CppRGCTXDataType)3, 1282 },
	{ (Il2CppRGCTXDataType)3, 1286 },
	{ (Il2CppRGCTXDataType)3, 1284 },
	{ (Il2CppRGCTXDataType)3, 1285 },
	{ (Il2CppRGCTXDataType)3, 1283 },
	{ (Il2CppRGCTXDataType)3, 6934 },
	{ (Il2CppRGCTXDataType)3, 6933 },
	{ (Il2CppRGCTXDataType)2, 1707 },
	{ (Il2CppRGCTXDataType)3, 6935 },
	{ (Il2CppRGCTXDataType)2, 1079 },
	{ (Il2CppRGCTXDataType)3, 1287 },
	{ (Il2CppRGCTXDataType)3, 7305 },
	{ (Il2CppRGCTXDataType)2, 755 },
	{ (Il2CppRGCTXDataType)2, 267 },
	{ (Il2CppRGCTXDataType)3, 3226 },
	{ (Il2CppRGCTXDataType)3, 8913 },
	{ (Il2CppRGCTXDataType)3, 8898 },
	{ (Il2CppRGCTXDataType)3, 8875 },
	{ (Il2CppRGCTXDataType)3, 8878 },
	{ (Il2CppRGCTXDataType)3, 8879 },
	{ (Il2CppRGCTXDataType)3, 8948 },
	{ (Il2CppRGCTXDataType)3, 8886 },
	{ (Il2CppRGCTXDataType)3, 8887 },
	{ (Il2CppRGCTXDataType)3, 8885 },
	{ (Il2CppRGCTXDataType)3, 8868 },
	{ (Il2CppRGCTXDataType)3, 8869 },
	{ (Il2CppRGCTXDataType)3, 8870 },
	{ (Il2CppRGCTXDataType)3, 8871 },
	{ (Il2CppRGCTXDataType)3, 8872 },
	{ (Il2CppRGCTXDataType)1, 56 },
	{ (Il2CppRGCTXDataType)3, 7363 },
	{ (Il2CppRGCTXDataType)2, 56 },
	{ (Il2CppRGCTXDataType)3, 3240 },
	{ (Il2CppRGCTXDataType)3, 3241 },
	{ (Il2CppRGCTXDataType)2, 269 },
	{ (Il2CppRGCTXDataType)3, 8671 },
	{ (Il2CppRGCTXDataType)1, 103 },
	{ (Il2CppRGCTXDataType)3, 8634 },
	{ (Il2CppRGCTXDataType)2, 103 },
	{ (Il2CppRGCTXDataType)3, 8672 },
	{ (Il2CppRGCTXDataType)3, 8670 },
	{ (Il2CppRGCTXDataType)3, 8706 },
	{ (Il2CppRGCTXDataType)2, 2101 },
	{ (Il2CppRGCTXDataType)3, 8730 },
	{ (Il2CppRGCTXDataType)3, 889 },
	{ (Il2CppRGCTXDataType)3, 883 },
	{ (Il2CppRGCTXDataType)3, 885 },
	{ (Il2CppRGCTXDataType)3, 891 },
	{ (Il2CppRGCTXDataType)3, 887 },
	{ (Il2CppRGCTXDataType)3, 888 },
	{ (Il2CppRGCTXDataType)3, 892 },
	{ (Il2CppRGCTXDataType)3, 890 },
	{ (Il2CppRGCTXDataType)3, 893 },
	{ (Il2CppRGCTXDataType)3, 884 },
	{ (Il2CppRGCTXDataType)2, 914 },
	{ (Il2CppRGCTXDataType)3, 886 },
	{ (Il2CppRGCTXDataType)2, 751 },
	{ (Il2CppRGCTXDataType)2, 2131 },
	{ (Il2CppRGCTXDataType)2, 2152 },
	{ (Il2CppRGCTXDataType)3, 2359 },
	{ (Il2CppRGCTXDataType)3, 4148 },
	{ (Il2CppRGCTXDataType)3, 4149 },
	{ (Il2CppRGCTXDataType)3, 2358 },
	{ (Il2CppRGCTXDataType)2, 615 },
	{ (Il2CppRGCTXDataType)3, 882 },
	{ (Il2CppRGCTXDataType)2, 1640 },
	{ (Il2CppRGCTXDataType)2, 317 },
	{ (Il2CppRGCTXDataType)1, 317 },
	{ (Il2CppRGCTXDataType)3, 6663 },
	{ (Il2CppRGCTXDataType)3, 8635 },
	{ (Il2CppRGCTXDataType)3, 8525 },
	{ (Il2CppRGCTXDataType)3, 6664 },
	{ (Il2CppRGCTXDataType)3, 6665 },
	{ (Il2CppRGCTXDataType)3, 8748 },
	{ (Il2CppRGCTXDataType)3, 8851 },
	{ (Il2CppRGCTXDataType)1, 101 },
	{ (Il2CppRGCTXDataType)3, 9053 },
	{ (Il2CppRGCTXDataType)2, 101 },
	{ (Il2CppRGCTXDataType)3, 9064 },
	{ (Il2CppRGCTXDataType)2, 1186 },
	{ (Il2CppRGCTXDataType)1, 104 },
	{ (Il2CppRGCTXDataType)3, 8669 },
	{ (Il2CppRGCTXDataType)2, 104 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	2356,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	27,
	s_rgctxIndices,
	106,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
