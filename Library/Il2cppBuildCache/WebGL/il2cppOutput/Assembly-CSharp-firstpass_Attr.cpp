﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// GameCreator.Variables.Variable/DataType[]
struct DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// GameCreator.Core.EventNameAttribute
struct EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// GameCreator.Core.IndentAttribute
struct IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// GameCreator.Core.LocStringNoPostProcessAttribute
struct LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992;
// GameCreator.Core.LocStringNoTextAttribute
struct LocStringNoTextAttribute_tFAA187D3CCF0D9D70EA928FA8713ADBB5BF5F1E9;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// GameCreator.Core.RotationConstraintAttribute
struct RotationConstraintAttribute_t4B4100789F0B127714CA267AAA8FB12D58524903;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// GameCreator.Core.TagSelectorAttribute
struct TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780;
// UnityEngine.Timeline.TrackClipTypeAttribute
struct TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59;
// UnityEngine.Timeline.TrackColorAttribute
struct TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1;
// System.Type
struct Type_t;
// GameCreator.Variables.VariableFilterAttribute
struct VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* ActionsAsset_tED7A2BE02E6133961A58377E6BC7C084F0470956_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ConditionsAsset_t0C09F6E62B15BDBCE2333B79706C7D7764A82A0A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* HookCamera_t44CFF2D561623323C70F70ED3EB953812CFFE47C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TriggerAsset_tF8F0065F50595732137DCE4EDEDEAA712D8592A8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_0_0_0_var;

struct DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.Timeline.TrackClipTypeAttribute
struct TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.Timeline.TrackClipTypeAttribute::inspectedType
	Type_t * ___inspectedType_0;
	// System.Boolean UnityEngine.Timeline.TrackClipTypeAttribute::allowAutoCreate
	bool ___allowAutoCreate_1;

public:
	inline static int32_t get_offset_of_inspectedType_0() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59, ___inspectedType_0)); }
	inline Type_t * get_inspectedType_0() const { return ___inspectedType_0; }
	inline Type_t ** get_address_of_inspectedType_0() { return &___inspectedType_0; }
	inline void set_inspectedType_0(Type_t * value)
	{
		___inspectedType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inspectedType_0), (void*)value);
	}

	inline static int32_t get_offset_of_allowAutoCreate_1() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59, ___allowAutoCreate_1)); }
	inline bool get_allowAutoCreate_1() const { return ___allowAutoCreate_1; }
	inline bool* get_address_of_allowAutoCreate_1() { return &___allowAutoCreate_1; }
	inline void set_allowAutoCreate_1(bool value)
	{
		___allowAutoCreate_1 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GameCreator.Core.EventNameAttribute
struct EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// GameCreator.Core.IndentAttribute
struct IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// GameCreator.Core.LocStringNoPostProcessAttribute
struct LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// GameCreator.Core.LocStringNoTextAttribute
struct LocStringNoTextAttribute_tFAA187D3CCF0D9D70EA928FA8713ADBB5BF5F1E9  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// GameCreator.Core.RotationConstraintAttribute
struct RotationConstraintAttribute_t4B4100789F0B127714CA267AAA8FB12D58524903  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// GameCreator.Core.TagSelectorAttribute
struct TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// UnityEngine.Timeline.TrackBindingFlags
struct TrackBindingFlags_t934F14348EB76C252C02391539711C45063B40E5 
{
public:
	// System.Int32 UnityEngine.Timeline.TrackBindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackBindingFlags_t934F14348EB76C252C02391539711C45063B40E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Timeline.TrackColorAttribute
struct TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_0;

public:
	inline static int32_t get_offset_of_m_Color_0() { return static_cast<int32_t>(offsetof(TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1, ___m_Color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_0() const { return ___m_Color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_0() { return &___m_Color_0; }
	inline void set_m_Color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_0 = value;
	}
};


// GameCreator.Variables.VariableFilterAttribute
struct VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// GameCreator.Variables.Variable/DataType[] GameCreator.Variables.VariableFilterAttribute::types
	DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* ___types_0;

public:
	inline static int32_t get_offset_of_types_0() { return static_cast<int32_t>(offsetof(VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD, ___types_0)); }
	inline DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* get_types_0() const { return ___types_0; }
	inline DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0** get_address_of_types_0() { return &___types_0; }
	inline void set_types_0(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* value)
	{
		___types_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___types_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GameCreator.Variables.Variable/DataType
struct DataType_t2E744D1B77B737ABF856DD2AB1ED0EC8D71C0FE1 
{
public:
	// System.Int32 GameCreator.Variables.Variable/DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t2E744D1B77B737ABF856DD2AB1ED0EC8D71C0FE1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.Timeline.TrackBindingTypeAttribute::type
	Type_t * ___type_0;
	// UnityEngine.Timeline.TrackBindingFlags UnityEngine.Timeline.TrackBindingTypeAttribute::flags
	int32_t ___flags_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_0), (void*)value);
	}

	inline static int32_t get_offset_of_flags_1() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780, ___flags_1)); }
	inline int32_t get_flags_1() const { return ___flags_1; }
	inline int32_t* get_address_of_flags_1() { return &___flags_1; }
	inline void set_flags_1(int32_t value)
	{
		___flags_1 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// GameCreator.Variables.Variable/DataType[]
struct DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void GameCreator.Variables.VariableFilterAttribute::.ctor(GameCreator.Variables.Variable/DataType[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639 (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * __this, DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* ___types0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void GameCreator.Core.LocStringNoPostProcessAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.Timeline.TrackClipTypeAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackClipTypeAttribute__ctor_m566BB9E77976D140DCB8CC176CB1D4398875B7D1 (TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 * __this, Type_t * ___clipClass0, const RuntimeMethod* method);
// System.Void UnityEngine.Timeline.TrackBindingTypeAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackBindingTypeAttribute__ctor_mCA0817AAD11C2E4EC5FA6FF048FCA3E3D959D35A (TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void UnityEngine.Timeline.TrackColorAttribute::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackColorAttribute__ctor_mE8F8BF09B3A86CA8FE2E3CC50CCBE823EFFB3BE0 (TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method);
// System.Void GameCreator.Core.LocStringNoTextAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocStringNoTextAttribute__ctor_m2F3042F3260B6BEB7DF68552F97EA5E96D8143FF (LocStringNoTextAttribute_tFAA187D3CCF0D9D70EA928FA8713ADBB5BF5F1E9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void GameCreator.Core.IndentAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338 (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, int32_t ___lines0, const RuntimeMethod* method);
// System.Void GameCreator.Core.EventNameAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventNameAttribute__ctor_m41852A61BDA5AB2D287649265057870A8DC92B15 (EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void GameCreator.Core.TagSelectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * __this, const RuntimeMethod* method);
// System.Void GameCreator.Core.RotationConstraintAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationConstraintAttribute__ctor_m67FAF4934092044140A0E4EDF2B2571EC3C1495C (RotationConstraintAttribute_t4B4100789F0B127714CA267AAA8FB12D58524903 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
static void AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ActionListVariableAdd_tE97C2318D01988C34C32D7803FD63CCBE48C5345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableAdd_tE97C2318D01988C34C32D7803FD63CCBE48C5345_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionListVariableClear_t0CA3D3BDF17016B08E7AF0B55243BF9B7A6DF6E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableIterator_t2245EB96586671E181315DB90CE2B697C3D891AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableIterator_t2245EB96586671E181315DB90CE2B697C3D891AC_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator_ActionListVariableLoop_Execute_m484A83A08BD40ACF0AC56B7F65DD61F11E15465E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t22946E06EA26E15F68CF21A5C4D4683C2C4D3636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_m658ECA02DE483EE45BD23E8B74735576A87A991B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_mA4C5A1494AACB2E9E96F1C9BD450A199CE8B687D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFD2215568C11F45456ADE5DF3E6A8D8F5004560(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEEBD76692D994EC6D6539E5B6392BE1A12366222(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_mD77DDDFDD37CE5E81FC21CF5E61415CDFC2FDC99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionListVariableRemove_tF8372B6EA9572A4565720BEEBACB9F8A9CD52448_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator_select(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator_assignToVariable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionVariableDebug_t4D9084BB18314789B9BEE917E1637D655FE47FA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableRandom_tE225EE7232A93F7C812A504BE92B99F7324000DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableRandom_tE225EE7232A93F7C812A504BE92B99F7324000DA_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesReset_tE2CD7C74F64A562086D82B2F7A1BBC31D088F554_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableAdd_t459299F242296854E096A69B18927552B8B04AD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableDivide_t6E9F9EFF1D807EE1A3F34873CC3297984F7E6270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_result(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_variable1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_variable2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariableMultiply_t81254610E1EC59EF7A12BE6388A11802E3E0231A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableOperationBase_t48A1078C98B26F63F8C064F1AF4EED1C372D3B02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariableOperationBase_t48A1078C98B26F63F8C064F1AF4EED1C372D3B02_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariableSubtract_t72261CD2C98212DA97316DA1AE4EE73896E65025_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD_CustomAttributesCacheGenerator_VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639____types0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void VariableProperty_tE9B38002E5B88A7A44A86DBD39FF554C964F9B71_CustomAttributesCacheGenerator_variableType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4C\x69\x73\x74\x20\x56\x61\x72\x69\x61\x62\x6C\x65\x73"), NULL);
	}
}
static void ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_U3CiteratorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_ListVariables_get_iterator_mFCBB7595D4EA279F6B1F9C15FA22579B60672FA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_ListVariables_set_iterator_m74F41B75E46D982BD624A4E8D6EDD63FCE9235BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LocalVariables_t7EEE0938AAE686CD26E6F0600EAA99FF319B1529_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4C\x6F\x63\x61\x6C\x20\x56\x61\x72\x69\x61\x62\x6C\x65\x73"), NULL);
	}
}
static void TextVariable_t9D2D43DA7989AEB960E0559A60987D61B5021C86_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x54\x65\x78\x74\x20\x28\x56\x61\x72\x69\x61\x62\x6C\x65\x29"), 20LL, NULL);
	}
}
static void ConditionGameObjectInList_t757559B04B7C567170360111079485E60D7676CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionGameObjectInList_t757559B04B7C567170360111079485E60D7676CF_CustomAttributesCacheGenerator_containsObject(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ConditionListVariableCount_t0B99E772D6DC0E8148062A9281F5EE38BABEBB40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionListVariableCount_t0B99E772D6DC0E8148062A9281F5EE38BABEBB40_CustomAttributesCacheGenerator_comparison(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void DatabaseVariables_t45ED3F34C851C8C28F7FD49C7942A8A9B9F6B390_CustomAttributesCacheGenerator_tags(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DatabaseVariables_t45ED3F34C851C8C28F7FD49C7942A8A9B9F6B390_CustomAttributesCacheGenerator_variables(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MBVariable_tB349AB541406D4F038A3EDCC0CBA3854D57C4D72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void GlobalVariablesManager_t78138F07EAEBEC5E10FE8F0E36BACEAF77E223BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void GlobalVariablesManager_t78138F07EAEBEC5E10FE8F0E36BACEAF77E223BF_CustomAttributesCacheGenerator_GlobalVariablesManager_InitializeOnLoad_mA313C49AF61E1FD4278C32C60581918067C63663(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 0LL, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varStr(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varNum(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varBol(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varCol(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varVc2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varVc3(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varTxt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varSpr(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VariableGeneric_1_t9D5408540E5BA670F2F7C41EAE3057F924C6C34D_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator_message(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator_ActionFloatingMessage_Execute_m5DB2F53398A5D023EA3DAB96EACBBBE242D448F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t327F970DAFBAAF04F67C01EE0D1156582B2E0633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m645B88BD53A458E607304C79B5A5D69EC4FBE72B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_m074630D9FC92E1AE2788E076CA71820143E7E731(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE43ABB7C01C2C671FEDD0BBFB7C5497148DC7B47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_mFE2F97328C5088F606C650B4DF62ACA28328F7C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m09CF5B3C1EC26C1A1F053626B8B2A6E8A7658AEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FloatingMessageManager_t605E5D87FFD45F180931B4A45268B973332BE841_CustomAttributesCacheGenerator_FloatingMessageManager_CoroutineShow_m0220E1AE93E6EC41522C334BA5C5F1287A269900(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_0_0_0_var), NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7__ctor_mCAE5A30C39DEC18976BFB9583C017FCCCA30A36E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_IDisposable_Dispose_m86C925F77A798544C527999C561D3874064C373B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m714EDE17A8EBE56F2D7BA04B146A1F94D5C2A1D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_Reset_m9BEB6319F95DA2D06C30F20C4A7D55000B14232B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_get_Current_mEB27EBC56988E61DF2FF2BEB9968EBC2ED2CA3A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator_message(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator_ActionSimpleMessageShow_Execute_mC5832C6D843A71A1A4C29DA4676AFF28998EE806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t4B19567383FBBC383EEE6CC58C53834828CD9213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_m3F5262E9C558F8015181D212E910AF955EED9179(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_m388E6650772D9F9F1B1C94AA393E1C65D0E5743E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28E82C9CEF55D8CF9A58366AD7F3FC1071937101(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m551180D660E24ED9170D68C6C7FC8E25934190CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mB797C426D079B14DA6B07F17558837335739D50A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SimpleMessageManager_t61808F8E20DF4106250E824C6A9D538912E69881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x61\x6E\x61\x67\x65\x72\x73\x2F\x53\x69\x6D\x70\x6C\x65\x4D\x65\x73\x73\x61\x67\x65\x4D\x61\x6E\x61\x67\x65\x72"), 100LL, NULL);
	}
}
static void SimpleMessageManager_t61808F8E20DF4106250E824C6A9D538912E69881_CustomAttributesCacheGenerator_SimpleMessageManager_HideTextDelayed_m1CB192B2F48BECC263728253FA9840D360B3CE64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_0_0_0_var), NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11__ctor_m7E5DCB81FCD6E15E8D6CC97EAD891CA0BD8F22A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_IDisposable_Dispose_m50F21BC68D8A41B1065AFDDFBB4BC323ED1024C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67242BD4F4EE571C0EFA15B38109ED78B3D974E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_Reset_m5869D5BC10C92C2F71A32895DDDFA44A0A862BB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_get_Current_m1C941C49AC049605C872428FD343A5D9990088AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionMeleeAddDefense_tF9B23CC96635AC449D9E64986642013DA9B5C896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeAddPoise_t00B2AEFAE5FE6BB9A393E24ECAC225CED6EA992D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeAttack_t1102798C754D0AE957F9B1789206D019A1E1483D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeBlock_t81A6D6135ECF5521AD061E79A45DA91B6FE61400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeBlock_t81A6D6135ECF5521AD061E79A45DA91B6FE61400_CustomAttributesCacheGenerator_blocking(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionMeleeChangeShield_t749B62201A9BF258DCBFF2416ACD339EBF951A21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeDraw_tEF384443AF276D6FF300C0D1B1180C638AAA03EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeDraw_tEF384443AF276D6FF300C0D1B1180C638AAA03EB_CustomAttributesCacheGenerator_drawPreviousWeapon(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionMeleeFocusTarget_t7286DE047091458BCE6020BC766BE9104E8CA151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeFocusTarget_t7286DE047091458BCE6020BC766BE9104E8CA151_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionMeleeSetInvincible_t63EDBFD5C0E72F452C002621CE9E7ECA0ADDBD7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMeleeSetInvincible_t63EDBFD5C0E72F452C002621CE9E7ECA0ADDBD7E_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionMeleeSheathe_tECF149A1FEDD4B9E695693B9265D214935143161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6C\x65\x65\x20\x43\x6C\x69\x70"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x65\x6C\x65\x65\x2F\x4D\x65\x6C\x65\x65\x20\x43\x6C\x69\x70"), NULL);
	}
}
static void MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_gravityInfluence(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_hitPauseAmount(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_MeleeClip_ExecuteHitPause_m443EF74676E99E02E6B0A751C098887E02AA66F5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_0_0_0_var), NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37__ctor_mA10EB23AFBB6F257F7D54CB541E96CDB237B2A47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_IDisposable_Dispose_m1FF79EE075B38D2D8362870A57BF06D9A6802B80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9ECB2917A67CF28F30304842FE0AA6968E907F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_Reset_m4A85E677F743397D8F2C984245BADCD93DE11221(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_get_Current_m7E7F2C0D3E1DE7198126B25DB63FF8E140EB3A79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6C\x65\x65\x20\x53\x68\x69\x65\x6C\x64"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x65\x6C\x65\x65\x2F\x4D\x65\x6C\x65\x65\x20\x53\x68\x69\x65\x6C\x64"), NULL);
	}
}
static void MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_shieldName(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_shieldDescription(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_lowerBodyRotation(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 180.0f, NULL);
	}
}
static void MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6C\x65\x65\x20\x57\x65\x61\x70\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x65\x6C\x65\x65\x2F\x4D\x65\x6C\x65\x65\x20\x57\x65\x61\x70\x6F\x6E"), NULL);
	}
}
static void MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator_weaponName(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator_weaponDescription(CustomAttributesCache* cache)
{
	{
		LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 * tmp = (LocStringNoPostProcessAttribute_t95216CFD8BDA349CDC51237C009AD48D933FF992 *)cache->attributes[0];
		LocStringNoPostProcessAttribute__ctor_m33D3FA3DD443658D56044785BA3A964566B5A26D(tmp, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_U3CMeleeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_boxInterframePredictions(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 19.0f, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_debugMode(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_EventAttackStart(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_BladeComponent_get_Melee_mB9E245D961F3BB1A87373D8E939D278F7C8C28F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_BladeComponent_set_Melee_m7071E3DCEB7520779AA743DA210BB810041366A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x65\x6C\x65\x65\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x4D\x65\x6C\x65\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_0_0_0_var), NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CPoiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CDefenseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsDrawingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsSheathingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsAttackingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsBlockingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CHasFocusTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventDrawWeapon(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventSheatheWeapon(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventAttack(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventStagger(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventBreakDefense(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventBlock(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventFocus(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CCharacterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CCharacterAnimatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CBladesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Poise_mAE7DA4E54BA255648BD8839974782B5FB9759A83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Poise_m4A8BF6725DF27A0938FCF2C698694491FA1CE201(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Defense_m507A720A94BCAB37B13ED5B740535DA9208302D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Defense_mB3F50AE24CE4E58C8D6763D7A1CCCB2FA8BAD61A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsDrawing_m896E27571073303B79D91CC2886EA9C0636D92CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsDrawing_m6DE5FA9449BA3AFE940FFCF9CBFE4C9C7EED6310(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsSheathing_mBD515D68F0139E9E1913CA37B3F8B9B10A735FE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsSheathing_mD90AAC761D35BF4D894733EC2AD265B6859CCAA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsAttacking_m4E8EE2CD01688E319C7166420F9342088E9B7BFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsAttacking_mA3B512832B1EAE92A05E64DE3791659CB78F8A7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsBlocking_mFF11D0FD9798CCCF39E1545039E591236C5C896A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsBlocking_mCB612F17CA908BA0BD4188DE62ACAA49A7367D95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_HasFocusTarget_m7BD8A39A0F492B250E44E3208F79B8B5C4DEBF20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_HasFocusTarget_m590FCFD6C2DC40DF79BB59FEDAC4A1AAEA08D2AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventDrawWeapon_m729A717864566C772CF71410BDE1924DBC3AE947(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventDrawWeapon_m3A5FA842827A19CBB1895C99397FA608F52E9D60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventSheatheWeapon_mFDA0966CF05CA40A002E10DCE4C99DB694C5AC77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventSheatheWeapon_mFA2B6F3F7DE02A7CFA9A36FE638A2B044AD289C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventAttack_m2DBB7EEA0FB85D42038CE65C50B355CB02C782BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventAttack_m8EEAE732C1B79F7BD62F5C72513543C2D361183E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventStagger_m4109CC64CD32AA9507C988F3934BCC867CF25381(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventStagger_m3E4FB53BF4351A020EDC55C7332F901CF14A99BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventBreakDefense_mB6BB87DFB00DB2B5C574CC8968782E6E6BDA702E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventBreakDefense_m6FCF86C0FEB98F29565FDA8088184CE910D40BBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventBlock_mF88A06EB7F4A0F2C1010BC793B5E4E29416468AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventBlock_m8A34212AA2E12435FF6D7EB286882E490296ADC0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventFocus_m634B32366980C642D31F60AC90C71CB7152BD014(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventFocus_m2EF8A205D03A9353EC32768ECFE79B2142FC9674(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Character_mC8213FA44F089EE7A6B5B06B0D7390E32E4CB5CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Character_m78B44B240657959C2834FB7887BA2D07FE375DE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_CharacterAnimator_mA4B0B02AB3070AEE773BF16764D3D3796A1A9FB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_CharacterAnimator_m2AD046145B4043A4D45654BB2C01E0F7BE6428EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Blades_m4E84AE0B4C496A680D941D9981142B0A81CBD87F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Blades_mC80F11AE570B6BD8E1339C79799EA35F12D6E0F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_Sheathe_mFB1AED1FFEB02DFD57F1E5FAA86CB41C8E18C3E7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_0_0_0_var), NULL);
	}
}
static void CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_Draw_m00C457FC85DEA4F4DFC918137617E27E6357A39B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_0_0_0_var), NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102__ctor_m786A68C4F750B9E38A58F10D081860846F94A69D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_IDisposable_Dispose_m689042F48C397182F117B6C74794F679471DEB34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1FEA64D0AD51EBDA277E8E71DA96251B74C7CA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_IEnumerator_Reset_m8D077FC808E8BC30A3CEE7FE46311E214B99C354(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_IEnumerator_get_Current_mD4C206E05163045BB434D18C816174F17F2B2741(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103__ctor_m3CDEEE7F8DB99DC8D219743C706D8827F2422D56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_IDisposable_Dispose_m24DBACDBA5ED681EB17250F583B4BA16E82115C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3BCBB31D4FD10EECEE363E70EB8F06ACBB66B39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_IEnumerator_Reset_m6768D2D348120C62AE0E8148B25D55D1CA19CBE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_IEnumerator_get_Current_m6E02C65508D6A1B59269C5B13E0D3A071A40EAAE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x2F\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x4D\x65\x6C\x65\x65\x20\x55\x49"), 0LL, NULL);
	}
}
static void CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator_poiseImageFill(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x69\x73\x65"), NULL);
	}
}
static void CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator_defenseImageFill(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x65\x6E\x73\x65"), NULL);
	}
}
static void ConditionMeleeArmed_t00E599B647974D4641746A1FF70201E321DB2E85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionMeleeCompareDefense_t672E338E78E0660A5DFA0B291ADA6F31587FFC7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionMeleeCompareDefense_t672E338E78E0660A5DFA0B291ADA6F31587FFC7E_CustomAttributesCacheGenerator_comparison(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ConditionMeleeComparePoise_t3B8CA0E3067DFF3659FC05681306522211EC2CCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionMeleeComparePoise_t3B8CA0E3067DFF3659FC05681306522211EC2CCE_CustomAttributesCacheGenerator_comparison(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ConditionMeleeFocusing_t3D34FD8161500B32691C0EAFF207E5B26C88EC19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionMeleeIsAttacking_tAD530ABBA66ADD5AA2C50F1DFDA1082BCF7ED2CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionMeleeIsBlocking_tB030896C16730D2021014D6DA3D960510FFDB694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMeleeOnAttack_tFEB32CECDA9FF3BE53A464420ADD6FA742E0CDAC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMeleeOnReceiveAttack_tC73836BB57D725D78FA798B32E5A96BFA1197313_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMeleeOnReceiveAttack_tC73836BB57D725D78FA798B32E5A96BFA1197313_CustomAttributesCacheGenerator_storeAttacker(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void PoolManager_t055017463B92A65D81A5E754D15D12DA13724CC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void PoolObject_tF0C5B9B1AAF3697954D07E70476F7D3F483EE588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x50\x6F\x6F\x6C\x2F\x50\x6F\x6F\x6C\x20\x4F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void PoolObject_tF0C5B9B1AAF3697954D07E70476F7D3F483EE588_CustomAttributesCacheGenerator_PoolObject_SetDisable_mAA2DF8FF1FE4B2A52BDA18F7D4D9711A81591C51(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_0_0_0_var), NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7__ctor_m82A6D08FFA1F31480B620ECA12FC75F1CAD29BCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_IDisposable_Dispose_m245E04359C4A3FC3EDA5E332275E22D78084C139(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB212C3BF8612759E13002DB305FDCD7C5C2FD19B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_IEnumerator_Reset_mF5D91E3D2FF98620E0869B07A72911E72F8F3B46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_IEnumerator_get_Current_mB428927DA65710A66D860A096A09C621EB5F7189(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionsTrack_t1B5EDD15620DBF9785327B7E180BC80327FAF5DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ActionsAsset_tED7A2BE02E6133961A58377E6BC7C084F0470956_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 * tmp = (TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 *)cache->attributes[0];
		TrackClipTypeAttribute__ctor_m566BB9E77976D140DCB8CC176CB1D4398875B7D1(tmp, il2cpp_codegen_type_get_object(ActionsAsset_tED7A2BE02E6133961A58377E6BC7C084F0470956_0_0_0_var), NULL);
	}
	{
		TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 * tmp = (TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 *)cache->attributes[1];
		TrackBindingTypeAttribute__ctor_mCA0817AAD11C2E4EC5FA6FF048FCA3E3D959D35A(tmp, il2cpp_codegen_type_get_object(Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_0_0_0_var), NULL);
	}
	{
		TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 * tmp = (TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 *)cache->attributes[2];
		TrackColorAttribute__ctor_mE8F8BF09B3A86CA8FE2E3CC50CCBE823EFFB3BE0(tmp, 0.800000012f, 0.75999999f, 0.639999986f, NULL);
	}
}
static void ConditionsTrack_t1748BE481979E47AB07BF6935F015C0386653B74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConditionsAsset_t0C09F6E62B15BDBCE2333B79706C7D7764A82A0A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 * tmp = (TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 *)cache->attributes[0];
		TrackBindingTypeAttribute__ctor_mCA0817AAD11C2E4EC5FA6FF048FCA3E3D959D35A(tmp, il2cpp_codegen_type_get_object(Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_0_0_0_var), NULL);
	}
	{
		TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 * tmp = (TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 *)cache->attributes[1];
		TrackClipTypeAttribute__ctor_m566BB9E77976D140DCB8CC176CB1D4398875B7D1(tmp, il2cpp_codegen_type_get_object(ConditionsAsset_t0C09F6E62B15BDBCE2333B79706C7D7764A82A0A_0_0_0_var), NULL);
	}
	{
		TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 * tmp = (TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 *)cache->attributes[2];
		TrackColorAttribute__ctor_mE8F8BF09B3A86CA8FE2E3CC50CCBE823EFFB3BE0(tmp, 0.589999974f, 0.790000021f, 0.75f, NULL);
	}
}
static void TriggerTrack_tB1B216F9527EDD1402F9FFE52EA9CD848139E906_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TriggerAsset_tF8F0065F50595732137DCE4EDEDEAA712D8592A8_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 * tmp = (TrackBindingTypeAttribute_t79AF88B1D799883A54208F08FB36D25582E1D780 *)cache->attributes[0];
		TrackBindingTypeAttribute__ctor_mCA0817AAD11C2E4EC5FA6FF048FCA3E3D959D35A(tmp, il2cpp_codegen_type_get_object(Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_0_0_0_var), NULL);
	}
	{
		TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 * tmp = (TrackClipTypeAttribute_t2802C3E113456E193F0FA077805F87B7F8C99F59 *)cache->attributes[1];
		TrackClipTypeAttribute__ctor_m566BB9E77976D140DCB8CC176CB1D4398875B7D1(tmp, il2cpp_codegen_type_get_object(TriggerAsset_tF8F0065F50595732137DCE4EDEDEAA712D8592A8_0_0_0_var), NULL);
	}
	{
		TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 * tmp = (TrackColorAttribute_t6EAC4E29A7C89815E16DD0735D129B7C3DCF6BF1 *)cache->attributes[2];
		TrackColorAttribute__ctor_mE8F8BF09B3A86CA8FE2E3CC50CCBE823EFFB3BE0(tmp, 0.75999999f, 0.75999999f, 0.75999999f, NULL);
	}
}
static void LocalizationManager_t90448B147FD8AC54B93A00F5F37AFB336180796D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x61\x6E\x61\x67\x65\x72\x73\x2F\x4C\x6F\x63\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x4D\x61\x6E\x61\x67\x65\x72"), 100LL, NULL);
	}
}
static void TextLocalized_t4B40B443794D291968B96B89B870C0DDBBDEE38D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x54\x65\x78\x74\x20\x28\x4C\x6F\x63\x61\x6C\x69\x7A\x65\x64\x29"), 20LL, NULL);
	}
}
static void TextLocalized_t4B40B443794D291968B96B89B870C0DDBBDEE38D_CustomAttributesCacheGenerator_locString(CustomAttributesCache* cache)
{
	{
		LocStringNoTextAttribute_tFAA187D3CCF0D9D70EA928FA8713ADBB5BF5F1E9 * tmp = (LocStringNoTextAttribute_tFAA187D3CCF0D9D70EA928FA8713ADBB5BF5F1E9 *)cache->attributes[0];
		LocStringNoTextAttribute__ctor_m2F3042F3260B6BEB7DF68552F97EA5E96D8143FF(tmp, NULL);
	}
}
static void ActionCharacterAttachment_t8BA5DEF42000816F2966B57D12CDD33DCB10143B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterDash_t7DCB83470E323AC4A96F916FFCC39E9FC5A1572A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterDash_t7DCB83470E323AC4A96F916FFCC39E9FC5A1572A_CustomAttributesCacheGenerator_dashClipForward(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionCharacterDefaultState_t19A88B6694C52D5F1A3818DABC5DD6110245D676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterDefaultState_t19A88B6694C52D5F1A3818DABC5DD6110245D676_CustomAttributesCacheGenerator_state(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionCharacterDirection_t2F3A3767657696F9CA5C9D191D638E7F1E517E01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterFollow_tC034266FE059628EE82A5761B752342791F27D79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterGesture_t0309F0651027D4353C4F5EB512CB0E266DB4495C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterGesture_t0309F0651027D4353C4F5EB512CB0E266DB4495C_CustomAttributesCacheGenerator_ActionCharacterGesture_Execute_mF079124C1F71B2CBA071614DF5D961DA403013ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t186ACC121AC3A0130436CCBC79C015D43F4630DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10__ctor_m3C6F29778E65D111F6E51FFF293FF6AB4E1D8B54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_IDisposable_Dispose_m4E1B6B7427CEFE2C3661378F63CEA456C34B7F18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80DEEACBD0C640866FB5D092E8FFE518CFA782A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_IEnumerator_Reset_m5DF0124AAAD326C0E6C5520768B34B7C811DBEB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_IEnumerator_get_Current_mBA7CD44299C5921344680D9462F7C272A5782DE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCharacterHand_t5F2347D31CF1E8C931A63584E1E4D59FEA79A816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterHand_t5F2347D31CF1E8C931A63584E1E4D59FEA79A816_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 5.0f, NULL);
	}
}
static void ActionCharacterIK_t296DD4F660C2521311B95CD9BC6D91D7D845AE66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterIK_t296DD4F660C2521311B95CD9BC6D91D7D845AE66_CustomAttributesCacheGenerator_part(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionCharacterModel_t4942BE160154AD1B5DFDD47FE30D10F85175B856_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterMount_t8E1FA590B6E809B4F42FB1720DF2CFCC3B8A165F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterMount_t8E1FA590B6E809B4F42FB1720DF2CFCC3B8A165F_CustomAttributesCacheGenerator_mounted(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_stopThreshold(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x74\x68\x61\x74\x20\x69\x73\x20\x63\x6F\x6E\x73\x69\x64\x65\x72\x65\x64\x20\x61\x73\x20\x72\x65\x61\x63\x68\x65\x64"), NULL);
	}
}
static void ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_ActionCharacterMoveTo_Execute_mFDD8702F438971F618BC2CC5070F79F5D2D755B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16__ctor_m11BFD9B002E8E393AA4F150E4D26ED9408BA65B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_IDisposable_Dispose_m76698B7FC09D4CEB478E55D7D28CCDDA5FD7A03B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14CCFEB1097ADA90AF91AB66664339DC1FA086D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_IEnumerator_Reset_m954AD4B93FCE4A094FE5D1EA875B8516F283DE95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_IEnumerator_get_Current_m1819E8E5ECDC100A7ACF93D0787045CFBA63EBCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCharacterProperies_tEF227A7B5249744012D763035B219BD2A32BB3EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterRagdoll_t54AAD233E21ACCA62FD602AD13FD0F8FA97AC8B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator_ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_0_0_0_var), NULL);
	}
}
static void ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator_ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B____parameters3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m93A9ED4176E3050FF375EC8C119F4EF8DF71FE74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_mC69556267AA0ECA60558F102F170DE67D89E5E2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF92FCFBC97E98DD5FA109F65537870A658014802(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m28C693D9D027244014063A86657F20629EB0D302(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m89023EE397F53C4FA098252012B715C3A57DF914(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCharacterState_t293D0F3506F4759A2E10FDCCABE8EAC4935A0E59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterState_t293D0F3506F4759A2E10FDCCABE8EAC4935A0E59_CustomAttributesCacheGenerator_weight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionCharacterStateWeight_tDE8624304A2B0BB21CB6C50539A25421C8EAADF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterStopGesture_tAFAB500F430FF9DDDC0E286092DD10F56A3988B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterStopMoving_tF512DA676C407B755DC1DADE13DCF8138AD2A05F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator_position(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator_ActionCharacterTeleport_Execute_m9542BA250FECE50E3E269827B77E3E44633BA65E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_m0229D5A895D47865D75B9CA9E6E669E1C2F14465(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m895F9261CA1402B728553CFF4F16F2F936BF7F9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m105C970E77214FD89055FF4D88816CB5BA8E1938(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m1A8C9CD34172B9F3BD01A4CBF933EEEE5DBC06EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mE70B6EA23875886C4CFBEC39F12808AC10AEB884(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCharacterVisibility_t9D49FE3829E4BCE271A9FC8E0F6EE0A05B567CB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterVisibility_t9D49FE3829E4BCE271A9FC8E0F6EE0A05B567CB9_CustomAttributesCacheGenerator_visible(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionHeadTrack_t2A9CB746A712A13C46EF8B5667083FF3774792B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionPlayerMovementInput_t51FC6C63D25AF2AC2A7FF05A7D141F49B5A494DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionCharacterBusy_t77B44CAC38A1EE5A3B07FE69A5D5A725B5B30B98_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionCharacterCapsule_t1FBE62DF09866E8E4DFA5A5FBA9F8422CED60612_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionCharacterProperty_t5D830CDF6022790EA73B880CFA9A7871AAF5B318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCharacterStep_t5931FF16E7708CE382A87FB8263A21345A23FA54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_CrossFadeGesture_m1F6197A3A14AB8E9EE47EAB409CE37AC6A7BB230____parameters5(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_SetState_mF674B49D07C0F6E9E1A28331F0189379391B5CDB____parameters6(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_SetState_m8C3CE69D37ECC30B3B9F9C352B2A3D365942A4D2____parameters7(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableBase_t07B459408084FCE8E65ACB7350D48128438B34E9_CustomAttributesCacheGenerator_PlayableBase_DestroyNextFrame_mD1CA8E6618014C5F0FF63CFB4CC580E8481DB93C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_0_0_0_var), NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16__ctor_mC2D8B87272A5FF7978C249A18186598873F2B420(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_IDisposable_Dispose_mD1D6D3F8B8A5047BE75FBBFD0DCD02F05CF12971(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70A9E325F1FF7756263DDB8DF254F4C1ABB5E9CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_Reset_m3FB96BB157E9D36F17571507B075F3A3B60EEB6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_get_Current_mE41FA7C32E7D6344416ECD3407387C2AD2F5365E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayableGestureRTC_t2DF9641C562EEC688128A23B56A0F66A35681E3F_CustomAttributesCacheGenerator_PlayableGestureRTC_Create_mAB25365F7D4B64317DFE04EFCB031E3728DA3366____parameters8(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableGestureRTC_t2DF9641C562EEC688128A23B56A0F66A35681E3F_CustomAttributesCacheGenerator_PlayableGestureRTC_CreateAfter_m171D1CC7E4B96BD20FA8DA9906BC797FC6B791A6____parameters7(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CLayerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CAnimationClipU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CCharacterStateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_Layer_mA0A0FEF3E34BB52210BEB65292E152C37F4E5305(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_Layer_mDBC6CF2A5E1072D454BCD7F6633D52C18E7C04E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_AnimationClip_m86C11355A72EB8137CA039DAB5123DD11D9AB080(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_AnimationClip_mAC5DDE460E808A580B977179B28D9EA95CDF44A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_CharacterState_m42DDC145AE77AB341C5AB3B0C78C34E0B6815A60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_CharacterState_m79263DC2A30FB8420BB2EBF7197CA9B823ADA473(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_Create_mFCCFE038760E4D43A4EB8FE25A5A740389DE1850____parameters11(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_CreateAfter_mD3174E0DE0FB1886551378074FC02AE8BC6FFDF9____parameters10(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_CreateBefore_m541F003F91662EAF4A5C74B16127F0A19D9816D0____parameters10(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_Create_mE1EF1120BD64AFC4853B67411BC949CED140C1F6____parameters10(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_CreateAfter_m04896D425083377B366B74F0B217FA12F4EC00CE____parameters9(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_CreateBefore_mDB2045A10400A6AF3BFA8F6DC46B31723666487D____parameters9(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x73\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72"), 100LL, NULL);
	}
}
static void Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_CustomAttributesCacheGenerator_Character_DelayJump_mA4414CFD1241AEB812807E713BC32E7980413D8F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_0_0_0_var), NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CactiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CrotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_active_m34FF82B6AF6DD2D3E928D916780B138E9719C367(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_active_m0BA91835FFFE3C402173307019ACB40D307A391C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_position_m5B68052A77DBC44449874754803BAE3644D63890(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_position_m093315344BB72869F08A67A90158EE06ADA7CEC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_rotation_mBD41328E6A7445448AE5836E0334AF7F452599CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_rotation_m6D4D33972A156DECD91F919E2CE0BE33EC8E560C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38__ctor_m45DB7082FB55923B86B2ED0DEF78A2C72F7DD8DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_IDisposable_Dispose_mB0D6B4E55D63E183B283D9DDD648BDF1F006DB49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDA92A55DECFFF61C95B266DE83FCC68616C1187(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_Reset_m3EAD32CF2878187EF54DD5E5BBBCEDEF3188B42C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_get_Current_m80E2FDA7B04AEE391BD2B84A8D27DD146AA6FBBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x73\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x41\x6E\x69\x6D\x61\x74\x6F\x72"), 100LL, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_defaultState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_ragdollMass(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x74\x61\x6C\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x6D\x61\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72"), NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_stableTimeout(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x6E\x65\x65\x64\x65\x64\x20\x74\x6F\x20\x63\x6F\x6E\x66\x69\x72\x6D\x20\x74\x68\x65\x20\x72\x61\x67\x64\x6F\x6C\x6C\x20\x69\x73\x20\x73\x74\x61\x62\x6C\x65\x20\x62\x65\x66\x6F\x72\x65\x20\x67\x65\x74\x74\x69\x6E\x67\x20\x75\x70"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 5.0f, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_timeScaleCoefficient(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_CrossFadeGesture_mE39F18534A58C1963F900C2D6FE97E8CADA6AF66____parameters5(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_SetState_mB85A38CEDE7CC182D218720577DC5E982BF28ADA____parameters6(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_SetState_m2A2E1EC36A2F8F29C58943482954A613185F21CC____parameters7(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CharacterAnimatorEvents_t890194BBCA5F9C0A0653D57C6DF10828C52B7647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterAnimatorEvents_t890194BBCA5F9C0A0653D57C6DF10828C52B7647_CustomAttributesCacheGenerator_CharacterAnimatorEvents_CoroutineTrigger_m9088D17F2E9CAA8C76F641F7F41BC210DDD32476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_0_0_0_var), NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7__ctor_mB1B3F3D66017ACD041A3DF472B357151D137719B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_IDisposable_Dispose_mDF30931154439004D31B2A026851F8AA63ACDDBA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D7C9AC15EAC985E0945CE394972822428370F2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_Reset_m623E4A129BD91E66D1397F77758386EBF64A20F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_get_Current_mC44FB9A3B262F7673714DD3EDA5E77E5891DBCB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_U3CtargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_get_target_m0B4324C67138CD8F6AAC5BC84888EFF7A978F1DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_set_target_m189D5417D456FCEBB0DD9F0DDBB4322E5AAB4A1D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_get_value_m2CDBD910B33AA9C38C3E807BACAC38C1978F9D63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_set_value_mC1E3E842276BA71CA344134C5593569748D6584E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_U3CattachmentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_CharacterAttachments_get_attachments_m0CB2DB2A159F8F311D19A4707283B2F5C9213D50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_CharacterAttachments_set_attachments_m8FDDE1DF5B13AEA89E97636F72BBF4222ECDCD08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_CharacterFootIK_get_Active_m026E791D09313D95E6552031863D8A3C177CE13F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_CharacterFootIK_set_Active_m6ED2E272FEB78F4FD67F9E8D9A6A4E1A7CE6303C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_CharacterHandIK_get_Active_m9DE731503AA6488A0F459F86B49C35C7DCBB38B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_CharacterHandIK_set_Active_mB3995094DD4DA840C8B4791913796CFA57475C64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_CharacterHeadTrack_get_Active_m3816DE3591D91706830CB25BEA2C2FE7DC5BACFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_CharacterHeadTrack_set_Active_mFC5D304F334A636912FC185B19AB2D4C7EF4FD95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_terrainNormal(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_verticalSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_canUseNavigationMesh(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x20\x74\x68\x69\x73\x20\x69\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x55\x6E\x69\x74\x79\x27\x73\x20\x4E\x61\x76\x4D\x65\x73\x68\x20\x61\x6E\x64\x20\x68\x61\x76\x65\x20\x61\x20\x6D\x61\x70\x20\x62\x61\x6B\x65\x64"), NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_character(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_animatorConstraint(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_characterController(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_navmeshAgent(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_U3CcurrentLocomotionTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_U3CcurrentLocomotionSystemU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_get_currentLocomotionType_mA4FD56B3638F170115848BC7803482297B3E29D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_set_currentLocomotionType_m56CF03F923B8B8A110975B02D4CB3BF2A402B3FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_get_currentLocomotionSystem_mD7F3DE1436DF555B140C54593D6095D9AB7EAA52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_set_currentLocomotionSystem_mED30E6DD7EDAB456C270565D8C8BA330C517ABBE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_U3CisDashingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_U3CisRootMovingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_set_isDashing_mC079E6C302938750FA3B860353FD0657B09C4A53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_get_isDashing_m1C6E364F5B8D885EEB5EF34053F5C9156B12A439(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_set_isRootMoving_m360142C3A457C12C703CD94B9674C1A64EB3F21C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_get_isRootMoving_mD154CF22484E4ADD2130D6A2BC32A3FF9A44AACD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerCharacter_tC1A0A4363913B90DD7455D8623BA8E85116052DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x73\x2F\x50\x6C\x61\x79\x65\x72\x20\x43\x68\x61\x72\x61\x63\x74\x65\x72"), 100LL, NULL);
	}
}
static void NavigationMarker_t3E505B1192D0E05285FF8768371DA89C20DFB3E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72\x73\x2F\x4D\x61\x72\x6B\x65\x72"), 100LL, NULL);
	}
}
static void NavigationMarker_t3E505B1192D0E05285FF8768371DA89C20DFB3E2_CustomAttributesCacheGenerator_stopThreshold(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void RagdollUtilities_t4E030B55562006D3A12299CD3EF3F186A340A87C_CustomAttributesCacheGenerator_RagdollUtilities_GetBounds_m663E840916CD6689EE671CBC57ECA6729C7757CD____points1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void AnimationClipGroup_t875D45CA91B307FFCCE37DA5DC9266FE08D2097F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x69\x6F\x20\x43\x6C\x69\x70\x20\x47\x72\x6F\x75\x70"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x44\x65\x76\x65\x6C\x6F\x70\x65\x72\x2F\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x43\x6C\x69\x70\x20\x47\x72\x6F\x75\x70"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 200LL, NULL);
	}
}
static void ActionCameraFOV_t3345CE936F4E3E63E5B1AFDEE215181B60E9E850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraFOV_t3345CE936F4E3E63E5B1AFDEE215181B60E9E850_CustomAttributesCacheGenerator_ActionCameraFOV_Execute_m2961F82EDD4ED139F459031B2114495B90DDBEB1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_m568C46FD84354F03E321B4A7D9B6A406155A6442(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m119B7D8FFAD38119F367543F53D104EB2CB9AC46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D53463E3447749CF12F7FEA5553D2AD9FE91169(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mA5FDD65159048AF6BDEE02D3389DF5B1870EEAFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m1AF795A4F8BA74E09BB67237231F64629E174B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCharacterJump_t8645160E4F7E41243D42244392F3FCDA546632C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCharacterJump_t8645160E4F7E41243D42244392F3FCDA546632C5_CustomAttributesCacheGenerator_ActionCharacterJump_Execute_m08FA51AE3EF80F74E8C7437DF69FE88F2B3074B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mD014279CDD1DA2CB66827900A7FEF6B36E4034A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_mC869E87A2210092C97E4DBD7E06FCBE358652E49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA829096573BA1FF01D24904DA418638E07E295D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m251ED7B0A59C1F6511E8A0752E09990150A66F22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mEA566C81C92D1772C5B0DA08E2015A4B14C12E25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCursor_tE1BC859AC9930A86B608D8C2D53A37E980F7827F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionOpenURL_t35EA3EEAAC48C3B9AEA91BE0B1376E89ED6DEF66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionQualitySettings_tC936EB6BCEE807D6BF77D5C2BD3ADE8B433E5CEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionQuit_t711AF74F42693EF8A496F54C81BEDC94DB3B18E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVideoPlay_t37842823888B365635130866198EF8AAC6256173_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAudioMixerParameter_t8EE5308452ACAA231E4BDC60F271EE7195188ACD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAudioMixerParameter_t8EE5308452ACAA231E4BDC60F271EE7195188ACD_CustomAttributesCacheGenerator_parameter(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionAudioPause_tAD50D4F921DE33610C8646E2B9125D62881A4267_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAudioSnapshot_tEC996A49D5FBBB69676F0C03408A69ABB6C5A5C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_mixerGroup(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_fadeIn(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_mixerGroup(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_fadeIn(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_mixerGroup(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_fadeIn(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_spatialBlend(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionStopAllSound_tBCE33DF50D635D2611B91D29F75B7BE0D3025B7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionStopAllSound_tBCE33DF50D635D2611B91D29F75B7BE0D3025B7E_CustomAttributesCacheGenerator_fadeOut(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator_audioClip(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator_fadeOut(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ActionStopSound_tD1F5F7BBBAFA9BC409999FECC74CA1A7FFBCCA80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionStopSound_tD1F5F7BBBAFA9BC409999FECC74CA1A7FFBCCA80_CustomAttributesCacheGenerator_fadeOut(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void ActionVolume_t03D30A48E823BD343B64484448B10027922594B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDebugBeep_t0DA6169FAB929E4CA46A9CF604686DDE46B87EC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDebugBreak_t0159F759B76062DC5F1BFFF3101901CF4FABD897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDebugMessage_t5B3A716ECBB0BBA1CA19CFCA4B7FE5173245E070_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDebugName_tB1F4625BCF538C75D7A6C4B29FD0633ADB8738C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDebugPause_t05914E187FBA23242DFD8C92AD0A0365C816E03A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator_ActionActions_Execute_mF60338A03B212040424031E749D555D34F89EC45(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t6D258995D994CE0D01571BB38DA16B5CEB44CCA4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m8F225E767DD6DB758FEA0B87C3078AA77FBFA5FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_m4376D2481CBD9E7ADDC3DAF2152CFC9092F5F7E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1684DA24B04EC7538FBC1999207915EF16FD474D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m7251172E50F11399CF598F7C05629B9825C0F233(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m0DA803CF52C2AF51D6ECF171BF22580ED5539F5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCancelActions_tA7C9650A934B1B4904E6CDDFCF350377C1C411A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCancelActions_tA7C9650A934B1B4904E6CDDFCF350377C1C411A6_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionChangeLanguage_tEBE8D653514B5FF2779BAB985BEB9A20A0E26C4A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionComment_tCA0EE138D25F8CC10ACA8F6B4272D6597F8415C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionComment_tCA0EE138D25F8CC10ACA8F6B4272D6597F8415C0_CustomAttributesCacheGenerator_comment(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338(tmp, 5LL, NULL);
	}
}
static void ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator_ActionConditions_Execute_mE0522B42F62D3F243F1AFAE772B9CD175E83C357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m9EAFACDF88412BADB6CC5CAEEA99FE03D10FDAE1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m659F01A84B88179C9CAECF6702B4C86FF9A43B89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC017996B99A9A285EE0A0982EF05AD814B15FC3E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_mF76B5998B28F241A1530AF47AF804C28A03FEFC2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m555CFC88911972DC5B58D4D04A9F0C3E3821B1ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionDispatchEvent_t8C31418203F39C3E2C9A1A378BE336F9315C2BB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDispatchEvent_t8C31418203F39C3E2C9A1A378BE336F9315C2BB8_CustomAttributesCacheGenerator_eventName(CustomAttributesCache* cache)
{
	{
		EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3 * tmp = (EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3 *)cache->attributes[0];
		EventNameAttribute__ctor_m41852A61BDA5AB2D287649265057870A8DC92B15(tmp, NULL);
	}
}
static void ActionGravity_t4B1C56C83CEC558AB1FBF1C7605F5BCF60475578_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMethods_t077B9F118F8F1D6B9E4BD45252B0618160CE8A96_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionReflectionProbes_t1714B7C2CBC1988BEFFC5F0129D642538DCC7AEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionReflectionProbes_t1714B7C2CBC1988BEFFC5F0129D642538DCC7AEA_CustomAttributesCacheGenerator_ActionReflectionProbes_Execute_m5A918D81E680B42CF41A202C722F9B6CA4DA1475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4__ctor_mE3705328BA02A5C6804D656A84D2F01521659B17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_IDisposable_Dispose_mBF5E03402ECCA456AD7654A8C19ECD5785307A89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD002A5B232EDAB262AD2577DA21E86AE289242CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mAF5226AA4EF8C651DDF193AA52AF3F58E3720CE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mE081B69A7BC0703684ACD7D36C0899EBDD746C3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionRestartActions_tEAC07EBD6B760E468ED67E3BBCFE9002004CDF47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionRestartActions_tEAC07EBD6B760E468ED67E3BBCFE9002004CDF47_CustomAttributesCacheGenerator_ActionRestartActions_Execute_m141874EBFFD0D71B3EEBC622D98CB24E7FB22955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0__ctor_m52231F20C05F8C80A3ED4721BF367C1DDFB06CE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_IDisposable_Dispose_mF52E298FC3DC9C9456316D73F74C0005B769B4E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37052B91459D04361E63F25F8DCA3E4C52A95CEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_IEnumerator_Reset_mB1712CAF9C824B2468F658B4D5A50B88CE06D799(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_IEnumerator_get_Current_m3C22C51AFB94DA83D80DB69FBF40AAAAD12D9356(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator_storePoint(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator_storeGameObject(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionTimescale_t99E9A7C32BBBF5E1471C50FFA4166E3DB86C8D62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionWait_t0F27D9E980892F6FCB53F16D73DF1DC1C42DF23B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionWait_t0F27D9E980892F6FCB53F16D73DF1DC1C42DF23B_CustomAttributesCacheGenerator_ActionWait_Execute_m04586A3FDF625719D21C4722DF245176A7190D66(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tFCBDF1E50FAAD128C70DCD0BAE468BCE9599DA00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m1C9147D9500D8992417D62EDB2969850D0BCA95F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_m46DB6B72EF206B2AA46FB515BB10A3E6CE5B8FB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E1BA8ED4D7167A7EC8B04C9E249F6C95F7C911(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mEDA5AA1846C72B143E01D49133FD304FA342C9E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_mD1D01BD5AE2C54941C4051CFEF0DE1FE6E368F8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionAddComponent_t3DFDE43F53629F6EFEE0720916A427BB0BF6AE02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAddComponent_t3DFDE43F53629F6EFEE0720916A427BB0BF6AE02_CustomAttributesCacheGenerator_componentName(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionAnimate_tFC32DFF0324C4F5EB3DFC09D1CE435D94A9A2826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAnimatorLayer_tD53D3C72E25A7FE76B5A46F8DCEEC5A68AC68C63_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAnimatorLayer_tD53D3C72E25A7FE76B5A46F8DCEEC5A68AC68C63_CustomAttributesCacheGenerator_weight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_blendShape(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_ActionBlendShape_Execute_mA4756B727376FBA06CDDC8C25893BB0FE8EEE787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_mF19135E1262F01401F62B65C6AAE21E195800841(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_mB01B52B59D5766C1B8DCB01058767AAE92EE08A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB6BDC403B3CC4A1A6CC2BAC53F492A49EC68B33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m7E4256ABD8A2DB3A8A5DD1F625898904BD4749E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mD6A3F2C62C6A20DBC26A9983D538106441A5CE1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionChangeColor_t986876EE1BDA98C61B219260C8151453A10F4A08_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionChangeColor_t986876EE1BDA98C61B219260C8151453A10F4A08_CustomAttributesCacheGenerator_color(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator_targetRenderer(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x74\x61\x72\x67\x65\x74"), NULL);
	}
}
static void ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator_materialIndex(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionChangeTag_tF00F8B76DE0DF80E7757713611A8DDF651A3F80D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionChangeTag_tF00F8B76DE0DF80E7757713611A8DDF651A3F80D_CustomAttributesCacheGenerator_newTag(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[1];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void ActionChangeTexture_t11A82311D8260104EC7C527046346A6FED2FDBF6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDestroy_tA7405067C7B521EDF28E389B2784B3ADC2DC111B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionEnableComponent_tDE3E15791F7FEC384069915AA7B03A5CE375616B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionEnableComponent_tDE3E15791F7FEC384069915AA7B03A5CE375616B_CustomAttributesCacheGenerator_componentName(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator_force(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionInstantiate_t512D6F8EE2CF483C5705B28097B05BD9017BACAF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionInstantiate_t512D6F8EE2CF483C5705B28097B05BD9017BACAF_CustomAttributesCacheGenerator_storeInstance(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionInstantiatePool_t8DAF4BAD44BC787100C677029855457A46E5B28F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLight_t03504821EF400C268A32470CB0ADACD61B4F326D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLight_t03504821EF400C268A32470CB0ADACD61B4F326D_CustomAttributesCacheGenerator_lightTarget(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x69\x67\x68\x74"), NULL);
	}
}
static void ActionLookAt_tEC6DCD5F9E01B5D6B05309B802D65B2CEE6B724C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLookAt_tEC6DCD5F9E01B5D6B05309B802D65B2CEE6B724C_CustomAttributesCacheGenerator_freezeRotation(CustomAttributesCache* cache)
{
	{
		RotationConstraintAttribute_t4B4100789F0B127714CA267AAA8FB12D58524903 * tmp = (RotationConstraintAttribute_t4B4100789F0B127714CA267AAA8FB12D58524903 *)cache->attributes[0];
		RotationConstraintAttribute__ctor_m67FAF4934092044140A0E4EDF2B2571EC3C1495C(tmp, NULL);
	}
}
static void ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator_propertyName(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator_ActionMaterialTransitionValue_Execute_m5532403A5A5539119EFFC92D16650C435CE447DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_m63455629CDFC718B52F66A855AA973034CA1EC15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_m33F4E3AABA145A6F5929A9646A66A74F9B8EB3F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m921C4AE7D1431654297FBFF722D990452EAB020D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEBCDDDC6151242BB567F141352561DD344537C13(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m4D68358EA683E8DFAA8FB743BAD43BCC240A86D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionNearestComponent_t4E686BA50BC81FFEA4364FBCAE3C85D15EA5CD19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionNearestComponent_t4E686BA50BC81FFEA4364FBCAE3C85D15EA5CD19_CustomAttributesCacheGenerator_componentName(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionNearestInLayer_tFBC7BB322B958B7DAD90EE7D1F15D1B48E53E923_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionNearestTag_tE4A35B560DEC48C377B647DF9A6B71900844724F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionNearestTag_tE4A35B560DEC48C377B647DF9A6B71900844724F_CustomAttributesCacheGenerator_tagName(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionNearestVariable_t6F08629C9A33EDA8B55703A29E00685CA063D4B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionNearestVariable_t6F08629C9A33EDA8B55703A29E00685CA063D4B7_CustomAttributesCacheGenerator_variableName(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_storeInVariable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_origin(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionPhysics_t7CD4CF007DDACD414E4114E2EBD2A632D2F2DEA4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionRigidbody_t068293BF57EECB874C57681272E55CD1C80748FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionSendMessage_t3C26828701A22AD482B91AAACD1E571E090908FE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionSendMessage_t3C26828701A22AD482B91AAACD1E571E090908FE_CustomAttributesCacheGenerator_method(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionSetActive_t2BFAD8428D415685F61BF1BDBA50A4514CAC2007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTimeline_t57B789539A109DC9C8849DCEA3932A24B83FFCB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionToggleActive_t0B496A703B766C2B218003306E71CE8CFBAAE0C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTransform_tDC86403B1F26B8B40C30AF8F0A078EF9A4A12776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTransformMove_t85738713489E31DF2CEDD5AF60A1B8730F61B5A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTransformMove_t85738713489E31DF2CEDD5AF60A1B8730F61B5A1_CustomAttributesCacheGenerator_ActionTransformMove_Execute_m83A314D67419B65DB5ADD6C174738DFE15984D53(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_mBBC55362DD6F73D1A9D3FD8C3F815D57E473521A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_m3BE76413C35E97EE41BF887F5C94338A08F59274(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B2FDCD99FC7D00F3F2E45033E4D4E43C434EF29(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_m9BFD11409852AC030EAD6175AF52C6C04FF2797B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m826A920F08D5B9712D8CAD38A583A9014C784C76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator_ActionTransformRotate_Execute_m52E7014DC3C1A4BC3CDDBA1EF21E37AF4EE154C0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m3EA267B4AAEB1FB64D9098EE4ACCA34AEA590FF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m213250BD24E8EE8853B913051A787012D435EB5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0305DABCAE84612AD3F76206412BD342D52129E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m204D54BF27D7CFB6F425C2D20648D125078A38A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_mE4DFC63A85A78AD6793EA847AC75148919B971AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionTransformRotateTowards_tF44DDE238077CFEB5FC5988BC1DDDCCA645A8B13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTransformRotateTowards_tF44DDE238077CFEB5FC5988BC1DDDCCA645A8B13_CustomAttributesCacheGenerator_ActionTransformRotateTowards_Execute_m8C328A448FA212122A818FAEC0C3D3AC2404A146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_m0706F423CF92DB9367533BF6A9123319F8CA8278(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_mCD2F8FA04E201EF31D00ACA8DD462E0A1E22BB9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF72942B4B178CA78A6749390E9BC859525A6EA48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m398E925CAA656EED3C0632C1A3ACBD8F10B14364(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mBA9BBCAB06CC9DF538CC5EC1E17E21165FD96FA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionTrigger_t09CE1270612E276C08475D8EF7E52A26C0C06AE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCurrentProfile_tBE72EF851A3E216667BAF6C4F0FE38FA4A47564C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionDeleteProfile_t43873144A33841E2CA744092567406088E3E60C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator_ActionLoadGame_Execute_mCA4BCD88110DF8FFB57B75DA02CA1D13A1B9E82A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_0_0_0_var), NULL);
	}
}
static void ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator_ActionLoadGame_U3CExecuteU3Eb__3_0_m5D65A8280B7151F6FBAD8401F772A0B860F290E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mEF7EE534F5AB0E4EC07E539E821DA3AC95D970D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m2DDDC6B615D89AC51CF63297B2EB8A205C1AAE4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C3C7C7718E40C63F6851C0502FBCBAE5D51D10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m6110CA08FADF00FD94D352C8091DE1191313BC5C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mC29C82265D7A10E38E4D4595D194B45C4A17ABE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator_ActionLoadLastGame_Execute_m0661A69EA5997349140E8D5EB8F76E601B056394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_0_0_0_var), NULL);
	}
}
static void ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator_ActionLoadLastGame_U3CExecuteU3Eb__1_0_m03276802B4B26570E825CDA0F2E78DFE703FD2EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1__ctor_mDED1314A51DDE61865770C493D2D9CCDE42C4957(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_IDisposable_Dispose_mB03A5268D828F14A443D72BCD62F9977A7434C2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE706719587A7A7BAE947DAB178A404636438E62A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m0A98AD78FF4D9FDFAFB4E2B4ADC7E918ECE084B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m14B357E056A4AADB78A9CADBC779A46325788ACD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionSaveGame_t77E39E382C2C29FF3E9B619D141500A30E87905E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionSaveGame_t77E39E382C2C29FF3E9B619D141500A30E87905E_CustomAttributesCacheGenerator_ActionSaveGame_Execute_m5C49646C313E617CF557E9FEE2D5EBC3AAA9FA10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m1A294E66E9D46043E2E23F4544014588A8D604B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_m14FDC26EDECC814A10CAA9F29C23A76370143B6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF519EBD37B180B513D8F652C309B25B1F9E64536(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mF841C3F84566662D6B63615136CA3AD0C469B029(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m488D15C23530AC60B5EA48F0D808A137B7F2ECDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionLoadScene_tA8C68DF3AC67C2B7A83125DD42B7D7BC6A3B940C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionLoadScene_tA8C68DF3AC67C2B7A83125DD42B7D7BC6A3B940C_CustomAttributesCacheGenerator_ActionLoadScene_Execute_mE4830BE17B75DC6B0D7C331BA8B1D2D73A4821A3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mB38F8F46EAA249E49BF209D521CA8E4DAD872375(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m9A55C15113480F1DFED0E5D92EFCA663FAD44C22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0C73195701400504EFD358451A634E203F71E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mC95F035CC2C00DD21F7CEB6C10C00AC264472D05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m61B9FE124AD3E7B14B38A197435F1766A601DAC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionLoadScenePlayer_tDD31F6E771C9730CAF8D30454DA01AC843DE8B62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionUnloadSceneAsync_t3529F909FA7691CECF11F5A3A18E1AC187E60892_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionUnloadSceneAsync_t3529F909FA7691CECF11F5A3A18E1AC187E60892_CustomAttributesCacheGenerator_ActionUnloadSceneAsync_Execute_m221F4EA5967752F1CE262C88154BBE883F5C5D7C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1__ctor_m50099F295DA27DCA901FA8869D15AABEFB2A89F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_IDisposable_Dispose_m73C667B2CD0942FC8C1D621030F62DE6EBCF43EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD24FFC8179AF6C49CF69C41720A1CA67CCCC3FF5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m7A06787F5F87A17EDF4B9F7E89E5F121D17FAA6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m302C6CB72E7DC52A8CE4D1CF360309C511F79770(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator_ActionCanvasGroup_Execute_m99266FDCA2C58E3B774F0F5D01CE04A0755E06DE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t3790C575EDE836D40EE7346D5FA20B81177D2492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_1_t234E446225875D99E16977CEDE8A28A171FB589D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m27E44C3A9DF57ADC577B19802B5193AE67FF720F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m6EDEDC583396BC219AC77E7CC4519AFF08AA95E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DC9E2350D3BAFA324B15BADB658E25EC071426(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m9A9BC40719774A6E6D1F34F82FE21CF69C9A2B55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m2AF14DBD96EB0B156A6600D4C0B33F3BB952F55D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionChangeFontSize_t72EFE4D913DD9C6BDF736BF2DEA6AFEE0413DFDC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionChangeText_t77F06EABAB40F6CE03852DF537BA1C3916BCB584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionFocusOnElement_tA73263034A9BF20A0C4E9254C6AF88907DF11827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator_ActionGraphicColor_Execute_mB50BCF17FFC8C783B2CAC5AA6999EC69282DBC60(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tAC7C769C854FA2327FCEE0E5FAB47DC94B93CB89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4__ctor_m719692616249D8B1B9A18A4639A2BEBCF839B63E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_IDisposable_Dispose_m322211ED0B980FF930875657D598C03697632275(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F499A2C7AEE9F7CAFC35900478C44882351C41B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mC64DC9F5F707FDAA833235FBCA5AB402F4B49918(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mAC358A6CCA560BCC9EFA484A7E9CE5F4D47CB979(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionImageSprite_t18EB88A1CA9F746D9867E1093B785D7E2802C6B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void AudioManager_t7C65AA205762F129C1F3AC7B700F3B2A350BC5A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x61\x6E\x61\x67\x65\x72\x73\x2F\x41\x75\x64\x69\x6F\x4D\x61\x6E\x61\x67\x65\x72"), 100LL, NULL);
	}
}
static void ConditionPattern_t582CFD24E498EB990A32A3658F1C30AB449B508B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionSceneLoaded_tE661F7DEC67FD493F744B6233883790DA9D29882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionSimple_t6C40610134B5FE7B6B5E93C76FA340347C973171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionInputKey_t988EEA390DFDE0AF2C65AEB39B74F71C56223130_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionInputMouse_t59C49393441E2F362650E9411525FFF546C64422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionActiveObject_t6E00476234FCDB730BE2A5B81D72D0E88EDBB95B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionEnabledComponent_t6D9FA8BC71B2D16B825B71D9E55CB982ADFDA8EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionExistsObject_tE6138C9BDD87D252F37149C4FD569D3B49008DED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionRigidbody_tE9A1A4936B3117EF12D6D90AAE28C21583028192_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionTag_t4F9E75EFC48A64AD7312675F1FA100918BBB796A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionTag_t4F9E75EFC48A64AD7312675F1FA100918BBB796A_CustomAttributesCacheGenerator_conditionTag(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void ConditionHasSaveGame_tF0A4373A70A076029A13905E99F9C8B05BFD09ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void HPCursor_t07495835A900993CB98DDAF4EC04C40C7E8A6A0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void HPHeadTrack_t093C66392B8B099D01F90B05F2D29F8AFC9A1459_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Data_t00BF099EF0B4878CFFFD85FEDC8F7DB6C9A2E8CA_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 20.0f, NULL);
	}
}
static void HPProximity_tAC941D4DA97DDB8D42ECEDC7BDB6CE3B4AE1448A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Data_tD6710293DD530E89D7C899221995C0C1847BB8AE_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 20.0f, NULL);
	}
}
static void Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator_trigger(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator_Igniter_ExecuteTrigger_m5913E13D844E3833A79E888FA2F887EAA1BDB204____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_withTag(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_withTag(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterCursorRaycast_t7A0832FA76E83AE02D77D147EABF8C9626B89F62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterCursorRaycast_t7A0832FA76E83AE02D77D147EABF8C9626B89F62_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator_eventName(CustomAttributesCache* cache)
{
	{
		EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3 * tmp = (EventNameAttribute_t38EF142AD39F90E9321E7A79BAD28C29F3505EE3 *)cache->attributes[0];
		EventNameAttribute__ctor_m41852A61BDA5AB2D287649265057870A8DC92B15(tmp, NULL);
	}
}
static void IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator_storeInvoker(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterHoverHoldKey_t163AF8254FCF70B343CDFE1A415877CCD28D420F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterHoverPressKey_t84E025F464493F7320EC8DEC3C5652BCAACE6920_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterInvisible_tD3D7101FF790017ECD8DCB2DA4DD4442C0798886_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterInvoke_tE99D198956B0C099612186021C2C1D910A5F28AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterInvoke_tE99D198956B0C099612186021C2C1D910A5F28AB_CustomAttributesCacheGenerator_storeInvoker(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterKeyDown_t8A4FD67226984359051AB76E1BC8251CCA1AE93D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterKeyHold_t6422AA78240B425AFB5AF5FF19DCD203F7E85CD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterKeyUp_t83F174654F37D98A64B56776629EE1FB17F7FAD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseDown_tB4B568DC4A42585793E3137418F3662B8C68FA4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseEnter_tC6546DD7558D98F7BC5D07517505325ABF039705_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseExit_tD924AF94D5A5474C4AB05B01708D19AA34C8FBCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseHoldLeft_t0363275354C0E0AEFE355B9541975AFFD035BFE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseHoldMiddle_tC6BBD4DBFB96A8F1EF1A1FB4590B1228B76A84E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseHoldRight_t066C8A4FFA07E6D97C349D4EC696643B2ED11386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseLeftClick_tE15BFB6E24F143B23DB23763712F61B48D8F1717_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseMiddleClick_t73EC98FBEF9CE4E8FA8EF95828B3F91622D88708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseRightClick_tED3EFEE63C320CD7B9236EE8C63231341A224158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterMouseUp_t80014B44A0995954B5C3636384C9EEC4F5FDB918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnDisable_t9211D525F51B5CAE110E99E06EAC64E217BF28D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnEnable_tDC0B302211F9F72F5F6932AB45318BC093869BDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnJump_t2C1A51B106B504CB6D3217F142F0844953827CC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnLand_t152974D5552AFE38AF1C756E280F902169905ED7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnLand_t152974D5552AFE38AF1C756E280F902169905ED7_CustomAttributesCacheGenerator_storeVerticalSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterOnLoad_tB7DBC9B30CDAFB0B01CF881A4CA32A821AEB3B30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnLoad_tB7DBC9B30CDAFB0B01CF881A4CA32A821AEB3B30_CustomAttributesCacheGenerator_IgniterOnLoad_DeferredOnLoad_m64C11C5F16BD9E5EAEC2D50D48E978F52A238DC6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_0_0_0_var), NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3__ctor_m5C8E1E3AA4B98F20105756C6130E53147A68BD9F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_IDisposable_Dispose_mB475A3D3A9F97EDC5FD87B397BC25ECBDE3760FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BF406ACFB64F6A8D3582DD5C4686BD52C2EC93E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_Reset_m5E740051AF8A54914754A26FF03008A1B83E4C1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_get_Current_mD8F7F0E8B01AF6EC3C85ACB8B40A7FF6242DEF73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void IgniterOnMouseEnterUI_t060C12E38BA5D9878667A94FBF73050ED42332A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnMouseExitUI_tD27E7DE56FDF877B351BE6CB42A8F273F2D3D911_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterOnSave_t299E08DCF64E7D58730F2DBB0A00D75C3A25BF00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterPlayerEnter_t3510146B1CBFD418ACE71FFC89C86A49DFCAD78F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterPlayerEnterPressKey_t87AB24DD7AFA55CBFBA7F62C480F90A5046B1988_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterPlayerExit_t578B5F596CE614B6BBD87EB1F3E64D3C3ABEF2DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterPlayerStayTimeout_t33CC63F34C4E0CB4471FF2D3A4436DBC74A73AD2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterRaycastMousePosition_t2F9FDEE6A57B8715831675B5BFE5A2F84E791FD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterRaycastMousePosition_t2F9FDEE6A57B8715831675B5BFE5A2F84E791FD8_CustomAttributesCacheGenerator_storeWorldPosition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterStart_t9A64ACE2AE4223AB096538DAD4979043A18E26B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_objectWithTag(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_objectWithTag(CustomAttributesCache* cache)
{
	{
		TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 * tmp = (TagSelectorAttribute_t059DCDC570E66AC86C372E24E50A254A4C0AC442 *)cache->attributes[0];
		TagSelectorAttribute__ctor_m40114022418F97440D6D12B74F8C20106FFAD5FE(tmp, NULL);
	}
}
static void IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[1];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator_storeSelf(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator_storeCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void IgniterVariable_tB145251A13724006BE57EAA9D42E6FDABF6734BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterVisible_tECB7F6EA7F17BC6D1305529971D6DFD2B1720D1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterWhileKeyDown_t8B12AEC42C645D88A0BDDE3DC1C6B499AB13490A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IgniterWhileMouseDown_tE2ACE93F9BD33FECE6B1C3EC581866DB4168E617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void TouchStick_t345F706DE9C5E257BBE3C47BB6672ED654F18EAC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void TouchStickManager_t3E25C081E2EFD3F7A5998AE514B0DFFAB31D85D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x41\x63\x74\x69\x6F\x6E\x73"), 0LL, NULL);
	}
}
static void Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_runInBackground(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x6C\x79\x20\x6F\x6E\x65\x20\x66\x6F\x72\x65\x67\x72\x6F\x75\x6E\x64\x20\x41\x63\x74\x69\x6F\x6E\x73\x20\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x63\x61\x6E\x20\x62\x65\x20\x65\x78\x65\x63\x75\x74\x65\x64\x20\x61\x74\x20\x61\x20\x67\x69\x76\x65\x6E\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
}
static void Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_destroyAfterFinishing(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x65\x78\x65\x63\x75\x74\x69\x6E\x67\x20\x61\x6E\x20\x41\x63\x74\x69\x6F\x6E\x73\x20\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x6F\x6E\x6C\x79\x20\x6F\x6E\x63\x65\x2E"), NULL);
	}
}
static void Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_Actions_Execute_m028F25C515D5D301413DB5E45F9C970A69C3387A____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_Actions_Execute_m753E2E2998E02C5F2E806D59A36F5FB78C9A753D____parameters0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73"), 0LL, NULL);
	}
}
static void Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator_Conditions_Interact_m730603257F8CBFDE70A458D95E42F1374675F25A____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator_Conditions_InteractCoroutine_m28D3C22682852BF779FC1C09E3A86331BB9EEB64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_0_0_0_var), NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5__ctor_mD95C463FEDE49E3E955F7F9A1BB8C951B75C3122(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_IDisposable_Dispose_mF1F4A66B05E74B40A8654C03EC212E38000C6FCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BCBBC8CAA0E427B467E5E183B7D934C8E4D42B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCFDA796A50A6135EA71F3FE4E5BC9E44C478C3E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mE8C1D7E5CA1FBBB04F671E6939B8BE5D7209572C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Hotspot_t6BD27EDB919139A4B5022B037854D39E6F2ADAAE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x48\x6F\x74\x73\x70\x6F\x74"), 0LL, NULL);
	}
}
static void Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x54\x72\x69\x67\x67\x65\x72"), 0LL, NULL);
	}
}
static void Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_CustomAttributesCacheGenerator_Trigger_Execute_m1A6FC0B07CE3CC98139220B8963BE093CF3F0EFE____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ScenesData_t17351779447C737995FE2021DC3AD84A983D2324_CustomAttributesCacheGenerator_ScenesData_OnLoad_m9DD2B0255D05F2CC56291A43DA68F1D794D9B8BA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_0_0_0_var), NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8__ctor_m2623EB815DC1D76469AE888CCFC58B3EC04319B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_IDisposable_Dispose_mDF1AEC6C64F34D03F84D44928EF2E34986C0CC9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC76155CEECA7B46580F2B1A8028F9F301BCDDAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_IEnumerator_Reset_m56DC36C13893D1FB4DF0C015C520BFE5A539A182(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_IEnumerator_get_Current_mA8ABABF1674E47A2E404A53461DBD8AF95075ED6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GameProfileUI_t9E06E0608F31DBD5BAF077DA615F6BA4E87FDA1B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x47\x61\x6D\x65\x20\x50\x72\x6F\x66\x69\x6C\x65"), 100LL, NULL);
	}
}
static void RememberActive_tC35DAFB65F7047AD72E03E57F330F2FBD0D19937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x52\x65\x6D\x65\x6D\x62\x65\x72\x2F\x52\x65\x6D\x65\x6D\x62\x65\x72\x20\x41\x63\x74\x69\x76\x65"), NULL);
	}
}
static void RememberBase_tC9496539ACD5CF521536F394CF611151CFA7000C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void RememberTransform_t520702CFAB253BEFEB32514CC6E646D3BBFD459D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x52\x65\x6D\x65\x6D\x62\x65\x72\x2F\x52\x65\x6D\x65\x6D\x62\x65\x72\x20\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x61\x6E\x61\x67\x65\x72\x73\x2F\x53\x61\x76\x65\x4C\x6F\x61\x64\x4D\x61\x6E\x61\x67\x65\x72"), 100LL, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CIsLoadingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CIsProfileLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CActiveProfileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CLoadedProfileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CsavesDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_IsLoading_m48BA1B1C94B4C49EDD9AE123CB172588AB0F677B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_IsLoading_m72592D60732CD135C5A77D6F0983FB422F577C07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_IsProfileLoaded_m5284D5C2C5C09F8DD70C4F0F655C382F849A95FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_IsProfileLoaded_mEF54E5023DF9D2A82D2B1B725486EACABE8A93FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_ActiveProfile_m9738798B39E0A1677771A2D85620B66F7D9E4D6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_ActiveProfile_mE1B1C4AE9C7C893FF128D3693F0AECB79E4AA4C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_LoadedProfile_mB06CD36F75FFBC8CD6CF6EAA8A9D49B52AAC186A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_LoadedProfile_mA4FCE167840C7250ED217EB4D9B03F89C7D4EFC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_savesData_mF5D63C886C2D47F216FA1BBBF71EA45CB5E825AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_savesData_m25C89E92AB5C3540BA52D7A12A63801808C510E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_InitializeOnLoad_m10EFC6CEB1BF1717756D029431963EDF4A3AB8E0(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_CoroutineLoad_mA7DDBA766A8C4F7A96D4F726AA978178F4FE9CF0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tEE0F1ECD9D55609B65DD3B41840D9ECD27A441B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41__ctor_m3223DF3BDBA925B9C01B1075D3CD3347CD43AEC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_IDisposable_Dispose_m9988DEBB87C0389FF609CC218B64C147B325A8FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m801AB1F500A3A2998AD56254068EE4CFEF1AFC57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_Reset_m1C07BD4300758CD834338BDDECA0875E978F353E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_get_Current_m5193714FB64E1B8E2DD8DACEBC03DD5545EF39D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ButtonActions_tBD805CDFD93B7EAB55ADC0EDC43018A193868CAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x42\x75\x74\x74\x6F\x6E"), 10LL, NULL);
	}
}
static void ButtonActions_tBD805CDFD93B7EAB55ADC0EDC43018A193868CAA_CustomAttributesCacheGenerator_ButtonActions_OnFinishSubmit_m913CD3FD4E3EDFF039CFE2655DE2FDA322EE6570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_0_0_0_var), NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6__ctor_m893E2EF3FB1B81650DC4D1CCFC1FCB37398F45EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_IDisposable_Dispose_m2B5E0BBD91BB5335BF4D0D72AD2B8E0D12ADEC7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE497123EB4BC9F432B4072E2D058559767136CAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_Reset_mFB604330DB8D7CD8B5C768C549F2946B73F59377(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_get_Current_m43798DF5AB19460ECD104AF7F4F2FC05ADB14768(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InputFieldVariable_t7FC56A2C5628D482E7352051D83321AEFA3AB9CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x49\x6E\x70\x75\x74\x20\x46\x69\x65\x6C\x64"), 10LL, NULL);
	}
}
static void InputFieldVariable_t7FC56A2C5628D482E7352051D83321AEFA3AB9CC_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void SliderVariable_t19D596997E54C9562569B0C66C2819DBDEC1763E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x53\x6C\x69\x64\x65\x72"), 10LL, NULL);
	}
}
static void SliderVariable_t19D596997E54C9562569B0C66C2819DBDEC1763E_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void SliderVectorVariable_t6902A88BAFA868F644F864155C34CBB7D717D620_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x53\x6C\x69\x64\x65\x72\x20\x56\x65\x63\x74\x6F\x72"), 10LL, NULL);
	}
}
static void SliderVectorVariable_t6902A88BAFA868F644F864155C34CBB7D717D620_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 5);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ToggleVariable_t6CD69B4A50AB5EF2B421F71EE496EED3B0088B13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x55\x49\x2F\x54\x6F\x67\x67\x6C\x65"), 10LL, NULL);
	}
}
static void ToggleVariable_t6CD69B4A50AB5EF2B421F71EE496EED3B0088B13_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 3);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void CanvasRenderMode_t205695765E012614AE119D5879699651BD769C5E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var), NULL);
	}
}
static void GlobalID_t3EE8A0E42F455874624AE78D40341287BBEE4BC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void GlobalID_t3EE8A0E42F455874624AE78D40341287BBEE4BC9_CustomAttributesCacheGenerator_gid(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoroutinesManager_t3CDEA6D5BD9DC6CA25DB1240EF110C25B4D48D85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void EventDispatchManager_t6C18B35DC50FA0949C4F1E70BBF105E1725D520D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void EventSystemManager_tB0DAB954362D25F953E8ADD214D1968DC8F380E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x4D\x61\x6E\x61\x67\x65\x72\x73\x2F\x45\x76\x65\x6E\x74\x53\x79\x73\x74\x65\x6D\x4D\x61\x6E\x61\x67\x65\x72"), 100LL, NULL);
	}
}
static void GameCreatorStandaloneInputModule_t304FE6C948A67D2C9C37DAAD8BCE7145FA9346F0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator_keys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator_values(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Singleton_1_tCC578427A4C18FBE3462F681D0BA3220CFA6F12C_CustomAttributesCacheGenerator_Singleton_1_DebugLogFormat_mEA821C32B38BAB9F92B41475B4B6FFC881BF4CDA____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TimeManager_t8401071402372C4B59931805FFC97CB377E59EB2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_prefabSimpleMessage(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62\x4D\x65\x73\x73\x61\x67\x65"), NULL);
	}
}
static void DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_prefabTouchstick(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x75\x63\x68\x73\x74\x69\x63\x6B"), NULL);
	}
}
static void DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_saveScenes(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x73\x61\x76\x69\x6E\x67\x2F\x6C\x6F\x61\x64\x69\x6E\x67\x20\x61\x20\x67\x61\x6D\x65\x20\x73\x74\x6F\x72\x65\x2F\x72\x65\x73\x74\x6F\x72\x65\x20\x77\x68\x69\x63\x68\x20\x73\x63\x65\x6E\x65\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x77\x61\x73\x20\x69\x6E\x3F"), NULL);
	}
}
static void DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_provider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator_Clause_CheckConditions_mC801467291AC7571A50985555DB38737062BC8F7____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator_Clause_ExecuteActions_m0CFB6797D8E897ACD15B1E4097B5CCFF6C06468B____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_InstantExecute_m512502AA6B9A94C8B5563F89D270D82275BD61C6____parameters3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mC7BC452CD9CEB42F6B5FF76A1653C0318E4E7F9F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_0_0_0_var), NULL);
	}
}
static void IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_0_0_0_var), NULL);
	}
}
static void IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE____parameters3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m7DDC9C9F4F6271CBC115EB695C09CD7020514C91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_mCE34EB7AF956AF35D260652DAA9487875F5E5E18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD451C8F90AB42372F393CA15C9D4C30110852768(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_m15914DD547004FBF1D2595DB46564433345D5090(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m9F64445ABE6D9783AD5D9537B7C8AAA82798ACEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mC5BAFE1C6CE5BF49D9685E6382F0D67665BA1271(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_mD044F79A0BF7913EA3A7F7D88B63DC0F6676553E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD2403DC5621D9B93CD571F03569AE0A49546A16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mB8CD42CE43D0BE19240A03A82805AAE7BDB19368(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m0810B2FE029F9C067DCCFD795ADD587BC8E9B744(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_Execute_m58A609A93DAF9BB26DDB4996AC23A068450D6D6B____parameters2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_0_0_0_var), NULL);
	}
}
static void IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE____parameters2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_U3CcoroutineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_U3CresultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_get_coroutine_m43BF4DBCBD32FA5208B4AC6690372328638225F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_set_coroutine_m84480EA36BEBF51030B497DC4EC7E7CDC3C93F64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_get_result_mC8B5641A49CCF3B6CDE09E713E41E47D51B78BD5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_set_result_m5C7494A81C65703C4CA6CA479A3196B72508332B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_Run_m5E2B10DAD3C70CE29D7C2785F8A0C216EFB90C40(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_0_0_0_var), NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10__ctor_m161D5DDA96EC9824164EAA25CFBA7959B31C271C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_IDisposable_Dispose_m4CB135F3D4224B9BAED517629FE906CE77A206F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96EC009DA64F1B02F4571CF2C15B947E77829245(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_IEnumerator_Reset_m1061AF69C60892787419DC2081CFB2D051A34488(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_IEnumerator_get_Current_m7AF120E5C89DA314DC5E5AB7794A4F40258E1F0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7__ctor_m83AD0BC314DA34572FA3D490CE3253BFB7F5FA68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_IDisposable_Dispose_mA37486D7327AA40D8CC84D0492AB0A0121403CB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA7ABC9F3A7C54472C63995C362F345FDCC8862B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m81F9FA9DDF2680024453C428C9D7EEF0CA4F7ED5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m4A0F6B20C3E45CE35088B56C0E871F65FB617DEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ICondition_t52D9BDEF26917F52FA8650B20FCF835A9625551A_CustomAttributesCacheGenerator_ICondition_Check_mEC8DAE0A9AEEAC0944D71A55F212091BC90562C0____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void IConditionsList_t572443118DEF6B658B137E448305AC95B87BF172_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void IConditionsList_t572443118DEF6B658B137E448305AC95B87BF172_CustomAttributesCacheGenerator_IConditionsList_Check_mE39A03DEA4F39E41F50EF48BEFA52F265AF7CE48____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_component(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73\x20\x72\x65\x71\x75\x69\x72\x65\x20\x61\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72\x20\x74\x6F\x20\x62\x65\x20\x67\x61\x74\x68\x65\x72\x65\x64"), NULL);
	}
}
static void ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_origin(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_listVariables(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tE7EADE9DD7ACD7429227C9E984BA38F18F3CF54D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator_origin(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator_listVariables(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tEDE96F7E2FF87C142781D10E5D19303E841153C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionVariablesAssignBool_t715FD74CB5522A0755A3996A2CE2C4EFB930C658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignBool_t715FD74CB5522A0755A3996A2CE2C4EFB930C658_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 3);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignColor_tF1D40B1E75EB8841F1CA6D2803AB36045E39DA5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignColor_tF1D40B1E75EB8841F1CA6D2803AB36045E39DA5C_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 4);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignGameObject_t5ACF6A1C73639C0D33CA1E467ADDD9CA57B79328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignGameObject_t5ACF6A1C73639C0D33CA1E467ADDD9CA57B79328_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignNumber_tCAD6F1431B547A3197D8D86D47C380BD25B951CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignNumber_tCAD6F1431B547A3197D8D86D47C380BD25B951CF_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignSprite_t6C3EFC56D37E212B5FD644FDB16BAB7B82572E38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignSprite_t6C3EFC56D37E212B5FD644FDB16BAB7B82572E38_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 8);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignString_t5FE23744A76D2761D10C7AC914568461C868738F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignString_t5FE23744A76D2761D10C7AC914568461C868738F_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignTexture2D_tCDA2092CCC64A81C1A66982186F9865A07B0C58E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignTexture2D_tCDA2092CCC64A81C1A66982186F9865A07B0C58E_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 7);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignVector2_t95829F4413220344010916387F0585045D97348C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignVector2_t95829F4413220344010916387F0585045D97348C_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 5);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesAssignVector3_t75B13EABEDE215B76CA64F47499A951A3A498C03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesAssignVector3_t75B13EABEDE215B76CA64F47499A951A3A498C03_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionVariablesToggleBool_t9E44F79B36DABA8251EE1AB7BD72A36FB82FE01F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionVariablesToggleBool_t9E44F79B36DABA8251EE1AB7BD72A36FB82FE01F_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 3);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void IActionVariablesAssign_tC3742C148AA3C7B2145B74F1FF0BFF7CB9E575B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariable_t74D25129AFB758A3F3CED2E764C0424CEC3ADD2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableBool_tEB9F7F35EA93AC362259EDAE1759A589CB7886C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableBool_tEB9F7F35EA93AC362259EDAE1759A589CB7886C0_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 3);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableColor_t3FE9D123A8F03BB488E4FECF8667BF25E528B6BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableColor_t3FE9D123A8F03BB488E4FECF8667BF25E528B6BE_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 4);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableGameObject_t4A90CB9A8CA33240D6020F75BA2DC57748ADEF2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableGameObject_t4A90CB9A8CA33240D6020F75BA2DC57748ADEF2A_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator_compareTo(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ConditionVariableSprite_tCEE0AB106FFFD19105D017DC2AB6F701E45327E4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableSprite_tCEE0AB106FFFD19105D017DC2AB6F701E45327E4_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 8);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableString_t38B161884AF2E8C551F5AF7012ECCFA2C2E4ACA6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableString_t38B161884AF2E8C551F5AF7012ECCFA2C2E4ACA6_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableTexture2D_t90CC4FE1CAB6978966D8AE1954B9D0260AB34C31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableTexture2D_t90CC4FE1CAB6978966D8AE1954B9D0260AB34C31_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 7);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableVector2_tBDBA87B67063C0589D4F2CE5B596828EEB5E3F12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableVector2_tBDBA87B67063C0589D4F2CE5B596828EEB5E3F12_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 5);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ConditionVariableVector3_t085E45E174F475E2571B9BEE687ECE58EAF8D802_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ConditionVariableVector3_t085E45E174F475E2571B9BEE687ECE58EAF8D802_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 6);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void U3CU3Ec_tC6637A83649FD2B2366A75D474EE4016A36B871F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CcurrentCharacterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CcurrentTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CnumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CidentifierU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_currentCharacter_m0F54B6D1EDB6099B9702A01A5B9834B74EC300A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_currentCharacter_m827A7EB986EA3DC7D3A2226918C13B18E732255E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_currentToken_mCD8FA828E6CC412CF1DFE31196E63C2FEB6B5F00(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_currentToken_m734975EE88A7476FFC961BE9C066FEF54FC7A574(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_number_mE49477FA7BB77D7AA94B3659689F5FB6AF0FD4B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_number_mEAE023F2196EB4BA001B1120783D84C0E7F29C47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_identifier_mCE413649912A31A44093A6BC8B0CC673405ADF79(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_identifier_mB59DC360B4A15FA7675821A2D0F05F6DD3591A0F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HookCamera_t44CFF2D561623323C70F70ED3EB953812CFFE47C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x48\x6F\x6F\x6B\x73\x2F\x48\x6F\x6F\x6B\x43\x61\x6D\x65\x72\x61"), 100LL, NULL);
	}
}
static void HookPlayer_t4F161DB71349F6FBF5B871438C2E3D1E059A7E4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x48\x6F\x6F\x6B\x73\x2F\x48\x6F\x6F\x6B\x50\x6C\x61\x79\x65\x72"), 100LL, NULL);
	}
}
static void ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_targetOffset(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_pivotOffset(CustomAttributesCache* cache)
{
	{
		IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 * tmp = (IndentAttribute_tA7B343E1FBF6B81A93C9B56C29279B46C0242AF0 *)cache->attributes[0];
		IndentAttribute__ctor_m0678C2E8B621F8FD9E0B390CFF6E49CECC57D87B(tmp, NULL);
	}
}
static void ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_ActionAdventureCamera_Execute_m4B5B9A7099FE47828DCE6EB5948F385DA040162D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_0_0_0_var), NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11__ctor_m0200F3F5D3D966175D9581F749E6B5A835BBA109(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_IDisposable_Dispose_m00942BBE5812FCCE5A1F9497036D58AE8C9E927E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68C82A5E23D0F81B0514376497701F2EE9F32A67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_IEnumerator_Reset_mDE04DF8E62B2F75FA258B59AB333597FBC2E57A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_IEnumerator_get_Current_m421DCFCEB82C325052928E10715A6D19482F238F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ActionCameraChange_t9F42D1CC19931750D54554B10DA4CB02F96185E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraChange_t9F42D1CC19931750D54554B10DA4CB02F96185E8_CustomAttributesCacheGenerator_transitionTime(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 60.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x30\x3A\x20\x4E\x6F\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x2E\x20\x56\x61\x6C\x75\x65\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x2E\x35\x20\x61\x6E\x64\x20\x31\x2E\x35\x20\x61\x72\x65\x20\x72\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64"), NULL);
	}
}
static void ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator_variable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0* _tmp_0 = (DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0*)SZArrayNew(DataTypeU5BU5D_t3BE4F08E742404C1173D0162BC18E828F928F7F0_il2cpp_TypeInfo_var, 1);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 9);
		VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD * tmp = (VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD *)cache->attributes[0];
		VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639(tmp, _tmp_0, NULL);
	}
}
static void ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator_transitionTime(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 60.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x30\x3A\x20\x4E\x6F\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x2E\x20\x56\x61\x6C\x75\x65\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x2E\x35\x20\x61\x6E\x64\x20\x31\x2E\x35\x20\x61\x72\x65\x20\x72\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64"), NULL);
	}
}
static void ActionCameraCullingMask_t525E309773192422FF4DE41248CBBCE26F271101_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator_dampingTranslation(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator_dampingRotation(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ActionCameraRotate_tD3A4862E67C35E9320D3249D72A8CBD54D950E9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator_roughness(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 30.0f, NULL);
	}
}
static void ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator_magnitude(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator_roughness(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 30.0f, NULL);
	}
}
static void ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator_magnitude(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator_period(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x61\x64\x20\x42\x6F\x62\x62\x69\x6E\x67"), NULL);
	}
}
static void ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator_modelManipulator(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionFixedCamera_t27A944771C1575027993778C49DB8048DD430A9A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionFollowCamera_t831FC58553E436DDAE3219A2B342E1A079B1408E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionRailwayCamera_t6E678631C1E84BF05E2A218B3F11621E3318A1B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator_anchor(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void CameraController_tE94B48FA4B37E6AEAD0FE1AC523656F750FEDA5B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HookCamera_t44CFF2D561623323C70F70ED3EB953812CFFE47C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(HookCamera_t44CFF2D561623323C70F70ED3EB953812CFFE47C_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x61\x6D\x65\x72\x61\x2F\x43\x61\x6D\x65\x72\x61\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), 100LL, NULL);
	}
}
static void CameraController_tE94B48FA4B37E6AEAD0FE1AC523656F750FEDA5B_CustomAttributesCacheGenerator_cameraSmoothTime(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CameraSmoothTime_t89DBA3D6521AB15643622EFB19EE41561B7D64C2_CustomAttributesCacheGenerator_positionDuration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CameraSmoothTime_t89DBA3D6521AB15643622EFB19EE41561B7D64C2_CustomAttributesCacheGenerator_rotationDuration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CameraMotor_t24EE44E47184D418A0C007C8156C95DB0EBDD373_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x43\x72\x65\x61\x74\x6F\x72\x2F\x43\x61\x6D\x65\x72\x61\x2F\x43\x61\x6D\x65\x72\x61\x20\x4D\x6F\x74\x6F\x72"), 100LL, NULL);
	}
}
static void CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator_maxPitch(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 180.0f, NULL);
	}
}
static void CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator_zoomSensitivity(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 20.0f, NULL);
	}
}
static void CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator_maxPitch(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 180.0f, NULL);
	}
}
static void CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator_smoothRotation(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CameraMotorTypeFixed_t3E135A9FC2B61445AA499EA4AF3ED71EB63E18CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeFollow_tB02465B88B2B99D98AE20C4C8393AF8FDF5A6401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeRailway_tE560EE57D4F7180C259B9864013697E359CF0F69_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeTarget_tA215C32844DEC5C76152138F5A2B3E49DF42816C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CameraMotorTypeTween_t7D4DAD354A7A1B9DC3251F78753B79EAA0DE725D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ICameraMotorType_t33A71BF290B80E05CFDD5C69B95B8503E67AFEB7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void ICameraMotorType_t33A71BF290B80E05CFDD5C69B95B8503E67AFEB7_CustomAttributesCacheGenerator_fieldOfView(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 179.0f, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[1021] = 
{
	ActionListVariableAdd_tE97C2318D01988C34C32D7803FD63CCBE48C5345_CustomAttributesCacheGenerator,
	ActionListVariableClear_t0CA3D3BDF17016B08E7AF0B55243BF9B7A6DF6E7_CustomAttributesCacheGenerator,
	ActionListVariableIterator_t2245EB96586671E181315DB90CE2B697C3D891AC_CustomAttributesCacheGenerator,
	ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t22946E06EA26E15F68CF21A5C4D4683C2C4D3636_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator,
	ActionListVariableRemove_tF8372B6EA9572A4565720BEEBACB9F8A9CD52448_CustomAttributesCacheGenerator,
	ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator,
	ActionVariableDebug_t4D9084BB18314789B9BEE917E1637D655FE47FA2_CustomAttributesCacheGenerator,
	ActionVariableRandom_tE225EE7232A93F7C812A504BE92B99F7324000DA_CustomAttributesCacheGenerator,
	ActionVariablesReset_tE2CD7C74F64A562086D82B2F7A1BBC31D088F554_CustomAttributesCacheGenerator,
	ActionVariableAdd_t459299F242296854E096A69B18927552B8B04AD7_CustomAttributesCacheGenerator,
	ActionVariableDivide_t6E9F9EFF1D807EE1A3F34873CC3297984F7E6270_CustomAttributesCacheGenerator,
	ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator,
	ActionVariableMultiply_t81254610E1EC59EF7A12BE6388A11802E3E0231A_CustomAttributesCacheGenerator,
	ActionVariableOperationBase_t48A1078C98B26F63F8C064F1AF4EED1C372D3B02_CustomAttributesCacheGenerator,
	ActionVariableSubtract_t72261CD2C98212DA97316DA1AE4EE73896E65025_CustomAttributesCacheGenerator,
	ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator,
	LocalVariables_t7EEE0938AAE686CD26E6F0600EAA99FF319B1529_CustomAttributesCacheGenerator,
	TextVariable_t9D2D43DA7989AEB960E0559A60987D61B5021C86_CustomAttributesCacheGenerator,
	ConditionGameObjectInList_t757559B04B7C567170360111079485E60D7676CF_CustomAttributesCacheGenerator,
	ConditionListVariableCount_t0B99E772D6DC0E8148062A9281F5EE38BABEBB40_CustomAttributesCacheGenerator,
	MBVariable_tB349AB541406D4F038A3EDCC0CBA3854D57C4D72_CustomAttributesCacheGenerator,
	GlobalVariablesManager_t78138F07EAEBEC5E10FE8F0E36BACEAF77E223BF_CustomAttributesCacheGenerator,
	ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t327F970DAFBAAF04F67C01EE0D1156582B2E0633_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator,
	ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t4B19567383FBBC383EEE6CC58C53834828CD9213_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator,
	SimpleMessageManager_t61808F8E20DF4106250E824C6A9D538912E69881_CustomAttributesCacheGenerator,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator,
	ActionMeleeAddDefense_tF9B23CC96635AC449D9E64986642013DA9B5C896_CustomAttributesCacheGenerator,
	ActionMeleeAddPoise_t00B2AEFAE5FE6BB9A393E24ECAC225CED6EA992D_CustomAttributesCacheGenerator,
	ActionMeleeAttack_t1102798C754D0AE957F9B1789206D019A1E1483D_CustomAttributesCacheGenerator,
	ActionMeleeBlock_t81A6D6135ECF5521AD061E79A45DA91B6FE61400_CustomAttributesCacheGenerator,
	ActionMeleeChangeShield_t749B62201A9BF258DCBFF2416ACD339EBF951A21_CustomAttributesCacheGenerator,
	ActionMeleeDraw_tEF384443AF276D6FF300C0D1B1180C638AAA03EB_CustomAttributesCacheGenerator,
	ActionMeleeFocusTarget_t7286DE047091458BCE6020BC766BE9104E8CA151_CustomAttributesCacheGenerator,
	ActionMeleeSetInvincible_t63EDBFD5C0E72F452C002621CE9E7ECA0ADDBD7E_CustomAttributesCacheGenerator,
	ActionMeleeSheathe_tECF149A1FEDD4B9E695693B9265D214935143161_CustomAttributesCacheGenerator,
	MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator,
	MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator,
	MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator,
	CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator,
	ConditionMeleeArmed_t00E599B647974D4641746A1FF70201E321DB2E85_CustomAttributesCacheGenerator,
	ConditionMeleeCompareDefense_t672E338E78E0660A5DFA0B291ADA6F31587FFC7E_CustomAttributesCacheGenerator,
	ConditionMeleeComparePoise_t3B8CA0E3067DFF3659FC05681306522211EC2CCE_CustomAttributesCacheGenerator,
	ConditionMeleeFocusing_t3D34FD8161500B32691C0EAFF207E5B26C88EC19_CustomAttributesCacheGenerator,
	ConditionMeleeIsAttacking_tAD530ABBA66ADD5AA2C50F1DFDA1082BCF7ED2CC_CustomAttributesCacheGenerator,
	ConditionMeleeIsBlocking_tB030896C16730D2021014D6DA3D960510FFDB694_CustomAttributesCacheGenerator,
	IgniterMeleeOnAttack_tFEB32CECDA9FF3BE53A464420ADD6FA742E0CDAC_CustomAttributesCacheGenerator,
	IgniterMeleeOnReceiveAttack_tC73836BB57D725D78FA798B32E5A96BFA1197313_CustomAttributesCacheGenerator,
	PoolManager_t055017463B92A65D81A5E754D15D12DA13724CC0_CustomAttributesCacheGenerator,
	PoolObject_tF0C5B9B1AAF3697954D07E70476F7D3F483EE588_CustomAttributesCacheGenerator,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator,
	ActionsTrack_t1B5EDD15620DBF9785327B7E180BC80327FAF5DF_CustomAttributesCacheGenerator,
	ConditionsTrack_t1748BE481979E47AB07BF6935F015C0386653B74_CustomAttributesCacheGenerator,
	TriggerTrack_tB1B216F9527EDD1402F9FFE52EA9CD848139E906_CustomAttributesCacheGenerator,
	LocalizationManager_t90448B147FD8AC54B93A00F5F37AFB336180796D_CustomAttributesCacheGenerator,
	TextLocalized_t4B40B443794D291968B96B89B870C0DDBBDEE38D_CustomAttributesCacheGenerator,
	ActionCharacterAttachment_t8BA5DEF42000816F2966B57D12CDD33DCB10143B_CustomAttributesCacheGenerator,
	ActionCharacterDash_t7DCB83470E323AC4A96F916FFCC39E9FC5A1572A_CustomAttributesCacheGenerator,
	ActionCharacterDefaultState_t19A88B6694C52D5F1A3818DABC5DD6110245D676_CustomAttributesCacheGenerator,
	ActionCharacterDirection_t2F3A3767657696F9CA5C9D191D638E7F1E517E01_CustomAttributesCacheGenerator,
	ActionCharacterFollow_tC034266FE059628EE82A5761B752342791F27D79_CustomAttributesCacheGenerator,
	ActionCharacterGesture_t0309F0651027D4353C4F5EB512CB0E266DB4495C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t186ACC121AC3A0130436CCBC79C015D43F4630DC_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator,
	ActionCharacterHand_t5F2347D31CF1E8C931A63584E1E4D59FEA79A816_CustomAttributesCacheGenerator,
	ActionCharacterIK_t296DD4F660C2521311B95CD9BC6D91D7D845AE66_CustomAttributesCacheGenerator,
	ActionCharacterModel_t4942BE160154AD1B5DFDD47FE30D10F85175B856_CustomAttributesCacheGenerator,
	ActionCharacterMount_t8E1FA590B6E809B4F42FB1720DF2CFCC3B8A165F_CustomAttributesCacheGenerator,
	ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator,
	ActionCharacterProperies_tEF227A7B5249744012D763035B219BD2A32BB3EB_CustomAttributesCacheGenerator,
	ActionCharacterRagdoll_t54AAD233E21ACCA62FD602AD13FD0F8FA97AC8B4_CustomAttributesCacheGenerator,
	ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator,
	ActionCharacterState_t293D0F3506F4759A2E10FDCCABE8EAC4935A0E59_CustomAttributesCacheGenerator,
	ActionCharacterStateWeight_tDE8624304A2B0BB21CB6C50539A25421C8EAADF3_CustomAttributesCacheGenerator,
	ActionCharacterStopGesture_tAFAB500F430FF9DDDC0E286092DD10F56A3988B5_CustomAttributesCacheGenerator,
	ActionCharacterStopMoving_tF512DA676C407B755DC1DADE13DCF8138AD2A05F_CustomAttributesCacheGenerator,
	ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator,
	ActionCharacterVisibility_t9D49FE3829E4BCE271A9FC8E0F6EE0A05B567CB9_CustomAttributesCacheGenerator,
	ActionHeadTrack_t2A9CB746A712A13C46EF8B5667083FF3774792B2_CustomAttributesCacheGenerator,
	ActionPlayerMovementInput_t51FC6C63D25AF2AC2A7FF05A7D141F49B5A494DE_CustomAttributesCacheGenerator,
	ConditionCharacterBusy_t77B44CAC38A1EE5A3B07FE69A5D5A725B5B30B98_CustomAttributesCacheGenerator,
	ConditionCharacterCapsule_t1FBE62DF09866E8E4DFA5A5FBA9F8422CED60612_CustomAttributesCacheGenerator,
	ConditionCharacterProperty_t5D830CDF6022790EA73B880CFA9A7871AAF5B318_CustomAttributesCacheGenerator,
	IgniterCharacterStep_t5931FF16E7708CE382A87FB8263A21345A23FA54_CustomAttributesCacheGenerator,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator,
	Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_CustomAttributesCacheGenerator,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator,
	CharacterAnimatorEvents_t890194BBCA5F9C0A0653D57C6DF10828C52B7647_CustomAttributesCacheGenerator,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator,
	CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator,
	CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator,
	CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator,
	CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator,
	PlayerCharacter_tC1A0A4363913B90DD7455D8623BA8E85116052DB_CustomAttributesCacheGenerator,
	NavigationMarker_t3E505B1192D0E05285FF8768371DA89C20DFB3E2_CustomAttributesCacheGenerator,
	AnimationClipGroup_t875D45CA91B307FFCCE37DA5DC9266FE08D2097F_CustomAttributesCacheGenerator,
	ActionCameraFOV_t3345CE936F4E3E63E5B1AFDEE215181B60E9E850_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator,
	ActionCharacterJump_t8645160E4F7E41243D42244392F3FCDA546632C5_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator,
	ActionCursor_tE1BC859AC9930A86B608D8C2D53A37E980F7827F_CustomAttributesCacheGenerator,
	ActionOpenURL_t35EA3EEAAC48C3B9AEA91BE0B1376E89ED6DEF66_CustomAttributesCacheGenerator,
	ActionQualitySettings_tC936EB6BCEE807D6BF77D5C2BD3ADE8B433E5CEF_CustomAttributesCacheGenerator,
	ActionQuit_t711AF74F42693EF8A496F54C81BEDC94DB3B18E5_CustomAttributesCacheGenerator,
	ActionVideoPlay_t37842823888B365635130866198EF8AAC6256173_CustomAttributesCacheGenerator,
	ActionAudioMixerParameter_t8EE5308452ACAA231E4BDC60F271EE7195188ACD_CustomAttributesCacheGenerator,
	ActionAudioPause_tAD50D4F921DE33610C8646E2B9125D62881A4267_CustomAttributesCacheGenerator,
	ActionAudioSnapshot_tEC996A49D5FBBB69676F0C03408A69ABB6C5A5C1_CustomAttributesCacheGenerator,
	ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator,
	ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator,
	ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator,
	ActionStopAllSound_tBCE33DF50D635D2611B91D29F75B7BE0D3025B7E_CustomAttributesCacheGenerator,
	ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator,
	ActionStopSound_tD1F5F7BBBAFA9BC409999FECC74CA1A7FFBCCA80_CustomAttributesCacheGenerator,
	ActionVolume_t03D30A48E823BD343B64484448B10027922594B0_CustomAttributesCacheGenerator,
	ActionDebugBeep_t0DA6169FAB929E4CA46A9CF604686DDE46B87EC8_CustomAttributesCacheGenerator,
	ActionDebugBreak_t0159F759B76062DC5F1BFFF3101901CF4FABD897_CustomAttributesCacheGenerator,
	ActionDebugMessage_t5B3A716ECBB0BBA1CA19CFCA4B7FE5173245E070_CustomAttributesCacheGenerator,
	ActionDebugName_tB1F4625BCF538C75D7A6C4B29FD0633ADB8738C8_CustomAttributesCacheGenerator,
	ActionDebugPause_t05914E187FBA23242DFD8C92AD0A0365C816E03A_CustomAttributesCacheGenerator,
	ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t6D258995D994CE0D01571BB38DA16B5CEB44CCA4_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator,
	ActionCancelActions_tA7C9650A934B1B4904E6CDDFCF350377C1C411A6_CustomAttributesCacheGenerator,
	ActionChangeLanguage_tEBE8D653514B5FF2779BAB985BEB9A20A0E26C4A_CustomAttributesCacheGenerator,
	ActionComment_tCA0EE138D25F8CC10ACA8F6B4272D6597F8415C0_CustomAttributesCacheGenerator,
	ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator,
	ActionDispatchEvent_t8C31418203F39C3E2C9A1A378BE336F9315C2BB8_CustomAttributesCacheGenerator,
	ActionGravity_t4B1C56C83CEC558AB1FBF1C7605F5BCF60475578_CustomAttributesCacheGenerator,
	ActionMethods_t077B9F118F8F1D6B9E4BD45252B0618160CE8A96_CustomAttributesCacheGenerator,
	ActionReflectionProbes_t1714B7C2CBC1988BEFFC5F0129D642538DCC7AEA_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator,
	ActionRestartActions_tEAC07EBD6B760E468ED67E3BBCFE9002004CDF47_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator,
	ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator,
	ActionTimescale_t99E9A7C32BBBF5E1471C50FFA4166E3DB86C8D62_CustomAttributesCacheGenerator,
	ActionWait_t0F27D9E980892F6FCB53F16D73DF1DC1C42DF23B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tFCBDF1E50FAAD128C70DCD0BAE468BCE9599DA00_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator,
	ActionAddComponent_t3DFDE43F53629F6EFEE0720916A427BB0BF6AE02_CustomAttributesCacheGenerator,
	ActionAnimate_tFC32DFF0324C4F5EB3DFC09D1CE435D94A9A2826_CustomAttributesCacheGenerator,
	ActionAnimatorLayer_tD53D3C72E25A7FE76B5A46F8DCEEC5A68AC68C63_CustomAttributesCacheGenerator,
	ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator,
	ActionChangeColor_t986876EE1BDA98C61B219260C8151453A10F4A08_CustomAttributesCacheGenerator,
	ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator,
	ActionChangeTag_tF00F8B76DE0DF80E7757713611A8DDF651A3F80D_CustomAttributesCacheGenerator,
	ActionChangeTexture_t11A82311D8260104EC7C527046346A6FED2FDBF6_CustomAttributesCacheGenerator,
	ActionDestroy_tA7405067C7B521EDF28E389B2784B3ADC2DC111B_CustomAttributesCacheGenerator,
	ActionEnableComponent_tDE3E15791F7FEC384069915AA7B03A5CE375616B_CustomAttributesCacheGenerator,
	ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator,
	ActionInstantiate_t512D6F8EE2CF483C5705B28097B05BD9017BACAF_CustomAttributesCacheGenerator,
	ActionInstantiatePool_t8DAF4BAD44BC787100C677029855457A46E5B28F_CustomAttributesCacheGenerator,
	ActionLight_t03504821EF400C268A32470CB0ADACD61B4F326D_CustomAttributesCacheGenerator,
	ActionLookAt_tEC6DCD5F9E01B5D6B05309B802D65B2CEE6B724C_CustomAttributesCacheGenerator,
	ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator,
	ActionNearestComponent_t4E686BA50BC81FFEA4364FBCAE3C85D15EA5CD19_CustomAttributesCacheGenerator,
	ActionNearestInLayer_tFBC7BB322B958B7DAD90EE7D1F15D1B48E53E923_CustomAttributesCacheGenerator,
	ActionNearestTag_tE4A35B560DEC48C377B647DF9A6B71900844724F_CustomAttributesCacheGenerator,
	ActionNearestVariable_t6F08629C9A33EDA8B55703A29E00685CA063D4B7_CustomAttributesCacheGenerator,
	IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator,
	ActionPhysics_t7CD4CF007DDACD414E4114E2EBD2A632D2F2DEA4_CustomAttributesCacheGenerator,
	ActionRigidbody_t068293BF57EECB874C57681272E55CD1C80748FF_CustomAttributesCacheGenerator,
	ActionSendMessage_t3C26828701A22AD482B91AAACD1E571E090908FE_CustomAttributesCacheGenerator,
	ActionSetActive_t2BFAD8428D415685F61BF1BDBA50A4514CAC2007_CustomAttributesCacheGenerator,
	ActionTimeline_t57B789539A109DC9C8849DCEA3932A24B83FFCB0_CustomAttributesCacheGenerator,
	ActionToggleActive_t0B496A703B766C2B218003306E71CE8CFBAAE0C9_CustomAttributesCacheGenerator,
	ActionTransform_tDC86403B1F26B8B40C30AF8F0A078EF9A4A12776_CustomAttributesCacheGenerator,
	ActionTransformMove_t85738713489E31DF2CEDD5AF60A1B8730F61B5A1_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator,
	ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator,
	ActionTransformRotateTowards_tF44DDE238077CFEB5FC5988BC1DDDCCA645A8B13_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator,
	ActionTrigger_t09CE1270612E276C08475D8EF7E52A26C0C06AE3_CustomAttributesCacheGenerator,
	ActionCurrentProfile_tBE72EF851A3E216667BAF6C4F0FE38FA4A47564C_CustomAttributesCacheGenerator,
	ActionDeleteProfile_t43873144A33841E2CA744092567406088E3E60C3_CustomAttributesCacheGenerator,
	ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator,
	ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator,
	ActionSaveGame_t77E39E382C2C29FF3E9B619D141500A30E87905E_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator,
	ActionLoadScene_tA8C68DF3AC67C2B7A83125DD42B7D7BC6A3B940C_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator,
	ActionLoadScenePlayer_tDD31F6E771C9730CAF8D30454DA01AC843DE8B62_CustomAttributesCacheGenerator,
	ActionUnloadSceneAsync_t3529F909FA7691CECF11F5A3A18E1AC187E60892_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator,
	ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t3790C575EDE836D40EE7346D5FA20B81177D2492_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_1_t234E446225875D99E16977CEDE8A28A171FB589D_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator,
	ActionChangeFontSize_t72EFE4D913DD9C6BDF736BF2DEA6AFEE0413DFDC_CustomAttributesCacheGenerator,
	ActionChangeText_t77F06EABAB40F6CE03852DF537BA1C3916BCB584_CustomAttributesCacheGenerator,
	ActionFocusOnElement_tA73263034A9BF20A0C4E9254C6AF88907DF11827_CustomAttributesCacheGenerator,
	ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tAC7C769C854FA2327FCEE0E5FAB47DC94B93CB89_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator,
	ActionImageSprite_t18EB88A1CA9F746D9867E1093B785D7E2802C6B3_CustomAttributesCacheGenerator,
	AudioManager_t7C65AA205762F129C1F3AC7B700F3B2A350BC5A0_CustomAttributesCacheGenerator,
	ConditionPattern_t582CFD24E498EB990A32A3658F1C30AB449B508B_CustomAttributesCacheGenerator,
	ConditionSceneLoaded_tE661F7DEC67FD493F744B6233883790DA9D29882_CustomAttributesCacheGenerator,
	ConditionSimple_t6C40610134B5FE7B6B5E93C76FA340347C973171_CustomAttributesCacheGenerator,
	ConditionInputKey_t988EEA390DFDE0AF2C65AEB39B74F71C56223130_CustomAttributesCacheGenerator,
	ConditionInputMouse_t59C49393441E2F362650E9411525FFF546C64422_CustomAttributesCacheGenerator,
	ConditionActiveObject_t6E00476234FCDB730BE2A5B81D72D0E88EDBB95B_CustomAttributesCacheGenerator,
	ConditionEnabledComponent_t6D9FA8BC71B2D16B825B71D9E55CB982ADFDA8EC_CustomAttributesCacheGenerator,
	ConditionExistsObject_tE6138C9BDD87D252F37149C4FD569D3B49008DED_CustomAttributesCacheGenerator,
	ConditionRigidbody_tE9A1A4936B3117EF12D6D90AAE28C21583028192_CustomAttributesCacheGenerator,
	ConditionTag_t4F9E75EFC48A64AD7312675F1FA100918BBB796A_CustomAttributesCacheGenerator,
	ConditionHasSaveGame_tF0A4373A70A076029A13905E99F9C8B05BFD09ED_CustomAttributesCacheGenerator,
	HPCursor_t07495835A900993CB98DDAF4EC04C40C7E8A6A0B_CustomAttributesCacheGenerator,
	HPHeadTrack_t093C66392B8B099D01F90B05F2D29F8AFC9A1459_CustomAttributesCacheGenerator,
	HPProximity_tAC941D4DA97DDB8D42ECEDC7BDB6CE3B4AE1448A_CustomAttributesCacheGenerator,
	Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator,
	IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator,
	IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator,
	IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator,
	IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator,
	IgniterCursorRaycast_t7A0832FA76E83AE02D77D147EABF8C9626B89F62_CustomAttributesCacheGenerator,
	IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator,
	IgniterHoverHoldKey_t163AF8254FCF70B343CDFE1A415877CCD28D420F_CustomAttributesCacheGenerator,
	IgniterHoverPressKey_t84E025F464493F7320EC8DEC3C5652BCAACE6920_CustomAttributesCacheGenerator,
	IgniterInvisible_tD3D7101FF790017ECD8DCB2DA4DD4442C0798886_CustomAttributesCacheGenerator,
	IgniterInvoke_tE99D198956B0C099612186021C2C1D910A5F28AB_CustomAttributesCacheGenerator,
	IgniterKeyDown_t8A4FD67226984359051AB76E1BC8251CCA1AE93D_CustomAttributesCacheGenerator,
	IgniterKeyHold_t6422AA78240B425AFB5AF5FF19DCD203F7E85CD4_CustomAttributesCacheGenerator,
	IgniterKeyUp_t83F174654F37D98A64B56776629EE1FB17F7FAD4_CustomAttributesCacheGenerator,
	IgniterMouseDown_tB4B568DC4A42585793E3137418F3662B8C68FA4C_CustomAttributesCacheGenerator,
	IgniterMouseEnter_tC6546DD7558D98F7BC5D07517505325ABF039705_CustomAttributesCacheGenerator,
	IgniterMouseExit_tD924AF94D5A5474C4AB05B01708D19AA34C8FBCF_CustomAttributesCacheGenerator,
	IgniterMouseHoldLeft_t0363275354C0E0AEFE355B9541975AFFD035BFE7_CustomAttributesCacheGenerator,
	IgniterMouseHoldMiddle_tC6BBD4DBFB96A8F1EF1A1FB4590B1228B76A84E6_CustomAttributesCacheGenerator,
	IgniterMouseHoldRight_t066C8A4FFA07E6D97C349D4EC696643B2ED11386_CustomAttributesCacheGenerator,
	IgniterMouseLeftClick_tE15BFB6E24F143B23DB23763712F61B48D8F1717_CustomAttributesCacheGenerator,
	IgniterMouseMiddleClick_t73EC98FBEF9CE4E8FA8EF95828B3F91622D88708_CustomAttributesCacheGenerator,
	IgniterMouseRightClick_tED3EFEE63C320CD7B9236EE8C63231341A224158_CustomAttributesCacheGenerator,
	IgniterMouseUp_t80014B44A0995954B5C3636384C9EEC4F5FDB918_CustomAttributesCacheGenerator,
	IgniterOnDisable_t9211D525F51B5CAE110E99E06EAC64E217BF28D0_CustomAttributesCacheGenerator,
	IgniterOnEnable_tDC0B302211F9F72F5F6932AB45318BC093869BDE_CustomAttributesCacheGenerator,
	IgniterOnJump_t2C1A51B106B504CB6D3217F142F0844953827CC9_CustomAttributesCacheGenerator,
	IgniterOnLand_t152974D5552AFE38AF1C756E280F902169905ED7_CustomAttributesCacheGenerator,
	IgniterOnLoad_tB7DBC9B30CDAFB0B01CF881A4CA32A821AEB3B30_CustomAttributesCacheGenerator,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator,
	IgniterOnMouseEnterUI_t060C12E38BA5D9878667A94FBF73050ED42332A3_CustomAttributesCacheGenerator,
	IgniterOnMouseExitUI_tD27E7DE56FDF877B351BE6CB42A8F273F2D3D911_CustomAttributesCacheGenerator,
	IgniterOnSave_t299E08DCF64E7D58730F2DBB0A00D75C3A25BF00_CustomAttributesCacheGenerator,
	IgniterPlayerEnter_t3510146B1CBFD418ACE71FFC89C86A49DFCAD78F_CustomAttributesCacheGenerator,
	IgniterPlayerEnterPressKey_t87AB24DD7AFA55CBFBA7F62C480F90A5046B1988_CustomAttributesCacheGenerator,
	IgniterPlayerExit_t578B5F596CE614B6BBD87EB1F3E64D3C3ABEF2DA_CustomAttributesCacheGenerator,
	IgniterPlayerStayTimeout_t33CC63F34C4E0CB4471FF2D3A4436DBC74A73AD2_CustomAttributesCacheGenerator,
	IgniterRaycastMousePosition_t2F9FDEE6A57B8715831675B5BFE5A2F84E791FD8_CustomAttributesCacheGenerator,
	IgniterStart_t9A64ACE2AE4223AB096538DAD4979043A18E26B7_CustomAttributesCacheGenerator,
	IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator,
	IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator,
	IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator,
	IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator,
	IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator,
	IgniterVariable_tB145251A13724006BE57EAA9D42E6FDABF6734BF_CustomAttributesCacheGenerator,
	IgniterVisible_tECB7F6EA7F17BC6D1305529971D6DFD2B1720D1D_CustomAttributesCacheGenerator,
	IgniterWhileKeyDown_t8B12AEC42C645D88A0BDDE3DC1C6B499AB13490A_CustomAttributesCacheGenerator,
	IgniterWhileMouseDown_tE2ACE93F9BD33FECE6B1C3EC581866DB4168E617_CustomAttributesCacheGenerator,
	TouchStick_t345F706DE9C5E257BBE3C47BB6672ED654F18EAC_CustomAttributesCacheGenerator,
	TouchStickManager_t3E25C081E2EFD3F7A5998AE514B0DFFAB31D85D9_CustomAttributesCacheGenerator,
	Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator,
	Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator,
	Hotspot_t6BD27EDB919139A4B5022B037854D39E6F2ADAAE_CustomAttributesCacheGenerator,
	Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_CustomAttributesCacheGenerator,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator,
	GameProfileUI_t9E06E0608F31DBD5BAF077DA615F6BA4E87FDA1B_CustomAttributesCacheGenerator,
	RememberActive_tC35DAFB65F7047AD72E03E57F330F2FBD0D19937_CustomAttributesCacheGenerator,
	RememberBase_tC9496539ACD5CF521536F394CF611151CFA7000C_CustomAttributesCacheGenerator,
	RememberTransform_t520702CFAB253BEFEB32514CC6E646D3BBFD459D_CustomAttributesCacheGenerator,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator,
	U3CU3Ec_tEE0F1ECD9D55609B65DD3B41840D9ECD27A441B0_CustomAttributesCacheGenerator,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator,
	ButtonActions_tBD805CDFD93B7EAB55ADC0EDC43018A193868CAA_CustomAttributesCacheGenerator,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator,
	InputFieldVariable_t7FC56A2C5628D482E7352051D83321AEFA3AB9CC_CustomAttributesCacheGenerator,
	SliderVariable_t19D596997E54C9562569B0C66C2819DBDEC1763E_CustomAttributesCacheGenerator,
	SliderVectorVariable_t6902A88BAFA868F644F864155C34CBB7D717D620_CustomAttributesCacheGenerator,
	ToggleVariable_t6CD69B4A50AB5EF2B421F71EE496EED3B0088B13_CustomAttributesCacheGenerator,
	CanvasRenderMode_t205695765E012614AE119D5879699651BD769C5E_CustomAttributesCacheGenerator,
	GlobalID_t3EE8A0E42F455874624AE78D40341287BBEE4BC9_CustomAttributesCacheGenerator,
	CoroutinesManager_t3CDEA6D5BD9DC6CA25DB1240EF110C25B4D48D85_CustomAttributesCacheGenerator,
	EventDispatchManager_t6C18B35DC50FA0949C4F1E70BBF105E1725D520D_CustomAttributesCacheGenerator,
	EventSystemManager_tB0DAB954362D25F953E8ADD214D1968DC8F380E2_CustomAttributesCacheGenerator,
	GameCreatorStandaloneInputModule_t304FE6C948A67D2C9C37DAAD8BCE7145FA9346F0_CustomAttributesCacheGenerator,
	SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator,
	TimeManager_t8401071402372C4B59931805FFC97CB377E59EB2_CustomAttributesCacheGenerator,
	Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator,
	IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator,
	IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator,
	IConditionsList_t572443118DEF6B658B137E448305AC95B87BF172_CustomAttributesCacheGenerator,
	ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tE7EADE9DD7ACD7429227C9E984BA38F18F3CF54D_CustomAttributesCacheGenerator,
	ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tEDE96F7E2FF87C142781D10E5D19303E841153C7_CustomAttributesCacheGenerator,
	ActionVariablesAssignBool_t715FD74CB5522A0755A3996A2CE2C4EFB930C658_CustomAttributesCacheGenerator,
	ActionVariablesAssignColor_tF1D40B1E75EB8841F1CA6D2803AB36045E39DA5C_CustomAttributesCacheGenerator,
	ActionVariablesAssignGameObject_t5ACF6A1C73639C0D33CA1E467ADDD9CA57B79328_CustomAttributesCacheGenerator,
	ActionVariablesAssignNumber_tCAD6F1431B547A3197D8D86D47C380BD25B951CF_CustomAttributesCacheGenerator,
	ActionVariablesAssignSprite_t6C3EFC56D37E212B5FD644FDB16BAB7B82572E38_CustomAttributesCacheGenerator,
	ActionVariablesAssignString_t5FE23744A76D2761D10C7AC914568461C868738F_CustomAttributesCacheGenerator,
	ActionVariablesAssignTexture2D_tCDA2092CCC64A81C1A66982186F9865A07B0C58E_CustomAttributesCacheGenerator,
	ActionVariablesAssignVector2_t95829F4413220344010916387F0585045D97348C_CustomAttributesCacheGenerator,
	ActionVariablesAssignVector3_t75B13EABEDE215B76CA64F47499A951A3A498C03_CustomAttributesCacheGenerator,
	ActionVariablesToggleBool_t9E44F79B36DABA8251EE1AB7BD72A36FB82FE01F_CustomAttributesCacheGenerator,
	IActionVariablesAssign_tC3742C148AA3C7B2145B74F1FF0BFF7CB9E575B6_CustomAttributesCacheGenerator,
	ConditionVariable_t74D25129AFB758A3F3CED2E764C0424CEC3ADD2F_CustomAttributesCacheGenerator,
	ConditionVariableBool_tEB9F7F35EA93AC362259EDAE1759A589CB7886C0_CustomAttributesCacheGenerator,
	ConditionVariableColor_t3FE9D123A8F03BB488E4FECF8667BF25E528B6BE_CustomAttributesCacheGenerator,
	ConditionVariableGameObject_t4A90CB9A8CA33240D6020F75BA2DC57748ADEF2A_CustomAttributesCacheGenerator,
	ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator,
	ConditionVariableSprite_tCEE0AB106FFFD19105D017DC2AB6F701E45327E4_CustomAttributesCacheGenerator,
	ConditionVariableString_t38B161884AF2E8C551F5AF7012ECCFA2C2E4ACA6_CustomAttributesCacheGenerator,
	ConditionVariableTexture2D_t90CC4FE1CAB6978966D8AE1954B9D0260AB34C31_CustomAttributesCacheGenerator,
	ConditionVariableVector2_tBDBA87B67063C0589D4F2CE5B596828EEB5E3F12_CustomAttributesCacheGenerator,
	ConditionVariableVector3_t085E45E174F475E2571B9BEE687ECE58EAF8D802_CustomAttributesCacheGenerator,
	U3CU3Ec_tC6637A83649FD2B2366A75D474EE4016A36B871F_CustomAttributesCacheGenerator,
	HookCamera_t44CFF2D561623323C70F70ED3EB953812CFFE47C_CustomAttributesCacheGenerator,
	HookPlayer_t4F161DB71349F6FBF5B871438C2E3D1E059A7E4F_CustomAttributesCacheGenerator,
	ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator,
	ActionCameraChange_t9F42D1CC19931750D54554B10DA4CB02F96185E8_CustomAttributesCacheGenerator,
	ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator,
	ActionCameraCullingMask_t525E309773192422FF4DE41248CBBCE26F271101_CustomAttributesCacheGenerator,
	ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator,
	ActionCameraRotate_tD3A4862E67C35E9320D3249D72A8CBD54D950E9D_CustomAttributesCacheGenerator,
	ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator,
	ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator,
	ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator,
	ActionFixedCamera_t27A944771C1575027993778C49DB8048DD430A9A_CustomAttributesCacheGenerator,
	ActionFollowCamera_t831FC58553E436DDAE3219A2B342E1A079B1408E_CustomAttributesCacheGenerator,
	ActionRailwayCamera_t6E678631C1E84BF05E2A218B3F11621E3318A1B6_CustomAttributesCacheGenerator,
	ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator,
	CameraController_tE94B48FA4B37E6AEAD0FE1AC523656F750FEDA5B_CustomAttributesCacheGenerator,
	CameraMotor_t24EE44E47184D418A0C007C8156C95DB0EBDD373_CustomAttributesCacheGenerator,
	CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator,
	CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator,
	CameraMotorTypeFixed_t3E135A9FC2B61445AA499EA4AF3ED71EB63E18CD_CustomAttributesCacheGenerator,
	CameraMotorTypeFollow_tB02465B88B2B99D98AE20C4C8393AF8FDF5A6401_CustomAttributesCacheGenerator,
	CameraMotorTypeRailway_tE560EE57D4F7180C259B9864013697E359CF0F69_CustomAttributesCacheGenerator,
	CameraMotorTypeTarget_tA215C32844DEC5C76152138F5A2B3E49DF42816C_CustomAttributesCacheGenerator,
	CameraMotorTypeTween_t7D4DAD354A7A1B9DC3251F78753B79EAA0DE725D_CustomAttributesCacheGenerator,
	ICameraMotorType_t33A71BF290B80E05CFDD5C69B95B8503E67AFEB7_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator,
	ActionListVariableAdd_tE97C2318D01988C34C32D7803FD63CCBE48C5345_CustomAttributesCacheGenerator_item,
	ActionListVariableIterator_t2245EB96586671E181315DB90CE2B697C3D891AC_CustomAttributesCacheGenerator_variable,
	ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator_variable,
	ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator_select,
	ActionListVariableSelect_t903EE535694958A9250C21AB3F0D3C96791A26AB_CustomAttributesCacheGenerator_assignToVariable,
	ActionVariableRandom_tE225EE7232A93F7C812A504BE92B99F7324000DA_CustomAttributesCacheGenerator_variable,
	ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_result,
	ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_variable1,
	ActionVariableMath_t83D705F206E4ECD12655FA839F1333CEA76FCDFD_CustomAttributesCacheGenerator_variable2,
	ActionVariableOperationBase_t48A1078C98B26F63F8C064F1AF4EED1C372D3B02_CustomAttributesCacheGenerator_variable,
	VariableProperty_tE9B38002E5B88A7A44A86DBD39FF554C964F9B71_CustomAttributesCacheGenerator_variableType,
	ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_U3CiteratorU3Ek__BackingField,
	ConditionGameObjectInList_t757559B04B7C567170360111079485E60D7676CF_CustomAttributesCacheGenerator_containsObject,
	ConditionListVariableCount_t0B99E772D6DC0E8148062A9281F5EE38BABEBB40_CustomAttributesCacheGenerator_comparison,
	DatabaseVariables_t45ED3F34C851C8C28F7FD49C7942A8A9B9F6B390_CustomAttributesCacheGenerator_tags,
	DatabaseVariables_t45ED3F34C851C8C28F7FD49C7942A8A9B9F6B390_CustomAttributesCacheGenerator_variables,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varStr,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varNum,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varBol,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varCol,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varVc2,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varVc3,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varTxt,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varSpr,
	Variable_tFD580A35A943E8D422DA764102730E958979A2B5_CustomAttributesCacheGenerator_varObj,
	VariableGeneric_1_t9D5408540E5BA670F2F7C41EAE3057F924C6C34D_CustomAttributesCacheGenerator_value,
	ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator_message,
	ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator_message,
	ActionMeleeBlock_t81A6D6135ECF5521AD061E79A45DA91B6FE61400_CustomAttributesCacheGenerator_blocking,
	ActionMeleeDraw_tEF384443AF276D6FF300C0D1B1180C638AAA03EB_CustomAttributesCacheGenerator_drawPreviousWeapon,
	ActionMeleeFocusTarget_t7286DE047091458BCE6020BC766BE9104E8CA151_CustomAttributesCacheGenerator_target,
	ActionMeleeSetInvincible_t63EDBFD5C0E72F452C002621CE9E7ECA0ADDBD7E_CustomAttributesCacheGenerator_duration,
	MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_gravityInfluence,
	MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_hitPauseAmount,
	MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_shieldName,
	MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_shieldDescription,
	MeleeShield_t332C155410B0BC1EAE7895A206242A63567DB5B2_CustomAttributesCacheGenerator_lowerBodyRotation,
	MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator_weaponName,
	MeleeWeapon_t38DC5924EE7C12813ED673756199EF3E54E3C6C5_CustomAttributesCacheGenerator_weaponDescription,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_U3CMeleeU3Ek__BackingField,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_boxInterframePredictions,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_debugMode,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_EventAttackStart,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CPoiseU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CDefenseU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsDrawingU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsSheathingU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsAttackingU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CIsBlockingU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CHasFocusTargetU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventDrawWeapon,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventSheatheWeapon,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventAttack,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventStagger,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventBreakDefense,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventBlock,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_EventFocus,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CCharacterU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CCharacterAnimatorU3Ek__BackingField,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_U3CBladesU3Ek__BackingField,
	CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator_poiseImageFill,
	CharacterMeleeUI_tA105E963B31698BAA47B1627BA5230C878C7FA8F_CustomAttributesCacheGenerator_defenseImageFill,
	ConditionMeleeCompareDefense_t672E338E78E0660A5DFA0B291ADA6F31587FFC7E_CustomAttributesCacheGenerator_comparison,
	ConditionMeleeComparePoise_t3B8CA0E3067DFF3659FC05681306522211EC2CCE_CustomAttributesCacheGenerator_comparison,
	IgniterMeleeOnReceiveAttack_tC73836BB57D725D78FA798B32E5A96BFA1197313_CustomAttributesCacheGenerator_storeAttacker,
	TextLocalized_t4B40B443794D291968B96B89B870C0DDBBDEE38D_CustomAttributesCacheGenerator_locString,
	ActionCharacterDash_t7DCB83470E323AC4A96F916FFCC39E9FC5A1572A_CustomAttributesCacheGenerator_dashClipForward,
	ActionCharacterDefaultState_t19A88B6694C52D5F1A3818DABC5DD6110245D676_CustomAttributesCacheGenerator_state,
	ActionCharacterHand_t5F2347D31CF1E8C931A63584E1E4D59FEA79A816_CustomAttributesCacheGenerator_duration,
	ActionCharacterIK_t296DD4F660C2521311B95CD9BC6D91D7D845AE66_CustomAttributesCacheGenerator_part,
	ActionCharacterMount_t8E1FA590B6E809B4F42FB1720DF2CFCC3B8A165F_CustomAttributesCacheGenerator_mounted,
	ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_variable,
	ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_stopThreshold,
	ActionCharacterState_t293D0F3506F4759A2E10FDCCABE8EAC4935A0E59_CustomAttributesCacheGenerator_weight,
	ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator_position,
	ActionCharacterVisibility_t9D49FE3829E4BCE271A9FC8E0F6EE0A05B567CB9_CustomAttributesCacheGenerator_visible,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CLayerU3Ek__BackingField,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CAnimationClipU3Ek__BackingField,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_U3CCharacterStateU3Ek__BackingField,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CactiveU3Ek__BackingField,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_U3CrotationU3Ek__BackingField,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_defaultState,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_ragdollMass,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_stableTimeout,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_timeScaleCoefficient,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_U3CtargetU3Ek__BackingField,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_U3CattachmentsU3Ek__BackingField,
	CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField,
	CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField,
	CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_U3CActiveU3Ek__BackingField,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_terrainNormal,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_verticalSpeed,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_canUseNavigationMesh,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_character,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_animatorConstraint,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_characterController,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_navmeshAgent,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_U3CcurrentLocomotionTypeU3Ek__BackingField,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_U3CcurrentLocomotionSystemU3Ek__BackingField,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_U3CisDashingU3Ek__BackingField,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_U3CisRootMovingU3Ek__BackingField,
	NavigationMarker_t3E505B1192D0E05285FF8768371DA89C20DFB3E2_CustomAttributesCacheGenerator_stopThreshold,
	ActionAudioMixerParameter_t8EE5308452ACAA231E4BDC60F271EE7195188ACD_CustomAttributesCacheGenerator_parameter,
	ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_mixerGroup,
	ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_fadeIn,
	ActionPlayMusic_t05296DAF9C36B5550C3282B83A802C88A2968DFA_CustomAttributesCacheGenerator_volume,
	ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_mixerGroup,
	ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_fadeIn,
	ActionPlaySound_t21F6EF434DBABDC451279FE53D3327A0C350A0B6_CustomAttributesCacheGenerator_volume,
	ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_mixerGroup,
	ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_fadeIn,
	ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_volume,
	ActionPlaySound3D_t9249EE3838F7BCDF41BBB2C52BD6B6D44E414747_CustomAttributesCacheGenerator_spatialBlend,
	ActionStopAllSound_tBCE33DF50D635D2611B91D29F75B7BE0D3025B7E_CustomAttributesCacheGenerator_fadeOut,
	ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator_audioClip,
	ActionStopMusic_tD084AD916E27EDFFCED26A386F91019B029ECDC4_CustomAttributesCacheGenerator_fadeOut,
	ActionStopSound_tD1F5F7BBBAFA9BC409999FECC74CA1A7FFBCCA80_CustomAttributesCacheGenerator_fadeOut,
	ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator_variable,
	ActionCancelActions_tA7C9650A934B1B4904E6CDDFCF350377C1C411A6_CustomAttributesCacheGenerator_variable,
	ActionComment_tCA0EE138D25F8CC10ACA8F6B4272D6597F8415C0_CustomAttributesCacheGenerator_comment,
	ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator_variable,
	ActionDispatchEvent_t8C31418203F39C3E2C9A1A378BE336F9315C2BB8_CustomAttributesCacheGenerator_eventName,
	ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator_storePoint,
	ActionStoreRaycastMouse_t6BC50730815C0D363D4B9A747D1A7B4F0CFF87D8_CustomAttributesCacheGenerator_storeGameObject,
	ActionAddComponent_t3DFDE43F53629F6EFEE0720916A427BB0BF6AE02_CustomAttributesCacheGenerator_componentName,
	ActionAnimatorLayer_tD53D3C72E25A7FE76B5A46F8DCEEC5A68AC68C63_CustomAttributesCacheGenerator_weight,
	ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_blendShape,
	ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_duration,
	ActionChangeColor_t986876EE1BDA98C61B219260C8151453A10F4A08_CustomAttributesCacheGenerator_color,
	ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator_targetRenderer,
	ActionChangeMaterial_t42F37F9636D75E2782AB220731C02E8FFF9CBCF5_CustomAttributesCacheGenerator_materialIndex,
	ActionChangeTag_tF00F8B76DE0DF80E7757713611A8DDF651A3F80D_CustomAttributesCacheGenerator_newTag,
	ActionEnableComponent_tDE3E15791F7FEC384069915AA7B03A5CE375616B_CustomAttributesCacheGenerator_componentName,
	ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator_radius,
	ActionExplosion_t26F1647F041218C7764D7B9D01D34FF1BD4B6E17_CustomAttributesCacheGenerator_force,
	ActionInstantiate_t512D6F8EE2CF483C5705B28097B05BD9017BACAF_CustomAttributesCacheGenerator_storeInstance,
	ActionLight_t03504821EF400C268A32470CB0ADACD61B4F326D_CustomAttributesCacheGenerator_lightTarget,
	ActionLookAt_tEC6DCD5F9E01B5D6B05309B802D65B2CEE6B724C_CustomAttributesCacheGenerator_freezeRotation,
	ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator_propertyName,
	ActionNearestComponent_t4E686BA50BC81FFEA4364FBCAE3C85D15EA5CD19_CustomAttributesCacheGenerator_componentName,
	ActionNearestTag_tE4A35B560DEC48C377B647DF9A6B71900844724F_CustomAttributesCacheGenerator_tagName,
	ActionNearestVariable_t6F08629C9A33EDA8B55703A29E00685CA063D4B7_CustomAttributesCacheGenerator_variableName,
	IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_storeInVariable,
	IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_origin,
	IActionNearest_t9789B240620B5DAA7399100F518529C446D8DA3D_CustomAttributesCacheGenerator_radius,
	ActionSendMessage_t3C26828701A22AD482B91AAACD1E571E090908FE_CustomAttributesCacheGenerator_method,
	ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator_space,
	ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator_duration,
	ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator_duration,
	ConditionTag_t4F9E75EFC48A64AD7312675F1FA100918BBB796A_CustomAttributesCacheGenerator_conditionTag,
	Data_t00BF099EF0B4878CFFFD85FEDC8F7DB6C9A2E8CA_CustomAttributesCacheGenerator_radius,
	Data_tD6710293DD530E89D7C899221995C0C1847BB8AE_CustomAttributesCacheGenerator_radius,
	Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator_trigger,
	IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator_storeSelf,
	IgniterCollisionEnter_t20A2640F1A35DFAF66464FDD60B0315E6CDF1ACF_CustomAttributesCacheGenerator_storeCollider,
	IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_withTag,
	IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_storeSelf,
	IgniterCollisionEnterTag_t456C1DB7AB30E7722FFE61DA058CB18ACE84B493_CustomAttributesCacheGenerator_storeCollider,
	IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator_storeSelf,
	IgniterCollisionExit_tC9F4B03AAE33D50B060569FDC080ED6CF421E9C0_CustomAttributesCacheGenerator_storeCollider,
	IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_withTag,
	IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_storeSelf,
	IgniterCollisionExitTag_t36CAF9B896351206CADE872776888CACB92BF140_CustomAttributesCacheGenerator_storeCollider,
	IgniterCursorRaycast_t7A0832FA76E83AE02D77D147EABF8C9626B89F62_CustomAttributesCacheGenerator_storeCollider,
	IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator_eventName,
	IgniterEventReceive_tD78E15B538BE7F49A62E14BCD49291872372AD66_CustomAttributesCacheGenerator_storeInvoker,
	IgniterInvoke_tE99D198956B0C099612186021C2C1D910A5F28AB_CustomAttributesCacheGenerator_storeInvoker,
	IgniterOnLand_t152974D5552AFE38AF1C756E280F902169905ED7_CustomAttributesCacheGenerator_storeVerticalSpeed,
	IgniterRaycastMousePosition_t2F9FDEE6A57B8715831675B5BFE5A2F84E791FD8_CustomAttributesCacheGenerator_storeWorldPosition,
	IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator_storeSelf,
	IgniterTriggerEnter_tCEE32E965A79D552F0C64FAD3D625D49DB069D14_CustomAttributesCacheGenerator_storeCollider,
	IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_objectWithTag,
	IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_storeSelf,
	IgniterTriggerEnterTag_t3E1714917CF23B0ABF3AED563F9E079EACDFA1C4_CustomAttributesCacheGenerator_storeCollider,
	IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator_storeSelf,
	IgniterTriggerExit_t53EB75FD23C9BB0BAB1361BF638C5511D9E4C299_CustomAttributesCacheGenerator_storeCollider,
	IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_objectWithTag,
	IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_storeSelf,
	IgniterTriggerExitTag_t5FDF79FBF86F680D194DD6A8B21AB8598553F1EC_CustomAttributesCacheGenerator_storeCollider,
	IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator_storeSelf,
	IgniterTriggerStayTimeout_tBD503383E886256BAAB15C301113AA61076A424E_CustomAttributesCacheGenerator_storeCollider,
	Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_runInBackground,
	Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_destroyAfterFinishing,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CIsLoadingU3Ek__BackingField,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CIsProfileLoadedU3Ek__BackingField,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CActiveProfileU3Ek__BackingField,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CLoadedProfileU3Ek__BackingField,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_U3CsavesDataU3Ek__BackingField,
	InputFieldVariable_t7FC56A2C5628D482E7352051D83321AEFA3AB9CC_CustomAttributesCacheGenerator_variable,
	SliderVariable_t19D596997E54C9562569B0C66C2819DBDEC1763E_CustomAttributesCacheGenerator_variable,
	SliderVectorVariable_t6902A88BAFA868F644F864155C34CBB7D717D620_CustomAttributesCacheGenerator_variable,
	ToggleVariable_t6CD69B4A50AB5EF2B421F71EE496EED3B0088B13_CustomAttributesCacheGenerator_variable,
	GlobalID_t3EE8A0E42F455874624AE78D40341287BBEE4BC9_CustomAttributesCacheGenerator_gid,
	SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator_keys,
	SerializableDictionaryBase_2_t5E82D0B748DE680C42E26B566CB43EA6521B2349_CustomAttributesCacheGenerator_values,
	DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_prefabSimpleMessage,
	DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_prefabTouchstick,
	DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_saveScenes,
	DatabaseGeneral_t4E3B154FE883BC3711237C4D9CBC421C9DC2DD2E_CustomAttributesCacheGenerator_provider,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_U3CcoroutineU3Ek__BackingField,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_U3CresultU3Ek__BackingField,
	ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_component,
	ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_origin,
	ActionGatherComponentsByDistance_t52125C28546BB2CB63D4FAAE86470802A04465D8_CustomAttributesCacheGenerator_listVariables,
	ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator_origin,
	ActionGatherTagsByDistance_t1FAAA26BBB28CEFF02C1B0F815E5FA153813BF27_CustomAttributesCacheGenerator_listVariables,
	ActionVariablesAssignBool_t715FD74CB5522A0755A3996A2CE2C4EFB930C658_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignColor_tF1D40B1E75EB8841F1CA6D2803AB36045E39DA5C_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignGameObject_t5ACF6A1C73639C0D33CA1E467ADDD9CA57B79328_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignNumber_tCAD6F1431B547A3197D8D86D47C380BD25B951CF_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignSprite_t6C3EFC56D37E212B5FD644FDB16BAB7B82572E38_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignString_t5FE23744A76D2761D10C7AC914568461C868738F_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignTexture2D_tCDA2092CCC64A81C1A66982186F9865A07B0C58E_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignVector2_t95829F4413220344010916387F0585045D97348C_CustomAttributesCacheGenerator_variable,
	ActionVariablesAssignVector3_t75B13EABEDE215B76CA64F47499A951A3A498C03_CustomAttributesCacheGenerator_variable,
	ActionVariablesToggleBool_t9E44F79B36DABA8251EE1AB7BD72A36FB82FE01F_CustomAttributesCacheGenerator_variable,
	ConditionVariableBool_tEB9F7F35EA93AC362259EDAE1759A589CB7886C0_CustomAttributesCacheGenerator_variable,
	ConditionVariableColor_t3FE9D123A8F03BB488E4FECF8667BF25E528B6BE_CustomAttributesCacheGenerator_variable,
	ConditionVariableGameObject_t4A90CB9A8CA33240D6020F75BA2DC57748ADEF2A_CustomAttributesCacheGenerator_variable,
	ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator_variable,
	ConditionVariableNumber_tBD10196833D3684F9284B7A5BF27D9B1E59F012A_CustomAttributesCacheGenerator_compareTo,
	ConditionVariableSprite_tCEE0AB106FFFD19105D017DC2AB6F701E45327E4_CustomAttributesCacheGenerator_variable,
	ConditionVariableString_t38B161884AF2E8C551F5AF7012ECCFA2C2E4ACA6_CustomAttributesCacheGenerator_variable,
	ConditionVariableTexture2D_t90CC4FE1CAB6978966D8AE1954B9D0260AB34C31_CustomAttributesCacheGenerator_variable,
	ConditionVariableVector2_tBDBA87B67063C0589D4F2CE5B596828EEB5E3F12_CustomAttributesCacheGenerator_variable,
	ConditionVariableVector3_t085E45E174F475E2571B9BEE687ECE58EAF8D802_CustomAttributesCacheGenerator_variable,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CcurrentCharacterU3Ek__BackingField,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CcurrentTokenU3Ek__BackingField,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CnumberU3Ek__BackingField,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_U3CidentifierU3Ek__BackingField,
	ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_targetOffset,
	ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_pivotOffset,
	ActionCameraChange_t9F42D1CC19931750D54554B10DA4CB02F96185E8_CustomAttributesCacheGenerator_transitionTime,
	ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator_variable,
	ActionCameraChangeVariable_t06A2F46F9E0B4AEBB48894BFDB6C558ED4FDA72E_CustomAttributesCacheGenerator_transitionTime,
	ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator_dampingTranslation,
	ActionCameraDamping_t0818BA1A89D35A2D7351E33EA8F2B005E7D21A25_CustomAttributesCacheGenerator_dampingRotation,
	ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator_roughness,
	ActionCameraShakeBurst_tD2A79C9C300A1607FE5FE7DF6E672F482160ECD4_CustomAttributesCacheGenerator_magnitude,
	ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator_roughness,
	ActionCameraShakeSustain_t3CA73C6578F7C3E6D442B0591A1CBA468F94EDA0_CustomAttributesCacheGenerator_magnitude,
	ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator_period,
	ActionFPSCamera_t95E2FFE99ABFB29210AF4A6C4181232E2AF1E7CA_CustomAttributesCacheGenerator_modelManipulator,
	ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator_anchor,
	ActionTargetCamera_t5FD1AC275A5DFB89D0A86C8E4879FB823E76B9C9_CustomAttributesCacheGenerator_target,
	CameraController_tE94B48FA4B37E6AEAD0FE1AC523656F750FEDA5B_CustomAttributesCacheGenerator_cameraSmoothTime,
	CameraSmoothTime_t89DBA3D6521AB15643622EFB19EE41561B7D64C2_CustomAttributesCacheGenerator_positionDuration,
	CameraSmoothTime_t89DBA3D6521AB15643622EFB19EE41561B7D64C2_CustomAttributesCacheGenerator_rotationDuration,
	CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator_maxPitch,
	CameraMotorTypeAdventure_t827A1E49E0A55638A830FD2FD5DF5B9935B0E583_CustomAttributesCacheGenerator_zoomSensitivity,
	CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator_maxPitch,
	CameraMotorTypeFirstPerson_t52BAA1FC2981D8E15F619B12857FCCC55CA7B153_CustomAttributesCacheGenerator_smoothRotation,
	ICameraMotorType_t33A71BF290B80E05CFDD5C69B95B8503E67AFEB7_CustomAttributesCacheGenerator_fieldOfView,
	ActionListVariableLoop_tAF63DAC984020897C288EA411838F56D0B0E0675_CustomAttributesCacheGenerator_ActionListVariableLoop_Execute_m484A83A08BD40ACF0AC56B7F65DD61F11E15465E,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_m658ECA02DE483EE45BD23E8B74735576A87A991B,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_mA4C5A1494AACB2E9E96F1C9BD450A199CE8B687D,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFD2215568C11F45456ADE5DF3E6A8D8F5004560,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEEBD76692D994EC6D6539E5B6392BE1A12366222,
	U3CExecuteU3Ed__8_t28744558F11EE205336BF63D49FB7844B016FE97_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_mD77DDDFDD37CE5E81FC21CF5E61415CDFC2FDC99,
	ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_ListVariables_get_iterator_mFCBB7595D4EA279F6B1F9C15FA22579B60672FA4,
	ListVariables_t57009A7D663A2B65A1FEA912C42A8E73B26587D9_CustomAttributesCacheGenerator_ListVariables_set_iterator_m74F41B75E46D982BD624A4E8D6EDD63FCE9235BB,
	GlobalVariablesManager_t78138F07EAEBEC5E10FE8F0E36BACEAF77E223BF_CustomAttributesCacheGenerator_GlobalVariablesManager_InitializeOnLoad_mA313C49AF61E1FD4278C32C60581918067C63663,
	ActionFloatingMessage_t786089899CE527986541BB3D225178CE24996ED3_CustomAttributesCacheGenerator_ActionFloatingMessage_Execute_m5DB2F53398A5D023EA3DAB96EACBBBE242D448F0,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m645B88BD53A458E607304C79B5A5D69EC4FBE72B,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_m074630D9FC92E1AE2788E076CA71820143E7E731,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE43ABB7C01C2C671FEDD0BBFB7C5497148DC7B47,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_mFE2F97328C5088F606C650B4DF62ACA28328F7C3,
	U3CExecuteU3Ed__7_tD299CA02692D0C39222B0621BA95C482AC7E5EE7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m09CF5B3C1EC26C1A1F053626B8B2A6E8A7658AEE,
	FloatingMessageManager_t605E5D87FFD45F180931B4A45268B973332BE841_CustomAttributesCacheGenerator_FloatingMessageManager_CoroutineShow_m0220E1AE93E6EC41522C334BA5C5F1287A269900,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7__ctor_mCAE5A30C39DEC18976BFB9583C017FCCCA30A36E,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_IDisposable_Dispose_m86C925F77A798544C527999C561D3874064C373B,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m714EDE17A8EBE56F2D7BA04B146A1F94D5C2A1D9,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_Reset_m9BEB6319F95DA2D06C30F20C4A7D55000B14232B,
	U3CCoroutineShowU3Ed__7_t2959AB7F29B22B2B56D8E0C456CFCBED418EB60B_CustomAttributesCacheGenerator_U3CCoroutineShowU3Ed__7_System_Collections_IEnumerator_get_Current_mEB27EBC56988E61DF2FF2BEB9968EBC2ED2CA3A0,
	ActionSimpleMessageShow_tA8DF56453532A25AD968496683BE68B660FC0EE4_CustomAttributesCacheGenerator_ActionSimpleMessageShow_Execute_mC5832C6D843A71A1A4C29DA4676AFF28998EE806,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_m3F5262E9C558F8015181D212E910AF955EED9179,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_m388E6650772D9F9F1B1C94AA393E1C65D0E5743E,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28E82C9CEF55D8CF9A58366AD7F3FC1071937101,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m551180D660E24ED9170D68C6C7FC8E25934190CD,
	U3CExecuteU3Ed__5_t3ECC32F0C297969A492A4D705936072BFEE6F378_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mB797C426D079B14DA6B07F17558837335739D50A,
	SimpleMessageManager_t61808F8E20DF4106250E824C6A9D538912E69881_CustomAttributesCacheGenerator_SimpleMessageManager_HideTextDelayed_m1CB192B2F48BECC263728253FA9840D360B3CE64,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11__ctor_m7E5DCB81FCD6E15E8D6CC97EAD891CA0BD8F22A1,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_IDisposable_Dispose_m50F21BC68D8A41B1065AFDDFBB4BC323ED1024C4,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67242BD4F4EE571C0EFA15B38109ED78B3D974E5,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_Reset_m5869D5BC10C92C2F71A32895DDDFA44A0A862BB0,
	U3CHideTextDelayedU3Ed__11_t75C1F07C733ABBA5FDEA0BF982F5C8F20DFC1BAF_CustomAttributesCacheGenerator_U3CHideTextDelayedU3Ed__11_System_Collections_IEnumerator_get_Current_m1C941C49AC049605C872428FD343A5D9990088AB,
	MeleeClip_t6BE90499EFC96FA9FBB543925677ED309F3C8658_CustomAttributesCacheGenerator_MeleeClip_ExecuteHitPause_m443EF74676E99E02E6B0A751C098887E02AA66F5,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37__ctor_mA10EB23AFBB6F257F7D54CB541E96CDB237B2A47,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_IDisposable_Dispose_m1FF79EE075B38D2D8362870A57BF06D9A6802B80,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9ECB2917A67CF28F30304842FE0AA6968E907F,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_Reset_m4A85E677F743397D8F2C984245BADCD93DE11221,
	U3CExecuteHitPauseU3Ed__37_tC994BE9DC9CFE7EFD2B85C0CA52BA43717DDFF57_CustomAttributesCacheGenerator_U3CExecuteHitPauseU3Ed__37_System_Collections_IEnumerator_get_Current_m7E7F2C0D3E1DE7198126B25DB63FF8E140EB3A79,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_BladeComponent_get_Melee_mB9E245D961F3BB1A87373D8E939D278F7C8C28F2,
	BladeComponent_t49B5D13E087567A8B248EFB5FB07928277663A4F_CustomAttributesCacheGenerator_BladeComponent_set_Melee_m7071E3DCEB7520779AA743DA210BB810041366A8,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Poise_mAE7DA4E54BA255648BD8839974782B5FB9759A83,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Poise_m4A8BF6725DF27A0938FCF2C698694491FA1CE201,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Defense_m507A720A94BCAB37B13ED5B740535DA9208302D3,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Defense_mB3F50AE24CE4E58C8D6763D7A1CCCB2FA8BAD61A,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsDrawing_m896E27571073303B79D91CC2886EA9C0636D92CD,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsDrawing_m6DE5FA9449BA3AFE940FFCF9CBFE4C9C7EED6310,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsSheathing_mBD515D68F0139E9E1913CA37B3F8B9B10A735FE1,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsSheathing_mD90AAC761D35BF4D894733EC2AD265B6859CCAA6,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsAttacking_m4E8EE2CD01688E319C7166420F9342088E9B7BFE,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsAttacking_mA3B512832B1EAE92A05E64DE3791659CB78F8A7E,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_IsBlocking_mFF11D0FD9798CCCF39E1545039E591236C5C896A,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_IsBlocking_mCB612F17CA908BA0BD4188DE62ACAA49A7367D95,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_HasFocusTarget_m7BD8A39A0F492B250E44E3208F79B8B5C4DEBF20,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_HasFocusTarget_m590FCFD6C2DC40DF79BB59FEDAC4A1AAEA08D2AE,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventDrawWeapon_m729A717864566C772CF71410BDE1924DBC3AE947,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventDrawWeapon_m3A5FA842827A19CBB1895C99397FA608F52E9D60,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventSheatheWeapon_mFDA0966CF05CA40A002E10DCE4C99DB694C5AC77,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventSheatheWeapon_mFA2B6F3F7DE02A7CFA9A36FE638A2B044AD289C5,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventAttack_m2DBB7EEA0FB85D42038CE65C50B355CB02C782BA,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventAttack_m8EEAE732C1B79F7BD62F5C72513543C2D361183E,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventStagger_m4109CC64CD32AA9507C988F3934BCC867CF25381,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventStagger_m3E4FB53BF4351A020EDC55C7332F901CF14A99BF,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventBreakDefense_mB6BB87DFB00DB2B5C574CC8968782E6E6BDA702E,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventBreakDefense_m6FCF86C0FEB98F29565FDA8088184CE910D40BBF,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventBlock_mF88A06EB7F4A0F2C1010BC793B5E4E29416468AB,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventBlock_m8A34212AA2E12435FF6D7EB286882E490296ADC0,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_add_EventFocus_m634B32366980C642D31F60AC90C71CB7152BD014,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_remove_EventFocus_m2EF8A205D03A9353EC32768ECFE79B2142FC9674,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Character_mC8213FA44F089EE7A6B5B06B0D7390E32E4CB5CA,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Character_m78B44B240657959C2834FB7887BA2D07FE375DE4,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_CharacterAnimator_mA4B0B02AB3070AEE773BF16764D3D3796A1A9FB6,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_CharacterAnimator_m2AD046145B4043A4D45654BB2C01E0F7BE6428EA,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_get_Blades_m4E84AE0B4C496A680D941D9981142B0A81CBD87F,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_set_Blades_mC80F11AE570B6BD8E1339C79799EA35F12D6E0F7,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_Sheathe_mFB1AED1FFEB02DFD57F1E5FAA86CB41C8E18C3E7,
	CharacterMelee_tFC3B3D8E984E88F887F250216DD7404254F9DA9C_CustomAttributesCacheGenerator_CharacterMelee_Draw_m00C457FC85DEA4F4DFC918137617E27E6357A39B,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102__ctor_m786A68C4F750B9E38A58F10D081860846F94A69D,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_IDisposable_Dispose_m689042F48C397182F117B6C74794F679471DEB34,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1FEA64D0AD51EBDA277E8E71DA96251B74C7CA9,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_IEnumerator_Reset_m8D077FC808E8BC30A3CEE7FE46311E214B99C354,
	U3CSheatheU3Ed__102_t731052B678D2239D6FDC5C9DB4DC4547FBA9D011_CustomAttributesCacheGenerator_U3CSheatheU3Ed__102_System_Collections_IEnumerator_get_Current_mD4C206E05163045BB434D18C816174F17F2B2741,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103__ctor_m3CDEEE7F8DB99DC8D219743C706D8827F2422D56,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_IDisposable_Dispose_m24DBACDBA5ED681EB17250F583B4BA16E82115C5,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3BCBB31D4FD10EECEE363E70EB8F06ACBB66B39,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_IEnumerator_Reset_m6768D2D348120C62AE0E8148B25D55D1CA19CBE7,
	U3CDrawU3Ed__103_tCEE670B0EF115BB493851BA6DCDC354AD7B0B556_CustomAttributesCacheGenerator_U3CDrawU3Ed__103_System_Collections_IEnumerator_get_Current_m6E02C65508D6A1B59269C5B13E0D3A071A40EAAE,
	PoolObject_tF0C5B9B1AAF3697954D07E70476F7D3F483EE588_CustomAttributesCacheGenerator_PoolObject_SetDisable_mAA2DF8FF1FE4B2A52BDA18F7D4D9711A81591C51,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7__ctor_m82A6D08FFA1F31480B620ECA12FC75F1CAD29BCC,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_IDisposable_Dispose_m245E04359C4A3FC3EDA5E332275E22D78084C139,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB212C3BF8612759E13002DB305FDCD7C5C2FD19B,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_IEnumerator_Reset_mF5D91E3D2FF98620E0869B07A72911E72F8F3B46,
	U3CSetDisableU3Ed__7_tA9D8A8007B0BB1485B9653856D69DE766DE781C4_CustomAttributesCacheGenerator_U3CSetDisableU3Ed__7_System_Collections_IEnumerator_get_Current_mB428927DA65710A66D860A096A09C621EB5F7189,
	ActionCharacterGesture_t0309F0651027D4353C4F5EB512CB0E266DB4495C_CustomAttributesCacheGenerator_ActionCharacterGesture_Execute_mF079124C1F71B2CBA071614DF5D961DA403013ED,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10__ctor_m3C6F29778E65D111F6E51FFF293FF6AB4E1D8B54,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_IDisposable_Dispose_m4E1B6B7427CEFE2C3661378F63CEA456C34B7F18,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80DEEACBD0C640866FB5D092E8FFE518CFA782A3,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_IEnumerator_Reset_m5DF0124AAAD326C0E6C5520768B34B7C811DBEB6,
	U3CExecuteU3Ed__10_tF10E5B18A9455EC3CC3A8C2066033C442F7A1CC0_CustomAttributesCacheGenerator_U3CExecuteU3Ed__10_System_Collections_IEnumerator_get_Current_mBA7CD44299C5921344680D9462F7C272A5782DE7,
	ActionCharacterMoveTo_t7C27E9E5F24F20F99621B07C6B338B746251FA30_CustomAttributesCacheGenerator_ActionCharacterMoveTo_Execute_mFDD8702F438971F618BC2CC5070F79F5D2D755B1,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16__ctor_m11BFD9B002E8E393AA4F150E4D26ED9408BA65B1,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_IDisposable_Dispose_m76698B7FC09D4CEB478E55D7D28CCDDA5FD7A03B,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14CCFEB1097ADA90AF91AB66664339DC1FA086D1,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_IEnumerator_Reset_m954AD4B93FCE4A094FE5D1EA875B8516F283DE95,
	U3CExecuteU3Ed__16_tC5935691DFC6899C223A52351DF3EDCFBFC2B2AF_CustomAttributesCacheGenerator_U3CExecuteU3Ed__16_System_Collections_IEnumerator_get_Current_m1819E8E5ECDC100A7ACF93D0787045CFBA63EBCD,
	ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator_ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m93A9ED4176E3050FF375EC8C119F4EF8DF71FE74,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_mC69556267AA0ECA60558F102F170DE67D89E5E2C,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF92FCFBC97E98DD5FA109F65537870A658014802,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m28C693D9D027244014063A86657F20629EB0D302,
	U3CExecuteU3Ed__7_t6C9DFEDC44C6C1D5E5FEC143036E70A2FDC87BB8_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m89023EE397F53C4FA098252012B715C3A57DF914,
	ActionCharacterTeleport_tCE6ECEBE461A4B7258C836577A28DB5ED3A27FD7_CustomAttributesCacheGenerator_ActionCharacterTeleport_Execute_m9542BA250FECE50E3E269827B77E3E44633BA65E,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_m0229D5A895D47865D75B9CA9E6E669E1C2F14465,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m895F9261CA1402B728553CFF4F16F2F936BF7F9C,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m105C970E77214FD89055FF4D88816CB5BA8E1938,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m1A8C9CD34172B9F3BD01A4CBF933EEEE5DBC06EF,
	U3CExecuteU3Ed__3_t2C21C96FE1A6B374FF3E3FE5036101027BDB89AC_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mE70B6EA23875886C4CFBEC39F12808AC10AEB884,
	PlayableBase_t07B459408084FCE8E65ACB7350D48128438B34E9_CustomAttributesCacheGenerator_PlayableBase_DestroyNextFrame_mD1CA8E6618014C5F0FF63CFB4CC580E8481DB93C,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16__ctor_mC2D8B87272A5FF7978C249A18186598873F2B420,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_IDisposable_Dispose_mD1D6D3F8B8A5047BE75FBBFD0DCD02F05CF12971,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70A9E325F1FF7756263DDB8DF254F4C1ABB5E9CD,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_Reset_m3FB96BB157E9D36F17571507B075F3A3B60EEB6D,
	U3CDestroyNextFrameU3Ed__16_t835FD3DCDB4F1598D984C714346FD59C08E93274_CustomAttributesCacheGenerator_U3CDestroyNextFrameU3Ed__16_System_Collections_IEnumerator_get_Current_mE41FA7C32E7D6344416ECD3407387C2AD2F5365E,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_Layer_mA0A0FEF3E34BB52210BEB65292E152C37F4E5305,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_Layer_mDBC6CF2A5E1072D454BCD7F6633D52C18E7C04E2,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_AnimationClip_m86C11355A72EB8137CA039DAB5123DD11D9AB080,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_AnimationClip_mAC5DDE460E808A580B977179B28D9EA95CDF44A8,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_get_CharacterState_m42DDC145AE77AB341C5AB3B0C78C34E0B6815A60,
	PlayableState_tB28A14A2E491DA5DA9765DF3582644ED2F20B04B_CustomAttributesCacheGenerator_PlayableState_set_CharacterState_m79263DC2A30FB8420BB2EBF7197CA9B823ADA473,
	Character_t5CC340A551F08F712A7AFCF82480BECF8064C20A_CustomAttributesCacheGenerator_Character_DelayJump_mA4414CFD1241AEB812807E713BC32E7980413D8F,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_active_m34FF82B6AF6DD2D3E928D916780B138E9719C367,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_active_m0BA91835FFFE3C402173307019ACB40D307A391C,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_position_m5B68052A77DBC44449874754803BAE3644D63890,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_position_m093315344BB72869F08A67A90158EE06ADA7CEC2,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_get_rotation_mBD41328E6A7445448AE5836E0334AF7F452599CD,
	OnLoadSceneData_t55443CA6B819A4D0D398BB4FB7436BFF4132940E_CustomAttributesCacheGenerator_OnLoadSceneData_set_rotation_m6D4D33972A156DECD91F919E2CE0BE33EC8E560C,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38__ctor_m45DB7082FB55923B86B2ED0DEF78A2C72F7DD8DF,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_IDisposable_Dispose_mB0D6B4E55D63E183B283D9DDD648BDF1F006DB49,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDA92A55DECFFF61C95B266DE83FCC68616C1187,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_Reset_m3EAD32CF2878187EF54DD5E5BBBCEDEF3188B42C,
	U3CDelayJumpU3Ed__38_t1B416A3EAD3946999EC73C66FE320C22DEBB8387_CustomAttributesCacheGenerator_U3CDelayJumpU3Ed__38_System_Collections_IEnumerator_get_Current_m80E2FDA7B04AEE391BD2B84A8D27DD146AA6FBBB,
	CharacterAnimatorEvents_t890194BBCA5F9C0A0653D57C6DF10828C52B7647_CustomAttributesCacheGenerator_CharacterAnimatorEvents_CoroutineTrigger_m9088D17F2E9CAA8C76F641F7F41BC210DDD32476,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7__ctor_mB1B3F3D66017ACD041A3DF472B357151D137719B,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_IDisposable_Dispose_mDF30931154439004D31B2A026851F8AA63ACDDBA,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D7C9AC15EAC985E0945CE394972822428370F2D,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_Reset_m623E4A129BD91E66D1397F77758386EBF64A20F5,
	U3CCoroutineTriggerU3Ed__7_tF75064F17B191BC8B3B07B9F8BE44EB9A90C3283_CustomAttributesCacheGenerator_U3CCoroutineTriggerU3Ed__7_System_Collections_IEnumerator_get_Current_mC44FB9A3B262F7673714DD3EDA5E77E5891DBCB5,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_get_target_m0B4324C67138CD8F6AAC5BC84888EFF7A978F1DF,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_set_target_m189D5417D456FCEBB0DD9F0DDBB4322E5AAB4A1D,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_get_value_m2CDBD910B33AA9C38C3E807BACAC38C1978F9D63,
	AnimFloat_tA7F6C12C784BFB9831B4E0B8A883609D806BD870_CustomAttributesCacheGenerator_AnimFloat_set_value_mC1E3E842276BA71CA344134C5593569748D6584E,
	CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_CharacterAttachments_get_attachments_m0CB2DB2A159F8F311D19A4707283B2F5C9213D50,
	CharacterAttachments_tD8AB03567E8D506B46846FF5C9C428C1F1FFCC71_CustomAttributesCacheGenerator_CharacterAttachments_set_attachments_m8FDDE1DF5B13AEA89E97636F72BBF4222ECDCD08,
	CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_CharacterFootIK_get_Active_m026E791D09313D95E6552031863D8A3C177CE13F,
	CharacterFootIK_tEABDA77542889F1B15342CFBC0B1EA4AEB8BE198_CustomAttributesCacheGenerator_CharacterFootIK_set_Active_m6ED2E272FEB78F4FD67F9E8D9A6A4E1A7CE6303C,
	CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_CharacterHandIK_get_Active_m9DE731503AA6488A0F459F86B49C35C7DCBB38B9,
	CharacterHandIK_t81E8550A7E8C30A24FBE7BF3511AE0FCA9F9F0C4_CustomAttributesCacheGenerator_CharacterHandIK_set_Active_mB3995094DD4DA840C8B4791913796CFA57475C64,
	CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_CharacterHeadTrack_get_Active_m3816DE3591D91706830CB25BEA2C2FE7DC5BACFD,
	CharacterHeadTrack_t58A88C7D2140D7C2CCE83128EF3427AE0AC10D1E_CustomAttributesCacheGenerator_CharacterHeadTrack_set_Active_mFC5D304F334A636912FC185B19AB2D4C7EF4FD95,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_get_currentLocomotionType_mA4FD56B3638F170115848BC7803482297B3E29D9,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_set_currentLocomotionType_m56CF03F923B8B8A110975B02D4CB3BF2A402B3FD,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_get_currentLocomotionSystem_mD7F3DE1436DF555B140C54593D6095D9AB7EAA52,
	CharacterLocomotion_t885BE6274159EEBF929622977C790D25421A657A_CustomAttributesCacheGenerator_CharacterLocomotion_set_currentLocomotionSystem_mED30E6DD7EDAB456C270565D8C8BA330C517ABBE,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_set_isDashing_mC079E6C302938750FA3B860353FD0657B09C4A53,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_get_isDashing_m1C6E364F5B8D885EEB5EF34053F5C9156B12A439,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_set_isRootMoving_m360142C3A457C12C703CD94B9674C1A64EB3F21C,
	ILocomotionSystem_tFA8E56EF2A6A76CB835EFD704821F0CEC077D4DB_CustomAttributesCacheGenerator_ILocomotionSystem_get_isRootMoving_mD154CF22484E4ADD2130D6A2BC32A3FF9A44AACD,
	ActionCameraFOV_t3345CE936F4E3E63E5B1AFDEE215181B60E9E850_CustomAttributesCacheGenerator_ActionCameraFOV_Execute_m2961F82EDD4ED139F459031B2114495B90DDBEB1,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_m568C46FD84354F03E321B4A7D9B6A406155A6442,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m119B7D8FFAD38119F367543F53D104EB2CB9AC46,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D53463E3447749CF12F7FEA5553D2AD9FE91169,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mA5FDD65159048AF6BDEE02D3389DF5B1870EEAFB,
	U3CExecuteU3Ed__3_tAC8DAC553AD786F5F6D368D6B58750B82745B27F_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m1AF795A4F8BA74E09BB67237231F64629E174B23,
	ActionCharacterJump_t8645160E4F7E41243D42244392F3FCDA546632C5_CustomAttributesCacheGenerator_ActionCharacterJump_Execute_m08FA51AE3EF80F74E8C7437DF69FE88F2B3074B5,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mD014279CDD1DA2CB66827900A7FEF6B36E4034A6,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_mC869E87A2210092C97E4DBD7E06FCBE358652E49,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA829096573BA1FF01D24904DA418638E07E295D1,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m251ED7B0A59C1F6511E8A0752E09990150A66F22,
	U3CExecuteU3Ed__3_t52F05B3C1BACBC4B620D7451286DE7D2AE7E0A4A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mEA566C81C92D1772C5B0DA08E2015A4B14C12E25,
	ActionActions_t27413FBD50297FD3F52423A620137407E6E6543B_CustomAttributesCacheGenerator_ActionActions_Execute_mF60338A03B212040424031E749D555D34F89EC45,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7__ctor_m8F225E767DD6DB758FEA0B87C3078AA77FBFA5FA,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_IDisposable_Dispose_m4376D2481CBD9E7ADDC3DAF2152CFC9092F5F7E5,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1684DA24B04EC7538FBC1999207915EF16FD474D,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_Reset_m7251172E50F11399CF598F7C05629B9825C0F233,
	U3CExecuteU3Ed__7_t7ED84D7CF74B4CE513EEF7D9D1F059A19AA182FA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__7_System_Collections_IEnumerator_get_Current_m0DA803CF52C2AF51D6ECF171BF22580ED5539F5B,
	ActionConditions_tB85988EB885E74EE99C077BA0739284C4615B67B_CustomAttributesCacheGenerator_ActionConditions_Execute_mE0522B42F62D3F243F1AFAE772B9CD175E83C357,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m9EAFACDF88412BADB6CC5CAEEA99FE03D10FDAE1,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m659F01A84B88179C9CAECF6702B4C86FF9A43B89,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC017996B99A9A285EE0A0982EF05AD814B15FC3E,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_mF76B5998B28F241A1530AF47AF804C28A03FEFC2,
	U3CExecuteU3Ed__6_t6D744C0F8ABAAFA26F89A0C1A6AF2B9947D6990A_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m555CFC88911972DC5B58D4D04A9F0C3E3821B1ED,
	ActionReflectionProbes_t1714B7C2CBC1988BEFFC5F0129D642538DCC7AEA_CustomAttributesCacheGenerator_ActionReflectionProbes_Execute_m5A918D81E680B42CF41A202C722F9B6CA4DA1475,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4__ctor_mE3705328BA02A5C6804D656A84D2F01521659B17,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_IDisposable_Dispose_mBF5E03402ECCA456AD7654A8C19ECD5785307A89,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD002A5B232EDAB262AD2577DA21E86AE289242CA,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mAF5226AA4EF8C651DDF193AA52AF3F58E3720CE6,
	U3CExecuteU3Ed__4_t80AE3BA2E94520193BE50EFA3F8988008565ED29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mE081B69A7BC0703684ACD7D36C0899EBDD746C3B,
	ActionRestartActions_tEAC07EBD6B760E468ED67E3BBCFE9002004CDF47_CustomAttributesCacheGenerator_ActionRestartActions_Execute_m141874EBFFD0D71B3EEBC622D98CB24E7FB22955,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0__ctor_m52231F20C05F8C80A3ED4721BF367C1DDFB06CE2,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_IDisposable_Dispose_mF52E298FC3DC9C9456316D73F74C0005B769B4E0,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37052B91459D04361E63F25F8DCA3E4C52A95CEA,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_IEnumerator_Reset_mB1712CAF9C824B2468F658B4D5A50B88CE06D799,
	U3CExecuteU3Ed__0_t21F66EC3193FCC812BD87DBD129649827932319D_CustomAttributesCacheGenerator_U3CExecuteU3Ed__0_System_Collections_IEnumerator_get_Current_m3C22C51AFB94DA83D80DB69FBF40AAAAD12D9356,
	ActionWait_t0F27D9E980892F6FCB53F16D73DF1DC1C42DF23B_CustomAttributesCacheGenerator_ActionWait_Execute_m04586A3FDF625719D21C4722DF245176A7190D66,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m1C9147D9500D8992417D62EDB2969850D0BCA95F,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_m46DB6B72EF206B2AA46FB515BB10A3E6CE5B8FB8,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E1BA8ED4D7167A7EC8B04C9E249F6C95F7C911,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mEDA5AA1846C72B143E01D49133FD304FA342C9E4,
	U3CExecuteU3Ed__2_tDC278E4CBD3A52F3D364F4B565ADE14F134BFDF2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_mD1D01BD5AE2C54941C4051CFEF0DE1FE6E368F8D,
	ActionBlendShape_tFFE7430806AEC2DB0F207E4C065A2827608C50EF_CustomAttributesCacheGenerator_ActionBlendShape_Execute_mA4756B727376FBA06CDDC8C25893BB0FE8EEE787,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_mF19135E1262F01401F62B65C6AAE21E195800841,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_mB01B52B59D5766C1B8DCB01058767AAE92EE08A3,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB6BDC403B3CC4A1A6CC2BAC53F492A49EC68B33,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m7E4256ABD8A2DB3A8A5DD1F625898904BD4749E5,
	U3CExecuteU3Ed__5_t2C8B56851246D88B3BD38D0DB1F39C939A4CD8E7_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mD6A3F2C62C6A20DBC26A9983D538106441A5CE1D,
	ActionMaterialTransitionValue_tE5862E2F18AF1B056A9E54EB44D925718DFE4307_CustomAttributesCacheGenerator_ActionMaterialTransitionValue_Execute_m5532403A5A5539119EFFC92D16650C435CE447DF,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_m63455629CDFC718B52F66A855AA973034CA1EC15,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_m33F4E3AABA145A6F5929A9646A66A74F9B8EB3F6,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m921C4AE7D1431654297FBFF722D990452EAB020D,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_mEBCDDDC6151242BB567F141352561DD344537C13,
	U3CExecuteU3Ed__8_tBBC38E31599325AE49BA3F70AC26F2C9807E4629_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m4D68358EA683E8DFAA8FB743BAD43BCC240A86D1,
	ActionTransformMove_t85738713489E31DF2CEDD5AF60A1B8730F61B5A1_CustomAttributesCacheGenerator_ActionTransformMove_Execute_m83A314D67419B65DB5ADD6C174738DFE15984D53,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8__ctor_mBBC55362DD6F73D1A9D3FD8C3F815D57E473521A,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_IDisposable_Dispose_m3BE76413C35E97EE41BF887F5C94338A08F59274,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B2FDCD99FC7D00F3F2E45033E4D4E43C434EF29,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_Reset_m9BFD11409852AC030EAD6175AF52C6C04FF2797B,
	U3CExecuteU3Ed__8_t0E9143553536967149F6C8B5A114DA7EFECB76AA_CustomAttributesCacheGenerator_U3CExecuteU3Ed__8_System_Collections_IEnumerator_get_Current_m826A920F08D5B9712D8CAD38A583A9014C784C76,
	ActionTransformRotate_t9CFF5A492010A0DE767A8F7F9D5436F3CED6A3AD_CustomAttributesCacheGenerator_ActionTransformRotate_Execute_m52E7014DC3C1A4BC3CDDBA1EF21E37AF4EE154C0,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m3EA267B4AAEB1FB64D9098EE4ACCA34AEA590FF8,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m213250BD24E8EE8853B913051A787012D435EB5F,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0305DABCAE84612AD3F76206412BD342D52129E,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m204D54BF27D7CFB6F425C2D20648D125078A38A6,
	U3CExecuteU3Ed__6_tA7AE9A5006397684618FFA76206099CAAFE54030_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_mE4DFC63A85A78AD6793EA847AC75148919B971AA,
	ActionTransformRotateTowards_tF44DDE238077CFEB5FC5988BC1DDDCCA645A8B13_CustomAttributesCacheGenerator_ActionTransformRotateTowards_Execute_m8C328A448FA212122A818FAEC0C3D3AC2404A146,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5__ctor_m0706F423CF92DB9367533BF6A9123319F8CA8278,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_IDisposable_Dispose_mCD2F8FA04E201EF31D00ACA8DD462E0A1E22BB9D,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF72942B4B178CA78A6749390E9BC859525A6EA48,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_Reset_m398E925CAA656EED3C0632C1A3ACBD8F10B14364,
	U3CExecuteU3Ed__5_t03A715F978B4CA1D9C3E7BCDA1DAE21FC216ECA2_CustomAttributesCacheGenerator_U3CExecuteU3Ed__5_System_Collections_IEnumerator_get_Current_mBA9BBCAB06CC9DF538CC5EC1E17E21165FD96FA9,
	ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator_ActionLoadGame_Execute_mCA4BCD88110DF8FFB57B75DA02CA1D13A1B9E82A,
	ActionLoadGame_tED441D7C12BF5C18573771E618CAA51B540E0561_CustomAttributesCacheGenerator_ActionLoadGame_U3CExecuteU3Eb__3_0_m5D65A8280B7151F6FBAD8401F772A0B860F290E0,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mEF7EE534F5AB0E4EC07E539E821DA3AC95D970D1,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m2DDDC6B615D89AC51CF63297B2EB8A205C1AAE4B,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C3C7C7718E40C63F6851C0502FBCBAE5D51D10,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_m6110CA08FADF00FD94D352C8091DE1191313BC5C,
	U3CExecuteU3Ed__3_tE2D18D3B2FB40133E8105594DD194228E609F537_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_mC29C82265D7A10E38E4D4595D194B45C4A17ABE8,
	ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator_ActionLoadLastGame_Execute_m0661A69EA5997349140E8D5EB8F76E601B056394,
	ActionLoadLastGame_t959788581B57E7F267E68C46704670B6CA92B958_CustomAttributesCacheGenerator_ActionLoadLastGame_U3CExecuteU3Eb__1_0_m03276802B4B26570E825CDA0F2E78DFE703FD2EE,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1__ctor_mDED1314A51DDE61865770C493D2D9CCDE42C4957,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_IDisposable_Dispose_mB03A5268D828F14A443D72BCD62F9977A7434C2E,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE706719587A7A7BAE947DAB178A404636438E62A,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m0A98AD78FF4D9FDFAFB4E2B4ADC7E918ECE084B7,
	U3CExecuteU3Ed__1_tE0720B71FD0D10F9245EFC8A2C2A6FE220731F29_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m14B357E056A4AADB78A9CADBC779A46325788ACD,
	ActionSaveGame_t77E39E382C2C29FF3E9B619D141500A30E87905E_CustomAttributesCacheGenerator_ActionSaveGame_Execute_m5C49646C313E617CF557E9FEE2D5EBC3AAA9FA10,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m1A294E66E9D46043E2E23F4544014588A8D604B5,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_m14FDC26EDECC814A10CAA9F29C23A76370143B6D,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF519EBD37B180B513D8F652C309B25B1F9E64536,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_mF841C3F84566662D6B63615136CA3AD0C469B029,
	U3CExecuteU3Ed__2_t4AD66998A3146BE2E3930A16FAB0E78AA7A8CB34_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m488D15C23530AC60B5EA48F0D808A137B7F2ECDC,
	ActionLoadScene_tA8C68DF3AC67C2B7A83125DD42B7D7BC6A3B940C_CustomAttributesCacheGenerator_ActionLoadScene_Execute_mE4830BE17B75DC6B0D7C331BA8B1D2D73A4821A3,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mB38F8F46EAA249E49BF209D521CA8E4DAD872375,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_m9A55C15113480F1DFED0E5D92EFCA663FAD44C22,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0C73195701400504EFD358451A634E203F71E2,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mC95F035CC2C00DD21F7CEB6C10C00AC264472D05,
	U3CExecuteU3Ed__3_t3D0850DAB7E4F314BF4587FDD1290C0639449CED_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m61B9FE124AD3E7B14B38A197435F1766A601DAC8,
	ActionUnloadSceneAsync_t3529F909FA7691CECF11F5A3A18E1AC187E60892_CustomAttributesCacheGenerator_ActionUnloadSceneAsync_Execute_m221F4EA5967752F1CE262C88154BBE883F5C5D7C,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1__ctor_m50099F295DA27DCA901FA8869D15AABEFB2A89F8,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_IDisposable_Dispose_m73C667B2CD0942FC8C1D621030F62DE6EBCF43EE,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD24FFC8179AF6C49CF69C41720A1CA67CCCC3FF5,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_Reset_m7A06787F5F87A17EDF4B9F7E89E5F121D17FAA6B,
	U3CExecuteU3Ed__1_t2726F92CC4A8F6485B940F1F00B86AF548864641_CustomAttributesCacheGenerator_U3CExecuteU3Ed__1_System_Collections_IEnumerator_get_Current_m302C6CB72E7DC52A8CE4D1CF360309C511F79770,
	ActionCanvasGroup_tCAF2FA593A2506F850EF3AB15EA2BEEAA97813A5_CustomAttributesCacheGenerator_ActionCanvasGroup_Execute_m99266FDCA2C58E3B774F0F5D01CE04A0755E06DE,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6__ctor_m27E44C3A9DF57ADC577B19802B5193AE67FF720F,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_IDisposable_Dispose_m6EDEDC583396BC219AC77E7CC4519AFF08AA95E0,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DC9E2350D3BAFA324B15BADB658E25EC071426,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_Reset_m9A9BC40719774A6E6D1F34F82FE21CF69C9A2B55,
	U3CExecuteU3Ed__6_tB2EEE872C5C576F2898BD0FBB1899771F5EF9300_CustomAttributesCacheGenerator_U3CExecuteU3Ed__6_System_Collections_IEnumerator_get_Current_m2AF14DBD96EB0B156A6600D4C0B33F3BB952F55D,
	ActionGraphicColor_tBE813AA8129BD51BE27578A6F91CF45F972B89FC_CustomAttributesCacheGenerator_ActionGraphicColor_Execute_mB50BCF17FFC8C783B2CAC5AA6999EC69282DBC60,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4__ctor_m719692616249D8B1B9A18A4639A2BEBCF839B63E,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_IDisposable_Dispose_m322211ED0B980FF930875657D598C03697632275,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F499A2C7AEE9F7CAFC35900478C44882351C41B,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_Reset_mC64DC9F5F707FDAA833235FBCA5AB402F4B49918,
	U3CExecuteU3Ed__4_tE33EA61FE816FA570EEF6D6D86CDBA5AA9DEE3D6_CustomAttributesCacheGenerator_U3CExecuteU3Ed__4_System_Collections_IEnumerator_get_Current_mAC358A6CCA560BCC9EFA484A7E9CE5F4D47CB979,
	IgniterOnLoad_tB7DBC9B30CDAFB0B01CF881A4CA32A821AEB3B30_CustomAttributesCacheGenerator_IgniterOnLoad_DeferredOnLoad_m64C11C5F16BD9E5EAEC2D50D48E978F52A238DC6,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3__ctor_m5C8E1E3AA4B98F20105756C6130E53147A68BD9F,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_IDisposable_Dispose_mB475A3D3A9F97EDC5FD87B397BC25ECBDE3760FF,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BF406ACFB64F6A8D3582DD5C4686BD52C2EC93E,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_Reset_m5E740051AF8A54914754A26FF03008A1B83E4C1E,
	U3CDeferredOnLoadU3Ed__3_tA6CF30B521C8E390658FED8F29B660E02489C313_CustomAttributesCacheGenerator_U3CDeferredOnLoadU3Ed__3_System_Collections_IEnumerator_get_Current_mD8F7F0E8B01AF6EC3C85ACB8B40A7FF6242DEF73,
	Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator_Conditions_InteractCoroutine_m28D3C22682852BF779FC1C09E3A86331BB9EEB64,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5__ctor_mD95C463FEDE49E3E955F7F9A1BB8C951B75C3122,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_IDisposable_Dispose_mF1F4A66B05E74B40A8654C03EC212E38000C6FCF,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BCBBC8CAA0E427B467E5E183B7D934C8E4D42B,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCFDA796A50A6135EA71F3FE4E5BC9E44C478C3E1,
	U3CInteractCoroutineU3Ed__5_t394FA2D800EC3299DBD42C37059BA49AFD5FCF2E_CustomAttributesCacheGenerator_U3CInteractCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mE8C1D7E5CA1FBBB04F671E6939B8BE5D7209572C,
	ScenesData_t17351779447C737995FE2021DC3AD84A983D2324_CustomAttributesCacheGenerator_ScenesData_OnLoad_m9DD2B0255D05F2CC56291A43DA68F1D794D9B8BA,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8__ctor_m2623EB815DC1D76469AE888CCFC58B3EC04319B0,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_IDisposable_Dispose_mDF1AEC6C64F34D03F84D44928EF2E34986C0CC9A,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC76155CEECA7B46580F2B1A8028F9F301BCDDAB,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_IEnumerator_Reset_m56DC36C13893D1FB4DF0C015C520BFE5A539A182,
	U3COnLoadU3Ed__8_t7780A326880D32DE519D9A572CFBCF5B8B09BFC7_CustomAttributesCacheGenerator_U3COnLoadU3Ed__8_System_Collections_IEnumerator_get_Current_mA8ABABF1674E47A2E404A53461DBD8AF95075ED6,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_IsLoading_m48BA1B1C94B4C49EDD9AE123CB172588AB0F677B,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_IsLoading_m72592D60732CD135C5A77D6F0983FB422F577C07,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_IsProfileLoaded_m5284D5C2C5C09F8DD70C4F0F655C382F849A95FC,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_IsProfileLoaded_mEF54E5023DF9D2A82D2B1B725486EACABE8A93FA,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_ActiveProfile_m9738798B39E0A1677771A2D85620B66F7D9E4D6E,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_ActiveProfile_mE1B1C4AE9C7C893FF128D3693F0AECB79E4AA4C2,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_LoadedProfile_mB06CD36F75FFBC8CD6CF6EAA8A9D49B52AAC186A,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_LoadedProfile_mA4FCE167840C7250ED217EB4D9B03F89C7D4EFC2,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_get_savesData_mF5D63C886C2D47F216FA1BBBF71EA45CB5E825AD,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_set_savesData_m25C89E92AB5C3540BA52D7A12A63801808C510E6,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_InitializeOnLoad_m10EFC6CEB1BF1717756D029431963EDF4A3AB8E0,
	SaveLoadManager_t1CCF2186B515BE69213A9FAE5A73E2058216B69C_CustomAttributesCacheGenerator_SaveLoadManager_CoroutineLoad_mA7DDBA766A8C4F7A96D4F726AA978178F4FE9CF0,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41__ctor_m3223DF3BDBA925B9C01B1075D3CD3347CD43AEC0,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_IDisposable_Dispose_m9988DEBB87C0389FF609CC218B64C147B325A8FA,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m801AB1F500A3A2998AD56254068EE4CFEF1AFC57,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_Reset_m1C07BD4300758CD834338BDDECA0875E978F353E,
	U3CCoroutineLoadU3Ed__41_t70D9E0DF1A2F3B3240C4F3FF086549E0F9686503_CustomAttributesCacheGenerator_U3CCoroutineLoadU3Ed__41_System_Collections_IEnumerator_get_Current_m5193714FB64E1B8E2DD8DACEBC03DD5545EF39D2,
	ButtonActions_tBD805CDFD93B7EAB55ADC0EDC43018A193868CAA_CustomAttributesCacheGenerator_ButtonActions_OnFinishSubmit_m913CD3FD4E3EDFF039CFE2655DE2FDA322EE6570,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6__ctor_m893E2EF3FB1B81650DC4D1CCFC1FCB37398F45EF,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_IDisposable_Dispose_m2B5E0BBD91BB5335BF4D0D72AD2B8E0D12ADEC7A,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE497123EB4BC9F432B4072E2D058559767136CAB,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_Reset_mFB604330DB8D7CD8B5C768C549F2946B73F59377,
	U3COnFinishSubmitU3Ed__6_tABC2AC28ED4158566C20EA3025FB97BC10821231_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ed__6_System_Collections_IEnumerator_get_Current_m43798DF5AB19460ECD104AF7F4F2FC05ADB14768,
	IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mC7BC452CD9CEB42F6B5FF76A1653C0318E4E7F9F,
	IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2__ctor_m7DDC9C9F4F6271CBC115EB695C09CD7020514C91,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_IDisposable_Dispose_mCE34EB7AF956AF35D260652DAA9487875F5E5E18,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD451C8F90AB42372F393CA15C9D4C30110852768,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_Reset_m15914DD547004FBF1D2595DB46564433345D5090,
	U3CExecuteU3Ed__2_t91AC9AAE6779C28DD3F8FB10AFDE0D39FE5CB136_CustomAttributesCacheGenerator_U3CExecuteU3Ed__2_System_Collections_IEnumerator_get_Current_m9F64445ABE6D9783AD5D9537B7C8AAA82798ACEF,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3__ctor_mC5BAFE1C6CE5BF49D9685E6382F0D67665BA1271,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_IDisposable_Dispose_mD044F79A0BF7913EA3A7F7D88B63DC0F6676553E,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD2403DC5621D9B93CD571F03569AE0A49546A16,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_Reset_mB8CD42CE43D0BE19240A03A82805AAE7BDB19368,
	U3CExecuteU3Ed__3_t94BA9DD5401D4AA99215368079520966BB17708C_CustomAttributesCacheGenerator_U3CExecuteU3Ed__3_System_Collections_IEnumerator_get_Current_m0810B2FE029F9C067DCCFD795ADD587BC8E9B744,
	IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_get_coroutine_m43BF4DBCBD32FA5208B4AC6690372328638225F4,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_set_coroutine_m84480EA36BEBF51030B497DC4EC7E7CDC3C93F64,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_get_result_mC8B5641A49CCF3B6CDE09E713E41E47D51B78BD5,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_set_result_m5C7494A81C65703C4CA6CA479A3196B72508332B,
	ActionCoroutine_tA8F422A7607A53B4FFF4A63FECA2D04466230F83_CustomAttributesCacheGenerator_ActionCoroutine_Run_m5E2B10DAD3C70CE29D7C2785F8A0C216EFB90C40,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10__ctor_m161D5DDA96EC9824164EAA25CFBA7959B31C271C,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_IDisposable_Dispose_m4CB135F3D4224B9BAED517629FE906CE77A206F1,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96EC009DA64F1B02F4571CF2C15B947E77829245,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_IEnumerator_Reset_m1061AF69C60892787419DC2081CFB2D051A34488,
	U3CRunU3Ed__10_t7A6114E7243B3C0E33B246FB657E245976D9BD1A_CustomAttributesCacheGenerator_U3CRunU3Ed__10_System_Collections_IEnumerator_get_Current_m7AF120E5C89DA314DC5E5AB7794A4F40258E1F0D,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7__ctor_m83AD0BC314DA34572FA3D490CE3253BFB7F5FA68,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_IDisposable_Dispose_mA37486D7327AA40D8CC84D0492AB0A0121403CB0,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA7ABC9F3A7C54472C63995C362F345FDCC8862B,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m81F9FA9DDF2680024453C428C9D7EEF0CA4F7ED5,
	U3CExecuteCoroutineU3Ed__7_t0F4E18D0EBEE9AA70827164CF8B68AD3217466E1_CustomAttributesCacheGenerator_U3CExecuteCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_m4A0F6B20C3E45CE35088B56C0E871F65FB617DEE,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_currentCharacter_m0F54B6D1EDB6099B9702A01A5B9834B74EC300A7,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_currentCharacter_m827A7EB986EA3DC7D3A2226918C13B18E732255E,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_currentToken_mCD8FA828E6CC412CF1DFE31196E63C2FEB6B5F00,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_currentToken_m734975EE88A7476FFC961BE9C066FEF54FC7A574,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_number_mE49477FA7BB77D7AA94B3659689F5FB6AF0FD4B1,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_number_mEAE023F2196EB4BA001B1120783D84C0E7F29C47,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_get_identifier_mCE413649912A31A44093A6BC8B0CC673405ADF79,
	Tokenizer_tE477CDE2BB1DC7F04B069E60A5844E53BB4FC67C_CustomAttributesCacheGenerator_Tokenizer_set_identifier_mB59DC360B4A15FA7675821A2D0F05F6DD3591A0F,
	ActionAdventureCamera_t9387732AD9054AC8879BC4F032252DFE2BA043F4_CustomAttributesCacheGenerator_ActionAdventureCamera_Execute_m4B5B9A7099FE47828DCE6EB5948F385DA040162D,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11__ctor_m0200F3F5D3D966175D9581F749E6B5A835BBA109,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_IDisposable_Dispose_m00942BBE5812FCCE5A1F9497036D58AE8C9E927E,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68C82A5E23D0F81B0514376497701F2EE9F32A67,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_IEnumerator_Reset_mDE04DF8E62B2F75FA258B59AB333597FBC2E57A7,
	U3CExecuteU3Ed__11_t6555FEA826AFE81A75463118CD4853E42BA44EEB_CustomAttributesCacheGenerator_U3CExecuteU3Ed__11_System_Collections_IEnumerator_get_Current_m421DCFCEB82C325052928E10715A6D19482F238F,
	VariableFilterAttribute_t816F7376E87ECF45EB9A6374D49407E0132AB6DD_CustomAttributesCacheGenerator_VariableFilterAttribute__ctor_m6827BBBBD1C3C45138DECC08A28CA7329B4B5639____types0,
	ActionCharacterRotateTowards_t1AB327BA1C69661FD4BF3A757A38EF8E5511A8A7_CustomAttributesCacheGenerator_ActionCharacterRotateTowards_Execute_mB59646DE966C7A38B1BA7EFAE882BC70A40BA95B____parameters3,
	CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_CrossFadeGesture_m1F6197A3A14AB8E9EE47EAB409CE37AC6A7BB230____parameters5,
	CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_SetState_mF674B49D07C0F6E9E1A28331F0189379391B5CDB____parameters6,
	CharacterAnimation_t27CD4030712E82BE3D04B461056EBF5E3BD72FAF_CustomAttributesCacheGenerator_CharacterAnimation_SetState_m8C3CE69D37ECC30B3B9F9C352B2A3D365942A4D2____parameters7,
	PlayableGestureRTC_t2DF9641C562EEC688128A23B56A0F66A35681E3F_CustomAttributesCacheGenerator_PlayableGestureRTC_Create_mAB25365F7D4B64317DFE04EFCB031E3728DA3366____parameters8,
	PlayableGestureRTC_t2DF9641C562EEC688128A23B56A0F66A35681E3F_CustomAttributesCacheGenerator_PlayableGestureRTC_CreateAfter_m171D1CC7E4B96BD20FA8DA9906BC797FC6B791A6____parameters7,
	PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_Create_mFCCFE038760E4D43A4EB8FE25A5A740389DE1850____parameters11,
	PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_CreateAfter_mD3174E0DE0FB1886551378074FC02AE8BC6FFDF9____parameters10,
	PlayableStateCharacter_t8FA144E47825722A79F77ACDA8C593F87E8175E2_CustomAttributesCacheGenerator_PlayableStateCharacter_CreateBefore_m541F003F91662EAF4A5C74B16127F0A19D9816D0____parameters10,
	PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_Create_mE1EF1120BD64AFC4853B67411BC949CED140C1F6____parameters10,
	PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_CreateAfter_m04896D425083377B366B74F0B217FA12F4EC00CE____parameters9,
	PlayableStateRTC_t6D7556F263E26174ECA104200BA1BC2B1EC334DE_CustomAttributesCacheGenerator_PlayableStateRTC_CreateBefore_mDB2045A10400A6AF3BFA8F6DC46B31723666487D____parameters9,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_CrossFadeGesture_mE39F18534A58C1963F900C2D6FE97E8CADA6AF66____parameters5,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_SetState_mB85A38CEDE7CC182D218720577DC5E982BF28ADA____parameters6,
	CharacterAnimator_t674CBEECA00F0B49E707E2635767111D55A6D8BD_CustomAttributesCacheGenerator_CharacterAnimator_SetState_m2A2E1EC36A2F8F29C58943482954A613185F21CC____parameters7,
	RagdollUtilities_t4E030B55562006D3A12299CD3EF3F186A340A87C_CustomAttributesCacheGenerator_RagdollUtilities_GetBounds_m663E840916CD6689EE671CBC57ECA6729C7757CD____points1,
	Igniter_t646789CC3B475402B3B3E83FD35510319DF8155A_CustomAttributesCacheGenerator_Igniter_ExecuteTrigger_m5913E13D844E3833A79E888FA2F887EAA1BDB204____parameters1,
	Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_Actions_Execute_m028F25C515D5D301413DB5E45F9C970A69C3387A____parameters1,
	Actions_tE28D7D56404677F71D3AA4A8A7276823218197D5_CustomAttributesCacheGenerator_Actions_Execute_m753E2E2998E02C5F2E806D59A36F5FB78C9A753D____parameters0,
	Conditions_t0AABB1AD0DB91FE8C6B3CAB2156C31155523DAD6_CustomAttributesCacheGenerator_Conditions_Interact_m730603257F8CBFDE70A458D95E42F1374675F25A____parameters1,
	Trigger_tB84456649DF19415D5DD3E52F3505B0148868152_CustomAttributesCacheGenerator_Trigger_Execute_m1A6FC0B07CE3CC98139220B8963BE093CF3F0EFE____parameters1,
	Singleton_1_tCC578427A4C18FBE3462F681D0BA3220CFA6F12C_CustomAttributesCacheGenerator_Singleton_1_DebugLogFormat_mEA821C32B38BAB9F92B41475B4B6FFC881BF4CDA____parameters1,
	Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator_Clause_CheckConditions_mC801467291AC7571A50985555DB38737062BC8F7____parameters1,
	Clause_tE9548122788716FFC9870CCFFB0BFE15EDD52FF6_CustomAttributesCacheGenerator_Clause_ExecuteActions_m0CFB6797D8E897ACD15B1E4097B5CCFF6C06468B____parameters1,
	IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_InstantExecute_m512502AA6B9A94C8B5563F89D270D82275BD61C6____parameters3,
	IAction_tFD3FAF9B7EED45C3433631D4D93A6F84E52C5696_CustomAttributesCacheGenerator_IAction_Execute_mE7A03FED8D84B5F458BA4080FC02AFB56D2D97AE____parameters3,
	IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_Execute_m58A609A93DAF9BB26DDB4996AC23A068450D6D6B____parameters2,
	IActionsList_t99FA582E195EC6E72F0971A5D1E46BB3F905C525_CustomAttributesCacheGenerator_IActionsList_ExecuteCoroutine_m8A227EA885AE12D3903EB57C997D298739C8FECE____parameters2,
	ICondition_t52D9BDEF26917F52FA8650B20FCF835A9625551A_CustomAttributesCacheGenerator_ICondition_Check_mEC8DAE0A9AEEAC0944D71A55F212091BC90562C0____parameters1,
	IConditionsList_t572443118DEF6B658B137E448305AC95B87BF172_CustomAttributesCacheGenerator_IConditionsList_Check_mE39A03DEA4F39E41F50EF48BEFA52F265AF7CE48____parameters1,
	AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
